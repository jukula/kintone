// **********************************************************************************
//
//  本山町農業公社  スケジュール管理
//
//  履歴
//    2020.01.20    新規作成(Jun.K)
//                                                        Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){


// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var title = '';

console.log('予定=' + record['予定']['value']);

        // 表示用タイトル設定
        if(record['予定']['value']) {
            title = record['予定']['value'];
        }
        if(record['タイトル']['value']) {
            if(title == "") title = record['タイトル']['value'];
            else            title = title + ' ' + record['タイトル']['value'];
        }
        if(record['担当者']['value']) {
            var tanto = '';
            for(i=0; i<record['担当者']['value'].length; i++) {
                if(tanto != "") tanto = tanto + ',';
                tanto = tanto + record['担当者']['value'][i]['name'];
            }
            if(title == "") title = tanto;
            else            title = title + ' ' + tanto;
        }

        record['表示用タイトル']['value'] = title;

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.タイトル', 'app.record.edit.change.タイトル'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {
/*
        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }
*/
    }

})();

