// **********************************************************************************
//
//  本山町農業公社  検査業務集計表
//
//  履歴
//    2020.09.23    新規作成(Jun.K)
//                                                        Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){


// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;

console.log('出荷基準=' + record['出荷基準']['value']);

        // 初期化
        record['天空の郷_1等']['value'] = "";
        record['天空の郷_2等']['value'] = "";
        record['天空の郷_3等']['value'] = "";
        record['天空の郷_規格外']['value'] = "";
        record['天空の郷_等外']['value'] = "";
        record['天空の郷_水分']['value'] = "";
        record['天空の郷_格付け理由']['value'] = "";
        record['棚田米_1等']['value'] = "";
        record['棚田米_2等']['value'] = "";
        record['棚田米_3等']['value'] = "";
        record['棚田米_規格外']['value'] = "";
        record['棚田米_等外']['value'] = "";
        record['棚田米_水分']['value'] = "";
        record['棚田米_格付け理由']['value'] = "";
        record['中米_1等']['value'] = "";
        record['中米_2等']['value'] = "";
        record['中米_3等']['value'] = "";
        record['中米_規格外']['value'] = "";
        record['中米_等外']['value'] = "";
        record['中米_水分']['value'] = "";
        record['中米_格付け理由']['value'] = "";
        record['一般米_1等']['value'] = "";
        record['一般米_2等']['value'] = "";
        record['一般米_3等']['value'] = "";
        record['一般米_規格外']['value'] = "";
        record['一般米_等外']['value'] = "";
        record['一般米_水分']['value'] = "";
        record['一般米_格付け理由']['value'] = "";

        // 出荷基準
        var kijun = record['出荷基準']['value'];
        if(kijun == "契約栽培米") kijun = "一般米";

        // 袋数
        record[kijun + "_" + record['等級']['value']]['value'] = record['袋数']['value'];

        // 水分
        record[kijun + "_水分"]['value'] = record['水分']['value'];
        
        // 格付け理由
        record[kijun + "_格付け理由"]['value'] = record['格付け理由']['value'];

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.タイトル', 'app.record.edit.change.タイトル'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {
/*
        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }
*/
    }

})();

