// **********************************************************************************
//
//  本山町農業公社
//
//  履歴
//    2020.03.10    受委託管理(春)に対応するために本山_委受託管理.jsを修正(honda)
//    2020.03.25    受委託管理(春)の各作業が自動計算を行えるように編集(honda)
//    2020.03.31    受委託管理(春)の現場計算が可能なように編集(honda)
//    2020.04.09    土壌分析費用だけ独立(Jun.K)
//
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){
    "use strict";

    // **********************************************************************************
    // アプリ定義
    // **********************************************************************************
    const App = {
        // 開発
        63: {
                calenderAppId: 64,     // 春カレンダーアプリ
            },
        // 本番
        67: {
                calenderAppId: 80      // 春カレンダーアプリ
            },
    }

    // **********************************************************************************
    // 各作業の自動計算
    // **********************************************************************************

    // *********************************
    //  現場計算
    // *********************************
    function genba(record, price) {
        var gKikai = 0;
        var gChingin = 0;
        var a = String(record['町外出張_育苗_']['value']);
        var b = String(record['町外出張_水田耕起_']['value']);
        var c = String(record['町外出張_水田整地_']['value']);
        var d = String(record['町外出張_田植え_']['value']);


        // 賃金と機械単価を取得
        switch(price.作業種類.value) {
            case '農作業':

                // 賃金を取得
                gChingin = Number(price.賃金.value);

                // 作業員追加時間が入力されている場合、追加賃金加算
                if(record['作業員追加時間_育苗_']['value'] !== undefined && record['作業員追加時間_育苗_']['value'] != ""){
                    gChingin = Number(price.賃金.value) * Number(record['作業員追加時間_育苗_']['value']);
                    record['育苗賃金']['value'] = Number(record['育苗賃金']['value']) + gChingin;
                }
                if(record['作業員追加時間_水田耕起_']['value'] !== undefined && record['作業員追加時間_水田耕起_']['value'] != ""){
                    gChingin = Number(price.賃金.value) * Number(record['作業員追加時間_水田耕起_']['value']);
                    record['水田耕起賃金']['value'] = Number(record['水田耕起賃金']['value']) + gChingin;
                }
                if(record['作業員追加時間_水田整地_']['value'] !== undefined && record['作業員追加時間_水田整地_']['value'] != ""){
                    gChingin = Number(price.賃金.value) * Number(record['作業員追加時間_水田整地_']['value']);
                    record['水田整地賃金']['value'] = Number(record['水田整地賃金']['value']) + gChingin;
                }
                if(record['作業員追加時間_田植え_']['value'] !== undefined && record['作業員追加時間_田植え_']['value'] != ""){
                    gChingin = Number(price.賃金.value) * Number(record['作業員追加時間_田植え_']['value']);
                    record['田植え賃金']['value'] = Number(record['田植え賃金']['value']) + gChingin;
                }
                break;
            case '遠距離農作業(～20km)':
            case '遠距離農作業(20km～)':
            case '遠距離農作業(30km～)':

                // 賃金と機械単価を取得
                gChingin = Number(price.賃金.value) * 1;
                gKikai = Number(price.使用機械.value) * 1;
                
                // 町外出張有りの場合、距離に応じて、機械および賃金加算
                if(a.indexOf('有') !== -1){
                    record['育苗機械費用']['value'] = Number(record['育苗機械費用']['value']) + gKikai;
                    record['育苗賃金']['value'] = Number(record['育苗賃金']['value']) + gChingin;
                }
                if(b.indexOf('有') !== -1){
                    record['水田耕起機械費用']['value'] = Number(record['水田耕起苗機械費用']['value']) + gKikai;
                    record['水田耕起賃金']['value'] = Number(record['水田耕起賃金']['value']) + gChingin;
                }
                if(c.indexOf('有') !== -1){
                    record['水田整地機械費用']['value'] = Number(record['水田整地機械費用']['value']) + gKikai;
                    record['水田整地賃金']['value'] = Number(record['水田整地賃金']['value']) + gChingin;
                }
                if(d.indexOf('有') !== -1){
                    record['田植え機械費用']['value'] = Number(record['田植え機械費用']['value']) + gKikai;
                    record['田植え賃金']['value'] = Number(record['田植え賃金']['value']) + gChingin;
                }
                break;
        }               
    }

    // *********************************
    //  育苗計算
    // *********************************
    function ikubyou(record, price) {
        var a = 0;
        var kasan = 0;
        var iKikai = 0;
        var iChingin = 0;

        switch(price.作業種類.value) {
            case '土壌分析':
                // 春作業種類の土壌分析にチェックがついていたら → 土壌分析だけ別で加算
                record['土壌分析費用']['value'] = Number(price.賃金.value) * Number(record['土壌分析']['value']);
                break;
            case '育苗':
                //　春作業種類の育苗にチェックがついていたら
                iChingin = Number(price.賃金.value) * Number(record['育苗数']['value']);
                break;
            case '箱処理':
                //　春作業種類の箱処理にチェックがついていたら
                a = Number(record['箱処理数']['value']);
                kasan = Number(record['箱処理薬代']['value']) * a;
                iChingin = Number(price.賃金.value) * a + kasan;
                break;
            case 'クイックレベラー':
                // 春作業種類のクイックレベラーにチェックがついていたら
                iKikai = Number(price.使用機械.value) * Number(record['クイックレベラー']['value']);
                break;
            case 'ポット運搬':
                //　春作業種類のポット運搬にチェックがついていたら
                iChingin = Number(price.賃金.value) * Number(record['育ポット運搬数']['value']);
                break;
        }
        
        // 育苗の機会費用、賃金計算
        record['育苗機械費用']['value'] = Number(record['育苗機械費用']['value']) + iKikai;
        record['育苗賃金']['value'] = Number(record['育苗賃金']['value']) + iChingin;

    }

    // *********************************
    //  水田耕起計算
    // *********************************
    function suidenkouki(record, price) {
        var ko = 0;
        var f = 0;
        var kon = 0;
        var une = 0;
        var kKikai = 0;
        var kChingin = 0;

        switch(price.作業種類.value) {
            case '水田耕起':
                // 春作業種類の水田耕起にチェックがついていたら
                ko = Number(record['水田耕起作業面積']['value']);
                kKikai = Number(price.使用機械.value) * ko / 10;
                kChingin = Number(price.賃金.value) * ko / 10 + 500 * Number(record['水田耕起作業面積5a未満']['value']);
                break;
            case 'フレールモア':
                // 春作業種類のフレールモアにチェックがついていたら
                f = Number(record['フレールモア作業面積']['value']);
                kKikai = Number(price.使用機械.value) * f / 10;
                kChingin = Number(price.賃金.value) * f / 10 + 500 * Number(record['フレールモア5a未満']['value']);
                break;
            case 'コンポキャスタ':
                // 春作業種類のコンポキャスタにチェックがついていたら
                kon = Number(record['コンポキャスタ作業面積']['value']);
                kKikai = Number(price.使用機械.value) * kon / 10;
                kChingin = Number(price.賃金.value) * kon / 10 + 500 * Number(record['コンポキャスタ5a未満']['value']);
                break;
            case '畝立整形':
                // 春作業種類の畝立整形にチェックがついていたら
                une = Number(record['畝立整形作業面積']['value']);
                kKikai = Number(price.使用機械.value) * une / 10;
                kChingin = Number(price.賃金.value) * une / 10 + 500 * Number(record['畝立整形5a未満']['value']);
                break;
        }
        
        // 水田耕起の機会費用、賃金計算
        record['水田耕起機械費用']['value'] = Number(record['水田耕起機械費用']['value']) + kKikai;
        record['水田耕起賃金']['value'] = Number(record['水田耕起賃金']['value']) + kChingin;
    }

    // *********************************
    //  水田整地計算
    // *********************************
    function suidenseichi(record, price) {
        var sei1 = 0;
        var sei2 = 0;
        var sei3 = 0;
        var sei4 = 0;
        var siro = 0;
        var aze = 0;
        var sKikai = 0;
        var sChingin = 0;

        switch(price.作業種類.value) {
            case '水田整地Ⅰ':
                // 春作業種類の水田整地Ⅰにチェックがついていたら
                sei1 = Number(record['整地1作業面積']['value']);
                sKikai = Number(price.使用機械.value) * sei1 / 10;
                sChingin = Number(price.賃金.value) * sei1 /10 + 500 * Number(record['整地15a未満']['value']);
                break;
            case '水田整地Ⅱ':
                // 春作業種類の水田整地Ⅱにチェックがついていたら
                sei2 = Number(record['整地2作業面積']['value']);
                sKikai = Number(price.使用機械.value) * sei2 / 10;
                sChingin = Number(price.賃金.value) * sei2 / 10 + 500 * Number(record['整地25a未満']['value']);
                break;
            case '水田整地Ⅲ':
                // 春作業種類の水田整地Ⅲにチェックがついていたら
                sei3 = Number(record['整地3作業面積']['value']);
                sKikai = Number(price.使用機械.value) * sei3 / 10;
                sChingin = Number(price.賃金.value) * sei3 / 10 + 500 * Number(record['整地35a未満']['value']);
                break;
            case '水田整地Ⅳ':
                // 春作業種類の水田整地Ⅳにチェックがついていたら
                sei4 = Number(record['整地4作業面積']['value']);
                sKikai = Number(price.使用機械.value) * sei4 / 10;
                sChingin = Number(price.賃金.value) * sei4 / 10 + 500 * Number(record['整地45a未満']['value']);
                break;
            case '代掻き':
                // 春作業種類の代掻きにチェックがついていたら
                siro = Number(record['代掻き作業面積']['value']);
                sKikai = Number(price.使用機械.value) * siro / 10;
                sChingin = Number(price.賃金.value) * siro / 10 + 500 * Number(record['代掻き5a未満']['value']);
                break
            case '代掻き(ハロー貸出)':
                // 春作業種類の代掻き(ハロー貸出)にチェックがついていたら
                sKikai = Number(price.使用機械.value) * Number(record['代掻き_ハロー貸出_作業面積']['value']) / 10;
                break;
            case '畔塗りⅠ':
                // 春作業種類の畔塗りⅠにチェックがついていたら
                aze = Number(record['畔塗り作業面積']['value']);
                sKikai = Number(price.使用機械.value) * aze / 10;
                sChingin =  Number(price.賃金.value) * aze + 500 * Number(record['畔塗り5a未満']['value']);
                break;
            case '畔塗りⅡ':
                // 春作業種類の畔塗りⅡにチェックがついていたら
                sKikai = Number(price.使用機械.value) * Number(record['畔塗り機貸出']['value']) / 10;
                break;
        }
        
        // 水田整地の機会費用、賃金計算
        record['水田整地機械費用']['value'] = Number(record['水田整地機械費用']['value']) + sKikai;
        record['水田整地賃金']['value'] = Number(record['水田整地賃金']['value']) + sChingin;
    }

    // *********************************
    //  田植え計算
    // *********************************
    function taue(record, price) {
        var taue1 = 0;
        var taue2 = 0
        var tKikai = 0;
        var tChingin = 0;

        switch(price.作業種類.value) {
            case '田植え':
                // 春作業種類の田植えにチェックがついていたら
                taue1 = Number(record['田植え_乗用_作業面積']['value']);
                tKikai = Number(price.使用機械.value) * taue1 / 10;
                tChingin = Number(price.賃金.value) * taue1 / 10 + 500 * Number(record['田植え_乗用_作業面積5a未満']['value']);
                break;
            case '田植え（歩行）':
                // 春作業種類の田植え（歩行）にチェックがついていたら
                taue2 = Number(record['田植え_歩行_作業面積']['value']);
                tKikai = Number(price.使用機械.value) * taue2 / 10;
                tChingin = Number(price.賃金.value) * taue2 / 10 + 500 * Number(record['田植え_歩行_作業面積5a未満']['value']);
                break;
        }
        
        // 田植えの機会費用、賃金計算
        record['田植え機械費用']['value'] = Number(record['田植え機械費用']['value']) + tKikai;
        record['田植え賃金']['value'] = Number(record['田植え賃金']['value']) + tChingin;
    }

    // *********************************
    //  カレンダー登録
    // *********************************
    function setCalender(kubun, sagyoDay, viewCal, orgId) {
        var appId = kintone.app.getId();

        var rec = {
            '作業区分': {"value": kubun},
            '作業日': {"value": sagyoDay},
            'カレンダー表示用': {"value": viewCal},
            '元情報': {"value": 'https://tenku.cybozu.com/k/' + appId + '/show#record=' + orgId},
            '作業終了日': {"value": sagyoDay},
            '終日': {"value": ["終日"]},
            '元レコード番号': {"value": orgId}
        }

        console.log(rec)

        // スケジュール
        var body = {
            'app': App[appId].calenderAppId,
            'record': rec,
//            'records': JSON.stringify(recData),
        };

        console.log(body);

        kintone.api(kintone.api.url('/k/v1/record', true), 'POST', body, function(resp) {
                // success
            console.log(resp);
        }, function(error) {
            // error
            console.log('ERR' + error);
        });
        
    }


    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';

        // 現場作業費
        var recid;
        recid = 19;
        var target = record['春作業種類']['value'];
        console.log('target=' + target);
        // 文字列置換
        target = '\"' + String(target).replace(/,/g, '\",\"') + '\"';
        var body = {
            "app": 39,
            "query": "作業種類 in (" + target + ")", // + target + ")",
            "fields": ["レコード番号", "作業区分", "作業種類", "使用機械", "賃金", "基本料金"]
        }

        //=====================
        // 各作業の金額計算
        //=====================
        return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', body).then(function(resp) {
            if (resp.records[0] !== null) {
                // success
                console.log(resp);
            } else {
                event.error = '金額が取得できません。';
            }
            // 金額設定
            var i;
            record['育苗機械費用']['value'] = 0;
            record['育苗賃金']['value']     = 0;
            record['水田耕起機械費用']['value'] = 0;
            record['水田耕起賃金']['value']     = 0;
            record['水田整地機械費用']['value'] = 0;
            record['水田整地賃金']['value']     = 0;
            record['田植え機械費用']['value'] = 0;
            record['田植え賃金']['value']     = 0;
            for(i=0; i<resp.records.length; i++) {
                // 作業区分で合計する
                switch(resp.records[i].作業区分.value) {
                    case '現場':
                        genba(record, resp.records[i]);
                        break;
                    case '育苗':
                        ikubyou(record, resp.records[i]);
                        break;
                    case '水田耕起':
                        suidenkouki(record, resp.records[i]);
                        break;
                    case '水田整地':
                        suidenseichi(record, resp.records[i]);
                        break;
                    case '田植え':
                        taue(record, resp.records[i]);
                        break;
                }
			}
            return event;
        }, function(resp) {
            console.log('料金が取得できません。');
            return event;        
        
        });

        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success'], function(event) {
        var record = event.record;

        //////////////////////
        // スケジュール登録
        //////////////////////
        var unpanDay = '';
        var kokiDay = '';
        var kokiWork = String(record['実施作業']['value']);
        var kokiMenseki = '';
        var kokiMaisu = '';
        var kokiTotal = '';
        var seichiDay = '';
        var seichiWork = String(record['水田整地実施作業']['value']);
        var seichiMenseki = '';
        var seichiMaisu = '';
        var seichiTotal = '';
        var taueDay = '';
        var taueWork = String(record['田植え実施作業']['value']);
        var taueMenseki = '';
        var taueMaisu = '';
        var taueTotal = '';
        

        // *************
        // 育苗
        // *************
        if(record['運搬実施日']['value'] !== undefined && record['運搬実施日']['value'] !== '') {
            unpanDay = record['運搬実施日']['value'];
        } else if(record['運搬予定日']['value'] !== undefined && record['運搬予定日']['value'] !== '') {
            unpanDay = record['運搬予定日']['value'];
        }
        
        if(unpanDay !== '' && unpanDay !== undefined && unpanDay !== null) {
            console.log(unpanDay);
            var calTitle = record['委託者']['value'] + '/【育苗】' + record['育苗数']['value'] + '枚/' + record['育苗オペレーター区分']['value'];
            console.log(calTitle)
            setCalender('育苗', unpanDay, calTitle, event. recordId);
        }
        
        
        // *************
        // 水田耕起
        // *************
        if(record['水田耕起作業実施日']['value'] !== undefined && record['水田耕起作業実施日']['value'] !== '') {
            kokiDay = record['水田耕起作業実施日']['value'];
        } else if(record['水田耕起作業予定日']['value'] !== undefined && record['水田耕起作業予定日']['value'] !== '') {
            kokiDay = record['水田耕起作業予定日']['value'];
        }
console.log(kokiDay);

        if(kokiWork.indexOf('水田耕起') !== -1){
            var kokiMenseki = '';
            var koukiMaisu = '';
            kokiMenseki = kokiMenseki + record['水田耕起作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['水田耕起作業面積5a未満']['value'] + '枚';
            kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            console.log(kokiTotal);
        } 
        if(kokiWork.indexOf('フレールモア') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['フレールモア作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['フレールモア5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + kokiMaisu;
            }
        }
        if(kokiWork.indexOf('コンポキャスタ') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['コンポキャスタ作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['コンポキャスタ5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + koukiMaisu;
            }
        }
        if(kokiWork.indexOf('畝立整形') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['畝立整形作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['畝立整形5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + kokiMaisu;
            }
        }
console.log(kokiWork);

        if(kokiDay !== '' && kokiDay !== undefined && kokiDay !== null) {
            console.log(kokiDay);
            var koukiTitle = record['委託者']['value'] + '/【' + kokiWork + '】/【' + kokiTotal + '】';
            setCalender('水田耕起', kokiDay, koukiTitle, event. recordId);
        }
        
        
        
        // *************
        // 水田整地
        // *************
        if(record['水田整地作業実施日']['value'] !== undefined && record['水田整地作業実施日']['value'] !== '') {
            seichiDay = record['水田整地作業実施日']['value'];
        } else if(record['水田整地作業予定日']['value'] !== undefined && record['水田整地作業予定日']['value'] !== '') {
            seichiDay = record['水田整地作業予定日']['value'];
        }

        if(seichiWork.indexOf('畔塗り') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['畔塗り作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['畔塗り5a未満']['value'] + '枚';
            seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
        }
        if(seichiWork.indexOf('水田整地Ⅰ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地1作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地15a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅱ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地2作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地25a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅲ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地3作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地35a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅳ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地4作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地45a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('代掻き') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['代掻き作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['代掻き5a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
console.log(seichiWork);

        if(seichiDay !== '' && seichiDay !== undefined && seichiDay !== null) {
            console.log(seichiDay);
            var seichiTitle = record['委託者']['value'] + '/【' + seichiWork + '】/【' + seichiTotal + '】';
            setCalender('水田整地', seichiDay, seichiTitle, event. recordId);
        }



        // *************
        // 田植え
        // *************
        if(record['田植え作業実施日']['value'] !== undefined && record['田植え作業実施日']['value'] !== '') {
            taueDay = record['田植え作業実施日']['value'];
        } else if(record['田植え作業予定日']['value'] !== undefined && record['田植え作業予定日']['value'] !== '') {
            taueDay = record['田植え作業予定日']['value'];
        }

        if(taueWork.indexOf('田植え（乗用）') !== -1){
            taueMenseki = '';
            taueMaisu = '';
            taueMenseki = taueMenseki + record['田植え_乗用_作業面積']['value'] + 'a';
            taueMaisu = taueMaisu + record['田植え_乗用_作業面積5a未満']['value'] + '枚';
            taueTotal = taueTotal + taueMenseki + '+' + taueMaisu;
        }
        if(taueWork.indexOf('田植え（歩行）') !== -1){
            taueMenseki = '';
            taueMaisu = '';
            taueMenseki = taueMenseki + record['田植え_歩行_作業面積']['value'] + 'a';
            taueMaisu = taueMaisu + record['田植え_歩行_作業面積5a未満']['value'] + '枚';
            if(taueTotal == ''){
                taueTotal = taueTotal + taueMenseki + '+' + taueMaisu;
            } else {
                taueTotal = taueTotal + '/' + taueMenseki + '+' + taueMaisu;
            }
        } 
console.log(taueWork);

        if(taueDay !== '' && taueDay !== undefined && taueDay !== null) {
            console.log(taueDay);
            var taueTitle = record['委託者']['value'] + '/【' + taueWork + '】/【' + taueTotal + '】';
            setCalender('田植え', taueDay, taueTitle, event. recordId);
        }
        
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on([ 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

//         //////////////////////
//         // スケジュール登録
//         var appId = kintone.app.getId();
//         var recData = [];

//         var body = {
//             "app": App[appId].calenderAppId,
//             "query": "元レコード番号 = " + record['レコード番号']['value'],
//             "fields": ["レコード番号", "作業区分", "カレンダー表示用"]
// //            "id": recid
//         }

//         // カレンダー情報取得
//         return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', body).then(function(resp) {
//             // 情報があるか確認し、登録 or 更新
// console.log(resp);
//             rec = resp.record;
//             delRec = rec.id;

//             return event;
//         }, function(resp) {
//             console.log('情報が取得できません。');
//             return event;        
        
//         });

//         console.log(App[appId].calenderAppId);

// //        return kintone.api('/k/v1/records', 'PUT', body)
//         return event;
//     });
       //////////////////////
        // スケジュール登録
        //////////////////////
        var unpanDay = '';
        var kokiDay = '';
        var kokiWork = String(record['実施作業']['value']);
        var kokiMenseki = '';
        var kokiMaisu = '';
        var kokiTotal = '';
        var seichiDay = '';
        var seichiWork = String(record['水田整地実施作業']['value']);
        var seichiMenseki = '';
        var seichiMaisu = '';
        var seichiTotal = '';
        var taueDay = '';
        var taueWork = String(record['田植え実施作業']['value']);
        var taueMenseki = '';
        var taueMaisu = '';
        var taueTotal = '';
        

        // *************
        // 育苗
        // *************
        if(record['運搬実施日']['value'] !== undefined && record['運搬実施日']['value'] !== '') {
            unpanDay = record['運搬実施日']['value'];
        } else if(record['運搬予定日']['value'] !== undefined && record['運搬予定日']['value'] !== '') {
            unpanDay = record['運搬予定日']['value'];
        }
        
        // if(unpanDay !== '' && unpanDay !== undefined && unpanDay !== null) {
        //     console.log(unpanDay);
        //     var calTitle = record['委託者']['value'] + '/【育苗】' + record['育苗数']['value'] + '枚/' + record['育苗オペレーター区分']['value'];
        //     console.log(calTitle)
        //     setCalender('育苗', unpanDay, calTitle, event. recordId);
        // }
        
        
        // *************
        // 水田耕起
        // *************
        if(record['水田耕起作業実施日']['value'] !== undefined && record['水田耕起作業実施日']['value'] !== '') {
            kokiDay = record['水田耕起作業実施日']['value'];
        } else if(record['水田耕起作業予定日']['value'] !== undefined && record['水田耕起作業予定日']['value'] !== '') {
            kokiDay = record['水田耕起作業予定日']['value'];
        }
console.log(kokiDay);

        if(kokiWork.indexOf('水田耕起') !== -1){
            var kokiMenseki = '';
            var koukiMaisu = '';
            kokiMenseki = kokiMenseki + record['水田耕起作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['水田耕起作業面積5a未満']['value'] + '枚';
            kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            console.log(kokiTotal);
        } 
        if(kokiWork.indexOf('フレールモア') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['フレールモア作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['フレールモア5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + kokiMaisu;
            }
        }
        if(kokiWork.indexOf('コンポキャスタ') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['コンポキャスタ作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['コンポキャスタ5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + koukiMaisu;
            }
        }
        if(kokiWork.indexOf('畝立整形') !== -1){
            var kokiMenseki = '';
            var kokiMaisu = '';
            kokiMenseki = kokiMenseki + record['畝立整形作業面積']['value'] + 'a';
            kokiMaisu = kokiMaisu + record['畝立整形5a未満']['value'] + '枚';
            if(kokiTotal == ''){
                kokiTotal = kokiTotal + kokiMenseki + '+' + kokiMaisu;
            } else {
                kokiTotal = kokiTotal + '/' + kokiMenseki + '+' + kokiMaisu;
            }
        }
console.log(kokiWork);

        // if(kokiDay !== '' && kokiDay !== undefined && kokiDay !== null) {
        //     console.log(kokiDay);
        //     var koukiTitle = record['委託者']['value'] + '/【' + kokiWork + '】/【' + kokiTotal + '】';
        //     setCalender('水田耕起', kokiDay, koukiTitle, event. recordId);
        // }
        
        
        
        // *************
        // 水田整地
        // *************
        if(record['水田整地作業実施日']['value'] !== undefined && record['水田整地作業実施日']['value'] !== '') {
            seichiDay = record['水田整地作業実施日']['value'];
        } else if(record['水田整地作業予定日']['value'] !== undefined && record['水田整地作業予定日']['value'] !== '') {
            seichiDay = record['水田整地作業予定日']['value'];
        }

        if(seichiWork.indexOf('畔塗り') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['畔塗り作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['畔塗り5a未満']['value'] + '枚';
            seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
        }
        if(seichiWork.indexOf('水田整地Ⅰ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地1作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地15a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅱ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地2作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地25a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅲ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地3作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地35a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('水田整地Ⅳ') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['整地4作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['整地45a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
        if(seichiWork.indexOf('代掻き') !== -1){
            var seichiMenseki = '';
            var seichiMaisu = '';
            seichiMenseki = seichiMenseki + record['代掻き作業面積']['value'] + 'a';
            seichiMaisu = seichiMaisu + record['代掻き5a未満']['value'] + '枚';
            if(seichiTotal == ''){
                seichiTotal = seichiTotal + seichiMenseki + '+' + seichiMaisu;
            } else {
                seichiTotal = seichiTotal + '/' + seichiMenseki + '+' + seichiMaisu;
            }
        }
console.log(seichiWork);

        // if(seichiDay !== '' && seichiDay !== undefined && seichiDay !== null) {
        //     console.log(seichiDay);
        //     var seichiTitle = record['委託者']['value'] + '/【' + seichiWork + '】/【' + seichiTotal + '】';
        //     setCalender('水田整地', seichiDay, seichiTitle, event. recordId);
        // }



        // *************
        // 田植え
        // *************
        if(record['田植え作業実施日']['value'] !== undefined && record['田植え作業実施日']['value'] !== '') {
            taueDay = record['田植え作業実施日']['value'];
        } else if(record['田植え作業予定日']['value'] !== undefined && record['田植え作業予定日']['value'] !== '') {
            taueDay = record['田植え作業予定日']['value'];
        }

        if(taueWork.indexOf('田植え（乗用）') !== -1){
            taueMenseki = '';
            taueMaisu = '';
            taueMenseki = taueMenseki + record['田植え_乗用_作業面積']['value'] + 'a';
            taueMaisu = taueMaisu + record['田植え_乗用_作業面積5a未満']['value'] + '枚';
            taueTotal = taueTotal + taueMenseki + '+' + taueMaisu;
        }
        if(taueWork.indexOf('田植え（歩行）') !== -1){
            taueMenseki = '';
            taueMaisu = '';
            taueMenseki = taueMenseki + record['田植え_歩行_作業面積']['value'] + 'a';
            taueMaisu = taueMaisu + record['田植え_歩行_作業面積5a未満']['value'] + '枚';
            if(taueTotal == ''){
                taueTotal = taueTotal + taueMenseki + '+' + taueMaisu;
            } else {
                taueTotal = taueTotal + '/' + taueMenseki + '+' + taueMaisu;
            }
        } 
console.log(taueWork);

        // if(taueDay !== '' && taueDay !== undefined && taueDay !== null) {
        //     console.log(taueDay);
        //     var taueTitle = record['委託者']['value'] + '/【' + taueWork + '】/【' + taueTotal + '】';
        //     setCalender('田植え', taueDay, taueTitle, event. recordId);
        // }
        
    });

    // ========================================================================================================
    // 印刷画面表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.print.show'], function(event) {
        var record = event.record;

        // 各グループをオープン
        kintone.app.record.setGroupFieldOpen('育苗', true);
        kintone.app.record.setGroupFieldOpen('水田耕起', true);
        kintone.app.record.setGroupFieldOpen('水田整地', true);
        kintone.app.record.setGroupFieldOpen('田植え', true);
        kintone.app.record.setGroupFieldOpen('運搬作業', true);

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // 各オペレーター
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.育苗オペレーター区分', 'app.record.edit.change.育苗オペレーター区分',
                       'app.record.create.change.水田耕起オペレーター区分', 'app.record.edit.change.水田耕起オペレーター区分',
                       'app.record.create.change.水田整地オペレーター区分', 'app.record.edit.change.水田整地オペレーター区分',
                       'app.record.create.change.田植えオペレーター区分', 'app.record.edit.change.田植えオペレーター区分',
                       'app.record.create.change.運搬員_出庫確認者区分', 'app.record.edit.change.運搬員_出庫確認者区分'
                    ], function(event) {
        var record = event.record;

        // 育苗オペレーター
        var genba = String(record['育苗オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('育苗オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('育苗オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('育苗オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('育苗オペレーター_農業公社_', false);
        }

        // 水田耕起オペレーター
        var genba = String(record['水田耕起オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('水田耕起オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('水田耕起オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('水田耕起オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('水田耕起オペレーター_農業公社_', false);
        }

        // 水田整地オペレーター
        var genba = String(record['水田整地オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('水田整地オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('水田整地オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('水田整地オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('水田整地オペレーター_農業公社_', false);
        }
        
         // 田植えオペレーター
        var genba = String(record['田植えオペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('田植えオペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('田植えオペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('田植えオペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('田植えオペレーター_農業公社_', false);
        }

        return event;

    });
    
    // ********************************************************************************************************************************
    // コール用関数・クラス
    // ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

