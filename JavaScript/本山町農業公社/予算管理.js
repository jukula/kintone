// **********************************************************************************
//
//  本山町農業公社  予算管理
//
//  履歴
//    2020.03.13    新規作成(Jun.K)
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){
    "use strict";

// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show','app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit','app.record.edit.submit'], function(event) {
        var record = event.record;
        // 残予算設定
        if(!record['消耗品残予算']['value']) {
            record['消耗品残予算']['value'] = record['消耗品年度予算']['value'];
        }
        if(!record['原材料残予算']['value']) {
            record['原材料残予算']['value'] = record['原材料年度予算']['value'];
        }
        if(!record['修繕残予算']['value']) {
            record['修繕残予算']['value'] = record['修繕年度予算']['value'];
        }
        if(!record['福利厚生残予算']['value']) {
            record['福利厚生残予算']['value'] = record['福利厚生年度予算']['value'];
        }
        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success','app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.購入部署', 'app.record.edit.change.購入部署'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

