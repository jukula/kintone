// **********************************************************************************
//
//  本山町農業公社  資材・消耗品購入申請
//
//  履歴
//    2020.01.29    新規作成(Jun.K)
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){

// ========================================================================================================
    // 一覧表示時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
        
        //一覧の要素を取得
        var elStatus = kintone.app.getFieldElements('ステータス'); //ステータスの情報を取得
        var elRequester = kintone.app.getFieldElements('申請者');//申請者
        var elDepartment = kintone.app.getFieldElements('部署名');//部署名
        var elPurchaser = kintone.app.getFieldElements('購入業者');//購入業者
        var elTotal = kintone.app.getFieldElements('合計金額');//合成金額
        var elTotalTax = kintone.app.getFieldElements('合計金額_税込_');//合計金額(税込)
        //var elProduct = ;//商品明細
        
        
        for (var i = 0; i < event.records.length; i++) {
          var record = event.records[i];
            
            switch(record['ステータス']['value']) {
              
              //ステータスが未完了の時(色変更なし)
//              case"未処理":
//                elStatus[i].parentNode.style.backgroundColor = '#FAAC58';
//                break;
              // 確認中の時
              case"確認中":
                elStatus[i].parentNode.style.backgroundColor = '#FAAC58';
                break;
              // 専務決済の時
              case"専務決済":
                elStatus[i].parentNode.style.backgroundColor = '#F0E68C';
                break;
              //ステータスが却下の時
              case"却下":
                elStatus[i].parentNode.style.backgroundColor = '#FA5858';
                break;
              //ステータスが完了の時
              case"完了":
                elStatus[i].parentNode.style.backgroundColor = '#81DAF5';
                break;
              // 予算増額申請の時
              case"予算増額申請":
                elStatus[i].parentNode.style.backgroundColor = '#66CDAA';
                break;
            }
        }
        return event;
    });
})();