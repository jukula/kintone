// **********************************************************************************
//
//  本山町農業公社
//
//  履歴
//    2019.09.24    新規作成(Jun.K)
//    2019.10.02    基本の計算処理追加、オペレーター表示・非表示制御
//    2020.01.15    数値０チェック追加、籾摺りのチェックボックスエラーチェック
//    2020.01.20    エラーチェック追加
//                                                  Copyright(C)2019-2020 Cross-Tier
// **********************************************************************************
(function(){

    // *********************************
    //  現場詳細計算
    // *********************************
    function calcGenba(record, price) {
        var s = 0;
        var unpan = 0;
        var kasan = 0;
        var pKikai = 0;
        var pChingin = 0;

        switch(price.作業種類.value) {
            case '草刈り':
            case '粉砕':
            case '運搬車使用':
                // 運搬車使用(チェックボックスがついていたら加算)
                if(record['別途機械使用状況']['value'].indexOf("運搬車使用") != -1) {
                    pKikai = Number(price.使用機械.value);
                }
                break;
            case '防除作業':
                // 作業面積をかける(小数点以下も使用する)
//                s = Math.ceil(record['作業面積']['value']);   // 切り上げ
                s = Number(record['作業面積']['value'] / 10);       // 10a単位
                pKikai   = Number(price.使用機械.value) * s;
                pChingin = Number(price.賃金.value) * s;
                break;
            case 'コンバイン作業':              // 別途機械使用状況にて「結束機使用」をチェックしているか？
            case 'コンバイン作業(結束機付)':
                // 作業面積をかける(10a単位)
                s = Number(record['作業面積']['value'] / 10);
                // 5a未満加算
                kasan = Number(record['_5a未満']['value']) * 1000;
                pKikai   = Number(price.使用機械.value) * s;
                pChingin = Number(price.賃金.value) * s + kasan;
                // 倒伏・湿田加算(3,000円 or 5,000円/10a)
                if(record['倒伏・湿田割増']['value'] == '30％超') {
                    pKikai += 1500 * s;
                    pChingin += 1500 * s;
                } else if(record['倒伏・湿田割増']['value'] == '50％超') {
                    pKikai += 2500 * s;
                    pChingin += 2500 * s;
                }
                break;
            case '生籾運搬':
                // 1石400円（基本料金）
                unpan = Number(record['公社運搬量']['value']) / 100; // 1石 = 100kg
                pChingin = Number(price.賃金.value) * unpan;
                break;
            case '農作業':
                // 1日8時間＋追加時間
                pChingin = Number(price.賃金.value) * (Number(record['作業員追加時間']['value']));
                break;
            case '遠距離農作業(～20km)':
            case '遠距離農作業(20km～)':
            case '遠距離農作業(30km～)':
                pKikai   = Number(price.使用機械.value) * Number(record['作業台数']['value']);
                pChingin = Number(price.賃金.value)     * Number(record['作業台数']['value']);
                break;
            }

        record['現場機械費用']['value'] = Number(record['現場機械費用']['value']) + pKikai;
        record['現場賃金']['value']     = Number(record['現場賃金']['value'])     + pChingin;
    }

    // *********************************
    //  乾燥１台ごと計算
    // *********************************
    function calcKanso(pKanso, pSuibun, price) {
        var priceK = 0;

        switch(price.作業種類.value) {
        case '乾燥':
            priceK = Number(price.使用機械.value);   // 基本料金

            // 700kgを越えている場合のみ加算
            if(pKanso > 700) {
                // 水分確認
                if(pSuibun >= 25) {
                    // 水分25%以上の場合、1石(100kg)あたり500円加算
                    priceK = priceK + ((pKanso - 700) / 100) * 500;
                } else if (pSuibun >= 20) {
                    // 水分20%以上の場合、1石(100kg)あたり400円加算
                    priceK = priceK + ((pKanso - 700) / 100) * 400;
                } else {
                    // 水分20%未満の場合、1石(100kg)あたり300円加算
                    priceK = priceK + ((pKanso - 700) / 100) * 300;
                }
            }
            break;
        case '半乾燥':
            if(pSuibun < 18) {
                priceK = Number(price.使用機械.value) * pKanso / 100;   // 1石(100kg)単位
            }
            break;
        }
console.log("乾燥：" + priceK);
        return priceK;
    }

    // *********************************
    //  籾摺り詳細計算
    // *********************************
    function calcMomisuri(record, price) {
        var pKikai = 0;
        var pChingin = 0;

console.log('籾摺_色選区分：' + record['籾摺_色選区分']['value']);
        // 籾摺／色選区分
        var chkBox = record['籾摺_色選区分']['value'];

        switch(price.作業種類.value) {
        case '籾出し':
            // 籾出し
            if( chkBox.indexOf("籾出し") != -1 ) {
                pKikai = Number(record['籾出し']['value']) * Number(price.使用機械.value);
            }
            break;
        case '籾摺り':
            // 籾摺（通常）
            if( chkBox.indexOf("籾摺（通常）") != -1 ) {
                pKikai = Number(record['籾摺り']['value']) * Number(price.使用機械.value) + Number(record['籾摺端数']['value']) * 10;
            }
            // 籾摺＋色選
            if( chkBox.indexOf("籾摺＋色選") != -1 ) {
                pKikai = Number(record['籾摺_色選']['value']) * Number(price.使用機械.value) + Number(record['籾摺_色選端数']['value']) * 10;
            }
            break;
        case '色彩選別':
            // 籾摺＋色選
            if( chkBox.indexOf("籾摺＋色選") != -1 ) {
                pKikai = Number(record['籾摺_色選']['value']) * Number(price.使用機械.value) + Number(record['籾摺_色選端数']['value']) * 10;
            }
            // 色選のみ
            if( chkBox.indexOf("色選のみ") != -1 ) {
                pKikai = Number(record['色選のみ']['value']) * (Number(price.使用機械.value) + 54);
            }
            break;
        case '精米':
            pKikai = Number(record['精米']['value']) * Number(price.使用機械.value) + Number(record['精米端数']['value']) * 17;
            break;
        case '食味分析器':
            pKikai = Number(record['食味分析器回数']['value']) * Number(price.使用機械.value);
            break;
        case '天空選別':
            // 土佐天空の郷（籾摺＋色選）
            if( chkBox.indexOf("土佐天空の郷（籾摺＋色選）") != -1 ) {
                pKikai = Number(record['土佐天空の郷']['value']) * Number(price.使用機械.value) + Number(record['土佐天空の郷端数']['value']) * 10;
            }
            break;
        }

        record['籾摺機械費用']['value'] = Number(record['籾摺機械費用']['value']) + pKikai + Number(record['袋代合計']['value']);
        record['籾摺賃金']['value']     = Number(record['籾摺賃金']['value'])     + pChingin;
    }



// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
//        record['送付先名']['disabled']   = false;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';

        //////////////////////////////////////////////////////////
        // 計算のために、0が設定されていない項目に0を設定する
        // 作業面積
        if(!record['作業面積']['value']) {
            record['作業面積']['value'] = 0;
        }
        // 5a未満
        if(!record['_5a未満']['value']) {
            record['_5a未満']['value'] = 0;
        }
        // 米検査料
        if(!record['米検査料']['value']) {
            record['米検査料']['value'] = 0;
        }
        // 乾燥作業
        if(!record['乾燥_1台目_']['value']) {
            record['乾燥_1台目_']['value'] = 0;
        }
        if(!record['乾燥_2台目_']['value']) {
            record['乾燥_2台目_']['value'] = 0;
        }
        if(!record['乾燥_3台目_']['value']) {
            record['乾燥_3台目_']['value'] = 0;
        }
        if(!record['乾燥_4台目_']['value']) {
            record['乾燥_4台目_']['value'] = 0;
        }
        if(!record['水分_1台目_']['value']) {
            record['水分_1台目_']['value'] = 0;
        }
        if(!record['水分_2台目_']['value']) {
            record['水分_2台目_']['value'] = 0;
        }
        if(!record['水分_3台目_']['value']) {
            record['水分_3台目_']['value'] = 0;
        }
        if(!record['水分_4台目_']['value']) {
            record['水分_4台目_']['value'] = 0;
        }
        // 生籾運搬
        if(!record['公社運搬量']['value']) {
            record['公社運搬量']['value'] = 0;
        }
        // 精米
        if(!record['精米']['value']) {
            record['精米']['value'] = 0;
        }
        if(!record['精米端数']['value']) {
            record['精米端数']['value'] = 0;
        }
        // 籾運搬
        if(!record['籾袋']['value']) {
            record['籾袋']['value'] = 0;
        }
        // 玄米運搬
        if(!record['玄米']['value']) {
            record['玄米']['value'] = 0;
        }
        // 農作業
        if(!record['作業員追加時間']['value']) {
            record['作業員追加時間']['value'] = 0;
        }
        // 遠距離農作業
        if(!record['作業台数']['value']) {
            record['作業台数']['value'] = 0;
        }
        //////////////////////////////////////////////////////////
console.log('委託者=' + record['委託者']['value']);

        if(typeof record['委託者']['value'] !== 'undefined') {
            search = record['委託者']['value'];
        }

        if((typeof record['現場名']['value'] !== 'undefined') && (record['現場名']['value'] != '')) {
            search = search + '(' + record['現場名']['value'] + ')';
        }

        if((typeof record['春作業種類']['value'] !== 'undefined') && (record['春作業種類']['value'] != '')) {
            search = search + '/' + record['春作業種類']['value'];
        }

        if(typeof record['秋作業種類']['value'] !== 'undefined') {
            if(search != '') {
                if((typeof record['春作業種類']['value'] !== 'undefined') && (record['春作業種類']['value'] != '')) {
                    search = search + ',';
                } else {
                    search = search + '/';
                }
            }
            record['カレンダー表示用']['value'] = search + record['秋作業種類']['value'];
        }

        // 現場作業費
        var recid;
        recid = 19;
        var target = record['秋作業種類']['value'] + ',運搬車使用';
console.log('target=' + target);
        // 文字列置換
        target = '\"' + String(target).replace(/,/g, '\",\"') + '\"';
        var body = {
            "app": 39,
            "query": "作業種類 in (" + target + ")", // + target + ")",
            "fields": ["レコード番号", "作業区分", "作業種類", "使用機械", "賃金", "基本料金"]
//            "id": recid
        }

        // 金額計算
        return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', body).then(function(resp) {
            if (resp.records[0] !== null) {
                // success
                console.log(resp);
            } else {
                event.error = '金額が取得できません。';
            }
            // 金額設定
            var i;
            var priceGenba = 0;
            var priceK = 0;
            record['現場機械費用']['value'] = 0;
            record['現場賃金']['value']     = 0;
//            record['乾燥費用']['value']     = 0;
            record['乾燥機械費用']['value'] = 0;
            record['乾燥賃金']['value']     = 0;
//            record['乾燥費用']['value']     = 0;
            record['籾摺機械費用']['value'] = 0;
            record['籾摺賃金']['value']     = 0;
//            record['籾摺費用']['value']     = 0;
            record['運搬機械費用']['value'] = 0;
            record['運搬賃金']['value']     = 0;
//            record['運搬費用']['value']     = 0;
            for(i=0; i<resp.records.length; i++) {
//console.log(resp.records[i].使用機械.value);
//console.log(record['機械費用']['value']);
                // 作業区分で合計する
                switch(resp.records[i].作業区分.value) {
                    case '現場':
                        calcGenba(record, resp.records[i]);
//                        record['現場機械費用']['value'] = Number(record['現場機械費用']['value']) + Number(resp.records[i].使用機械.value);
//                        record['現場賃金']['value']     = Number(record['現場賃金']['value'])     + Number(resp.records[i].賃金.value);
//                        record['現場費用']['value']     = Number(record['現場費用']['value'])     + Number(resp.records[i].基本料金.value);
                        break;
                    case '乾燥':
                        // 1台目
                        if((typeof record['乾燥_1台目_']['value'] !== 'undefined') && (record['乾燥_1台目_']['value'] != '')) {
                            priceK = calcKanso(Number(record['乾燥_1台目_']['value']), Number(record['水分_1台目_']['value']), resp.records[i]);
                        }
                        // 2台目
                        if((typeof record['乾燥_2台目_']['value'] !== 'undefined') && (record['乾燥_2台目_']['value'] != '')) {
                            priceK = priceK + calcKanso(Number(record['乾燥_2台目_']['value']), Number(record['水分_2台目_']['value']), resp.records[i]);
                        }
                        // 3台目
                        if((typeof record['乾燥_3台目_']['value'] !== 'undefined') && (record['乾燥_3台目_']['value'] != '')) {
                            priceK = priceK + calcKanso(Number(record['乾燥_3台目_']['value']), Number(record['水分_3台目_']['value']), resp.records[i]);
                        }
                        // 4台目
                        if((typeof record['乾燥_4台目_']['value'] !== 'undefined') && (record['乾燥_4台目_']['value'] != '')) {
                            priceK = priceK + calcKanso(Number(record['乾燥_4台目_']['value']), Number(record['水分_4台目_']['value']), resp.records[i]);
                        }

                        record['乾燥機械費用']['value'] = Number(record['乾燥機械費用']['value']) + priceK;
                        break;
                    case '籾摺':
                        calcMomisuri(record, resp.records[i]);
//                        record['籾摺機械費用']['value'] = Number(record['籾摺機械費用']['value']) + Number(resp.records[i].使用機械.value);
//                        record['籾摺賃金']['value']     = Number(record['籾摺賃金']['value'])     + Number(resp.records[i].賃金.value);
//                        record['籾摺費用']['value']     = Number(record['籾摺費用']['value'])     + Number(resp.records[i].基本料金.value);
                        break;
                    case '運搬':
                        // 玄米運搬
                        if(resp.records[i].作業種類.value == "玄米運搬") {
                            record['運搬賃金']['value'] = Number(record['運搬賃金']['value']) + Number(resp.records[i].賃金.value) * Number(record['玄米']['value']);
                        }
                        // 籾運搬
                        if(resp.records[i].作業種類.value == "籾運搬") {
                            record['運搬賃金']['value'] = Number(record['運搬賃金']['value']) + Number(resp.records[i].賃金.value) * Number(record['籾袋']['value']);
                        }
//                        record['運搬機械費用']['value'] = Number(record['運搬機械費用']['value']) + Number(resp.records[i].使用機械.value);
//                        record['運搬賃金']['value']     = Number(record['運搬賃金']['value'])     + Number(resp.records[i].賃金.value);
//                        record['運搬費用']['value']     = Number(record['運搬費用']['value'])     + Number(resp.records[i].基本料金.value);
                        break;
                }
			}

console.log('現場作業進行状況=' + record['現場作業進行状況']['value']);
console.log('全体進捗状況=' + record['全体進捗状況']['value']);
            // 全体進行状況
            if((typeof record['稲刈り日']['value'] !== 'undefined') && (record['稲刈り日']['value'] != '')) {
                if( ((record['現場作業進行状況']['value'] == '実施済') || (record['現場作業進行状況']['value'] == '刈取り無し'))
                        && (record['全体進捗状況']['value'] == '未実施') ) {
                    record['作業予約日']['value'] = record['稲刈り日']['value'];
                    record['全体進捗状況']['value'] = '乾燥';
                }
            }
            // 乾燥
            if((typeof record['乾燥日']['value'] !== 'undefined') && (record['乾燥日']['value'] != '')) {
                if(record['乾燥作業進行状況']['value'] == '実施済') {
                    // 作業予約日の更新
                    if (record['現場作業進行状況']['value'] == '刈取り無し') {
                        record['作業予約日']['value'] = record['乾燥日']['value'];
                    }
                    // 全体進行状況の更新
                    record['全体進捗状況']['value'] = '籾摺';
                }
            }
            if(record['乾燥作業進行状況']['value'] == '乾燥無し') {
                // 全体進行状況の更新
                record['全体進捗状況']['value'] = '籾摺';
            }
            // 籾摺
            if((typeof record['籾摺_色選日']['value'] !== 'undefined') && (record['籾摺_色選日']['value'] != '')) {
                if( record['籾摺作業進行状況']['value'] == '実施済' ) {
                    // 作業予約日の更新
                    if( (record['現場作業進行状況']['value'] == '刈取り無し') && (record['乾燥作業進行状況']['value'] == '乾燥無し') ) {
                        record['作業予約日']['value'] = record['籾摺_色選日']['value'];
                    } 
                    // 全体進行状況の更新
                    record['全体進捗状況']['value'] = '運搬';
                    // 出庫予定日の更新
                    record['出庫予定日']['value'] = record['籾摺_色選日']['value'];
                }
            }
            if(record['籾摺作業進行状況']['value'] == '籾摺無し(籾だし)') {
                // 全体進行状況の更新
                record['全体進捗状況']['value'] = '運搬';
            }
    
            // 運搬
            if((record['運搬作業進行状況']['value'] == '運搬なし') || (record['運搬作業進行状況']['value'] == '実施済')) {
                // 全体進行状況の更新
                record['全体進捗状況']['value'] = '完了';
            }
            if((typeof record['運搬_出庫日']['value'] !== 'undefined') && (record['運搬_出庫日']['value'] != '')) {
                // 出庫予定日の更新
                record['出庫予定日']['value'] = record['運搬_出庫日']['value'];
            }
            
            // 秋作業の複数選択チェック
            if(record['別途機械使用状況']['value'].indexOf('結束機使用') != -1) {
                // コンバイン作業(結束機付)をチェックしないといけない！
//                record['秋作業種類']['value'] = record['秋作業種類']['value'].push('コンバイン作業(結束機付)');
            }

            return event;
        }, function(resp) {
            console.log('料金が取得できません。');
            return event;        
        
        });
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 印刷画面表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.print.show'], function(event) {
        var record = event.record;

        // 各グループをオープン
        kintone.app.record.setGroupFieldOpen('現場情報', true);
        kintone.app.record.setGroupFieldOpen('乾燥作業', true);
        kintone.app.record.setGroupFieldOpen('籾摺・色選作業', true);
        kintone.app.record.setGroupFieldOpen('運搬作業', true);

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // 各オペレーター
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.現場オペレーター区分', 'app.record.edit.change.現場オペレーター区分',
                       'app.record.create.change.乾燥オペレーター区分', 'app.record.edit.change.乾燥オペレーター区分',
                       'app.record.create.change.籾摺_色彩選別オペレーター区分', 'app.record.edit.change.籾摺_色彩選別オペレーター区分',
                       'app.record.create.change.運搬員_出庫確認者区分', 'app.record.edit.change.運搬員_出庫確認者区分'
                    ], function(event) {
        var record = event.record;

        // 現場オペレーター
        var genba = String(record['現場オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('現場オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('現場オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('現場オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('現場オペレーター_農業公社_', false);
        }

        // 乾燥オペレーター
        var genba = String(record['乾燥オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('乾燥オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('乾燥オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('乾燥オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('乾燥オペレーター_農業公社_', false);
        }

        // 籾摺_色彩選別オペレーター
        var genba = String(record['籾摺_色彩選別オペレーター区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('籾摺_色彩選別オペレーター_農家_', true);
        } else {
            kintone.app.record.setFieldShown('籾摺_色彩選別オペレーター_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('籾摺_色彩選別オペレーター_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('籾摺_色彩選別オペレーター_農業公社_', false);
        }

        // 運搬員_出庫確認者
        var genba = String(record['運搬員_出庫確認者区分']['value']);
        if(genba.indexOf('農家') !== -1) {
            kintone.app.record.setFieldShown('運搬員_出庫確認者_農家_', true);
        } else {
            kintone.app.record.setFieldShown('運搬員_出庫確認者_農家_', false);
        }
        if(genba.indexOf('農業公社') !== -1) {
            kintone.app.record.setFieldShown('運搬員_出庫確認者_農業公社_', true);
        } else {
            kintone.app.record.setFieldShown('運搬員_出庫確認者_農業公社_', false);
        }

        return event;

    });
    
    // 項目チェック
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.籾出し','app.record.edit.change.籾出し',
                       'app.record.create.change.色選のみ','app.record.edit.change.色選のみ',
                       'app.record.create.change.籾摺り','app.record.edit.change.籾摺り',
                       'app.record.create.change.籾摺端数','app.record.edit.change.籾摺端数',
                       'app.record.create.change.籾摺_色選','app.record.edit.change.籾摺_色選',
                       'app.record.create.change.籾摺_色選端数','app.record.edit.change.籾摺_色選端数',
                       'app.record.create.change.精米','app.record.edit.change.精米',
                       'app.record.create.change.食味分析器回数','app.record.edit.change.食味分析器回数',
                       'app.record.create.change.土佐天空の郷','app.record.edit.change.土佐天空の郷',
                       'app.record.create.change.土佐天空の郷端数','app.record.edit.change.土佐天空の郷端数'
                        ], function(event) {
        var record = event.record;
        var lst = record['秋作業種類']['value']
        var chk = record['籾摺_色選区分']['value'];
        var lstmsg = "";
        var chkmsg = "";
        var errormsg = "";
        
        //////////////////////////
        //  色彩選別
        //////////////////////////
        // 籾出し
        if(record['籾出し']['value']) {
            // 籾出しのIndex検索
            if(chk.indexOf('籾出し') == -1) {
                errormsg += "「籾摺／色選　区分」の籾出しをチェックして下さい\n";
                chkmsg += "籾出しをチェックする必要があります\n";
            }
            if(lst.indexOf('籾出し') == -1) {
                errormsg += "「秋作業種類」の籾出しを選択して下さい\n";
                lstmsg += "籾出しを選択する必要があります\n";
            }
        }
        
        // 色選のみ
        if(Number(record['色選のみ']['value']) > 0) {
            if(chk.indexOf('色選のみ') == -1) {
                errormsg += "「籾摺／色選　区分」の色選のみをチェックして下さい\n";
                chkmsg += "色選のみをチェックする必要があります\n";
            }
            if(lst.indexOf('色彩選別') == -1) {
                errormsg += "「秋作業種類」の色彩選別を選択して下さい\n";
                lstmsg += "色彩選別を選択する必要があります\n";
            }
            kintone.app.record.setFieldShown('色選後の袋数', true);
            kintone.app.record.setFieldShown('色選後の端数', true);
        }else{
            kintone.app.record.setFieldShown('色選後の袋数', false);
            kintone.app.record.setFieldShown('色選後の端数', false);
        }

        // 通常籾摺り
        if((Number(record['籾摺り']['value']) > 0) || (Number(record['籾摺端数']['value']) > 0)) {
            if(chk.indexOf('籾摺（通常）') == -1) {
                errormsg += "「籾摺／色選　区分」の籾摺（通常）をチェックして下さい\n";
                chkmsg += "籾摺（通常）をチェックする必要があります\n";
            }
            if(lst.indexOf('籾摺り') == -1) {
                errormsg += "「秋作業種類」の籾摺りを選択して下さい\n";
                lstmsg += "籾摺りを選択する必要があります\n";
            }
        }

        // 籾摺＋色選
        if((Number(record['籾摺_色選']['value']) > 0) || (Number(record['籾摺_色選端数']['value']) > 0)) {
            if(chk.indexOf('籾摺＋色選') == -1) {
                errormsg += "「籾摺／色選　区分」の籾摺＋色選をチェックして下さい\n";
                chkmsg += "籾摺＋色選をチェックする必要があります\n";
            }
            // 秋作業は、籾摺りと色彩選別の両方をチェックする必要あり
            if(lst.indexOf('籾摺り') == -1) {
                errormsg += "「秋作業種類」の籾摺りを選択して下さい\n";
                lstmsg += "籾摺りを選択する必要があります\n";
            }
            if(lst.indexOf('色彩選別') == -1) {
                errormsg += "「秋作業種類」の色彩選別を選択して下さい\n";
                lstmsg += "色彩選別を選択する必要があります\n";
            }
        }

        // 天空の郷
        if((Number(record['土佐天空の郷']['value']) > 0) || (Number(record['土佐天空の郷端数']['value']) > 0)) {
            if(chk.indexOf('土佐天空の郷（籾摺＋色選）') == -1) {
                errormsg += "「籾摺／色選　区分」の土佐天空の郷をチェックして下さい\n";
                chkmsg += "土佐天空の郷をチェックする必要があります\n";
            }
            if(lst.indexOf('天空選別') == -1) {
                errormsg += "「秋作業種類」の天空選別を選択して下さい\n";
                lstmsg += "天空選別を選択する必要があります\n";
            }
        }

        // 精米
        if((Number(record['精米']['value']) > 0) || (Number(record['精米端数']['value']) > 0)) {
            if(lst.indexOf('精米') == -1) {
                errormsg += "「秋作業種類」の精米を選択して下さい\n";
                lstmsg += "精米を選択する必要があります\n";
            }
        }

        // 食味分析器
        if(Number(record['食味分析器回数']['value']) > 0) {
            if(lst.indexOf('食味分析器') == -1) {
                errormsg += "「秋作業種類」の食味分析器を選択して下さい\n";
                lstmsg += "食味分析器を選択する必要があります\n";
            }
        }

        if(lstmsg != "")  record['秋作業種類']['error'] = lstmsg;
        if(chkmsg != "")  record['籾摺_色選区分']['error'] = chkmsg;
        if(errormsg != "")  event.error = errormsg;

        return event;
     });


// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

