﻿// **********************************************************************************
//
//  本山町農業公社  PCA連携
//
//  履歴
//
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************

/// <reference path="../reference/index.js" />
/// <reference path="../reference/edit.js" />
/// <reference path="../reference/moment.min.js" />
/// <reference path="plugin_desktop.js" />

jQuery(function ($) {

    "use strict";

    kintone.events.on('app.record.index.show', function (event) {
        /// <summary>
        /// レコード一覧画面表示後イベント
        /// </summary>
        updateRecordHeaderSpace();
        return event;
    });

    function updateRecordHeaderSpace() {
        /// <summary>
        /// レコード一覧画面のヘッダースペースの内容を更新します。
        /// </summary>
        var h = kintone.app.getHeaderMenuSpaceElement();
        while (h.firstChild)
            h.removeChild(h.firstChild);

        // ログインボタン
        $("<button />", { text: "PCAクラウド Web-API へログイン",
            click: function () {
                pca.webapi.login()
                .then(function () {
                    return pca.webapi.findDataArea();   // 使用可能なデータ領域を取得
                }).then(function (result) {
                    localStorage.dataAreas = JSON.stringify(result);
                    return pca.webapi.getLogOnUser();   // ログイン中のユーザー情報を取得
                }).then(function (result) {
                    localStorage.logOnUser = JSON.stringify(result);
                    updateRecordHeaderSpace();
                });
            }
        }).appendTo(h);

        if (!pca.webapi.hasAccess())
            return;

        var h = kintone.app.getHeaderSpaceElement();
        while (h.firstChild)
            h.removeChild(h.firstChild);

        var div = $("<div />", { style: "display: inline-block; margin: 0px 16px 16px 16px;" }).appendTo(h);
        var user = JSON.parse(localStorage.logOnUser);
        $("<b>", { text: "現在のユーザー: " + user.Name, style: "font-size: large; color: green;" }).appendTo(div);
        $("<b>", { text: "データ領域:", style: "margin: 0px 10px;" }).appendTo(div);
        var select = $("<select/>", { change: onAreaChange }).appendTo(div);

        $("<option/>", { text: "領域を選択して下さい", value: "", selected: (pca.webapi.dataarea_name == "") }).appendTo(select);
        if (localStorage.dataAreas) {
            $.each(JSON.parse(localStorage.dataAreas), function (i, a) {
                $("<option/>", { value: a.Name, text: a.CompanyCode + " " + a.CompanyName, selected: (pca.webapi.dataarea_name == a.Name) }).appendTo(select);
            });
        }

        // 部門選択 select
        $("<b>", { text: "店舗:", style: "margin: 0px 10px;" }).appendTo(div);
        var select = $("<select/>", {
            change: function () {
                localStorage.bumonCode = this.value;
                localStorage.bumonName = this[this.selectedIndex].text;
            }
        }).appendTo(div);

        $("<option/>", { text: "部門を選択して下さい", value: "", selected: (pca.webapi.dataarea_name == "") }).appendTo(select);
        if (localStorage.bumons) {
            $.each(JSON.parse(localStorage.bumons), function (i, b) {
                $("<option/>", { value: b.BumonCode, text: b.BumonCode + " " + b.BumonMei, selected: (localStorage.bumonCode == b.BumonCode) }).appendTo(select);
            });
        }

        // 得意先選択 select
        $("<b>", { text: "得意先:", style: "margin: 0px 10px;" }).appendTo(div);
        var select = $("<select/>", {
            change: function () {
                localStorage.TokuisakiCode = this.value;
                localStorage.TokuisakiName = this[this.selectedIndex].text;
            }
        }).appendTo(div);

        $("<option/>", { text: "-得意先-", value: "", selected: (pca.webapi.dataarea_name == "") }).appendTo(select);
        if (localStorage.tms) {
            $.each(JSON.parse(localStorage.tms), function (i, b) {
                $("<option/>", { value: b.TokuisakiCode, text: b.TokuisakiCode + " " + b.TokuisakiMei, selected: (localStorage.TokuisakiCode == b.TokuisakiCode) }).appendTo(select);
            });
        }
    }

    function onAreaChange() {
        /// <summary>
        /// データ領域の選択イベント
        /// </summary>
        localStorage.companyName = localStorage.bumons = localStorage.bumonCode = localStorage.bumonName = localStorage.smss = localStorage.tms = "";

        var dataAreaName = this.value;
        var companyName = this[this.selectedIndex].text;

        pca.webapi.selectDataArea(dataAreaName)
        .then(function () {
            localStorage.companyName = companyName;

            return kintone.Promise.all([
                // 部門マスター (店舗)
                pca.webapi.send("Find/MasterBumon", "GET", {}, {})
                .then(function (args) { // [b, status, h]
                    var res = JSON.parse(args[0]);
                    if (args[1] != 200) {
                        return kintone.Promise.reject(res.Message);
                    }
                    var bumons = JSON.parse(args[0]).ArrayOfBEMasterBumon.BEMasterBumon;
                    localStorage.bumons = JSON.stringify(bumons);
                    return kintone.Promise.resolve();
                }),
                // 商品マスター
                pca.webapi.send("Find/MasterSms?Limit=20", "GET", {}, {})
                .then(function (args) { // [b, status, h]
                    var res = JSON.parse(args[0]);
                    if (args[1] != 200) {
                        return kintone.Promise.reject(res.Message);
                    }
                    var smss = JSON.parse(args[0]).ArrayOfBEMasterSms.BEMasterSms;
                    localStorage.smss = smss ? JSON.stringify(smss) : "";
                    return kintone.Promise.resolve();
                }),
                // 得意先マスター (最初の1件を取得)
                pca.webapi.send("Find/MasterTms?Limit=1", "GET", {}, {})
                .then(function (args) { // [b, status, h]
                    var res = JSON.parse(args[0]);
                    if (args[1] != 200) {
                        return kintone.Promise.reject(res.Message);
                    }
                    var tmss = JSON.parse(args[0]).ArrayOfBEMasterTms.BEMasterTms;
                    localStorage.tms = (tmss && tmss.length > 0) ? JSON.stringify(tmss[0]) : '';
                    return kintone.Promise.resolve();
                })
            ])
        })
        .then(function (args) {
            updateRecordHeaderSpace();
        })
        .catch(function (reason) {
            console.log(reason);
            updateRecordHeaderSpace();
        });
    }

/*
    kintone.events.on('app.record.create.show', function (event) {
        /// <summary>
        /// レコード追加画面表示時イベント
        /// </summary>
        var record = event.record;
        var table = record.明細;
        var rows = [];

        if (!event.reuse) {
            // 新規追加

            // 部門
            record.店舗コード.value = localStorage.bumonCode;
            record.店舗名.value = localStorage.bumonName;

            // 商品マスターをテーブルに追加
            if (localStorage.smss) {
                $.each(JSON.parse(localStorage.smss), function (i, sms) {
                    rows.push({
                        "value": {
                            "商品コード": { "type": "SINGLE_LINE_TEXT", "value": sms.SyohinCode },
                            "商品名": { "type": "SINGLE_LINE_TEXT", "value": sms.SyohinMei },
                            "単価": { "type": "NUMBER", "value": parseInt(sms.HyojunKakaku) },
                            "数量": { "type": "NUMBER", "value": "0" },
                            "金額": { "type": "CALC", "value": "単価 * 数量" }
                        }
                    });
                });
            }

        } else {
            // レコードの再利用
            record.伝票日付.value = moment(new Date()).format('YYYY-MM-DD');
            record.伝票No.value = null;

            // 数量をクリア
            rows = table.value;
            $.each(rows, function (i, row) {
                row.value.数量.value = "0";
            });
        }
        table.value = rows;
        return event;
    });

    kintone.events.on('app.record.create.show', function (event) {
        /// <summary>
        /// レコード追加画面表示時イベント
        /// </summary>
        if (!pca.webapi.hasAccess()) {
            event.error = 'PCAクラウド Web-APIにログインしてから実行してください。';
            return event;
        }

        event.record.伝票No.disabled = true;
        event.record.データ領域.disabled = true;
        event.record.ユーザー名.disabled = true;
        event.record.店舗コード.disabled = true;
        event.record.店舗名.disabled = true;
        event.record.データ領域.value = localStorage.companyName;

        if (localStorage.logOnUser) {
            var user = JSON.parse(localStorage.logOnUser);
            event.record.ユーザー名.value = user.Name;
        }

        if (!event.record.データ領域.value) {
            event.error = "データ領域を選択して下さい。";
            return event;
        }
        if (!event.record.店舗コード.value) {
            event.error = "店舗を選択して下さい。";
            return event;
        }

        return event;
    });

    kintone.events.on("app.record.create.submit", function (event) {
        /// <summary>
        /// レコード追加保存実行前イベント
        /// </summary>

        // 売上伝票エンティティJSON作成
        var record = event.record;
        var tms = (localStorage.tms) ? JSON.parse(localStorage.tms) : null;
        if (!tms) {
            event.error = "得意先マスターが登録されていません。";
            return event;
        }
        var entity = createInpuSYK(record, tms.TokuisakiCode);
        if (entity == null) {
            event.error = "数量を入力してください。";
            return event;
        }

        // PCA WebAPI で売上伝票を登録
        return pca.webapi.send("Create/InputSYK?CalcTotal=True&CalcTax=True&CalcDetailTax=True", "POST", {}, JSON.stringify(entity))
        .then(function (args) { // [b, status, h]
            var res = JSON.parse(args[0]);
            if (args[1] != 200) {
                var message = (res.BusinessValue) ? res.BusinessValue.BEKonIntegrationResultOfBEInputSYK.ErrorMessage : res.Message;
                return kintone.Promise.reject(message);
            }
            // 結果から伝票番号を取得してレコードにセット
            var denpNo = res.BEKonIntegrationResultOfBEInputSYK.Target.InputSYKH.DenpyoNo;
            record.伝票No.value = denpNo;

            return event;
        })
        .catch(function (error) {
            event.error = error;
            return event;
        });
    });
*/
    function createInpuSYK(record, tokuisakiCode) {
        /// <summary>
        /// 画面に入力された内容から売上伝票(InputSYK)の登録用JSON文字列を作成します。
        /// 金額が入った明細がない場合は null を返します。
        /// </summary>
        /// <param name="record" type="">kintoneレコード</param>
        /// <param name="tokuisakiCode" type="String">得意先コード</param>
        var date = moment(record.伝票日付.value);   // 伝票日付
        var bmnCode = record.店舗コード.value;      // 部門コード

        // 売上伝票
        var inputSYK = {
            "BEInputSYK": {
                /***upd***top 2020/01/23 */
                "BEVersion": "401",                         // 1. エンティティバージョン DXのRev3.00以降の場合800
                /***upd***bottom 2020/01/23 */
                "InputSYKH": {                              // 売上伝票ヘッダー
                    "Denku": "1",                           // 102. 伝区
                    "Uriagebi": date.format('YYYYMMDD'),    // 103. 売上日
                    "Seikyubi": date.format('YYYYMMDD'),    // 104. 請求日 - 売上日と同じ
                    "TokuisakiCode": tokuisakiCode,         // 106. 得意先コード
                    "SyohizeiTsuchi": "2",                  // 123. 得意先 消費税通知
                    "BumonCode": bmnCode,                   // 133. 部門コード
                },
                "InputSYKDList": function () {              // 売上伝票データ(明細)
                    var array = [];
                    var table = record.明細;
                    var rows = table.value;
                    $.each(rows, function (i, row) {
                        var value = row.value;
                        if (value.数量.value != 0) {
                            array.push({
                                "BEInputSYKD": {
                                    "SyohinCode": value.商品コード.value,             // 204. 商品コード
                                    "ZeiKubun": "2",                                 // 207. 税区分
                                    "ZeikomiKubun": "1",                             // 208. 税込区分
                                    "SyohinMei": value.商品名.value,                 // 211. 商品名
                                    "Suryo": value.数量.value,                       // 220. 数量
                                    "Tanka": value.単価.value,                       // 222. 単価
                                    "Kingaku": value.金額.value,                     // 225. 金額
                                    "ZeiRitsu": "8.0",                              // 234. 税率
                                    /***upd***top 2020/01/23 */
                                    "UriageZeiSyubetsu": "1",                       // 267. 売上税種別
                                    /***upd***bottom 2020/01/23 */
                                }
                            });
                        }
                    });
                    return array;
                }()
            }
        }

        var res = JSON.parse(JSON.stringify(inputSYK));
        if (res.BEInputSYK.InputSYKDList.length == 0)   // 数量の入力された明細がなければ null を返す
            return null;
        return res;
    }
});