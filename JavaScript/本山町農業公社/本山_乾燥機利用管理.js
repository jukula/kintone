// **********************************************************************************
//
//  本山町農業公社  乾燥機利用管理
//
//  履歴
//    2019.10.02    新規作成(Jun.K)
//    2019.10.07    終了日に利用日(開始日)をコピー
//                                                        Copyright(C)2019 Cross-Tier
// **********************************************************************************
(function(){


// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目自動入力
        record['委託者']['lookup']   = true;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';

console.log('委託者=' + record['委託者']['value']);

        if(typeof record['利用乾燥機']['value'] !== 'undefined') {
            search = record['利用乾燥機']['value'];
        }

        if((typeof record['委託者']['value'] !== 'undefined') && (record['委託者']['value'] != '')) {
            search = search + '／' + record['委託者']['value'];
        }

        record['カレンダー表示用']['value'] = search;

        // 終了日を開始日と同じにする
        record['終了日']['value'] = record['利用日']['value'];

        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.フィールド名', 'app.record.edit.change.フィールド名'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

