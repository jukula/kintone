// **********************************************************************************
//
//  本山町農業公社  資材・消耗品購入申請
//
//  履歴
//    2020.01.29    新規作成(Jun.K)
//    2020.03.03    申請時に予算減算、却下時点で予算増
//    2020.03.13    年度対応
//    2020.04.12    開発・本番切替対応
//    2020.04.30    完了で予算減、却下で予算増
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){
    "use strict";

// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    const App = {
        // 開発
        59: {
                yosanAppId: 60,     // 予算管理アプリ
            },
        // 本番
        58: {
                yosanAppId: 61      // 予算管理アプリ
            },
    }


    var zanYosan = 0;

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show','app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        var nStatus = event.nextStatus.value;
        var nendo = moment(record['申請日']['value']).format('YYYY');
console.log(nendo);
        var appId = kintone.app.getId();
        var paramGet = {
            "app": App[appId].yosanAppId,
            "id": record['予算管理レコード番号']['value']
        }
        switch(nStatus) {
            case "完了":
                return kintone.api(kintone.api.url('/k/v1/record', true), 'GET', paramGet).then(function(resp) {
                    // 予算残高更新
console.log(resp);
                    var rec = resp.record;
                    var zanYosan1 = Number(rec["消耗品残予算"]["value"]);    // 消耗品
                    if(isNaN(zanYosan1)) zanYosan1 = Number(rec["消耗品年度予算"]["value"]);
                    var zanYosan2 = Number(rec["原材料残予算"]["value"]);      // 原材料
                    if(isNaN(zanYosan2)) zanYosan2 = Number(rec["原材料年度予算"]["value"]);
                    var zanYosan3 = Number(rec["修繕残予算"]["value"]);      // 修繕
                    if(isNaN(zanYosan3)) zanYosan3 = Number(rec["修繕年度予算"]["value"]);
                    var zanYosan4 = Number(rec["福利厚生残予算"]["value"]);      // 福利厚生
                    if(isNaN(zanYosan4)) zanYosan4 = Number(rec["福利厚生年度予算"]["value"]);
                    switch(record["区分"]["value"]) {
                        case "消耗品":
                            zanYosan1 = zanYosan1 - Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "原材料":
                            zanYosan2 = zanYosan2 - Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "修繕":
                            zanYosan3 = zanYosan3 - Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "福利厚生":
                            zanYosan4 = zanYosan4 - Number(record["合計金額_税込_"]["value"]);
                            break;
                    }

                    var paramPut = {
                        "app": App[appId].yosanAppId,
                        'id': rec['$id']['value'],
                        'record': {
                            '消耗品残予算': {"value": zanYosan1},
                            '原材料残予算': {"value": zanYosan2},
                            '修繕残予算': {"value": zanYosan3},
                            '福利厚生残予算': {"value": zanYosan4}
                            }
                        };
                    return kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', paramPut);
                }).then(function(resp2) {
                    // 処理成功
                    return event;
                }).catch(function(error) {
                    // error
                    alert('残予算の更新でエラーが発生しました。\n' + error.message);
                    return event;
                });
            case "却下":
                return kintone.api(kintone.api.url('/k/v1/record', true), 'GET', paramGet).then(function(resp) {
                    // 予算残高更新
console.log(resp);
                    var rec = resp.record;
                    var zanYosan1 = Number(rec["消耗品残予算"]["value"]);    // 消耗品
                    if(isNaN(zanYosan1)) zanYosan1 = Number(rec["消耗品年度予算"]["value"]);
                    var zanYosan2 = Number(rec["原材料残予算"]["value"]);      // 原材料
                    if(isNaN(zanYosan2)) zanYosan2 = Number(rec["原材料年度予算"]["value"]);
                    var zanYosan3 = Number(rec["修繕残予算"]["value"]);      // 修繕
                    if(isNaN(zanYosan3)) zanYosan3 = Number(rec["修繕年度予算"]["value"]);
                    var zanYosan4 = Number(rec["福利厚生残予算"]["value"]);      // 福利厚生
                    if(isNaN(zanYosan4)) zanYosan4 = Number(rec["福利厚生年度予算"]["value"]);
                    switch(record["区分"]["value"]) {
                        case "消耗品":
                            zanYosan1 = zanYosan1 + Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "原材料":
                            zanYosan2 = zanYosan2 + Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "修繕":
                            zanYosan3 = zanYosan3 + Number(record["合計金額_税込_"]["value"]);
                            break;
                        case "福利厚生":
                            zanYosan4 = zanYosan4 + Number(record["合計金額_税込_"]["value"]);
                            break;
                    }

                    var paramPut = {
                        "app": App[appId].yosanAppId,
                        'id': rec['$id']['value'],
                        'record': {
                            '消耗品残予算': {"value": zanYosan1},
                            '原材料残予算': {"value": zanYosan2},
                            '修繕残予算': {"value": zanYosan3},
                            '福利厚生残予算': {"value": zanYosan4}
                            }
                        };
                    return kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', paramPut);
                }).then(function(resp2) {
                    // 処理成功
                    return event;
                }).catch(function(error) {
                    // error
                    alert('残予算の更新でエラーが発生しました。\n' + error.message);
                    return event;
                });
    
        }

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.edit.submit'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success'], function(event) {
        var record = event.record;
/*  プロセス管理の完了と却下の時点で更新
        var zanYosan1 = Number(record["申請時残予算_消耗品_"]["value"]);    // 消耗品
        if(isNaN(zanYosan1)) zanYosan1 = Number(record["年間予算_消耗品_"]["value"]);
        var zanYosan2 = Number(record["申請時残予算_原材料_"]["value"]);      // 原材料
        if(isNaN(zanYosan2)) zanYosan2 = Number(record["年間予算_原材料_"]["value"]);
        var zanYosan3 = Number(record["申請時残予算_修繕_"]["value"]);      // 修繕
        if(isNaN(zanYosan3)) zanYosan3 = Number(record["年間予算_修繕_"]["value"]);
        var zanYosan4 = Number(record["申請時残予算_福利厚生_"]["value"]);      // 福利厚生
        if(isNaN(zanYosan4)) zanYosan4 = Number(record["年間予算_福利厚生_"]["value"]);
        switch(record["区分"]["value"]) {
            case "消耗品":
                zanYosan1 = zanYosan1 - Number(record["合計金額_税込_"]["value"]);
                break;
            case "原材料":
                zanYosan2 = zanYosan2 - Number(record["合計金額_税込_"]["value"]);
                break;
             case "修繕":
                zanYosan3 = zanYosan3 - Number(record["合計金額_税込_"]["value"]);
                break;
            case "福利厚生":
                zanYosan4 = zanYosan4 - Number(record["合計金額_税込_"]["value"]);
                break;
        }

console.log('zan1=' + zanYosan1 + ' zan2=' + zanYosan2 + ' zan3=' + zanYosan3 + ' zan4=' + zanYosan4);

        var appId = kintone.app.getId();

console.log(appId);
console.log(App[appId]);
console.log(App[appId].yosanAppId);

        // 残予算更新
        var body = {
            'app': App[appId].yosanAppId,
            'id': record['予算管理レコード番号']['value'],
            'record': {
                '消耗品残予算': {"value": zanYosan1},
                '原材料残予算': {"value": zanYosan2},
                '修繕残予算': {"value": zanYosan3},
                '福利厚生残予算': {"value": zanYosan4}
            }
        };

        console.log(body);

        return kintone.api('/k/v1/record', 'PUT', body)
    }).then (function(resp) {
        // 成功
        console.log(resp);
        return event;
    }).catch (function(error) {
        // エラー表示をする
        alert('残予算の更新でエラーが発生しました。\n' + error.message);
        event.error = '更新エラー';
*/
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.edit.submit.success'], function(event) {
        var record = event.record;
        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.購入部署', 'app.record.edit.change.購入部署'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

