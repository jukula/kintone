// **********************************************************************************
//
//  ワールドフォースインターナショナル
//
//  履歴
//    2019.08.22    新規作成(Jun.K)
//    2019.09.03    接続文字列を"_"から"-"に変更
//                                                        Copyright(C)2019 Cross-Tier
// **********************************************************************************
(function(){


// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
//        record['送付先名']['disabled']   = false;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';

console.log(record['代理店ID']['value']);

        if(typeof record['代理店ID']['value'] === 'undefined') {
            search = '-';
        } else {
            search = record['代理店ID']['value'] + '-';
        }

        if(typeof record['代理店名']['value'] === 'undefined') {
            search = search + '-';
        } else {
            search = search + record['代理店名']['value'] + '-';
        }

        if(typeof record['担当']['value'] === 'undefined') {
            record['代理店ID_代理店名_担当']['value'] = search;
        } else {
            record['代理店ID_代理店名_担当']['value'] = search + record['担当']['value'];
        }

        //  REST API サンプルコード
        // ************************** １件取得 **********************
        var body = {
            "app": 25,
            "id": 17074
        };
        
        kintone.api(kintone.api.url('/k/v1/record', true), 'GET', body, function(resp) {
            // success
            console.log(resp);
            
        }, function(error) {
            // error
            console.log(error);
        });


        // ************************** 複数取得(条件指定) **********************
        var body = {
            "app": 25,
            "query": "名前 like \"竹田\"",
            "fields": ["名前", "ふりがな", "エリア"]
        };
        kintone.api(kintone.api.url('/k/v1/records', true), 'GET', body, function(resp) {
            // success
            console.log(resp);
        }, function(error) {
            // error
            console.log(error);
        });
        
        //  kintone API サンプルここまで


        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.フィールド名', 'app.record.edit.change.フィールド名'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

