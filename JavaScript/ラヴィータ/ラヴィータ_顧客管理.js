// **********************************************************************************
//
//  ラヴィータ  顧客管理
//
//  履歴
//    2020.05.15    新規作成(Jun.K)
//                                                   Copyright(C)2020 Caminova, Inc.
// **********************************************************************************
(function(){
    "use strict";
	// 生年月日が入っているフィールドコード名
	const BIRTHDAY_FIELD_NAME = '生年月日';

	// 現在の年齢計算する関数
	function getYearMonth(arg_yyyy_mm_dd) {

		//本日の日付を取得
		var today=new Date();
		
		// 肝心の年齢計算について
		// 年月日の日は、そのまま引き算。
		// 年月日の月は、２ケタ左シフトして(100倍にする。1月は0なので+100からスタート)から、引き算。
		// 年月日の年は、４ケタ左シフトして(１万倍にする)から、引き算。
		// この計算方法の優れた所は、年・月・日を単独で計算して、足らなかったら上位からマイナスするという点！
		// 単純ながら、確実な年齢計算が出来る優れたアルゴリズムです！
		// ちなみに日数計算はできません。あくまで年数計算のみ
		// 参照URL：http://d.hatena.ne.jp/toku-hiro/20070824
		today=today.getFullYear()*10000+today.getMonth()*100+100+today.getDate();
		
		// ハイフンをけずる(yyyy-mm-dd → yyyymmdd)
		var birthday=parseInt(arg_yyyy_mm_dd.replace(/-/g, ''));

		// 単純に引き算して、下４ケタを切り捨てて、年部分だけを返す
		return(Math.floor((today-birthday)/10000)); 

    }
    
    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
		
		var emBirthDay = kintone.app.getFieldElements(BIRTHDAY_FIELD_NAME);
		
		// レコード数の分だけループ
		for (var i = 0; i < records.length; i++) {
			var record = records[i];

			// 値の取得
			var record_data = record[BIRTHDAY_FIELD_NAME]['value'];

			// DOM要素の取得し、年齢を追加
			var part = emBirthDay[i];
			part.innerHTML = record_data + "（" + getYearMonth(record_data) + "歳）";

		}

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;


		// 生年月日
		var emBirthDay = kintone.app.record.getFieldElement(BIRTHDAY_FIELD_NAME);

		// 年齢を表示するためのラベルを生成
		var emLabel = document.createElement("label");
		var emDiv = document.createElement("span");
		emBirthDay.appendChild(emDiv);
		
		// 半角スペース的にちょっと間を開ける
		emBirthDay.parentNode.style.width = (parseInt(emBirthDay.parentNode.style.width) + 50) + 'px';
		emDiv.appendChild(emLabel); 		   
		
		var valBirthday = getYearMonth(record[BIRTHDAY_FIELD_NAME]['value']);
        emLabel.appendChild(document.createTextNode("（" + valBirthday + "歳）"));

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show','app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit','app.record.edit.submit'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success','app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.XXX', 'app.record.edit.change.XXX'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************
/*
    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }
    }
*/

})();

