// **********************************************************************************
//
//  ラ・ヴィータ
//
//  履歴
//    2020.06.22    新規作成(Jun.K)
//    2020.06.29    自動入力_js_作成
//    2020.07.08    自動入力項目修正
//                                                        Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){


// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;
        
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
        record['顧客キー'].lookup = true;

        // 顧客名-お届け先名(自動入力)のロック
        record['顧客名_お届け先名']['disabled'] = true;

        // コピーボタン要素作成
        var btnCopy = document.createElement('button');
        btnCopy.id = 'btn_copy_button';
        btnCopy.innerHTML = 'ご依頼主データをお届け先へコピー';
        btnCopy.onclick = function() {
            var name = "";
            var phonenum = "";
            var rec = kintone.app.record.get();
            if (rec) {
                name = rec.record.お名前_ご依頼主様_.value;
                phonenum = rec.record.ご依頼主様_電話番号.value;
            }
            rec.record.お届け先_お名前または会社名.value = name;
            rec.record.お届け先_電話番号.value = phonenum;
            kintone.app.record.set(rec);
        }
        // スペースにボタン要素を付加
        kintone.app.record.getSpaceElement('btn_copy').appendChild(btnCopy);

/*
        //お届け先へコピー
        var name = record['お名前_ご依頼主様_']['value'];
        var namekana = record['ご依頼主様_ふりがな']['value'];
        var phonenum = record['ご依頼主様_電話番号']['value'];
        var company = record['ご依頼主様_会社名']['value'];
        var postnum = record['ご依頼主様_郵便番号']['value'];
        var per = record['ご依頼主様_都道府県NEW']['value'];
        var city = record['ご依頼主様_市区町村・町名NEW']['value'];
        var building = record['ご依頼主様_マンション・建物名']['value'];
        console.log(name);

        //ボタンをクリックしたときのイベント
        btnCopy.onclick = function() {
            //コピー
            record['お届け先_お名前または会社名']['value'] = name;
            console.log(record['お届け先_お名前または会社名']['value']);
            record['お届け先_お名前または会社名_ふりがな']['value'] = namekana;
            record['お届け先_電話番号']['value'] = phonenum;
            record['お届け先_会社名']['value'] = company;
            record['お届け先_郵便番号']['value'] = postnum;
            record['お届け先_都道府県NEW']['value'] = per;
            record['お届け先_市区町村・町名']['value'] = city;
            record['お届け先_マンション・建物名']['value'] = building;
        };
*/

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';
        var checkSpace = record['お届け先_お名前または会社名']['value'];
        var deletSpace = checkSpace.replace(/\s+/g, "");

console.log(record['お名前_ご依頼主様_']['value']);


        // 必須項目なので、そのまま連結します
        record['顧客名_お届け先名']['value'] = record['お名前_ご依頼主様_']['value'] + "-" + deletSpace;
        
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.フィールド名', 'app.record.edit.change.フィールド名'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();