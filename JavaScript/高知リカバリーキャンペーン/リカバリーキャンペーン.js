// **********************************************************************************
//
//  高知リカバリーキャンペーン
//
//  履歴
//    2020.07.30    新規作成
//    2020.08.06    レコード番号作成処理追加
//    2020.08.13    銀行ルックアップ対応
//    2020.08.18    算出レコード番号 → kojinidに変更
//
//                                                        Copyright(C)2020 Caminova
// **********************************************************************************
(function(){
    "use strict";

    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show'], function(event) {
        var record = event.record;

        // 入力不可
        record.kojinid.disabled = true;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        var search = '';

        var yokin = record['yokin_syubetu']['value'];
        console.log('yokin=' + yokin);

        if(yokin == "普通預金") {
            record['預金種目コード']['value'] = "1";
        } else {
            record['預金種目コード']['value'] = "2";
        }

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success'], function(event) {
        var record = event.record;
        var appId = event.appId;
        var recordId = event.recordId;

        //レコードの更新API
        var body = {
            "app": appId,
            "id": recordId,
            "record": {
                "kojinid": {
                    "value": parseFloat(recordId)
                },
                "kojinid_print": {
                    "value": recordId
                }
            }
        };
        return kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', body).then(function(resp) {
            return event;
        }, function(error) {
            alert(error.message);
            event.error = "個人IDが取得できませんでした。少し時間をおいて「保存」を押してください";
            return event;
        });

        return event;
    });

    // ========================================================================================================
    // 印刷画面表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.print.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.銀行名WORK', 'app.record.edit.change.銀行名WORK',
                       'app.record.create.change.銀行種別WORK', 'app.record.edit.change.銀行種別WORK',
                       'app.record.create.change.銀行コードWORK', 'app.record.edit.change.銀行コードWORK',
                       'app.record.create.change.支店名WORK', 'app.record.edit.change.支店名WORK',
                       'app.record.create.change.支店種別WORK', 'app.record.edit.change.支店種別WORK',
                       'app.record.create.change.支店コードWORK', 'app.record.edit.change.支店コードWORK',
                      ], function(event) {
        var record = event.record;

        // 銀行名コピー
        if(record['銀行名WORK']['value'] !== undefined && record['銀行名WORK']['value'] !== '') {
            record['honten']['value'] = record['銀行名WORK']['value'];
        }
        // 銀行種別コピー
        if(record['銀行種別WORK']['value'] !== undefined && record['銀行種別WORK']['value'] !== '') {
            record['ginko_syubetu']['value'] = record['銀行種別WORK']['value'];
        }
        // 銀行コードコピー
        if(record['銀行コードWORK']['value'] !== undefined && record['銀行コードWORK']['value'] !== '') {
            record['ginko_code']['value'] = record['銀行コードWORK']['value'];
        }
        // 支店名コピー
        if(record['支店名WORK']['value'] !== undefined && record['支店名WORK']['value'] !== '') {
            record['siten']['value'] = record['支店名WORK']['value'];
        }
        // 支店種別コピー
        if(record['支店種別WORK']['value'] !== undefined && record['支店種別WORK']['value'] !== '') {
            record['siten_syubetu']['value'] = record['支店種別WORK']['value'];
        }
        // 支店コードコピー
        if(record['支店コードWORK']['value'] !== undefined && record['支店コードWORK']['value'] !== '') {
            record['siten_code']['value'] = record['支店コードWORK']['value'];
        }
        return event;
    });
    
    // ********************************************************************************************************************************
    // コール用関数・クラス
    // ********************************************************************************************************************************
    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

