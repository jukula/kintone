// **********************************************************************************
//
//  リカバリーキャンペーン　申請管理
//
//  履歴
//    2020.07.29      honda 新規作成    
//
// **********************************************************************************

(function(){
    "use strict";
	
    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
		
        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show','app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });
    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success','app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // チェック項目の確認
    //航空機
    kintone.events.on(['app.record.create.change.koukuki', 'app.record.edit.change.koukuki'], function(event) {
        var record = event.record;

        if(record.koukuki.value.indexOf('航空機') != -1) {
            console.log(record.koukuki.value);
            record.koukuki_str.value = "✓";
        } else {
            record.koukuki_str.value = "";
        }
        return event;
    });

    //鉄道
    kintone.events.on(['app.record.create.change.tetudo', 'app.record.edit.change.tetudo'], function(event) {
        var record = event.record;

        if(record.tetudo.value.indexOf('鉄道') != -1) {
            console.log(record.tetudo.value);
            record.tetudo_str.value = "✓";
        } else {
            record.tetudo_str.value = "";
        }
        return event;
    });

    //高速バス
    kintone.events.on(['app.record.create.change.kosokubas', 'app.record.edit.change.kosokubas'], function(event) {
        var record = event.record;

        if(record.kosokubas.value.indexOf('高速バス') != -1) {
            console.log(record.kosokubas.value);
            record.kosokubas_str.value = "✓";
        } else {
            record.kosokubas_str.value = "";
        }
        return event;
    });

    //高速道路
    kintone.events.on(['app.record.create.change.kosoku', 'app.record.edit.change.kosoku'], function(event) {
        var record = event.record;

        if(record.kosoku.value.indexOf('高速道路利用料') != -1) {
            console.log(record.kosoku.value);
            record.kosoku_str.value = "✓";
        } else {
            record.kosoku_str.value = "";
        }                
        return event;
    });

    //レンタカー/リース
    kintone.events.on(['app.record.create.change.rentaka', 'app.record.edit.change.rentaka'], function(event) {
        var record = event.record;

        if(record.rentaka.value.indexOf('レンタカー/リース') != -1) {
            console.log(record.rentaka.value);
            record.rentaka_str.value = "✓";
        } else {
            record.rentaka_str.value = "";
        }

        return event;
    });

    //フェリー運賃
    kintone.events.on(['app.record.create.change.feri', 'app.record.edit.change.feri'], function(event) {
        var record = event.record;
        
        if(record.feri.value.indexOf('フェリー運賃') != -1) {
            console.log(record.feri.value);
            record.feri_str.value = "✓";
        } else {
            record.feri_str.value = "";
        }
        return event;
    });

    //タクシー
    kintone.events.on(['app.record.create.change.taxi', 'app.record.edit.change.taxi'], function(event) {
        var record = event.record;
        
        if(record.taxi.value.indexOf('タクシー') != -1) {
            console.log(record.taxi.value);
            record.taxi_str.value = "✓";
        } else {
            record.taxi_str.value = "";
        }
        return event;
    });

    //観光バス
    kintone.events.on(['app.record.create.change.kankoubus', 'app.record.edit.change.kankoubus'], function(event) {
        var record = event.record;
        
        if(record.kankoubus.value.indexOf('観光バス') != -1) {
            console.log(record.kankoubus.value);
            record.kankou_str.value = "✓";
        } else {
            record.kankou_str.value = "";
        }
        return event;
    });

    //周遊バス
    kintone.events.on(['app.record.create.change.syuyubus', 'app.record.edit.change.syuyubus'], function(event) {
        var record = event.record;
        
        if(record.syuyubus.value.indexOf('周遊バス') != -1) {
            console.log(record.syuyubus.value);
            record.syuyubus_str.value = "✓";
        } else {
            record.syuyubus_str.value = "";
        }
        return event;
    });

    //自家用バス
    kintone.events.on(['app.record.create.change.zikayou', 'app.record.edit.change.zikayou'], function(event) {
        var record = event.record;
        
        if(record.zikayou.value.indexOf('自家用バス') != -1) {
            console.log(record.zikayou.value);
            record.zikayou_str.value = "✓";
        } else {
            record.zikayou_str.value = "";
        }
        return event;
    });

    //定期船
    kintone.events.on(['app.record.create.change.teikisen', 'app.record.edit.change.teikisen'], function(event) {

        var record = event.record;
    
        if(record.teikisen.value.indexOf('定期船') != -1) {
            console.log(record.teikisen.value);
            record.teikisen_str.value = "✓";
        } else {
            record.teikisen_str.value = "";
        }
        return event;
    });

})();