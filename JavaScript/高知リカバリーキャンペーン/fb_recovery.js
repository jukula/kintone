(function() {
  "use strict";

  // 入力値チェックカスタマイズ
  fb.events.form.created.push(function (state) {
    state.fields[4].validations.push({rule: 'seikana_validation', params: []}); // フリガナ(姓)
    state.fields[5].validations.push({rule: 'meikana_validation', params: []}); // フリガナ(名)
    state.fields[6].validations.push({rule: 'yuubin', params: []}); // 郵便番号
    state.fields[8].validations.push({rule: 'tel', params: []}); // 連絡先TEL
    state.fields[52].validations.push({rule: 'syuppatu_validation', params: []}); // 出発日
    state.fields[54].validations.push({rule: 'toutyaku_validation', params: []}); // 到着日
    state.fields[60].validations.push({rule: 'hankakukana_validation', params: []}); // 受取人ﾌﾘｶﾞﾅ(半角)
    state.fields[203].validations.push({rule: 'custom_validation', params: []});   // 請求総額

    //    for(i=0; i<state.fields; i++)

    return state;
  });
  fb.addValidators = function (state) {
    return {
      seikana_validation: {
        getMessage: function (fieldCode, params) {
          return '全角カタカナで入力してください';
        },
        validate: function (value, params) {
          var str = state.record.sinseisya_sei_furigana.value;  // 受取人ﾌﾘｶﾞﾅ(半角)
          console.log(str);
          if(str.match(/^[ァ-ヶー　]*$/)){    //"ー"の後ろの文字は全角スペース。
            return true;
          }else{
            return false; //エラー
          }
        }
      },
      meikana_validation: {
        getMessage: function (fieldCode, params) {
          return '全角カタカナで入力してください';
        },
        validate: function (value, params) {
          var str = state.record.sinseisya_mei_furigana.value;  // フリガナ(姓)
          console.log(str);
          if(str.match(/^[ァ-ヶー　]*$/)){    //"ー"の後ろの文字は全角スペース。
            return true;
          }else{
            return false; //エラー
          }
        }
      },
      yuubin: {
        getMessage: function (fieldCode, params) {
          return '半角数字か"-"で入力して下さい。';
        },
        validate: function (value, params) {
          var str = state.record.shinsei_yuubin.value;  //郵便番号
          console.log(str);
          if(str.match(/^\d{3}[-]?\d{4}$/)){
            return true;
          }else{
            return false; //エラー
          }
        }
      },
      tel: {
        getMessage: function (fieldCode, params) {
          return '半角数字か"-"で入力して下さい。';
        },
        validate: function (value, params) {
          var str = state.record.連絡先TEL.value;  // 連絡先TEL
          console.log(str);
          if(str.match(/^[0-9\-]+$/)){
            return true;
          }else{
            return false; //エラー
          }
        }
      },
      syuppatu_validation: {
        getMessage: function (fieldCode, params) {
          return '出発日〜帰宅日の間の日付を入力してください';
        },
        validate: function (value, params) {
          var res = true;
          var checkIn = state.record.syuppatu.value; // 出発日
          var checkOut = state.record.toutyaku.value; // 帰宅日
          console.log(checkIn);
          console.log(value);
          if((checkIn !== '' && checkIn > value) || (checkOut !== '' && checkOut < value)) res = false;  // エラー
          return res;
        }
      },
      toutyaku_validation: {
        getMessage: function (fieldCode, params) {
          return '出発日〜帰宅日の間の日付を入力してください';
        },
        validate: function (value, params) {
          var res = true;
          var checkIn = state.record.syuppatu.value; // 出発日
          var checkOut = state.record.toutyaku.value; // 帰宅日
          console.log(checkOut);
          console.log(value);
          if((checkOut !== '' && checkOut < value) || (checkIn !== '' && checkIn > value)) res = false;  // エラー
          return res;
        }
      },
      hankakukana_validation: {
        getMessage: function (fieldCode, params) {
          return '半角カタカナ,半角スペースで入力してください';
        },
        validate: function (value, params) {
          var str = state.record.kozameigi_furigana.value;  // フリガナ(名)
          console.log(str);
          if(str.match(/^[ｦ-ﾟ ]*$/)){    //"ー"の後ろの文字は半角スペース。
            return true;
          }else{
            return false; //エラー
          }
        }
      },
      custom_validation: {
        getMessage: function (fieldCode, params) {
          return '請求金額について、最大金額を超えてないか、内訳と一致しているか再度ご確認ください。';
//          return '助成金額が移動合計金額または人数×5,000円を超えています';
//            return '助成金額が0あるいは、移動合計金額または人数×5,000円を超えています';
        },
        validate: function (value, params) {
          var valMin = 0;                             // 申請下限金額
          var valMax = 0;                             // 申請上限金額
          var num = state.record.jyosei_ninzu.value;  // 人数
          var total = state.record.idou_goukei.value; // 移動合計
          var res = true;
          valMax = 5000 * num;
          if(total > 5000 * num) {
            // 移動合計金額 > 人数 x 5,000円
            valMin = 5000 * num;
            valMax = 5000 * num;
          } else {
            // 移動合計金額 < 人数 x 5,000円
            valMin = total;
            valMax = total;
          }
          console.log(value);
//          if((value > valMax) || (value < valMin)) res = false;        // 移動合計額を超えている
          if( (value ?? '') == '' ) res = false;        // ''
          else if( value > valMax ) res = false;        // 移動合計額を超えている
          if( value == 0 )          res = false;        // 0円もエラー

          var josei1 = state.record.tanka1.value * state.record.ninzu1.value;   // 内訳1
          var josei2 = state.record.tanka2.value * state.record.ninzu2.value;   // 内訳2
          var josei3 = state.record.tanka3.value * state.record.ninzu3.value;   // 内訳3
          var josei4 = state.record.tanka4.value * state.record.ninzu4.value;   // 内訳4
          console.log(josei1);
          console.log(josei2);
          console.log(josei3);
          console.log(josei4);
          if((josei1 + josei2 + josei3 + josei4 > 0) && (value != josei1 + josei2 + josei3 + josei4)) res = false;

          return res;
        }
      }
    };
  };

    fb.events.step.next = [function (state) {
      var str = state.record.seikyu_ninzuu.value;  // 人数
      console.log(str);
      var num = Number(str.replace(/人/g, ""));
      console.log(num);
      if(str > 1 && shukuhaku_yuubin_2 === ""){
        console.log("進めません")
      }
          
    }];

    // 出発日
    fb.events.fields.syuppatu.changed = [function (state) {
      var kaishi_date = new Date (state.record.syuppatu.value);
      var date_Day =kaishi_date.getDay();

      //曜日表示処理
      var day_Week = [ "日", "月", "火", "水", "木", "金", "土" ][date_Day];
      state.record.syuppatsu_youbi.value = day_Week;

      return;
    }];

    // 到着日
    fb.events.fields.toutyaku.changed = [function (state) {
      //終了日
      var shuryo_date = new Date (state.record.toutyaku.value);
      var shuryo_year = shuryo_date.getFullYear();
      var shuryo_month = shuryo_date.getMonth() + 1;
      var shuryo_day = shuryo_date.getDate();
      var date_Day =shuryo_date.getDay();

      //開始日
      var kaishi_date = new Date (state.record.syuppatu.value);
      var kaishi_year = kaishi_date.getFullYear();
      var kaishi_month = kaishi_date.getMonth() + 1;
      var kaishi_day = kaishi_date.getDate();
      console.log(shuryo_date - kaishi_date);

      //曜日表示処理
      var day_Week = [ "日", "月", "火", "水", "木", "金", "土" ][date_Day];
      state.record.toutyaku_youbi.value = day_Week;

      // //日帰り判定
      // //滞在開始日と滞在終了日が同じ場合を日帰りとみなす
      var shuryo = shuryo_year + shuryo_month + shuryo_day;
      var kaishi = kaishi_year + kaishi_month + kaishi_day;
      if(kaishi == shuryo) {
          alert('日帰り旅行は、本キャンペーンの対象外です');
          state.record.toutyaku.value = "";
          state.record.toutyaku_youbi.value = "";
      }

      return;
  }];

  // 滞在開始
    fb.events.fields.taizaikaisibi.changed = [function (state) {
      var kaishi_date = new Date (state.record.taizaikaisibi.value);
      var date_Day =kaishi_date.getDay();

      // 曜日表示処理
      var day_Week = [ "日", "月", "火", "水", "木", "金", "土" ][date_Day];
      state.record.kaishi_Day.value = day_Week;

      return;
    }];

    //滞在終了
    fb.events.fields.taizaisyuryobi.changed = [function (state) {
        //終了日
        var shuryo_date = new Date (state.record.taizaisyuryobi.value);
        var shuryo_year = shuryo_date.getFullYear();
        var shuryo_month = shuryo_date.getMonth() + 1;
        var shuryo_day = shuryo_date.getDate();
        var date_Day =shuryo_date.getDay();

        //開始日
        var kaishi_date = new Date (state.record.taizaikaisibi.value);
        var kaishi_year = kaishi_date.getFullYear();
        var kaishi_month = kaishi_date.getMonth() + 1;
        var kaishi_day = kaishi_date.getDate();
        console.log(shuryo_date - kaishi_date);

        //曜日表示処理
        var day_Week = [ "日", "月", "火", "水", "木", "金", "土" ][date_Day];
        state.record.shuryou_Day.value = day_Week;

        // //日帰り判定
        // //滞在開始日と滞在終了日が同じ場合を日帰りとみなす
        var shuryo = shuryo_year + shuryo_month + shuryo_day;
        var kaishi = kaishi_year + kaishi_month + kaishi_day;
        if(kaishi == shuryo) {
            alert('日帰り旅行は、本キャンペーンの対象外です');
            state.record.taizaisyuryobi.value = "";
            state.record.shuryou_Day.value = "";
        } else {
          state.record.syukuhaku_nisu.value = String((shuryo_date - kaishi_date) / 60 / 60 /24 / 1000);
        }

        return;
    }];


    // 情報コピー
    fb.events.fields.sinseisya_sei.changed = [function (state) {
      var sei = state.record.sinseisya_sei.value;
      if (sei != "") state.record.simei_1_sei.value = sei;
    }];
    fb.events.fields.sinseisya_mei.changed = [function (state) {
      var mei = state.record.sinseisya_mei.value;
      if (mei != "") state.record.simei_1_mei.value = mei;
    }];
    fb.events.fields.shinsei_yuubin.changed = [function (state) {
      var yubin = state.record.shinsei_yuubin.value;
      if (yubin != "") state.record.shukuhaku_yuubin_1.value = yubin;
    }];
    fb.events.fields.sinseisya_address.changed = [function (state) {
      var yubin = state.record.sinseisya_address.value;
      if (yubin != "") state.record.jyuusyo_1.value = yubin;
    }];

  // チェックボックスの確認
  fb.events.fields.koukuki.changed = [function (state) {
    var strName = '航空機';

    if(state.record.koukuki.value.indexOf(strName) != -1) {
      console.log(state.record.koukuki.value);
      state.record.koukuki_str.value = "✓";
    } else {
      state.record.koukuki_str.value = "";
    }
    return;
  }];

  fb.events.fields.tetudo.changed = [function (state) {
    var strName = '鉄道';

    if(state.record.tetudo.value.indexOf(strName) != -1) {
      console.log(state.record.tetudo.value);
      state.record.tetudo_str.value = "✓";
    } else {
      state.record.tetudo_str.value = "";
    }
    return;
  }];

  fb.events.fields.kosokubas.changed = [function (state) {
    var strName = '高速バス';

    if(state.record.kosokubas.value.indexOf(strName) != -1) {
      console.log(state.record.kosokubas.value);
      state.record.kosokubas_str.value = "✓";
    } else {
      state.record.kosokubas_str.value = "";
    }
    return;
  }];

  fb.events.fields.kosoku.changed = [function (state) {
    var strName = '高速道路利用料';

    if(state.record.kosoku.value.indexOf(strName) != -1) {
      console.log(state.record.kosoku.value);
      state.record.kosoku_str.value = "✓";
    } else {
      state.record.kosoku_str.value = "";
    }
    return;
  }];

  fb.events.fields.feri.changed = [function (state) {
    var strName = 'フェリー';

    if(state.record.feri.value.indexOf(strName) != -1) {
      console.log(state.record.feri.value);
      state.record.feri_str.value = "✓";
    } else {
      state.record.feri_str.value = "";
    }
    return;
  }];

  fb.events.fields.taxi.changed = [function (state) {
    var strName = 'タクシー';

    if(state.record.taxi.value.indexOf(strName) != -1) {
      console.log(state.record.taxi.value);
      state.record.taxi_str.value = "✓";
    } else {
      state.record.taxi_str.value = "";
    }
    return;
  }];

  fb.events.fields.rentaka.changed = [function (state) {
    var strName = 'レンタカー';

    if(state.record.rentaka.value.indexOf(strName) != -1) {
      console.log(state.record.rentaka.value);
      state.record.rentaka_str.value = "✓";
    } else {
      state.record.rentaka_str.value = "";
    }
    return;
  }];

  fb.events.fields.kankoubus.changed = [function (state) {
    var strName = '観光バス';

    if(state.record.kankoubus.value.indexOf(strName) != -1) {
      console.log(state.record.kankoubus.value);
      state.record.kankou_str.value = "✓";
    } else {
      state.record.kankou_str.value = "";
    }
    return;
  }];

  fb.events.fields.kasikiri.changed = [function (state) {
    var strName = '貸切バス';

    if(state.record.kasikiri.value.indexOf(strName) != -1) {
      console.log(state.record.kasikiri.value);
      state.record.kasikiribus_str.value = "✓";
    } else {
      state.record.kasikiribus_str.value = "";
    }
    return;
  }];

  fb.events.fields.syuyubus.changed = [function (state) {
    var strName = '周遊バス';

    if(state.record.syuyubus.value.indexOf(strName) != -1) {
      console.log(state.record.syuyubus.value);
      state.record.syuyubus_str.value = "✓";
    } else {
      state.record.syuyubus_str.value = "";
    }
    return;
  }];

  fb.events.fields.teikisen.changed = [function (state) {
    var strName = '定期船';

    if(state.record.teikisen.value.indexOf(strName) != -1) {
      console.log(state.record.teikisen.value);
      state.record.teikisen_str.value = "✓";
    } else {
      state.record.teikisen_str.value = "";
    }
    return;
  }];

  /*
  fb.events.form.mounted = [function(state){
    var koukuki = state.record.koukuki.value;
    var koukuki_ninzu = state.record.koukuki_ninzu.value;
    var koukuki_untin = state.record.koukuki_untin.value;
    var kakunin_mes = `利用交通機関：${koukuki}　人（台）数：${koukuki_ninzu}　運賃合計：${koukuki_untin}`
    console.log(kakunin_mes);
    if(state.record.koukuki.value.indexOf('航空機') != -1) {
      console.log(state.record.koukuki.value);
      state.fields[150].label = kakunin_mes;
    }
    

    return state;
  }];
*/

  // 空白除去
  String.prototype.trim = function () {
    return this.replace(/^[\s　\uFEFF\xA0]+|[\s　\uFEFF\xA0]+$/g, '');
  }
  fb.events.form.confirm = [function(state){
    // 半角および全角空白を除去
    var record = state.record;
    // 申請者(姓)
    record.sinseisya_sei.value = record.sinseisya_sei.value.trim();
    // 申請者(名)
    record.sinseisya_mei.value = record.sinseisya_mei.value.trim();
    // 宿泊者1(姓)
    record.simei_1_sei.value = record.simei_1_sei.value.trim();
    // 宿泊者1(名)
    record.simei_1_mei.value = record.simei_1_mei.value.trim();
    // 宿泊者2(姓)
    record.simei_2_sei.value = record.simei_2_sei.value.trim();
    // 宿泊者2(名)
    record.simei_2_mei.value = record.simei_2_mei.value.trim();
    // 宿泊者3(姓)
    record.simei_3_sei.value = record.simei_3_sei.value.trim();
    // 宿泊者3(名)
    record.simei_3_mei.value = record.simei_3_mei.value.trim();
    // 宿泊者4(姓)
    record.simei_4_sei.value = record.simei_4_sei.value.trim();
    // 宿泊者4(名)
    record.simei_4_mei.value = record.simei_4_mei.value.trim();

  }];

  function setTotal(state) {
    var shoukei1 = state.record.uchiwake1.value;
    var shoukei2 = state.record.uchiwake2.value;
    var shoukei3 = state.record.uchiwake3.value;
    var shoukei4 = state.record.uchiwake4.value;
    state.record.jyosei_kingaku.value = shoukei1 + shoukei2 + shoukei3 + shoukei4;
  }

  function setNinzu(state) {
    var shoukei1 = state.record.ninzu1.value;
    var shoukei2 = state.record.ninzu2.value;
    var shoukei3 = state.record.ninzu3.value;
    var shoukei4 = state.record.ninzu4.value;
    state.record.jyosei_ninzu.value = shoukei1 + shoukei2 + shoukei3 + shoukei4;
  }

  fb.events.fields.ninzu1.changed = [function (state) {
    setNinzu(state);
    setTotal(state);
    return;
  }];

  fb.events.fields.ninzu2.changed = [function (state) {
    setNinzu(state);
    setTotal(state);
    return;
  }];

  fb.events.fields.ninzu3.changed = [function (state) {
    setNinzu(state);
    setTotal(state);
    return;
  }];

  fb.events.fields.ninzu4.changed = [function (state) {
    setNinzu(state);
    setTotal(state);
    return;
  }];

  fb.events.fields.tanka1.changed = [function (state) {
    setTotal(state);
    return;
  }];

  fb.events.fields.tanka2.changed = [function (state) {
    setTotal(state);
    return;
  }];

  fb.events.fields.tanka3.changed = [function (state) {
    setTotal(state);
    return;
  }];

  fb.events.fields.tanka4.changed = [function (state) {
    setTotal(state);
    return;
  }];

})();