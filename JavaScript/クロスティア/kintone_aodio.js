(function(){
    "use strict";

   
    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
		
        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;
        var audio = document.createElement("audio");
        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://res.cloudinary.com/code-kitchen/video/upload/v1555038697/posts/zk5sldkxuebny7mwlhh3.mp3");
        audio.id = "kintone_aodio";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        document.getElementById("kintone_aodio").style.display="none";

        return event;
    });

    // 入力画面、編集画面、詳細画面
    kintone.events.on(['app.record.create.show','app.record.edit.show','app.record.detail.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit','app.record.edit.submit'], function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面 編集画面
    kintone.events.on(['app.record.create.submit','app.record.edit.submit'], function(event) {
        var record = event.record;
        
        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success','app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show','app.record.detail.show',
                       'app.record.create.change.xxx', 'app.record.edit.change.xxx'], function(event) {
        var record = event.record;
        
        
        return event;
    });

})();

