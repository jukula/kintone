// **********************************************************************************
//
//  kint音
//
//  履歴
//    2020.08.31    新規作成(Jun.K)
//                                                        Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){



// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;
        var audio = document.createElement("audio");
        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/detail.mp3");
        audio.id = "kintone_audio_detail";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        if(document.getElementById('kintone_audio_detail') !== null) {
            kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        }
        document.getElementById("kintone_audio_detail").style.display="none";

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show'], function(event) {
        var record = event.record;
//        var audio = document.createElement("audio");
        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/create.mp3");
        audio.id = "kintone_audio_create";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        document.getElementById("kintone_audio_create").style.display="none";

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
/*
        $('.recordlist-file-others-gaia').each(function() {
            let href = $(this).find("a").attr('href');
            let hrefarry = href.split(".");
            let extention = hrefarry[hrefarry.length-1];
            if(extention==="mp3"|| extention==="m4a" || extention==="wav"){
                $(this).parent().append(`<li><audio src="${href}" controls><p>音声を再生するには、audioタグをサポートしたブラウザが必要です。</p></audio></li> `);
            }
        });
*/
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;
/*
        var audio = document.createElement("audio");
        audio.control = "control";
        audio.src = kintone.api.url("/k/v1/file", true) + "?fileKey=" + fileKey;
        kintone.app.getHeaderMenuSpaceElement().appendChild(audio);
*/
        return event;
    });

    // ========================================================================================================
    // グラフ表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.report.show'], function(event) {
        var record = event.record;

        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/report.mp3");
        audio.id = "kintone_audio_report";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        document.getElementById("kintone_audio_report").style.display="none";

        return event;
    });

    // ========================================================================================================
    // 印刷画面表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.print.show'], function(event) {
        var record = event.record;


        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // 売上入力
    kintone.events.on(['app.record.create.change.売上金額', 'app.record.edit.change.売上金額'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['売上金額']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;

            console.log(record['売上金額']['value']);
            console.log(record['売上目標']['value']);
            if( record['売上金額']['value'] >= record['売上目標']['value'] ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/uriage_ok.mp3");
            } else {
               // 出力テスト
               console.log(audio);
               audio = new Audio("https://www.cross-tier.com/hack_mp3/uriage_ng.mp3");
            }
    
            audio.id = "kintone_audio_uriage";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_uriage') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
            document.getElementById("kintone_audio_uriage").style.display="none";
        }

        return event;

    });

    // ラジオボタン
    kintone.events.on(['app.record.create.change.ラジオボタン', 'app.record.edit.change.ラジオボタン'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['ラジオボタン']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;

            if( record['ラジオボタン']['value'] == 'sample1' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/radio_01.mp3");
            } else if( record['ラジオボタン']['value'] == 'sample2' ) {
               // 出力テスト
               console.log(audio);
               audio = new Audio("https://www.cross-tier.com/hack_mp3/radio_02.mp3");
            } else if( record['ラジオボタン']['value'] == 'sample3' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/radio_03.mp3");
            } else if( record['ラジオボタン']['value'] == 'sample4' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/radio_04.mp3");
            } else if( record['ラジオボタン']['value'] == 'sample5' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/radio_05.mp3");
            }
    
            audio.id = "kintone_audio_radio";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_radio') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
            document.getElementById("kintone_audio_radio").style.display="none";
        }

        return event;

    });

    // チェックボックス
    kintone.events.on(['app.record.create.change.チェックボックス', 'app.record.edit.change.チェックボックス'], function(event) {
        var record = event.record;

        // チェックボックス
        var chk = String(record['チェックボックス']['value']);
        var audio_chk = null;
        if(chk.indexOf('sample1') !== -1) {
        //            var audio = document.createElement("audio");
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_01.mp3");
        } else if(chk.indexOf('sample2') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_02.mp3");
        } else if(chk.indexOf('sample3') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_03.mp3");
        } else if(chk.indexOf('sample4') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_04.mp3");
        }
        audio_chk.id = "kintone_audio_check";
        audio_chk.controls = true;
        audio_chk.autoplay = true;
        // 出力テスト
        console.log(audio_chk);
        if(document.getElementById('kintone_audio_check') !== null) {
        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio_chk);
        }
        document.getElementById("kintone_audio_check").style.display="none";


        return event;
});

    // ラジオボタン(声サンプル)
    kintone.events.on(['app.record.create.change.声サンプル', 'app.record.edit.change.声サンプル'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['声サンプル']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = new Audio("https://www.cross-tier.com/hack_mp3/" + record['声サンプル']['value'] + ".mp3");
    
            audio.id = "kintone_audio_voice";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_voice') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
            document.getElementById("kintone_audio_voice").style.display="none";
        }

        return event;

    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

