// **********************************************************************************
//
//  kint音
//
//  履歴
//    2020.08.31    新規作成(Jun.K)
//                                                        Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){



// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        // 増殖対策
        if (document.getElementById('mySelect') !== null) {
            return;
        }
        // 音声ボタン
        var mySelect = document.createElement('select');
        var myOption1 = document.createElement('option');
        myOption1.text = 'ON';
        myOption1.value = 'on';
        mySelect.appendChild(myOption1);
        var myOption2 = document.createElement('option');
        myOption2.text = 'OFF';
        myOption2.value = 'off';
        mySelect.appendChild(myOption2);

        kintone.app.getHeaderMenuSpaceElement().appendChild(mySelect);
        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;
        var audio = document.createElement("audio");
        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/detail.mp3");
        audio.id = "kintone_audio_detail";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        if(document.getElementById('kintone_audio_detail') !== null) {
            kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        }
//        document.getElementById("kintone_audio_detail").style.display="none";

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show'], function(event) {
        var record = event.record;
//        var audio = document.createElement("audio");
        // 出力テスト
//        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/start.mp3");
        audio.id = "kintone_audio_start";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
        document.getElementById("kintone_audio_start").style.display="none";

        // 「回答」ボタンを設置
        if (document.getElementById('btn_q2') !== null) {
            return;
        }
        var mySpaceFieldButton = document.createElement('button');
        mySpaceFieldButton.id = 'btn_q2';
        mySpaceFieldButton.innerText = 'ファイナルアンサー';
        mySpaceFieldButton.onclick = function () {
            // Q2確認
            var rec = kintone.app.record.get();
            var chk = rec.record.q2.value;
            var audio_chk = null;
            if((chk.indexOf('フライパン') !== -1) && (chk.indexOf('アンパンマンパンマン') !== -1)) {
                audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/q_ok.mp3");
            }
            if((chk.indexOf('あんぱん') !== -1) || (chk.indexOf('アンパンマン') !== -1) || (chk.indexOf('アンパンマンパン') !== -1)) {
            //            var audio = document.createElement("audio");
                audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/q_ng.mp3");
            }
            audio_chk.id = "kintone_audio_q2";
            audio_chk.controls = true;
            audio_chk.autoplay = true;
            // 出力テスト
            console.log(audio_chk);
            if(document.getElementById('kintone_audio_q2') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio_chk);
            }

        }
        kintone.app.record.getSpaceElement('btn_q2').appendChild(mySpaceFieldButton);

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

/*
        $('.recordlist-file-others-gaia').each(function() {
            let href = $(this).find("a").attr('href');
            let hrefarry = href.split(".");
            let extention = hrefarry[hrefarry.length-1];
            if(extention==="mp3"|| extention==="m4a" || extention==="wav"){
                $(this).parent().append(`<li><audio src="${href}" controls><p>音声を再生するには、audioタグをサポートしたブラウザが必要です。</p></audio></li> `);
            }
        });
*/
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;
/*
        var audio = document.createElement("audio");
        audio.control = "control";
        audio.src = kintone.api.url("/k/v1/file", true) + "?fileKey=" + fileKey;
        kintone.app.getHeaderMenuSpaceElement().appendChild(audio);
*/
        return event;
    });

    // ========================================================================================================
    // グラフ表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.report.show'], function(event) {
        var record = event.record;

        // 出力テスト
        console.log(audio);
        var audio = new Audio("https://www.cross-tier.com/hack_mp3/report.mp3");
        audio.id = "kintone_audio_report";
        audio.controls = true;
        audio.autoplay = true;
        // 出力テスト
        console.log(audio);

        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
//        document.getElementById("kintone_audio_report").style.display="none";

        return event;
    });

    // ========================================================================================================
    // 印刷画面表示時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.print.show'], function(event) {
        var record = event.record;


        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // 問１
    kintone.events.on(['app.record.create.change.q1', 'app.record.edit.change.q1'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['q1']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;
    
            if( record['q1']['value'] == '東から昇って西に沈む') {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ok.mp3");
            } else  {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ng.mp3");
            }

            audio.id = "kintone_audio_q1";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_q1') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_q1").style.display="none";
        }

        return event;
    });

    // 問３
    kintone.events.on(['app.record.create.change.q3', 'app.record.edit.change.q3'], function(event) {
        var record = event.record;

        // 数値
        if(record['q3']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;

            if( record['q3']['value'] == 2019 ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q3_ok.mp3");
            } else if( record['q3']['value'] < 2019 ) {
               // 出力テスト
               console.log(audio);
               audio = new Audio("https://www.cross-tier.com/hack_mp3/q3_ng1.mp3");
            } else {
               // 出力テスト
               console.log(audio);
               audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ng.mp3");
            }
    
            audio.id = "kintone_audio_q3";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_q3') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_q3").style.display="none";
        }

        return event;

    });

    // kintone 問１
    kintone.events.on(['app.record.create.change.kin1', 'app.record.edit.change.kin1'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['kin1']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;
    
            if( record['kin1']['value'] == '2011年11月') {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ok.mp3");
            } else  {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ng.mp3");
            }

            audio.id = "kintone_audio_q4";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_q4') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_q1").style.display="none";
        }

        return event;
    });

    // kintone 問２
    kintone.events.on(['app.record.create.change.kin2', 'app.record.edit.change.kin2'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['kin2']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;
    
            if( record['kin2']['value'] == 'サイボウズ') {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ok.mp3");
            } else  {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/q_ng.mp3");
            }

            audio.id = "kintone_audio_q5";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_q5') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_q1").style.display="none";
        }

        return event;
    });

    // ラジオボタン
    kintone.events.on(['app.record.create.change.sample_radio', 'app.record.edit.change.sample_radio'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['sample_radio']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = null;

            if( record['sample_radio']['value'] == 'sample1' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/sam_01.mp3");
            } else if( record['sample_radio']['value'] == 'sample2' ) {
               // 出力テスト
               console.log(audio);
               audio = new Audio("https://www.cross-tier.com/hack_mp3/sam_02.mp3");
            } else if( record['sample_radio']['value'] == 'sample3' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/sam_03.mp3");
            } else if( record['sample_radio']['value'] == 'sample4' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/sam_04.mp3");
            } else if( record['sample_radio']['value'] == 'sample5' ) {
                // 出力テスト
                console.log(audio);
                audio = new Audio("https://www.cross-tier.com/hack_mp3/sam_05.mp3");
            }
    
            audio.id = "kintone_audio_radio";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_radio') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_radio").style.display="none";
        }

        return event;

    });

    // チェックボックス
    kintone.events.on(['app.record.create.change.sample_check', 'app.record.edit.change.sample_check'], function(event) {
        var record = event.record;

        // チェックボックス
        var chk = String(record['sample_check']['value']);
        var audio_chk = null;
        if(chk.indexOf('sample4') !== -1) {
        //            var audio = document.createElement("audio");
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_04.mp3");
        } else if(chk.indexOf('sample3') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_03.mp3");
        } else if(chk.indexOf('sample2') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_02.mp3");
        } else if(chk.indexOf('sample1') !== -1) {
        audio_chk = new Audio("https://www.cross-tier.com/hack_mp3/chk_01.mp3");
        }
        audio_chk.id = "kintone_audio_check";
        audio_chk.controls = true;
        audio_chk.autoplay = true;
        // 出力テスト
        console.log(audio_chk);
        if(document.getElementById('kintone_audio_check') !== null) {
        kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio_chk);
        }

        return event;
});

    // ラジオボタン(声サンプル)
    kintone.events.on(['app.record.create.change.sample_voice', 'app.record.edit.change.sample_voice'], function(event) {
        var record = event.record;

        // ラジオボタン
        if(record['sample_voice']['value'] !== undefined) {
//            var audio = document.createElement("audio");
            var audio = new Audio("https://www.cross-tier.com/hack_mp3/" + record['sample_voice']['value'] + ".mp3");
    
            audio.id = "kintone_audio_voice";
            audio.controls = true;
            audio.autoplay = true;
            // 出力テスト
            console.log(audio);
            if(document.getElementById('kintone_audio_voice') !== null) {
                kintone.app.record.getHeaderMenuSpaceElement().appendChild(audio);
            }
//            document.getElementById("kintone_audio_voice").style.display="none";
        }

        return event;

    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

