// *****************************************************************************************************************
// ★岡村文具_まいぷれ高知.js
//
// ▼メモ
//  - Cybozu CDN ( https://cybozudev.zendesk.com/hc/ja/articles/202960194 )
//
// ▼更新履歴
//  2019/07/03  新規作成 (Jun.K)
//                                                                           Copyright(C)2019 CrossTier Co.,Ltd.
// *****************************************************************************************************************
(function(){


// ********************************************************************************************************************************
// kintoneイベントハンドラ
// ********************************************************************************************************************************

    // ========================================================================================================
    // 画面表示時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
        var el = kintone.app.getHeaderMenuSpaceElement();

        // 請求先一覧(先月分)
        if ( event.viewId == 5522929 ) {

            // 次月請求レコード作成ボタン
            if ( $('#copyRec').length == 0 ) {

                var user = kintone.getLoginUser();
                if ( user.code != "j_kataoka" && user.code != "Administrator" ) { return; }

                var el = kintone.app.getHeaderMenuSpaceElement();
                $(el).append('<input type="button" value="レコード作成" id="copyRec" style="margin-left:15px;">');
                $('#copyRec').click(function(){

                    var strQuery = '請求書発行日 = THIS_MONTH()';
                    kintone.api("/k/guest/16/v1/records", "GET", {"app":kintone.app.getId(), "query":strQuery}, function(resp_Get) {

                        var objPrms = new Array();
                        for ( var i = 0; i < records.length; i++ ) {
                            var today = new Date();
                            today.setDate(1);
                            var year  = today.getFullYear();
                            var month = today.getMonth()+1;
                            var day   = today.getDate();
                            var outN  = year + "-" + ("00" + month).slice(-2) + "-" + ("00" + day).slice(-2);
                            // var outD  = records[i]['請求書発行日']['value'].split("-");
                            // var outW  = new Date( Number(outD[0]), Number(outD[1]), 1 );
                            // var outN  = outW.getFullYear() + "-" + ("00" + (outW.getMonth()+1)).slice(-2) + "-" + ("00" + outW.getDate()).slice(-2);
                            var lastD = records[i]['期日']['value'].split("-");
                            var lastW = new Date( year, month + 1, 0 );
                            var lastT = new Date( year, month, 0 );
                            var endD  = Number( lastD[2] );
                            if ( endD == lastT.getDate() || endD > lastW.getDate() ) { endD = lastW.getDate(); }
                            var lastN = lastW.getFullYear() + "-" + ("00" + (lastW.getMonth()+1)).slice(-2) + "-" + ("00" + endD).slice(-2);
                            var hakko = "未";
                            if ( records[i]['発行状態']['value'] == "不要" ) { hakko = "不要"; }
                            var tko   = records[i]['商品名2']['value'].substr(-4);
                            if ( tko == "月掲載料" ) {
                                tko = month + tko;
                            } else {
                                tko = records[i]['商品名2']['value'];
                            }

                            // 更新対象チェック
                            //   すでに次月請求レコードが作成されている場合、更新対象外
                            var check = true;
                            for ( var j = 0; j < resp_Get['records'].length; j++ ) {
                                var checkRec = resp_Get['records'][j];
                                var checkWk  = checkRec['請求書発行日']['value'].split("-");
                                if ( month == checkWk[1] && records[i]['店名検索']['value'] == checkRec['店名検索']['value'] ) {
                                    check = false;
                                }
                            }

//                            // 請求金額計算
//                            var sum = 0;
//                            for ( var a = 0; a < records[i]['契約情報']['value'].length; a++ ) {
//                                switch ( records[i]['契約情報']['value'][a] ) {
//                                    case "プラス(基本2000円)":
//                                        sum += 2000;
//                                        break;
//                                    case "ニュース(+5000円)":
//                                        sum += 5000;
//                                        break;
//                                    default:
//                                        break;
//                                }
//                            }

                            if ( check ) {
                                var work  = {
                                    "契約情報"              : { "value" : records[i]['契約情報']['value'] },
                                    "請求書発行日"          : { "value" : outN },
                                    "請求金額"              : { "value" : records[i]['単価2']['value'] },
                                    "商品名2"               : { "value" : tko },
                                    "商品名3"               : { "value" : records[i]['商品名3']['value'] },
                                    "商品名4"               : { "value" : records[i]['商品名4']['value'] },
                                    "数量2"                 : { "value" : records[i]['数量2']['value'] },
                                    "数量3"                 : { "value" : records[i]['数量3']['value'] },
                                    "数量4"                 : { "value" : records[i]['数量4']['value'] },
                                    "単位2"                 : { "value" : records[i]['単位2']['value'] },
                                    "単位3"                 : { "value" : records[i]['単位3']['value'] },
                                    "単位4"                 : { "value" : records[i]['単位4']['value'] },
                                    "単価2"                 : { "value" : records[i]['単価2']['value'] },
                                    "単価3"                 : { "value" : records[i]['単価3']['value'] },
                                    "単価4"                 : { "value" : records[i]['単価4']['value'] },
                                    "発行状態"              : { "value" : hakko },
                                    "FLN"                   : { "value" : records[i]['FLN']['value'] },
                                    "種別"                  : { "value" : records[i]['種別']['value'] },
                                    "期日"                  : { "value" : lastN },
                                    "店名検索"              : { "value" : records[i]['店名検索']['value'] },
                                    "請求金額_税込_"        : { "value" : records[i]['請求金額_税込_']['value'] }
                                };
                                objPrms.push(work);
                            }
                        }

                        kintone.api("/k/guest/16/v1/records", "POST", {"app":kintone.app.getId(), "records": objPrms }, function(resp){
                            alert( "レコード新規作成完了：" + objPrms.length + "件" );
                        }, function(resp){alert(resp['message']);});

                    }, function(resp_Get) { console.log(resp_Get); });

                });

            }

        }

        // 請求金額確認用一覧
        if ( event.viewId == 5522987 ) {

            // 指定項目の背景色変更
            colorChange(records,'初期登録費用');
            colorChange(records,'月額掲載料');
            colorChange(records,'追加請求額');
            colorChange(records,'請求金額');

            // 上記項目のヘッダーの背景色変更
            $('.label-5523475').css('background-color','#F2F5A9');
            $('.label-5523478').css('background-color','#F2F5A9');
            $('.label-5523481').css('background-color','#F2F5A9');
            $('.label-5520543').css('background-color','#F2F5A9');

        }

        // 請求先一覧(確認用リスト)
        if ( event.viewId == 5520448 ) {

            // 請求書一括発行ボタン
            if ( $('#allSeikyu').length == 0 ) {

                var user = kintone.getLoginUser();
                if ( user.code != "j_kataoka" && user.code != "Administrator" ) { return; }

                var el = kintone.app.getHeaderMenuSpaceElement();
                $(el).append('<input type="button" value="請求書一括発行" id="allSeikyu" style="margin-left:15px;">');
                $('#allSeikyu').click(function(){

                    var query = kintone.app.getQueryCondition();
                    query    += ' and 発行状態 not in ("不要")';
                    var strQuery = [
                        { key: "pAppId", val: kintone.app.getId() },
                        { key: "pName",  val: "" },
                        { key: "pQuery", val: query }
                    ];
                    CTCallPHP("outxls_seikyuAll.php", strQuery, false, "okamura-bungu");

                });

            }

        }

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        if ( record['発行状態']['value'] != "不要" ) {
            // 請求書発行ボタン作成
            var el = kintone.app.record.getHeaderMenuSpaceElement();
            $(el).append('<input type="button" value="請求書発行" id="outSeikyu" style="margin-left:20px;">');
            $('#outSeikyu').click(function(){

                var query       = 'レコード番号 = ' + kintone.app.record.getId();
                var record      = kintone.app.record.get();
                var strQuery    = [
                    { key: "pAppId", val: kintone.app.getId() },
                    { key: "pName",  val: record['record']['請求先名']['value'] },
                    { key: "pQuery", val: query }
                ];
                CTCallPHP("outxls_seikyuAll.php", strQuery, false, "okamura-bungu");

            });
        }

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
        record['送付先名']['disabled']   = false;
        record['送付先宛名']['disabled'] = false;

        return event;
    });


    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;
        return event;
    });


    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;
        return event;
    });


    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.フィールド名', 'app.record.edit.change.フィールド名'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

