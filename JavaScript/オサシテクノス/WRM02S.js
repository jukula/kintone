// **********************************************************************************
//
//  WRM-02S
//
//  履歴
//    2020.05.15    新規作成(Jun.K)
//                                                   Copyright(C)2020 Caminova, Inc.
// **********************************************************************************
(function(){



// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
        record['システム番号']['disabled']              = false;
        record['データロガー_型式']['disabled']         = false;
        record['データロガー_製造番号']['disabled']     = false;
        record['水位センサ_型式']['disabled']           = false;
        record['水位センサ_製造番号']['disabled']       = false;
        record['バージョン_水位センサ部']['disabled']   = false;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.電波強度', 'app.record.edit.change.電波強度',
                       'app.record.create.change.電源電圧値', 'app.record.edit.change.電源電圧値',
                       'app.record.create.change.水位センサ供給電圧値', 'app.record.edit.change.水位センサ供給電圧値',
                       'app.record.create.change.電池パネル電圧値', 'app.record.edit.change.電池パネル電圧値',
                       'app.record.create.change.バッテリ電圧値', 'app.record.edit.change.バッテリ電圧値',
                      ], function(event) {
        var record = event.record;

console.log("電波強度：" + record['電波強度']['value']);        
        // 電波強度
        if(record['電波強度']['value'] !== undefined && record['電波強度']['value'] !== '') {
            tmp = parseInt(record['電波強度']['value']);
            console.log("tmp：" + tmp);
            if(tmp == 0) {
                record['判定_電波強度']['value'] = '否';
            } else if(tmp <= 2) {
                record['判定_電波強度']['value'] = '注意';
            } else if(tmp >= 3) {
                record['判定_電波強度']['value'] = '良';
            }
        }
console.log("電源電圧値：" + record['電源電圧値']['value']);        
        // 電源電圧値
        if(record['電源電圧値']['value'] !== undefined && record['電源電圧値']['value'] !== '') {
            tmp = parseFloat(record['電源電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp < 11.5 || tmp > 15.0) {
                record['判定_電源電圧']['value'] = '否';
            } else if(tmp >= 11.5 && tmp <= 15.0) {
                record['判定_電源電圧']['value'] = '良';
            }
        }
        // 水位センサ供給電圧値
        if(record['水位センサ供給電圧値']['value'] !== undefined && record['水位センサ供給電圧値']['value'] !== '') {
            tmp = parseFloat(record['水位センサ供給電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 24.6 && tmp <= 23.4) {
                record['判定_水位センサ供給電圧']['value'] = '良';
            } else if(tmp >= 22.8 && tmp <= 25.2) {
                record['判定_水位センサ供給電圧']['value'] = '注意';
            } else {
                record['判定_水位センサ供給電圧']['value'] = '否';
            }
        }

        // 電池パネル電圧値
        if(record['電池パネル電圧値']['value'] !== undefined && record['電池パネル電圧値']['value'] !== '') {
            tmp = parseFloat(record['電池パネル電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 6.0 && tmp <= 25.0) {
                record['判定_電池パネル電圧']['value'] = '良';
            } else {
                record['判定_電池パネル電圧']['value'] = '否';
            }
        }
        // バッテリ電圧値
        if(record['バッテリ電圧値']['value'] !== undefined && record['バッテリ電圧値']['value'] !== '') {
            tmp = parseFloat(record['バッテリ電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 11.5 && tmp < 14.5) {
                record['判定_バッテリ電圧']['value'] = '良';
            } else {
                record['判定_バッテリ電圧']['value'] = '否';
            }
        }

        return event;
    });

})();

