// **********************************************************************************
//
//  WRM-01S
//
//  履歴
//    2020.05.15    新規作成(Jun.K)
//    2020.10.12    変更箇所記録処理
//                                                   Copyright(C)2020 Caminova, Inc.
// **********************************************************************************
(function(){



// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 詳細画面
    kintone.events.on('app.record.detail.show', function(event) {
        var record = event.record;

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ルックアップ項目活性化
        record['システム番号']['disabled']              = false;
        record['データロガー_型式']['disabled']         = false;
        record['データロガー_製造番号']['disabled']     = false;
        record['水位センサ_型式']['disabled']           = false;
        record['水位センサ_製造番号']['disabled']       = false;
        record['バージョン_水位センサ部']['disabled']   = false;

        // 「前回取得」ボタンを設置
        if (document.getElementById('getPre') !== null) {
            return;
        }
        var mySpaceFieldButton = document.createElement('button');
        mySpaceFieldButton.id = 'getPre';
        mySpaceFieldButton.innerText = '前回取得';
        mySpaceFieldButton.onclick = function () {
            // 前回データ取得処理
            var appID = kintone.app.getId();
            var rec = kintone.app.record.get();
            var target = rec.record.地点.value;
            if(target == "") return event;
            var body = {
                "app": appID,
                "query": '地点 = "' + target + '" order by 実施日 desc',
                "fields": ["判定_機器設定", "備考_機器設定", "判定_システム設定", "備考_システム設定", "判定_電波強度", "電波強度", "備考_電波強度","判定_SDカード","ログ回収_ロガー部","ログ回収_通信部","備考_SDカード"
                ,"判定_端子台","備考_端子台","判定_LED","備考_LED","判定_電源電圧","電源電圧値","備考_電源電圧","判定_水位センサ供給電圧","水位センサ供給電圧値","備考_電源電圧"
                ,"判定_大気開放パイプ","備考_大気開放パイプ","判定_シリカゲル","備考_シリカゲル","判定_圧着端子部","備考_圧着端子部","判定_センサ抵抗値","備考_センサ抵抗値","センサ抵抗値_赤_黄"
                ,"センサ抵抗値_黄_白","センサ抵抗値_白_青","センサ抵抗値_青_赤","判定_樹脂ケース","備考_樹脂ケース","判定_水位センサ保護管","備考_水位センサ保護管","判定_樹脂ケース内部","備考_樹脂ケース内部"
                ,"判定_センサケーブル","備考_センサケーブル","判定_水位追従試験","備考_水位追従試験","センサー移動量_１","機器表示_１","センサー移動量_２","機器表示_２","センサー移動量_３","機器表示_３"
                ,"判定_充放電コントローラ","備考_充放電コントローラ","判定_電池パネル電圧","電池パネル電圧値","備考_バッテリ電圧","判定_電池パネル受光部","備考_電池パネル受光部","判定_バッテリ電圧","バッテリ電圧値"
                ,"備考_バッテリ電圧","判定_使用年数","備考_使用年数","判定_支柱他","備考_支柱他"]
            }
            kintone.api(kintone.api.url('/k/v1/records', true), 'GET', body, function(resp) {
                // success
                console.log(resp);
                // レコード取得なしの場合何もしない
                if(resp.records.length > 0) {
                    // 値更新
                    var val = '';
                    if(!resp.records[0].判定_機器設定.value) val = null;
                    else val = resp.records[0].判定_機器設定.value;
                    rec['record']['判定_機器設定']['value'] = val;
                    rec['record']['備考_機器設定']['value'] = resp.records[0].備考_機器設定.value;
                    if(!resp.records[0].判定_システム設定.value) val = null;
                    else val = resp.records[0].判定_システム設定.value;
                    rec['record']['判定_システム設定']['value'] = val;
                    rec['record']['備考_システム設定']['value'] = resp.records[0].備考_システム設定.value;
                    if(!resp.records[0].判定_電波強度.value) val = null;
                    else val = resp.records[0].判定_電波強度.value;
                    rec['record']['判定_電波強度']['value'] = val;
                    rec['record']['電波強度']['value'] = resp.records[0].電波強度.value;
                    rec['record']['備考_電波強度']['value'] = resp.records[0].備考_電波強度.value;
                    if(!resp.records[0].判定_SDカード.value) val = null;
                    else val = resp.records[0].判定_SDカード.value;
                    rec['record']['判定_SDカード']['value'] = val;
                    rec['record']['ログ回収_ロガー部']['value'] = val;
                    rec['record']['ログ回収_通信部']['value'] = val;
                    rec['record']['備考_SDカード']['value'] = resp.records[0].備考_SDカード.value;
                    if(!resp.records[0].判定_端子台.value) val = null;
                    else val = resp.records[0].判定_端子台.value;
                    rec['record']['判定_端子台']['value'] = val;
                    rec['record']['備考_端子台']['value'] = resp.records[0].備考_端子台.value;
                    if(!resp.records[0].判定_LED.value) val = null;
                    else val = resp.records[0].判定_LED.value;
                    rec['record']['判定_LED']['value'] = val;
                    rec['record']['備考_LED']['value'] = resp.records[0].備考_LED.value;
                    if(!resp.records[0].判定_電源電圧.value) val = null;
                    else val = resp.records[0].判定_電源電圧.value;
                    rec['record']['判定_電源電圧']['value'] = val;
                    rec['record']['電源電圧値']['value'] = resp.records[0].電源電圧値.value;
                    rec['record']['備考_電源電圧']['value'] = resp.records[0].備考_電源電圧.value;
                    if(!resp.records[0].判定_水位センサ供給電圧.value) val = null;
                    else val = resp.records[0].判定_水位センサ供給電圧.value;
                    rec['record']['判定_水位センサ供給電圧']['value'] = val;
                    rec['record']['水位センサ供給電圧値']['value'] = resp.records[0].水位センサ供給電圧値.value;
                    rec['record']['備考_電源電圧']['value'] = resp.records[0].備考_水位センサ供給電圧.value;
                    if(!resp.records[0].判定_大気開放パイプ.value) val = null;
                    else val = resp.records[0].判定_大気開放パイプ.value;
                    rec['record']['判定_大気開放パイプ']['value'] = val;
                    rec['record']['備考_大気開放パイプ']['value'] = resp.records[0].備考_大気開放パイプ.value;
                    if(!resp.records[0].判定_シリカゲル.value) val = null;
                    else val = resp.records[0].判定_シリカゲル.value;
                    rec['record']['判定_シリカゲル']['value'] = val;
                    rec['record']['備考_シリカゲル']['value'] = resp.records[0].備考_シリカゲル.value;
                    if(!resp.records[0].判定_圧着端子部.value) val = null;
                    else val = resp.records[0].判定_圧着端子部.value;
                    rec['record']['判定_圧着端子部']['value'] = val;
                    rec['record']['備考_圧着端子部']['value'] = resp.records[0].備考_圧着端子部.value;
                    if(!resp.records[0].判定_センサ抵抗値.value) val = null;
                    else val = resp.records[0].判定_センサ抵抗値.value;
                    rec['record']['判定_センサ抵抗値']['value'] = val;
                    rec['record']['備考_センサ抵抗値']['value'] = resp.records[0].備考_センサ抵抗値.value;
                    rec['record']['センサ抵抗値_赤_黄']['value'] = resp.records[0].センサ抵抗値_赤_黄.value;
                    rec['record']['センサ抵抗値_黄_白']['value'] = resp.records[0].センサ抵抗値_黄_白.value;
                    rec['record']['センサ抵抗値_白_青']['value'] = resp.records[0].センサ抵抗値_白_青.value;
                    rec['record']['センサ抵抗値_青_赤']['value'] = resp.records[0].センサ抵抗値_青_赤.value;
                    if(!resp.records[0].判定_樹脂ケース.value) val = null;
                    else val = resp.records[0].判定_樹脂ケース.value;
                    rec['record']['判定_樹脂ケース']['value'] = val;
                    rec['record']['備考_樹脂ケース']['value'] = resp.records[0].備考_樹脂ケース.value;
                    if(!resp.records[0].判定_水位センサ保護管.value) val = null;
                    else val = resp.records[0].判定_水位センサ保護管.value;
                    rec['record']['判定_水位センサ保護管']['value'] = val;
                    rec['record']['備考_水位センサ保護管']['value'] = resp.records[0].備考_水位センサ保護管.value;
                    if(!resp.records[0].判定_樹脂ケース内部.value) val = null;
                    else val = resp.records[0].判定_樹脂ケース内部.value;
                    rec['record']['判定_樹脂ケース内部']['value'] = val;
                    //チェックボックスはどのように扱えばいいでしょうか？
                    rec['record']['備考_樹脂ケース内部']['value'] = resp.records[0].備考_樹脂ケース内部.value;
                    if(!resp.records[0].判定_センサケーブル.value) val = null;
                    else val = resp.records[0].判定_センサケーブル.value;
                    rec['record']['判定_センサケーブル']['value'] = val;
                    rec['record']['備考_センサケーブル']['value'] = resp.records[0].備考_センサケーブル.value;
                    if(!resp.records[0].判定_水位追従試験.value) val = null;
                    else val = resp.records[0].判定_水位追従試験.value;
                    rec['record']['判定_水位追従試験']['value'] = val;
                    rec['record']['備考_水位追従試験']['value'] = resp.records[0].備考_水位追従試験.value;
                    rec['record']['センサー移動量_１']['value'] = resp.records[0].センサー移動量_１.value;
                    rec['record']['機器表示_１']['value'] = resp.records[0].機器表示_１.value;
                    rec['record']['センサー移動量_２']['value'] = resp.records[0].センサー移動量_２.value;
                    rec['record']['機器表示_２']['value'] = resp.records[0].機器表示_２.value;
                    rec['record']['センサー移動量_３']['value'] = resp.records[0].センサー移動量_３.value;
                    rec['record']['機器表示_３']['value'] = resp.records[0].機器表示_３.value;
                    if(!resp.records[0].判定_充放電コントローラ.value) val = null;
                    else val = resp.records[0].判定_充放電コントローラ.value;
                    rec['record']['判定_充放電コントローラ']['value'] = val;
                    rec['record']['備考_充放電コントローラ']['value'] = resp.records[0].備考_充放電コントローラ.value;
                    if(!resp.records[0].判定_電池パネル電圧.value) val = null;
                    else val = resp.records[0].判定_電池パネル電圧.value;
                    rec['record']['判定_電池パネル電圧']['value'] = val;
                    rec['record']['電池パネル電圧値']['value'] = resp.records[0].電池パネル電圧値.value;
                    rec['record']['備考_バッテリ電圧']['value'] = resp.records[0].備考_バッテリ電圧.value;
                    if(!resp.records[0].判定_電池パネル受光部.value) val = null;
                    else val = resp.records[0].判定_電池パネル受光部.value;
                    rec['record']['判定_電池パネル受光部']['value'] = val;
                    rec['record']['備考_電池パネル受光部']['value'] = resp.records[0].備考_電池パネル受光部.value;
                    if(!resp.records[0].判定_バッテリ電圧.value) val = null;
                    else val = resp.records[0].判定_バッテリ電圧.value;
                    rec['record']['判定_バッテリ電圧']['value'] = val;
                    rec['record']['バッテリ電圧値']['value'] = resp.records[0].バッテリ電圧値.value;
                    rec['record']['備考_バッテリ電圧']['value'] = resp.records[0].備考_バッテリ電圧.value;
                    if(!resp.records[0].判定_使用年数.value) val = null;
                    else val = resp.records[0].判定_使用年数.value;
                    rec['record']['判定_使用年数']['value'] = val;
                    rec['record']['備考_使用年数']['value'] = resp.records[0].備考_使用年数.value; 
                    if(!resp.records[0].判定_支柱他.value) val = null;
                    else val = resp.records[0].判定_支柱他.value;
                    rec['record']['判定_支柱他']['value'] = val;
                    rec['record']['備考_支柱他']['value'] = resp.records[0].備考_支柱他.value;                  
                    kintone.app.record.set(rec);
                } 
                return resp;
            }, function(error) {
                // error
                console.log(error);
            });

            }
        kintone.app.record.getSpaceElement('getPre').appendChild(mySpaceFieldButton);

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.電波強度', 'app.record.edit.change.電波強度',
                       'app.record.create.change.電源電圧値', 'app.record.edit.change.電源電圧値',
                       'app.record.create.change.水位センサ供給電圧値', 'app.record.edit.change.水位センサ供給電圧値',
                       'app.record.create.change.電池パネル電圧値', 'app.record.edit.change.電池パネル電圧値',
                       'app.record.create.change.バッテリ電圧値', 'app.record.edit.change.バッテリ電圧値',
                      ], function(event) {
        var record = event.record;

console.log("電波強度：" + record['電波強度']['value']);        
        // 電波強度
        if(record['電波強度']['value'] !== undefined && record['電波強度']['value'] !== '') {
            tmp = parseInt(record['電波強度']['value']);
            console.log("tmp：" + tmp);
            if(tmp == 0) {
                record['判定_電波強度']['value'] = '否';
            } else if(tmp <= 2) {
                record['判定_電波強度']['value'] = '注意';
            } else if(tmp >= 3) {
                record['判定_電波強度']['value'] = '良';
            }
        }
console.log("電源電圧値：" + record['電源電圧値']['value']);        
        // 電源電圧値
        if(record['電源電圧値']['value'] !== undefined && record['電源電圧値']['value'] !== '') {
            tmp = parseFloat(record['電源電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp < 11.5 || tmp > 15.0) {
                record['判定_電源電圧']['value'] = '否';
            } else if(tmp >= 11.5 && tmp <= 15.0) {
                record['判定_電源電圧']['value'] = '良';
            }
        }
        // 水位センサ供給電圧値
        if(record['水位センサ供給電圧値']['value'] !== undefined && record['水位センサ供給電圧値']['value'] !== '') {
            tmp = parseFloat(record['水位センサ供給電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 3.71 && tmp <= 3.79) {
                record['判定_水位センサ供給電圧']['value'] = '良';
            } else if(tmp >= 3.65 && tmp < 3.86) {
                record['判定_水位センサ供給電圧']['value'] = '注意';
            } else {
                record['判定_水位センサ供給電圧']['value'] = '否';
            }
        }

        // 電池パネル電圧値
        if(record['電池パネル電圧値']['value'] !== undefined && record['電池パネル電圧値']['value'] !== '') {
            tmp = parseFloat(record['電池パネル電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 6.0 && tmp <= 25.0) {
                record['判定_電池パネル電圧']['value'] = '良';
            } else {
                record['判定_電池パネル電圧']['value'] = '否';
            }
        }
        // バッテリ電圧値
        if(record['バッテリ電圧値']['value'] !== undefined && record['バッテリ電圧値']['value'] !== '') {
            tmp = parseFloat(record['バッテリ電圧値']['value']);
            console.log("tmp：" + tmp);
            if(tmp >= 11.5 && tmp < 14.5) {
                record['判定_バッテリ電圧']['value'] = '良';
            } else {
                record['判定_バッテリ電圧']['value'] = '否';
            }
        }

        return event;
    });
    
    // ========================================================================================================
    // 変更箇所記録
    // ========================================================================================================
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.地点レコード番号', 'app.record.edit.change.地点レコード番号',
                      ], function(event) {
        var record = event.record;

        // システム番号
        if(record['地点レコード番号']['value'] !== undefined && record['地点レコード番号']['value'] !== '') {
            // 変更箇所リセット
            record['変更箇所']['value'] = [];
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.システム番号', 'app.record.edit.change.システム番号',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // システム番号
        if(record['システム番号']['value'] !== undefined && record['システム番号']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            console.log(tmp);
            tmp.push('システム番号');
console.log(tmp);
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.データロガー_型式', 'app.record.edit.change.データロガー_型式',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // システム番号
        if(record['データロガー_型式']['value'] !== undefined && record['データロガー_型式']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            console.log(tmp);
            tmp.push('データロガー_型式');
console.log(tmp);
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.データロガー_製造番号', 'app.record.edit.change.データロガー_製造番号',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // システム番号
        if(record['データロガー_製造番号']['value'] !== undefined && record['データロガー_製造番号']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            console.log(tmp);
            tmp.push('データロガー_製造番号');
console.log(tmp);
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.水位センサ_型式', 'app.record.edit.change.水位センサ_型式',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // システム番号
        if(record['水位センサ_型式']['value'] !== undefined && record['水位センサ_型式']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            console.log(tmp);
            tmp.push('水位センサ_型式');
console.log(tmp);
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.水位センサ_製造番号', 'app.record.edit.change.水位センサ_製造番号',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // 水位センサ_製造番号
        if(record['水位センサ_製造番号']['value'] !== undefined && record['水位センサ_製造番号']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            console.log(tmp);
            tmp.push('水位センサ_製造番号');
console.log(tmp);
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.バージョン_水位センサ部', 'app.record.edit.change.バージョン_水位センサ部',
                      ], function(event) {
        var record = event.record;
        var tmp = [];

        // バージョン_水位センサ部
        if(record['バージョン_水位センサ部']['value'] !== undefined && record['バージョン_水位センサ部']['value'] !== '') {
            tmp = record['変更箇所']['value'];
            tmp.push('バージョン_水位センサ部');
            record['変更箇所']['value'] = tmp;
        }

        return event;
    });

})();

