// **********************************************************************************
//
//  カミノバ  CaféNOVAお問い合わせ
//
//  履歴
//    2020.06.29    新規作成(Jun.K)
//                                                   Copyright(C)2020 Caminova, Inc.
// **********************************************************************************
(function(){
    "use strict";

    // **********************************************************************************
    // kintoneイベントハンドラ
    // **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;
		
        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        // mail
        var mail = record['E_mail']['value'];
        var subject = "subject=" + "お問い合わせありがとうございます";
        var body = "body=" + record['回答']['value'];
        // 送信ボタン(link)を設置
        var sendLink = document.createElement('a');
        sendLink.href = 'mailto:' + mail + '?' + subject + '&' + body;
        var str = document.createTextNode("メール送信");
        sendLink.appendChild(str);

console.log(sendLink.href);
        kintone.app.record.getSpaceElement('sendmail').appendChild(sendLink);

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show','app.record.edit.show'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 画面保存時に実行
    // ========================================================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        return event;
    });

    // プロセス管理
    kintone.events.on(['app.record.detail.process.proceed'], function(event) {
        var record = event.record;
        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit','app.record.edit.submit'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // 保存成功時に実行
    // ========================================================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success','app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });

    // ========================================================================================================
    // フィールド値変更時に実行
    // ========================================================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.XXX', 'app.record.edit.change.XXX'], function(event) {
        var record = event.record;
        return event;
    });




// ********************************************************************************************************************************
// コール用関数・クラス
// ********************************************************************************************************************************
/*
    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }
    }
*/

})();

