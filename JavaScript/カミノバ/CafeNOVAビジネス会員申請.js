// **********************************************************************************
//
//  カミノバ
//
//  履歴
//    2020.06.08    新規作成(Jun.K)
//                                                       Copyright(C)2020 Cross-Tier
// **********************************************************************************
(function(){

// **********************************************************************************
// kintoneイベントハンドラ
// **********************************************************************************

    // ==============================================================================
    // 画面表示時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.show'], function(event) {
        var records = event.records;

        // ここに処理を記載

        return event;
    });

    // 詳細画面
    kintone.events.on(['app.record.detail.show'], function(event) {
        var record = event.record;

        // ここに処理を記載

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        var record = event.record;

        // ここに処理を記載

        return event;
    });


    // ==============================================================================
    // 画面保存時に実行
    // ==============================================================================
    // 一覧画面
    kintone.events.on(['app.record.index.edit.submit'], function(event) {
        var records = event.records;

        // ここに処理を記載

        return event;
    });

    // 入力画面
    kintone.events.on(['app.record.create.submit', 'app.record.edit.submit'], function(event) {
        var record = event.record;

console.log(record['申請ステータス']['value']);

        // 入会日が入って、「処理済み」になった時点で会員番号を保存
        if(record['申請ステータス']['value'] == "処理済み") {
            if((record['入会日']['value'] == null) || (record['入会日']['value'] == "")) {
                record["入会日"]["error"] = "処理済みにした場合、入会日は必須です";
                event.error = "入会日を入力してください";
            } else {
                var memberNO = 10000 + event.recordId;
                record['会員番号']['value'] = (record['会員種別']['value'] == 'スタンダード' ? 'S' : 'C') + String(memberNO);
            }
        }
        

        return event;
    });


    // ==============================================================================
    // 保存成功時に実行
    // ==============================================================================
    // 入力画面
    kintone.events.on(['app.record.create.submit.success', 'app.record.edit.submit.success'], function(event) {
        var record = event.record;

        return event;
    });


    // ==============================================================================
    // フィールド値変更時に実行
    // ==============================================================================
    // [フィールド名]
    kintone.events.on(['app.record.create.show', 'app.record.edit.show',
                       'app.record.create.change.フィールド名', 'app.record.edit.change.フィールド名'], function(event) {
        var record = event.record;

        // ここに処理を記載

        return event;
    });


// **********************************************************************************
// コール用関数・クラス
// **********************************************************************************

    function colorChange( records , setName ) {

        var getField = kintone.app.getFieldElements(setName);

        // 一覧に表示された件数分ループ
        for ( var i = 0; i < records.length; i++ ) {

            var fieldAry = [];
            fieldAry.push(getField[i]);

            var colorBg = "background-color:#F2F5A9";
            $(fieldAry).attr('style',colorBg);

        }

    }

})();

