
/* global gapi */

(function() {
    'use strict';
    // API キー
    var api_key = 'AIzaSyBh0XCKeEdAD3c_MwZbBaynd1tyYkFeB8Q';
    // クライアントID
    var client_id = '819636261037-vvcf13qljumrq4fe8k5kd04h3vanv3mk.apps.googleusercontent.com';
    // カレンダーID
    var calendar_id = 'caminova.jp_9pt5nfnm77gmtf0lt4aepp4b3g@group.calendar.google.com';
    // 認証用URL（読み取り／更新）
    var scope = 'https://www.googleapis.com/auth/calendar';
    // Discovery Docs
    var discovery_docs = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'];

    function initClient() {
        gapi.client.init({
            'apiKey': api_key,
            'discoveryDocs': discovery_docs,
            'clientId': client_id,
            'scope': scope
        }).then(function() {
            // Google認証済みのチェック
            if (!gapi.auth2.getAuthInstance().isSignedIn.get()) {
                // Google認証の呼び出し
                gapi.auth2.getAuthInstance().signIn();
            }
        });
    }

    // APIクライアントとOauth2モジュールのロード
    // モジュールロード後のinitClient関数の呼び出し
    gapi.load('client:auth2', {
        callback: function() {
            // gapi.clientのイニシャライズ処理
            initClient();
        },
        onerror: function() {
            // エラー時の処理
            alert('gapi.client のロードに失敗しました!');
        }
    });

    function publishEvent(event) {
        // レコードのデータの取得
        var record = kintone.app.record.get().record;
        if (record) {
            // Google認証済みのチェック
            if (!gapi.auth2.getAuthInstance().isSignedIn.get()) {
                // Google認証の呼び出し
                gapi.auth2.getAuthInstance().signIn();
                alert('Google認証されていません。');
                return;
            }
            // API リクエスト
            // リクエストパラメータの設定
            if (record.allday.value.indexOf('終日') !== -1) {
                // 終日
                var params = {
                    // イベントのタイトル
                    'summary': record.event_name.value,
                    'start': {
                        // 開始日・時刻
                        'date': record.event_date.value
                    },
                    'end': {
                        // 終了日・時刻
                        'date': record.event_date.value
                    },
                    // 場所の指定
                    'location': record.event_location.value,
                    // イベントの説明
                    'description': record.event_description.value
                };
            } else {
                // 通常
                var params = {
                    // イベントのタイトル
                    'summary': record.event_name.value,
                    'start': {
                        // 開始日・時刻
                        'dateTime': record.start_datetime.value,
                        'timeZone': 'America/Los_Angeles'
                    },
                    'end': {
                        // 終了日・時刻
                        'dateTime': record.end_datetime.value,
                        'timeZone': 'America/Los_Angeles'
                    },
                    // 場所の指定
                    'location': record.event_location.value,
                    // イベントの説明
                    'description': record.event_description.value
                };
            }
            var request;
            // リクエストメソッドとパラメータの設定
            // 公開済みイベントを更新
            if (record.event_id.value) {
                request = gapi.client.calendar.events.update(
                    {
                        'calendarId': calendar_id,
                        'eventId': record.event_id.value,
                        'resource': params
                    }
                );
            } else {
                // 未公開のイベントを追加
                request = gapi.client.calendar.events.insert(
                    {
                        'calendarId': calendar_id,
                        'resource': params
                    }
                );
            }
            // Googleカレンダーへのイベント登録の実行
            request.execute(function(resp) {
                if (resp.error) {
                    alert('イベントの登録に失敗しました。');
                } else {
                    var body = {
                        'app': kintone.app.getId(),
                        'id': record.$id.value,
                        'record': {
                            'event_id': {
                                'value': resp.result.id
                            }
                        }
                    };
                    return kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', body).then(function(success) {
                        alert('Googleカレンダーにイベントを登録しました。');
                        location.reload();
                    }).catch(function(error) {
                        alert('Google イベントIDの登録に失敗しました。');
                    });
                }
            }, function(error) {
                alert('Google イベントIDの登録に失敗しました。');
            });
        }
    }
    
    function removeEvent(event) {
        // レコードのデータの取得
        var record = kintone.app.record.get().record;
        if (record) {
            // Google認証済みのチェック
            if (!gapi.auth2.getAuthInstance().isSignedIn.get()) {
                // Google認証の呼び出し
                gapi.auth2.getAuthInstance().signIn();
                alert('Google認証されていません。');
                return;
            }
            // API リクエスト
            // Calendar.Events.remove(calendarId, eventId, {sendUpdates: "all"});

            // リクエストパラメータの設定
            var params = {
                // イベントのタイトル
                'sendUpdates': "all"
            };

            var request;
            // リクエストメソッドとパラメータの設定
            // 公開済みイベントを更新
            if (record.event_id.value) {
                request = gapi.client.calendar.events.delete(
                    {
                        'calendarId': calendar_id,
                        'eventId': record.event_id.value,
                        'resource': params
                    }
                );
            }
            // Googleカレンダーのイベント削除の実行
            request.execute(function(resp) {
                if (resp.error) {
                    alert('イベントの削除に失敗しました。');
                } else {
                    var body = {
                        'app': kintone.app.getId(),
                        'id': record.$id.value,
                        'record': {
                            'event_id': {
                                'value': resp.result.id
                            }
                        }
                    };
                    return kintone.api(kintone.api.url('/k/v1/record', true), 'PUT', body).then(function(success) {
                        alert('Googleカレンダーのイベントを非公開にしました。');
                        location.reload();
                    }).catch(function(error) {
                        alert('Google イベントIDの削除に失敗しました。');
                    });
                }
            }, function(error) {
                alert('Google イベントIDの登録に失敗しました。');
            });
        }
    }

    // レコード詳細画面の表示後イベント
    kintone.events.on('app.record.detail.show', function(event) {
        // 増殖バグ回避
        if (document.getElementById('publish_button') !== null) {
            return event;
        }
        // 画面下部にボタンを設置
        var publishButton = document.createElement('button');
        publishButton.id = 'publish_button';
        publishButton.innerHTML = '公開する';
        publishButton.className = 'button-simple-cybozu geo-search-btn';
        publishButton.style = 'margin-top: 30px; margin-left: 10px;';
        publishButton.addEventListener('click', function() {
            publishEvent(event);
        });
        kintone.app.record.getSpaceElement('publish_button_space').appendChild(publishButton);

        // 予定削除
        if (event.record.event_id.value) {
            var removeButton = document.createElement('button');
            removeButton.id = 'remove_button';
            removeButton.innerHTML = '非公開にする';
            removeButton.className = 'button-simple-cybozu geo-search-btn';
            removeButton.style = 'margin-top: 30px; margin-left: 10px;';
            removeButton.addEventListener('click', function() {
                removeEvent(event);
            });
            kintone.app.record.getSpaceElement('publish_button_space').appendChild(removeButton);
        }


        return event;
    });

    kintone.events.on(['app.record.create.show', 'app.record.edit.show'], function(event) {
        // フィールドを編集不可へ
        event.record.event_id.disabled = true;
        return event;
    });
})();
