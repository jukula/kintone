<?php
/*****************************************************************************/
/*  kintone連携用定数定義ファイル                             (Version 2.00) */
/*                                                                           */
/*   本ファイル内で定義されたものは各サイトごとに異なるものです              */
/*                                                                           */
/*                                    Copyright(C)2013-2019 CrossTier Corp.  */
/*****************************************************************************/
if (!defined("DEFKINTONE_INC")) {
    // ２重インクルード防止
    define("DEFKINTONE_INC", true);

    ///////////////////////////////////////////////////////////////////////////////
    // kintone定数
    ///////////////////////////////////////////////////////////////////////////////
    
	// kintoneデータ取得・更新モード
	define("CT_MODE_SEL"    , 0 ); // SELECT
    define("CT_MODE_INS"    , 1 ); // INSERT
    define("CT_MODE_UPD"    , 2 ); // UPDATE
    define("CT_MODE_DEL"    , 3 ); // DELETE
    define("CT_MODE_UPL"    , 10 ); // アップロード
    define("CT_MODE_DWL"    , 11 ); // ダウンロード

	// レコード件数（kintoneの制限）
	define("CY_SEL_MAX"		, 100 );	// 選択時(kintoneの制限ではない。in を使う時のため。)
	define("CY_INS_MAX"		, 50  );	// 登録時 2013.8.8 100件では502エラーになる場合があるので、50件に変更。
	define("CY_UPD_MAX"		, 50  );	// 更新時 2013.8.8 100件では502エラーになる場合があるので、50件に変更。
	define("CY_DEL_MAX"		, 50  );	// 削除時 2013.8.8 100件では502エラーになる場合があるので、50件に変更。

	// 最大最小取得（データ並び順）
	define("CY_DAT_MIN"		, "asc"  );	// 最小(昇順)
	define("CY_DAT_MAX"		, "desc" );	// 最大(降順)

	// REST API呼び出し用
	define( "CT_CONTENTTYPE_JSON" 	, "application/json" );		// データ更新など
	define( "CT_CONTENTTYPE_MPFD" 	, "multipart/form-data" );	// ファイルアップロード

}
?>
