<?php
/*****************************************************************************/
/* 労務原価集計PHP                                            (Version 1.01) */
/*   ファイル名 : romutotal.php                                              */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*              2013/04/30  Version 1.01(T.M)                                */
/*                          updANKK 0クリア処理 追加                         */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcSkiSgnp();

/*
echo '$_GET'."<br>\n"; print_r($_GET); echo "<br><br>\n\n";
echo '$_GET'."<br>\n"; print_r($_GET); echo "<br><br>\n\n";
echo '$_POST'."<br>\n"; print_r($_POST); echo "<br><br>\n\n";
*/
	// 実行
	print_r( $clsSrs->main() );


	class TcSkiSgnp
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $pStrDate;
		var $pEndDate;
		var $pSki;

		var $datRmgk;		// 作業日報( 作業日、担当者、案件レコード番号、社内外、対応区分、製品区分、作業区分、区分明細、経過時間(分)案件、単価、実行労務費(案件)円 )
		var $datRmgkCount;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcSkiSgnp() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// ------------
			// パラメタ保存
			// ------------
			// 日付項目
			$this->pStrDate = $_GET['strDate'];
			$this->pEndDate = $_GET['endDate'];
			// 集計項目
			foreach( $_GET as $key => $val ) {
				if( substr($key,0,4) == "ski_" ) {
					$this->pSki[$key] = $val;
				}
			}

			// 検索用日付を生成
			if( $this->pStrDate == "" ) {
				//
			} else {
				// 同月1日
				$this->pStrDate = $this->pStrDate."-1";
			}
			if( $this->pEndDate == "" ) {
				//
			} else {
				// 翌月1日
				$wk = explode( "-" , $this->pEndDate );
				$this->pEndDate = date( 'Y-m-d' ,  mktime( 0,0,0 , $wk[1] + 1 , 1 , $wk[0] ) );
				
			}

			// 作業日報を読み込み中
			$ret = $this->getRmgk();
			if( $ret == 200 ) {
				print_r($this->datRmgk);
			} else {
				//
				print_r($ret);
			}

			// ----------------------------------------------
			// 更新対象の作業日報をメインに以降の処理を行う。
			// ----------------------------------------------

			return;
		}

		/*************************************************************************/
	    /* 労務原価から最新の作成日付を取得する                                  */
	    /*  引数	なし                                                         */
	    /*  関数値  string		正常終了:労務原価の最終作成日時、異常終了:null   */
	    /*************************************************************************/
		function getRmgk() {
			$ret = null;

			// 取得するデータを初期化
			$this->datRmgk = array();
			$this->datRmgkCount =0;

			// 読込開始
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TCRMGK;	// アプリID（作業日報）

			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			// 取得件数制限ごとにループして処理を行う。
			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
				if( $this->pStrDate == "" ) {
					//
				} else {
					$aryQ[] = "作業日付 >= \"".$this->pStrDate."\"";
				}
				if( $this->pEndDate == "" ) {
					//
				} else {
					$aryQ[] = "作業日付 < \"".$this->pEndDate."\"";
				}
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$json = $k->runCURLEXEC( TC_MODE_SEL );

				// エラーチェック
				if( $k->strHttpCode == 200 ) {
					// データ無し
					if( $k->intDataCount == 0 ) {
						break;
					} else {
						// 次データ読込用
						$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;
						// データを保存
						$this->datRmgk = array_merge( $this->datRmgk , $json->records );
						$this->datRmgkCount += $k->intDataCount;
					}
				} else {
					print_r($k);
					break;
				}

			} while( $k->intDataCount > 0 );

			return ( $k->strHttpCode );
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
