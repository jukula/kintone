<?php
/*****************************************************************************/
/* 保守管理PHP                                				  (Version 1.00) */
/*   ファイル名 : seikyu_hs_ikt.php                                          */
/*   更新履歴   2013/10/04  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*   [機能]			                                                         */
/*   ・保守管理から、請求データを作成する。		 	(seikyu_hs_ikt.html)     */
/*   ・保守管理から、請求前確認用のデータを返す。	(seikyu_hs_ikt.html)     */
/*   ・保守管理の明細から、有効データ抽出し、保守管理(最新明細)を作成する。  */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define("TC_RITU_SKETA"	, 1 ); 			// 売上％、粗利％の少数桁数

	// 処理区分(パラメタ)
	define("TC_SYORI_KBN_CHK"	, "MAE" ); 	// 売上計上前チェック
	define("TC_SYORI_KBN_SBN"	, "SBN" ); 	// 契約変更点チェック
	define("TC_SYORI_KBN_SEI"	, "SEI" ); 	// 売上計上処理
	define("TC_SYORI_KBN_NEW"	, "NEW" ); 	// 最新保守作成
	define("TC_SYORI_KBN_ZEN"	, "ZEN" ); 	// 前回締日情報

	// 保守管理
	define("TC_SEK_TNI_NEN"		, '年');	// 契約、積算単位：年
	define("TC_SEK_TNI_TUKI"	, '月');	// 契約、積算単位：月

	define("TC_SKEI_KBN_URI"	, '売上' ); // 売上
	define("TC_SKEI_KBN_GEN"	, '原価' ); // 原価
	define("TC_SKEI_KBN_ARA"	, '粗利' ); // 粗利

	// 請求入金管理
	define("TC_PROC_SAKUCHU"	, '請求書作成中' );
	define("TC_PROC_SUMI"		, '請求書発行済' );
	define("TC_PROC_KANRYO"		, '入金完了' );

	// 契約ステータス
	define("TC_KYKST_KEIYAKU"	, '契約中' );
	define("TC_KYKST_SYURYO"	, '終了' );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsHsUr = new TcHosyuUriage();

	// -----
	// 実行
	// -----
	$clsHsUr->main();


	// クラスを開放する
	$clsHsUr = null;


	/*****************************************************************************/
	/* クラス                                                                    */
	/*****************************************************************************/
	class TcHosyuUriage
	{
	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
		var $shimeKiriDate		= null;	// 請求対象の締日(日付型) 2013.12.21 契約変更チェックの場合は、抽出対象年月とする。

		var $paraShimeKiriDate	= "";	// 請求対象の締切日
		var $paraShimeBi		= "";	// 請求対象の締日
		var $parakrecno			= "";	// 顧客レコード番号
		var $paraSyoriKBN		= "";	// 処理区分

		var $kkkData		= array();	// TC顧客管理データ
		var $snkData		= array();	// TC請求入金管理データ
		var $hskData		= array();	// TC保守管理データ
		var $zenData		= array();	// TC保守管理データ(前月)
		var $sbnData		= array();	// TC保守管理データ(差分)

		var $kijKisDate		= "";		// 計上期間(開始) 締切日の前月締日翌日
		var $kijSroDate		= "";		// 計上期間(終了) 締切日
		var $kijKisDateZen	= "";		// 前回の計上期間(開始) 
		var $kijSroDateZen	= "";		// 前回の計上期間(終了) 

		var $KaisSite		= array();	// 回収予定日算出用配列

	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcHosyuUriage() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// ------------------------------------------------
			// パラメタ設定
			// ------------------------------------------------
			$this->paraShimeKiriDate 	= trim( $_GET["shimeKiriDate"]."" );	// 入力年月の1日。
			$this->paraShimeBi 			= trim( $_GET["shimeBi"]."" );
			$this->parakrecno 			= trim( $_GET["krecno"]."" );
			$this->paraSyoriKBN 		= trim( $_GET["SyoriKBN"]."" );

			switch( $this->paraSyoriKBN ) {
				case TC_SYORI_KBN_CHK:		// 売上計上前チェック
				case TC_SYORI_KBN_SEI:		// 請求・入金管理へ保存する

					// ------------------------------------------------
					// 締切日、計上期間の設定(締日基準)
					// ------------------------------------------------
					// 締切日 末日対応
					$wkSkb = $this->strShimebiTaio( $this->paraShimeKiriDate , $this->paraShimeBi );
					// 日付妥当性チェック
					if( date( "Y-m", strtotime( $wkSkb ) ) == date( "Y-m", strtotime( $this->paraShimeKiriDate ) ) ) {
						$this->paraShimeKiriDate = $wkSkb;
					} else {
						echo "'$wk<br>$wkSkb は存在しない日付です。<br>締切年月、締日を確認してください。'";
						return;
					}

					$this->shimeKiriDate = new DateTime( $this->paraShimeKiriDate );

					// 前月1日と前月締日翌日の年月を比較する。
					$prDate = new DateTime( $this->shimeKiriDate->format("Y-m-1") );
					$prDate->modify("-1 months");		// 前月1日
					$ckDate = new DateTime( $prDate->format("Y-m-").$this->paraShimeBi );
					$ckDate->modify("+1 days");			// 前月締日翌日
					// 日付チェック
					if( $prDate->format("Ym") ==  $ckDate->format("Ym") ) {
						$this->kijKisDate = clone $ckDate;
					} else {
						$this->kijKisDate = new DateTime( $this->shimeKiriDate->format("Y-m-1") );
					}
					// 計上期間 終了
					$this->kijSroDate	= clone $this->shimeKiriDate;

					// ------------------------------
					// 前回計上期間 終了
					$this->kijSroDateZen = clone $this->kijKisDate;
					$this->kijSroDateZen->modify("-1 days");			// 計上日開始の前日

					// 前回計上期間 開始
					$prDate = new DateTime( $this->kijSroDateZen->format("Y-m-1") );
					$prDate->modify("-1 months");		// 前月1日
					$ckDate = new DateTime( $prDate->format("Y-m-").$this->paraShimeBi );
					$ckDate->modify("+1 days");			// 前月締日翌日
					// 日付チェック
					if( $prDate->format("Ym") ==  $ckDate->format("Ym") ) {
						$this->kijKisDateZen = clone $ckDate;
					} else {
						$this->kijKisDateZen = new DateTime( $this->kijSroDateZen->format("Y-m-1") );
					}
					break;

				case TC_SYORI_KBN_SBN:		// 契約変更点チェック	2013.12.21 締日でデータを抽出しない。
					$this->shimeKiriDate = new DateTime( $this->paraShimeKiriDate );
					break;

				case TC_SYORI_KBN_NEW:		// 最新保守作成
					$this->shimeKiriDate = new DateTime();	// 処理時間時点で最新
					break;
			
				case TC_SYORI_KBN_ZEN:		// 前回締切日の取得
					echo json_encode( $this->getZenkaiSKB() );
					return;
					break;
			}

			// ------------------------------------------------
			// 回収予定日算出用
			// ------------------------------------------------
			$this->KaisSite['当月'] 	= 0;
			$this->KaisSite['翌月'] 	= 1;
			$this->KaisSite['翌々月'] 	= 2;

			// ------------------------------------------------
			// 処理開始
			// ------------------------------------------------
			$this->kkkData = array();
			$this->snkData = array();
			$this->hskData = array();
			$this->zenData = array();
			$this->sbnData = array();

			switch( $this->paraSyoriKBN ) {
				case TC_SYORI_KBN_CHK:					// ◆売上計上前チェック◆
					$this->getKKK();					// 顧客管理
					$this->getSNK();					// 請求入金管理
					$this->getHSK();					// 保守管理

					$this->sknKKK();					// 請求なし顧客を追加
					echo json_encode( $this->hskData );	// データを戻す
					break;

				case TC_SYORI_KBN_SBN:					// ◆契約変更点チェック◆
					$this->getKKK();					// 顧客管理
					$this->getHSK();					// 保守管理

					echo json_encode( $this->sbnData );	// データを戻す(差分)
					break;

				case TC_SYORI_KBN_SEI:					// ◆請求・入金管理へ保存する◆
					$this->getKKK();					// 顧客管理
					$this->getSNK();					// 請求入金管理
					$this->getHSK();					// 保守管理

					echo $this->saveSeiNyu();			// データを更新する
					break;

				case TC_SYORI_KBN_NEW:					// ◆最新保守作成◆
					echo "<html>\n";
					echo "<meta http-equiv='content-type' content='text/html; charset=UTF-8'>\n";
					echo "<head></head>\n";
					echo "<body>\n";
					echo "保守管理から有効な明細を抽出し、<br>";
					echo "保守管理(最新明細)データを作成します。<br>";
					echo "<br>\n";
					echo "処理開始…<br><br>\n";
					echo str_pad(" " , 256);
					flush();

					$this->getHSKall();					// 最新明細データを作成する
					$this->saveHSKRnew();				// データを更新する

					echo "<br>";
					echo "処理が終了しました。<br>";
					echo "<br>";
					echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
					echo "</body></html>";
					break;
			}

		}

		/*************************************************************************/
	    /* 「請求入金データ」をから最新の締切日を取得する	   		             */
	    /*  引数	                                                             */
	    /*  関数値  日付(YYYY-MM-DD)              								 */
	    /*************************************************************************/
		function getZenkaiSKB() {
			$ret = "";

			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;		// TC請求入金管理
			$k->arySelFields	= array( "レコード番号" , "締切日" , "締め日" );

			// 条件設定(顧客データから条件を作成する)
		    $k->strQuery = "( 保守レコード番号 > 0 ) order by 締切日 desc limit 1";
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// 件数をチェックする。
			if( $k->intDataCount == 0 ) {
				$ret = null;
			} else {
				$ret = $json->records[0];
			}

			return( $ret );
		}

		/*************************************************************************/
	    /* 顧客管理から、締日に合う顧客を抽出する                                */
	    /*  引数	                                                             */
	    /*  関数値  array			正常終了:データ、異常終了:false              */
	    /*************************************************************************/
		function getKKK() {

			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;		// TC顧客管理
			$k->arySelFields	= array( "レコード番号","顧客名","郵便番号","住所１","住所２","締め日","回収サイト","回収方法","回収日","回収条件" );

			// 条件設定
			$qry = array();

			if( $this->paraShimeBi == "" ) {
				//
			} else {
				$qry[] = "締め日 = ".$this->paraShimeBi;
			}
			if( $this->parakrecno == "" ) {
				//
			} else {
				$qry[] = "レコード番号 = \"".$this->parakrecno."\"";
			}

			$wkQry = implode( $qry , " and " );
			if( $wkQry == "" ) {
			} else {
				$wkQry .= " and ";
			}

			$recno = 0;
			do {
			    $k->strQuery = $wkQry." ( レコード番号 > $recno ) order by レコード番号 asc";
				$json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->strHttpCode == 200 ) {
					// 正常
				} else {
					$errmsg = "抽出エラー -> CODE：".$k->strHttpCode;
					break;
				}

				// 件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $json->records as $key => $rec ) {
					$this->kkkData[ $rec->レコード番号->value ] = $rec;
				}
			} while( $k->intDataCount > 0 );

		}


		/*************************************************************************/
	    /* 締日で絞り込んだ顧客の「請求入金データ」を取得する	                 */
	    /*  引数	                                                             */
	    /*  関数値  array			正常終了:データ、異常終了:false              */
	    /*************************************************************************/
		function getSNK() {

			// 保守管理の顧客レコード番号をキーに請求・入金データを取得する
			$aryhs = $this->makeMaxArray( array_keys($this->kkkData) , CY_SEL_MAX );
			$arySNRecNo	= array();	// 請求入金のレコード番号、顧客レコード番号
			
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;		// TC請求入金管理
			$k->arySelFields	= array( "レコード番号", "顧客レコード番号" , "プロセス" , "締切日" , "保守合計" , "保守明細テーブル" );

			foreach( $aryhs as $kkkRecNo ) {
				$recno = 0;
				do {
					// 条件設定(顧客データから条件を作成する)
					$qry = array();
					$qry[] = "( 顧客レコード番号 in (".implode( $kkkRecNo , "," ).") )";
					
					if( $this->paraShimeKiriDate == "" ) {
						//
					} else {
						$qry[] = "( 締切日 = \"".$this->paraShimeKiriDate."\" )";
					}
					$qry[] = "( レコード番号 > $recno )";
				    $k->strQuery = implode( $qry , " and " )." order by レコード番号 asc";
					$json = $k->runCURLEXEC( TC_MODE_SEL );

					// 件数をチェックする。
					if( $k->intDataCount == 0 ) {
						break;
					}
					// 次に読み込む条件の設定
					$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

					// レコード番号を取得する
					foreach( $json->records as $rec ) {
						// 同一締切日を顧客ごとにまとめる
						$this->snkData[ $rec->顧客レコード番号->value ][] = $rec;
					}
				} while( $k->intDataCount > 0 );
			}

		}


		/*************************************************************************/
	    /* 保守管理から、処理対象を抽出する(締日で絞り込んだ顧客)                */
	    /*  引数	                                                             */
	    /*  関数値  array			正常終了:データ、異常終了:false              */
	    /*************************************************************************/
		function getHSK() {
			$arykkk = array();

			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;		// TC保守管理
			$k->arySelFields	= array( "レコード番号","顧客レコード番号","契約開始日","契約終了日","契約数","契約単位","保守契約明細テーブル" );

			// ----------------------------------------
			// 保守管理を読み込む(読み込み件数制限ごと)
			// ----------------------------------------
			$arykkk = $this->makeMaxArray( array_keys($this->kkkData) , CY_SEL_MAX );
			foreach( $arykkk as $val ) {
				// データ読込、設定
				$this->loadHSK( $k , "顧客レコード番号 in (".implode( $val , "," ).") and" );
			}
		}

		/*************************************************************************/
	    /* 保守管理から、処理対象を抽出する(全件)                                */
	    /*  引数	                                                             */
	    /*  関数値  array			正常終了:データ、異常終了:false              */
	    /*************************************************************************/
		function getHSKall() {
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;		// TC保守管理
			$k->arySelFields	= array( "レコード番号","顧客レコード番号","契約開始日","契約終了日","契約数","契約単位","保守契約明細テーブル" );

			// 保守管理を全て読込む
			$this->loadHSK( $k , "" );

		}

		/*************************************************************************/
	    /* 保守管理から、処理対象を抽出する                                      */
	    /*  引数	$pK		kintoneクラス                                        */
	    /*  		$pQry	クエリ条件                                       	 */
	    /*  関数値  array			正常終了:データ、異常終了:false              */
	    /*************************************************************************/
		function loadHSK( &$pK , $pQry ) {
			// ----------------------------------------
			// 保守管理を読み込む(読み込み件数制限ごと)
			// ----------------------------------------
			$recno = 0;

			do {
				// 条件設定(顧客データから条件を作成する)
			    $pK->strQuery = $pQry." ( 契約ステータス in (\"".TC_KYKST_KEIYAKU."\") ) and ( レコード番号 > $recno ) order by レコード番号 asc";
				$json = $pK->runCURLEXEC( TC_MODE_SEL );

				// 件数をチェックする。
				if( $pK->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $json->records[ $pK->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $json->records as $rec ) {
					if( count( $rec->保守契約明細テーブル->value ) == 0 ) {
						continue;
					}
					// ----------------------------
					// 保守請求対象のチェック、設定
					// ----------------------------
					switch( $this->paraSyoriKBN ) {
						case TC_SYORI_KBN_SBN:
							// 差分
							$this->makeHsSbnData( $rec );
							break;

						case TC_SYORI_KBN_NEW:
							// 最新保守作成
							$this->makeHsNewData( $rec );
							break;

						default:
							$this->makeHsData( $rec );
							break;

					}
				}

			} while( $pK->intDataCount > 0 );
		}

		/*************************************************************************/
	    /* 保守管理から、請求・入金データを作成する								 */
	    /*  引数	保守管理レコード                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function makeHsData( &$pRec ) {
			$ret = false;
			$aryHsData = array();

			// ------------------------------
			// 全体の契約日をチェック
			// ------------------------------
			if( $pRec->契約開始日->value == "" ) {
				$kykDateStr = "";
			} else {
				$kykDateStr = new DateTime( $pRec->契約開始日->value );
				// 契約開始日が締切日(計上終了日)以降。
				if( $kykDateStr > $this->kijSroDate ) {
					return( $ret );
				}
			}
			//
			if( $pRec->契約終了日->value == "" ) {
				$kykDateEnd = clone $this->shimeKiriDate;
			} else {
				$kykDateEnd = new DateTime( $pRec->契約終了日->value );
			}
			// 終了済み(計上開始日以前)は計算しない。
			if( $kykDateEnd < $this->kijKisDate ) {
				return( $ret );
			}

			// ---------------------------------------------------------------------
			// 契約更新月の算出し、締日月と同じであれば請求対象とする。
			// 積算数によって締日月が請求対象外の可能性があるので、
			// 契約開始日から締日月まで計算を繰り返して、判断する。
			//
			// 数量、積算数 0は 摘要の可能性があるので、計算対象(有効データ)とする。
			// ---------------------------------------------------------------------
			$hsKingakuKei = 0;		// 保守金額計算用
			$pHsTbl = $pRec->保守契約明細テーブル->value;
			foreach( $pHsTbl as $val ) {
				$mei = &$val->value;

				// 開始日がない場合は計算しない
				if( trim( $pRec->契約開始日->value.$mei->開始日->value ) == "" ) {
					continue;
				}

				// 計算用日付の設定
				if( $mei->開始日->value == "" ) {
					$strDate = clone $kykDateStr;
				} else {
					$strDate = new DateTime( $mei->開始日->value );
				}
				//
				if( $mei->終了日->value == "" ) {
					$endDate = clone $kykDateEnd;
				} else {
					$endDate = new DateTime( $mei->終了日->value );
				}

				// 開始終了計算
				$idxDate 	= clone $strDate;
				$idxDateDay = $strDate->format("d");

				$endCnt = ( $endDate->format("Y") -  $strDate->format("Y") ) * 12 + ( $endDate->format("m") - $strDate->format("m") + 1 ) + 1;
				$sekisan = $mei->積算数->value;
				if( $mei->積算単位->value == TC_SEK_TNI_NEN ) {
					$sekisan = $sekisan * 12;
				}
				//
				for( $cnt=0; $cnt <= $endCnt; $cnt = $cnt + $sekisan ) {
					// 計算日が計上日開始～計上日終了(締切日)の範囲内。
					if( ( $this->kijKisDate <= $idxDate ) && ( $idxDate <= $this->kijSroDate ) ) {
						$wk = &$aryHsData[ $mei->表示順->value ]->value;
						$wk = new stdClass;
						// 請求・入金用のテーブル項目に変更
						$wk->表示順					= $this->valEnc( $mei->表示順->value );
						$wk->日付_保守_				= $this->valEnc( $this->paraShimeKiriDate );
						$wk->商品・サービス名_保守_	= $this->valEnc( $mei->商品・サービス名->value );
						$wk->商品詳細_保守_			= $this->valEnc( $mei->商品詳細->value );
						$wk->数量_保守_				= $this->valEnc( $mei->数量->value );
						$wk->単位_保守_				= $this->valEnc( $mei->単位->value );
						$wk->単価_保守_				= $this->valEnc( $mei->売単価->value );
						$wk->摘要_保守_				= $this->valEnc( $mei->摘要->value );
						$wk->金額_保守__0			= $this->valEnc( $mei->売上金額->value );
						$wk->詳細情報_保守_			= $this->valEnc( "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHSKR."/show#record=".$pRec->レコード番号->value );

						// 保守管理(最新明細)用
						$wk->ステータス				= $this->valEnc( $mei->ステータス１->value );
						$wk->原単価					= $this->valEnc( $mei->原単価->value );
						$wk->仕入金額				= $this->valEnc( $mei->仕入金額->value );
						$wk->粗利金額				= $this->valEnc( $mei->粗利金額->value );
						$wk->積算数					= $this->valEnc( $mei->積算数->value );
						$wk->積算単位				= $this->valEnc( $mei->積算単位->value );
						$wk->開始日					= $this->valEnc( $mei->開始日->value );
						$wk->終了日					= $this->valEnc( $mei->終了日->value );

						// 保守金額集計
						$hsKingakuKei = $hsKingakuKei + $mei->売上金額->value;

						break;
					}
					// 次の集計年月計算用(○ヶ月単位)
					$idxDate = $this->NextMonth( $idxDate , $idxDateDay , $sekisan );

					// 計上日終了(締切日)以降は計算を終わる
					if( $idxDate > $endDate ) {
						break;
					}
				}
			}
			// 計算対象のデータが無い
			if( count( $aryHsData ) == 0 ) {
				return( $ret );
			}

			// ------------------------------
			// 請求・入金用データの生成
			// ------------------------------
			$kkkRecNo 	= $pRec->顧客レコード番号->value;
			$wkKdat		= &$this->kkkData[ $kkkRecNo ];

			//
			$nykYti_y 	= $this->shimeKiriDate->format( 'Y' );
			$nykYti_m	= $this->shimeKiriDate->format( 'm' ) + $this->KaisSite[ $wkKdat->回収サイト->value ];
			$nykYti_d 	= $wkKdat->回収日->value;
			// 末日対応：回収予定日
			if( $nykYti_d == 31 ) {
				$nykYti = date( 'Y-m-d' , mktime( 0 ,0 ,0 , $nykYti_m + 1 , 0 , $nykYti_y) ); 
			} else {
				$nykYti	= date( 'Y-m-d' , mktime( 0 ,0 ,0 , $nykYti_m , $nykYti_d , $nykYti_y) );
			}

			// 請求チェック用
			$snSaveKbn 		= null;
			$snKeiZumiKin 	= 0;
			$snRecNo 		= 0;
			$chkSNKrec = &$this->snkData[ $kkkRecNo ];

			switch( count( $chkSNKrec ) ) {
				case 0;		// データ無し
					$snSaveKbn		= TC_MODE_INS;	// 追加
					$snKeiZumiKin 	= 0;
					$snRecNo		= 0;			// 請求入金レコード番号
					break;

				case 1: 	// 1件あり
					// プロセスチェック
					if( $chkSNKrec[0]->プロセス->value == TC_PROC_SAKUCHU ) {
						// 作成中なら更新対象にする。
						$snSaveKbn	= TC_MODE_UPD;
					} else {
						$snSaveKbn	= null;
					}
					$snKeiZumiKin	= $chkSNKrec[0]->保守合計->value;
					$snRecNo		= $chkSNKrec[0]->レコード番号->value;	// 請求入金レコード番号
					break;

				default:	// 複数件(手入力データあり)
					// 作成中以外がある場合は、更新対象外とする。
					foreach( $chkSNKrec as $valSNK ) {
						// 請求入金レコード番号
						$snRecNo = $valSNK->レコード番号->value;

						switch( $valSNK->プロセス->value ) {

							case TC_PROC_SAKUCHU:	// 請求書作成中
								$snSaveKbn		= TC_MODE_UPD;
								$snKeiZumiKin	= $valSNK->保守合計->value;
								break;

							case TC_PROC_SUMI:		// 請求書発行済
							case TC_PROC_KANRYO:	// 入金完了
								$snSaveKbn		= null;
								$snKeiZumiKin	= $valSNK->保守合計->value;
								break(2);

							default:
								break;
						}
					}
					break;
			}
			// 請求入金管理のURL
			if( $snRecNo > 0 ) {
				$snURL = "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCSNKK."/show#record=".$snRecNo;
			} else {
				$snURL = "";
			}

			//
			$wk = &$this->hskData[ $pRec->顧客レコード番号->value ];
			$wk = new stdClass;
			$wk->顧客レコード番号	= $this->valEnc( $kkkRecNo );
			$wk->請求先名			= $this->valEnc( $wkKdat->顧客名->value );
			$wk->郵便番号			= $this->valEnc( $wkKdat->郵便番号->value );
			$wk->住所１				= $this->valEnc( $wkKdat->住所１->value );
			$wk->住所２				= $this->valEnc( $wkKdat->住所２->value );
			$wk->締め日				= $this->valEnc( $wkKdat->締め日->value );
			$wk->回収サイト			= $this->valEnc( $wkKdat->回収サイト->value );
			$wk->回収方法			= $this->valEnc( $wkKdat->回収方法->value );
			$wk->回収日				= $this->valEnc( $wkKdat->回収日->value );
			$wk->回収条件			= $this->valEnc( $wkKdat->回収条件->value );
			//
			$wk->保守合計			= $this->valEnc( $hsKingakuKei );
			$wk->締切日				= $this->valEnc( $this->paraShimeKiriDate );
			$wk->入金予定日			= $this->valEnc( $nykYti );
			$wk->保守詳細情報		= $this->valEnc( "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHSKR."/show#record=".$pRec->レコード番号->value );
			$wk->保守レコード番号	= $this->valEnc( $pRec->レコード番号->value );
			//
			$wk->請求入金レコード番号	= $this->valEnc( $snRecNo );
			$wk->請求入金保存区分		= $this->valEnc( $snSaveKbn );
			$wk->計上済み金額			= $this->valEnc( $snKeiZumiKin );
			$wk->請求入金詳細情報		= $this->valEnc( $snURL );
			//
			asort($aryHsData);		// 表示順で並び替える
			$wk->保守明細テーブル->value = $aryHsData;

		}

		/*************************************************************************/
	    /* 保守管理から、変更のあった明細を取得する								 */
	    /*  引数	保守管理レコード                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function makeHsSbnData( &$pRec ) {
			$ret = false;
			$aryHsData = array();

			// ------------------------------
			// 全体の契約日をチェック
			// ------------------------------
/* 2013.12.21 全体の契約開始、終了日は見ない。印刷用の項目になった。
			if( $pRec->契約開始日->value == "" ) {
				$kykDateStr = "";
			} else {
				$kykDateStr = new DateTime( $pRec->契約開始日->value );
				// 契約開始日が締切日(計上終了日)以降。
				if( $kykDateStr > $this->kijSroDate ) {
					return( $ret );
				}
			}
			//
			if( $pRec->契約終了日->value == "" ) {
				$kykDateEnd = clone $this->shimeKiriDate;
			} else {
				$kykDateEnd = new DateTime( $pRec->契約終了日->value );
			}
			// 終了済み(計上開始日以前)は計算しない。
			if( $kykDateEnd < $this->kijKisDateZen ) {
				return( $ret );
			}
*/
			// ---------------------------------------------------------------------
			// 契約更新月の算出し、締日月と同じであれば請求対象とする。
			// 積算数によって締日月が請求対象外の可能性があるので、
			// 契約開始日から締日月まで計算を繰り返して、判断する。
			// ---------------------------------------------------------------------
			$hsKingakuKei = 0;		// 保守金額計算用
			$pHsTbl = $pRec->保守契約明細テーブル->value;

			foreach( $pHsTbl as $val ) {
				$mei = &$val->value;

				// 開始日がない場合は計算しない
				if( trim( $mei->開始日->value.$mei->終了日->value ) == "" ) {
					continue;
				}

				// 開始、終了日が範囲外あれば対象外
				$flg = false;
/*
				// 前月終了したもの
				if( ( $this->kijKisDateZen->format("Y-m-d") <= $mei->終了日->value ) && ( $mei->終了日->value <= $this->kijSroDateZen->format("Y-m-d") ) ) {
					$flg = true;
				}
*/
/* 2013.12.21 締日処理をしないようにする。
				// 当月開始・終了したもの
				if( ( $this->kijKisDate->format("Y-m-d") <= $mei->開始日->value ) && ( $mei->開始日->value <= $this->kijSroDate->format("Y-m-d") ) ) {
					$flg = true;
				}
				if( ( $this->kijKisDate->format("Y-m-d") <= $mei->終了日->value ) && ( $mei->終了日->value <= $this->kijSroDate->format("Y-m-d") ) ) {
					$flg = true;
				}
				if( $flg == false ) {
					continue;
				}
*/
				if( trim( $mei->開始日->value ) == "" ) {
					$wkStr = "";
				} else {
					$wkStr = date( "Y-m", strtotime( $mei->開始日->value ) );
				}
				if( trim( $mei->終了日->value ) == "" ) {
					$wkEnd = "";
				} else {
					$wkEnd = date( "Y-m", strtotime( $mei->終了日->value ) );
				}

				if( $this->shimeKiriDate->format("Y-m") == $wkStr ) {
					$flg = true;
				}
				if( $this->shimeKiriDate->format("Y-m") == $wkEnd ) {
					$flg = true;
				}
				if( $flg == false ) {
					continue;
				}

				// 数量が無いデータは対象外
				if( ( $mei->数量->value - 0 ) == 0 ) {
					continue;
				}

				$wk = &$aryHsData[ $mei->表示順->value ]->value;
				$wk = new stdClass;
				// 請求・入金用のテーブル項目に変更
				$wk->表示順					= $this->valEnc( $mei->表示順->value );
				$wk->日付_保守_				= $this->valEnc( $this->paraShimeKiriDate );
				$wk->商品・サービス名_保守_	= $this->valEnc( $mei->商品・サービス名->value );
				$wk->商品詳細_保守_			= $this->valEnc( $mei->商品詳細->value );
				$wk->数量_保守_				= $this->valEnc( $mei->数量->value );
				$wk->単位_保守_				= $this->valEnc( $mei->単位->value );
				$wk->単価_保守_				= $this->valEnc( $mei->売単価->value );
				$wk->摘要_保守_				= $this->valEnc( $mei->摘要->value );
				$wk->金額_保守__0			= $this->valEnc( $mei->売上金額->value );
/*
20190523 kintone側から項目削除(m.t)
				$wk->予約_開始日_			= $this->valEnc( $mei->予約_開始日_->value );	// 2013.12.22
				$wk->予約_終了日_			= $this->valEnc( $mei->予約_終了日_->value );	// 2013.12.22
				$wk->後請求					= $this->valEnc( $mei->後請求->value );			// 2013.12.22
*/
				// 保守管理(最新明細)用
				$wk->ステータス				= $this->valEnc( $mei->ステータス１->value );
				$wk->原単価					= $this->valEnc( $mei->原単価->value );
				$wk->仕入金額				= $this->valEnc( $mei->仕入金額->value );
				$wk->粗利金額				= $this->valEnc( $mei->粗利金額->value );
				$wk->積算数					= $this->valEnc( $mei->積算数->value );
				$wk->積算単位				= $this->valEnc( $mei->積算単位->value );
				$wk->開始日					= $this->valEnc( $mei->開始日->value );
				$wk->終了日					= $this->valEnc( $mei->終了日->value );

			}

			// 計算対象のデータが無い
			if( count( $aryHsData ) == 0 ) {
				return( $ret );
			}

			// ------------------------------
			// 請求・入金用データの生成
			// ------------------------------
			$kkkRecNo 	= $pRec->顧客レコード番号->value;
			$wkKdat		= &$this->kkkData[ $kkkRecNo ];
			//
			$wk = &$this->sbnData[ $pRec->顧客レコード番号->value ];
			$wk = new stdClass;
			$wk->顧客レコード番号	= $this->valEnc( $kkkRecNo );
			$wk->請求先名			= $this->valEnc( $wkKdat->顧客名->value );
			$wk->郵便番号			= $this->valEnc( $wkKdat->郵便番号->value );
			$wk->住所１				= $this->valEnc( $wkKdat->住所１->value );
			$wk->住所２				= $this->valEnc( $wkKdat->住所２->value );
			$wk->締め日				= $this->valEnc( $wkKdat->締め日->value );
			$wk->回収サイト			= $this->valEnc( $wkKdat->回収サイト->value );
			$wk->回収日				= $this->valEnc( $wkKdat->回収日->value );
			//
			$wk->締切日				= $this->valEnc( $this->paraShimeKiriDate );
			$wk->保守詳細情報		= $this->valEnc( "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHSKR."/show#record=".$pRec->レコード番号->value );
			$wk->保守レコード番号	= $this->valEnc( $pRec->レコード番号->value );
			//
			asort($aryHsData);		// 表示順で並び替える
			$wk->保守明細テーブル->value = $aryHsData;

		}

		/*************************************************************************/
	    /* 保守管理の最新お契約のみ取得する										 */
	    /*  引数	保守管理レコード                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function makeHsNewData( &$pRec ) {
			$ret = false;
			$aryHsData = array();

			// ------------------------------
			// 全体の契約日をチェック
			// ------------------------------
// 2016.03.01 全体の契約開始、終了日は見ない。c.komatsu
/*
			if( $pRec->契約開始日->value == "" ) {
				$kykDateStr = new DateTime();
			} else {
				$kykDateStr = new DateTime( $pRec->契約開始日->value );
				// 契約開始日が締切日(計上終了日)以降。
				if( $kykDateStr > $this->shimeKiriDate ) {
					return( $ret );
				}
			}
			//
			if( $pRec->契約終了日->value == "" ) {
				$kykDateEnd = new DateTime();
				$kykDateEnd->modify("+1 days");
			} else {
				$kykDateEnd = new DateTime( $pRec->契約終了日->value );
			}
			// 終了済み(計上開始日以前)は計算しない。
			if( $kykDateEnd < $this->kijKisDateZen ) {
				return( $ret );
			}
*/
			// ---------------------------------------------------------------------
			// 契約開始が本日以前、契約終了日が本日以降のデータを対象とする。
			//
			// 数量、積算数 0は 摘要の可能性があるので、計算対象(有効データ)とする。
			// ---------------------------------------------------------------------
			$hsKingakuKei = 0;		// 保守金額計算用
			$pHsTbl = $pRec->保守契約明細テーブル->value;

			foreach( $pHsTbl as $val ) {
				$mei = &$val->value;

				// 開始日がない場合は計算しない
//				if( trim( $mei->開始日->value.$mei->終了日->value ) == "" ) {
				if( trim( $mei->開始日->value ) == "" ) {
					continue;
				}

				if( $mei->開始日->value == "" ) {
//					$ksdate = clone $kykDateStr;
					$ksdate = new DateTime();
				} else {
					$ksdate = new DateTime( $mei->開始日->value );
				}
				if( $mei->終了日->value == "" ) {
//					$srdate = clone $kykDateEnd;
					$srdate = new DateTime();
					$srdate->modify("+1 days");
				} else {
					$srdate = new DateTime( $mei->終了日->value );
				}

				// 当月開始・終了したもの
				if( ( $ksdate <= $this->shimeKiriDate ) && ( $this->shimeKiriDate < $srdate ) ) {
					//
				} else {
					continue;
				}

				$wk = &$aryHsData[ $mei->表示順->value ]->value;
				$wk = new stdClass;

				$wk->表示順					= $this->valEnc( $mei->表示順->value );
				$wk->日付_保守_				= $this->valEnc( $this->paraShimeKiriDate );
				$wk->商品・サービス名_保守_	= $this->valEnc( $mei->商品・サービス名->value );
				$wk->商品詳細_保守_			= $this->valEnc( $mei->商品詳細->value );
				$wk->数量_保守_				= $this->valEnc( $mei->数量->value );
				$wk->単位_保守_				= $this->valEnc( $mei->単位->value );
				$wk->単価_保守_				= $this->valEnc( $mei->売単価->value );
				$wk->摘要_保守_				= $this->valEnc( $mei->摘要->value );
				$wk->金額_保守__0			= $this->valEnc( $mei->売上金額->value );

				$wk->ステータス				= $this->valEnc( $mei->ステータス１->value );
				$wk->原単価					= $this->valEnc( $mei->原単価->value );
				$wk->仕入金額				= $this->valEnc( $mei->仕入金額->value );
				$wk->粗利金額				= $this->valEnc( $mei->粗利金額->value );
				$wk->積算数					= $this->valEnc( $mei->積算数->value );
				$wk->積算単位				= $this->valEnc( $mei->積算単位->value );
				$wk->開始日					= $this->valEnc( $mei->開始日->value );
				$wk->終了日					= $this->valEnc( $mei->終了日->value );

			}

			// 計算対象のデータが無い
			if( count( $aryHsData ) == 0 ) {
				return( $ret );
			}

			// ------------------------------
			// 請求・入金用データの生成
			// ------------------------------
			$kkkRecNo 	= $pRec->レコード番号->value;
			$wkKdat		= &$this->kkkData[ $kkkRecNo ];
			//
			$wk = &$this->hskData[ $kkkRecNo ];
			$wk = new stdClass;
			$wk->顧客レコード番号	= $this->valEnc( $kkkRecNo );
			$wk->請求先名			= $this->valEnc( $wkKdat->顧客名->value );
			$wk->郵便番号			= $this->valEnc( $wkKdat->郵便番号->value );
			$wk->住所１				= $this->valEnc( $wkKdat->住所１->value );
			$wk->住所２				= $this->valEnc( $wkKdat->住所２->value );
			$wk->締め日				= $this->valEnc( $wkKdat->締め日->value );
			$wk->回収サイト			= $this->valEnc( $wkKdat->回収サイト->value );
			$wk->回収日				= $this->valEnc( $wkKdat->回収日->value );
			//
			$wk->締切日				= $this->valEnc( $this->paraShimeKiriDate );
			$wk->保守詳細情報		= $this->valEnc( "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHSKR."/show#record=".$pRec->レコード番号->value );
			$wk->保守レコード番号	= $this->valEnc( $pRec->レコード番号->value );
			//
			asort($aryHsData);		// 表示順で並び替える
			$wk->保守明細テーブル = new stdClass();
			$wk->保守明細テーブル->value = $aryHsData;
		}

		/*************************************************************************/
	    /* 請求なし顧客を追加する(チェック時のみ呼び出す)                        */
	    /*  引数                                                                 */
	    /*  関数値  										                     */
	    /*************************************************************************/
	    function sknKKK() {
			
			foreach( $this->kkkData as $kkkRecNo => $wkKdat ) {
				if( array_key_exists( $kkkRecNo , $this->hskData ) ) {
					// 保守データが存在するものは処理なし
					continue;
				}

				// 請求なし顧客のデータを作成する。
				$wk = &$this->hskData[ $kkkRecNo ];
				$wk = new stdClass;
				$wk->顧客レコード番号	= $this->valEnc( $kkkRecNo );
				$wk->請求先名			= $this->valEnc( $wkKdat->顧客名->value );
				$wk->郵便番号			= $this->valEnc( $wkKdat->郵便番号->value );
				$wk->住所１				= $this->valEnc( $wkKdat->住所１->value );
				$wk->住所２				= $this->valEnc( $wkKdat->住所２->value );
				$wk->締め日				= $this->valEnc( $wkKdat->締め日->value );
				$wk->回収サイト			= $this->valEnc( $wkKdat->回収サイト->value );
				$wk->回収方法			= $this->valEnc( $wkKdat->回収方法->value );
				$wk->回収日				= $this->valEnc( $wkKdat->回収日->value );
				$wk->回収条件			= $this->valEnc( $wkKdat->回収条件->value );
				//
				$wk->保守合計			= $this->valEnc( 0 );
				$wk->締切日				= $this->valEnc( "" );
				$wk->入金予定日			= $this->valEnc( "" );
				$wk->保守詳細情報		= $this->valEnc( "" );
				$wk->保守レコード番号	= $this->valEnc( 0 );
				//
				$wk->請求入金レコード番号	= $this->valEnc( 0 );
				$wk->請求入金保存区分		= $this->valEnc( "" );
				$wk->計上済み金額			= $this->valEnc( 0 );
				$wk->請求入金詳細情報		= $this->valEnc( "" );
				//
				$wk->保守明細テーブル->value = array();
			}

		}

		/*************************************************************************/
	    /* 請求・入金へ保存する                                                  */
	    /*  引数                                                                 */
	    /*  関数値  boolean		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function saveSeiNyu() {
			$errmsg = "";				// 更新時エラーメッセージ

			// 顧客レコード番号ごとに追加・更新の振り分け。
			$insSN	= array();	// 追加データ
			$updSN	= array();	// 更新データ

			$kensu_all	= 0;			// 計算済み全件数
			$kensu_saku = 0;			// 請求書作成中 件数
			$kensu_sumi = 0;			// 請求書発行済、入金完了 件数

			foreach( array_keys($this->hskData) as $kkkRecno ) {
				// 追加、更新データ作成
				switch( $this->hskData[ $kkkRecno ]->請求入金保存区分->value ) {
					case TC_MODE_INS:
						$insSN[]	= $this->hskData[ $kkkRecno ];
						break;

					case TC_MODE_UPD:
						// 更新
						$recObj = new stdClass;
						$recObj->id							= $this->hskData[ $kkkRecno ]->請求入金レコード番号->value;
						$recObj->record->保守合計			= $this->hskData[ $kkkRecno ]->保守合計;
						$recObj->record->保守詳細情報		= $this->hskData[ $kkkRecno ]->保守詳細情報;
						$recObj->record->保守レコード番号	= $this->hskData[ $kkkRecno ]->保守レコード番号;
		 				$recObj->record->保守明細テーブル	= $this->hskData[ $kkkRecno ]->保守明細テーブル;
						//
						$updSN[]	= $recObj;
						break;
					
					default:
						break;
				}
			}

			$kensu_all	= count($this->hskData);			// 計算済み全件数
			$kensu_saku = count($insSN) + count($updSN);	// 請求書作成中 件数
			$kensu_sumi = $kensu_all - $kensu_saku;			// 請求書発行済、入金完了 件数

			// --------------------------
			// 請求・入金へ「追加」する。
			// --------------------------
			if( count( $insSN ) > 0 ) {
				$aryInsSN = $this->makeMaxArray( $insSN , CY_INS_MAX );
				//
				$k = new TcKintone();
				$k->parInit();									// API連携用のパラメタを初期化する
				$k->intAppID 		= TC_APPID_TCHRRK;			// アプリID
				foreach( $aryInsSN as $key => $val ) {
					$insData 			= new stdClass;
					$insData->app 		= TC_APPID_TCSNKK;
					$insData->records 	= $val;
					$k->aryJson = $insData;						// 追加対象のレコード番号
					$json = $k->runCURLEXEC( TC_MODE_INS );

					if( $k->strHttpCode == 200 ) {
						// 正常
					} else {
						$errmsg = "追加エラー -> CODE：".$k->strHttpCode;
						break;
					}
				}
			}
			// --------------------------
			// 請求・入金へ「更新」する。
			// --------------------------
			if( ( count( $updSN ) > 0 ) && ( $errmsg == "" ) ) {
				$aryUpdSN = $this->makeMaxArray( $updSN , CY_UPD_MAX );
				//
				$k = new TcKintone();
				$k->parInit();									// API連携用のパラメタを初期化する
				$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
				foreach( $aryUpdSN as $key => $val ) {
					$updData 			= new stdClass;
					$updData->app 		= TC_APPID_TCSNKK;
					$updData->records 	= $val;
					$k->aryJson = $updData;						// 更新対象のレコード番号
					$json = $k->runCURLEXEC( TC_MODE_UPD );

					if( $k->strHttpCode == 200 ) {
						// 正常
					} else {
						$errmsg = "更新エラー -> CODE：".$k->strHttpCode;
						break;
					}
				}
			}

			// --------------------------
			// 戻り値メッセージ
			// --------------------------
			$msg   = array();
			if( $errmsg == "" ) {
				// 正常時
				$msg[] = "";
				$msg[] = "<b>計上処理が終了しました。</b>";
				$msg[] = "";
				$msg[] = "■計上対象";
				$msg[] = "　　<b>$kensu_all</b> 件";
				$msg[] = "";
				$msg[] = "■売上計上データ作成・更新 (今回計上したもの)";
				$msg[] = "　　<b>$kensu_saku</b> 件";
				$msg[] = "";
				$msg[] = "■請求書発行済、入金完了 (計上しない)";
				$msg[] = "　　<b>$kensu_sumi</b> 件";
			} else {
				// エラー時
				$msg[] = "";
				$msg[] = "<b>計上処理を中止しました。</b>";
				$msg[] = "";
				$msg[] = "請求入金データ作成時に、エラーが発生しました。";
				$msg[] = "　　<b>$errmsg</b>";
			}
			return( join( "<br>\n" , $msg ) );
		}

		/*************************************************************************/
	    /* 保守管理(最新明細)	                                                 */
	    /*  引数                                                                 */
	    /*  関数値  boolean		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function saveHSKRnew() {
			// ------------------------------------------------
			// 保守管理(最新明細)：一括削除用レコード番号取得
			// ------------------------------------------------
			$aryHSRecNo	= array();

			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSNW;		// TC保守管理(最新明細)
			$k->arySelFields	= array( "レコード番号" );
			$recno = 0;
			do {
				// 条件設定(顧客データから条件を作成する)
			    $k->strQuery = "( レコード番号 > $recno ) order by レコード番号 asc";
				$json = $k->runCURLEXEC( TC_MODE_SEL );

				// 件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				foreach( $json->records as $rec ) {
					$aryHSRecNo[] = $rec->レコード番号->value;
				}

			} while( $k->intDataCount > 0 );

			// ------------------------
			// 保守管理(最新明細)：削除
			// ------------------------
			$arydel = $this->makeMaxArray( $aryHSRecNo , CY_DEL_MAX );
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSNW;			// アプリID
			$k->aryDelRecNo		= array();
			foreach( $arydel as $key => $val ) {
	    		$k->aryDelRecNo = $val;						// 削除対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_DEL );
			}

			// ------------------------------------------------
			// 保守管理(最新明細)
			// ------------------------------------------------
			$insSN	= array();	// 追加データ
			foreach( $this->hskData as $kkkRecno => $rec ) {
				foreach( $rec->保守明細テーブル->value as $key => $val ) {
					$mei = &$val->value;
					$recObj = new stdClass;
					$recObj->表示順					= $this->valEnc( $mei->表示順->value );
					$recObj->ステータス１			= $this->valEnc( $mei->ステータス->value );
					$recObj->商品・サービス名		= $this->valEnc( $mei->商品・サービス名_保守_->value );
					$recObj->商品詳細				= $this->valEnc( $mei->商品詳細_保守_->value );
					$recObj->数量					= $this->valEnc( $mei->数量_保守_->value );
					$recObj->単位					= $this->valEnc( $mei->単位_保守_->value );
					$recObj->売単価					= $this->valEnc( $mei->単価_保守_->value );
					$recObj->売上金額				= $this->valEnc( $mei->金額_保守__0->value );
					$recObj->摘要					= $this->valEnc( $mei->摘要_保守_->value );
					$recObj->原単価					= $this->valEnc( $mei->原単価->value );
					$recObj->仕入金額				= $this->valEnc( $mei->仕入金額->value );
					$recObj->粗利金額				= $this->valEnc( $mei->粗利金額->value );
					$recObj->積算数					= $this->valEnc( $mei->積算数->value );
					$recObj->積算単位				= $this->valEnc( $mei->積算単位->value );
					$recObj->開始日					= $this->valEnc( $mei->開始日->value );
					$recObj->終了日					= $this->valEnc( $mei->終了日->value );
					$recObj->保守管理レコード番号	= $this->valEnc( $rec->保守レコード番号->value );
					//
					$insSN[] = $recObj;
				}
			}

			// --------------------------
			// アプリへ「追加」する。
			// --------------------------
			if( count( $insSN ) > 0 ) {
				$aryInsSN = $this->makeMaxArray( $insSN , CY_INS_MAX );
				//
				$k = new TcKintone();
				$k->parInit();									// API連携用のパラメタを初期化する
				$k->intAppID 		= TC_APPID_TCHSNW;			// アプリID
				foreach( $aryInsSN as $key => $val ) {
					$insData 			= new stdClass;
					$insData->app 		= TC_APPID_TCHSNW;
					$insData->records 	= $val;
					$k->aryJson = $insData;						// 追加対象のレコード番号
					$json = $k->runCURLEXEC( TC_MODE_INS );

					if( $k->strHttpCode == 200 ) {
					} else {
						echo "保守管理(最新明細)の追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
						break;
					}

				}
			}

		}

		/*************************************************************************/
	    /* arrayをMAX件数ごとに分割する。                                        */
	    /*  引数	&$pAry    	処理対象の配列                                   */
	    /*       	$pMax    	分割する件数                                     */
	    /*  関数値  array       $pMaxごとの2次元配列                             */
	    /*************************************************************************/
		function makeMaxArray( &$pAry , $pMax ) {
			$aryRet = array();

			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			foreach( $pAry as $key => $val ) {
				if( isNull($val) ){
					//
				} else {
					$aryRet[ $idxM ][ $idxR ] = $val;
					$idxR += 1;
					if( $idxR == $pMax ) {
						$idxM += 1;
						$idxR = 0;
					}
				}
			}

			return ( $aryRet );
		}

		/*************************************************************************/
	    /* 率の計算をする   				                                     */
	    /*  引数	分子となる数			                                     */
	    /*       	分母となる数			                                     */
	    /*       	少数桁数 				                                     */
	    /*  関数値  率(%)							                             */
	    /*************************************************************************/
		function CalcRitu( $pNum1 , $pNum2 , $pSKeta ) {
			if( $pNum2 == 0 ) {
				$ret = number_format( 0 , $pSKeta );
			} else {
				$ret = number_format( ( ($pNum1 - 0) / ($pNum2 - 0) ) * 100 , $pSKeta , "." , "" );
			}

			return( $ret );
		}

		/*************************************************************************/
	    /* 末日の処理   			;	                                     	 */
	    /*  引数	日付(文字列)			                                     */
	    /*       	締め日					                                     */
	    /*  関数値  処理後の日付(文字列)			                             */
	    /*************************************************************************/
		function strShimebiTaio( $pData , $pShimebi ) {
			$ret = $pData;

			// 締め日対応
			if( $pShimebi == 31 ) {
				$ret = date("Y-m-t", strtotime( $pData ) );
			} else {
				$ret = date("Y-m-", strtotime( $pData ) ).$pShimebi;
			}

			return( $ret );
		}

		/*************************************************************************/
	    /* 積算月単位の処理 			                                     	 */
	    /*  引数	日付(DateTime)			                                     */
	    /*       	日						                                     */
	    /*       	積算単位				                                     */
	    /*  関数値  処理後の日付(DateTime)			                             */
	    /*************************************************************************/
		function NextMonth( $pDate , $pDay , $pSekisan) {
			$ret = null;

			// 翌月1日
			$ntDate = new DateTime( $pDate->format("Y-m-1") );
			$ntDate->modify("+".$pSekisan." months");
			// 翌月の日を締め日にする。締日が末日より大きければ、更に翌月になる。
			$ckDate = new DateTime( $ntDate->format("Y-m-").$pDay );

			if( $ntDate->format("Ym") ==  $ckDate->format("Ym") ) {
				$ret = clone $ckDate;
			} else {
				$ret = new DateTime( $ntDate->format("Y-m-t") );
			}

			return( $ret );
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function getDateNenGetsu( $pDate ) {
			$ret = null;

			if( isNull($val) ){
				//
			} else {
				$ret = date( "Ym", $pDate );
			}

			return ( $ret );
		}

		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			if( is_array($val) ){
				foreach( $val as $key => $encwk ) {
					$wk->value[] = mb_convert_encoding($encwk , "UTF-8", "auto");
				}
			} else {
				$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			}
			return ( $wk );
		}

		function SendUrl( $pUrl) {
			$ret = null;
			$cHand = null;

			// ----------------
			// http通信の初期化
			// ----------------
			$cHand = curl_init( $pUrl );
			if( $cHand ) {
				//
			} else {
				$this->err->setError( ERR_T000 );
				return( $ret );
			}

			$this->aryOptionHeader = array(
				"Host: timeconcier.jp:443" ,
			);
			// オプション設定
			curl_setopt($cHand, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($cHand, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($cHand, CURLOPT_SSLVERSION, 3);
			curl_setopt($cHand, CURLOPT_HEADER, false);
			curl_setopt($cHand, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($cHand, CURLOPT_FAILONERROR, true);
			curl_setopt($cHand, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

			// ------------------
			// HTTP通信を実行する
			// ------------------
			$respons = curl_exec($cHand);

			// HTTPレスポンスのステータスコードを標準出力する
			$strHttpCode = curl_getinfo($cHand , CURLINFO_HTTP_CODE);

			// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
			curl_close($cHand);

			// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
			if ( $strHttpCode == 200 ) {
				$ret = $respons;
			}

			return( $ret );
		}

	}

?>
