<?php
/*****************************************************************************/
/*  エラー情報クラス                                          (Version 1.00) */
/*   クラス名       TcError                                                  */
/*   メンバ変数     $ErrorNo        エラーNo                                 */
/*                  $ErrorMes       エラーメッセージ                         */
/*                  $ErrorPlace     エラー発生個所                           */
/*                  $ErrorList      複数エラー発生時メッセージリスト         */
/*   メンバ関数     getErrorNo      エラーNo取得                             */
/*                  setErrorNo      エラーNo設定                             */
/*   メソッド       mergeError      エラー情報追加設定                       */
/*                  setError        エラー情報設定                           */
/*                  addList         エラーリスト追加設定                     */
/*   作成日         2013/04/08                                               */
/*   更新履歴       20xx/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
require_once("deferror.inc");
require_once("errmes.inc");

// ログ出力する場合
if (TC_ERRLOGOUT) {
    require_once("tclog.php");
}

if (!defined("TCERROR_PHP")) {
    // ２重インクルード防止
    define("TCERROR_PHP", true);

    ///////////////////////////////////////////////////////////////////////////////
    // 定数定義
    ///////////////////////////////////////////////////////////////////////////////
    
    ///////////////////////////////////////////////////////////////////////////////
    // クラス定義
    ///////////////////////////////////////////////////////////////////////////////
    class TcError
    {
        /*************************************************************************/
        /* メンバ変数                                                            */
        /*************************************************************************/
        var $ErrorNo     = 0 ;  // エラーNo
        var $ErrorMes    = "";  // エラーメッセージ
        var $ErrorPlace  = "";  // エラー発生個所
        var $ListErrorNo     ;  // 複数エラー発生時エラーNo
        var $ErrorList;         // 複数エラー発生時メッセージリスト
        var $ListPlace = null;  // 複数エラー発生時エラー発生箇所リスト
        var $ListAsta ;         // 複数エラー発生時エラー発生箇所リスト(*文字設定)
    
        /*************************************************************************/
        /* コンストラクタ                                                        */
        /*  引数    なし                                                         */
        /*************************************************************************/
        function TcError() {
            $this->ListErrorNo = array();
            $this->ErrorList   = array();
            $this->ListPlace   = array();
        }
    
        /*************************************************************************/
        /* メンバ関数                                                            */
        /*************************************************************************/
        // エラーNo
        function getErrorNo() {
            return( $this->ErrorNo );
        }
        function setErrorNo( $arg ) {
            $this->ErrorNo = $arg;
        }
        // エラーメッセージ
        function getErrorMes() {
            return( $this->ErrorMes );
        }
        function setErrorMes( $arg ) {
            $this->ErrorMes = $arg;
        }
        // エラー発生箇所
        function getErrorPlace() {
            return( $this->ErrorPlace );
        }
        function setErrorPlace( $arg ) {
            $this->ErrorPlace = $arg;
        }
        // 複数エラー発生時メッセージリスト
        function getErrorList() {
            return( $this->ErrorList );
        }
        function setErrorList( $arg ) {
            $this->ErrorList = $arg;
        }
        // 複数エラー発生時エラーNoリスト
        function getListErrorNo() {
            return( $this->ListErrorNo );
        }
        // 複数エラー発生時エラー箇所リスト
        function getListPlace() {
            return( $this->ListPlace );
        }
        // エラー情報クリア
        function clearError() {
            $this->ErrorNo     = 0;
            $this->ErrorMes    = "";
            $this->ErrorPace   = "";
            $this->ListErrroNo = array();
            $this->ErrorList   = array();
            $this->ListPlace   = array();
        }

        /*************************************************************************/
        /* メソッド                                                              */
        /*************************************************************************/

        /*************************************************************************/
        /* エラー情報追加設定                                                    */
        /*  引数    $pVo            エラーオブジェクト                           */
        /*  関数値  なし                                                         */
        /*************************************************************************/
        function mergeError( $pVo ) {
            // 単独エラーが発生している場合
            if( ($this->ErrorNo != 0) && ($this->ErrorNo != ERR_LIST) ) {
                // 発生しているエラーをListに移し変える
                $this->ErrorList[] = $this->ErrorMes;
            }
    
            // エラーマージ
            if( $pVo->ErrorNo != ERR_LIST ) {
                // 単独の場合
                $this->ErrorList[] = $pVo->ErrorMes;
            } else {
                // 複数の場合
                for( $i=0; $i<count($pVo->ErrorList); $i++ ) {
                    array_push( $this->ErrorList, $pVo->ErrorList[$i] );
                }
            }
    
            // エラーNo設定
            $this->ErrorNo = ERR_LIST;
        }
    
        /*************************************************************************/
        /* エラー情報設定                                                        */
        /*  引数    $eNo            エラーNo                                     */
        /*          $mes            追加メッセージ                               */
        /*          $mes2           追加メッセージ                               */
        /*  関数値  なし                                                         */
        /*************************************************************************/
        function setError( $eNo, $mes = "", $mes2 = "" ) {
            global $eMes;
    
            if($this->ErrorNo != ERR_LIST) $this->ErrorNo = $eNo;
            $strMes = sprintf( $eMes[$eNo], $mes, $mes2 );
    
            // 一般用エラーメッセージ入替
            if( TC_ERRDEBUG || ($eNo >= ERR_V000) ) {
                $this->ErrorMes = $strMes;
            } else {
                $this->ErrorMes = sprintf( $eMes[ERR_S002], $eNo );
            }
    
            // ログ出力
            if (TC_ERRLOGOUT) {
                $log = new TcLog(TC_ERRLOGFILE);
                $log->writeLog("ENO:$eNo MESSAGE:$strMes");
            }
        }
    
        /*************************************************************************/
        /* エラーリスト追加設定                                                  */
        /*  引数    $eNo            エラーNo                                     */
        /*          $mes            追加メッセージ                               */
        /*          $mes2           追加メッセージ                               */
        /*  関数値  なし                                                         */
        /*************************************************************************/
        function addList( $eNo, $mes = "", $mes2 = "" ) {
            global $eMes;
            $addMes = sprintf( $eMes[$eNo], $mes, $mes2 );
    
            // 一般用エラーメッセージ入替
            if( TC_ERRDEBUG || ($eNo >= ERR_V000) ) {
                $this->ErrorList[] = $addMes;
            } else {
                $this->ErrorList[] = sprintf( $eMes[ERR_S002], $eNo );
            }
            $this->ListErrorNo[] = $eNo;
            $this->ErrorNo = ERR_LIST;
    
    
            // ログ出力
            if (TC_ERRLOGOUT) {
                $log = new ctLog(TC_ERRLOGFILE);
                $log->writeLog("ENO:$eNo MESSAGE:$strMes");
            }
        }
    
        /*************************************************************************/
        /* エラー発生箇所リスト追加                                              */
        /*  引数    $pPlace         発生箇所                                     */
        /*          $eNo            エラーNo                                     */
        /*          $mes            追加メッセージ                               */
        /*          $mes2           追加メッセージ                               */
        /*          $pOpt           通常メッセージを追加するか                   */
        /*  関数値  なし                                                         */
        /*************************************************************************/
        function addPlace($pPlace, $eNo, $mes = "", $mes2 = "", $pOpt = false) {
            global $eMes;
    
            // 一般用エラーメッセージ入替
            if( TC_ERRDEBUG || ($eNo >= ERR_V000) ) {
                $strMes = sprintf( $eMes[$eNo], $mes, $mes2 );
            } else {
                $strMes = sprintf( $eMes[ERR_S002], $eNo );
            }
    
            // すでにセットされていた場合、改行に続けてセット
            if (!isset($this->ListPlace[$pPlace]) || isNull($this->ListPlace[$pPlace])) {
                $this->ListPlace[$pPlace] = $strMes;
            } else {
                $this->ListPlace[$pPlace] .= "\r\n" . $strMes;
            }
            // ※文字設定
            $this->ListAsta[$pPlace] = "※";
            
            // 通常処理
            if ($pOpt) $this->addList($eNo, $mes, $mes2);
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    // エラーハンドラ設定
    ///////////////////////////////////////////////////////////////////////////////
    if (TC_ERRHANDLER) {
        ini_set("display_errors", true);
        error_reporting(TC_ERRHANDLER_LEVEL);
        set_error_handler("TcErrorHandler");
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    // エラーハンドラ関数
    ///////////////////////////////////////////////////////////////////////////////
    function TcErrorHandler($errno, $errstr, $errfile, $errline, $errcontext) {
        // 現在時刻取得
        $strNow = date("Y/m/d H:i:s");
    
        $errType = array (
                            E_ERROR           => "Error",
                            E_WARNING         => "Warning",
                            E_PARSE           => "Parsing Error",
                            E_NOTICE          => "Notice",
                            E_CORE_ERROR      => "Core Error",
                            E_CORE_WARNING    => "Core Warning",
                            E_COMPILE_ERROR   => "Compile Error",
                            E_COMPILE_WARNING => "Compile Warning",
                            E_USER_ERROR      => "User Error",
                            E_USER_WARNING    => "User Warning",
                            E_USER_NOTICE     => "User Notice"
                           );
    
        // エラーメッセージ編集
        $strMess = "エラー種別：" . $errType[$errno] . "<br>\n"
                 . "発生箇所　：" . $errfile . "(" . $errline . "行目)" . "<br>\n"
                 . "メッセージ：" . $errstr . "<br>\n";
        // エラー情報を出力
        print mb_convert_encoding( $strMess, mb_http_output());
    //    print mb_convert_encoding( $strMess, "SJIS", "EUC-JP");
        // エラーハンドラ関数の回復
        if (TC_ERRRESTORE) restore_error_handler();
    }

}
?>
