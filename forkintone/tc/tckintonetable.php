<?php
/*****************************************************************************/
/*  kintoneサブテーブルクラス                                 (Version 1.00) */
/*   クラス名       TcKintoneTable                                           */
/*   メンバ変数     intNumber       取得したレコード数                       */
/*                  recList         取得したレコード情報                     */
/*                  err             エラー情報格納用                         */
/*   メンバ関数     openLog         ログファイルオープン                     */
/*                  writeLog        ログ出力                                 */
/*   メソッド       checkSize       ファイルサイズチェック                   */
/*                  logBackup       ファイルバックアップ                     */
/*                  openLog         ログファイルオープン                     */
/*                  closeLog        ログファイルクローズ                     */
/*                  writeLog        ログ出力                                 */
/*   作成日         2013/04/10      J.Kataoka                                */
/*   更新履歴       20xx/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
include_once("tcerror.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////

class TcKintoneTable
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $intNumber = 0; // 行数
    var $tblList;
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function TcKintoneTable() {
        // 初期化
        $this->tblList = array();
        $this->err = new TcError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/

    /*************************************************************************/
    /* 格納している行数取得                                                  */
    /*  引数    なし                                                         */
    /*************************************************************************/
    function getNumber() {
        return( $this->intNumber );
    }


    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* Kintone → TcKintone                                                  */
    /*  引数    $pTable     行データの配列                                   */
    /*************************************************************************/
    function setTable( $pTable ) {
        $idx = 0;
        // 行データの集合が渡されるので、それを配列に展開
        foreach( $pTable as $idx => $row ) {
//            $tmp = get_object_vars( $row );
            foreach( $row->value as $key => $val ) {
                $this->tblList[$idx][$key] = $val;
            }
            $idx++;
        }

        // 設定した行数
        $this->intNumber = $idx;
    }

    /*************************************************************************/
    /* Kintone → TcKintone                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function setRecord( $pRecord ) {
        $ret = array();
        // フィールドの集合
        foreach( $pRecord as $field => $data ) {
            $tmp = get_object_vars( $field );
            $ret[$field] = $data;
        }
        return($ret);

    }

    /*************************************************************************/
    /* Kintone → TcKintone                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function getRow( $idx ) {
        return($this->tblList[$idx]);
    }

    /*************************************************************************/
    /* 変数の中身を返す関数                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function getValue( $field ) {
        $getvalue = "";

        // フィールド型判断
        switch ($field->type) {
            case "USER_SELECT":
                foreach($field->value as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
                }
                break;
            case "CREATOR":
            case "MODIFIER":

                foreach($field as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
                }

                break;
            case "CHECK_BOX":
            case "MULTI_SELECT":
            case "FILE":
            case "STATUS_ASSIGNEE":
                $ret = "未対応";
                break;
            default:
                $getvalue = $field->value;
                break;
        }
        
        return($getvalue);
    }

    /*************************************************************************/
    /* テーブル内のフィールド値を取得する                                    */
    /*  引数    $idx        行番号(0～)                                      */
    /*          $fieldCD    フィールドコード                                 */
    /*  関数値  取得したフィールド値                                         */
    /*************************************************************************/
    function getFieldValue( $idx, $fieldCD ) {
        // 列データ取得
        $row = $this->getRow( $idx );
        // 値取得
        $val = $this->getValue($row[$fieldCD]);
        
        return($val);
    }

    /*************************************************************************/
    /* テーブル内のフィールドタイプを取得する                                */
    /*  引数    $idx        行番号(0～)                                      */
    /*          $fieldCD    フィールドコード                                 */
    /*  関数値  取得したフィールドタイプ                                     */
    /*************************************************************************/
    function getFieldType( $idx, $fieldCD ) {
        // 列データ取得
        $row = $this->getRow( $idx );
        $typ = $row[$fieldCD]->type;
        
        return($typ);
    }

    /*************************************************************************/
    /* 変数の中身を返す関数                                                  */
    /*  引数    $pTable     テーブルデータ                                   */
    /*          $pColNum    行番号                                           */
    /*************************************************************************/
    function getColum( $pTable, $pColNum ) {
        // 行データ
        $col = $pTable->value[$pColNum]->value;
    }

    /*************************************************************************/
    /* 変数のテーブル項目の値を返す関数                                      */
    /*  引数    $p_string   フィールドコード                                 */
    /*          $p_tbstring テーブル内のフィールドコード                     */
    /*          $p_tbno     取得するテーブル行数                             */
    /*************************************************************************/
    function gettbValue( $p_string, $p_tbstring, $p_tbno ) {
        $gettype = "";
        $gettbvalue = "";
        
        // テーブル情報取得
        $table = $this->getField( $p_string );

    
        if (!empty($p_record->$p_string)) {
            $gettype = $table->value[$p_tbno]->value->$p_tbstring->type;
            if ($p_record->$p_string->type == "SUBTABLE"){
                switch ($gettype) {
                    case "USER_SELECT":
                        $gettbvalue = $p_record->$p_string->value[$p_tbno]->value->$p_tbstring->value[0]->name;
                        break;
                    default:
                        $gettbvalue = $p_record->$p_string->value[$p_tbno]->value->$p_tbstring->value;
                        break;
                }
            }
        }
        return($gettbvalue);
    }

}
