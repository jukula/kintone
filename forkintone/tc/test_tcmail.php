<?php 
	include_once("../tccom/tcalert.php");

	$mailCustomer = false;  // お客様へメール送信するか？
	$mailCustomer = true;   // お客様へメール送信する

	$title  = "タイトル";
	$body   = "送信テスト";
	$mailto = "komatsu@cross-tier.com";

	// メール送信
	$mail = new tcAlert();
	// お客様向け
	if ($mailCustomer) {
	    $mail->setTo( $mailto );
	    $mail->setTitle( $title );
	    $mail->setBody( $body );
	    $res = $mail->sendMail();
	}
?>
