<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tc_web.php                 				                 */
/*   更新履歴   2013/07/16  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcMail();

	// 実行
	$clsSrs->main();


	class TcMail
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcMail() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$ret = false;

			// ----------------------------------------------
			// 名刺管理から対象データを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= 2772;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json[$i]->records[ $k->intDataCount - 1 ]->レコード番号->value;

				for($a = 0; $a < count($mail_json[$i]->records); $a++) {
					$kmk	= $mail_json[$i]->records[$a]->添付;
					$kmkkey = $mail_json[$i]->records[$a]->添付->value;
//					echo($mail_json[$i]->records[$a]->レコード番号->value.":".$mail_json[$i]->records[$a]->文字列->value."<br>");
					if( $kmk->value ) {
						// 新しいファイルキーを取得する
						$retDUnfk = $this->getDwnUpNewFileKey( $kmk , $kmkkey );
						if( $retDUnfk  == "" ) {
							// 正常
							// 新しいファイルキーをセットする
							$mail_json[$i]->records[$a]->添付->value[0]->fileKey = $kmkkey;
						} else {
							// エラー
							echo $retDUnfk."<br>\n 終了。";
							exit;
						}
					}
				}

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			file_put_contents('serialized.txt', serialize($mail_json));
			$array	= (unserialize(file_get_contents('serialized.txt')));
/*
			for($a = 0; $a < count($array[0]->records); $a++) {
				echo($array[0]->records[$a]->レコード番号->value.":".$array[0]->records[$a]->文字列->value."<br>");
			}
*/
			for($a = 0; $a < count($array[0]->records); $a++) {
				// --------------------
				// 更新する。
				// --------------------
				$k = new TcKintone();
				$k->parInit();							// API連携用のパラメタを初期化する
				$k->intAppID 		= 2772;			// アプリID
				$k->strContentType	= "Content-Type: application/json";


				// プロセスが請求書作成中の場合、請求書発行済にする
				$recObj = new stdClass;
				$recObj->id 			= $array[0]->records[$a]->レコード番号->value;
				$recObj->record->文字列 = $this->valEnc( $array[0]->records[$a]->文字列->value );
				$recObj->record->添付   = $array[0]->records[$a]->添付;

				$updData 			= new stdClass;
				$updData->app 		= 2772;
				$updData->records[] = $recObj;
				$k->aryJson = $updData;
				$jsonSNKK = $k->runCURLEXEC( TC_MODE_UPD );
			}
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}

		/*************************************************************************/
	    /* 添付ファイルをダウン->アップし、新filekeyを取得・設定する             */
	    /*  引数	項目情報(参照) 、 レコード情報(参照)                         */
	    /*  関数値  正常："" 、 異常：エラーメッセージ                           */
	    /*************************************************************************/
		function getDwnUpNewFileKey( &$pKmk , &$pKmkFileKey ) {
			$ret = "";

			if( $pKmk->type == "FILE" ) {
			} else {
				return $ret;
			}

			// ダウンロード
			$f = new TcKintone();
			$f->parInit();
			$f->intAppID = 2772;
			$wkFileInfo = array();
			if( is_array($pKmk->value) ) {
				// 複数ファイル有
				$wkFileInfo = $pKmk->value;
			} else {
				// 単一ファイル
				$wkFileInfo[] = $pKmk->value;
			}
			foreach( $wkFileInfo as $key_f => $val_f ) {
				if( $val_f ) {
					$dwndat = $f->runDOWNLOAD( $val_f );
					if( $f->strHttpCode == 200 ) {

						// アップロード
						$filePath = "/home/cross-tier/timeconcier.jp/public_html/forkintone/apicom/tctmp/";
						$u = new TcKintone();
						$u->parInit();
						$u->intAppID = 2772;
						$retFileKey = $u->runUPLOAD( $val_f , $dwndat , $filePath);
						if( $u->strHttpCode == 200 ) {
							// 新filekey
							$pKmkFileKey = $retFileKey->fileKey;
						} else {
							$ret = "ファイルをアップロードできませんでした。 ".$val_f-name." (".$u->strHttpCode.":".$u->message.")";
							break;
						}
					} else {
						$ret = "ファイルをダウンロードできませんでした。 ".$val_f-name." (".$f->strHttpCode.":".$f->message.")";
						break;
					}
				}
			}

			return $ret;
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
