<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>年号年齢早見表</title>

</head>
<body> 
<center><h1>年号年齢早見表</h1> 

<p> 
<div class=top> 
<form action="hayami.php" method="post">
<input id="inyear" TYPE="text" SIZE="9" NAME="inyear" value="<?=$_POST['inyear'];?>">
年時点
<input TYPE="submit" VALUE="表示"> 
</form>
</div> 
</p>
<p>
<div class=table> 
  <table border="1"> 
    <colgroup bgcolor="#e0e0e0" width=50> </colgroup> 
    <colgroup> 
    <col span=1 width=50> 
    <col span=1 width=80> 
    </colgroup> 
    <colgroup bgcolor="#e0e0e0" width=50> </colgroup> 
    <colgroup> 
    <col span=1 width=50> 
    <col span=1 width=80> 
    </colgroup> 
    <colgroup bgcolor="#e0e0e0" width=50> </colgroup> 
    <colgroup> 
    <col span=1 width=50> 
    <col span=1 width=80> 
    </colgroup> 
    <colgroup bgcolor="#e0e0e0" width=50> </colgroup> 
    <colgroup> 
    <col span=1 width=50> 
    <col span=1 width=80> 
    </colgroup> 
    <thead> 
      <tr> 
	<th>年齢</th> 
        <th>西暦</th> 
        <th>元号</th> 
        
	<th>年齢</th> 
        <th>西暦</th> 
        <th>元号</th> 
      
	<th>年齢</th>
        <th>西暦</th> 
        <th>元号</th> 
      
	<th>年齢</th>
        <th>西暦</th> 
        <th>元号</th> 
      </tr> 
    </thead> 

<?php
/*****************************************************************************/
/*   年号年齢早見表PHP		                                  (Version 1.00) */
/*   ファイル名 : hayami.php                                                 */
/*   更新履歴   2013/04/08      Version 1.00(C.Komatsu)                      */
/*                                                                           */
/*   [備考]                                                                  */
/*      					 					                             */
/*   [必要ファイル]                                                          */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	$maxcol=1;
	$count=0;
	$today=getdate();
	
	// 値がPOSTされればそれを使う（無ければ現在の年）
	if($_POST["inyear"] != "" ){
		$NowYear=$_POST["inyear"];
	} else {
		$NowYear=$today['year'];
	}
	$SetYearH1=0;
	$SetYearH2=0;
	$SetYearH3=0;

for($a = ($NowYear); $a >= ($NowYear -29); $a--){
	if($count==0) print "<tr>";
	// 一列目の処理
	$SetYear=$a;

	if ( ($NowYear - $SetYear) >=0){
		$SetYearH3=(($NowYear - $SetYear));
	}
	else {
		$SetYearH3=("-");
	}

	print "<td align='center'>$SetYearH3</td>";
	print "<td align='center'>$SetYear</td>";

	if ($SetYear > 2018){
		$SetYearH1=($SetYear -2018);	
	} else if ($SetYear > 1988){
		$SetYearH1=($SetYear -1988);	
	} else if ($SetYear > 1925){
		$SetYearH1=($SetYear -1925);	
	} else if ($SetYear > 1911){
		$SetYearH1=($SetYear -1911);	
	} else if ($SetYear > 1867){
		$SetYearH1=($SetYear -1867);	
	}

	if ($SetYear > 2018){
		if ($SetYear == 2019){
			$SetYearH2=("*)令和");
		} else {
			$SetYearH2=("令和");
		}	
	} else if ($SetYear > 1988){
		if ($SetYear == 1989){
			$SetYearH2=("*)平成");
		} else {
			$SetYearH2=("平成");
		}	
	} else if ($SetYear > 1925){
		if ($SetYear == 1926){
			$SetYearH2=("*)昭和");
		} else {
			$SetYearH2=("昭和");
		}
	} else if ($SetYear > 1911){
		if ($SetYear == 1912){
			$SetYearH2=("*)大正");
		} else {
			$SetYearH2=("大正");
		}
	} else if ($SetYear > 1867){
			$SetYearH2=("明治");
	}

	print "<td>$SetYearH2 $SetYearH1</td>";

	// 二列目の処理
	$SetYear=$a - 30;

	if ( ($NowYear - $SetYear) >=0){
		$SetYearH3=(($NowYear - $SetYear));
	}
	else {
		$SetYearH3=("-");
	}

	print "<td align='center'>$SetYearH3</td>";

	print "<td align='center'>$SetYear</td>";

	if ($SetYear > 2018){
		$SetYearH1=($SetYear -2018);	
	} else if ($SetYear > 1988){
		$SetYearH1=($SetYear -1988);	
	} else if ($SetYear > 1925){
		$SetYearH1=($SetYear -1925);	
	} else if ($SetYear > 1911){
		$SetYearH1=($SetYear -1911);	
	} else if ($SetYear > 1867){
		$SetYearH1=($SetYear -1867);	
	}

	if ($SetYear > 2018){
		if ($SetYear == 2018){
			$SetYearH2=("*)令和");
		} else {
			$SetYearH2=("令和");
		}	
	} else if ($SetYear > 1988){
		if ($SetYear == 1989){
			$SetYearH2=("*)平成");
		} else {
			$SetYearH2=("平成");
		}	
	} else if ($SetYear > 1925){
		if ($SetYear == 1926){
			$SetYearH2=("*)昭和");
		} else {
			$SetYearH2=("昭和");
		}
	} else if ($SetYear > 1911){
		if ($SetYear == 1912){
			$SetYearH2=("*)大正");
		} else {
			$SetYearH2=("大正");
		}
	} else if ($SetYear > 1867){
			$SetYearH2=("明治");	
	}

	print "<td>$SetYearH2 $SetYearH1</td>";
	// 三列目の処理
	$SetYear=$a -60;
	if ( ($NowYear - $SetYear) >=0){
			$SetYearH3=(($NowYear - $SetYear));
		}
		else {
			$SetYearH3=("-");
		}

	print "<td align='center'>$SetYearH3</td>";

	print "<td align='center'>$SetYear</td>";

	if ($SetYear > 2018){
		$SetYearH1=($SetYear -2018);	
	} else if ($SetYear > 1988){
		$SetYearH1=($SetYear -1988);	
	} else if ($SetYear > 1925){
		$SetYearH1=($SetYear -1925);	
	} else if ($SetYear > 1911){
		$SetYearH1=($SetYear -1911);	
	} else if ($SetYear > 1867){
		$SetYearH1=($SetYear -1867);	
	}

	if ($SetYear > 2018){
		if ($SetYear == 2018){
			$SetYearH2=("*)令和");
		} else {
			$SetYearH2=("令和");
		}	
	} else if ($SetYear > 1988){
		if ($SetYear == 1989){
			$SetYearH2=("*)平成");
		} else {
			$SetYearH2=("平成");
		}	
	} else if ($SetYear > 1925){
		if ($SetYear == 1926){
			$SetYearH2=("*)昭和");
		} else {
			$SetYearH2=("昭和");
		}
	} else if ($SetYear > 1911){
		if ($SetYear == 1912){
			$SetYearH2=("*)大正");
		} else {
			$SetYearH2=("大正");
		}
	} else if ($SetYear > 1867){
			$SetYearH2=("明治");	
	}

	print "<td>$SetYearH2 $SetYearH1</td>";
	// 四列目の処理
	$SetYear=$a-90;
	if ( ($NowYear - $SetYear) >=0){
			$SetYearH3=(($NowYear - $SetYear));
		}
		else {
			$SetYearH3=("-");
		}

	print "<td align='center'>$SetYearH3</td>";

	print "<td align='center'>$SetYear</td>";

	if ($SetYear > 2018){
		$SetYearH1=($SetYear -2018);	
	} else if ($SetYear > 1988){
		$SetYearH1=($SetYear -1988);	
	} else if ($SetYear > 1925){
		$SetYearH1=($SetYear -1925);	
	} else if ($SetYear > 1911){
		$SetYearH1=($SetYear -1911);	
	} else if ($SetYear > 1867){
		$SetYearH1=($SetYear -1867);	
	}

	if ($SetYear > 2018){
		if ($SetYear == 2018){
			$SetYearH2=("*)令和");
		} else {
			$SetYearH2=("令和");
		}	
	} else if ($SetYear > 1988){
		if ($SetYear == 1989){
			$SetYearH2=("*)平成");
		} else {
			$SetYearH2=("平成");
		}	
	} else if ($SetYear > 1925){
		if ($SetYear == 1926){
			$SetYearH2=("*)昭和");
		} else {
			$SetYearH2=("昭和");
		}
	} else if ($SetYear > 1911){
		if ($SetYear == 1912){
			$SetYearH2=("*)大正");
		} else {
			$SetYearH2=("大正");
		}
	} else if ($SetYear > 1867){
		$SetYearH2=("明治");	
	}

	print "<td>$SetYearH2 $SetYearH1</td>";

	if($count==$maxcol -1) print "</tr>\n";
	$count=++$count % $maxcol;
}
while($count!=0 and $count<$maxcol){
print "<td>&nbsp;</td>";
if($count==($maxcol -1)) print "</tr>";
$count++;
}
?>

  </table></center>
</div>
<center>
	<table BORDER=0 CELLSPACING=0 CELLPADDING=6 COLS=1 WIDTH="640" >
	<tr>
	<td align='center'><sup>*)</sup>年号の代わり目の日付については、以下に示します。</td>
	</tr>
	</table>
	<table Border=2 Cellpadding=4 Cellspacing=0 bordercolor="" class="tbl">
		<tr>
			<td BGCOLOR="">2019年</td>
			<td BGCOLOR="">平成31年は4月30日まで</td>
			<td BGCOLOR="">令和元年は5月1日から</td>
		</tr>
		<tr>
			<td BGCOLOR="">1989年</td>
			<td BGCOLOR="">昭和64年は1月7日まで</td>
			<td BGCOLOR="">平成元年は1月8日から</td>
		</tr>
		<tr>
			<td BGCOLOR="">1926年</td>
			<td BGCOLOR="">大正15年は12月24日まで</td>
			<td BGCOLOR="">昭和元年は12月25日から</td>
		</tr>
		<tr>
			<td BGCOLOR="">1912年</td>
			<td BGCOLOR="">明治45年は7月29日まで</td>
			<td BGCOLOR="">大正元年は7月30日から</td>
		</tr>
	</table>
</center>
</body>
</html>
