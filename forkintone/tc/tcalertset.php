<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset.php               				                 */
/*   更新履歴   2014/02/05  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcAlertSet
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数
		var $mailfrom   = "c.komatsu@timeconcier.jp"; // 送信元

		var $AppID = "";            // アプリID 
		var $Query = array();       // SQL文
		var $Body  = "";     	    // メール本文
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAlertSet() {
	        $this->err = new TcError();
	    }
	    /*************************************************************************/
    	/* メンバ関数                                                            */
    	/*************************************************************************/
		// データ差し込み用のアプリIDを取得
	    function setAppID( $arg ) {
	        $this->AppID = $arg;
	    }
	    function getAppID() {
	        return( $this->AppID );
	    }

		// データ取得用のクエリを取得
	    function setQuery( $arg ) {
	        $this->Query = $arg;
	    }
	    function getQuery() {
	        return( $this->Query );
	    }

		// 本文を取得
	    function setBody( $arg ) {
	        $this->Body = $arg;
	    }
	    function getBody() {
	        return( $this->Body );
	    }
		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$ret = false;

			// ----------------------------------------------
			// 名刺管理から対象データを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の0分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json[$i]->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// 差込データ作成
			// ----------------------------------------------
			$i = 0;
			$setbody = "";
			$age     = "";
			$ageDay     = "";
/*
			for($i = 0; $i <= count($mail_json); $i++) {
				for($a = 0; $a < count($mail_json[$i]->records); $a++) {

					if( ($mail_json[$i]->records[$a]->誕生日_年_->value != "") && ($mail_json[$i]->records[$a]->月->value != "") && ($mail_json[$i]->records[$a]->日->value != "")){
						$age = $this->birthToAge($mail_json[$i]->records[$a]->誕生日_年_->value."-".$mail_json[$i]->records[$a]->月->value."-".$mail_json[$i]->records[$a]->日->value ) . "歳";
					}else{
						$age = "";
					}

					if( $mail_json[$i]->records[$a]->日->value != "" ){
						$ageDay = $mail_json[$i]->records[$a]->月->value."月".$mail_json[$i]->records[$a]->日->value . "日";
					}else{
						$ageDay = "";
					}

					$setbody.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."さん ".$ageDay." ".$age."\n";
				}
			}
*/

//echo($setbody."<br>");
			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TEST;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->メールアドレス->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->メールアドレス->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->メールアドレス->value;
						}
					}
				}
			}

			// -------------
			// メールを送信する
			// -------------

			$subject= "TC送信のテスト";    // タイトルの設定
			// 本文の設定
			$mailbody 	= $this->Body."\n";
			$mailbody  .= $setbody; //繰り返し項目
//			$mailbody  .= "-----------------------------------------------------------------\nタイムコンシェル株式会社\nTIME CONCIER Co.,Ltd.\n〒780-8007 高知県高知市仲田町2-11\nTEL：050-3367-5536\n\nタイムコンシェルオフィシャルサイト\nhttp://www.timeconcier.jp\n\n";
//			$mailbody  .= "グランドコンシェル紹介サイト\nhttp://groundconcier.com/\n-----------------------------------------------------------------";

			$sendmail = new tcAlert();
			// お客様向け
			    $sendmail->setTo( $email );
			    $sendmail->setTitle( $subject );
			    $sendmail->setBody( $mailbody );
			    $res = $sendmail->sendMail();

			return $ret;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
