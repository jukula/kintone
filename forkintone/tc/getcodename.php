<?php
/*****************************************************************************/
/* JaveScriptで使用。コードから名称を取得する。				  (Version 1.00) */
/*   ファイル名 : getcodename.php                                            */
/*   更新履歴   2013/10/04  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsHsUr = new TcShimebicheck();

	// -----
	// 実行
	// -----
	$clsHsUr->pAPPID 	= $_REQUEST['APPID'];
	$clsHsUr->pKEY 		= $_REQUEST['KEY'];
	$clsHsUr->pVAL 		= $_REQUEST['VAL'];

	print_r( $clsHsUr->main() );

	// クラスを開放する
	$clsHsUr = null;


	/*****************************************************************************/
	/* クラス                                                                    */
	/*****************************************************************************/
	class TcShimebicheck
	{
	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $pAPPID		= ""; 	// アプリID
		var $pKEY		= "";	// 検索項目
		var $pVAL		= "";	// 検索値

	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcShimebicheck() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// ------------------------------------------------
			// TC保守管理の顧客を読み込む
			// ------------------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= $this->pAPPID;		// アプリID
			$k->arySelFileds	= array();

			// 取得件数制限ごとにループして処理を行う。
		    $k->strQuery = $this->pKEY." = ".$this->pVAL;
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// 検索結果
			if( $k->intDataCount == 0 ) {
				return ( "null" );
			} else {
				return ( json_encode($json->records) );
			}
		}
	}

?>
