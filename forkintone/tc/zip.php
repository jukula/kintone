<?php
/*****************************************************************************/
/*   住所取得PHP                                              (Version 1.00) */
/*   ファイル名 : zip.php			                                         */
/*   更新履歴   2014/07/23  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*   [必要ファイル]                                                          */
/*      jigyosyo.csv / ken_all.csv 										     */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	// kinntoneにパラメータを返す場合に使用する
	header("Access-Control-Allow-Origin: *");
    header("Content-Type:text/html;charset=utf-8");

    mb_language("Japanese");


	$flg_error = FALSE;
	$result = FALSE;

	if ($format == '') {
	    $format = 'x';
	}

	// 郵便番号
	$zip_code = '';
	if (array_key_exists('zipNo', $_REQUEST)) {
	    $zip_code = $_REQUEST['zipNo'];
		$zip_code = str_replace("-","",$zip_code);
	    if (!preg_match('/^\d{7}$/', $zip_code)) {
	        $zip_code = '';
	    }
	}

	if ($zip_code == '') {
	    $flg_error = TRUE;
	}

	if (!$flg_error) {
	    // データファイル
	    define('DATA_DIR', '');

	    $fh = fopen(DATA_DIR . 'ken_all.csv', 'r');
	    if ($fh === FALSE) {
	        exit;
	    }
	    while (($data = fgetcsv($fh, 1000)) !== FALSE) {
	        if ($data[2] == $zip_code) {
	            // エンコーディングを変換
	            $data[3] = mb_convert_encoding($data[3], 'UTF-8', 'Shift_JIS');
	            $data[4] = mb_convert_encoding($data[4], 'UTF-8', 'Shift_JIS');
	            $data[5] = mb_convert_encoding($data[5], 'UTF-8', 'Shift_JIS');
	            $data[6] = mb_convert_encoding($data[6], 'UTF-8', 'Shift_JIS');
	            $data[7] = mb_convert_encoding($data[7], 'UTF-8', 'Shift_JIS');
	            $data[8] = mb_convert_encoding($data[8], 'UTF-8', 'Shift_JIS');

	            // $data[9] が 1 の場合は、$data[5], $data[8] の末尾の (...) を取
	            // る。
	            // (...) が存在しない場合は、$data[5], $data[8] を空文字列とする。
	            if ($data[9] == 1) {
	                if (preg_match('/\(.+\)$/', $data[5])) {
	                    $data[5] = preg_replace('/\s*\(.+\)$/', '', $data[5]);
	                } else {
	                    $data[5] = '';
	                }
	                
	                if (preg_match('/（.+）$/', $data[8])) {
	                    $data[8] = preg_replace('/\s*（.+）$/', '', $data[8]);
	                } else {
	                    $data[8] = '';
	                }
	            }

	            // $data[12] が 1 の場合は、$data[5], $data[8] を空文字列とする。
	            if ($data[12] == 1) {
	                $data[5] = $data[8] = '';
	            }

	            // $data[8] == '以下に掲載がない場合' の場合、$data[5], $data[8]
	            // を空文字列とする
	            if ($data[8] == '以下に掲載がない場合') {
	                $data[5] = $data[8] = '';
	            }

	            $result = array($data[0],  // 全国地方公共団体コード
	                            $data[1],  // (旧)郵便番号(5桁)
	                            $data[2],  // 郵便番号(7桁)
	                            $data[3],  // 都道府県名 (カタカナ)
	                            $data[4],  // 市区町村名 (カタカナ)
	                            $data[5],  // 町域名 (カタカナ)
	                            '',        // 丁目、番地、建物名 (カタカナ)
	                            '',        // 事業所名 (カタカナ)
	                            $data[6],  // 都道府県名
	                            $data[7],  // 市区町村名
	                            $data[8],  // 町域名
	                            '',        // 丁目、番地、建物名
	                            '',        // 事業所名
	                            '',        // 取扱郵便局名
	                            $data[9],  // 一町域が二つ以上の郵便番号に分割
	                            $data[10], // 小字毎に番地が起番されている
	                            $data[11], // 丁目を有する町域
	                            $data[12], // 一つの郵便番号で二つ以上の町域表す
	                            '',        // 個別番号の種別の表示
	                            '',        // 複数番号の有無
	                            $data[13], // 更新フラグ
	                            $data[14], // 更新理由
	                            );
	            break;
	        }
	    }
	    fclose($fh);
	    if ($result === FALSE) {
	        // 見つからなかった場合は、事業所のリストを探す
	        $fh = fopen(DATA_DIR . 'jigyosyo.csv', 'r');
	        if ($fh === FALSE) {
	            return FALSE;
	        }
	        while (($data = fgetcsv($fh, 1000)) !== FALSE) {
	            if ($data[7] == $zip_code) {
	                // エンコーディングを変換
	                $data[1] = mb_convert_encoding($data[1], 'UTF-8', 'Shift_JIS');
	                $data[2] = mb_convert_encoding($data[2], 'UTF-8', 'Shift_JIS');
	                $data[3] = mb_convert_encoding($data[3], 'UTF-8', 'Shift_JIS');
	                $data[4] = mb_convert_encoding($data[4], 'UTF-8', 'Shift_JIS');
	                $data[5] = mb_convert_encoding($data[5], 'UTF-8', 'Shift_JIS');
	                $data[6] = mb_convert_encoding($data[6], 'UTF-8', 'Shift_JIS');
	                $data[9] = mb_convert_encoding($data[9], 'UTF-8', 'Shift_JIS');

	                $result = array($data[0],  // 全国地方公共団体コード
	                                $data[8],  // (旧)郵便番号(5桁)
	                                $data[7],  // 郵便番号(7桁)
	                                '',        // 都道府県名 (カタカナ)
	                                '',        // 市区町村名 (カタカナ)
	                                '',        // 町域名 (カタカナ)
	                                '',        // 小字名、丁目、番地など (カタカナ)
	                                $data[1],  // 事業所名 (カタカナ)
	                                $data[3],  // 都道府県名
	                                $data[4],  // 市区町村名
	                                $data[5],  // 町域名
	                                $data[6],  // 小字名、丁目、番地など
	                                $data[2],  // 事業所名
	                                $data[9],  // 取扱郵便局名
	                                '',        // 一町域が二つ以上の郵便番号に分割
	                                '',        // 小字毎に番地が起番されている
	                                '',        // 丁目を有する町域
	                                '',        // 一つの郵便番号で二つ以上の町域表す
	                                $data[10], // 個別番号の種別の表示
	                                $data[11], // 複数番号の有無
	                                $data[12], // 更新フラグ
	                                '',        // 更新理由
	                                );
	                break;
	            }
	        }
	    }
	}

	// 戻り値の設定
	// 呼び出し元で eval する値を返す。
	echo ("var phpRet = '".$data[6]."&".$data[7].$data[8]."';\n");

?>
