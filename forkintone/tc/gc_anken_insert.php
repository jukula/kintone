<?php
/*****************************************************************************/
/* 案件契約書PHP                                              (Version 1.00) */
/*   ファイル名 : keiyakusyo_anken.php                                       */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define( "TC_URL1" , "https://".TC_CY_DOMAIN."/k/guest/12/".TC_APPID_ANKJ."/show#record=" ); // 案件管理URL
	define( "TC_URL2" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCSNKK."/show#record=" ); // 請求入金管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;
	$clsSrs->paraTableNO = $_REQUEST['rowno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
	    var $paraTableNO		= null; 	// テーブル行番号（パラメタ）

		var $err;
		var $SNKKID             = null;     // 請求入金管理作成ID
		var $LEASEFLG           = 0;     // リースフラグ  0 = リース以外 / 1 = リース
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 請求先の名前保持用

			// 案件管理アプリのデータを取得
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			if( $jsonANKK->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求区分->value == "リース" ){
				$this->LEASEFLG = 1;
			}else{
				$this->LEASEFLG = 0;
			}

			// 顧客管理アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$jsonANKK->records[0]->TC顧客レコード番号->value; // クエリパラメータ
			$jsonKKKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 請求入金アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			if ($this->LEASEFLG == 1 ) {
				$k->strQuery = "( (請求先名 = \"".$jsonANKK->records[0]->リース会社名->value."\") and ( プロセス in (\"".請求書作成中."\") ) )"; // クエリパラメータ
			}else{
		    	$k->strQuery = "( (顧客レコード番号 = \"".$jsonANKK->records[0]->TC顧客レコード番号->value."\") and ( プロセス in (\"".請求書作成中."\") ) )"; // クエリパラメータ
			}

			$jsonSNKK = $k->runCURLEXEC( TC_MODE_SEL );

			if(count($jsonKKKK->records) > 0 || $this->LEASEFLG == 1 ){
				if($jsonKKKK->records[0]->締め日->value != "" || $this->LEASEFLG == 1 ){
					//会社単位で動かす
					// 請求先名を保持

					if ($this->LEASEFLG == 1 ) {
						$sAnkenname = $jsonANKK->records[0]->リース会社名->value;
					}else{
						$sAnkenname = $jsonKKKK->records[0]->顧客名->value;
					}

					if( $k->intDataCount > 0 ) {
						if($k->intDataCount == 1){
							// あればアップデート
							$this->updSNKK( $jsonANKK , $jsonSNKK ,$jsonKKKK, 0);
							$this->updANKK( $jsonANKK , $jsonKKKK);
							$rmgkKei = "請求区分(".$jsonANKK->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求区分->value."):".$sAnkenname."の請求計上(会社単位)を追加しました。";
						}else{
							$rmgkKei = "FALSE";
						}
					}else{
						// なければインサート
						$this->insSNKK( $jsonANKK , $jsonKKKK);
						$this->updANKK( $jsonANKK , $jsonKKKK);
						$rmgkKei = "請求区分(".$jsonANKK->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求区分->value."):".$sAnkenname."の請求計上(会社単位)を新規登録しました。";
					}
				}else{
					$rmgkKei = "【注意】締め日が設定されていません。";
				}
			}else{
				$rmgkKei = "【注意】顧客データがありません。顧客登録をおこなってください。";
			}
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  日付型から月か日を返す                                               */
	    /*  引数	$pDat　 日付項目                                             */
	    /*  引数	$pPara　変換内容   0 = 月 / 1 = 日                           */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setDayGet( &$pDat , $pPara ) {
			$ret = "";

			$Date = explode("-", $pDat);

			if($pPara == 0 ){
				$ret = $Date[1];
			}else{
				$ret = $Date[2];
			}

			return ($ret);
		}

		/*************************************************************************/
	    /*  請求入金管理へデータを追加する                                       */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insSNKK( &$sgnp_json , &$sknp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsSnkk  = array();
			$kokyakusetflg = 0;
			$dkeizyobi = "";         // 請求予定日（初期）を格納

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			//顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			$i=0;

			// 計上日を取得し、分解する 
			$dkeizyobi  = $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value;
			$ardata 	= explode("-",$sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value);

			$kokyaku['締め日']	   = "締め日";

			$year  = $ardata[0];
			$month = $ardata[1] + 1;
			$timestamp = mktime(0, 0, 0, $month, 0, $year);
			$last_month = date('Y-m-d', $timestamp);
			$setdata = "";

			if($sknp_json->records[0]->$kokyaku['締め日']->value != "" && $sknp_json->records[0]->$kokyaku['締め日']->value != 0 ){
				if( $sknp_json->records[0]->$kokyaku['締め日']->value == "31"){
					// 締日が末の場合は末日を設定
					$setdata = $last_month;
				}else if( $ardata[2] <= $sknp_json->records[0]->$kokyaku['締め日']->value){
					// 締日を過ぎていない場合は当月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 1 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$next_month = date('Y-m-d', $timest);
					$nextardata = explode("-",$next_month);

					$syear   = $nextardata[0];
					$smonth  = $nextardata[1];
					$sday1   = $nextardata[2];
					$sday2   = $sknp_json->records[0]->$kokyaku['締め日']->value;
					// 日付の存在チェック
					if( checkdate( $smonth , $sday2 , $syear )){
						$setdata = $syear."-".$smonth."-".$sday2;
					}else{
						$setdata = $syear."-".$smonth."-".$sday1;
					}
				}else{
					// 締日が過ぎている場合、来月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 2 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$nonth = date('Y-m-d', $timest);
					$ndata = explode("-",$nonth);

					$cyear   = $ndata[0];
					$cmonth  = $ndata[1];
					$cday   = $ndata[2];

					if( checkdate( $cmonth , $sknp_json->records[0]->$kokyaku['締め日']->value , $cyear )){
						$setdata = $cyear."-".$cmonth."-".$sknp_json->records[0]->$kokyaku['締め日']->value;
					}else{
						$setdata = $cyear."-".$cmonth."-".$cday;
					}
				}
			}else{
				$setdata = $dkeizyobi; // 締日が空の場合、請求予定日（初期）を入れる。
			}

			// 書込み準備
			$recObj = new stdClass;
			// 通常項目

			if ($this->LEASEFLG == 1 ) {
				$recObj->請求先名					  = $this->tgfEnc( $tcRec , $i , "リース会社名" );
				$recObj->締切日				  		  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value );
				$recObj->入金予定日					  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->入金予定日_請求_->value );
			}else{
				$recObj->顧客レコード番号 			  = $this->valEnc( $sknp_json->records[0]->レコード番号->value );
				$recObj->締切日				  		  = $this->valEnc($setdata);
				$recObj->入金予定日					  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->入金予定日_請求_->value );
				$recObj->請求先名					  = $this->tgfEnc( $tcReck , $i , "顧客名" );
				$recObj->締め日 					  = $this->tgfEnc( $tcReck , $i , "締め日" );
				$recObj->回収サイト 				  = $this->tgfEnc( $tcReck , $i , "回収サイト" );
				$recObj->回収日		 				  = $this->tgfEnc( $tcReck , $i , "回収日" );
				$recObj->回収条件 					  = $this->tgfEnc( $tcReck , $i , "回収条件" );
				$recObj->回収方法 					  = $this->tgfEnc( $tcReck , $i , "回収方法" );
				$recObj->郵便番号 					  = $this->tgfEnc( $tcReck , $i , "郵便番号" );
				$recObj->住所１ 					  = $this->tgfEnc( $tcReck , $i , "住所１" );
			}

			$tbl = $tcRec->getFieldValue( $i , "請求計上情報テーブル" );
			// テーブル項目
			$recObj->請求明細テーブル->value[0]->value->日付 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "請求計上日" );
			$recObj->請求明細テーブル->value[0]->value->商品・サービス名 	=	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_請求_" );
			$recObj->請求明細テーブル->value[0]->value->商品詳細 			= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品詳細_請求_" );
			$recObj->請求明細テーブル->value[0]->value->数量1 			 	=	 $this->tgfEnc( $tbl , $this->paraTableNO , "数量_請求_" );
			$recObj->請求明細テーブル->value[0]->value->単位 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単位_請求_" );
			$recObj->請求明細テーブル->value[0]->value->単価1 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単価_請求_" );
			$recObj->請求明細テーブル->value[0]->value->詳細情報 		 	=    $this->valEnc( TC_URL1.$this->paraAnkenID);
			$recObj->請求明細テーブル->value[0]->value->案件レコード番号 	= 	 $this->tgfEnc( $tcRec , $i , "レコード番号" );

			// リースの場合、摘要に会社名を入れる
			if ($this->LEASEFLG == 1 ) {
				$recObj->請求明細テーブル->value[0]->value->摘要1 	= 	 $this->tgfEnc( $tcRec , $i , "会社名" );;
			}

			// --------------------
			// 請求入金管理へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$insData 			= new stdClass;
			$insData->app 		= TC_APPID_TCSNKK;
			$insData->records[] = $recObj;

			$k->aryJson = $insData;						// 追加対象のレコード番号
			$json = $k->runCURLEXEC( TC_MODE_INS );
			$this->SNKKID = $k->strInsRecNo[0];
		}

		/*************************************************************************/
	    /*  請求入金管理へデータを追加する                                       */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updSNKK( &$sgnp_json , &$snnp_json , &$sknp_json , $No) {

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			//顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			$i = 0;
			$tbcount = 0;
			$strSyubetu = "";        // 調査種別設定用
			$kokyakusetflg = 0;      // 締日が設定されたか知るフラグ
			$dkeizyobi = "";         // 計上日を格納

			// --------------------
			// 請求入金管理を更新する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $snnp_json->records[$No]->レコード番号->value;
			$this->SNKKID = $snnp_json->records[$No]->レコード番号->value;

			$tbcount = count($snnp_json->records[$No]->請求明細テーブル->value);

			$hairetu1 = "";
			$hairetu2 = "";

			for($a = 0; $a <= ($tbcount - 1); $a++) {
				$hairetu1[]	  = $snnp_json->records[$No]->請求明細テーブル->value[$a]->value->日付->value.",".$a.",0";
			}

			// 日付順に並び替える準備
			$dkeizyobi    = $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value;
			$hairetu1[]	  = $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value.",".$a.",1";

			sort($hairetu1);
			for($i = 0; $i <= ($tbcount); $i++) {
				$hairetu2[] = explode(",", $hairetu1[$i]);
			}

			// 計上日を取得し、分解する 
			$ardata = explode("-",$sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value);


			$kokyaku['締め日']	   = "締め日";

			$year  = $ardata[0];
			$month = $ardata[1] + 1;
			$timestamp = mktime(0, 0, 0, $month, 0, $year);
			$last_month = date('Y-m-d', $timestamp);
			$setdata = "";

			if($sknp_json->records[0]->$kokyaku['締め日']->value != "" && $sknp_json->records[0]->$kokyaku['締め日']->value != 0 ){
				if( $sknp_json->records[0]->$kokyaku['締め日']->value == "31"){
					// 締日が末の場合は末日を設定
					$setdata = $last_month;
				}else if( $ardata[2] <= $sknp_json->records[0]->$kokyaku['締め日']->value){
					// 締日を過ぎていない場合は当月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 1 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$next_month = date('Y-m-d', $timest);
					$nextardata = explode("-",$next_month);

					$syear   = $nextardata[0];
					$smonth  = $nextardata[1];
					$sday1   = $nextardata[2];
					$sday2   = $sknp_json->records[0]->$kokyaku['締め日']->value;
					// 日付の存在チェック
					if( checkdate( $smonth , $sday2 , $syear )){
						$setdata = $syear."-".$smonth."-".$sday2;
					}else{
						$setdata = $syear."-".$smonth."-".$sday1;
					}
				}else{
					// 締日が過ぎている場合、来月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 2 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$nonth = date('Y-m-d', $timest);
					$ndata = explode("-",$nonth);

					$cyear   = $ndata[0];
					$cmonth  = $ndata[1];
					$cday   = $ndata[2];

					if( checkdate( $cmonth , $sknp_json->records[0]->$kokyaku['締め日']->value , $cyear )){
						$setdata = $cyear."-".$cmonth."-".$sknp_json->records[0]->$kokyaku['締め日']->value;
					}else{
						$setdata = $cyear."-".$cmonth."-".$cday;
					}
				}
			}else{
				$setdata = $dkeizyobi;
			}

			$i = 0;

			// 差込データ設定

			if ($this->LEASEFLG == 1 ) {
				$sAnkenname = $jsonANKK->records[0]->リース会社名->value;
				$recObj->record->請求先名					  = $this->tgfEnc( $tcRec , $i , "リース会社名" );
				$recObj->record->締切日				  		  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日->value );
				$recObj->record->入金予定日					  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->入金予定日_請求_->value );
			}else{
				$recObj->record->顧客レコード番号 			  = $this->valEnc( $sknp_json->records[0]->レコード番号->value );
				$recObj->record->締切日				  		  = $this->valEnc( $setdata );
				$recObj->record->入金予定日					  = $this->valEnc( $sgnp_json->records[0]->請求計上情報テーブル->value[$this->paraTableNO]->value->入金予定日_請求_->value );
				$recObj->record->請求先名					  = $this->tgfEnc( $tcReck , $i , "顧客名" );
				$recObj->record->締め日 					  = $this->tgfEnc( $tcReck , $i , "締め日" );
				$recObj->record->回収サイト 				  = $this->tgfEnc( $tcReck , $i , "回収サイト" );
				$recObj->record->回収日		 				  = $this->tgfEnc( $tcReck , $i , "回収日" );
				$recObj->record->回収条件 					  = $this->tgfEnc( $tcReck , $i , "回収条件" );
				$recObj->record->回収方法 					  = $this->tgfEnc( $tcReck , $i , "回収方法" );
				$recObj->record->郵便番号 					  = $this->tgfEnc( $tcReck , $i , "郵便番号" );
				$recObj->record->住所１ 					  = $this->tgfEnc( $tcReck , $i , "住所１" );
			}
			$tbl = $tcRec->getFieldValue( $i , "請求計上情報テーブル" );

			for($a = 0; $a <= ($tbcount); $a++) {
				if($hairetu2[$a][2] == "1"){
					// 新規データの設定
					$recObj->record->請求明細テーブル->value[$a]->value->日付 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "請求計上日" );
					$recObj->record->請求明細テーブル->value[$a]->value->商品・サービス名 		=  $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_請求_" );
					$recObj->record->請求明細テーブル->value[$a]->value->商品詳細 				=  $this->tgfEnc( $tbl , $this->paraTableNO , "商品詳細_請求_" );
					$recObj->record->請求明細テーブル->value[$a]->value->数量1 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "数量_請求_" );
					$recObj->record->請求明細テーブル->value[$a]->value->単位 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "単位_請求_" );
					$recObj->record->請求明細テーブル->value[$a]->value->単価1 			  		=  $this->tgfEnc( $tbl , $this->paraTableNO , "単価_請求_" );
					$recObj->record->請求明細テーブル->value[$a]->value->詳細情報 		  		=  $this->valEnc( TC_URL1.$this->paraAnkenID);
					$recObj->record->請求明細テーブル->value[$a]->value->案件レコード番号 		=  $this->valEnc( $this->paraAnkenID );

					// リースの場合、摘要に会社名を入れる
					if ($this->LEASEFLG == 1 ) {
						$recObj->record->請求明細テーブル->value[$a]->value->摘要1 				=  $this->tgfEnc( $tcRec , $i , "会社名" );;
					}

				}else{
					// 既存テーブルへの設定
					$recObj->record->請求明細テーブル->value[$a]->value->日付 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->日付;
					$recObj->record->請求明細テーブル->value[$a]->value->商品・サービス名 		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->商品・サービス名;
					$recObj->record->請求明細テーブル->value[$a]->value->商品詳細		  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->商品詳細;
					$recObj->record->請求明細テーブル->value[$a]->value->数量1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->数量1;
					$recObj->record->請求明細テーブル->value[$a]->value->単位 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->単位;
					$recObj->record->請求明細テーブル->value[$a]->value->単価1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->単価1;
					$recObj->record->請求明細テーブル->value[$a]->value->摘要1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->摘要1;
					$recObj->record->請求明細テーブル->value[$a]->value->請求特記事項_案件_ 	=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->請求特記事項_案件_;
					$recObj->record->請求明細テーブル->value[$a]->value->詳細情報 		  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->詳細情報;
					$recObj->record->請求明細テーブル->value[$a]->value->案件レコード番号 		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->案件レコード番号;
				}
			}

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_TCSNKK;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );

		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json , &$sknp_json) {

			// 案件管理(地盤)
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			// 顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			$i=0;
			$tbcount = 0;

			// -----------------------------
			// 案件管理(地盤調査)を更新する。
			// -----------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;

			$recObj->record->会社名 	  			= $this->tgfEnc( $tcReck , $i , "顧客名" );
			$recObj->record->会社名フリガナ		  	= $this->tgfEnc( $tcReck , $i , "フリガナ" );
			$recObj->record->郵便番号			  	= $this->tgfEnc( $tcReck , $i , "郵便番号" );
			$recObj->record->都道府県			  	= $this->tgfEnc( $tcReck , $i , "都道府県名" );
			$recObj->record->所在地 			  	= $this->tgfEnc( $tcReck , $i , "住所１" );
			$recObj->record->電話番号 		  		= $this->tgfEnc( $tcReck , $i , "電話番号" );
			$recObj->record->ＦＡＸ		 		  	= $this->tgfEnc( $tcReck , $i , "FAX" );
			$recObj->record->代表者氏名 			= $this->tgfEnc( $tcReck , $i , "代表者名" );
			$recObj->record->代表者フリガナ 		= $this->tgfEnc( $tcReck , $i , "代表者名フリガナ" );
			$recObj->record->担当者 			  	= $this->tgfEnc( $tcReck , $i , "担当者" );
			$recObj->record->担当者携帯 			= $this->tgfEnc( $tcReck , $i , "担当者携帯" );
			$recObj->record->締め日 			  	= $this->tgfEnc( $tcReck , $i , "締め日" );
			$recObj->record->回収サイト 			= $this->tgfEnc( $tcReck , $i , "回収サイト" );
			$recObj->record->回収日 			  	= $this->tgfEnc( $tcReck , $i , "回収日" );
			$recObj->record->回収条件 			  	= $this->tgfEnc( $tcReck , $i , "回収条件" );
			$recObj->record->回収方法 			  	= $this->tgfEnc( $tcReck , $i , "回収方法" );

			$tbcount = count($sgnp_json->records[0]->請求計上情報テーブル->value);
			$tbl = $tcRec->getFieldValue( $i , "請求計上情報テーブル" );

			// 既存のデータを再登録する
		    for($a = 0; $a <= ($tbcount - 1); $a++) {
				$recObj->record->請求計上情報テーブル->value[$a]->value->請求計上日 	    	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求計上日;
				$recObj->record->請求計上情報テーブル->value[$a]->value->商品・サービス名_請求_    =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品・サービス名_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->商品詳細_請求_   		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品詳細_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->数量_請求_ 		 	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->数量_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->単位_請求_ 		  	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単位_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->単価_請求_ 		  	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単価_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->請求区分   			   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求区分;
				$recObj->record->請求計上情報テーブル->value[$a]->value->入金予定日_請求_   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->入金予定日_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->請求計上_請求_   		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求計上_請求_;
				$recObj->record->請求計上情報テーブル->value[$a]->value->請求リンク  	 		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求リンク;
		    }

			// チェックを入れる
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上日 		 		 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "請求計上日" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->商品・サービス名_請求_ 	 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->商品詳細_請求_ 			 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品詳細_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->数量_請求_ 	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "数量_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->単位_請求_  	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単位_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->単価_請求_ 	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単価_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->請求区分		 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "請求区分" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->入金予定日_請求_       	 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "入金予定日_請求_" );
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->請求計上_請求_  			 =	 $this->valEnc("済み");
			$recObj->record->請求計上情報テーブル->value[$this->paraTableNO]->value->請求リンク 		 		 = 	 $this->valEnc(TC_URL2.$this->SNKKID);

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_ANKJ;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );

		}
	}

?>
