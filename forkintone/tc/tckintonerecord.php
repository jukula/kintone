<?php
/*****************************************************************************/
/*  kintoneレコードクラス                                     (Version 1.00) */
/*   クラス名       TcKintoneRecord                                          */
/*   プロパティ     intNumber       取得したレコード数                       */
/*                  recList         取得したレコード情報                     */
/*                  err             エラー情報格納用                         */
/*   メソッド       getField        指定されたフィールド情報取得             */
/*                  getNumber       格納しているレコード数取得               */
/*                  setRecordList   KintoneAPI(JSON)→TcRecordに展開         */
/*                  logBackup       ファイルバックアップ                     */
/*                  openLog         ログファイルオープン                     */
/*                  closeLog        ログファイルクローズ                     */
/*                  writeLog        ログ出力                                 */
/*   作成日         2013/04/08      J.Kataoka                                */
/*   更新履歴       20xx/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
include_once("tckintonetable.php");
include_once("tcerror.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////
class TcKintoneField
{
    var $type;
    var $value;
    
    function TcKintoneField($pArray) {
var_dump($pArray);
        $this->type = $pArray["type"];
        $this->value = $pArray["value"];
    }

    /***********
     フィールド値取得
     ****************/
    function getValue() {
        switch ($this->type) {
            case "USER_SELECT":
                foreach($this->value as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
                }
                break;
            case "CREATOR":
            case "MODIFIER":
                foreach($field as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
                }
                break;
            case "CHECK_BOX":
            case "MULTI_SELECT":
            case "FILE":
            case "STATUS_ASSIGNEE":
                $ret = "未対応";
                break;
            default:
                $ret = $field->value;
                break;
        }
        
        return($ret);
    }
}


class TcKintoneRecord
{
    /*************************************************************************/
    /* メンバ変数(プロパティ)                                                */
    /*************************************************************************/
    var $intNumber = 0; // 行数
    var $recList;
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function TcKintoneRecord() {
        // 初期化
        $this->recList = array();
        $this->err = new TcError();
    }

    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* 指定されたフィールド情報取得                                          */
    /*  引数    $pFieldCD   フィールドコード                                 */
    /*  関数値  フィールド情報オブジェクト                                   */
    /*************************************************************************/
    function getField( $pFieldCD ) {
        if( isNull($this->record) ) {
            // エラー
            return("");
        }
        $tmp = get_object_vars( $this->record );
        return( $tmp[$pFieldCD] );
    }

    /*************************************************************************/
    /* 格納しているレコード数取得                                            */
    /*  引数    なし                                                         */
    /*  関数値  レコード数                                                   */
    /*************************************************************************/
    function getNumber() {
        return( $this->intNumber );
    }

    /*************************************************************************/
    /*************************************************************************/
    function setJson( $pJson ) {
        $obj = json_decode($pJson);
        $this->setRecordList($obj->records);
/*
        foreach($obj["records"] as $key => $val) {
            $rec = new TcKintoneField((array)$val);
            $record = array($key => $rec);
            $this->recList[] = $record;
            var_dump($record);
            print("<br>");
        }
*/
    }

    /*************************************************************************/
    /* KintoneAPIで取得したJSONデータをTCレコードオブジェクトに展開          */
    /*  引数    $pRecords   JSONからPHP配列に展開した後のレコードデータ      */
    /*************************************************************************/
    function setRecordList( $pRecords ) {
        $idx = 0;
        // レコードの集合が渡されるので、それを配列に展開
        foreach( $pRecords as $rec ) {
            $tmp = get_object_vars( $rec );
            foreach( $tmp as $key => $val ) {
                // SUBTABLE対応
                $this->recList[$idx][$key] = new stdClass();
                if($val->type == "SUBTABLE") {
                    $tcTable = new TcKintoneTable();
                    $tcTable->setTable($val->value);
                    // 他の項目に合わせるため、type/valueの形で保存します
                    $this->recList[$idx][$key]->type  = "SUBTABLE";
                    $this->recList[$idx][$key]->value = $tcTable;
                } else {
                    $this->recList[$idx][$key] = $val;
                }
            }
            $idx++;
        }

        // 設定したレコード数
        $this->intNumber = $idx;
    }

    /*************************************************************************/
    /* Kintone → TcKintone                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function setRecord( $pRecord ) {
        $ret = array();
        // フィールドの集合
        foreach( $pRecord as $field => $data ) {
            $tmp = get_object_vars( $field );
            $ret[$field] = $data;
        }
        return($ret);

    }

    /*************************************************************************/
    /* Kintone → TcKintone                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function getRecord( $idx ) {
        return($this->recList[$idx]);
    }

    /*************************************************************************/
    /* 変数の中身を返す関数                                                  */
    /*  引数    $p_string   フィールドコード                                 */
    /*************************************************************************/
    function getValue( $field ) {
        $ret = "";

        // フィールド情報取得
//        $field = $this->getField( $p_string );
        // フィールド型判断
        switch ($field->type) {
            case "USER_SELECT":
                foreach($field->value as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
                }
//                $getvalue = $field->value[0]->name;
                break;
            case "CREATOR":
            case "MODIFIER":
//                $ret = $field->value->name;

                foreach($field as $val) {
                    $ret .= (($ret == "") ? "" : ",") . $val->name;
//                    $ret = $field->value["name"];
                }

                break;
            case "CHECK_BOX":
            case "MULTI_SELECT":
            case "FILE":
            case "STATUS_ASSIGNEE":
                $ret = "未対応";
                break;
            default:
                $ret = $field->value;
                break;
        }
        
        return($ret);
    }

    /*************************************************************************/
    /* フィールド値を取得する                                                */
    /*  引数    $idx        レコード番号(0～)                                */
    /*          $fieldCD    フィールドコード                                 */
    /*  関数値  取得したフィールド値                                         */
    /*************************************************************************/
    function getFieldValue( $idx, $fieldCD ) {
        // レコードデータ取得
        $rec = $this->getRecord( $idx );
        // 値取得
        $val = $this->getValue($rec[$fieldCD]);
        
        return($val);
    }

    /*************************************************************************/
    /* フィールドタイプを取得する                                            */
    /*  引数    $idx        レコード番号(0～)                                */
    /*          $fieldCD    フィールドコード                                 */
    /*  関数値  取得したフィールドタイプ                                     */
    /*************************************************************************/
    function getFieldType( $idx, $fieldCD ) {
        // レコードデータ取得
        $rec = $this->getRecord( $idx );
        $typ = $rec[$fieldCD]->type;
        
        return( $typ );
    }

}
