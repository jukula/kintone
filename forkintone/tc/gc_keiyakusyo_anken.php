<?php
/*****************************************************************************/
/* 	 案件管理(地盤調査)契約書PHP                              (Version 1.00) */
/*   ファイル名 : keiyakusyo_anken.php                                       */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_SYOKI_SHN_GYO" , 7 );
	define( "TC_RUNNING_SHN_GYO" , 7 );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
    // 対象のレコード番号を受け取る。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

// 実行
	$clsSrs->main();



	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $err;
		var $common;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// 案件管理(地盤調査)アプリ
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客管理アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$json->records[0]->TC顧客レコード番号->value; // クエリパラメータ
			$jsonKKKK = $k->runCURLEXEC( TC_MODE_SEL );

			if( count($jsonKKKK->records) > 0 ) {

				// エクセルの契約書テンプレートの準備
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
				$objPHPExcel = $objReader->load("templates/keiyakusyo_anken.xls");

				// セルへ設定
				if( count($json->records) > 0 ) {
					$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
				}

				// ダウンロード用エクセルを準備
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				// ファイル名生成
				list($msec, $sec) = explode(" ", microtime());
				$saveName = "注文請書(".$json->records[0]->会社名->value.")_".date('YmdHis').sprintf("%.0f",($msec * 1000)).".xls";
				// ダウンロード用エクセルを保存
				$objWriter->save("tctmp/".$saveName);
				$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

				echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
				echo $msg;
			}else{
				echo '顧客データがTC顧客管理に登録されていません。<br>顧客登録ボタンから登録をしてください。';
			}
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('E4'		,	$pDat->会社名->value	)
			            ->setCellValue('R4'		,	$pDat->会社名フリガナ->value	)
			            ->setCellValue('E6'		,	$pDat->電話番号->value	)
			            ->setCellValue('R6'		,	$pDat->ＦＡＸ->value	)
			            ->setCellValue('E5'		,	$pDat->郵便番号->value	)
			            ->setCellValue('J5'		,	$pDat->所在地->value	)
			            ->setCellValue('E7'		,	$pDat->代表者氏名->value	)
			            ->setCellValue('R7'		,	$pDat->代表者フリガナ->value	)
			            ->setCellValue('E8'		,	$pDat->担当者->value	)
			            ->setCellValue('R8'		,	$pDat->担当者携帯->value	)
			            ->setCellValue('H21'	,	$pDat->支払条件_初期_->value	)
						->setCellValue('H33'	,	$this->common->setDayChange($pDat->サービス開始時期->value , 1)	)
						->setCellValue('Z33'	,	$this->common->setDayChange($pDat->支払開始日->value , 1)	)
						->setCellValue('H34'	,	$pDat->支払条件_保守サポート_->value	)
			            ->setCellValue('H36'	,	$pDat->その他条件->value	)
			            ->setCellValue('H37'	,	$pDat->別途申込書->value	)
			            ->setCellValue('H38'	,	$pDat->別途契約書->value	)
						->setCellValue('AE1'	,	"No.".sprintf('%06d', $pDat->レコード番号->value)	);

			// 納期契約選択の選択によって表示を変える
			if($pDat->納期契約選択->value == "リース" ){
			// リースが選択されている場合
			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
						->setCellValue('A20'	,	"リース契約時期"	)
						->setCellValue('H20'	,	$pDat->リース契約時期->value	)
						->setCellValue('S20'	,	"支払開始日"	)
			            ->setCellValue('Z20'	,	$pDat->支払開始日_リース_->value	);
			}else{
			// 現金が選択されている場合
			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
						->setCellValue('A20'	,	"納品時期"	)
						->setCellValue('H20'	,	$pDat->納品時期->value	)
						->setCellValue('S20'	,	"支払予定日"	)
			            ->setCellValue('Z20'	,	$pDat->支払予定日->value	);
			}


			// 初期
			$idx_s = 0;		    // データ数カウント
			foreach( $pDat->初期導入費用テーブル->value as $key => $val ) {
				$shn = $val->value;
				if( $shn->契約出力_初期_->value[0] != ""){
					// 内容
					if( $idx_s < TC_SYOKI_SHN_GYO ) {
						$y = 12 + $idx_s;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('B'.$y 	,	$shn->商品・サービス名_初期_->value.$shn->商品詳細_初期_->value	)
						            ->setCellValue('O'.$y	,	$shn->数量_初期_->value	)
						            ->setCellValue('R'.$y	,	$shn->単位_初期_->value	)
						            ->setCellValue('V'.$y	,	$shn->単価_初期_->value	)
						            ->setCellValue('AD'.$y	,	$shn->契約摘要_初期_->value	);
					}
					$idx_s++; 		//レコードのカウントアップ
				}
			}

			// 保守
			$idx_r = 0;		// データ数カウント
			foreach( $pDat->保守サポート費用テーブル->value as $key => $val ) {
				$shn = $val->value;
				if( $shn->契約出力_保守_->value[0] != ""){
					// 条件
					if( $idx_r < TC_RUNNING_SHN_GYO ) {
						$y = 25 + $idx_r;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('B'.$y 	,	$shn->商品・サービス名_保守_->value.$shn->商品詳細_保守_->value	)
						            ->setCellValue('O'.$y	,	$shn->数量_保守_->value	)
						            ->setCellValue('R'.$y	,	$shn->単位_保守_->value	)
						            ->setCellValue('V'.$y	,	$shn->単価_保守_->value	)
						            ->setCellValue('AD'.$y	,	$shn->契約摘要_保守_->value	);
					}
					$idx_r++;
				}
			}
			
			// 明細件数チェック
			if( $idx_s > TC_SYOKI_SHN_GYO ) {
				$ret = $ret."　　初期導入の商品が ".TC_SYOKI_SHN_GYO."件を超えています。<br>";
			}
			if( $idx_r > TC_RUNNING_SHN_GYO ) {
				$ret = $ret."　　保守の商品が ".TC_RUNNING_SHN_GYO."件を超えています。<br>";
			}
			if( $ret != "" ) {
				$ret = "注）印刷されない商品があります。<br>".$ret;
			}


			return ($ret);
		}


	}

?>
