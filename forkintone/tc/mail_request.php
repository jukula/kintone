<?php
/*****************************************************************************/
/* 労務原価集計PHP                                            (Version 1.01) */
/*   ファイル名 : authentication-serial.php                                  */
/*   更新履歴   2013/07/16  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define( "TC_URL_EG" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_CC_EGMK."/show#record=" ); // 案件管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcAuthSerial();

	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	// 戻り値設定
	// ERR_000 :認証OK
	// ERR_D005:人数オーバー
	// ERR_D008:認証OK-ログ書込みエラー
	// ERR_V000:シリアル無し
	// ERR_V200:不正アクセス
	switch( $clsSrs->err->ErrorNo ) {
		case ERR_000:  $msg = "確認OK"; break;
		case ERR_D005: $msg = "確認OK、人数エラー"; break;
		case ERR_D008: $msg = "確認OK、ログエラー"; break;
		case ERR_V000: $msg = "確認NG、レコード無し"; break;
		case ERR_V200: $msg = "確認NG、不正アクセス"; break;
	}
	echo "// ".$msg."\n";
	echo "// ".$_REQUEST['SERIAL']."\n";
	echo "phpRet = ".$clsSrs->err->ErrorNo;


	class TcAuthSerial
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAuthSerial() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$ret   = false;
			$total = 0;

			$authkekka = "";							// 認証結果
			$seting = "";
			// ----------------------------------------------
			// 呼び出し元チェック
			// ----------------------------------------------
 			if ( false ) {
				$this->err->setError( ERR_V200 );
				$authkekka = "不正アクセス";					// 認証結果
			} else {

				// ------------
				// データの読込
				// ------------
				$k = new TcKintone();
				$k->parInit();						// API連携用のパラメタを初期化する
				$k->intAppID 	= TC_APPID_CC_EGMK;	// アプリID（営業マネージャー管理）
				$k->arySelFields = array();
			    $k->strQuery = " レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
				$get_json = $k->runCURLEXEC( TC_MODE_SEL );

				// メール送信処理
				if( $k->intDataCount != 0 ) {
					// -------------
					// 成功した場合、メールアドレスにメールを送信する
					// -------------
					mb_language("Japanese");
					mb_internal_encoding("UTF-8");
					$subject= "【Cコン】営業マネージャー管理の変更依頼(".TC_CY_DOMAIN.")";    // タイトルの設定
					// 本文の設定
					$body 	= "以下の変更を受け付けました。\n";
					$body  .= "対応をしてください。\n";
					$body  .= "-------------------------------------------------------------\n";
					$body  .= "依頼元ドメイン：".TC_CY_DOMAIN."\n";
					$body  .= "レコード番号　：".$get_json->records[0]->レコード番号->value."\n";
					$body  .= "通知元　　　　：".$get_json->records[0]->通知元->value."\n";
					$body  .= "目的　　　　　：".$get_json->records[0]->目的->value." \n";
					$body  .= "ＵＲＬ　　　　：".TC_URL_EG.$get_json->records[0]->レコード番号->value." \n\n";
					$header = "From: support@timeconcier.jp"."\r\n"; 		// 送信元の設定.  .BCCの設定（送信元にも送る）

					if (mb_send_mail("support@timeconcier.jp", $subject, $body , $header )) {
						// echo "メールが送信されました。";
					} else {
						// echo "メールの送信に失敗しました。";
					}

					// 正常終了
					$this->err->setError( ERR_000 );
					$ret = true;
				}else{
					$this->err->setError( ERR_V000 );
					$authkekka = "エラー";				// 認証結果
				}
			}

			return $ret;
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
