<?php
ini_set("memory_limit","200M");
	/*****************************************************************************/
	/* 集計PHP                    	       		                  (Version 1.00) */
	/*   ファイル名 : graph_insert_v2.php                              			 */
	/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
	/*   [備考]                                                                  */
	/*      tcutility.incを必ずインクルードすること                              */
	/*   [必要ファイル]                                                          */
	/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
	/*                                                                           */
	/*                                                                           */
	/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
	/*****************************************************************************/

	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	define( "TC_URL_AK" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCANKK."/show#record=" ); // 案件管理URL
	define( "TC_URL_ST" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHUIT."/show#record=" ); // 案件管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();

// 実行
	if($_POST['Submit']){
		$clsSrs->main();
	}

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }


		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {

			echo "データ作成処理を開始します。このまましばらくお待ちください。<br><br>\n";

			$msg  = "";
			$day1 = date('Y-01-01', strtotime('+1 year'));//来年

			$statDay = $_POST['yaer'].'-01-01';   //期間開始日付を取得
			$endDay  = $_POST['yaer'].'-12-31';  //期間終了日付を取得

			// ----------------------------------------------------
			// TC案件管理グラフのデータを削除する
			// ----------------------------------------------------
			$aryAnkgRecno = array();

			// TC案件管理グラフアプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANGK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "(( (請求日 >= \"".$statDay."\") and (請求日 <= \"".$endDay."\") ) or ( (入金日 >= \"".$statDay."\") and (入金日 <= \"".$endDay."\") ) or ( (支払日 >= \"".$statDay."\") and (支払日 <= \"".$endDay."\") ))";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_del[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json_del[$dtcount]->records as $key => $rec ) {
					$aryRmgkRecno[] = $rec->レコード番号->value;
				}

				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_del[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// --------
			// 削除実行
			// --------
            if($dtcount != 0){
			    $this->delData( TC_APPID_TCANGK , $aryRmgkRecno );
            }

			// ----------------------------------------------------
			// TC案件管理のデータを登録する（ストック以外）
			// ----------------------------------------------------
			// TC案件管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "(( (請求日 >= \"".$statDay."\") and (請求日 <= \"".$endDay."\") ) or ( (入金日_請求_ >= \"".$statDay."\") and (入金日_請求_ <= \"".$endDay."\") ) or ( (支払日 >= \"".$statDay."\") and (支払日 <= \"".$endDay."\") ))";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_ank[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_ank[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ
			} while( $k->intDataCount > 0 );

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録データを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsRmgk = array();

			for ($k = 0; $k < count($json_ank); $k++) {
				$tcRec = new TcKintoneRecord();
				$tcRec->setRecordList( $json_ank[$k]->records );

				for( $i=0; $i < $tcRec->getNumber(); $i++ ) {
					$tbl = $tcRec->getFieldValue( $i , "請求テーブル" );
					// TC案件管理 -> TC案件管理グラフ
					for( $j=0; $j < $tbl->getNumber(); $j++ ) {

						// 書込み準備
						$recObj = new stdClass;

						$arydate_si = array();
						$arydate_se = array();

						$arydate_si = explode("-",$json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->入金日_請求_->value);
						$arydate_se = explode("-",$json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->支払日->value);

						// 通常項目
						$recObj->受注確度		= $this->tgfEnc( $tcRec , $i , "受注確度" );
						$recObj->顧客名			= $this->tgfEnc( $tcRec , $i , "顧客名" );
						$recObj->案件名			= $this->tgfEnc( $tcRec , $i , "案件名" );
						$recObj->TC案件リンク	= $this->valEnc(TC_URL_AK.$json_ank[$k]->records[$i]->レコード番号->value);

						$recObj->事業区分		= $this->tgfEnc( $tcRec , $i , "事業区分" );
						$recObj->エリア区分		= $this->tgfEnc( $tcRec , $i , "エリア区分" );
						$recObj->直間区分		= $this->tgfEnc( $tcRec , $i , "直間区分" );
						$recObj->案件受注確度	= $this->tgfEnc( $tcRec , $i , "受注確度" );

						if( $json_ank[$k]->records[$i]->営業施策区分->value != "" ){
							$recObj->営業施策区分	= $this->tgfEnc( $tcRec , $i , "営業施策区分" );
						}

						$recObj->集客手段		= $this->tgfEnc( $tcRec , $i , "集客手段" );
						$recObj->受注目標時期	= $this->tgfEnc( $tcRec , $i , "受注目標時期" );
						$recObj->最新の営業状況	= $this->tgfEnc( $tcRec , $i , "最新の営業状況" );

						// テーブル項目
						$recObj->請求日	= $this->tgfEnc( $tbl , $j , "請求日" );
						$recObj->入金日	= $this->tgfEnc( $tbl , $j , "入金日_請求_" );
						$recObj->支払日	= $this->tgfEnc( $tbl , $j , "支払日" );

						if($json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->請求額->value != "" ){
							$recObj->請求額	= $this->tgfEnc( $tbl , $j , "請求額" );
							if(($arydate_si[0]."-".$arydate_si[1] == $arydate_se[0]."-".$arydate_se[1]) && $json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->支払日->value != "" ){
								$recObj->入金額	= $this->tgfEnc( $tbl , $j , "粗利" );
							}else{
								$recObj->入金額	= $this->tgfEnc( $tbl , $j , "請求額" );
							}
						}

						if($json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->原価->value != "" ){
							$recObj->原価	= $this->tgfEnc( $tbl , $j , "原価" );
						}

						$aryInsRmgk[ $idxM ][ $idxR ] = $recObj;
						$idxR += 1;
						if( $idxR == CY_INS_MAX ) {
							$idxM += 1;
							$idxR = 0;
						}
						$recObj = null;

						if(($arydate_si[0]."-".$arydate_si[1] != $arydate_se[0]."-".$arydate_se[1]) && $json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->支払日->value != "" ){
							// 書込み準備
							$recObj = new stdClass;
							// 通常項目
							$recObj->受注確度		= $this->tgfEnc( $tcRec , $i , "受注確度" );
							$recObj->顧客名			= $this->tgfEnc( $tcRec , $i , "顧客名" );
							$recObj->案件名			= $this->tgfEnc( $tcRec , $i , "案件名" );
							$recObj->TC案件リンク	= $this->valEnc(TC_URL_AK.$json_ank[$k]->records[$i]->レコード番号->value);

							$recObj->事業区分		= $this->tgfEnc( $tcRec , $i , "事業区分" );
							$recObj->エリア区分		= $this->tgfEnc( $tcRec , $i , "エリア区分" );
							$recObj->直間区分		= $this->tgfEnc( $tcRec , $i , "直間区分" );
							$recObj->案件受注確度	= $this->tgfEnc( $tcRec , $i , "受注確度" );

							if( $json_ank[$k]->records[$i]->営業施策区分->value != "" ){
								$recObj->営業施策区分	= $this->tgfEnc( $tcRec , $i , "営業施策区分" );
							}

							$recObj->集客手段		= $this->tgfEnc( $tcRec , $i , "集客手段" );
							$recObj->受注目標時期	= $this->tgfEnc( $tcRec , $i , "受注目標時期" );
							$recObj->最新の営業状況	= $this->tgfEnc( $tcRec , $i , "最新の営業状況" );

							// テーブル項目
							$recObj->請求日	= $this->tgfEnc( $tbl , $j , "請求日" );
							$recObj->入金日	= $this->tgfEnc( $tbl , $j , "支払日" );
							$recObj->支払日	= $this->tgfEnc( $tbl , $j , "支払日" );

							if($json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->原価->value != "" ){
								$setkingaku 	= "-".$json_ank[$k]->records[$i]->請求テーブル->value[$j]->value->原価->value;
								$recObj->入金額	= $this->valEnc( $setkingaku );
							}

							$aryInsRmgk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_INS_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

						}

					}
				}
			}
			// ---------------------------
			// TC案件管理グラフへ追加する。
			// ---------------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANGK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryInsRmgk as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCANGK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json_ins   = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
//					echo "案件管理データの追加に成功しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
				} else {
					echo "案件管理データの追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				$rmgkInsKensu += count( $val );
			}

			echo "ストック売上管理からデータ取得中<br>\n";

			// ------------------------------------
			// TC案件管理からストック（実）情報を取得する
			// ------------------------------------
			// ストック売上管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHUIT;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
//				$aryQ[] = "(( (年 = \"".$_POST['yaer']."\") and (区分 in (\""."粗利"."\")) ) or ( (年 = \"".($_POST['yaer'] - 1)."\") and (区分 in (\""."粗利"."\")) and (_12月 !=  \""."\") ))";
				$aryQ[] = "(( (年 = \"".$_POST['yaer']."\") and (区分 in (\""."粗利"."\")) ))";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_stj[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_stj[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ
			} while( $k->intDataCount > 0 );

			// --------------------------------------------------------
			// 一括登録件数の制限ごとに登録データを加工する。(ストック（実）)
			// --------------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsgraph = array();
			$k = 0;
			$hsReco = array();

			for ($k = 0; $k < count($json_stj); $k++) {
				$tcRec = new TcKintoneRecord();
				$tcRec->setRecordList( $json_stj[$k]->records );

				for( $i=0; $i < $tcRec->getNumber(); $i++ ) {
					$hsReco[] = $json_stj[$k]->records[$i]->保守レコード番号->value;
					// TCストック売上管理 -> TC案件管理グラフ
					for( $j=0; $j <= 12; $j++ ) {
						// 対象年のみ処理を行う
						if($json_stj[$k]->records[$i]->年->value == ($_POST['yaer']) ){
							// 初回は先年の処理を行う
							if( $j == 0 ){
/*
								// 書込み準備
								$recObj = new stdClass;
								// 通常項目
								$recObj->受注確度		= $this->valEnc("0：ストック（実績）");
								$recObj->顧客名			= $this->tgfEnc( $tcRec , $i , "顧客名" );
								$recObj->案件名			= $this->tgfEnc( $tcRec , $i , "契約名" );
								$recObj->集客手段		= $this->tgfEnc( $tcRec , $i , "集客手段" );
								$recObj->事業区分		= $this->tgfEnc( $tcRec , $i , "事業区分" );
								$recObj->エリア区分		= $this->tgfEnc( $tcRec , $i , "エリア区分" );
								$recObj->直間区分		= $this->tgfEnc( $tcRec , $i , "直間区分" );
								$recObj->案件受注確度	= $this->valEnc( "1：Ｓ(１００％)_受注" );
								if( $json_stj[$k]->records[$i]->営業施策区分->value != "" ){
									$recObj->営業施策区分	= $this->tgfEnc( $tcRec , $i , "営業施策区分" );
								}
								if($json_stj[$k]->records[$i]->案件レコード番号->value != "" ){
									$recObj->TC案件リンク	= $this->valEnc(TC_URL_AK.$json_stj[$k]->records[$i]->案件レコード番号->value);
								}
								$recObj->TC保守リンク	= $this->valEnc(TC_URL_ST.$json_stj[$k]->records[$i]->レコード番号->value);

								// 日付の設定
								$setyaer	= date("Y-m-d", strtotime($_POST['yaer']."-01-01"));
								$setyaer_m  = date('Y-m-t', strtotime($setyaer." -1 month"));
								$setyaer_n	= date("Y-m-t", strtotime($_POST['yaer']."-01-01"));

								$setamount  = "0";
								$setgamount  = "0";
								$setuamount  = "0";
								for ($l = 0; $l < count($json_stj); $l++) {
									for( $n=0; $n < $tcRec->getNumber(); $n++ ) {
										if(($json_stj[$l]->records[$n]->年->value) == ($_POST['yaer'] - 1 )){
											if($json_stj[$l]->records[$n]->保守レコード番号->value == $json_stj[$k]->records[$i]->保守レコード番号->value){
												$setamount   = $json_stj[$l]->records[$n]->_12月->value;
												$setgamount  = $json_stj[$l]->records[$n]->原価12月->value;
												$setuamount  = $json_stj[$l]->records[$n]->売上12月->value;
											}
										}
									}
								}

								$recObj->請求日	= $this->valEnc( $setyaer_m );
								$recObj->請求額	= $this->valEnc( 0 );
								$recObj->入金日	= $this->valEnc( $setyaer_n );
								$recObj->入金額	= $this->valEnc( $setamount );
								$recObj->原価	= $this->valEnc( $setgamount );
								$aryInsgraph[ $idxM ][ $idxR ] = $recObj;
								$idxR += 1;

								if( $idxR == CY_INS_MAX ) {
									$idxM += 1;
									$idxR = 0;
								}

								$recObj = null;
*/
								continue;
							}

							// 書込み準備
							$recObj = new stdClass;
							// 通常項目
							$recObj->受注確度		= $this->valEnc("0：ストック（実績）");
							$recObj->顧客名			= $this->tgfEnc( $tcRec , $i , "顧客名" );
							$recObj->案件名			= $this->tgfEnc( $tcRec , $i , "契約名" );
							$recObj->集客手段		= $this->tgfEnc( $tcRec , $i , "集客手段" );
							$recObj->事業区分		= $this->tgfEnc( $tcRec , $i , "事業区分" );
							$recObj->エリア区分		= $this->tgfEnc( $tcRec , $i , "エリア区分" );
							$recObj->直間区分		= $this->tgfEnc( $tcRec , $i , "直間区分" );
							$recObj->案件受注確度	= $this->valEnc( "1：Ｓ(１００％)_受注" );
							if( $json_stj[$k]->records[$i]->営業施策区分->value != "" ){
								$recObj->営業施策区分	= $this->tgfEnc( $tcRec , $i , "営業施策区分" );
							}
							if($json_stj[$k]->records[$i]->案件レコード番号->value != "" ){
								$recObj->TC案件リンク	= $this->valEnc(TC_URL_AK.$json_stj[$k]->records[$i]->案件レコード番号->value);
							}
							$recObj->TC保守リンク	= $this->valEnc(TC_URL_ST.$json_stj[$k]->records[$i]->レコード番号->value);

							// テーブル項目
							$setyaer1	= date("Y-m-t", strtotime($_POST['yaer']."-".$j."-01"));
							$setyaer2	= date("Y-m-d", strtotime($_POST['yaer']."-".$j."-01"));
							$setyaer_m = date('Y-m-t', strtotime($setyaer2." +1 month"));

							$recObj->請求日	= $this->valEnc( $setyaer1 );

							$kom = "売上".($j)."月";
							if($json_stj[$k]->records[$i]->$kom->value != ""){
								$recObj->請求額	= $this->tgfEnc( $tcRec , $i , ("売上".($j)."月") );
							}

							$recObj->入金日	= $this->valEnc( $setyaer_m );

							$kom = "売上".($j)."月";
							if($json_stj[$k]->records[$i]->$kom->value != ""){
								$recObj->入金額	= $this->tgfEnc( $tcRec , $i , ("_".($j)."月") );
							}

							$kom = "原価".($j)."月";
							if($json_stj[$k]->records[$i]->$kom->value != ""){
								$recObj->原価	= $this->tgfEnc( $tcRec , $i , ("原価".($j)."月") );
							}

							$aryInsgraph[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;

							if( $idxR == CY_INS_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}

							$recObj = null;
						}
					}
				}
			}

			// ---------------------------
			// TC案件管理グラフへ追加する。
			// ---------------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANGK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryInsgraph as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCANGK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json_ins   = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
//					echo "案件管理データの追加に成功しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
				} else {
					echo "案件管理データの追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				$rmgkInsKensu += count( $val );
			}

			echo "TC案件管理からストック予定データ取得中<br>\n";

			// ------------------------------------
			// TC案件管理からストック(予定)情報を取得する
			// ------------------------------------
			// TC案件管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "( (月額ストック粗利益 != \""."\") and (ストック開始日 < \"".$day1."\") )";
				$aryQ[] = "( 月額ストック粗利益 != 0 )";
				$aryQ[] = "( ストック開始日 != \""."\" )";
				$aryQ[] = "( 受注確度 not in (\""."6：Ｅ(１０％)_卵"."\",\""."9：Ｆ(０％)_失注延期"."\") )";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_st[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_st[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ
			} while( $k->intDataCount > 0 );

			// --------------------------------------------------------
			// 一括登録件数の制限ごとに登録データを加工する。(ストック)
			// --------------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsgraph = array();
			$k = 0;
			$setflg = "true";

			for ($k = 0; $k < count($json_st); $k++) {
				$tcRec = new TcKintoneRecord();
				$tcRec->setRecordList( $json_st[$k]->records );
				for( $i=0; $i < $tcRec->getNumber(); $i++ ) {

					for ($n = 0; $n < count($hsReco); $n++) {
						if($hsReco[$n] == $json_st[$k]->records[$i]->保守レコード番号->value){
							$setflg = "false";

						}
					}

					if($setflg == "true"){
						// TC案件管理 -> TC案件管理グラフ
						for( $j=0; $j <= 12; $j++ ) {

							$getendstday = array();
							$setendstday = "0";

							// 初回は先年の処理を行う
							if( $j == 0 ){
								continue;
							}

							// ストック開始日が入っている場合、ストックが開始しているかチェック
							if($json_st[$k]->records[$i]->ストック開始日->value != "" ){
								$setday = array();
								$setday = explode('-',$json_st[$k]->records[$i]->ストック開始日->value);

								if($setday[0] > $_POST['yaer']){
									continue;
								}else{
									if(($setday[0] == $_POST['yaer'] ) && ($setday[1] > $j)){
										continue;
									}
								}
							}

							// 書込み準備
							$recObj = new stdClass;
							// 通常項目
							$recObj->受注確度		= $this->valEnc("0：ストック（予定）");
							$recObj->顧客名			= $this->tgfEnc( $tcRec , $i , "顧客名" );
							$recObj->案件名			= $this->tgfEnc( $tcRec , $i , "案件名" );
							$recObj->TC案件リンク	= $this->valEnc(TC_URL_AK.$json_st[$k]->records[$i]->レコード番号->value);

							$recObj->事業区分		= $this->tgfEnc( $tcRec , $i , "事業区分" );
							$recObj->エリア区分		= $this->tgfEnc( $tcRec , $i , "エリア区分" );
							$recObj->直間区分		= $this->tgfEnc( $tcRec , $i , "直間区分" );
							$recObj->案件受注確度	= $this->tgfEnc( $tcRec , $i , "受注確度" );
							if( $json_st[$k]->records[$i]->営業施策区分->value != "" ){
								$recObj->営業施策区分	= $this->tgfEnc( $tcRec , $i , "営業施策区分" );
							}

							$recObj->集客手段		= $this->tgfEnc( $tcRec , $i , "集客手段" );
							$recObj->受注目標時期	= $this->tgfEnc( $tcRec , $i , "受注目標時期" );
							$recObj->最新の営業状況	= $this->tgfEnc( $tcRec , $i , "最新の営業状況" );

							// テーブル項目
							$setyaer1	= date("Y-m-t", strtotime($_POST['yaer']."-".$j."-01"));
							$setyaer2	= date("Y-m-d", strtotime($_POST['yaer']."-".$j."-01"));
							$setyaer_m = date('Y-m-t', strtotime($setyaer2." +1 month"));

							$recObj->請求日	= $this->valEnc( $setyaer1 );
							$recObj->入金日	= $this->valEnc( $setyaer_m );
							$recObj->支払日	= $this->valEnc( $setyaer_m );

							// 空なら0を入れる
							if( $json_st[$k]->records[$i]->売単価額合計->value != "" ){
								$recObj->請求額	= $this->tgfEnc( $tcRec , $i , "売単価額合計" );
							}

							if( $json_st[$k]->records[$i]->原単価額合計->value != "" ){
								$recObj->原価	= $this->tgfEnc( $tcRec , $i , "原単価額合計" );
							}

							if( $json_st[$k]->records[$i]->月額ストック粗利益->value != "" ){
//								$recObj->粗利	= $this->tgfEnc( $tcRec , $i , "月額ストック粗利益" );
								$recObj->入金額	= $this->tgfEnc( $tcRec , $i , "月額ストック粗利益" );
							}

							$aryInsgraph[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;

							if( $idxR == CY_INS_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}

							$recObj = null;
						}
					}

					$setflg = "true";
				}
			}
// print_r($aryInsgraph);

			// ---------------------------
			// TC案件管理グラフへ追加する。
			// ---------------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANGK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryInsgraph as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCANGK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json_ins   = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
//					echo "案件管理データの追加に成功しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
				} else {
					echo "案件管理データの追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				$rmgkInsKensu += count( $val );
			}
			echo "処理が終了しました。<br><br>\n";
			echo "<input type='button' value='閉じる' onClick='window.close()'>\n";
			exit;
		}

		/*************************************************************************/
	    /* 指定したアプリのデータを削除する                                      */
	    /*  引数	$appid    		アプリＩＤ                                   */
	    /*       	$pDelRecno   	削除対象のレコード番号                       */
	    /*  関数値  string			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function delData( $appid , &$pDelRecno ) {
			// -----------------------------------------------
			// CY_DEL_MAX個ごとに処理をする。（kintoneの制限）
			// -----------------------------------------------
			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			$aryDelRmgk = array();
			foreach( $pDelRecno as $key => $val ) {
				$aryDelRmgk[ $idxM ][ $idxR ] = $val;
				$idxR += 1;
				if( $idxR == CY_DEL_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
			}
			// 削除実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->intAppID 		= $appid;				// アプリID
			$k->aryDelRecNo		= array();
			foreach( $aryDelRmgk as $key => $val ) {
	    		$k->aryDelRecNo = $val;					// 削除対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_DEL );
			}
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>集計表出力</title>
 </head>
 <script type="text/javascript">
 <!--
 function btndis(btn){
  elem = document.getElementById("submitmsg");
	//elem.disabled = true;
	elem.style.visibility = "visible";
  return true;
   //btn.disabled=true;
 }
 // -->
 </script>
 <body>
	<p class="txt">
        集計表作成対象の年を選択し、作成を押してください。<br>
    </p>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post" onsubmit="return btndis()">
		<select name="yaer" size="1">
<?php
    $i = -2;
	while ( $i <= 7) {
	    $year = date("Y") + $i;
        if ( $year == date("Y") ) {
     	    echo '<option value="' . $year . '" selected="selected">' . $year . '</option>';
		} else {
     	    echo '<option value="' . $year . '">' . $year . '</option>';
		}
        $i++;
	}
?>
		</select>
		年

		<input id="btnsub" type="submit" name="Submit" value="作成" >
    <p id="submitmsg" style="visibility:hidden">データ作成処理を開始します。このまましばらくお待ちください。</p>
	</form>
 </body>
</html>


