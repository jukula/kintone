<?php
/*****************************************************************************/
/*  kintoneｱﾌﾟﾘ初期定義ファイル                               (Version 1.00) */
/*                                                                           */
/*   本ファイル内で定義されたものは各サイトごとに異なるものです              */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
if (!defined("DEFKINTONECONF_INC")) {
    // ２重インクルード防止
    define("DEFKINTONECONF_INC", true);

    ///////////////////////////////////////////////////////////////////////////////
    // 初期設定
    ///////////////////////////////////////////////////////////////////////////////
    define("TC_CY_DOMAIN"   , "timeconcier.cybozu.com"); // ドメイン
    define("TC_CY_PHP_DOMAIN", "tc"); // PHPファイルのドメイン

    define("TC_CY_USER"     , "Administrator"         ); // ユーザ名
    define("TC_CY_PASSWORD" , "kintone2010"       ); // パスワード

    define("TC_CY_AUTH_USER", ""                      ); // 認証ユーザ
    define("TC_CY_AUTH_PASS", ""                      ); // 認証パスワード

	///////////////////////////////////////////////////////////////////////////////
	// kintoneｱﾌﾟﾘ設定
	///////////////////////////////////////////////////////////////////////////////
	// アプリID
	define("TC_APPID_TCSGNP"	, 2816 ); // 作業日報 TC作業日報入力
	define("TC_APPID_TCRMGK"	, 2890 ); // 労務原価 TC労務原価管理(案件)
	define("TC_APPID_TCANKK"	, 2221 ); // 案件管理 TC案件管理
	define("TC_APPID_TCRMHK"	, 2891 ); // 労務原価 TC労原価管理(保守)
	define("TC_APPID_TCHSKR"	, 2383 ); // 保守管理 TC保守管理
	define("TC_APPID_TCHRRK"	, 3015 ); // 保守売上履歴 TC保守売上履歴
	define("TC_APPID_TCHSNW"	, 3239 ); // 保守管理(最新明細)  TC保守管理(最新明細)
	define("TC_APPID_TCHUIT"	, 3111 ); // ストック売上管理 TCストック売上管理
	define("TC_APPID_TCMTKK"	, 2892 ); // 見積管理 TC見積管理
	define("TC_APPID_TCSNKK"	, 3228 ); // 請求入金管理 TC請求入金管理
	define("TC_APPID_TCKKKK"	, 2228 ); // 顧客管理 TC顧客管理
	define("TC_APPID_TCITKK"	, 2583 ); // IT契約管理
	define("TC_APPID_TCMISK"	, 2226 ); // TC名刺管理
	define("TC_APPID_TCANGK"	, 3917 ); // 案件管理 TC案件管理グラフ
	define("TC_APPID_TCKDHK"	, 3485 ); // TC活動報告

	define("TC_APPID_TEST"		, 2772 ); // テストアプリ（後で削除）

	define("TC_APPID_ANKJ"	    , 3195 ); // 案件管理（地盤調査）

	define("TC_APPID_SGNP"		, 2953 ); // 作業日報 作業日報入力(入稿用)(案件管理sol)
	define("TC_APPID_RMGK"		, 2959 ); // 労務原価 労務原価管理(案件)(入稿用)(案件管理sol)
	define("TC_APPID_ANKK"		, 2954 ); // 案件管理 案件管理(入稿用)(案件管理sol)

	define("TC_GUESTID_TCGC"	, 12 );   // ゲストID(グランドコンシェル)

	define("TC_APPID_CC_EGMK"	, 3496 ); // 営業マネージャー管理

	// テスト用後で消す
	define("TC_APPID_CC_SSIK"		, 3939  ); // 請求書支払依頼書
	define("TC_APPID_CC_SSSK"		, 3938  ); // 請求支払集計

	// 集計用 行区分（保守管理集計用）
	define("TC_SKIKBN_MEISAI"   , 0);		// 明細行
	define("TC_SKIKBN_KEI_TUKI" , 1);		// 集計行：月計（未使用：明細が月計のため）
	define("TC_SKIKBN_KEI_NEN"  , 2);		// 集計行：年計
	define("TC_SKIKBN_KEI_RKEI" , 3);		// 集計行：累計

	define("TC_SEK_TNI_TUKI" 	, "月");	// 積算単位
	define("TC_SEK_TNI_NEN" 	, "年");	// 積算単位

	///////////////////////////////////////////////////////////////////////////////
	// アップロード、ダウンロード用一時フォルダ
	///////////////////////////////////////////////////////////////////////////////
	define( "TC_UPLOAD_FOLDER" 	, "c:/uploadtmp/" );

    ///////////////////////////////////////////////////////////////////////////////
    // グローバル変数
    ///////////////////////////////////////////////////////////////////////////////

}
?>