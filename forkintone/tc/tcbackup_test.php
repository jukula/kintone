<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tc_web.php                 				                 */
/*   更新履歴   2013/07/16  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcMail();

	$clsSrs->paraAppID = $_REQUEST['appno'] - 0;

	// 実行
	$clsSrs->main();

	class TcMail
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;
		var $paraAppID 	= null;
		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcMail() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$ret = false;

			// ----------------------------------------------
			// 名刺管理から対象データを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->paraAppID;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数
			$keyset = false;
			$keyset_tb = false;

			$file_name = "./bkfile/".$this->paraAppID;

			//ファイルが存在するかを調べる
			if( file_exists( $file_name ) ){
//				print "{$file_name}はすでに存在します\n";
			}else{
				//ディレクトリ（フォルダ）を作成する。
				mkdir( $file_name, 0700 );
//				print "{$file_name}を作成しました\n";
			}

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				if($keyset == false){
					while ($ary_name = current($mail_json[$i]->records[0])) {
					    $ary_keys[] = key($mail_json[$i]->records[0]);
					    next($mail_json[$i]->records[0]);
					}
					$keyset = true;
				}
//print_r($ary_keys);

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json[$i]->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード数だけループ
				for($a = 0; $a < count($mail_json[$i]->records); $a++) {
					// 項目数だけループ
					for($komcount = 0; $komcount < count($ary_keys); $komcount++) {
						// サブテーブル、ファイルの場合は添付物をサーバに保存する処理を行う
						switch( $mail_json[$i]->records[$a]->$ary_keys[$komcount]->type ) {
							case "SUBTABLE":
								// サブテーブルの項目を取得
								if ( $keyset_tb == false ){
									while ($ary_name = current($mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[0]->value)) {
									    $ary_keys_tb[] = key($mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[0]->value);
									    next($mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[0]->value);
									}
								}

								$tbcount = count($mail_json[$i]->records[$a]->$ary_keys[$komcount]->value);
							    for($k = 0; $k <= ($tbcount - 1); $k++) {
									// サブテーブルの項目の数だけループし、FILE型があれば添付物をサーバに保存する
				    				for($j = 0; $j <= (count($ary_keys_tb) - 1); $j++) {
										if($mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[$k]->value->$ary_keys_tb[$j]->type == "FILE" ){
											$kmk	= $mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[$k]->value->$ary_keys_tb[$j];
											$kmkkey = $mail_json[$i]->records[$a]->$ary_keys[$komcount]->value[$k]->value->$ary_keys_tb[$j]->value;

											if( $kmk->value ) {
												// 新しいファイルキーを取得する
												$retDUnfk = $this->getDwnUpNewFileKey( $kmk , $kmkkey );
												if( $retDUnfk  == "" ) {
													// 正常
													// 新しいファイルキーをセットする
						//							$mail_json[$i]->records[$a]->添付->value[$k]->fileKey = $kmkkey;
												} else {
													// エラー
													echo $retDUnfk."<br>\n 終了。";
													exit;
												}
											}
										}
									}
								}
								break;
							case "FILE":
								$kmk	= $mail_json[$i]->records[$a]->$ary_keys[$komcount];
								$kmkkey = $mail_json[$i]->records[$a]->$ary_keys[$komcount]->value;

								if( $kmk->value ) {
									// 新しいファイルキーを取得する
									$retDUnfk = $this->getDwnUpNewFileKey( $kmk , $kmkkey );
									if( $retDUnfk  == "" ) {
										// 正常
										// 新しいファイルキーをセットする
			//							$mail_json[$i]->records[$a]->添付->value[0]->fileKey = $kmkkey;
									} else {
										// エラー
										echo $retDUnfk."<br>\n 終了。";
										exit;
									}
								}
								break;
							default:
						}
					}
					$keyset_tb = true;
				}

				$i++; // カウントアップ
			} while( $k->intDataCount > 0 );

			file_put_contents($file_name."/".$this->paraAppID.'.txt', serialize($mail_json));
			echo "バックアップが完了しました。";
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){

			$base  = new DateTime();
			$today = $base->format('Ymd');

			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');

			$age = (int) (($today - $birthday) / 10000);

			return $age;
		}

		/*************************************************************************/
	    /* 添付ファイルをダウン->アップし、新filekeyを取得・設定する             */
	    /*  引数	項目情報(参照) 、 レコード情報(参照)                         */
	    /*  関数値  正常："" 、 異常：エラーメッセージ                           */
	    /*************************************************************************/
		function getDwnUpNewFileKey( &$pKmk , &$pKmkFileKey ) {
			$ret = "";

			if( $pKmk->type == "FILE" ) {
			} else {
				return $ret;
			}

			// ダウンロード
			$f = new TcKintone();
			$f->parInit();
			$f->intAppID = $clsSrs->paraAppID;
			$wkFileInfo = array();
			if( is_array($pKmk->value) ) {
				// 複数ファイル有
				$wkFileInfo = $pKmk->value;
			} else {
				// 単一ファイル
				$wkFileInfo[] = $pKmk->value;
			}
			foreach( $wkFileInfo as $key_f => $val_f ) {
				if( $val_f ) {
					$dwndat = $f->runDOWNLOAD( $val_f );
					if( $f->strHttpCode == 200 ) {
						// アップロード
						$filePath = "/home/cross-tier/timeconcier.jp/public_html/forkintone/tc/bkfile/".$this->paraAppID."/";
						$u = new TcKintone();
						$u->parInit();
						$u->intAppID = $clsSrs->paraAppID;
						$retFileKey = $u->runUPLOAD( $val_f , $dwndat , $filePath);
						if( $u->strHttpCode == 200 ) {
							// 新filekey
							$pKmkFileKey = $retFileKey->fileKey;
						} else {
							$ret = "ファイルをアップロードできませんでした。 ".$val_f-name." (".$u->strHttpCode.":".$u->message.")";
							break;
						}
					} else {
						$ret = "ファイルをダウンロードできませんでした。 ".$val_f-name." (".$f->strHttpCode.":".$f->message.")";
						break;
					}
				}
			}

			return $ret;
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
