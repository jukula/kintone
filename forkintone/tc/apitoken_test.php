<?php
/*****************************************************************************/
/* 見積印刷PHP                                                (Version 1.00) */
/*   ファイル名 : ccmitumori.php                       		                 */
/*   更新履歴   2014/08/18  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_TB1_COUNT" , 8 );  //見積テーブル
	define( "TC_TB2_COUNT" , 5 );   //工事費
	define( "TC_TB3_COUNT" , 4 );   //その他
	define( "TC_TB4_COUNT" , 13 );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	$clsSrs->paraRecId = $_REQUEST['ptRecId'];
	$clsSrs->paraAppId = $_REQUEST['ptAppId'];
	$clsSrs->paraState = $_REQUEST['ptState'];

// 実行
	$clsSrs->main();



	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraRecId		= null; 	// 案件レコード番号（パラメタ）
		var $paraAppId			= null; 	// アプリＩＤ（パラメタ）
		var $paraState		= null;
		var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// ----------------------------------------------
			// 更新権限のない別アプリフィールドをAPIトークン指定して更新してみるテスト
			// ----------------------------------------------
			$k = new TcKintone("","","","","","","LQG8BHjExMRbEHAwZEZORYRgRAEM9nRVTsc1amdV");	// 最後にAPIトークンを指定
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->paraAppId;				// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraRecId;
			$recObj->record->APIトークン更新用 = $this->common->valEnc($this->paraState);

			$updData 			= new stdClass;
			$updData->app 		= $this->paraAppId;
			$updData->records[] = $recObj;
			$k->aryJson = $updData;
			$jsonSNKK = $k->runCURLEXEC( TC_MODE_UPD );


		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";
			
			$date = explode("-",$pDat->見積日->value);

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('A4'	,	$pDat->見積先名->value. "" )
			            ->setCellValue('BD2',	$date[0]."年".$date[1]."月".$date[2]."日" )
			            ->setCellValue('F11',	$pDat->工事件名->value )
			            ->setCellValue('F13',	$pDat->工事場所->value )
			            ->setCellValue('F15',	$pDat->工事概要->value )
			            ->setCellValue('AI11',	$pDat->工事期間->value )
			            ->setCellValue('AI13',	$pDat->支払条件->value )
			            ->setCellValue('AI15',	$pDat->有効期限->value );

			//--------------------------------
			// 工事名称一覧出力(1ページ目)
			//--------------------------------
$DEBUG = false;
if($DEBUG){ echo "---------------- debug message ----------------<br>★出力対象の見積情報↓<br>"; }
/*
			$stack = array_filter($stack);
			// 工事名称を見積書の1ページ目に出力
			$n = 1;		// 工事名称NO
			$y1 = 23;	// 出力開始行(鏡)
			$y2 = 45;	// 出力開始行(明細)
			foreach ( $stack as $key => $val ) {
			    // PHPExcel出力設定
			    $val = explode("$$",$val);
			    if ( $val[0] == "undefined" ) { $val[0] = ""; }
			    if ( $val[1] == "undefined" ) { $val[1] = ""; }
			    if ( $val[2] == "undefined" ) { $val[2] = ""; }
			    $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('A'.$y1 , $n )
			                                                   -> setCellValue('B'.$y1 , $val[0] )
			                                                   -> setCellValue('J'.$y1 , $val[1] )
			                                                   -> setCellValue('Z'.$y1 , $val[2] )
			                                                   -> setCellValue('R'.$y1 , 1 )
			                                                   -> setCellValue('T'.$y1 , "式" );

if($DEBUG){ echo "<br>". $val[0] . "<br>"; }

			    //--------------------------------
			    // 工事内訳出力設定
			    //--------------------------------
			    if ( $val[3] ) {

			        switch ($val[3]) {
			            case "外壁塗装テーブル":  $id = '1_1'; break;
			            case "外壁付帯テーブル1": $id = '1_2'; break;
			            case "外壁付帯テーブル2": $id = '1_3'; break;
			            case "屋根塗装テーブル":  $id = '2_1'; break;
			            case "屋根付帯テーブル1": $id = '2_2'; break;
			            case "屋根付帯テーブル2": $id = '2_3'; break;
			            case "仮設工事テーブル":  $id = '3'; break;
			            default: break;
			        }
			        
			        // 鏡への工事ごとの金額を出力設定
			        $fld = "小計".$id;
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('W'.$y1 , $pDat->$fld->value );

			        // 工事名称の見出し出力設定
			        //  - 太字・フォントサイズ変更
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('A'.$y2 , $n ) -> setCellValue('B'.$y2 , $val[0] );
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('A'.$y2) -> getFont() -> setBold(true);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setBold(true);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('A'.$y2) -> getFont() -> setSize(14);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setSize(14);
			        if ( ($y2 % 38) == 1 ) { $y2 += 6; } else { $y2 += 2; }


			        // 工事種別の出力設定
			        //  - [工事種別]は任意入力なので、未入力の場合は処理しない
			        //  - 太字・セル結合
			        $fld = "工事種別".$id;
			        if ( $pDat->$fld->value ) {
			            $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('B'.$y2 , $pDat->$fld->value );
			            $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setBold(true);
			            $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setSize(14);
			            $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> mergeCells('B'.$y2.":Z".$y2);
			            if ( ($y2 % 38) == 1 ) { $y2 += 6; } else { $y2 += 2; }
			        }

			        // 工事ごとの内訳情報を1行毎処理
			        foreach ( $pDat->$val[3]->value as $key => $row ) {
			            $fld = array();
			            array_push($fld, "品名".$id, "仕様".$id, "数量".$id, "単位".$id, "売単価".$id, "見積金額".$id, "備考".$id);
if($DEBUG){ echo($row->value->$fld[0]->value. "<br>"); }
			            $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('B'.$y2 , $row->value->$fld[0]->value )
			                                                           -> setCellValue('J'.$y2 , $row->value->$fld[1]->value )
			                                                           -> setCellValue('R'.$y2 , $row->value->$fld[2]->value )
			                                                           -> setCellValue('T'.$y2 , $row->value->$fld[3]->value )
			                                                           -> setCellValue('U'.$y2 , $row->value->$fld[4]->value )
			                                                           -> setCellValue('W'.$y2 , $row->value->$fld[5]->value )
			                                                           -> setCellValue('Z'.$y2 , $row->value->$fld[6]->value );
			            // インクリ
			            if ( ($y2 % 38) == 1 ) { $y2 += 6; } else { $y2 += 2; }
			        }

			        //----------------------------------------------------------------
			        // 1工事分の見積内訳出力設定終わり
			        //----------------------------------------------------------------

			        if ( ($y2 % 38) == 1 ) {
			            $y2 += 6;					// 空き行がない場合、改ページ
			        } else if ( ($y2 % 38 ) > 30 ) {
			            $y2 += (45 - ($y2%38));		// 残り5行分しか空きがない場合、改ページ
			        } else {
			            $y2 += 2;					// 空きに余裕がある場合
			        }

			    } else {
			        // 【現場管理諸経費】の場合
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('W'.$y1 , $pDat->諸経費->value );
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('A'.$y2 , $n ) -> setCellValue('B'.$y2 , $val[0] ) -> setCellValue('R'.$y2 , 1 ) -> setCellValue('T'.$y2 , "式" ) -> setCellValue('W'.$y2 , $pDat->諸経費->value );
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('A'.$y2) -> getFont() -> setBold(true);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setBold(true);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('A'.$y2) -> getFont() -> setSize(14);
			        $pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle('B'.$y2) -> getFont() -> setSize(14);

			    }

			    // 次の工事名称の処理を行う為必要項目をインクリメント
			    $y1 += 2;
			    $n++;
			}


			// 見積内訳書の小計行
			// そのシートの最後の行をサーチ
			$y2 += (39 - ($y2%38));
			$pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('J'.$y2 , "小　　　　計" ) -> setCellValue('W'.$y2 , $pDat->見積合計金額->value );
			$pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle( 'A'.$y2.':AF'.$y2 ) -> getBorders() -> getTop() -> setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
			$pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> getStyle( 'J'.$y2 ) -> getAlignment() -> setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );


			// 日付(見積書出力日付)
//			$pPHPExcel -> setActiveSheetIndex( $pSheetNo ) -> setCellValue('Y7' , date("Y年n月j日") );
*/
if($DEBUG){ echo "<br>-----------------------------------------------<br><br>"; }




			return ($ret);
		}


	}


?>
