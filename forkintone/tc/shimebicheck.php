<?php
/*****************************************************************************/
/* TC保守管理に登録されている顧客の締日情報をチェックするPHP  (Version 1.01) */
/*   ファイル名 : shimebicheck.php                                           */
/*   更新履歴   2013/10/04  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

/* 削除
	define("TC_RITU_SKETA"	, 1 ); // 売上％、粗利％の少数桁数

	define("TC_SEK_TNI_NEN"		, '年');	// 契約、積算単位：年
	define("TC_SEK_TNI_TUKI"	, '月');	// 契約、積算単位：月
	
	define("TC_SKEI_KBN_URI"	, '売上' ); // 売上
	define("TC_SKEI_KBN_GEN"	, '原価' ); // 原価
	define("TC_SKEI_KBN_ARA"	, '粗利' ); // 粗利
*/

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsHsUr = new TcShimebicheck();

	// -----
	// 実行
	// -----
	$clsHsUr->main();


	// 画面メッセージ表示
	if( $clsHsUr->HosyuGmnKbn ) {
		//
	} else {
		echo "<br>";
		echo "処理が終了しました。<br>";
		echo "<br>";
		echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
		echo "</body></html>";
	}

	// クラスを開放する
	$clsHsUr = null;


	/*****************************************************************************/
	/* クラス                                                                    */
	/*****************************************************************************/
	class TcShimebicheck
	{
	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraHosyuID		= null; 	// 保守レコード番号（パラメタ）
		var $HosyuGmnKbn		= false;	// 保守管理画面から呼ばれたか？
		var $arrHskrRecno		= array();	// 処理対象の捕手管理レコード番号
		var $uriData 			= array();	// データ更新前編集
		var $uriDataRuiKei		= array();	// データ更新前編集(累計)
		var $stockData			= array();	// ストック売上管理
		var $kykupdM			= array();	// 契約更新月（本年分のみ） 本年：このphpを実行した日(年)。

	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcShimebicheck() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// ------------------------------------------------
			// TC保守管理の顧客を読み込む
			// ------------------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;		// 保守管理 TC保守管理
			$k->arySelFileds	= array("レコード番号" , "顧客コード");

			// 取得件数制限ごとにループして処理を行う。
			$aryTmp = array();
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
				$aryQ 	= array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				$json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $json->records as $key => $rec ) {
					if( $rec->顧客コード->value == "" ) {
						// 顧客未選択
					} else {
						$aryTmp[] = $rec->顧客コード->value;
					}
				}
			} while( $k->intDataCount > 0 );

			// 読込件数制限ごとにまとめる
			$idxM = 0;
			$idxR = 0;
			$aryKCode = array();
			foreach( array_unique( $aryTmp ) as $key => $val ) {
				// 更新用配列へ
				$aryKCode[ $idxM ][ $idxR ] = $val;

				$idxR += 1;
				if( $idxR == CY_UPD_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
			}

			// ------------------------------------------------
			// TC顧客情報から請求関連情報を取得する。
			// ------------------------------------------------
			$aryErrDat = array();						// エラーのデータ

			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;		// 保守管理 TC保守管理
			$k->arySelFields	= array("レコード番号" , "顧客コード" , "顧客名" , "締め日" , "回収サイト" , "回収日" , "_自動_回収サイト");

			foreach( $aryKCode as $key => $val ) {
			    $k->strQuery = "顧客コード in (".implode( $val , ',').")";
				$json = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $json->records as $key => $rec ) {
					// 請求に関する情報があるかどうかをチェックする。
					if( ($rec->締め日->value == "") || ($rec->回収サイト->value == "")  || ($rec->回収日->value == "") ){
						$aryTmp = array();
						$aryTmp[] = "顧客コード："		.$rec->顧客コード->value;
						$aryTmp[] = "顧客名："			.$rec->顧客名->value;
						$aryTmp[] = "締め日："			.$rec->締め日->value;
						$aryTmp[] = "回収サイト："		.$rec->回収サイト->value;
						$aryTmp[] = "回収日："			.$rec->回収日->value;
						$aryTmp[] = "(自動)回収サイト："	.$rec->_自動_回収サイト->value;
						$aryErrDat[] = implode( $aryTmp , "　" );
					}
				}
			}
print_r($aryErrDat);
			return ( implode($aryErrDat,"<br>\n") );
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function getDateNenGetsu( $pDate ) {
			$ret = null;

			if( isNull($val) ){
				//
			} else {
				$ret = date( "Ym", $pDate );
			}

			return ( $ret );
		}

		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			if( is_array($val) ){
				foreach( $val as $key => $encwk ) {
					$wk->value[] = mb_convert_encoding($encwk , "UTF-8", "auto");
				}
			} else {
				$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			}
			return ( $wk );
		}

		function SendUrl( $pUrl) {
			$ret = null;
			$cHand = null;

			// ----------------
			// http通信の初期化
			// ----------------
			$cHand = curl_init( $pUrl );
			if( $cHand ) {
				//
			} else {
				$this->err->setError( ERR_T000 );
				return( $ret );
			}

			$this->aryOptionHeader = array(
				"Host: timeconcier.jp:443" ,
			);
			// オプション設定
			curl_setopt($cHand, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($cHand, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($cHand, CURLOPT_SSLVERSION, 3);
			curl_setopt($cHand, CURLOPT_HEADER, false);
			curl_setopt($cHand, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($cHand, CURLOPT_FAILONERROR, true);
			curl_setopt($cHand, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

			// ------------------
			// HTTP通信を実行する
			// ------------------
			$respons = curl_exec($cHand);

			// HTTPレスポンスのステータスコードを標準出力する
			$strHttpCode = curl_getinfo($cHand , CURLINFO_HTTP_CODE);

			// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
			curl_close($cHand);

			// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
			if ( $strHttpCode == 200 ) {
				$ret = $respons;
			}

			return( $ret );
		}

	}

?>
