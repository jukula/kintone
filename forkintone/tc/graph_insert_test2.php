<?php
	/*****************************************************************************/
	/* 集計PHP                    	       		                  (Version 1.00) */
	/*   ファイル名 : zaseki.php                                      			 */
	/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
	/*   [備考]                                                                  */
	/*      tcutility.incを必ずインクルードすること                              */
	/*   [必要ファイル]                                                          */
	/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
	/*                                                                           */
	/*                                                                           */
	/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
	/*****************************************************************************/

	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	define( "TC_URL_AK" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCANKK."/show#record=" ); // 案件管理URL
	define( "TC_URL_ST" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHUIT."/show#record=" ); // 案件管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();

// 実行
	if($_POST['Submit']){
		$clsSrs->main();
	}

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }


		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {

			echo "データ作成処理を開始します。このまましばらくお待ちください。<br><br>\n";

			// ----------------------------------------------------
			// TC案件管理グラフのデータを削除する
			// ----------------------------------------------------
			$aryAnkgRecno = array();

			// TC案件管理グラフアプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= 2772;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "( レコード番号 = 1521 )";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_del[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json_del[$dtcount]->records as $key => $rec ) {
					$aryRmgkRecno[] = $rec->レコード番号->value;
				}

				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_del[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ

			} while( $k->intDataCount > 0 );

			for($i = 0; $i <= count($json_del[0]->records[0]->ユーザー選択->value); $i++) {
				echo $json_del[0]->records[0]->ユーザー選択->value[$i]->code."<br>";
			}
		}
	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>集計表出力</title>
 </head>
 <body>
	<p class="txt">
        集計表作成対象の年を選択し、作成を押してください。<br>
    </p>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<select name="yaer" size="1">
<?php
    $i = -2;
	while ( $i <= 7) {
	    $year = date("Y") + $i;
        if ( $year == date("Y") ) {
     	    echo '<option value="' . $year . '" selected="selected">' . $year . '</option>';
		} else {
     	    echo '<option value="' . $year . '">' . $year . '</option>';
		}
        $i++;
	}
?>
		</select>
		年

		<input type="submit" name="Submit" value="作成" >
	</form>
 </body>
</html>


