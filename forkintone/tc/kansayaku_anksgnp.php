<?php
/*****************************************************************************/
/* 監査役提出用 作業日報集計PHP                               (Version 1.01) */
/*   ファイル名 : kansayaku_anksgnp.php                                      */
/*   更新履歴   2013/07/26  Version 1.00(T.M)                                */
/*                                                                           */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new Tckansayaku_AnkSgnp();
	

	// 実行
	$clsSrs->main();

	class Tckansayaku_AnkSgnp
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function Tckansayaku_AnkSgnp() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			$Rmgk = $this->getRmgk();
//print_r($Rmgk);
			$Ankk = $this->getAnkk();
print_r($Ankk);

			return;
		}


		/*************************************************************************/
	    /* 労務原価をを読み込む                                                  */
	    /*  引数	                                                             */
	    /*  関数値  array 		作業日報データ                                   */
	    /*************************************************************************/
		function getRmgk() {
			$ret = array();;

			// ----------------------------------
			// 労務原価から実行労務費を集計する。
			// ----------------------------------
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCRMGK;				// アプリID
		    $k->strQuery    	= ""; 	// クエリパラメータ
		    $k->arySelFields   	= array( "レコード番号" , "作業日付" , "作業担当者名" , "案件レコード番号" , "経過時間_分_案件" );

			$recno = 0;
			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$ret_json = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				$recno = $ret_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// ------------------------------------------------
				// 戻り値にデータを設定する
				// ------------------------------------------------
				foreach( $ret_json->records as $key => $rec ) {
					$ret[] = $rec;
				}

			} while( $k->intDataCount > 0 );

			return ( $ret );

		}

		/*************************************************************************/
	    /* 案件管理から案件情報を読み込む                                        */
	    /*  引数	                                                             */
	    /*  関数値  array 		作業日報データ                                   */
	    /*************************************************************************/
		function getAnkk() {
			$ret = array();;

			// ----------------------------------
			// 案件管理から案件情報を読み込む
			// ----------------------------------
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;				// アプリID
		    $k->strQuery    	= ""; 	// クエリパラメータ
		    $k->arySelFields   	= array( "レコード番号", "顧客名", "案件名", "内示日", "受注日", "進捗状況" );

			$recno = 0;
			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$ret_json = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				$recno = $ret_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// ------------------------------------------------
				// 戻り値にデータを設定する
				// ------------------------------------------------
				foreach( $ret_json->records as $key => $rec ) {
					$ret[] = $rec;
				}

			} while( $k->intDataCount > 0 );

			return ( $ret );

		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
