<?php
/*****************************************************************************/
/* 労務原価集計PHP                                            (Version 1.01) */
/*   ファイル名 : romutotal.php                                              */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*              2013/04/30  Version 1.01(T.M)                                */
/*                          updANKK 0クリア処理 追加                         */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcSyncRmgkSgnp();
	
	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。
	$clsSrs->AnkenGmnKbn = array_key_exists( 'ankenid' , $_REQUEST );
	if( $clsSrs->AnkenGmnKbn ) {
		$clsSrs->paraAnkenID = $_REQUEST['ankenid'];
	} else {
		$clsSrs->paraAnkenID = null;
	}
	$clsSrs->kensu_Sgnp = 0;

	// 実行
	$clsSrs->main();

	class TcSyncRmgkSgnp
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $AnkenGmnKbn		= false;	// 案件管理画面から呼ばれたか？
	    var $dateSabun      	= null;     // 差分読み込み用
		
		var $kensu_Sgnp			= 0;		// 作業日報の処理対象件数
		var $kensu_Rmgk			= 0;		// 労務原価の処理対象件数
		
	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcSyncRmgkSgnp() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// 案件管理を更新するため、
			// 更新対象の作業日報から集計対象の案件を絞り込む。
			if( $this->AnkenGmnKbn ) {
				//
			} else {
				// 画面メッセージ表示
				echo "<html>\n";
				echo "<meta http-equiv='content-type' content='text/html; charset=UTF-8'>\n";
				echo "<head></head>\n";
				echo "<body>\n";
				echo "作業日報から労務原価（案件）データを作成しています。<br><br>\n";
				echo "処理開始…<br><br>\n";
				echo str_pad(" " , 256);
				flush();
			}

		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

			// ----------------------------------------------
			// 更新対象の作業日報をメインに以降の処理を行う。
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TCSGNP;	// アプリID（作業日報）

			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			// 取得件数制限ごとにループして処理を行う。
			$rmgk_date = $this->getRMGK_newRegistDate();
			do {
				// 検索条件を作成する。
				$aryQ = array();
			    if( $this->AnkenGmnKbn ) {
					// 案件管理から呼ばれた場合、案件を特定する。
					$aryQ[] = "(案件レコード番号 = $this->paraAnkenID)";
				} else {
					if( $rmgk_date == null ) {
						//
					} else {
						$aryQ[] = "( (作成日時 >= \"".$rmgk_date."\") or (更新日時 >= \"".$rmgk_date."\") )";
					}
				}

				$aryQ[] = "( レコード番号 > $recno )";
				//$aryQ[] = "( 経過時間_分_案件 > 0 )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$sgnp_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->strHttpCode == 200 ) {
				} else {
					echo "作業日報の読込に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}

				if( $this->AnkenGmnKbn ) {
					//
				} else {
					// 処理に時間が掛かるので表示
					echo "・";
					echo str_pad(" " , 256);
					flush();
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_Sgnp	+= $k->intDataCount;
				$recno 			 	 = $sgnp_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// ------------------------------------------------
				// 作業日報に紐づく労務原価を一旦削除し、追加する。
				// ------------------------------------------------
				$aryDelRecno = array();
				foreach( $sgnp_json->records as $key => $rec ) {
					$aryDelRecno[] = $rec->レコード番号->value;
				}

				$this->delRMGK( $aryDelRecno );		// 削除
				$this->insRMGK( $sgnp_json );		// 追加

			} while( $k->intDataCount > 0 );

			// --------------------------------------------------
			// 作業日報と労務原価の同期する。
			// 削除された作業日報を労務原価から削除する。
			// レスポンスを考慮し、案件管理画面から呼ばれた時は、
			// 労務原価全体を作業日報と同期しない。
			// --------------------------------------------------
			if( $this->AnkenGmnKbn ) {
				$this->sycSgnpRmgk();
			} else {
				echo "削除データの同期中…<br><br>\n";
				echo str_pad(" " , 256);
				flush();

			    // 出力バッファの内容を送信する
			    @ob_flush();
			    @flush();

				$this->sycSgnpRmgk();
				
				echo "案件労務原価を集計中…<br><br>\n";
				echo str_pad(" " , 256);
				flush();

			    // 出力バッファの内容を送信する
			    @ob_flush();
			    @flush();

			}

			// --------------------------------------------------
			// 案件管理を更新（集計）する。
			// --------------------------------------------------
			$rmgkKei = $this->updANKK();

			// --------------------
			// 終了メッセージ
			// --------------------
			// 案件管理画面から呼ばれた場合は、集計結果を返す。
			if( $this->AnkenGmnKbn ) {
				echo ("var phpRet = '".$rmgkKei."';\n");
			} else {
				echo "<br>";
				echo "処理が終了しました。<br>";
				echo "<br>";
				echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
				echo "</body></html>";
				echo str_pad(" " , 256);
				flush();
			}

		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

			return;
		}

		/*************************************************************************/
	    /* 労務原価から最新の作成日付を取得する                                  */
	    /*  引数	なし                                                         */
	    /*  関数値  string		正常終了:労務原価の最終作成日時、異常終了:null   */
	    /*************************************************************************/
		function getRMGK_newRegistDate() {
			$ret = null;

			// 労務原価データの最終作成日付を取得する
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TCRMGK;			// アプリID
		    $k->arySelFields   = array("作成日時"); 	// 読込用フィールドパラメータ

		    // 案件画面から呼ばれた場合は、その案件のみを対象とする。
			if( $this->AnkenGmnKbn ) {
				$k->strQuery = " (案件レコード番号 = $this->paraAnkenID)";
			} else {
				$k->strQuery = "";
			}
		    $k->strQuery .= " order by 作成日時 desc limit 1"; // クエリパラメータ

			$json = $k->runCURLEXEC( TC_MODE_SEL );

			if( $k->intDataCount == 0 ) {
				//
			} else {
				$ret = $json->records[0]->作成日時->value;
			}

			return ( $ret );
		}

		/*************************************************************************/
	    /* アプリの項目の最大値、最小値を取得する                                */
	    /*  引数	$appid    	アプリＩＤ                                       */
	    /*       	$fld    	取得するフィールドコード                         */
	    /*       	$kbn    	最小(CY_SEL_MIN)、最大(CY_SEL_MAX) 取得区分      */
	    /*  関数値  $fld値      データあり：$fld値、なし：null                   */
	    /*************************************************************************/
		function getAppRecno( $appid , $fld , $kbn ) {
			$ret = null;

			// 労務原価データの最終作成日付を取得する
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= $appid;						// アプリID
		    $k->arySelFields	= array( $fld ); 				// 読込用フィールドパラメータ
		    $k->strQuery 		= "order by $fld $kbn limit 1";	// クエリパラメータ

			$json = $k->runCURLEXEC( TC_MODE_SEL );
			if( $k->intDataCount == 0 ) {
				//
			} else {
				$ret = $json->records[0]->$fld->value;
			}

			return ( $ret );
		}

		/*************************************************************************/
	    /* 作業日報のレコード番号を指定して労務原価を削除する                    */
	    /*  引数	array  		作業日報のレコード番号                           */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function delRMGK( &$aryDelRecno ) {
			$ret = false;

			// ----------------------------------------------------------------
			// パラメタが長くなり過ぎないように、CY_DEL_MAX個ごとに処理をする。
			// ----------------------------------------------------------------
			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			$arySgnpRecno = array();
			foreach( $aryDelRecno as $key => $val ) {
				if( isNull($val) ){
					//
				} else {
					$arySgnpRecno[ $idxM ][ $idxR ] = $val;
					$idxR += 1;
					if( $idxR == CY_SEL_MAX ) {
						$idxM += 1;
						$idxR = 0;
					}
				}
			}

			// ----------------------------------------------------
			// 労務原価から削除対象のデータ(レコード番号)を取得する
			// ----------------------------------------------------
			$aryRmgkRecno = array();

			// kintone データ読込実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->strHeader		= "";					// ヘッダパラメタ
			$k->intAppID 		= TC_APPID_TCRMGK;		// アプリID
		    $k->arySelFileds	= array( "レコード番号" );

			foreach( $arySgnpRecno as $key => $sgnprecs ) {
				$recno = 0;
				do {
					$aryQ 	= array();
					$aryQ[] = "( レコード番号_作業日報入力_ in ( ".implode( $sgnprecs , ",")." ) )";
					$aryQ[] = "( レコード番号 > $recno )";
				    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

					$json = $k->runCURLEXEC( TC_MODE_SEL );
					if( $k->intDataCount == 0 ) {
						break;
					}
					foreach( $json->records as $key => $rec ) {
						$aryRmgkRecno[] = $rec->レコード番号->value;
					}

					// 次に読み込む条件の設定
					$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;


				} while( $k->intDataCount > 0 );
			}

			// --------
			// 削除実行
			// --------
			$this->delData( TC_APPID_TCRMGK , $aryRmgkRecno );

			$ret = true;

			return( $ret );
		}


		/*************************************************************************/
	    /* 指定したアプリのデータを削除する                                      */
	    /*  引数	$appid    		アプリＩＤ                                   */
	    /*       	$pDelRecno   	削除対象のレコード番号                       */
	    /*  関数値  string			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function delData( $appid , &$pDelRecno ) {
			// -----------------------------------------------
			// CY_DEL_MAX個ごとに処理をする。（kintoneの制限）
			// -----------------------------------------------
			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			$aryDelRmgk = array();
			foreach( $pDelRecno as $key => $val ) {
				$aryDelRmgk[ $idxM ][ $idxR ] = $val;
				$idxR += 1;
				if( $idxR == CY_DEL_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
			}
			// 削除実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->intAppID 		= $appid;				// アプリID
			$k->aryDelRecNo		= array();
			foreach( $aryDelRmgk as $key => $val ) {
	    		$k->aryDelRecNo = $val;					// 削除対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_DEL );
			}
		}


		/*************************************************************************/
	    /* 更新対象の作業日報を労務原価へ追加する                                */
	    /*  引数	$sgnp_json  作業日報データを参照する                         */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insRMGK( &$sgnp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsRmgk = array();

			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			for( $i=0; $i < $tcRec->getNumber(); $i++ ) {
				$tbl = $tcRec->getFieldValue( $i , "作業内容_案件_テーブル" );
				// 作業日報項目 -> 労務原価項目
				for( $j=0; $j < $tbl->getNumber(); $j++ ) {
					// 作業時間が 0時間 のものは対象外とする。
					if( $tbl->getFieldValue( $j , "経過時間_分_案件" ) == 0 ) {
						continue;
					}
					// 書込み準備
					$recObj = new stdClass;
					// 通常項目
					$recObj->レコード番号_作業日報入力_ = $this->tgfEnc( $tcRec , $i , "レコード番号" );
					$recObj->作業日付 					= $this->tgfEnc( $tcRec , $i , "作業日付" );
					$recObj->作業担当者名				= $this->tgfEnc( $tcRec , $i , "作業担当者名" );
					$recObj->更新日時_作業日報入力_		= $this->tgfEnc( $tcRec , $i , "更新日時" );
					// テーブル項目
					$recObj->案件レコード番号		= $this->tgfEnc( $tbl , $j , "案件レコード番号" );
//					$recObj->顧客コード				= $this->tgfEnc( $tbl , $j , "顧客コード_案件_" );	// 2013.9.4
					$recObj->顧客名_案件_			= $this->tgfEnc( $tbl , $j , "顧客名_案件_" );
					$recObj->案件名					= $this->tgfEnc( $tbl , $j , "案件名" );
					$recObj->社内社外_案件_			= $this->tgfEnc( $tbl , $j , "社内社外_案件_" );	// kintone画面で必須
					$recObj->対応区分_案件_			= $this->tgfEnc( $tbl , $j , "対応区分_案件_" );	// kintone画面で必須
					$recObj->製品区分_案件_			= $this->tgfEnc( $tbl , $j , "製品区分_案件_" );	// kintone画面で必須
					$recObj->作業区分_案件_			= $this->tgfEnc( $tbl , $j , "作業区分_案件_" );
					$recObj->作業内訳_案件_			= $this->tgfEnc( $tbl , $j , "作業内訳_案件_" );
					$recObj->開始時刻１				= $this->tgfEnc( $tbl , $j , "開始時刻1_1" );
					$recObj->終了時刻１				= $this->tgfEnc( $tbl , $j , "終了時刻1_1" );
					$recObj->開始時刻２				= $this->tgfEnc( $tbl , $j , "開始時刻1_2" );
					$recObj->終了時刻２				= $this->tgfEnc( $tbl , $j , "終了時刻1_2" );
					$recObj->開始時刻３				= $this->tgfEnc( $tbl , $j , "開始時刻1_3" );
					$recObj->終了時刻３				= $this->tgfEnc( $tbl , $j , "終了時刻1_3" );
					$recObj->開始時刻４				= $this->tgfEnc( $tbl , $j , "開始時刻1_4" );
					$recObj->終了時刻４				= $this->tgfEnc( $tbl , $j , "終了時刻1_4" );
					$recObj->除外時間				= $this->tgfEnc( $tbl , $j , "除外時間１" );
					$recObj->経過時間_分_案件		= $this->tgfEnc( $tbl , $j , "経過時間_分_案件" );
					$recObj->労務単価				= $this->tgfEnc( $tbl , $j , "労務単価１" );
					$recObj->実行労務費_案件_円		= $this->tgfEnc( $tbl , $j , "実行労務費_案件_円" );
					$recObj->作業内容_案件_			= $this->tgfEnc( $tbl , $j , "作業内容_案件_" );
					$aryInsRmgk[ $idxM ][ $idxR ] = $recObj;
					$idxR += 1;
					if( $idxR == CY_INS_MAX ) {
						$idxM += 1;
						$idxR = 0;
					}
					$recObj = null;
				}
			}
			// --------------------
			// 労務原価へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCRMGK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryInsRmgk as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCRMGK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
				} else {
					echo "労務原価の追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				$rmgkInsKensu += count( $val );
			}
		}

		/*************************************************************************/
	    /* 作業日報と労務原価を同期する                                          */
	    /*  引数	                                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function sycSgnpRmgk() {
			$ret = false;

			//---------------------------------------
			// 作業日報のレコード番号を全て取得する。
			//---------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSGNP;		// アプリID（作業日報）
			$k->arySelFileds	= array(レコード番号);

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			$arySgnpRecno = array();
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
			    $k->strQuery = "レコード番号 > $recno order by レコード番号 asc";
				$sgnp_json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $sgnp_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $sgnp_json->records as $key => $rec ) {
					$arySgnpRecno[] = $rec->レコード番号->value;
				}
			} while( $k->intDataCount > 0 );

			//---------------------------------------
			// レコード番号を取得する。
			//---------------------------------------
			// 作業日報の最小、最大レコード番号
			if( count( $arySgnpRecno ) == 0 ) {
				$sgnpMinRecno = 0;
				$sgnpMaxRecno = 0;
			} else {
				$sgnpMinRecno = $arySgnpRecno[0];
				$sgnpMaxRecno = $arySgnpRecno[ count($arySgnpRecno) -1 ];
			}

			// 労務原価の最小、最大レコード番号
			$rmgkMinRecno = $this->getAppRecno( TC_APPID_TCRMGK , "レコード番号_作業日報入力_" , CY_DAT_MIN );
			$rmgkMaxRecno = $this->getAppRecno( TC_APPID_TCRMGK , "レコード番号_作業日報入力_" , CY_DAT_MAX );

			//-------------------------------------------
			// 作業日報のレコード番号から欠番を抽出する。
			// （削除対象のレコード番号）
			//-------------------------------------------
			$aryDelRecno = array();
			// 労務原価_最小 < 作業日報_最小（労務原価_最小 から 作業日報_最小 までが削除対象。）
			for( $i = $rmgkMinRecno; $i < $sgnpMinRecno; $i++ ) {
				$aryDelRecno[] = $i;
			}
			// 作業日報_最大 < 労務原価_最大（作業日報_最大 から 労務原価_最大 までが削除対象。）
			for( $i = $sgnpMaxRecno + 1; $i <= $rmgkMaxRecno; $i++ ) {
				$aryDelRecno[] = $i;
			}
			// 作業日報_最小 から 作業日報_最大 までの欠番を探す。（削除対象）
			for( $i=0; $i < count($arySgnpRecno) - 1; $i++ ) {
				$strRec = $arySgnpRecno[ $i ];
				$endRec = $arySgnpRecno[ $i + 1 ];
				//
				$ketu = $endRec - $strRec;
				if( $ketu > 1 ) {
					for( $delRec = $strRec + 1 ; $delRec < $endRec; $delRec++ ) {
						$aryDelRecno[] = $delRec;
					}
				}
			}

			// --------
			// 削除実行
			// --------
			if( count($aryDelRecno) > 0 ) {
				$this->delRMGK( $aryDelRecno );
			}

			$ret = true;
			return ( $ret );
		}

		/*************************************************************************/
	    /* 更新対象の労務原価を集計し、案件管理を更新する                        */
	    /*  引数	&$aryAnkRecno  	更新対象の案件レコード番号                   */
	    /*      	$updClearKbn  	0更新区分  true:0クリア、false:集計          */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function updANKK() {
			$ret = 0;

			$aryAnkRecno	= array();	// 全案件
			$aryRmgkKei 	= array();	// 労務原価計(全案件)
			$aryAnkJiban	= array();	// 案件(地盤調査) (必要なものだけ)

			if( $this->paraAnkenID == "" ) {
				$qryAnk = "";
			} else {
				$qryAnk = "レコード番号 = ".$this->paraAnkenID." and ";
			}
			//---------------------------------------
			// 案件管理のレコード番号を全て取得する。
			//---------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;		// アプリID（案件管理）
			$k->arySelFileds	= array( "レコード番号" , "案件管理_地盤調査_レコード番号" );

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
			    $k->strQuery = $qryAnk."レコード番号 > $recno order by レコード番号 asc";
				$sgnp_json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $sgnp_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $sgnp_json->records as $key => $rec ) {
					$aryAnkRecno[ $rec->レコード番号->value ] = $rec->案件管理_地盤調査_レコード番号->value;
				}
			} while( $k->intDataCount > 0 );

			// ------------------------------------------------
			// 対象案件が多い場合を考慮して分割して処理をする。
			// 集計結果の初期化もする。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryUpdAnkRecno = array();
			foreach( $aryAnkRecno as $ankRecno => $val ) {
				if( isNull($ankRecno) ) {
				} else {
					$aryUpdAnkRecno[ $idxM ][ $idxR ] = $ankRecno;
					$idxR += 1;
					if( $idxR == CY_SEL_MAX ) {
						$idxM += 1;
						$idxR = 0;
					}
					// 集計結果を初期化
					$aryRmgkKei[ $ankRecno ] = 0;
				}
			}
			// ----------------------------------
			// 労務原価から実行労務費を集計する。
			// ----------------------------------
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCRMGK;		// アプリID
		    $k->strQuery    	= ""; 					// クエリパラメータ
		    $k->arySelFields   	= array( "案件レコード番号" , "実行労務費_案件_円" ,"レコード番号" );
			//
			foreach( $aryUpdAnkRecno as $key => $val ) {
				$recno = 0;
				do {
					$aryQ = array();
					$aryQ[] = "( 案件レコード番号 in (".implode( $val , "," ).") )";
					$aryQ[] = "( レコード番号 > $recno )";
				    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

					$rmgk_json = $k->runCURLEXEC( TC_MODE_SEL );

					if( $k->strHttpCode == 200 ) {
						if( $k->intDataCount == 0 ) {
							break;
						}
					} else {
						echo "労務原価の読込に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
						break;
					}

					// 次に読み込む条件の設定
					$recno = $rmgk_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

					// 集計
					foreach( $rmgk_json->records as $key => $rec ) {
						$aryRmgkKei[ $rec->案件レコード番号->value ] += $rec->実行労務費_案件_円->value - 0;
					}

				} while( $k->intDataCount > 0 );
			}

			//-----------------------------------------------------------
			// 案件管理(地盤調査)の受注金額(初期)、紹介料合計を取得する。
			//-----------------------------------------------------------
			$k = new TcKintone( TC_CY_DOMAIN , TC_CY_USER , TC_CY_PASSWORD , "" , "" , TC_GUESTID_TCGC );	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;		// アプリID（案件管理(地盤調査)）
			$k->arySelFileds	= array( "レコード番号" , "受注金額_初期_" , "紹介料合計" );

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			// kintoneデータ取得件数制限の対応。
			$aryAnkj = $this->makeMaxArray( $aryAnkRecno , CY_SEL_MAX );
			foreach( $aryAnkj as $val ) {
				$recno = 0;
				do {
				    $k->strQuery = "レコード番号 in (". implode( $val , "," ) .") and レコード番号 > $recno order by レコード番号 asc";
					$json = $k->runCURLEXEC( TC_MODE_SEL );
					// 取得件数をチェックする。
					if( $k->intDataCount == 0 ) {
						break;
					}
					// 次に読み込む条件の設定
					$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

					// レコード番号を取得する
					foreach( $json->records as $key => $rec ) {
						$recObj = new stdClass;
						$recObj->レコード番号 	= $rec->レコード番号;
						$recObj->受注金額_初期_ = $rec->受注金額_初期_;
						$recObj->紹介料合計		= $rec->紹介料合計;
						$aryAnkJiban[ $rec->レコード番号->value ] = $recObj;
					}
				} while( $k->intDataCount > 0 );

				//
				if( $this->AnkenGmnKbn ) {
					//
				} else {
					// 処理に時間が掛かるので表示
					echo "・";
					echo str_pad(" " , 256);
					flush();

				    // 出力バッファの内容を送信する
				    @ob_flush();
				    @flush();

				}
			}
//print_r($aryAnkJiban); exit;

			// ----------------------------------------------------------------
			// 案件管理画面から呼ばれた場合は、案件の更新処理をせずに終了する。
			// ----------------------------------------------------------------
			if( $this->AnkenGmnKbn ) {
				// 画面表示用に集計値を返す。
				$ret = $aryRmgkKei[ $this->paraAnkenID ];

			} else {
				// ----------------------------------------------
				// 一括更新件数の制限を考慮してデータを加工する。
				// ----------------------------------------------
				$idxM = 0;
				$idxR = 0;
				$aryUpdAnk = array();
				foreach( $aryRmgkKei as $ankRecno => $val ) {
				    $recObj = new stdClass();
					$recObj->id = $ankRecno;
				    $recObj->record = new stdClass();
					$recObj->record->実行労務費累計	= $this->valEnc( $val );

					// 地盤からの案件判定
					if( array_key_exists( $aryAnkRecno[ $ankRecno ], $aryAnkJiban ) ) {
						// 地盤案件の案件金額、原価金額を設定
						$aj = &$aryAnkJiban[ $aryAnkRecno[ $ankRecno ] ];
						$recObj->record->案件金額_円_	= $this->valEnc( $aj->受注金額_初期_->value );
						$recObj->record->案件原価_円_	= $this->valEnc( $aj->紹介料合計->value );
					}

					$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
					$idxR += 1;
					if( $idxR == CY_UPD_MAX ) {
						$idxM += 1;
						$idxR = 0;
					}
					$recObj = null;
				}
//print_r( $aryUpdAnk); exit;
				// --------------------
				// 案件管理を更新する。
				// --------------------
				$k = new TcKintone();
				$k->parInit();									// API連携用のパラメタを初期化する
				$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
				$k->strContentType	= "Content-Type: application/json";
				foreach( $aryUpdAnk as $key => $val ) {
					$updData 			= new stdClass;
					$updData->app 		= TC_APPID_TCANKK;
					$updData->records 	= $val;
					$k->aryJson = $updData;						// 更新対象のレコード番号
					$json = $k->runCURLEXEC( TC_MODE_UPD );

					if( $k->strHttpCode == 200 ) {
						echo "・";
					    // 出力バッファの内容を送信する
			    		@ob_flush();
			    		@flush();
					} else {
//print_r($k);
						echo "案件管理の更新に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
						break;
					}
					//
					if( $this->AnkenGmnKbn ) {
						//
					} else {
						// 処理に時間が掛かるので表示
						echo "・";
						echo str_pad(" " , 256);
						flush();
					}
				}
			}

			return ( $ret );
		}


		/*************************************************************************/
	    /* 更新対象の労務原価を集計し、案件管理を更新する                        */
	    /*  引数																 */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function getAnkRomuHi() {
			$ret = 0;
			// ----------------------------------
			// 労務原価から実行労務費を集計する。
			// ----------------------------------
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;				// アプリID
		    $k->strQuery    	= "レコード番号 = $this->paraAnkenID"; 	// クエリパラメータ
		    $k->arySelFields   	= array( "実行労務費累計" );

			$json = $k->runCURLEXEC( TC_MODE_SEL );
			if( $k->intDataCount == 0 ) {
				break;
			} else {
				$ret = $json->records[0]->実行労務費累計->value;
			}

			return ( $ret );

		}

		/*************************************************************************/
	    /* arrayをMAX件数ごとに分割する。                                        */
	    /*  引数	&$pAry    	処理対象の配列                                   */
	    /*       	$pMax    	分割する件数                                     */
	    /*  関数値  array       $pMaxごとの2次元配列                             */
	    /*************************************************************************/
		function makeMaxArray( &$pAry , $pMax ) {
			$aryRet = array();

			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			foreach( $pAry as $key => $val ) {
				if( isNull($val) ){
					//
				} else {
					$aryRet[ $idxM ][ $idxR ] = $val;
					$idxR += 1;
					if( $idxR == $pMax ) {
						$idxM += 1;
						$idxR = 0;
					}
				}
			}

			return ( $aryRet );
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {

			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
