<?php
/*****************************************************************************/
/* 案件契約書PHP                                              (Version 1.00) */
/*   ファイル名 : keiyakusyo_anken.php                                       */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define( "TC_URL1" , "https://".TC_CY_DOMAIN."/k/guest/12/".TC_APPID_ANKJ."/show#record=" ); // 案件管理URL
	define( "TC_URL2" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCSNKK."/show#record=" ); // 請求入金管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;
	$clsSrs->paraTableNO = $_REQUEST['rowno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
	    var $paraTableNO		= null; 	// テーブル行番号（パラメタ）

		var $err;
		var $SNKKID             = null;     // 請求入金管理作成ID

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 請求先の名前保持用

			// 案件管理アプリのデータを取得
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 請求先名を保持
			$sAnkenname = $jsonANKK->records[0]->会社名->value;

			// 請求入金アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "( (請求先名 = \"".$sAnkenname."\") and ( プロセス = \"".請求書作成中."\" ) )"; // クエリパラメータ
			$jsonSNKK = $k->runCURLEXEC( TC_MODE_SEL );

// 発行単位は会社単位に固定
/*
			if(($jsonKKKK->records[0]->請求書発行単位->value == "案件毎") or ($jsonKKKK->records[0]->請求書発行単位->value == "会社単位")){

				if($jsonKKKK->records[0]->請求書発行単位->value == "案件毎"){
					$insflg = 0;	// インサートフラグ
					for($i = 0; $i < count($jsonSNKK->records); $i++) {
						$tbflg = 0;		// アップデートを対象かチェックするためのフラグ

						// テーブルをループし、レコード番号がすべてが同じならアップデートを行う
				    	for($a = 0; $a < count($jsonSNKK->records[$i]->請求明細テーブル->value); $a++) {
							if($jsonSNKK->records[$i]->請求明細テーブル->value[$a]->value->案件レコード番号->value <> $this->paraAnkenID ){
								$tbflg = 1;
							}
				    	}

						if($tbflg == 0){
							// 全部同じなのでアップデート
							$this->updSNKK( $jsonANKK , $jsonSNKK , $i);
							$insflg = 1; //アップデートを行ったのでフラグを立てる
							break;
						}
					}

					if($insflg == 1){
						// 書き込み済みなので終了
						$this->updANKK( $jsonANKK );
						$rmgkKei = $sAnkenname."の".$this->paraKubun."請求計上(案件毎)を追加しました。";
					}else{
						// 対象無しなので新規登録
						$this->insSNKK( $jsonANKK );
						$this->updANKK( $jsonANKK );
						$rmgkKei = $sAnkenname."の".$this->paraKubun."請求計上(案件毎)を新規登録しました。";
					}

					echo ("phpRet = '".$rmgkKei."';\n");
					echo $msg;
				}else{
*/
			//会社単位で動かす
			// セルへ設定
			if( $k->intDataCount > 0 ) {
				if($k->intDataCount == 1){
					// あればアップデート
					$this->updSNKK( $jsonANKK , $jsonSNKK , 0);
					$this->updANKK( $jsonANKK );
					$rmgkKei = $sAnkenname."の請求計上(会社単位)を追加しました。";
				}else{
					$rmgkKei = "FALSE";
				}
			}else{
				// なければインサート
				$this->insSNKK( $jsonANKK );
				$this->updANKK( $jsonANKK );
				$rmgkKei = $sAnkenname."の請求計上(会社単位)を新規登録しました。";
			}
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;
/*
			}

			}else{
					$rmgkKei = $sAnkenname."の請求書発行単位が設定されていないため、実行できません。顧客管理から設定してください。";
					echo ("phpRet = '".$rmgkKei."';\n");
					echo $msg;
			}
*/

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  日付型から月か日を返す                                               */
	    /*  引数	$pDat　 日付項目                                             */
	    /*  引数	$pPara　変換内容   0 = 月 / 1 = 日                           */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setDayGet( &$pDat , $pPara ) {
			$ret = "";

			$Date = explode("-", $pDat);

			if($pPara == 0 ){
				$ret = $Date[1];
			}else{
				$ret = $Date[2];
			}

			return ($ret);
		}

		/*************************************************************************/
	    /*  請求入金管理へデータを追加する                                       */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insSNKK( &$sgnp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsSnkk  = array();
			$kokyakusetflg = 0;
			$dkeizyobi = "";         // 請求予定日（初期）を格納

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			$i=0;

			// 計上日を取得し、分解する 
			$dkeizyobi  = $sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_->value;
			$ardata 	= explode("-",$sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_->value);

			$kokyaku['締め日']	   = "締め日";

			$year  = $ardata[0];
			$month = $ardata[1] + 1;
			$timestamp = mktime(0, 0, 0, $month, 0, $year);
			$last_month = date('Y-m-d', $timestamp);
			$setdata = "";

			if($sgnp_json->records[0]->$kokyaku['締め日']->value != "" && $sgnp_json->records[0]->$kokyaku['締め日']->value != 0 ){
				if( $sgnp_json->records[0]->$kokyaku['締め日']->value == "31"){
					// 締日が末の場合は末日を設定
					$setdata = $last_month;
				}else if( $ardata[2] <= $sgnp_json->records[0]->$kokyaku['締め日']->value){
					// 締日を過ぎていない場合は当月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 1 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$next_month = date('Y-m-d', $timest);
					$nextardata = explode("-",$next_month);

					$syear   = $nextardata[0];
					$smonth  = $nextardata[1];
					$sday1   = $nextardata[2];
					$sday2   = $sgnp_json->records[0]->$kokyaku['締め日']->value;
					// 日付の存在チェック
					if( checkdate( $smonth , $sday2 , $syear )){
						$setdata = $syear."-".$smonth."-".$sday2;
					}else{
						$setdata = $syear."-".$smonth."-".$sday1;
					}
				}else{
					// 締日が過ぎている場合、来月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 2 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$nonth = date('Y-m-d', $timest);
					$ndata = explode("-",$nonth);

					$cyear   = $ndata[0];
					$cmonth  = $ndata[1];
					$cday   = $ndata[2];

					if( checkdate( $cmonth , $sgnp_json->records[0]->$kokyaku['締め日']->value , $cyear )){
						$setdata = $cyear."-".$cmonth."-".$sgnp_json->records[0]->$kokyaku['締め日']->value;
					}else{
						$setdata = $cyear."-".$cmonth."-".$cday;
					}
				}
			}else{
				$setdata = $dkeizyobi; // 締日が空の場合、請求予定日（初期）を入れる。
			}

			// 書込み準備
			$recObj = new stdClass;
			// 通常項目
				$recObj->締切日			  = $this->valEnc($setdata);

				$recObj->請求先名			  = $this->tgfEnc( $tcRec , $i , "会社名" );
				$recObj->締め日 			  = $this->tgfEnc( $tcRec , $i , "締め日" );
				$recObj->回収サイト 		  = $this->tgfEnc( $tcRec , $i , "回収サイト" );
				$recObj->回収日		 		  = $this->tgfEnc( $tcRec , $i , "回収日" );
				$recObj->回収条件 			  = $this->tgfEnc( $tcRec , $i , "回収条件" );
				$recObj->回収方法 			  = $this->tgfEnc( $tcRec , $i , "回収方法" );
				$recObj->郵便番号 			  = $this->tgfEnc( $tcRec , $i , "郵便番号" );
				$recObj->住所１ 			  = $this->tgfEnc( $tcRec , $i , "所在地" );

				$tbl = $tcRec->getFieldValue( $i , "初期導入費用テーブル" );
				// テーブル項目
				$recObj->請求明細テーブル->value[0]->value->日付 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "請求予定日_初期_" );
//				$recObj->請求明細テーブル->value[0]->value->案件名 			 	=	 $this->tgfEnc( $tcRec , $i , "案件名" ); // 案件（地盤）では差し込まない
				$recObj->請求明細テーブル->value[0]->value->品名 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_初期_" );
				$recObj->請求明細テーブル->value[0]->value->数量1 			 	=	 $this->tgfEnc( $tbl , $this->paraTableNO , "数量_初期_" );
				$recObj->請求明細テーブル->value[0]->value->単位 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単位_初期_" );
				$recObj->請求明細テーブル->value[0]->value->単価1 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単価_初期_" );
				$recObj->請求明細テーブル->value[0]->value->摘要1 			 	= 	 $this->tgfEnc( $tbl , $this->paraTableNO , "契約摘要_初期_" );
//				$recObj->請求明細テーブル->value[0]->value->請求特記事項_案件_ 	= 	 $this->tgfEnc( $tcRec , $i , "請求特記事項_調査_" ); // 案件（地盤）では差し込まない
				$recObj->請求明細テーブル->value[0]->value->詳細情報 		 	=    $this->valEnc( TC_URL1.$this->paraAnkenID);
				$recObj->請求明細テーブル->value[0]->value->案件レコード番号 	= 	 $this->tgfEnc( $tcRec , $i , "レコード番号" );


			// --------------------
			// 請求入金管理へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$insData 			= new stdClass;
			$insData->app 		= TC_APPID_TCSNKK;
			$insData->records[] = $recObj;

			$k->aryJson = $insData;						// 追加対象のレコード番号
			$json = $k->runCURLEXEC( TC_MODE_INS );
			$this->SNKKID = $k->strInsRecNo[0];
		}

		/*************************************************************************/
	    /*  請求入金管理へデータを追加する                                       */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updSNKK( &$sgnp_json , &$snnp_json , $No) {

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			$i = 0;
			$tbcount = 0;
			$strSyubetu = "";        // 調査種別設定用
			$kokyakusetflg = 0;      // 締日が設定されたか知るフラグ
			$dkeizyobi = "";         // 計上日を格納

			// --------------------
			// 請求入金管理を更新する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCSNKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $snnp_json->records[$No]->レコード番号->value;
			$this->SNKKID = $snnp_json->records[$No]->レコード番号->value;

			$tbcount = count($snnp_json->records[$No]->請求明細テーブル->value);

			$hairetu1 = "";
			$hairetu2 = "";

			for($a = 0; $a <= ($tbcount - 1); $a++) {
				$hairetu1[]	  = $snnp_json->records[$No]->請求明細テーブル->value[$a]->value->日付->value.",".$a.",0";
			}

			// 日付順に並び替える準備
			$dkeizyobi    = $sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_->value;
			$hairetu1[]	  = $sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_->value.",".$a.",1";

			sort($hairetu1);
			for($i = 0; $i <= ($tbcount); $i++) {
				$hairetu2[] = explode(",", $hairetu1[$i]);
			}

			// 計上日を取得し、分解する 
			$ardata = explode("-",$sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_->value);


			$kokyaku['締め日']	   = "締め日";

			$year  = $ardata[0];
			$month = $ardata[1] + 1;
			$timestamp = mktime(0, 0, 0, $month, 0, $year);
			$last_month = date('Y-m-d', $timestamp);
			$setdata = "";

			if($sgnp_json->records[0]->$kokyaku['締め日']->value != "" && $sgnp_json->records[0]->$kokyaku['締め日']->value != 0 ){
				if( $sgnp_json->records[0]->$kokyaku['締め日']->value == "31"){
					// 締日が末の場合は末日を設定
					$setdata = $last_month;
				}else if( $ardata[2] <= $sgnp_json->records[0]->$kokyaku['締め日']->value){
					// 締日を過ぎていない場合は当月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 1 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$next_month = date('Y-m-d', $timest);
					$nextardata = explode("-",$next_month);

					$syear   = $nextardata[0];
					$smonth  = $nextardata[1];
					$sday1   = $nextardata[2];
					$sday2   = $sgnp_json->records[0]->$kokyaku['締め日']->value;
					// 日付の存在チェック
					if( checkdate( $smonth , $sday2 , $syear )){
						$setdata = $syear."-".$smonth."-".$sday2;
					}else{
						$setdata = $syear."-".$smonth."-".$sday1;
					}
				}else{
					// 締日が過ぎている場合、来月を締日にする
					$year  = $ardata[0];
					$month = $ardata[1] + 2 ;
					$timest = mktime(0, 0, 0, $month, 0, $year);
					$nonth = date('Y-m-d', $timest);
					$ndata = explode("-",$nonth);

					$cyear   = $ndata[0];
					$cmonth  = $ndata[1];
					$cday   = $ndata[2];

					if( checkdate( $cmonth , $sgnp_json->records[0]->$kokyaku['締め日']->value , $cyear )){
						$setdata = $cyear."-".$cmonth."-".$sgnp_json->records[0]->$kokyaku['締め日']->value;
					}else{
						$setdata = $cyear."-".$cmonth."-".$cday;
					}
				}
			}else{
				$setdata = $dkeizyobi;
			}

			$i = 0;

			$recObj->record->締切日				  = $this->valEnc($setdata);
			$recObj->record->請求先名			  = $this->tgfEnc( $tcRec , $i , "会社名" );
			$recObj->record->締め日 			  = $this->tgfEnc( $tcRec , $i , "締め日" );
			$recObj->record->回収サイト 		  = $this->tgfEnc( $tcRec , $i , "回収サイト" );
			$recObj->record->回収日		 		  = $this->tgfEnc( $tcRec , $i , "回収日" );
			$recObj->record->回収条件 			  = $this->tgfEnc( $tcRec , $i , "回収条件" );
			$recObj->record->回収方法 			  = $this->tgfEnc( $tcRec , $i , "回収方法" );
			$recObj->record->郵便番号 			  = $this->tgfEnc( $tcRec , $i , "郵便番号" );
			$recObj->record->住所１ 			  = $this->tgfEnc( $tcRec , $i , "所在地" );

			$tbl = $tcRec->getFieldValue( $i , "初期導入費用テーブル" );

			for($a = 0; $a <= ($tbcount); $a++) {
				if($hairetu2[$a][2] == "1"){
					// 新規データの設定
					$recObj->record->請求明細テーブル->value[$a]->value->日付 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "請求予定日_初期_" );
//					$recObj->record->請求明細テーブル->value[$a]->value->案件名 				=  $this->tgfEnc( $tcRec , $i , "案件名" ); // 案件（地盤）では差し込まない
					$recObj->record->請求明細テーブル->value[$a]->value->品名 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_初期_" );
					$recObj->record->請求明細テーブル->value[$a]->value->数量1 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "数量_初期_" );
					$recObj->record->請求明細テーブル->value[$a]->value->単位 					=  $this->tgfEnc( $tbl , $this->paraTableNO , "単位_初期_" );
					$recObj->record->請求明細テーブル->value[$a]->value->単価1 			  		=  $this->tgfEnc( $tbl , $this->paraTableNO , "単価_初期_" );
					$recObj->record->請求明細テーブル->value[$a]->value->摘要1 			  		=  $this->tgfEnc( $tbl , $this->paraTableNO , "契約摘要_初期_" );
//					$recObj->record->請求明細テーブル->value[$a]->value->請求特記事項_案件_ 	=  $this->tgfEnc( $tcRec , $i , "請求特記事項_調査_" ); // 案件（地盤）では差し込まない
					$recObj->record->請求明細テーブル->value[$a]->value->詳細情報 		  		=  $this->valEnc( TC_URL1.$this->paraAnkenID);
					$recObj->record->請求明細テーブル->value[$a]->value->案件レコード番号 		=  $this->tgfEnc( $tcRec , $i , "レコード番号" );
				}else{
					// 既存テーブルへの設定
					$recObj->record->請求明細テーブル->value[$a]->value->日付 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->日付;
					$recObj->record->請求明細テーブル->value[$a]->value->案件名 		  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->案件名;
					$recObj->record->請求明細テーブル->value[$a]->value->品名			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->品名;
					$recObj->record->請求明細テーブル->value[$a]->value->数量1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->数量1;
					$recObj->record->請求明細テーブル->value[$a]->value->単位 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->単位;
					$recObj->record->請求明細テーブル->value[$a]->value->単価1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->単価1;
					$recObj->record->請求明細テーブル->value[$a]->value->摘要1 			  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->摘要1;
					$recObj->record->請求明細テーブル->value[$a]->value->請求特記事項_案件_ 	=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->請求特記事項_案件_;
					$recObj->record->請求明細テーブル->value[$a]->value->詳細情報 		  		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->詳細情報;
					$recObj->record->請求明細テーブル->value[$a]->value->案件レコード番号 		=  $snnp_json->records[$No]->請求明細テーブル->value[$hairetu2[$a][1]]->value->案件レコード番号;
				}
			}

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_TCSNKK;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );
		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json ) {

			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );
			$i=0;
			$tbcount = 0;

			// -----------------------------
			// 案件管理(地盤調査)を更新する。
			// -----------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;

			$tbcount = count($sgnp_json->records[0]->初期導入費用テーブル->value);
			$tbl = $tcRec->getFieldValue( $i , "初期導入費用テーブル" );

			// 既存のデータを再登録する
		    for($a = 0; $a <= ($tbcount - 1); $a++) {
				$recObj->record->初期導入費用テーブル->value[$a]->value->NO_初期_ 	    		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->NO_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->商品・サービス名_初期_    =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->商品・サービス名_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->数量_初期_ 		 	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->数量_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->単位_初期_ 		  	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->単位_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->単価_初期_ 		  	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->単価_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->見積摘要_初期_ 		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->見積摘要_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->契約出力_初期_ 	  	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->契約出力_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->契約摘要_初期_    		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->契約摘要_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->支払区分_初期_   		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->支払区分_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->請求予定日_初期_   	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->請求予定日_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->入金予定日_初期_   	   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->入金予定日_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->請求計上_初期_   		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->請求計上_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->請求情報_初期_   		   =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->請求情報_初期_;
				$recObj->record->初期導入費用テーブル->value[$a]->value->商品名レコード番号_初期_  =  $sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->商品名レコード番号_初期_;
		    }

			// チェックを入れる
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->NO_初期_ 		 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "NO_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->商品・サービス名_初期_ 	 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品・サービス名_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->数量_初期_ 	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "数量_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->単位_初期_  	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単位_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->単価_初期_ 	 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "単価_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->見積摘要_初期_  		 	 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "見積摘要_初期_" );

			// 契約出力(初期)にチェックが無ければ何もしない。
			if($sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->契約出力_初期_->value[0] != ""){
				$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->契約出力_初期_  			 =	 $this->valEnc($sgnp_json->records[0]->初期導入費用テーブル->value[$this->paraTableNO]->value->契約出力_初期_->value[0]);
			}

			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->契約摘要_初期_ 	 		 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "契約摘要_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->支払区分_初期_ 			 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "支払区分_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->請求予定日_初期_  			 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "請求予定日_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->入金予定日_初期_       	 =	 $this->tgfEnc( $tbl , $this->paraTableNO , "入金予定日_初期_" );
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->請求計上_初期_  			 =	 $this->valEnc("済み");
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->請求情報_初期_ 	 		 = 	 $this->valEnc(TC_URL2.$this->SNKKID);
			$recObj->record->初期導入費用テーブル->value[$this->paraTableNO]->value->商品名レコード番号_初期_ 	 = 	 $this->tgfEnc( $tbl , $this->paraTableNO , "商品名レコード番号_初期_" );

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_ANKJ;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );
		}
	}

?>
