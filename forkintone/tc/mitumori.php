<?php
/*****************************************************************************/
/* 	 見積管理PHP                                              (Version 1.00) */
/*   ファイル名 : mitumori.php                                       		 */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
//	include_once("tckintone.php");          // INCLUDE不要(2014.11.9 Kataoka)

//    include_once("../tccom/defkintone.inc");
    include_once("defkintoneconf.inc");
    include_once("../tccom/tcutility.inc");
    include_once("tcerror.php");
    // kintoneクラスの定義を呼び出す。
    // 以下、整理(2015.2.9 Kataoka)
//    include_once("../tccom/tckintoneclass.php");
    include_once("../tccom/tckintone.php");
//    include_once("tckintone2.php");     // テストのため
	include_once("../tccom/tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_TB1_COUNT" , 15 );
	define( "TC_TB2_COUNT" , 7 );

	// パターンＢ用
	define( "TC_TB3_COUNT" , 14 );
	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;
	$clsSrs->paraLayout  = $_REQUEST['ptrn'];

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
	    var $paraLayout			= null; 	// レコード番号（パラメタ）
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err 	  = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// 見積管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCMTKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );
			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel5');

			if( $this->paraLayout == "b" ){
				$objPHPExcel = $objReader->load("templates/mitumori2.xls");
			}else{
				$objPHPExcel = $objReader->load("templates/mitumori.xls");
			}

			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "見積書(".$json->records[0]->顧客名_印刷用_->value.")_".date('YmdHis').sprintf("%.0f",($msec * 1000)).".xls";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , $pDat ) {
			$ret = "";
			$rowcount = 0; //パターンによって行数を変える

			if( $this->paraLayout == "b" ){
				$rowcount = TC_TB3_COUNT;
			}else{
				$rowcount = TC_TB1_COUNT;
			}

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('H1'		,	"No.".date('ym').sprintf('%04d', $pDat->レコード番号->value)	)
			            ->setCellValue('G2'		,	$this->common->setDayChange($pDat->TCJ見積日->value , 1 )	)
			            ->setCellValue('A4'		,	$pDat->顧客名_印刷用_->value	)
			            ->setCellValue('A15'	,	$pDat->案件名_印刷用_->value	)
			            ->setCellValue('H12'	,	$pDat->見積担当者->value[0]->name	)
			            ->setCellValue('B5'		,	"ご担当：".$pDat->先方担当者名->value	)
			            ->setCellValue('A40'	,	$pDat->備考->value	);

			if( $this->paraLayout == "b" ){
				$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
				            ->setCellValue('D31'	,	"５年リース月額（料率".sprintf("%.2f", $pDat->５年リース料率->value)."）"	)
				            ->setCellValue('G31'	,	$pDat->５年リース月額->value	);
			}

			// お見積内容
			$idx_s = 0;		    // 内容
			foreach( $pDat->初期導入費用テーブル->value as $key => $val ) {
				$shn = $val->value;

				$quantity1 = "";
				$price1    = "";

				if( $shn->数量1->value != 0 ){
					$quantity1 = $shn->数量1->value;
				}

				if( $shn->単価1->value != 0 ){
					$price1 = $shn->単価1->value;
				}

				// 内容
				if( $idx_s < $rowcount ) {
					$y = 16 + $idx_s;
					$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
								->setCellValue('A'.$y	,	$shn->NO1->value	)
								->setCellValue('B'.$y 	,	$shn->商品・サービス名1->value	)
					            ->setCellValue('D'.$y	,	$quantity1	)
					            ->setCellValue('E'.$y	,	$shn->単位1->value	)
					            ->setCellValue('F'.$y	,	$price1	)
					            ->setCellValue('H'.$y	,	$shn->摘要1->value	);
				}
				$idx_s++; 		//レコードのカウントアップ
			}

			// お見積条件
			$idx_r = 0;		// 条件
			foreach( $pDat->ランニング費用テーブル->value as $key => $val ) {
				$shn = $val->value;

				$quantity2 = "";
				$price2    = "";

				if( $shn->数量2->value != 0 ){
					$quantity2 = $shn->数量2->value;
				}

				if( $shn->単価2->value != 0 ){
					$price2 = $shn->単価2->value;
				}

				// 条件
				if( $idx_r < TC_TB2_COUNT ) {
					$y = 32 + $idx_r;
					$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
								->setCellValue('A'.$y	,	$shn->NO2->value	)
								->setCellValue('B'.$y 	,	$shn->商品・サービス名2->value	)
					            ->setCellValue('D'.$y	,	$quantity2	)
					            ->setCellValue('E'.$y	,	$shn->単位2->value	)
					            ->setCellValue('F'.$y	,	$price2	)
					            ->setCellValue('H'.$y	,	$shn->摘要2->value	);
				}
				$idx_r++;
			}
			
			// 明細件数チェック
			if( $idx_s > $rowcount ) {
				$ret = $ret."　　見積内容が ".$rowcount."件を超えています。<br>";
			}
			if( $idx_r > TC_TB2_COUNT ) {
				$ret = $ret."　　お見積条件が ".TC_TB2_COUNT."件を超えています。<br>";
			}
			if( $ret != "" ) {
				$ret = "注）印刷されない商品があります。<br>".$ret;
			}

			return ($ret);
		}

	}

?>
