<?php
/*****************************************************************************/
/* 案件請求テーブル転記PHP                                    (Version 1.00) */
/*   ファイル名 : keiyakusyo_anken.php                                       */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）

		var $err;
		var $SNKKID             = null;     // 請求入金管理作成ID

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 請求先の名前保持用

			// 案件管理アプリのデータを取得
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			if( $k->intDataCount > 0 ) {
				// あればアップデート
				$this->updANKK( $jsonANKK );
				$rmgkKei = "請求計上情報テーブルに追加しました。";
			}
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json ) {

			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );
			$i=0;
			$j=0;
			$tbcount = 0;

			// -----------------------------
			// 案件管理(地盤調査)を更新する。
			// -----------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;

			//	初期導入費用テーブルデータ
			$tbcount1 = count($sgnp_json->records[0]->初期導入費用テーブル->value);
			$tbl1 = $tcRec->getFieldValue( $i , "初期導入費用テーブル" );

			//	請求計上情報テーブルデータ
			$tbcount2 = count($sgnp_json->records[0]->請求計上情報テーブル->value);
			$tbl2 = $tcRec->getFieldValue( $i , "請求計上情報テーブル" );

			// 既存のデータを再登録する
		    for($a = 0; $a <= ($tbcount2 - 1); $a++){
				if($sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品・サービス名_請求_->value == "" && $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品詳細_請求_->value == "" && $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->数量_請求_->value == "" && $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単位_請求_->value == "" && $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単価_請求_->value == "" ){
				}else{
					$recObj->record->請求計上情報テーブル->value[$a]->value->請求計上日 	    		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求計上日;
					$recObj->record->請求計上情報テーブル->value[$a]->value->商品・サービス名_請求_  	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品・サービス名_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->商品詳細_請求_				   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->商品詳細_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->数量_請求_ 		 	   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->数量_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->単位_請求_ 		  	   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単位_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->単価_請求_ 		  	   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->単価_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->請求区分   		   		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求区分;
					$recObj->record->請求計上情報テーブル->value[$a]->value->入金予定日_請求_   	   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->入金予定日_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->請求計上_請求_   		   	   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求計上_請求_;
					$recObj->record->請求計上情報テーブル->value[$a]->value->請求リンク   		   		   =  $sgnp_json->records[0]->請求計上情報テーブル->value[$a]->value->請求リンク;
				}
		    }

			// 初期導入費用のデータで契約出力にチェックのあるデータを登録する
		    for($a = 0; $a <= ($tbcount1 - 1); $a++) {
				if($sgnp_json->records[0]->初期導入費用テーブル->value[$a]->value->契約出力_初期_->value[0] != ""){
					$j++;
					$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->商品・サービス名_請求_ 	 =	 $this->tgfEnc( $tbl1 , $a , "商品・サービス名_初期_" );
					$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->商品詳細_請求_ 			 =	 $this->tgfEnc( $tbl1 , $a , "商品詳細_初期_" );
					$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->数量_請求_ 	 			 = 	 $this->tgfEnc( $tbl1 , $a , "数量_初期_" );
					$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->単位_請求_  	 		 = 	 $this->tgfEnc( $tbl1 , $a , "単位_初期_" );
					$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->単価_請求_ 	 			 = 	 $this->tgfEnc( $tbl1 , $a , "単価_初期_" );

					if($sgnp_json->records[0]->納期契約選択->value == "現金"){
						$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->請求区分 	 			 = 	 $this->valEnc( "現金(一括)" );
					}else if($sgnp_json->records[0]->納期契約選択->value == "リース"){
						$recObj->record->請求計上情報テーブル->value[($tbcount2 +  $j)]->value->請求区分 	 			 = 	 $this->valEnc( "リース" );
					}else{
					}

				}
			}
			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_ANKJ;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );
		}
	}

?>
