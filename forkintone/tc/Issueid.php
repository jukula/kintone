<?php
/*****************************************************************************/
/* ID発行PHP	                                              (Version 1.01) */
/*   ファイル名 : Issueid.php                                                */
/*   更新履歴   															 */
/*                          						                         */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcSetSerialNo();

	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。

	// 実行
	$clsSrs->main();

	class TcSetSerialNo
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $AnkenGmnKbn		= false;	// 案件管理画面から呼ばれたか？
	    var $dateSabun      	= null;     // 差分読み込み用

	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcSetSerialNo() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// 画面メッセージ表示
			echo "<html>\n";
			echo "<meta http-equiv='content-type' content='text/html; charset=UTF-8'>\n";
			echo "<head></head>\n";
			echo "<body>\n";
			echo "シリアルNOを作成しています。<br><br>\n";
			echo "処理開始…<br><br>\n";
			echo str_pad(" " , 256);
			flush();

			// --------------------------------------------------
			// 案件管理を更新（シリアルNo）する。
			// --------------------------------------------------
			$rmgkKei = $this->SetSelNo();

			// --------------------
			// 終了メッセージ
			// --------------------
			echo "<br>";
			echo "処理が終了しました。<br>";
			echo "<br>";
			echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
			echo "</body></html>";

		}

		/*************************************************************************/
	    /*  案件管理(地盤調査)のシリアルNOを更新する                             */
	    /*  引数	&$aryAnkRecno  	更新対象の案件レコード番号                   */
	    /*      	$updClearKbn  	0更新区分  true:0クリア、false:集計          */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function SetSelNo() {
			$aryAnkRecno = array();

			//-----------------------------------------
			// シリアルNo未発行の案件データを取得する。
			//-----------------------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;		// アプリID（案件管理(地盤調査)）
			$k->arySelFileds	= array( "レコード番号" , "シリアルNO");

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
				$k->strQuery = "レコード番号 > $recno and シリアルNO = \"\"order by レコード番号 asc";
				$sgnp_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $sgnp_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $sgnp_json->records as $key => $rec ) {
					$aryAnkRecno[] = $rec->レコード番号->value;
				}
			} while( $k->intDataCount > 0 );

			if( count( $aryAnkRecno ) == 0 ) {
				echo "シリアルNoが未発行の案件データがありません。<br><br>\n";
				return;
			} else {
				echo count( $aryAnkRecno )." 件の案件データにシリアルNoを発行します。<br><br>\n";
			}

			// ----------------------------------------------
			// 一括更新件数の制限を考慮してデータを加工する。
			// ----------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryUpdAnk = array();
			foreach( $aryAnkRecno as $ankRecno ) {

				$recObj = new stdClass;
				$recObj->id = $ankRecno;
				$recObj->record->シリアルNO	= $this->valEnc( $this->valPass( $ankRecno ) );		// シリアルNo発行

				$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
				$idxR += 1;
				if( $idxR == CY_UPD_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
				$recObj = null;
			}

			// --------------------
			// 案件管理を更新する。
			// --------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryUpdAnk as $key => $val ) {
				$updData 			= new stdClass;
				$updData->app 		= TC_APPID_ANKJ;
				$updData->records 	= $val;

				$k->aryJson = $updData;						// 更新対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_UPD );
			}

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		function valPass( $val ) {
			$val = str_pad($val, 5, "0", STR_PAD_LEFT);
			$wk = "1".str_pad($val, 5, "0", STR_PAD_LEFT);
			$wk .= substr((substr($val,0,2) + substr($val,2,2) * substr($val,4,1)),-1);
			return ( $wk );
		}

	}

?>
