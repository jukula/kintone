<?php
	/*****************************************************************************/
	/* 集計PHP                    	       		                  (Version 1.00) */
	/*   ファイル名 : graph_insert.php                                     		 */
	/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
	/*   [備考]                                                                  */
	/*      tcutility.incを必ずインクルードすること                              */
	/*   [必要ファイル]                                                          */
	/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
	/*                                                                           */
	/*                                                                           */
	/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
	/*****************************************************************************/

	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();

// 実行
	if($_POST['Submit']){
		$clsSrs->main();
	}

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }


		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {

			echo "データ作成処理を開始します。このまましばらくお待ちください。<br><br>\n";

			// ----------------------------------------------------
			// 請求支払集計のデータを削除する
			// ----------------------------------------------------
			$aryAnkgRecno = array();

			// 請求支払集計アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_SSSK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_del[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json_del[$dtcount]->records as $key => $rec ) {
					$aryRmgkRecno[] = $rec->レコード番号->value;
				}

				// 件数カウント、次に読み込む条件の設定
				$recno 			 	 = $json_del[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// --------
			// 削除実行
			// --------

            if($dtcount != 0){
			    $this->delData( TC_APPID_CC_SSSK , $aryRmgkRecno );
            }

			// ----------------------------------------------------
			// 請求書支払依頼書のデータを登録する
			// ----------------------------------------------------
			$msg  = "";

			$statDay 	 = $_POST['yaer'].'-01-01';   //期間開始日付を取得
			$statDay_chk = $_POST['yaer'].'0101';   //期間開始日付を取得

			// 請求書支払依頼書アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_SSIK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			// kintoneデータ取得件数制限の対応。
			$recno 	 = 0;
			$dtcount = 0;
			$data=0;

			do {
				// 検索条件を作成する
				$aryQ = array();
				$aryQ[] = "( (請求書日付 >= \"".$statDay."\") )";
				$aryQ[] = "( レコード番号 > $recno )";
				$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する
				$json_ANKK[$dtcount] = $k->runCURLEXEC( TC_MODE_SEL );
print_r($k);
				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json_ANKK[$dtcount]->records as $key => $rec ) {
					$aryANKK[] = $rec->レコード番号->value;
				}

				// 件数カウント、次に読み込む条件の設定
				$data 				+= $k->intDataCount;
				$recno 			 	 = $json_ANKK[$dtcount]->records[ $k->intDataCount - 1 ]->レコード番号->value;
				$dtcount++; // カウントアップ
			} while( $k->intDataCount > 0 );

			echo($data."件の対象データがあります。");

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録データを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsRmgk = array();

			for ($k = 0; $k < count($json_ANKK); $k++) {
				$tcRec = new TcKintoneRecord();
				$tcRec->setRecordList( $json_ANKK[$k]->records );

				for( $i=0; $i < $tcRec->getNumber(); $i++ ) {
					$tbl = $tcRec->getFieldValue( $i , "請求書情報" );
					// 請求書支払依頼書 -> 請求支払集計
					for( $j=0; $j < $tbl->getNumber(); $j++ ) {

						if(str_replace("-", "", $json_ANKK[$k]->records[$i]->請求書情報->value[$j]->value->請求書日付->value) >= $statDay_chk ){

							// 書込み準備
							$recObj = new stdClass;
							// 通常項目
							$recObj->請求書支払依頼書レコード番号	= $this->tgfEnc( $tcRec , $i , "レコード番号" );

							// テーブル項目
							$recObj->支払先			= $this->tgfEnc( $tbl , $j , "支払先" );
							$recObj->請求書日付		= $this->tgfEnc( $tbl , $j , "請求書日付" );
							$recObj->金額_税抜_		= $this->tgfEnc( $tbl , $j , "金額_税抜__5" );
							$recObj->消費税			= $this->tgfEnc( $tbl , $j , "消費税" );
							$recObj->金額_税込_		= $this->tgfEnc( $tbl , $j , "金額_税込_" );
							$recObj->項目			= $this->tgfEnc( $tbl , $i , "項目" ); //収入の場合は不要
							$recObj->目的_内容		= $this->tgfEnc( $tbl , $j , "目的_内容_1" );
							$recObj->放送年			= $this->tgfEnc( $tbl , $j , "放送年" );
							$recObj->放送月			= $this->tgfEnc( $tbl , $j , "放送月" );
							$recObj->放送日			= $this->tgfEnc( $tbl , $j , "放送日" );
							$recObj->回				= $this->tgfEnc( $tbl , $j , "回" );

							$aryInsRmgk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_INS_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;
						}
					}
				}
			}

			// ---------------------------
			// 請求支払集計へ追加する。
			// ---------------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_SSSK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			foreach( $aryInsRmgk as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_CC_SSSK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json_ins   = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
					echo "請求書支払依頼書データの追加に成功しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
				} else {
					echo "請求書支払依頼書データの追加に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
					break;
				}

				$rmgkInsKensu += count( $val );
			}

			echo "処理が終了しました。<br><br>\n";
			echo "<input type='button' value='閉じる' onClick='window.close()'>\n";
			exit;
		}

		/*************************************************************************/
	    /* 指定したアプリのデータを削除する                                      */
	    /*  引数	$appid    		アプリＩＤ                                   */
	    /*       	$pDelRecno   	削除対象のレコード番号                       */
	    /*  関数値  string			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function delData( $appid , &$pDelRecno ) {
			// -----------------------------------------------
			// CY_DEL_MAX個ごとに処理をする。（kintoneの制限）
			// -----------------------------------------------
			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			$aryDelRmgk = array();
			foreach( $pDelRecno as $key => $val ) {
				$aryDelRmgk[ $idxM ][ $idxR ] = $val;
				$idxR += 1;
				if( $idxR == CY_DEL_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
			}
			// 削除実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->intAppID 		= $appid;				// アプリID
			$k->aryDelRecNo		= array();
			foreach( $aryDelRmgk as $key => $val ) {
	    		$k->aryDelRecNo = $val;					// 削除対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_DEL );
			}
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>集計表出力</title>
 </head>
 <body>
	<p class="txt">
        集計対象の年を選択し、作成を押してください。<br>
    </p>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<select name="yaer" size="1">
<?php
    $i = -2;
	while ( $i <= 7) {
	    $year = date("Y") + $i;
        if ( $year == date("Y") ) {
     	    echo '<option value="' . $year . '" selected="selected">' . $year . '</option>';
		} else {
     	    echo '<option value="' . $year . '">' . $year . '</option>';
		}
        $i++;
	}
?>
		</select>
		年

		<input type="submit" name="Submit" value="作成" >
	</form>
 </body>
</html>

