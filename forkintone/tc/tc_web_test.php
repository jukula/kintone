#!/usr/bin/php53
<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tc_web.php                 				                 */
/*   更新履歴   2013/07/16  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define( "TC_URL_MS" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCMISK."/show#record=" ); // 案件管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcMail();

	// 実行
	$clsSrs->main();


	class TcMail
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数
		var $mailfrom   = "c.komatsu@timeconcier.jp"; // 送信元
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcMail() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$ret = false;

			$today1 = date("Y");
			$today2 = date("n");
			$today3 = date("d");

			// ----------------------------------------------
			// 名刺管理から対象データを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TCMISK;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( 月 = \"".$today2."\")";
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json[$i]->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// 差込データ作成
			// ----------------------------------------------
			$i = 0;
			$setbody = $today2."月生まれの方\n\n";
			$age     = "";
			$ageDay  = "";
			$arrsort = "";
			$n = 0;

			for($i = 0; $i <= count($mail_json); $i++) {
				for($a = 0; $a < count($mail_json[$i]->records); $a++) {
					if( ($mail_json[$i]->records[$a]->誕生日_年_->value != "") && ($mail_json[$i]->records[$a]->月->value != "") && ($mail_json[$i]->records[$a]->日->value != "")){
						$age = ($this->birthToAge($mail_json[$i]->records[$a]->誕生日_年_->value."-".$mail_json[$i]->records[$a]->月->value."-".$mail_json[$i]->records[$a]->日->value )) + 1;
						$age = $age . "歳";
					}else{
						$age = "";
					}

					if( $mail_json[$i]->records[$a]->日->value != "" ){
						if(mb_strlen($mail_json[$i]->records[$a]->日->value) == 1 ){
							$ageDay = $mail_json[$i]->records[$a]->月->value."月 ".$mail_json[$i]->records[$a]->日->value . "日　";
						}else{
							$ageDay = $mail_json[$i]->records[$a]->月->value."月".$mail_json[$i]->records[$a]->日->value . "日　";
						}
					}else{
						$ageDay = "";
					}

					$arrsort[$n][id] = $mail_json[$i]->records[$a]->日->value;
					$arrsort[$n][val] = $ageDay.$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."さん "." ".$age."\n　　　　　".TC_URL_MS.$mail_json[$i]->records[$a]->レコード番号->value."\n";
//					$setbody.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."さん ".$ageDay." ".$age."\n";
					$n++;
				}
			}

			foreach($arrsort as $key=>$value){
			    $id[$key] = $value['id'];
			}

			array_multisort($id ,SORT_ASC,$arrsort);

			for($a = 0; $a < count($arrsort); $a++) {
				$setbody.= $arrsort[$a][val];
			}

/*
			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone();				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= TC_APPID_TEST;	// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->メールアドレス->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->メールアドレス->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->メールアドレス->value;
						}
					}
				}
			}
*/

			// 固定で送る
//			$email = "m.fukushima@timeconcier.jp".","."c.komatsu@timeconcier.jp";
			$email = "all@timeconcier.jp";

			// -------------
			// メールを送信する
			// -------------
			mb_language("Japanese");
			mb_internal_encoding("UTF-8");
			$subject= "TC送信のテスト";    // タイトルの設定
			// 本文の設定
			$body 	= "【TC名刺管理】今月が誕生日の方\n";
			$body  .= $setbody; //繰り返し項目
			$body  .= "-----------------------------------------------------------------\nタイムコンシェル株式会社\nTIME CONCIER Co.,Ltd.\n〒781-8007 高知県高知市仲田町2-11\nTEL：050-3367-5536\n\nタイムコンシェルオフィシャルサイト\nhttp://www.timeconcier.jp\n\n";
			$body  .= "グランドコンシェル紹介サイト\nhttp://groundconcier.com/\n-----------------------------------------------------------------";
			$header = "From: ".$this->mailfrom."\r\n"; 		// 送信元の設定.  .BCCの設定（送信元にも送る）


			// ヘッダーと繋ぐ
			$mailbody 	= $this->Header;	 // 本文の設定
			$mailbody  .= $setlist; 		 //繰り返し項目

			// メール送信処理
			$sendmail = new tcAlert();
			$sendmail->setFrom( "komatsu@cross-tier.com" , "テスト" );
			$sendmail->setTo( "test1@cross-tier.com" );
			$sendmail->setTitle( $subject );
			$sendmail->setBody( $body );

			$res = $sendmail->sendMail( "", "", "komatsu@cross-tier.com" );


/*
			if (mb_send_mail($email, $subject, $body , $header )) {
				// echo "メールが送信されました。";
			} else {
				// echo "メールの送信に失敗しました。";
			}
*/
			return $ret;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
