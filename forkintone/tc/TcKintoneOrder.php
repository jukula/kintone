<?php
/*****************************************************************************/
/* 	 社内向けkintone申込書出力PHP                             (Version 1.00) */
/*   ファイル名 : TcKintoneOrder.php                                         */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_SHN_GYO" , 10 );		// 1シートあたりの対象行
	define( "TC_SHN_SHEET" , 2 );		// 1ファイルあたりのシート数

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
    // 対象のレコード番号を受け取る。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;
	$clsSrs->paraRecID   = $_REQUEST['prno'] - 0;

	// 実行
	$clsSrs->main();



	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
		var $err;
		var $common;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/TcKintoneOrder.xls");


			/*
			** TC案件管理アプリより値取得
			*/
			$k = new TcKintone();
			$k->parInit();											// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;					// アプリID
		    $k->arySelFields	= array(); 							// 読込用フィールドパラメータ
		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID;	// クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );
			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCellTCMatter( $objPHPExcel , 0 , $json->records[0] );
			}

			/*
			** IT契約管理アプリより値取得
			*/
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCITKK;				// アプリID
		    $k->arySelFields	= array(); 						// 読込用フィールドパラメータ
		    $k->strQuery = "レコード番号 = ".$this->paraRecID;	// クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );
			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			/*
			** エラーメッセージがない場合のみ、Excelダウンロードリンクを表示する
			*/
			if ( $msg == "" ) {
				// ダウンロード用エクセルを準備
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				// ファイル名生成
				list($msec, $sec) = explode(" ", microtime());
				$saveName = "サイボウズ／kintone申込書（".$json->records[0]->顧客名->value."様分）.xls";
				// ダウンロード用エクセルを保存
				$objWriter->save("tctmp/".$saveName);
				$saveurl = "https://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName;

				echo '<li><a href="' .$saveurl. '" target="_blank">申込書ファイルのダウンロードはこちら</a>（エクセル形式）</li><br>';
			}
			echo $msg;
		}

		/*************************************************************************/
		/* セルへ値を設定する。                                                  */
		/* ★TC案件管理より値を取得                                              */
		/*  引数	なし                                                         */
		/*************************************************************************/
		function setCellTCMatter( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('E6'		,	$pDat->顧客名->value	)
			            ->setCellValue('R6'		,	$pDat->貴社名フリガナ->value	)
			            ->setCellValue('E7'		,	$pDat->郵便番号->value	)
			            ->setCellValue('J7'		,	$pDat->住所1->value	)
			            ->setCellValue('E8'		,	$pDat->ＴＥＬ->value	)
			            ->setCellValue('R8'		,	$pDat->ＦＡＸ->value	);

			return ($ret);
		}

		/*************************************************************************/
		/* セルへ値を設定する。                                                  */
		/* ★自身のアプリ(IT契約管理)より値を取得                                */
		/*  引数	なし                                                         */
		/*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$idx_s = 0;					 // 全体のカウント数
			$idx_r = 0;					 // シート毎の行数
			$rowcount = 1;				// シート変更の数
			$sheetCount = $pSheetNo;	// シートNo
			$idx_sheet  = 0;			// 初期フラグ

			// 契約種別(月額/年額によって設定位置を決定)
			if ( $pDat->契約種別->value == "月額" ) {
			    $setPos = 'N14';		// 月単位
			} else {
			    $setPos = 'N15';		// 年単位
			}

			// URLのサブドメインのみを$urlに保持
			$url = str_replace("https:", "", $pDat->URL->value);
			$url = str_replace(".cybozu.com", "", $url);
			$url = str_replace("/", "", $url);

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('E9'		,	$pDat->申込者->value	)
			            ->setCellValue('R9'		,	$pDat->申込者フリガナ->value	)
			            ->setCellValue('E10'	,	$pDat->緊急連絡先->value	)
			            ->setCellValue('R10'	,	$pDat->E_mail->value	)
			            ->setCellValue('I13'	,	$pDat->契約数->value	)
			            ->setCellValue($setPos	,	"■"	)
			            ->setCellValue('M19'	,	$url	);

			foreach( $pDat->tbl_アカウント情報->value as $key => $val ) {
				$shn = $val->value;
				$ppoint = $sheetCount + 1;	// 現在のページ数

				// テーブル処理
				$y = 26 + $idx_r;


				// 行データ設定
				$pPHPExcel	->setActiveSheetIndex( $sheetCount )
					        ->setCellValue('C'.$y	  ,	$shn->姓カナ->value	)
					        ->setCellValue('G'.$y	  ,	$shn->名カナ->value	)
					        ->setCellValue('C'.($y+1) ,	$shn->姓->value	)
					        ->setCellValue('G'.($y+1) ,	$shn->名->value	)
					        ->setCellValue('K'.$y	  ,	$shn->メールアドレス->value	)
					        ->setCellValue('W'.$y	  ,	$shn->ログイン名->value	)
					        ->setCellValue('AD'.$y	  ,	$shn->パスワード->value	);
				$idx_r += 2;		//セル差し込み行数(1行2セルのため+2する)

				$idx_s++;			//データ数のカウントアップ(総合)
				$rowcount++;		//データ数のカウントアップ(シート別)


				// データ数が TC_SHN_GYO を超えていればシート切替
				if( $rowcount > TC_SHN_GYO ){
					$rowcount = 0; 
					$idx_r = 0;
					$sheetCount += 1;
				}

				// シート数が TC_SHN_SHEET 枚を超えていれば終了
				if( $sheetCount > TC_SHN_SHEET - 1 ){
			      $ret = "シート数が". TC_SHN_SHEET ."枚を超えているため、出力できません。";
				  break;
				}

			}

			return ($ret);
		}


	}

?>
