<?php
/*****************************************************************************/
/* 	 保守サポート契約書印刷PHP                                (Version 1.00) */
/*   ファイル名 : hosyukeiyaku.php                                           */
/*   更新履歴   2013/10/25  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_TB1_COUNT" , 7 );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
    // 対象のレコード番号を受け取る。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
		var $err;
		var $common;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// TC保守管理アプリ
			$k = new TcKintone();	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// エクセルの保守サポート契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/hosyukeiyaku.xls");

			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "保守サポート契約書(".$json->records[0]->顧客名->value.")_".date('YmdHis').sprintf("%.0f",($msec * 1000)).".xls";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('B5'		,	$pDat->仕様・詳細内容->value	)
			            ->setCellValue('H29'	,	$pDat->支払条件->value	)
			            ->setCellValue('H32'	,	$pDat->その他条件->value	)
			            ->setCellValue('H33'	,	$this->setDayGet($pDat->契約開始日->value, 0 )	)
			            ->setCellValue('M33'	,	$this->setDayGet($pDat->契約開始日->value, 1 )	)
			            ->setCellValue('P33'	,	$this->setDayGet($pDat->契約開始日->value, 2 )	)
			            ->setCellValue('T33'	,	$this->setDayGet($pDat->契約終了日->value, 0 )	)
			            ->setCellValue('Y33'	,	$this->setDayGet($pDat->契約終了日->value, 1 )	)
			            ->setCellValue('AB33'	,	$this->setDayGet($pDat->契約終了日->value, 2 )	)
			            ->setCellValue('H34'	,	$pDat->解約申入期日->value	)
			            ->setCellValue('C40'	,	"委託者:".$pDat->委託者->value."(".$pDat->委託者ふりがな->value.")様"	)
			            ->setCellValue('C41'	,	"受託者:".$pDat->受託者->value."(".$pDat->受託者ふりがな->value.")"	)
			            ->setCellValue('F48'	,	$pDat->顧客名->value	)
			            ->setCellValue('AO88'	,	$this->common->setDayChange($pDat->契約日->value, 1 )	)
			            ->setCellValue('AR92'	,	$pDat->住所1->value.$pDat->住所2->value	)
			            ->setCellValue('AR93'	,	$pDat->顧客名->value	)
			            ->setCellValue('AR94'	,	"代表取締役社長　　".$pDat->代表者->value	);

			if ( $pDat->契約単位->value == "年" ) {
				$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
				            ->setCellValue('AF33',	"(".$pDat->契約数->value."年単位)" );
			}

			// 保守契約明細
			$idx_s = 0;		    // 内容
			foreach( $pDat->保守契約明細テーブル->value as $key => $val ) {
				$shn = $val->value;

					// 内容
					if( $idx_s < TC_TB1_COUNT ) {
						$y = 19 + $idx_s;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('C'.$y	,	$shn->商品・サービス名->value	)
									->setCellValue('O'.$y 	,	$shn->数量->value	)
						            ->setCellValue('Q'.$y	,	$shn->単位->value	)
						            ->setCellValue('U'.$y	,	$shn->売単価->value	)
						            ->setCellValue('AD'.$y	,	$shn->摘要->value	);
					}
					$idx_s++; 		//レコードのカウントアップ
			}
			
			// 明細件数チェック
			if( $idx_s > TC_TB1_COUNT ) {
				$ret = $ret."　　保守契約明細が ".TC_TB1_COUNT."件を超えています。<br>";
			}
			if( $ret != "" ) {
				$ret = "注）印刷されない商品があります。<br>".$ret;
			}

			return ($ret);
		}

		/*************************************************************************/
	    /*  日付型から月か日を返す                                               */
	    /*  引数	$pDat　 日付項目                                             */
	    /*  引数	$pPara　変換内容  0 = 年 / 1 = 月 / 2 = 日                   */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setDayGet( &$pDat , $pPara ) {
			$ret = "";

			$Date = explode("-", $pDat);

			if( $pPara == 0 ){
				$ret = $Date[0];
			}else if( $pPara == 1 ) {
				$ret = $Date[1];
			}else{
				$ret = $Date[2];
			}

			return ($ret);
		}


	}

?>
