<?php
/*****************************************************************************/
/* 保守売上履歴作成PHP                                        (Version 1.01) */
/*   ファイル名 : romutotal.php                                              */
/*   更新履歴   2013/05/04  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define("TC_RITU_SKETA"	, 1 ); // 売上％、粗利％の少数桁数

	define("TC_SEK_TNI_NEN"		, '年');	// 契約、積算単位：年
	define("TC_SEK_TNI_TUKI"	, '月');	// 契約、積算単位：月
	
	define("TC_SKEI_KBN_URI"	, '売上' ); // 売上
	define("TC_SKEI_KBN_GEN"	, '原価' ); // 原価
	define("TC_SKEI_KBN_ARA"	, '粗利' ); // 粗利
	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsHsUr = new TcHosyuUriage();

	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。
	$clsHsUr->HosyuGmnKbn = array_key_exists( 'hosyuid' , $_REQUEST );
	if( $clsHsUr->HosyuGmnKbn ) {
		$clsHsUr->paraHosyuID = $_REQUEST['hosyuid'];
	} else {
		$clsHsUr->paraHosyuID = null;
	}

	// -----------------------------
	// 労務原価（保守）のデータ更新
	// -----------------------------
	// 画面メッセージ表示
	if( $clsHsUr->HosyuGmnKbn ) {
		//
	} else {
		echo "<html>\n";
		echo "<meta http-equiv='content-type' content='text/html; charset=UTF-8'>\n";
		echo "<head></head>\n";
		echo "<body>\n";
		echo "労務原価（保守）データを更新しています…<br><br>\n";
		echo str_pad(" " , 256);
		flush();
	}
	//
//	$clsHsUr->SendUrl( "https://www.timeconcier.jp/forkintone/tc/syncsgnprmgkh.php" );

	// -----
	// 実行
	// -----
	$clsHsUr->main();


	// 画面メッセージ表示
	if( $clsHsUr->HosyuGmnKbn ) {
		//
	} else {
		echo "<br>";
		echo "処理が終了しました。<br>";
		echo "<br>";
		echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
		echo "</body></html>";
	}

	// クラスを開放する
	$clsHsUr = null;


	/*****************************************************************************/
	/* クラス                                                                    */
	/*****************************************************************************/
	class TcHosyuUriage
	{
	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraHosyuID		= null; 	// 保守レコード番号（パラメタ）
		var $HosyuGmnKbn		= false;	// 保守管理画面から呼ばれたか？
		var $arrHskrRecno		= array();	// 処理対象の捕手管理レコード番号
		var $uriData 			= array();	// データ更新前編集
		var $uriDataRuiKei		= array();	// データ更新前編集(累計)
		var $stockData			= array();	// ストック売上管理
		var $kykupdM			= array();	// 契約更新月（本年分のみ） 本年：このphpを実行した日(年)。

	    var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcHosyuUriage() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {

			// ------------------------------------------------
			// 処理対象データを取得する。
			// ------------------------------------------------
			// 保守売上履歴の作成日の最新を取得する。
			//$syoriDate = $this->getAppMaxMin( TC_APPID_TCHRRK , "作成日時" , CY_DAT_MAX );


			// ------------------------------------------------
			// 各アプリからデータを抽出し、uriData へ格納する。
			// ------------------------------------------------
			// 画面メッセージ表示
			if( $clsHsUr->HosyuGmnKbn ) {
				//
			} else {
				echo "売上、仕入を集計しています…<br><br>\n";
				echo str_pad(" " , 256);
				flush();
			}

		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

/*
			// 前回の保守売上履歴の作成日時 以降の作成・更新をデータを対象とする。
			$hrrkDate = $this->getAppMaxMin( TC_APPID_TCHRRK , "作成日時" , CY_DAT_MAX );
*/
			$hrrkDate =null;
			$this->arrHskrRecno = $this->getHosyuUriGen( $hrrkDate );

			// 労務原価（保守）から、労務原価を取得する。
			if( $clsHsUr->HosyuGmnKbn ) {
				//
			} else {
				echo "労務原価を集計しています…<br><br>\n";
				echo str_pad(" " , 256);
				flush();
			}

		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

			$this->getRomuGen();

			// 年計、累計を計算する。
			$this->calcData();

			// ----------------------------------------
			// 保守売上履歴、保守管理の累計を更新する。
			// ----------------------------------------
			$this->delHRRK( null );
			$retmsg = $this->insHRRK();
			if( $clsHsUr->HosyuGmnKbn ) {
				//
			} else {
				echo $retmsg."<br><br>\n";
				echo str_pad(" " , 256);
				flush();
			}
			$this->updHSKR();

			// ----------------------------------------------
			// 削除データを同期する（保守管理と保守売上履歴）
			// ----------------------------------------------
			$this->sycHskr();

			// ------------------------
			// ストック売上管理を更新する。
			// ------------------------
			if( $clsHsUr->HosyuGmnKbn ) {
				//
			} else {
				echo "ストック売上管理を更新しています…<br><br>\n";
				echo str_pad(" " , 256);
				flush();
			}

		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

			$this->delHUIT();


			echo "更新を開始します。<br><br>\n";
		    // 出力バッファの内容を送信する
		    @ob_flush();
		    @flush();

			$this->insHUIT();

			// --------------------
			// 終了メッセージ
			// --------------------
			// 案件管理画面から呼ばれた場合は、集計結果を返す。
			if( $this->HosyuGmnKbn ) {
				echo ("var phpRet = '".$rmgkKei."';\n");
			} else {
				//
			}
			return;
		}

		/*************************************************************************/
	    /* 保守管理から、売上・仕入を取得する。                                  */
	    /*  引数	                                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function getHosyuUriGen( $pDate ) {
			$ret = null;
			$aryHsHiduke = array();

			//---------------------------------------
			// 保守管理からデータを全て取得する。
			//---------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;		// 保守管理 TC保守管理
//			$k->arySelFileds	= array("レコード番号" , "顧客コード" , "保守契約明細テーブル");
			$k->arySelFileds	= array("レコード番号" , "保守契約明細テーブル");
			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			$aryHosyuData = array();
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
				$aryQ 	= array();
				if( $pDate == "" ) {
					//
				} else {
					$aryQ[] = "( 作成日時 >= \"".$pDate."\" )";
				}
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				$sgnp_json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $sgnp_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $sgnp_json->records as $key => $rec ) {
					if( count( $rec->保守契約明細テーブル->value ) == 0 ) {
						continue;
					}
					$dat = &$aryHosyuData[ $rec->レコード番号->value ];
//					$dat['kkkcode'] = $rec->顧客コード->value;
					$dat['kkkname'] = $rec->顧客名->value;
					$dat['kykname'] = $rec->契約名->value;

					$dat['契約開始日']	= $rec->契約開始日->value;
					$dat['契約終了日'] 	= $rec->契約終了日->value;
					$dat['契約数'] 	 	= $rec->契約数->value;
					$dat['契約単位'] 	= $rec->契約単位->value;

					$dat['meisai']  = $rec->保守契約明細テーブル->value;
				}

			} while( $k->intDataCount > 0 );

			//---------------------------------------------------
			// 保守明細から、保守の開始日、終了日を取得する。
			// 親の契約開始日、契約終了日は印刷用なので使用しない。
			// 2013.12.21
			//---------------------------------------------------
			foreach( $aryHosyuData as $hsRecno => $val ) {
				// 保守案件内のテーブル（保守データ）
				$HsMsKaishi = "9999-99-99";
				$HsMsSyuryo = null;
				$HsMsKeiyakuSyuryoFlg = false;	// 未入力の終了日があれば、契約全体は終了ではない。
				foreach( $val['meisai'] as $key => $val2 ) {
					$hd = &$val2->value;
					
					if( $hd->積算数->value == 0 ) {
						continue;
					}

					if( $HsMsKaishi > $hd->開始日->value ) {
						$HsMsKaishi = $hd->開始日->value;
					}
					if( $HsMsSyuryo < $hd->終了日->value ) {
						$HsMsSyuryo = $hd->終了日->value;
					}
					if( $hd->終了日->value == "" ) {
						$HsMsKeiyakuSyuryoFlg = true;
					}
				}
				if( $HsMsKaishi == "9999-99-99" ) {
					$HsMsKaishi = null;
				}
				if( $HsMsKeiyakuSyuryoFlg ) {
					$HsMsSyuryo = null;
				}
				$aryHsHiduke[ $hsRecno ]['保守開始日'] = $HsMsKaishi;
				$aryHsHiduke[ $hsRecno ]['保守終了日'] = $HsMsSyuryo;
			}

			//---------------------------------------------------
			// 売上データを作成・集計、仕入れを集計する。
			// 積算数単位で、開始日から終了日(今月)まで作成する。
			//---------------------------------------------------
			$dateNow 	= new DateTime( "now" );
			$dateNowStc = new DateTime( ($dateNow->format("Y") + 4)."-12-31" );	// 当年含む5年。
			foreach( $aryHosyuData as $hsRecno => $val ) {
				// 保守案件内のテーブル（保守データ）
				foreach( $val['meisai'] as $key => $val2 ) {
					$hd = &$val2->value;

					// ------------------
					// 開始・終了日の設定
					// ------------------
					$strDate 	= new DateTime( $hd->開始日->value );
					if( $hd->終了日->value == "" ) {
						$endDate 	= null;
						$endDateStc = null;
					} else {
						$endDate 	= new DateTime( $hd->終了日->value );
						$endDateStc = new DateTime( $hd->終了日->value );
					}
					// 売上履歴集計用。MAX 本日まで。
					if( $endDate == null || ( $dateNow < $endDate ) ) {
						$endDate = $dateNow;
					}
					// ストック売上集計用。MAX 本日+4年まで。
					if( $endDateStc == null || ( $dateNowStc < $endDateStc ) ) {
						$endDateStc = $dateNowStc;
					}

					// --------------------
					// 対象外のデータ判定。
					// --------------------
					if( $hd->積算数->value == "" ) {
						// 積算数が未入力のもの。
						continue;
					}
					if( $hd->開始日->value == "" ) {
						// 開始日が入っていない。
						continue;
					}

					// --------------------
					// 集計期間の編集
					// --------------------
					// 日付編集（月単位で計算するので、日を「1日」とする）
					// データごとの集計期間を取得する。(売上、原価を計上する期間)
					$strDate->setDate( $strDate->format("Y") , $strDate->format("m") , 1 );	// 計算結果がづれ無いようにする。 例）1/31 に 2か月足すと、3/3 になる。
					$endDate->setDate( $endDate->format("Y") , $endDate->format("m") , 1 );	// 計算結果がづれ無いようにする。

					// ------------------------
					// 積算単位の判定（月・年）
					// ------------------------
					$sekisan = $hd->積算数->value;
					if( $hd->積算単位->value == TC_SEK_TNI_NEN ) {
						$sekisan = $sekisan * 12;
					}

					// ******************
					// 保守売上履歴の集計
					// ******************
					$idxDate = clone $strDate;
					$endCnt = ( $endDate->format("Y")*12 + $endDate->format("m") ) - ( $strDate->format("Y")*12 + $strDate->format("m") );
					if( $endCnt < 0 ) {
						// 開始日が当月以降の場合は計算しない。
					} else {
						for( $cnt=0; $cnt <= $endCnt; $cnt = $cnt + $sekisan ) {
							// 集計年月
							$idxNenGetsu = $idxDate->format( 'Ym' );

							$dat = &$this->uriData[ $hsRecno ][ $idxNenGetsu ];
							$dat[ 'uriage' ] 	+= $hd->売上金額->value;
							$dat[ 'genka' ] 	+= $hd->仕入金額->value;
							$dat[ 'arari' ] 	 = $dat[ 'uriage' ] - $dat[ 'genka' ];
							$dat[ 'skikbn' ] 	 = TC_SKIKBN_MEISAI;
							$dat[ 'kkkcode' ] 	 = $aryHosyuData[ $hsRecno ][ 'kkkcode' ];
							$dat[ 'kkkname' ] 	 = $aryHosyuData[ $hsRecno ][ 'kkkname' ];

							// 次の集計年月計算用(○ヶ月単位)
							$idxDate->add( new DateInterval("P".$sekisan."M") );
						}
					}

					// **********************
					// ストック売上管理の集計
					// **********************
					$idxDate = clone $strDate;
					$endCnt = ( $endDateStc->format("Y")*12 + $endDateStc->format("m") ) - ( $strDate->format("Y")*12 + $strDate->format("m") );
					if( $endCnt < 0 ) {
						// 開始日が当月以降の場合は計算しない。
					} else {
						for( $cnt=0; $cnt <= $endCnt; $cnt = $cnt + $sekisan ) {
							// 集計年月
							$idxNenGetsu = $idxDate->format( 'Ym' );
							$wkNen	 = $idxDate->format( 'Y' );
							$wkTsuki = $idxDate->format( 'm' ) - 0;	// 0詰なし

							$dat = &$this->stockData[ $hd->積算単位->value ][ $wkNen ][ $val[kkkcode] ][ $hsRecno ];
							$dat[ '顧客名' ]	 = $val[kkkname];
							$dat[ '契約名' ]	 = $val[kykname];
							// 金額
							$dat_m = &$dat['金額'][ TC_SKEI_KBN_URI ];	// 売上
							$dat_m[ $wkTsuki ]	+= $hd->売上金額->value;
							$dat_m[ 'kei' ]		+= $hd->売上金額->value;
							$dat_m = &$dat['金額'][ TC_SKEI_KBN_GEN ];	// 原価
							$dat_m[ $wkTsuki ]	+= $hd->仕入金額->value;
							$dat_m[ 'kei' ]		+= $hd->仕入金額->value;
							$dat_m = &$dat['金額'][ TC_SKEI_KBN_ARA ];	// 粗利
							$dat_m[ $wkTsuki ]	+= $hd->売上金額->value - $hd->仕入金額->value;
							$dat_m[ 'kei' ]		+= $hd->売上金額->value - $hd->仕入金額->value;

							// 次の集計年月計算用(○ヶ月単位)
							$idxDate->add( new DateInterval("P".$sekisan."M") );
						}
					}
				}

			}

			// ------------------------------
			// 契約更新月の算出。(本年分のみ)
			// ------------------------------
			foreach( $aryHosyuData as $hsRecno => $val ) {
				$nowYear	= date( 'Y' );

				//$strDate 	= new DateTime( $val['契約開始日'] );	2013.12.21
				//$endDate 	= new DateTime( $val['契約終了日'] );	2013.12.21
				$strDate 	= new DateTime( $aryHsHiduke[ $hsRecno ]['保守開始日'] );
				$endDate 	= new DateTime( $aryHsHiduke[ $hsRecno ]['保守終了日'] );

				//if( $val['契約終了日'] == "" ) { 2013.12.21
				if( $aryHsHiduke[ $hsRecno ]['保守終了日'] == "" ) {
					// 年内を対象とする
					$endDate->setDate( $endDate->format("Y") , 12 , 1 );
				}
				// 開始が来年以降は計算しない。
				if( $nowYear < $strDate->format("Y") ) {
					continue;
				}
				// 終了済みは計算しない。
				if( $endDate->format("Y") < $nowYear ) {
					continue;
				}
				// 契約数がない場合は計算しない。
				if( $val['契約数'] == "" ) {
					continue;
				}

				// 開始終了計算
				$strDate->setDate( $strDate->format("Y") , $strDate->format("m") , 1 );	// 計算結果がづれ無いようにする。 例）1/31 に 2か月足すと、3/3 になる。
				$endDate->setDate( $endDate->format("Y") , $endDate->format("m") , 1 ); // 計算結果がづれ無いようにする。

				$idxDate = clone $strDate;
				$endCnt = ( $endDate->format("Y")*12 + $endDate->format("m") ) - ( $strDate->format("Y")*12 + $strDate->format("m") );

				$sekisan = $val['契約数'];
				if( $val['契約単位'] == TC_SEK_TNI_NEN ) {
					$sekisan = $sekisan * 12;
				}
				for( $cnt=0; $cnt <= $endCnt; $cnt = $cnt + $sekisan ) {
					// 集計年月
					$wkY = $idxDate->format( 'Y' );
					$wkM = $idxDate->format( 'm' ) - 0;
					switch( true ) {
						case ( $wkY == $nowYear ):	// 本年
							$this->kykupdM[ $hsRecno ][ $wkM ] = 1;
							break;

						case ( $wkY > $nowYear ):	// 本年以降
							break 2;				// 計算を終わる

						default:
							break;
					}
//echo "保守 $hsRecno 　　年月 $wkY-$wkM ($nowYear) 　積算 $sekisan 　終わり $endCnt<br>\n";

					// 次の集計年月計算用(○ヶ月単位)
					$idxDate->add( new DateInterval("P".$sekisan."M") );
				}
			}
//print_r($this->kykupdM);
//print_r($this->stockData);

			// 保守管理のレコード番号を返す。
			$ret = array_keys( $aryHosyuData );

			return ( $ret );
		}

		/*************************************************************************/
	    /* 労務原価（保守）から、労務原価を取得する。                            */
	    /*  引数	                                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function getRomuGen() {
			$ret = false;

			//---------------------------------------
			// 労務原価（保守）のデータを全て取得する。
			//---------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCRMHK;		// 労務原価 TC労原価管理(保守)
			$k->arySelFileds	= array( "レコード番号" , "保守レコード番号" , "作業日付" , "実行労務費_保守_円" );

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			$aryRomuData = array();
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
			    $k->strQuery = "レコード番号 > $recno order by レコード番号 asc";
				$rmhs_json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の作業日報の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設定
				$recno = $rmhs_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $rmhs_json->records as $key => $rec ) {
					if( $rec->作業日付->value == "" ) {
						continue;
					}
					$idxNenGetsu = date( "Ym" , strtotime($rec->作業日付->value) );
					$dat = &$this->uriData[ $rec->保守レコード番号->value ][ $idxNenGetsu ];
					$dat[ 'romugenka' ]	+= $rec->実行労務費_保守_円->value;
					$dat[ 'junrieki' ]   = $dat[ 'arari' ] - $dat[ 'romugenka' ];
					$dat[ 'skikbn' ] 	 = TC_SKIKBN_MEISAI;
				}
			} while( $k->intDataCount > 0 );

			$ret = true;

			return ( $ret );
		}

		/*************************************************************************/
	    /* 年計、累計を計算する                                                  */
	    /*  引数	                                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function calcData() {
			$nenkei = array();
			$ruikei = array();

			// 年計
			foreach( $this->uriData as $hsRecno => $hrec ) {
				foreach( $hrec as $key => $val ) {
					$idx = substr( $key , 0 , 4 )."99";
					$dat = &$nenkei[ $hsRecno ][ $idx ];
					$dat[ 'uriage' ] 		+= $val[ 'uriage' ];
					$dat[ 'genka' ] 		+= $val[ 'genka' ];
					$dat[ 'arari' ] 		+= $val[ 'arari' ];
					$dat[ 'romugenka' ] 	+= $val[ 'romugenka' ];
					$dat[ 'skikbn' ]		 = TC_SKIKBN_KEI_NEN;
				}
			}
			// 年計をデータ配列へ統合
			foreach( $nenkei as $hsRecno => $hrec ) {
				foreach( $hrec as $key => $val ) {
					$this->uriData[ $hsRecno ][ $key ] = $val;
				}
			}

			// 累計
			foreach( $nenkei as $hsRecno => $hrec ) {
				foreach( $hrec as $key => $val ) {
					$dat = &$this->uriDataRuiKei[ $hsRecno ];
					$dat[ 'uriage' ] 		+= $val[ 'uriage' ];
					$dat[ 'genka' ] 		+= $val[ 'genka' ];
					$dat[ 'arari' ] 		+= $val[ 'arari' ];
					$dat[ 'romugenka' ] 	+= $val[ 'romugenka' ];
					$dat[ 'uririekiritu' ]   = $this->CalcRitu( $dat[ 'junrieki' ] , $dat[ 'uriage' ] , TC_RITU_SKETA );
					$dat[ 'araririekiritu' ] = $this->CalcRitu( $dat[ 'junrieki' ] , $dat[ 'arari' ]  , TC_RITU_SKETA );
					$dat[ 'skikbn' ]		 = TC_SKIKBN_KEI_RKEI;

				}
			}

			// 更新用ダミー
			foreach( $this->arrHskrRecno as $key => $hsRecno ) {
				$dat = &$this->uriDataRuiKei[ $hsRecno ];
			}
		}

		/*************************************************************************/
	    /* 保守売上履歴を削除する                                                */
	    /*  引数	array       削除対象の保守管理レコード番号                   */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function delHRRK( $pRecno ) {
			$ret = false;

			// ----------------------------------------------------
			// 労務原価から削除対象のデータ(レコード番号)を取得する
			// ----------------------------------------------------
			$aryHskrRecno = array();

			// kintone データ読込実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->strHeader		= "";					// ヘッダパラメタ
			$k->intAppID 		= TC_APPID_TCHRRK;		// アプリID
		    $k->arySelFileds	= array( "レコード番号" );

			$recno = 0;
			do {
				$aryQ 	= array();
				if( count( $pRecno ) == 0 ) {
					//
				} else {
					$aryQ[] = "( レコード番号_保守契約_ in ( ".implode( $pRecno , ",")." ) )";
				}
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				$json = $k->runCURLEXEC( TC_MODE_SEL );
				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json->records as $key => $rec ) {
					$aryHskrRecno[] = $rec->レコード番号->value;
				}

				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;


			} while( $k->intDataCount > 0 );

			// --------
			// 削除実行
			// --------
			$this->delData( TC_APPID_TCHRRK , $aryHskrRecno );

			$ret = true;

			return( $ret );
		}

		/*************************************************************************/
	    /* 保守売上履歴へ追加する                                                */
	    /*  引数                                                                 */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insHRRK() {
			$ret = false;

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsHsRRK = array();

			foreach( $this->uriData as $hsRecno => $hrec ) {
				foreach( $hrec as $key => $val ) {
					// 書込み準備

					// 年月、保守レコード番号 集計行対応
					$wkNen	 = substr( $key , 0 , 4 )."年";
					$wkTsuki = substr( $key , 4 , 2 )."月";
					switch( $val[ 'skikbn' ] ) {
						case TC_SKIKBN_MEISAI:
							$wkNenGetsu = $wkNen.$wkTsuki;
							$wkSkeikbn  = "-";
							break;

						case TC_SKIKBN_KEI_TUKI:	// 2013.5.10 未使用
							$wkNenGetsu = "【 ".$wkTsuki." 】";
							$wkSkeikbn  = "月計";
							break;

						case TC_SKIKBN_KEI_NEN:
							$wkNenGetsu = "【 ".$wkNen."合計 】";
							$wkSkeikbn  = "年計";
							break;

						case TC_SKIKBN_KEI_RKEI:	// 2013.5.10 未使用
							$wkNenGetsu = "【 累計 】";
							$wkSkeikbn  = "累計";
							break;

						default:
							$wk = "";
							$wkSkeikbn  = "";
					}
					// 保守レコード番号 集計行対応

					$val[ 'uriage' ] -= 0;
					$val[ 'genka' ]  -= 0;
					$val[ 'arari' ]  -= 0;
					$val[ 'romugenka' ] -= 0;
					$val[ 'junrieki' ]  = $val[ 'arari' ] - $val[ 'romugenka' ];
					$recObj = new stdClass;
					$recObj->レコード番号_保守契約_	= $this->valEnc( $hsRecno );
					//$recObj->顧客コード_保守_		= $this->valEnc( $val['kkkcode'] );
					$recObj->年月 					= $this->valEnc( $wkNenGetsu );
					$recObj->年月計算				= $this->valEnc( $key );
	 				$recObj->売上			 		= $this->valEnc( $val[ 'uriage' ] );
					$recObj->原価					= $this->valEnc( $val[ 'genka' ] );
					$recObj->粗利益					= $this->valEnc( $val[ 'arari' ] );
					$recObj->労務原価				= $this->valEnc( $val[ 'romugenka' ] );
					$recObj->純利益					= $this->valEnc( $val[ 'junrieki' ] );
					$recObj->売上利益率				= $this->valEnc( $this->CalcRitu( $val[ 'junrieki' ] , $val[ 'uriage' ] , TC_RITU_SKETA ) );
					$recObj->粗利利益率				= $this->valEnc( $this->CalcRitu( $val[ 'junrieki' ] , $val[ 'arari' ]  , TC_RITU_SKETA ) );
					$recObj->集計行区分				= $this->valEnc( $wkSkeikbn );
					$aryInsHsRRK[ $idxM ][ $idxR ] = $recObj;
					$idxR += 1;
					if( $idxR == CY_INS_MAX ) {
						$idxM += 1;
						$idxR = 0;
					}
					$recObj = null;

				}
			}

			// ------------------------
			// 売上保守履歴へ追加する。
			// ------------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHRRK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";
			foreach( $aryInsHsRRK as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCHRRK;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_INS );

				if( $k->strHttpCode == 200 ) {
					// 正常
				} else {
					$errmsg = "更新エラー -> CODE：".$k->strHttpCode;
					break;
				}
			}

			// --------------------------
			// 戻り値メッセージ
			// --------------------------
			$msg   = array();
			if( $errmsg == "" ) {
				// 正常時
				$msg[] = "保守計上履歴を作成しました。</b>";
			} else {
				// エラー時
				$msg[] = "<b>保守計上履歴の作成を中止しました。</b>";
				$msg[] = "保守計上履歴データ作成時に、エラーが発生しました。";
				$msg[] = "<b>$errmsg</b>";
				$msg[] = "";
			}

			return( join( "<br>\n" , $msg ) );

//			$ret = true;
//
//			return( $ret );
		}

		/*************************************************************************/
	    /* 保守管理を更新する                                                    */
	    /*  引数                                                                 */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updHSKR() {
			$ret = false;

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryUpdHskr = array();

			foreach( $this->uriDataRuiKei as $hsRecno => $val ) {
				if( $hsRecno =="" ) {
					continue;
				}
				// 保守レコード番号 集計行対応
				$val[ 'junrieki' ]  = $val[ 'arari' ] - $val[ 'romugenka' ];
				$recObj = new stdClass;
				$recObj->id					= $hsRecno;
 				$recObj->record->売上		= $this->valEnc( $val[ 'uriage' ] - 0 );
				$recObj->record->原価		= $this->valEnc( $val[ 'genka' ]  - 0 );
				$recObj->record->粗利益		= $this->valEnc( $val[ 'arari' ]  - 0 );
				$recObj->record->労務原価	= $this->valEnc( $val[ 'romugenka' ] - 0 );
				$recObj->record->純利益		= $this->valEnc( $val[ 'junrieki' ]  - 0 );
				$recObj->record->売上利益率	= $this->valEnc( $this->CalcRitu( $val[ 'junrieki' ] , $val[ 'uriage' ] , TC_RITU_SKETA ) );
				$recObj->record->粗利利益率	= $this->valEnc( $this->CalcRitu( $val[ 'junrieki' ] , $val[ 'arari' ]  , TC_RITU_SKETA ) );
				// 更新月チェック
/*
				for( $i = 1 ; $i <= 12 ; $i++ ) {
					$wkAry[] = $i."月";
 					  $this->kykupdM[ $hsRecno ][$i] );
				}
*/
				if( count( $this->kykupdM[ $hsRecno ] ) > 0 ) {
					$wkAry = array();
					foreach( $this->kykupdM[ $hsRecno ] as $tuki => $dmy ) {
						if( $dmy == "" ) {
							//
						} else {
							$wkAry[] = $tuki."月";
						}
					}
					$recObj->record->契約更新月 = $this->valEnc( $wkAry );
				}

				// 更新用配列へ
				$aryUpdHskr[ $idxM ][ $idxR ] = $recObj;
				$idxR += 1;
				if( $idxR == CY_UPD_MAX ) {
					$idxM += 1;
					$idxR = 0;
				}
				$recObj = null;
			}
			// --------------------
			// 保守管理を更新する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
			$k->strContentType	= "Content-Type: application/json";
			foreach( $aryUpdHskr as $key => $val ) {
				$updData 			= new stdClass;
				$updData->app 		= TC_APPID_TCHSKR;
				$updData->records 	= $val;
				$k->aryJson = $updData;						// 更新対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_UPD );
			}

			$ret = true;

			return( $ret );
		}

		/*************************************************************************/
	    /* 保守管理と保守売上履歴を同期する                                      */
	    /*  引数	                                                             */
	    /*  関数値  boolean			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function sycHskr() {
			$ret = false;

			//---------------------------------------
			// 保守管理のレコード番号を全て取得する。
			//---------------------------------------
			$k = new TcKintone();						// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;		// アプリID（保守管理）
			$k->arySelFileds	= array(レコード番号);

			// 取得件数制限ごとにループして処理を行う。
			// レコード番号の昇順ソートは、欠番を拾うためにも必須。
			$aryHskrRecno = array();
			// kintoneデータ取得件数制限の対応。
			$recno = 0;
			do {
			    $k->strQuery = "レコード番号 > $recno order by レコード番号 asc";
				$hskr_json = $k->runCURLEXEC( TC_MODE_SEL );
				// 更新対象の保守管理の取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 次に読み込む条件の設
				$recno = $hskr_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				// レコード番号を取得する
				foreach( $hskr_json->records as $key => $rec ) {
					$aryHskrRecno[] = $rec->レコード番号->value;
				}
			} while( $k->intDataCount > 0 );

			//---------------------------------------
			// レコード番号を取得する。
			//---------------------------------------
			// 保守管理の最小、最大レコード番号
			if( count( $aryHskrRecno ) == 0 ) {
				$hskrMinRecno = 0;
				$hskrMaxRecno = 0;
			} else {
				$hskrMinRecno = $aryHskrRecno[0];
				$hskrMaxRecno = $aryHskrRecno[ count($aryHskrRecno) -1 ];
			}

			// 保守売上履歴の最小、最大レコード番号
			$hrrkMinRecno = $this->getAppMaxMin( TC_APPID_TCHRRK , "レコード番号_保守契約_" , CY_DAT_MIN );
			$hrrkMaxRecno = $this->getAppMaxMin( TC_APPID_TCHRRK , "レコード番号_保守契約_" , CY_DAT_MAX );

			//-------------------------------------------
			// 保守管理のレコード番号から欠番を抽出する。
			// （削除対象のレコード番号）
			//-------------------------------------------
			$aryDelRecno = array();
			// 保守売上履歴_最小 < 保守管理_最小（保守売上履歴_最小 から 保守管理_最小 までが削除対象。）
			for( $i = $hrrkMinRecno; $i < $hskrMinRecno; $i++ ) {
				$aryDelRecno[] = $i;
			}
			// 保守管理_最大 < 保守売上履歴_最大（保守管理_最大 から 保守売上履歴_最大 までが削除対象。）
			for( $i = $hskrMaxRecno + 1; $i <= $hrrkMaxRecno; $i++ ) {
				$aryDelRecno[] = $i;
			}
			// 保守管理_最小 から 保守管理_最大 までの欠番を探す。（削除対象）
			for( $i=0; $i < count($aryHskrRecno) - 1; $i++ ) {
				$strRec = $aryHskrRecno[ $i ];
				$endRec = $aryHskrRecno[ $i + 1 ];
				//
				$ketu = $endRec - $strRec;
				if( $ketu > 1 ) {
					for( $delRec = $strRec + 1 ; $delRec < $endRec; $delRec++ ) {
						$aryDelRecno[] = $delRec;
					}
				}
			}

			// --------
			// 削除実行
			// --------
			if( count($aryDelRecno) > 0 ) {
				$this->delHRRK( $aryDelRecno );
			}

			$ret = true;
			return ( $ret );
		}


		/*************************************************************************/
	    /* 保守売上履歴を削除する                                                */
	    /*  引数	array       削除対象の保守管理レコード番号                   */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function delHUIT() {
			$ret = false;

			// --------------------
			// ストック売上管理を全削除
			// --------------------
			$aryDelRecno = array();

			// kintone データ読込実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->strHeader		= "";					// ヘッダパラメタ
			$k->intAppID 		= TC_APPID_TCHUIT;		// アプリID
		    $k->arySelFileds	= array( "レコード番号" );

			$recno = 0;
			do {
				$aryQ 	= array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				$json = $k->runCURLEXEC( TC_MODE_SEL );
				if( $k->intDataCount == 0 ) {
					break;
				}
				foreach( $json->records as $key => $rec ) {
					$aryDelRecno[] = $rec->レコード番号->value;
				}

				// 次に読み込む条件の設定
				$recno = $json->records[ $k->intDataCount - 1 ]->レコード番号->value;

			} while( $k->intDataCount > 0 );

			// --------
			// 削除実行
			// --------
			$this->delData( TC_APPID_TCHUIT , array_unique( $aryDelRecno ) );

			$ret = true;

			return( $ret );		}

		/*************************************************************************/
	    /* 保守売上履歴へ追加する                                                */
	    /*  引数                                                                 */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insHUIT() {
			$ret = false;

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------
			$idxM = 0;
			$idxR = 0;
			$aryInsHsRRK = array();

			foreach( $this->stockData as $sekisan => $val_1 ) {
				foreach( $val_1 as $nen => $val_2 ) {
					foreach( $val_2 as $kkkcode => $uriRec ) {
						foreach( $uriRec as $hsrecno => $val ) {
							foreach( $val['金額'] as $kbn => $kin ) {
								// 書込み準備
								$recObj = new stdClass;
								$recObj->積算単位 	= $this->valEnc( $sekisan );
								$recObj->年 		= $this->valEnc( $nen );
//								$recObj->顧客コード	= $this->valEnc( $kkkcode );
								$recObj->顧客名		= $this->valEnc( $val['顧客名'] );
								$recObj->契約名		= $this->valEnc( $val['契約名'] );
								$recObj->保守レコード番号	= $this->valEnc( $hsrecno );
								$recObj->保守レコード番号_ルックアップ_	= $this->valEnc( $hsrecno );
				 				$recObj->区分 		= $this->valEnc( $kbn );
								switch( $kbn) {
									case TC_SKEI_KBN_URI:	// 売上
										$kbnsort = 1;
										break;
									case TC_SKEI_KBN_GEN:	// 原価
										$kbnsort = 2;
										break;
									case TC_SKEI_KBN_ARA:	// 粗利
										$kbnsort = 3;

										for( $utuki = 1 ; $utuki <= 12 ; $utuki++ ) {
											$wkTuki = "売上".$utuki."月";
							 				$recObj->$wkTuki = $this->valEnc( $val['金額']['売上'][$utuki] );
										}
										for( $gtuki = 1 ; $gtuki <= 12 ; $gtuki++ ) {
											$wkTuki = "原価".$gtuki."月";
							 				$recObj->$wkTuki = $this->valEnc( $val['金額']['原価'][$gtuki] );
										}

										break;
									default:
										$kbnsort = 0;
										break;
								}
								$recObj->区分並び順	= $this->valEnc( $kbnsort );
								$recObj->合計 		= $this->valEnc( $kin[ 'kei' ] );
								for( $tuki = 1 ; $tuki <= 12 ; $tuki++ ) {
									$wkTuki = "_".$tuki."月";
					 				$recObj->$wkTuki = $this->valEnc( $kin[ $tuki ] );
								}

								$aryInsHsRRK[ $idxM ][ $idxR ] = $recObj;
								$idxR += 1;
								if( $idxR == CY_INS_MAX ) {
									$idxM += 1;
									$idxR = 0;
								}
								$recObj = null;
							}
						}
					}
					echo "・";
				}
			}

			// --------------------
			// 労務原価へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHUIT;			// アプリID
			$k->strContentType	= "Content-Type: application/json";
			foreach( $aryInsHsRRK as $key => $val ) {
				$insData 			= new stdClass;
				$insData->app 		= TC_APPID_TCHUIT;
				$insData->records 	= $val;
				$k->aryJson = $insData;						// 追加対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_INS );
print_r($k);
			}

			$ret = true;

			return( $ret );
		}


		/*************************************************************************/
	    /* 指定したアプリのデータを削除する                                      */
	    /*  引数	$appid    		アプリＩＤ                                   */
	    /*       	$pDelRecno   	削除対象のレコード番号                       */
	    /*  関数値  string			正常終了:true、異常終了:false                */
	    /*************************************************************************/
		function delData( $appid , &$pDelRecno ) {
			// -----------------------------------------------
			// CY_DEL_MAX個ごとに処理をする。（kintoneの制限）
			// -----------------------------------------------
			$aryDelRmgk = $this->makeMaxArray( $pDelRecno , CY_DEL_MAX );

			// 削除実行
			$k = new TcKintone();
			$k->parInit();								// API連携用のパラメタを初期化する
			// 以下、要修正
			$k->intAppID 		= $appid;				// アプリID
			$k->aryDelRecNo		= array();
			foreach( $aryDelRmgk as $key => $val ) {
	    		$k->aryDelRecNo = $val;					// 削除対象のレコード番号
				$json = $k->runCURLEXEC( TC_MODE_DEL );
			}
		}

		/*************************************************************************/
	    /* アプリの項目の最大値、最小値を取得する                                */
	    /*  引数	$appid    	アプリＩＤ                                       */
	    /*       	$fld    	取得するフィールドコード                         */
	    /*       	$kbn    	最小(CY_SEL_MIN)、最大(CY_SEL_MAX) 取得区分      */
	    /*  関数値  $fld値      データあり：$fld値、なし：null                   */
	    /*************************************************************************/
		function getAppMaxMin( $appid , $fld , $kbn ) {
			$ret = null;

			// 労務原価データの最終作成日付を取得する
			$k = new TcKintone();
			$k->parInit();										// API連携用のパラメタを初期化する
			$k->intAppID 		= $appid;						// アプリID
		    $k->arySelFields	= array( $fld ); 				// 読込用フィールドパラメータ
		    $k->strQuery 		= "order by $fld $kbn limit 1";	// クエリパラメータ

			$json = $k->runCURLEXEC( TC_MODE_SEL );
			if( $k->intDataCount == 0 ) {
				//
			} else {
				$ret = $json->records[0]->$fld->value;
			}

			return ( $ret );
		}

		/*************************************************************************/
	    /* arrayをMAX件数ごとに分割する。                                        */
	    /*  引数	&$pAry    	処理対象の配列                                   */
	    /*       	$pMax    	分割する件数                                     */
	    /*  関数値  array       $pMaxごとの2次元配列                             */
	    /*************************************************************************/
		function makeMaxArray( &$pAry , $pMax ) {
			$aryRet = array();

			$idxM = 0;	// CY_DEL_MAX件ごとのインデックス
			$idxR = 0;	//
			foreach( $pAry as $key => $val ) {
				if( isNull($val) ){
					//
				} else {
					$aryRet[ $idxM ][ $idxR ] = $val;
					$idxR += 1;
					if( $idxR == $pMax ) {
						$idxM += 1;
						$idxR = 0;
					}
				}
			}

			return ( $aryRet );
		}

		/*************************************************************************/
	    /* arrayをMAX件数ごとに分割する。                                        */
	    /*  引数	&$pAry    	処理対象の配列                                   */
	    /*       	$pMax    	分割する件数                                     */
	    /*  関数値  array       $pMaxごとの2次元配列                             */
	    /*************************************************************************/
		function CalcRitu( $pNum1 , $pNum2 , $pSKeta ) {
			if( $pNum2 == 0 ) {
				$ret = number_format( 0 , $pSKeta );
			} else {
				$ret = number_format( ( ($pNum1 - 0) / ($pNum2 - 0) ) * 100 , $pSKeta , "." , "" );
			}

			return( $ret );
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function getDateNenGetsu( $pDate ) {
			$ret = null;

			if( isNull($val) ){
				//
			} else {
				$ret = date( "Ym", $pDate );
			}

			return ( $ret );
		}

		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			if( is_array($val) ){
				foreach( $val as $key => $encwk ) {
					$wk->value[] = mb_convert_encoding($encwk , "UTF-8", "auto");
				}
			} else {
				$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			}
			return ( $wk );
		}

		function SendUrl( $pUrl) {
			$ret = null;
			$cHand = null;

			// ----------------
			// http通信の初期化
			// ----------------
			$cHand = curl_init( $pUrl );
			if( $cHand ) {
				//
			} else {
				$this->err->setError( ERR_T000 );
				return( $ret );
			}

			$this->aryOptionHeader = array(
				"Host: timeconcier.jp:443" ,
			);
			// オプション設定
			curl_setopt($cHand, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($cHand, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($cHand, CURLOPT_SSLVERSION, 3);
			curl_setopt($cHand, CURLOPT_HEADER, false);
			curl_setopt($cHand, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($cHand, CURLOPT_FAILONERROR, true);
			curl_setopt($cHand, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

			// ------------------
			// HTTP通信を実行する
			// ------------------
			$respons = curl_exec($cHand);

			// HTTPレスポンスのステータスコードを標準出力する
			$strHttpCode = curl_getinfo($cHand , CURLINFO_HTTP_CODE);

			// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
			curl_close($cHand);

			// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
			if ( $strHttpCode == 200 ) {
				$ret = $respons;
			}

			return( $ret );
		}

	}

?>
