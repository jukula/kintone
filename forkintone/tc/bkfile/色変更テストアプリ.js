(function(){
/***********************************************************/
//グラフのバーの色を変えるサンプル
//
/***********************************************************/
	var setIDflg = 0;  // 適応済みフラグ（0=未変更/1=変更済）

    setInterval(TcMainFunction, 1000);

    function TcMainFunction(){

        /************************************************/
        // 定期的に動作
        /************************************************/
		if(setIDflg == 0){

			var graphData = document.getElementsByClassName('highcharts-series highcharts-tracker');	//1レコードごとのデータを取得する
			var listData = document.getElementsByClassName('highcharts-legend-item');					//1レコードごとのデータを取得する

			//要素数だけclass名を変更する
			for(var cnt = 0 ; cnt < listData.length ; cnt++){
				for(var k = 0 ; k < listData[cnt].childNodes.length ; k++){

					switch (listData[cnt].childNodes[k].textContent){
					  case "9：Ｆ(０％)_失注":
						listData[cnt].childNodes[(k+1)].style.fill 			= "paleturquoise" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "paleturquoise" ;
						}
					    break;
					  case "6：Ｅ(１０％)_卵":
						listData[cnt].childNodes[(k+1)].style.fill 			= "skyblue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "skyblue" ;
						}
					    break;
					  case "5：Ｄ(３０％)":
						listData[cnt].childNodes[(k+1)].style.fill 			= "deepskyblue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "deepskyblue" ;
						}
					    break;
					  case "4：Ｃ(５０％)":
						listData[cnt].childNodes[(k+1)].style.fill 			= "dodgerblue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "dodgerblue" ;
						}
					    break;
					  case "3：Ｂ(７０％)":
						listData[cnt].childNodes[(k+1)].style.fill 			= "royalblue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "royalblue" ;
						}
					    break;
					  case "2：Ａ(９０％)_内示":
						listData[cnt].childNodes[(k+1)].style.fill 			= "blue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "blue" ;
						}
					    break;
					  case "1：Ｓ(１００％)_受注":
						listData[cnt].childNodes[(k+1)].style.fill 			= "midnightblue" ;
						for(var j = 0 ; j < graphData[cnt].childNodes.length ; j++){
							graphData[cnt].childNodes[j].style.fill = "midnightblue" ;
						}
					    break;
					}
				}
			}

			setIDflg = 1; // 変更済なのでフラグを1に変更
		}
	}
	
})();