/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TC基本Utility
//   どのアプリからでも呼び出されるモジュール集
//
//
// ▼どこでも利用
//  - TcPutDmyField_Sp().........要素IDで指定したスペース(空白パーツ)をダミーフィールドに置き換え
//  - TcGetDmyField_Val()........フィールドIDで指定したダミーフィールドのValue値を取得
//  - TcPutFieldMsg()............フィールドIDで指定した通常フィールドに対してメッセージを表示
//  (具体的な機能)
//    - TcChangeColorTbl().......特定のチェックがされたテーブル行の背景色を変更
//    - TcCalcTerm().............開始日～終了日から期間(日数)を算出
//    - TcAppDataPost()..........アプリIDで指定した同サブドメイン内の他アプリを新規編集で開いてデータ転記
//    - TcComputeDate()..........n日後の日付を取得
//
// ▼イベントハンドラで利用
//  - TcPutButton_Cd()...........フィールドコードで指定した通常フィールドに対してボタンを配置(詳細画面のみ対応)
//  - TcPutButton_Sp()...........要素IDで指定したスペース(空白パーツ)に対してボタンを配置(詳細・追加・編集画面対応)
//  - TcPutButton_Index()........一覧画面のメニューの右or下側にボタンを配置(一覧画面のみ対応)
//  - TcTextSizeChange().........詳細画面表示時に、指定したフォントサイズに変更
//
// ▼イベントハンドラ以外で利用
//  - TcPutButton_Id()...........フィールドIDで指定した通常・テーブルフィールドに対してボタン配置
//  - TcDisplayField()...........フィールドIDで指定した通常・テーブルフィールドの表示非表示切替
//  - TcGetRadioChecked()........フィールドIDで指定したラジオボタンの選択値を取得
//  - TcGetChkBoxChecked().......フィールドIDで指定したチェックボックスの選択値を取得
//  - TcGetDropDown()............フィールドIDで指定したドロップダウンの選択値を取得
//  - TcGetTimeText()............フィールドIDで指定した時刻フィールドの値を取得
//  - TcGetUserText()............フィールドIDで指定したユーザフィールドの選択値を取得
//  (具体的な機能)
//    - TcClassifySumTbl().......テーブルの指定数値をドロップダウンの選択値(種目)別に合計値算出
//    - TcTblSeqOperate()........テーブルの行追加削除ボタンを、チェックボックスの状態によって制御
//    - TcPostTblChkData().......テーブルのチェックされた行の指定値(金額など)を、決定値として別フィールドに転記
//    - TcCalcTblSubTtl()........テーブル行毎の小計、テーブル全体の小計を計算してフィールド更新
//    - TcPostFieldData()........指定したフィールドの値を別のフィールドに差し込む
//    - TcPostDDtoLU()...........ルックアップ取得内容をドロップダウンの内容で絞り込む
//
// ▽モジュール
//  - TcSwCopy().................別アプリに転記するためのモジュール(TcAppDataPost()等で利用)
//  - TcRetObjVal()..............JSONオブジェクトのValue値を返す
//  - TcCallPHP()................外部PHPの呼び出し実行
//  - TcMakeButton().............ボタン要素を作成・返す
//  - TcFilLookUpFld()...........ドロップダウン選択値をルックアップ入力欄に転記
//
// ▼メモ
// ※詳細画面表示時のテーブルに対してボタン配置やフィールド制御はできない
//   画面表示時にテーブルが表示されきれていない場合があり、テーブルサイズが取得できない為である
//
// ▼更新履歴
// 20140117	新規作成(m.takeuchi)
// 20140520 TcGetChkBoxChecked内のinnerText→textContentに変更(a.oono)
/*--------------------------------------------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TCUtility(JS)
//   - TC社の基本的なユーティリティ関数で、どのアプリからでも利用される想定のもの
/*--------------------------------------------------------------------------------------------------------------------------------------------*/

// --------------------------------
// ▼どこでも利用
// --------------------------------
    /******************************************************/
    // ダミーフィールド配置
    //  - 要素IDで指定したスペース(空白パーツ)をダミーフィールドに置き換え
    //   FldCd		置き換え先スペース要素ID
    //   FldLbl		表示するラベル
    //   FldVal		表示する値
    //   Wsize		フィールド横幅(省略可)
    //   Fsize		Valueのフォントサイズ(省略可)
    /******************************************************/
    function TcPutDmyField_Sp (FldCd, FldLbl, FldVal, Wsize, Fsize) {

        if ( Wsize == undefined || Wsize == "auto" ) { Wsize = "auto"; } else { Wsize += "px";}		// Wsizeが省略された場合、サイズは"auto"
        if ( Fsize == undefined ) { Fsize = 10; }			// Fsizeが省略された場合、規定フォントサイズは10pt

        // ダミーフィールド(div要素)作成
        var div1 = document.createElement('div');
        div1.setAttribute("style", "box-sizing: border-box; width: " + Wsize + "; height: auto;");
        div1.setAttribute("class", "control-gaia control-show-gaia");
        div1.innerHTML = "<div class='control-label-gaia'><span>" + FldLbl + "</span></div>"
                        + "<div class='control-value-gaia'><span class='dmyVal' style='white-space: nowrap; font-size:" + Fsize + "pt'>" + FldVal + "</span></div>"
                        + "<div class='control-design-gaia'></div>";
        div1.id = "DmyFld_" + FldCd;

        // FldCdのスペースをダミーフィールドに置き換え
        var tmpKmk = kintone.app.record.getSpaceElement(FldCd);
        // ボタン配置
        if ( isShowPage() ) {
            tmpKmk.parentNode.appendChild(div1);	// 詳細画面の場合
        } else {
            tmpKmk.appendChild(div1);				// 追加・編集画面の場合
        }
    }


    /******************************************************/
    // ダミーフィールドのValue値を取得
    //   FldId		ダミーフィールドのスペース要素ID
    //   関数値		フィールド値
    /******************************************************/
    function TcGetDmyField_Val(FldId) {

        // 指定ダミーフィールドをIDで取得
        var elDmy = document.getElementById("DmyFld_" + FldId);

        // 指定ダミーフィールドのValue値をreturn
        return elDmy.getElementsByClassName("dmyVal")[0].textContent;
    }


    /******************************************************/
    // フィールドメッセージ表示
    //  - 任意のメッセージを出力したい場合に使用
    //   FldId		表示対象フィールドID
    //   MsgKind	メッセージ種別 ("OK"/"ERR")
    //   MsgStr		表示メッセージ内容
    /******************************************************/
    function TcPutFieldMsg(FldId, MsgKind, MsgStr){

        if ( MsgKind == "OK" ) {
            var strClass = 'validator-valid-cybozu';	// ルックアップ取得メッセージ
        } else if ( MsgKind == "ERR" ) {
            var strClass = 'input-error-cybozu';		// ルックアップ入力漏れメッセージ
        }

        var check = getFieldValueEl_(FldId).getElementsByClassName( strClass );
        if(check.length == 0){
            // 必須メッセージがない場合のみ、エレメント追加
            var el = document.createElement('div');
            el.className = strClass;
            el.innerHTML = MsgStr;
            getFieldValueEl_(FldId).appendChild(el);
        }
    }


    /******************************************************/
    // チェックされたテーブル行の背景色を変更する
    //  - チェックボックス・ラジオボタンに対応
    //   FldId		チェック対象フィールドID
    //   SelStr		チェックされたら背景色を変える選択文字
    //   COLOR		変更する色
    /******************************************************/
    function TcChangeColorTbl (FldId, SelStr, COLOR) {

        /*
        ** フィールドIDを配列に変更
        */
        if ( Array.isArray(FldId) == true ) {
            var aryPutFld = FldId;		// フィールドIDを配列で指定した場合
        } else {
            var aryPutFld = [FldId];	// フィールドIDを配列以外で指定した場合
        }

        for ( var cnt = 0; cnt < aryPutFld.length; cnt++ ) {

            var FldId = aryPutFld[cnt];

            // すべての行に対して背景色変更を適用する
            for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

                if ( isShowPage() ) {
                    // [仮入力]チェック有の場合、その行の背景色を変更する
                    var State = getFieldValueFromTable(FldId, row);		// [仮入力]の値を取得
                    if ( State.indexOf(SelStr) != -1 ) {
                        getFieldValueEls_(FldId, row).parentNode.parentNode.parentNode.setAttribute("style", COLOR);
                    }

                } else if ( isAddPage() || isEditPage() ) {
                    /*
                    ** クラス名からフィールド種別を判定しチェック状態を判定
                    */
                    var clsName = getFieldValueEls_(FldId, row).childNodes[0].className;
                    if ( clsName.indexOf("input-checkbox-cybozu") != -1 ) {
                        var State = TcGetChkBoxChecked(FldId, row);	// チェックされたボックス名
                    } else if ( clsName.indexOf("input-radio-cybozu") != -1 ) {
                        var State = TcGetRadioChecked(FldId, row);		// チェックされたボタン名
                    }

                    /*
                    ** テーブル行の色を変更する
                    */
                    var elTrgRow = getFieldValueEls_(FldId,row).parentNode.parentNode.parentNode;
                    if ( State.indexOf(SelStr) != -1 ) {
                        elTrgRow.setAttribute("style", COLOR);	// 指定文字のチェック有り：対象行の背景色を変更
                    } else {
                        elTrgRow.setAttribute("style", "");		// 指定文字のチェック無し：対象行の背景色を元に戻す
                    }

                } else { return 0; }
            }
        }
    }


    /******************************************************/
    // 開始日～終了日までの期間(日数)を求める
    //  - kintoneの計算フィールドでも計算できるが、値の書き換えをしたい際に使用する
    //    ★経過年等を求めるように拡張も可能(現段階2014/01/08では必要ない為書かない)
    //   strSrt		開始日
    //   strEnd		終了日
    //   関数値		日数
    /******************************************************/
    function TcCalcTerm(strSrt, strEnd) {

        // 取得文字列を日付型(システム経過秒)に変換
        var datSrtDate = Date.parse(strSrt);	// 開始日
        var datEndDate = Date.parse(strEnd);	// 終了日
        var ret = "";							// 戻り値

        // [着工予定日]と[完工予定日]が空白でない場合、工期を求める
        if ( strSrt != undefined && strEnd != undefined ) {
            ret = Math.ceil((datEndDate - datSrtDate) / (60*60*24*1000) + 1);	// 開始日から終了日までの日数
        }

        return ret;

    }


    /******************************************************/
    // 他アプリへデータをPOSTする汎用関数
    //   AppId      転記先アプリID
    //   Query      転記するデータ(形式はTcSwCopy()参照)
    //   RecId      詳細モードで開くレコード番号(省略可)
    //   関数値     なし
    /******************************************************/
    function TcAppDataPost(AppId, Query, RecId) {

        // 環境変数取得
        var prm = TcGetEnvPrm();

        // ウインドウ名の生成("childwin時分秒")
        var DD = new Date();
        var Hours = DD.getHours();
        var Minutes = DD.getMinutes();
        var Seconds = DD.getSeconds();
        var win_name = "childwin" + Hours + Minutes + Seconds;

        // 転記先アプリ
        if ( RecId == undefined || RecId < 1 ) {
            // レコード番号指定無しの場合：編集モードで開く
            var appURL = "https://" + prm['サブドメイン'] + ".cybozu.com/k/" + AppId + "/edit";
console.log("EditMode");
        } else {
            // レコード番号指定有りの場合：詳細モードで開く
            var appURL = "https://" + prm['サブドメイン'] + ".cybozu.com/k/" + AppId + "/show#record=" + RecId;
            Query = {};
console.log("ViewMode");
        }
        win = window.open(appURL, win_name);

        /*
        ** 別窓のアプリにTcSwCopy()でデータ転記
        ** アプリの表示、JS読み込みのタイミングをずらす(setInterval)
        */
        var tc = 0;
        timerID = setInterval( function() {
                                if(TcSwCopy(win, Query)) {
                                    clearInterval(timerID);
                                    timerID = null;
                                } else {
                                    tc++;
                                    if(tc>3) {
                                        clearInterval(timerID);
                                        timerID = null;
                                    }
                                }
                            }, 2500);
    }


    /******************************************************/
    // n日後の日付を取得
    // strDate		日付("yyyy-mm-dd"形式)
    // addDays		加算日(マイナス指定でn日前も設定可能)
    /******************************************************/
    function TcComputeDate(strDate, addDays) {

        // 日付がない(null)の場合はreturn
        if ( strDate == null ) { return (null); }

        // yyyy-mm-dd形式を年月日に分割
        var year  = strDate.split("-")[0];	// 年
        var month = strDate.split("-")[1];	// 月
        var day   = strDate.split("-")[2];	// 日

        // addDays日後の日付を求める
        var dt = new Date(year, month - 1, day);
        var baseSec = dt.getTime();
        var addSec = addDays * 86400000;		//日数 * 1日のミリ秒数
        var targetSec = baseSec + addSec;
        dt.setTime(targetSec);

        // yyyy-mm-dd形式にする
        var yy = dt.getFullYear();
        var mm = ("0" + (dt.getMonth() + 1)).slice(-2);
        var dd = ("0" + (dt.getDate())).slice(-2);
        var retDate = yy + "-" + mm + "-" + dd;

        return retDate;

    }


// --------------------------------
// ▼イベントハンドラで利用
// --------------------------------
    /******************************************************/
    // コードで指定されたフィールドに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //    ※詳細画面でのみ利用可能
    //   FldCd		ボタン配置先フィールドコード
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Cd(FldCd, BtnLbl, FncName, BtnTag, SetStyle) {

        // 詳細画面でない場合は処理しない
        if ( !isShowPage() ) {
            console.log("TcPutButton_Cdは、レコード詳細画面でのみ利用可能です。");	// 追加・編集画面ではgetFieldElement()は対応していない
            return;
        }

        // ★フィールドコードで指定した要素を取得
        var tmpKmk = kintone.app.record.getFieldElement(FldCd);

        // ボタンパーツを取得
        var emButton = TcMakeButton(FldCd, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        tmpKmk.parentNode.appendChild(emButton);	// 詳細画面の場合

    }


    /******************************************************/
    // 要素IDで指定されたスペースに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   ElmId		ボタン配置先スペース要素ID
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Sp(ElmId, BtnLbl, FncName, BtnTag, SetStyle) {

        // ★画面のスペース(空白)要素を取得
        var tmpKmk = kintone.app.record.getSpaceElement(ElmId);

        // ボタンパーツを取得
        var emButton = TcMakeButton(ElmId, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        if ( isShowPage() ) {
            tmpKmk.parentNode.appendChild(emButton);	// 詳細画面の場合
        } else {
            tmpKmk.appendChild(emButton);				// 追加・編集画面の場合
        }

    }


    /******************************************************/
    // 一覧画面のメニューの右or下側にボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   PosCd		ボタン配置先指定(right/bottom)
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Index(PosCd, BtnLbl, FncName, BtnTag, SetStyle) {

       // ★指定したポジション(右・下)の要素を取得
        if ( PosCd == "right" ) {
            var tmpKmk = kintone.app.getHeaderMenuSpaceElement();		// メニュー右側の空白
        } else if ( PosCd == "bottom" ) {
            var tmpKmk = kintone.app.getHeaderSpaceElement();			// メニュー下側の空白
        } else {
            console.log("PosCdの指定が不正です。");
        }

        // ボタンパーツを取得
        var emButton = TcMakeButton(PosCd, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        tmpKmk.appendChild(emButton);

    }


    /******************************************************/
    // 全体の文字サイズを変更
    //   sfontsize	フォントサイズ(pt)
    /******************************************************/
    function TcTextSizeChange(sfontsize) {
/*
        var odate = document.getElementsByClassName ('row-gaia clearFix-cybozu');
        if ( odate.length > 0 ) {
            for ( var odate_cnt = 0; odate_cnt < odate.length; odate_cnt++ ) {
                for ( var odate_Child_cnt = 0; odate_Child_cnt < odate[odate_cnt].childNodes.length; odate_Child_cnt++ ) {
                    if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes.length > 2 ) {
                        if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[1].childNodes.length == 1 ) {
                            if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[0].innerHTML.indexOf("<span>") == 0 ) {
                                 odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[1].childNodes[0].setAttribute("style", "font-size:" + sfontsize + "pt;");
                            }
                        }
                    }
                }
            }
        }
*/

        /*
        ** フィールド値の要素をすべて取得し、フォントサイズ指定する
        */
        var elVal = document.getElementsByClassName('control-value-gaia');
        for ( var cnt = 0; cnt < elVal.length; cnt++ ) {

            // ★取得した要素にsubtableを含むか判定
            var eltmp = elVal[cnt].getElementsByClassName('subtable-gaia');
            if ( !eltmp.length ) {
                // subtableを含まない(一般要素)場合、フォントサイズを変更する
                elVal[cnt].setAttribute("style", "font-size:" + sfontsize + "pt;");
            } else {
                // subtableを含む(関連レコード一覧要素)場合、列幅をauto指定
                elVal[cnt].parentNode.style.width = "auto"
            }

        }

    }


// --------------------------------
// ▼イベントハンドラ以外で利用
// --------------------------------
    /******************************************************/
    // IDで指定されたフィールドに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   FldId		ボタン配置先フィールドID
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Id(FldId, BtnLbl, FncName, BtnTag, SetStyle) {

        /*
        ** 引数FldIdをフィールドID配列に代入
        */
        if ( Array.isArray(FldId) == true ) {
            var aryPutFld = FldId;		// フィールドIDを配列で指定した場合
        } else {
            var aryPutFld = [FldId];	// フィールドIDを配列以外で指定した場合
        }

        if ( BtnTag == undefined ) { BtnTag = "btn"; }

        // 指定フィールドID個数分処理
        for ( var cnt = 0; cnt < aryPutFld.length; cnt++ ) {

            // ボタン配置対象を取得
            var FldId = aryPutFld[cnt];

            if ( typeof(FldId) != "number" ) { console.log("TcPutButton_Id()のFldIdは数値で指定してください。"); return 0; }	// FldIdが数値でない場合、リターン
            // 要素(Label/Value)を取得(ラベルのクラス名でテーブルか判定する為)
            var elLbl = getFieldLabelEl_(FldId);

            /*
            ** ボタンの配置方法によって処理を分ける
            **  - テーブルである場合：各行指定フィールドに対してボタンを配置。テーブル行の動的変更に対応する為、CALLの度にボタンIDを更新する。
            **  - テーブルでない場合：指定フィールドに対してボタンを配置。
            */
            if ( elLbl.className.indexOf("subtable") != -1 ) {

                /*
                ** テーブルの場合
                **  - テーブルのすべての行に対してボタン配置処理
                **    - 未配置の行には新規でボタンを配置
                **    - 配置済の行には既存ボタンのid情報を更新(id自体が行情報を保持するため)
                */
                for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

                    // 指定フィールドの要素数が2以下の場合のみボタンを置く(2個は必ずある)
                    if ( getFieldValueEls_(FldId, row).parentNode.childNodes.length < 3 ) {

                        // ボタンパーツを取得
                        var emButton = TcMakeButton(FldId, BtnLbl, FncName, BtnTag, SetStyle, row);

                        // 行毎にボタン配置
                        getFieldValueEls_(FldId, row).parentNode.appendChild(emButton);

                    } else {
                        /*
                        ** すでにボタン配置済みの場合、行番号でボタンidを更新する
                        */
                        var emButton = getFieldValueEls_(FldId, row).parentNode.lastChild;	// ボタン要素取得
                        emButton.id = 'btn' + FldId + '_' + row;							// ボタンID(行番号)更新
                    }

                }

            } else {

                /*
                ** テーブルではない場合
                **  - 指定された位置に新規でボタンを配置
                */
                // ボタンパーツを取得
                var emButton = TcMakeButton(FldId, BtnLbl, FncName, BtnTag, SetStyle);

                // ボタン配置
                if ( isShowPage() ) {
                    getFieldValueEl_(FldId).parentNode.appendChild(emButton);	// 詳細画面の場合
                } else {
                    getFieldValueEl_(FldId).appendChild(emButton);				// 追加・編集画面の場合
                }
            }

        }
    }


    /******************************************************/
    // 指定されたフィールドの表示非表示を切り替える
    //   FldId	対象フィールドID
    //   isDsp	表示:1 非表示:0
    /******************************************************/
    function TcDisplayField(FldId, isDsp) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(FldId);
        var elVal = document.getElementsByClassName("value-" + FldId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        // style.displayを書き換える値を設定(isDspが1の場合：空文字(表示)/ 0の場合："none"(非表示))
        if ( isDsp == 1 ) { DspState = ""; } else { DspState = "none"; }

        // DOM内部構造(style.display)を直接変更する
        // ※kintoneバージョンアップ後に動作しなくなる可能性あり
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルの場合
            elLbl.style.display = DspState;									// テーブルヘッダ<th>のlabel
            for ( var row = 0; row < elVal.length; row++ ) {
                elVal[row].parentNode.parentNode.style.display = DspState;	// テーブルデータ<td>のValue(行数分)
            }
        } else {
            // テーブルではない場合
            elVal[0].parentNode.style.display = DspState;					// LabelとValueごとdisplayを書き換える
        }

    }


    /******************************************************/
    // ラジオボタンの選択されたボタン名を取得
    //   FldId		アプリの項目のフィールドID
    //   row		テーブルの場合指定(行番号)
    //   関数値		選択されたボタン名
    /******************************************************/
    function TcGetRadioChecked(FldId, row) {

        // ラジオのボタングループ要素取得
        if ( row == undefined ) { row = 0; }
        var elRadio = getFieldValueEls_(FldId, row).childNodes[0];

        /*
        ** ラジオのすべてのボタンのうち、チェックされたボタン名を返す
        **  - ラジオボタンでは必ず1つなので見つかった時点でreturn
        */
        for ( var cnt = 0; cnt < elRadio.childNodes.length; cnt++ ) {
            if (elRadio.childNodes[cnt].childNodes[0].checked == true) {
                // ヒットした子ノードの値を返却
                return (elRadio.childNodes[cnt].childNodes[0].value);
            }
        }
    }


    /******************************************************/
    // チェックボックスの選択されたチェック名を取得
    //   FldId		アプリの項目のフィールドID
    //   row		テーブルの場合指定(行番号)
    //   関数値		選択されたチェック名(配列)
    /******************************************************/
    function TcGetChkBoxChecked(FldId, row) {

        // チェックボックス要素取得
        if ( row == undefined ) { row = 0; }
        var elCheck = getFieldValueEls_(FldId, row);

        var box = new Array();

        /*
        ** チェックされたボックス名を配列に格納
        **  - すべてのボックス要素を判定し、チェックされたボックス名をすべて格納してreturn
        */
        for ( var cnt = 0; cnt < elCheck.childNodes[0].childNodes.length; cnt++ ) {
            if (elCheck.childNodes[0].childNodes[cnt].childNodes[0].checked == true) {
                // チェックされたボックス名を配列に格納
                box.push( elCheck.getElementsByTagName("label")[cnt].textContent );
            }
        }
        return box;    // 配列を返す
    }


    /******************************************************/
    // ドロップダウンの選択された内容を取得
    //   fieldId	取得したいドロップダウンのフィールドID
    //   row		行番号
    //   関数値		取得した内容
    /******************************************************/
    function TcGetDropDown(fieldId, row) {
        if ( row == undefined ) { row = 0; }
        // 選択された文字を返却
        var strDD = getTextContent_(getFieldValueEls_(fieldId,row).getElementsByTagName("div")[0]);
        if ( strDD.indexOf("-----") != -1 ) { strDD = ""; }

        return strDD;
    }


    /******************************************************/
    // 時刻フィールド値取得
    //   FldId		アプリの項目のフィールドID
    //   関数値		取得した時刻文字列
    //            ※ テキストの書式はユーザ設定による
    /******************************************************/
    function TcGetTimeText(FldId) {

        var sRet = "";

        /*
        ** 時刻値フィールドを取得して、valueを返却
        */
        var el = getFieldValueEl_(FldId).getElementsByClassName("input-time-cybozu")[0];
        if ( el != undefined ) {
            sRet = el.childNodes[0].value;
        }
        return sRet;

    }


    /******************************************************/
    // ユーザーフィールド値取得
    //   ※選択肢が指定されたフィールドの場合は非対応
    //   FldId		アプリの項目のフィールドID
    //   関数値		取得したユーザー名(複数ユーザーの場合、カンマ(,)区切り)
    /******************************************************/
    function TcGetUserText(FldId) {

        // TODO:配列で返却したいが、既存箇所でテキスト取得しているところを考慮して現状はsRetを返す(2014/01/17)
        var sRet = "";
//        var aryRet = new Array();

        /*
        ** 選択済みユーザリストを取得し、選択されたユーザの表示名を配列で返却
        */
        var el = getFieldValueEl_(FldId).getElementsByClassName("entity-list-inner-cybozu")[0];
        if ( el != undefined ) {
            // 選択ユーザ数分ループ
            for ( var cnt = 0; cnt < el.childNodes.length; cnt++ ) {
                // 選択されたユーザの表示名を取得し、配列に格納
                var userName = el.childNodes[cnt].getElementsByClassName("entity-user-cybozu")[0].innerHTML;
                if ( sRet == "" ) { sRet = userName; } else { sRet = sRet + "," + userName; }
//                aryRet.push(userName);
            }
        }

        return sRet;
    }


    function TcPostFldVal() {}

    /******************************************************/
    // テーブルの合計値を分類別に保持したオブジェクトを取得
    //   objSum		分類別合計値保持オブジェクト
    //   ClsId		分類ドロップダウンフィールドID
    //   TrgId		合計対象フィールドID
    //   関数値		objSum[ {分類名:合計値}, {...} ]
    /******************************************************/
    function TcClassifySumTbl(objSum, ClsId, TrgId) {

        /*
        ** objSum[ {分類名:合計値}, {...} ] の形で分類別の合計値を保持する
        */
        for ( var row = 0; row < getFieldValueLength(ClsId); row++ ) {

            // ドロップダウンの選択値を取得
            var strDD = TcGetDropDown(ClsId, row).replace(/\s+/g, "");

            // 初出の分類の場合、初期化(0クリア)
            if ( objSum[strDD] == undefined ) { objSum[strDD] = 0; }

            // [金額]のフィールド値を数値として取得
            var numVal = parseInt(getFieldValueFromTableInputValue(TrgId, row).split(",").join(""));

            // [金額]フィールドがNaNではない(数値)の場合のみ処理
            if( isNaN(numVal) == false ) {
                // 合計対象フィールド(TrgId)を分類毎(ClsId)に分けて足しこむ
                objSum[strDD] += numVal;	// 区切り記号カンマを外し数値にしたうえで足しこみ
            }



        }

        return objSum;

    }


    /******************************************************/
    // チェックされたテーブル行の操作ボタンを無効/有効化
    //   FldId      対象チェックボックスフィールドID
    //   関数値     なし
    /******************************************************/
    function TcTblSeqOperate(FldId) {

        for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

            // その行のチェックボックス要素取得
            var elVal = getFieldValueEls_(FldId,row);

            //「×」ボタン要素取得
            var elBtn = elVal.parentNode.parentNode.parentNode.getElementsByClassName("remove-row-image-gaia")[0];

            // 1個目のチェックボックス選択状態取得
            var chkState = elVal.childNodes[0].childNodes[0].childNodes[0].checked;
            if ( chkState == true ) {
                elBtn.setAttribute("style", "display:none;");		// チェック有の場合：ボタン非表示
            } else if ( getFieldValueLength(FldId) != 1 )  {
                elBtn.removeAttribute("style");						// チェック無かつテーブル2行以上の場合：ボタン表示
            }
        }

    }


    /******************************************************/
    // チェックされた行の指定フィールドを別フィールドに転記
    //   TrgId    差し込み先フィールドID
    //   SrcId    差し込み元フィールドID
    //   ChkId    判定チェックボックスID
    //   iniVal   初期値
    //   関数値   なし
    /******************************************************/
    function TcPostTblChkData(TrgId, SrcId, ChkId, iniVal) {

        var chkCnt = 0;								// チェック数カウンタ
        var elTrg = getSingleLineInput(TrgId);		// 差し込み先フィールド

        for ( var row = 0; row < getFieldValueLength(SrcId); row++ ) {

            // テーブル行要素
            var elSrc = getFieldValueFromTableInput(SrcId, row);						// 差し込み元フィールド
            var elChk = getFieldValueEls_(ChkId, row);									// 判定チェックボックス
            var ChkState = elChk.childNodes[0].childNodes[0].childNodes[0].checked;		// チェック状態

            /*
            ** ★チェックされた行データのフィールド値を差し込み先フィールドに転記
            **  - データ転記＋チェックされた行カウンタをインクリメント
            **  - チェックされた行以外のチェックボックスを無効化
            */
            if ( ChkState == true ) {
                elTrg.value = elSrc.value;		// 差し込み先.value ← 差し込み元.value
                chkCnt++;						// チェック数カウント

                /*
                ** ★lcnt(チェックされた行)以外のチェックボックスを無効にする
                */
                for ( var cnt = 0; cnt < getFieldValueLength(SrcId); cnt++ ) {
                    if ( cnt != row ) {
                        // チェックされた行ではない場合
                        var elChk2 = getFieldValueEls_(ChkId, cnt);								// 判定チェックボックス
                        elChk2.childNodes[0].childNodes[0].childNodes[0].disabled = true;		// チェックを無効化
                    }
                }
            }
        }

        /*
        ** ★テーブル単位のチェック数別の処理
        */
        if ( chkCnt > 1 ) {
            // チェック数が2個以上の場合、警告メッセージ表示
            alert("採用区分は1テーブルにつき1チェックにしてください");
        }
        if ( chkCnt != 1 ) {
            // チェック数が1ではない(0個)の場合、すべてのチェックボックスを有効化
            for ( var cnt=0; cnt < getFieldValueLength(SrcId); cnt++ ) {
                elTrg.value = iniVal;												// 差し込み先フィールドを初期化
                var elChk2 = getFieldValueEls_(ChkId, cnt);							// 判定チェックボックス
                elChk2.childNodes[0].childNodes[0].childNodes[0].disabled = false;	// チェックを有効化
                elChk2.childNodes[0].childNodes[0].childNodes[0].checked = false;	// チェックをはずす
            }
        }

    }


    /******************************************************/
    // テーブル行毎の小計、テーブル全体の小計を計算してフィールド更新
    //   SelId    [分類]ドロップダウンフィールドID
    //   QtyId    [数量]数値フィールドID
    //   PrcId    [単価]数値フィールドID
    //   MnyId    [金額]数値フィールドID
    //   TtlId    [小計]数値フィールドID
    //   関数値   なし
    /******************************************************/
    function TcCalcTblSubTtl(SelId, QtyId, PrcId, MnyId, TtlId) {

        // 計算用変数
        var subTtl_row = 0;			// 行小計([金額]用)
        var subTtl_all = 0;			// 全小計([小計]用)

        // 要素取得
        var elTtl = getSingleLineInput(TtlId);		// [小計]要素

        for ( var row = 0; row < getFieldValueLength(SelId); row++ ) {

            // テーブル行単位要素取得&金額計算
            var elQty = getFieldValueFromTableInput(QtyId, row);	// [数量]要素
            var elPrc = getFieldValueFromTableInput(PrcId, row);	// [単価]要素
            var elMny = getFieldValueFromTableInput(MnyId, row);	// [金額]要素
            var calcResult = elQty.value * elPrc.value;				// 金額算出

            // [分類]ドロップダウンの選択値を取得
            var selStr = TcGetDropDown(SelId, row);

            /*
            ** ★[分類]によって設定する行情報を切り替える
            **  - "小計"である場合：[数量][単価]に空文字、[金額]に小計を設定
            **  - "小計"でない場合：[数量][単価]が空文字の場合0クリア、[金額]に金額値を設定し、小計を足し込む
            */
            if (  selStr.indexOf("小計") != -1 ) {
                // "小計"である場合
                elQty.value = "";								// [数量]に空文字代入
                elPrc.value = "";								// [単価]に空文字代入
                elMny.value = subTtl_row;						// [金額]に行小計を差し込む
                subTtl_row = 0;									// 行小計0クリア
            } else {
                // "小計"でない場合
                if ( elQty.value == "" ) { elQty.value = 0; }	// [数量]が空文字の場合0クリア
                if ( elPrc.value == "" ) { elPrc.value = 0; }	// [単価]が空文字の場合0クリア
                elMny.value = calcResult;						// [金額]に金額差し込む
                subTtl_row += calcResult;						// 行小計を足し込む
                subTtl_all += calcResult;						// 全小計を足し込む
            }

            /*
            ** ★フィールド無効化
            */
            // [金額]フィールド
            elMny.parentNode.className = "input-text-outer-cybozu disabled-cybozu";
            elMny.disabled = true;

        }

        // [小計]フィールド更新
        elTtl.value = subTtl_all;

    }


    /******************************************************/
    // 指定したフィールドの値を別のフィールドに差し込む
    //  - 通常/テーブルのフィールド両方に対応
    //   TrgId    差し込み先フィールドID
    //   SrcId    差し込み元フィールドID
    //   関数値   なし
    /******************************************************/
    function TcPostFieldData(TrgId, SrcId) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(TrgId);
        var elVal = document.getElementsByClassName("value-" + TrgId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        /*
        ** ★差し込み先フィールド値が空文字の場合、差し込み元フィールド値を差し込む
        */
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルの場合
            for ( var row = 0; row < elVal.length; row++ ) {
                var TrgVal = getFieldValueFromTableInputValue(TrgId, row);	// 差し込み先フィールド値取得
                if ( TrgVal == "" ) {
                    // 空文字の場合、SrcIdのフィールド値をTrgIdのフィールドに差し込む
                    getFieldValueFromTableInput(TrgId,row).value = getFieldValueFromTableInputValue(SrcId, row).replace(/\s+$/, "");
                }
            }
        } else {
            // テーブルではない場合
            var TrgVal = getSingleLineInputValue(TrgId);
            if ( TrgVal == "" ) {
                // 空文字の場合、SrcIdのフィールド値をTrgIdのフィールドに差し込む
                getSingleLineInput(TrgId).value = getSingleLineInputValue(SrcId).replace(/\s+$/, "");
            }
        }

    }


    /******************************************************/
    // ルックアップ取得内容をドロップダウンの内容で絞り込む
    //   DDId	ドロップダウンフィールドID(各分類)
    //   LUId	ルックアップフィールドID(材料費・労務費・機械費・その他)
    //   row	行番号
    /******************************************************/
    function TcPostDDtoLU(DDId, LUId, row) {

        // ドロップダウンとルックアップの値を取得
        var str_dd = TcGetDropDown( DDId, row ).replace(/\s+$/, "");

        // 取得したドロップダウンの選択値をルックアップフィールドに転記
        getFieldValueFromTableInput( LUId, row ).value = str_dd.replace(/\-/g, "");

        // ルックアップフィールドの無効化(disabled)
        getFieldValueEls_( LUId, row ).childNodes[0].childNodes[0].className = "input-text-outer-cybozu disabled-cybozu";
        getFieldValueEls_( LUId, row ).childNodes[0].childNodes[0].childNodes[0].disabled= true;
    }


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★モジュール
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
    /**********************************************************/
    // 別アプリへのデータ転記
    //   oSubwin    サブwindowオブジェクト
    //   oQuery     転記する文字列を下記形式で指定
    //                oQuery = [ { src:value1, target:fieldid1},
    //                           { src:value2, target:fieldid2},
    //                         ];
    //              value : 転記元となる値
    //              target: 転記先のフィールドID
    //   関数値     True:成功 / False:失敗
    /**********************************************************/
    function TcSwCopy(oSubwin, oQuery) {
        var bRet = true;
        for ( var i = 0; i < oQuery.length; i++ ) {
            var item = oQuery[i];
            // 転記先がラジオボタンの場合
            if ( item["type"] == "radio" ) {
                var elTarget = oSubwin.getFieldValueEl_(item["target"]);
                if ( elTarget ) {
                    var ipTags = elTarget.getElementsByTagName("input");
                    for ( var rCnt = 0; rCnt < ipTags.length; rCnt++ ) {
                        if ( ipTags[rCnt].value == item["src"] ) {
                            ipTags[rCnt].checked = true;
                        } else {
                            ipTags[rCnt].checked = false;
                        }
                    }
                } else {
                    bRet = false;
                }
            // 転記先がチェックボックスの場合
            } else if ( item["type"] == "check" ) {
                var elTarget = oSubwin.getFieldValueEl_(item["target"]);
                if ( elTarget ) {
                    var ipTags = elTarget.getElementsByTagName("input");
                    for ( var rCnt = 0; rCnt < ipTags.length; rCnt++ ) {
                        if ( getTextContent_(ipTags[rCnt].parentNode.childNodes[1]) == item["src"] ) {
                            ipTags[rCnt].checked = true;
                        } else {
                            ipTags[rCnt].checked = false;
                        }
                    }
                } else {
                    bRet = false;
                }
            // 転記先がマルチテキストの場合
            } else if ( item["type"] == "multi" ) {
                var elTarget = oSubwin.getMultiLineInput(item["target"]);
                if ( elTarget ) elTarget.value = item["src"];
                else bRet = false;
            } else {
                var elTarget = oSubwin.getSingleLineInput(item["target"]);
                if ( elTarget ) elTarget.value = item["src"];
                else bRet = false;
            }
        }
        return(bRet);
    }


    /******************************************************/
    // JSONオブジェクトのValue値を返す
    //  - オブジェクトの値がある場合はその値を、オブジェクト自体がないor値がない場合は空白を返す
    //   関数値   オブジェクトの値 or 空白
    /******************************************************/
    function TcRetObjVal(objFld) {
        /*
        ** ★引数オブジェクト値をreturn
        */
        if ( objFld != undefined && objFld['value'] != null ) {
            // 引数オブジェクトに値が入っている
            return objFld['value'];
        } else {
            // 引数オブジェクト自体が存在しない または 値が入っていない
            return "";
        }
    }


    /******************************************************/
    // 外部PHPの呼び出し実行
    //   FileName	実行ファイル名
    //   Query		PHPパラメタ
    //   isReload	リロードするか(bool)(省略可)
    //   Domain 	使用するドメイン指定(省略可)
    //   関数値		なし
    /******************************************************/
    function TcCallPHP(FileName, Query, isReload ,Domain) {

        // 環境変数取得
        var prm = TcGetEnvPrm();

        // 送信先URLを設定
		if(Domain == undefined ){
        	var url = "https://www.timeconcier.jp/forkintone/" + prm['PHPドメイン'] + "/" + FileName;
		}else{
        	var url = "https://www.timeconcier.jp/forkintone/" + Domain + "/" + FileName;
		}

        TcOpenHtml(url, Query, "_newWin", "POST");

        // isReloadがfalseでない場合はリロードする
        if ( isReload == undefined || isReload == true ) {
            setTimeout( function() { location.reload(); }, 1500);	// 1.5秒待ってリロード
        }

    }


    /******************************************************/
    // ボタン要素を作成して返す
    //   BtnId		ボタン識別子
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    //   row		配置先がテーブルの場合、行番号(省略可)
    /******************************************************/
    function TcMakeButton(BtnId, BtnLbl, FncName, BtnTag, SetStyle, row) {

        // ボタンパーツ作成
        var emButton = document.createElement('input');
        emButton.type = 'button';
        emButton.value = BtnLbl;
        emButton.id  = 'btn_' + BtnId;						// クリックイベントからはbtn.idで呼び出し元を判定する
        if ( row != undefined ) {
            emButton.id += '_' + row;						// 行番号指定時
        }
        emButton.tag = BtnTag;								// ボタン識別のためのタグ名

        // イベントが指定されている場合は、クリックイベントを追加する
        if ( FncName != undefined ) {
            addEventListener_(emButton, 'click', FncName);	// クリックイベント追加
        } else {
            emButton.value += "(EV未設定)";
            emButton.disabled = true;						// ボタン無効化
        }

        // スタイルが指定されている場合は、ボタンスタイルを設定する
        if ( SetStyle != undefined ) { emButton.setAttribute("style", SetStyle); }

        // ボタン要素を返す
        return emButton;
    }


    /******************************************************/
    // ドロップダウン選択値をルックアップ入力欄に転記
    //   TrgId		ルックアップフィールドID
    //   SrcId		ドロップダウンフィールドID
    //   row		行番号
    /******************************************************/
    function TcFilLookUpFld(TrgId, SrcId, row) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(TrgId);
        var elVal = document.getElementsByClassName("value-" + TrgId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        /*
        ** ★ドロップダウンの選択値をルックアップフィールドに差し込む
        */
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルである場合
            for ( var row = 0; row < elVal.length; row++ ) {

                // 転記
                var str_dd = TcGetDropDown(SrcId, row).replace(/\s+$/, "");					// ドロップダウンの選択値取得
                getFieldValueFromTableInput(TrgId, row).value = str_dd.replace(/\-/g, "");	// 取得した選択値をルックアップに転記

                // ルックアップフィールドの無効化(disabled)
                var elTrg = getFieldValueEls_(TrgId, row).childNodes[0].childNodes[0];	// ルックアップ要素取得
                elTrg.className = "input-text-outer-cybozu disabled-cybozu";
                elTrg.childNodes[0].disabled= true;
            }
        } else {
            // テーブルでない場合
            console.log("通常フィールドは未対応");
        }
    }


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★kintone標準Utility
//   - kintone標準(サイボウズ提供)のユーティリティ関数
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// URLの?や#より前の文字列を取得
function getUrlPath_() {
        var url = document.URL;
        if (url.indexOf('?') > 0) {
                return url.substr(0, url.indexOf('?'));
        }
        if (url.indexOf('#') > 0) {
                return url.substr(0, url.indexOf('#'));
        }
        return url;
}
  
// レコード追加画面ならtrue
function isAddPage() {
    return getUrlPath_().match(/edit$/);
}

// レコード詳細画面ならtrue
function isShowPage() {
    return getUrlPath_().match(/show$/) && !location.hash.match(/mode=edit/);
}

// レコード編集画面ならtrue
function isEditPage() {
    return getUrlPath_().match(/show$/) && location.hash.match(/mode=edit/);
}

// レコード一覧画面ならtrue
function isViewPage() {
    return getUrlPath_().match(/\/[0-9]+\/$/);
}

// ポータル画面ならtrue
function isPortalPage() {
    var url = document.URL;
    return url.match(/portal$/);
}


//----------------------------------------

// 要素のテキストを取得
function getTextContent_(el) {
    return el.childNodes[0].textContent !== undefined ? el.childNodes[0].textContent : el.childNodes[0].innerText;
}
 
// フィールド名の要素を取得
function getFieldLabelEl_(fieldId) {
    return document.getElementsByClassName('label-' + fieldId)[0];
}
 
// フィールド値の要素を取得
function getFieldValueEl_(fieldId) {
    return document.getElementsByClassName('value-' + fieldId)[0];
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueLength(fieldId) {
    return document.getElementsByClassName('value-' + fieldId).length;
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueEls_(fieldId, i) {
    return document.getElementsByClassName('value-' + fieldId)[i];
}
 
// フィールド名を取得（すべての画面で利用可）
function getFieldName(fieldId) {
    return getTextContent_(getFieldLabelEl_(fieldId));
}
 
// フィールド値を取得（一覧画面と詳細画面で利用可、一部のフィールドタイプには未対応）
function getFieldValue(fieldId) {
    return getTextContent_(getFieldValueEl_(fieldId));
}

// フィールド値を取得（テーブル内のフィールド）
function getFieldValueFromTable(fieldId, i) {
    return getTextContent_(getFieldValueEls_(fieldId, i));
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（テーブル内のフィールド）
function getFieldValueFromTableInput(fieldId,i) {
    return getFieldValueEls_(fieldId,i).querySelector('input');
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（テーブル内のフィールド）
function getFieldValueFromTableInputValue(fieldId,i) {
    return getFieldValueFromTableInput(fieldId,i).value;
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（追加画面と編集画面で利用可）
function getSingleLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('input');
}
 
// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（追加画面と編集画面で利用可）
function getSingleLineInputValue(fieldId) {
    return getSingleLineInput(fieldId).value;
}
 
// 文字列(複数行)フィールドのTEXTAREA要素を取得（追加画面と編集画面で利用可）
function getMultiLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('textarea');
}
 
// 文字列(複数行)フィールドのTEXTAREA要素の入力値を取得（追加画面と編集画面で利用可）
function getMultiLineInputValue(fieldId) {
    return getMultiLineInput(fieldId).value;
}

//----------------------------------------

// イベントリスナーを登録
function addEventListener_(el, type, listener) {
    if (el.addEventListener !== undefined) {
        el.addEventListener(type, listener);
    } else if (el.attachEvent !== undefined) {
        el.attachEvent('on' + type, listener);
    }
}

// イベントリスナーを削除
function delEventListener_(el, type, listener) {
    if (el.removeEventListener !== undefined) {
        el.removeEventListener(type, listener);
    } else if (el.detachEvent !== undefined) {
        el.detachEvent('on' + type, listener);
    }
}


/******************************************************/
// 外部ページへのデータ渡し
//   url      渡し先URL
//   query    送信するデータ(key:xxx val:xxx)
//   name     送信ターゲット名
//   meth     POST or GET
//   関数値   なし
/******************************************************/
function TcOpenHtml( url, query, name, meth ) {
        var win = window.open( "", name );
        
// formを生成
        var fm = document.createElement("form");
        fm.target = name;
        fm.action = url;
        fm.method = meth;
        for( var i = 0; i < query.length; i++ ) {
            var kv = query[i];
            var key = kv["key"];
            var val = kv["val"];
            if ( key ) {
                var input = document.createElement("input");
                val = ( val != null ? val : "" );
                input.type = "hidden";
                input.name = key;
                input.value = val;
                fm.appendChild(input);
                }
        }
        // 一時的にbodyへformを追加。サブミット後、formを削除する
        var body = document.getElementsByTagName("body")[0];
        body.appendChild(fm);
        fm.submit();
        body.removeChild(fm);
}


