<?php
/*****************************************************************************/
/* 案件契約書PHP                                              (Version 1.00) */
/*   ファイル名 : keiyakusyo_anken.php                                       */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_SYOKI_SHN_GYO" , 7 );
	define( "TC_RUNNING_SHN_GYO" , 7 );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

// 実行
	$clsSrs->main();



	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $err;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// 案件管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/keiyakusyo_anken.xls");

			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "注文請書(".$json->records[0]->顧客名_出力_->value.")_".date('YmdHis').sprintf("%.0f",($msec * 1000)).".xls";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('E4'		,	$pDat->顧客名_出力_->value	)
			            ->setCellValue('R4'		,	$pDat->貴社名フリガナ_出力_->value	)
			            ->setCellValue('E6'		,	$pDat->ＴＥＬ_出力_->value	)
			            ->setCellValue('R6'		,	$pDat->ＦＡＸ_出力_->value	)
			            ->setCellValue('E5'		,	$pDat->郵便番号_出力_->value	)
			            ->setCellValue('J5'		,	$pDat->住所_出力_->value	)
			            ->setCellValue('E7'		,	$pDat->代表者_出力_->value	)
			            ->setCellValue('R7'		,	$pDat->代表者フリガナ_出力_->value	)
			            ->setCellValue('E8'		,	$pDat->担当者_出力_->value	)
			            ->setCellValue('R8'		,	$pDat->担当者携帯_出力_->value	)
//						->setCellValue('C43'	,	$pDat->当社営業担当者１->value[0]->name	)
			            ->setCellValue('H21'	,	$pDat->支払条件_初期_->value	)

						->setCellValue('B12'	,	$pDat->商品・サービス名１->value	)
						->setCellValue('O12'	,	$pDat->数量１->value	)
			            ->setCellValue('R12'	,	$pDat->単位１->value	)
			            ->setCellValue('V12'	,	$pDat->単価１->value	)
			            ->setCellValue('AD12'	,	$pDat->摘要１->value	)

						->setCellValue('H33'	,	$pDat->サービス開始時期->value	)
						->setCellValue('Z33'	,	$pDat->支払開始日->value	)
						->setCellValue('H34'	,	$pDat->支払条件_ランニング_->value	)
			            ->setCellValue('H36'	,	$pDat->その他条件->value	)
			            ->setCellValue('H37'	,	$pDat->別途申込書->value	)
			            ->setCellValue('H38'	,	$pDat->別途契約書->value	)

						->setCellValue('AE1'	,	"No.".sprintf('%06d', $pDat->レコード番号->value)	);

			// 納期契約選択の選択によって表示を変える
			if($pDat->納期契約選択->value == "リース" ){
			// リースが選択されている場合
			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
						->setCellValue('A20'	,	"リース契約時期"	)
						->setCellValue('H20'	,	$pDat->リース契約時期->value	)
						->setCellValue('S20'	,	"支払開始日"	)
			            ->setCellValue('Z20'	,	$pDat->支払開始日_リース_->value	);
			}else{
			// 現金が選択されている場合
			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
						->setCellValue('A20'	,	"納品時期"	)
						->setCellValue('H20'	,	$pDat->納品時期->value	)
						->setCellValue('S20'	,	"支払予定日"	)
			            ->setCellValue('Z20'	,	$pDat->支払予定日->value	);
			}

			// 初期、ランニング 商品
			$idx_s = 0;		// 初期
			$idx_r = 0;		// ランニング
			foreach( $pDat->テーブル_お支払条件->value as $key => $val ) {
				$shn = $val->value;

				// 初期
				if( $shn->商品区分->value == "初期導入費" ) {
					if( $idx_s < TC_SYOKI_SHN_GYO ) {
						$y = 12 + $idx_s;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('B'.$y	,	$shn->商品名->value	)
									->setCellValue('O'.$y	,	$shn->数量->value	)
						            ->setCellValue('R'.$y	,	$shn->単位->value	)
						            ->setCellValue('V'.$y	,	$shn->単価->value	)
						            ->setCellValue('AD'.$y	,	$shn->商品名備考->value	) ;
					}
					$idx_s++;
				}

				// 保守・ランニング
				if( $shn->商品区分->value == "ランニング費" ) {
					if( $idx_r < TC_RUNNING_SHN_GYO ) {
						$y = 25 + $idx_r;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('B'.$y	,	$shn->商品名->value	)
									->setCellValue('O'.$y	,	$shn->数量->value	)
						            ->setCellValue('R'.$y	,	$shn->単位->value	)
						            ->setCellValue('V'.$y	,	$shn->単価->value	)
						            ->setCellValue('AD'.$y	,	$shn->商品名備考->value	) ;
					}
					$idx_r++;
				}
			}
			
			// 明細件数チェック
			if( $idx_s > TC_SYOKI_SHN_GYO ) {
				$ret = $ret."　　初期導入の商品が ".TC_SYOKI_SHN_GYO."件を超えています。<br>";
			}
			if( $idx_r > TC_RUNNING_SHN_GYO ) {
				$ret = $ret."　　ランニングの商品が ".TC_RUNNING_SHN_GYO."件を超えています。<br>";
			}
			if( $ret != "" ) {
				$ret = "注）印刷されない商品があります。<br>".$ret;
			}

			return ($ret);
		}

	}

?>
