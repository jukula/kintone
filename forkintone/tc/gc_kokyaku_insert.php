<?php
/*****************************************************************************/
/* 顧客作成PHP                                                (Version 1.00) */
/*   ファイル名 : gc_kokyaku_insert.php                                      */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）

		var $err;
		var $KKKKID             = null;     // TC顧客管理作成ID

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 会社名の名前保持用

			// 案件管理(地盤調査)アプリのデータを取得
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			// リターン時の表示用に会社名を保持
			$sAnkenname = $jsonANKK->records[0]->会社名->value;

			// TC顧客アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "( レコード番号 = \"".$jsonANKK->records[0]->TC顧客レコード番号->value."\" )"; // クエリパラメータ
			$jsonKNKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 登録されているかチェック
			if( $k->intDataCount > 0 ) {
				// 登録済ならFALSEを返す
				$rmgkKei = "FALSE";
			}else{
				// なければインサート
				$this->insKKKK( $jsonANKK );
				$this->updANKK( $jsonANKK );
				$rmgkKei = $sAnkenname."をTC顧客管理に新規登録しました。";
			}

			// 値を返す
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  TC顧客管理へデータを追加する                                         */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insKKKK( &$sgnp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			$i=0;

			// 書込み準備
			$recObj = new stdClass;
			// 通常項目
			$recObj->顧客名 	  			= $this->tgfEnc( $tcRec , $i , "会社名" );
			$recObj->フリガナ			  	= $this->tgfEnc( $tcRec , $i , "会社名フリガナ" );
			$recObj->郵便番号			  	= $this->tgfEnc( $tcRec , $i , "郵便番号" );
			$recObj->都道府県名			  	= $this->tgfEnc( $tcRec , $i , "都道府県" );
			$recObj->住所１ 			  	= $this->tgfEnc( $tcRec , $i , "所在地" );
			$recObj->電話番号 		  		= $this->tgfEnc( $tcRec , $i , "電話番号" );
			$recObj->FAX		 		  	= $this->tgfEnc( $tcRec , $i , "ＦＡＸ" );
			$recObj->代表者名 			  	= $this->tgfEnc( $tcRec , $i , "代表者氏名" );
			$recObj->代表者名フリガナ 		= $this->tgfEnc( $tcRec , $i , "代表者フリガナ" );
			$recObj->担当者 			  	= $this->tgfEnc( $tcRec , $i , "担当者" );
			$recObj->担当者携帯 			= $this->tgfEnc( $tcRec , $i , "担当者携帯" );
			$recObj->締め日 			  	= $this->tgfEnc( $tcRec , $i , "締め日" );
			$recObj->回収サイト 			= $this->tgfEnc( $tcRec , $i , "回収サイト" );
			$recObj->回収日 			  	= $this->tgfEnc( $tcRec , $i , "回収日" );
			$recObj->回収条件 			  	= $this->tgfEnc( $tcRec , $i , "回収条件" );
			$recObj->回収方法 			  	= $this->tgfEnc( $tcRec , $i , "回収方法" );
			$recObj->客区分 			  	= $this->valEnc( "既存客" );

			// --------------------
			// TC顧客管理へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$insData 			= new stdClass;
			$insData->app 		= TC_APPID_TCKKKK;
			$insData->records[] = $recObj;

			$k->aryJson = $insData;						// 追加対象のレコード番号
			$json = $k->runCURLEXEC( TC_MODE_INS );
			$this->KKKKID = $k->strInsRecNo[0];
		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json ) {

			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );
			$i=0;
			$tbcount = 0;

			// -----------------------------
			// 案件管理(地盤調査)を更新する。
			// -----------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;
			$recObj->record->TC顧客レコード番号 = $this->valEnc( $this->KKKKID );

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_ANKJ;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );
		}
	}

?>
