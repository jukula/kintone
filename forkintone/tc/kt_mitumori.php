<?php
/*****************************************************************************/
/* 	 案件管理(地盤調査)見積印刷PHP                            (Version 1.00) */
/*   ファイル名 : gc_mitumori.php                                            */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_TB1_COUNT" , 15 );
	define( "TC_TB2_COUNT" , 7 );

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
    // 対象のレコード番号を受け取る。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

// 実行
	$clsSrs->main();



	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
		var $err;
		var $common;

	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// 案件管理(地盤調査)アプリ
			$k = new TcKintone();	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= 2772;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/test.xls");

			// セルへ設定
			if( $k->intDataCount > 0 ) {
//				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "見積書()_".date('YmdHis').sprintf("%.0f",($msec * 1000)).".xls";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , &$pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('H1'		,	"No.".sprintf('%06d', $pDat->レコード番号->value)	)
			            ->setCellValue('G2'		,	$this->common->setDayChange($pDat->見積日->value , 1 )	)
			            ->setCellValue('A4'		,	$pDat->会社名->value	)
			            ->setCellValue('A15'	,	$pDat->案件名->value	)
			            ->setCellValue('H12'	,	$pDat->営業担当者->value	)
			            ->setCellValue('B5'		,	"ご担当：".$pDat->担当者->value	)
			            ->setCellValue('A40'	,	$pDat->備考_見積_->value	);

			// お見積内容
			$idx_s = 0;		    // 内容
			foreach( $pDat->初期導入費用テーブル->value as $key => $val ) {
				$shn = $val->value;

					// 内容
					if( $idx_s < TC_TB1_COUNT ) {
						$y = 16 + $idx_s;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('A'.$y	,	$shn->NO_初期_->value	)
									->setCellValue('B'.$y 	,	$shn->商品・サービス名_初期_->value.$shn->商品詳細_初期_->value	)
						            ->setCellValue('D'.$y	,	$shn->数量_初期_->value	)
						            ->setCellValue('E'.$y	,	$shn->単位_初期_->value	)
						            ->setCellValue('F'.$y	,	$shn->単価_初期_->value	)
						            ->setCellValue('H'.$y	,	$shn->見積摘要_初期_->value	);
					}
					$idx_s++; 		//レコードのカウントアップ
			}

			// お見積条件
			$idx_r = 0;		// 条件
			foreach( $pDat->保守サポート費用テーブル->value as $key => $val ) {
				$shn = $val->value;

					// 条件
					if( $idx_r < TC_TB2_COUNT ) {
						$y = 32 + $idx_r;
						$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
									->setCellValue('A'.$y	,	$shn->NO_保守_->value	)
									->setCellValue('B'.$y 	,	$shn->商品・サービス名_保守_->value.$shn->商品詳細_保守_->value	)
						            ->setCellValue('D'.$y	,	$shn->数量_保守_->value	)
						            ->setCellValue('E'.$y	,	$shn->単位_保守_->value	)
						            ->setCellValue('F'.$y	,	$shn->単価_保守_->value	)
						            ->setCellValue('H'.$y	,	$shn->見積摘要_保守_->value	);
					}
					$idx_r++;
			}
			
			// 明細件数チェック
			if( $idx_s > TC_TB1_COUNT ) {
				$ret = $ret."　　見積内容が ".TC_TB1_COUNT."件を超えています。<br>";
			}
			if( $idx_r > TC_TB2_COUNT ) {
				$ret = $ret."　　お見積条件が ".TC_TB2_COUNT."件を超えています。<br>";
			}
			if( $ret != "" ) {
				$ret = "注）印刷されない商品があります。<br>".$ret;
			}

			return ($ret);
		}

	}

?>
