<?php
/*****************************************************************************/
/* 業務日誌PHP                                                (Version 1.00) */
/*   ファイル名 : xls_travel_requisition.php                                 */
/*   更新履歴   2015/05/21  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg     = "";
		    $rowdata = array();

			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load("templates/gensen.xlsx");

				$aryfilelist = array();

				$url = "image/maru.gif";
				$aryfilelist[] = $url;

				$sheet = $objPHPExcel->getActiveSheet(0);

				foreach($aryfilelist as $path){
					$this->pastePic("B9",$path,$sheet);
				}


			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "出張申請(".$this->setUserName($jsonTRRE->records[0]->レコード番号->value->code).")_".date('YmdHi').".xlsx";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell_s1( &$pPHPExcel , $pDatTRRE ) {
			$ret = "";
			$shiharai = "";
			$setdate = "";

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( 0 )
			            ->setCellValue('A1'		,	"旅費番号　".$pDatTRRE->レコード番号->value	)
			            ->setCellValue('A6'		,	"申請日 :".$this->common->setDayChange($pDatTRRE->作成日時->value , 1 )	)
			            ->setCellValue('A7'		,	"申請者 :".$this->setUserName($pDatTRRE->作成者->value->code)	)
			            ->setCellValue('B9'		,	$pDatTRRE->文字列__複数行_->value	);

			// テーブル行数分ループする
			foreach( $pDatTRRE->Table_5->value as $key => $val ){
				$shn = $val->value;
				$y = 13 + $idx_s;

				$pPHPExcel	->setActiveSheetIndex( 0 )
							->setCellValue('A'.$y	  ,	$this->common->setDayChange($shn->Date_0->value , 1 )  )
							->setCellValue('B'.$y	  ,	$shn->Single_line_text_3->value  )
							->setCellValue('D'.$y	  , $pDatTRRE->Time_1->value."～".$pDatTRRE->Time_2->value	)
							->setCellValue('D'.($y+1) ,	$shn->Time->value."～".$shn->Time_0->value	)
							->setCellValue('F'.$y 	  ,	$shn->交通手段->value	)
							->setCellValue('H'.$y	  , $shn->Drop_down_0->value	);

				$idx_s++; 		//レコードのカウントアップ
				$idx_s++; 		//レコードのカウントアップ2行対応
			}

			return ($ret);
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell_s2( &$pPHPExcel , $pDatTRRE , $pList , $pSheetIndex ) {
			$ret = "";
			$shiharai = "";
			$setdate = "";

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
			            ->setCellValue('A1'		,	"旅費番号　".$pDatTRRE->レコード番号->value	)
			            ->setCellValue('D6'		,	$this->common->setDayChange($pDatTRRE->作成日時->value , 1 )	)
			            ->setCellValue('D8'		,	$pDatTRRE->ドロップダウン->value	)
			            ->setCellValue('P8'		,	$this->setUserName($pDatTRRE->作成者->value->code) )
			            ->setCellValue('D9'		,	$pDatTRRE->Single_line_text_11->value	)
			            ->setCellValue('D10'	,	$this->common->setDayChange($pDatTRRE->Date_1->value , 1)	)
						->setCellValue('P10'	,   $pDatTRRE->Time_3->value."～".$pDatTRRE->Time_4->value	)
			            ->setCellValue('D11'	,	$pDatTRRE->Multi_line_text_0->value	)
			            ->setCellValue('A14'	,	$pList	);
			return ($ret);
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell_s3( &$pPHPExcel , $pDatTRRE ) {
			$ret = "";
			$shiharai = "";
			$setdate = "";
			$pSheetIndex = 1;
			$arysetday = array();
			$weekjp_array = array('日', '月', '火', '水', '木', '金', '土');

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
			            ->setCellValue('A3'		,	"旅費番号　".$pDatTRRE->レコード番号->value	)
			            ->setCellValue('F6'		,	$this->common->setDayChange($pDatTRRE->Date_1->value , 1 )	)
			            ->setCellValue('F7'		,	$pDatTRRE->ドロップダウン->value	)
			            ->setCellValue('F8'		,	$this->setUserName($pDatTRRE->作成者->value->code)	)
			            ->setCellValue('F9'		,	$pDatTRRE->Single_line_text_11->value	);

			// 費用明細テーブル行数分ループする
			foreach( $pDatTRRE->費用明細->value as $key => $val ){
				$shn = $val->value;
				$y = 14 + $idx_s;
				if($shn->日付->value != "" ){
					$arydate = array();
					$arydate = explode("-",$shn->日付->value); // データを分解する
					$arysetday[$y] = $shn->日付->value;

					//タイムスタンプを取得
					$ptimestamp = mktime(0, 0, 0, $arydate[1], $arydate[2], $arydate[0]);
					//曜日番号を取得
					$weekno = date('w', $ptimestamp);
					//日本語の曜日を出力
					$weekjp = $weekjp_array[$weekno];

					$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
								->setCellValue('A'.$y	  ,	sprintf('%01d', $arydate[0])  )
								->setCellValue('B'.$y	  ,	sprintf('%01d', $arydate[1])  )
								->setCellValue('C'.$y	  , sprintf('%01d', $arydate[2])  )
								->setCellValue('D'.$y     ,	$weekjp	)
								->setCellValue('E'.$y	  , $shn->Single_line_text_2->value	)
								->setCellValue('F'.$y	  , $shn->Single_line_text_5->value	)
								->setCellValue('G'.$y	  , ""	)
								->setCellValue('H'.$y	  , $shn->ドロップダウン_0->value	)
								->setCellValue('I'.$y	  , $shn->数値->value	)
								->setCellValue('J'.$y	  , ""	)
								->setCellValue('K'.$y	  , ""	)
								->setCellValue('L'.$y	  , ""	)
								->setCellValue('M'.$y	  , $shn->Single_line_text_7->value	);

					$idx_s++; 		//レコードのカウントアップ
				}
			}

			// 日付の重複を削除
			$arysetday_chk = array_unique($arysetday);

			// 配列キーを取得
			$arykey = array_keys($arysetday_chk);

			// Tableテーブル行数分ループする
			foreach( $pDatTRRE->Table->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_1->value != "" ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_1->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('G'.$arykey[$k]	  , $shn->Single_line_text_0->value	)
										->setCellValue('J'.$arykey[$k]	  , $shn->数値_6->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_9->value	);
						}
					}
				}
			}

			// Table_0テーブル行数分ループする
			foreach( $pDatTRRE->Table_0->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_3->value != "" ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_3->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_14->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_13->value	);
						}
					}
				}
			}

			// Table_1テーブル行数分ループする
			foreach( $pDatTRRE->Table_1->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_2->value != "" ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_2->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_9->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_10->value	);
						}
					}
				}
			}
/*
			// Table_0テーブル行数分ループする
			foreach( $pDatTRRE->Table_0->value as $key => $val ){
				$shn = $val->value;
				$y = 14 + $idx_s;
				if($shn->日付_3->value != "" ){
					$arydate = array();
					$arydate = explode("-",$shn->日付_3->value); // データを分解する

					//タイムスタンプを取得
					$ptimestamp = mktime(0, 0, 0, $arydate[1], $arydate[2], $arydate[0]);
					//曜日番号を取得
					$weekno = date('w', $ptimestamp);
					//日本語の曜日を出力
					$weekjp = $weekjp_array[$weekno];

					$pPHPExcel	->setActiveSheetIndex( 2 )
								->setCellValue('A'.$y	  ,	sprintf('%01d', $arydate[0])  )
								->setCellValue('B'.$y	  ,	sprintf('%01d', $arydate[1])  )
								->setCellValue('C'.$y	  , sprintf('%01d', $arydate[2])  )
								->setCellValue('D'.$y     ,	$weekjp	)
								->setCellValue('E'.$y	  , ""	)
								->setCellValue('F'.$y	  , ""	)
								->setCellValue('G'.$y	  , ""	)
								->setCellValue('H'.$y	  , ""	)
								->setCellValue('I'.$y	  , ""	)
								->setCellValue('J'.$y	  , ""	)
								->setCellValue('K'.$y	  , $shn->数値_14->value	)
								->setCellValue('L'.$y	  , ""	)
								->setCellValue('M'.$y	  , $shn->Single_line_text_13->value	);

					$idx_s++; 		//レコードのカウントアップ
				}
			}

			// Table_1テーブル行数分ループする
			foreach( $pDatTRRE->Table_1->value as $key => $val ){
				$shn = $val->value;
				$y = 14 + $idx_s;
				if($shn->日付_2->value != "" ){
					$arydate = array();
					$arydate = explode("-",$shn->日付_2->value); // データを分解する

					//タイムスタンプを取得
					$ptimestamp = mktime(0, 0, 0, $arydate[1], $arydate[2], $arydate[0]);
					//曜日番号を取得
					$weekno = date('w', $ptimestamp);
					//日本語の曜日を出力
					$weekjp = $weekjp_array[$weekno];

					$pPHPExcel	->setActiveSheetIndex( 2 )
								->setCellValue('A'.$y	  ,	sprintf('%01d', $arydate[0])  )
								->setCellValue('B'.$y	  ,	sprintf('%01d', $arydate[1])  )
								->setCellValue('C'.$y	  , sprintf('%01d', $arydate[2])  )
								->setCellValue('D'.$y     ,	$weekjp	)
								->setCellValue('E'.$y	  , ""	)
								->setCellValue('F'.$y	  , ""	)
								->setCellValue('G'.$y	  , ""	)
								->setCellValue('H'.$y	  , ""	)
								->setCellValue('I'.$y	  , ""	)
								->setCellValue('J'.$y	  , ""	)
								->setCellValue('K'.$y	  , ""	)
								->setCellValue('L'.$y	  , $shn->数値_9->value	)
								->setCellValue('M'.$y	  , $shn->Single_line_text_10->value	);

					$idx_s++; 		//レコードのカウントアップ
				}
			}
*/
			return ($ret);
		}
		/*************************************************************************/
	    /* データをセルへ設定する。(USD)                                         */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell_s4( &$pPHPExcel , $pDatTRRE ) {
			$ret = "";
			$shiharai = "";
			$setdate = "";
			$pSheetIndex = 2;
			$arysetday = array();
			$weekjp_array = array('日', '月', '火', '水', '木', '金', '土');

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
			            ->setCellValue('A3'		,	"旅費番号　".$pDatTRRE->レコード番号->value	)
			            ->setCellValue('F6'		,	$this->common->setDayChange($pDatTRRE->Date_1->value , 1 )	)
			            ->setCellValue('F7'		,	$pDatTRRE->ドロップダウン->value	)
			            ->setCellValue('F8'		,	$this->setUserName($pDatTRRE->作成者->value->code)	)
			            ->setCellValue('F9'		,	$pDatTRRE->Single_line_text_11->value	);

			// 費用明細テーブル行数分ループする
			foreach( $pDatTRRE->費用明細->value as $key => $val ){
				$shn = $val->value;
				$y = 14 + $idx_s;
				if($shn->日付->value != "" && $shn->数値_0->value != 0 ){
					$arydate = array();
					$arydate = explode("-",$shn->日付->value); // データを分解する
					$arysetday[$y] = $shn->日付->value;

					//タイムスタンプを取得
					$ptimestamp = mktime(0, 0, 0, $arydate[1], $arydate[2], $arydate[0]);
					//曜日番号を取得
					$weekno = date('w', $ptimestamp);
					//日本語の曜日を出力
					$weekjp = $weekjp_array[$weekno];

					$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
								->setCellValue('A'.$y	  ,	sprintf('%01d', $arydate[0])  )
								->setCellValue('B'.$y	  ,	sprintf('%01d', $arydate[1])  )
								->setCellValue('C'.$y	  , sprintf('%01d', $arydate[2])  )
								->setCellValue('D'.$y     ,	$weekjp	)
								->setCellValue('E'.$y	  , $shn->Single_line_text_2->value	)
								->setCellValue('F'.$y	  , $shn->Single_line_text_5->value	)
								->setCellValue('G'.$y	  , ""	)
								->setCellValue('H'.$y	  , $shn->ドロップダウン_0->value	)
								->setCellValue('I'.$y	  , $shn->数値_0->value	)
								->setCellValue('J'.$y	  , ""	)
								->setCellValue('K'.$y	  , ""	)
								->setCellValue('L'.$y	  , ""	)
								->setCellValue('M'.$y	  , $shn->Single_line_text_7->value	);

					$idx_s++; 		//レコードのカウントアップ
				}
			}

			// 日付の重複を削除
			$arysetday_chk = array_unique($arysetday);

			// 配列キーを取得
			$arykey = array_keys($arysetday_chk);

			// Tableテーブル行数分ループする
			foreach( $pDatTRRE->Table->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_1->value != "" && $shn->数値_7->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_1->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('G'.$arykey[$k]	  , $shn->Single_line_text_0->value	)
										->setCellValue('J'.$arykey[$k]	  , $shn->数値_7->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_9->value	);
						}
					}
				}
			}

			// Table_0テーブル行数分ループする
			foreach( $pDatTRRE->Table_0->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_3->value != "" && $shn->数値_15->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_3->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_15->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_13->value	);
						}
					}
				}
			}

			// Table_1テーブル行数分ループする
			foreach( $pDatTRRE->Table_1->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_2->value != "" && $shn->数値_10->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_2->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_10->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_10->value	);
						}
					}
				}
			}

			return ($ret);
		}
		/*************************************************************************/
	    /* データをセルへ設定する。(Euro)                                        */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell_s5( &$pPHPExcel , $pDatTRRE ) {
			$ret = "";
			$shiharai = "";
			$setdate = "";
			$pSheetIndex = 3;
			$arysetday = array();
			$weekjp_array = array('日', '月', '火', '水', '木', '金', '土');

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
			            ->setCellValue('A3'		,	"旅費番号　".$pDatTRRE->レコード番号->value	)
			            ->setCellValue('F6'		,	$this->common->setDayChange($pDatTRRE->Date_1->value , 1 )	)
			            ->setCellValue('F7'		,	$pDatTRRE->ドロップダウン->value	)
			            ->setCellValue('F8'		,	$this->setUserName($pDatTRRE->作成者->value->code)	)
			            ->setCellValue('F9'		,	$pDatTRRE->Single_line_text_11->value	);

			// 費用明細テーブル行数分ループする
			foreach( $pDatTRRE->費用明細->value as $key => $val ){
				$shn = $val->value;
				$y = 14 + $idx_s;
				if($shn->日付->value != "" && $shn->数値_2->value != 0 ){
					$arydate = array();
					$arydate = explode("-",$shn->日付->value); // データを分解する
					$arysetday[$y] = $shn->日付->value;

					//タイムスタンプを取得
					$ptimestamp = mktime(0, 0, 0, $arydate[1], $arydate[2], $arydate[0]);
					//曜日番号を取得
					$weekno = date('w', $ptimestamp);
					//日本語の曜日を出力
					$weekjp = $weekjp_array[$weekno];

					$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
								->setCellValue('A'.$y	  ,	sprintf('%01d', $arydate[0])  )
								->setCellValue('B'.$y	  ,	sprintf('%01d', $arydate[1])  )
								->setCellValue('C'.$y	  , sprintf('%01d', $arydate[2])  )
								->setCellValue('D'.$y     ,	$weekjp	)
								->setCellValue('E'.$y	  , $shn->Single_line_text_2->value	)
								->setCellValue('F'.$y	  , $shn->Single_line_text_5->value	)
								->setCellValue('G'.$y	  , ""	)
								->setCellValue('H'.$y	  , $shn->ドロップダウン_0->value	)
								->setCellValue('I'.$y	  , $shn->数値_2->value	)
								->setCellValue('J'.$y	  , ""	)
								->setCellValue('K'.$y	  , ""	)
								->setCellValue('L'.$y	  , ""	)
								->setCellValue('M'.$y	  , $shn->Single_line_text_7->value	);

					$idx_s++; 		//レコードのカウントアップ
				}
			}

			// 日付の重複を削除
			$arysetday_chk = array_unique($arysetday);

			// 配列キーを取得
			$arykey = array_keys($arysetday_chk);

			// Tableテーブル行数分ループする
			foreach( $pDatTRRE->Table->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_1->value != "" && $shn->数値_8->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_1->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('G'.$arykey[$k]	  , $shn->Single_line_text_0->value	)
										->setCellValue('J'.$arykey[$k]	  , $shn->数値_8->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_9->value	);
						}
					}
				}
			}

			// Table_0テーブル行数分ループする
			foreach( $pDatTRRE->Table_0->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_3->value != "" && $shn->数値_16->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_3->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_16->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_13->value	);
						}
					}
				}
			}

			// Table_1テーブル行数分ループする
			foreach( $pDatTRRE->Table_1->value as $key => $val ){
				$shn = $val->value;
				if($shn->日付_2->value != "" && $shn->数値_11->value != 0 ){
					for ($k = 0; $k < count($arysetday_chk); $k++) {
						if($arysetday_chk[$arykey[$k]] == $shn->日付_2->value ){
							$pPHPExcel	->setActiveSheetIndex( $pSheetIndex )
										->setCellValue('K'.$arykey[$k]	  , $shn->数値_11->value	);
//										->setCellValue('M'.$arykey[$k]	  , $shn->Single_line_text_10->value	);
						}
					}
				}
			}

			return ($ret);
		}
		/*************************************************************************/
	    /* ユーザー情報の変換                                                    */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setUserName( $pCode ) {

			$setName = "";

			switch ( $pCode ){
			case "Akemi Anzai":
				$setName = "安斎明美";
				break;
			case "Maricar":
				$setName = "Maricar";
			  	break;
			case "Marsha":
				$setName = "Marsha";
			  	break;
			case "Masae Tomoda":
				$setName = "共田昌恵";
			  	break;
			case "tmotomura@evaheart-usa.com":
				$setName = "本村禎";
			  	break;
			case "amiura@evaheartinc.com":
				$setName = "三浦明美";
			  	break;
			case "miyamoto-koji@evaheart.co.jp":
				$setName = "宮本浩次";
			  	break;
			case "jhamaguchi@evaheartinc.com":
				$setName = "濱口淳";
			  	break;
			default:
			}

			return ( $setName );
		}

		/*************************************************************************/
	    /* 添付ファイルをエクセルに配置   								         */
		/* $cell  貼り付けcell位置   							   	             */
        /* $path  画像path   								                     */
        /* $sheet 貼り付けたいシート   								             */
	    /*************************************************************************/
		function pastePic($cell,$path,$sheet){
			$draw = new PHPExcel_Worksheet_Drawing();
			$draw->setPath($path);
			$draw->setCoordinates($cell);
			$draw->setResizeProportional(false);
			$draw->setHeight(57);
			$draw->setwidth(25);
			$draw->setWorksheet($sheet);
		}

		/*************************************************************************/
	    /* 添付ファイルをダウン->アップし、新filekeyを取得・設定する             */
	    /*  引数	項目情報(参照) 、 レコード情報(参照)                         */
	    /*  関数値  正常："" 、 異常：エラーメッセージ                           */
	    /*************************************************************************/
		function getDwnUpNewFileKey( &$pKmk , &$pKmkFileKey ) {
			$ret = "";

			if( $pKmk->type == "FILE" ) {
			} else {
				return $ret;
			}

			// ダウンロード
			$f = new TcKintone();
			$f->parInit();
			$f->intAppID = TC_APPID_CC_TRRE;
			$wkFileInfo = array();
			if( is_array($pKmk->value) ) {
				// 複数ファイル有
				$wkFileInfo = $pKmk->value;
			} else {
				// 単一ファイル
				$wkFileInfo[] = $pKmk->value;
			}
			foreach( $wkFileInfo as $key_f => $val_f ) {
				if( $val_f ) {
					$dwndat = $f->runDOWNLOAD( $val_f );
					if( $f->strHttpCode == 200 ) {

						// アップロード
						$filePath = "/home/cross-tier/timeconcier.jp/public_html/forkintone/cc_evij/image/";
						$u = new TcKintone();
						$u->parInit();
						$u->intAppID = TC_APPID_CC_TRRE;
						$retFileKey = $u->runUPLOAD( $val_f , $dwndat , $filePath);
						if( $u->strHttpCode == 200 ) {
							// 新filekey
							$pKmkFileKey = $retFileKey->fileKey;
						} else {
							$ret = "ファイルをアップロードできませんでした。 ".$val_f-name." (".$u->strHttpCode.":".$u->message.")";
							break;
						}
					} else {
						$ret = "ファイルをダウンロードできませんでした。 ".$val_f-name." (".$f->strHttpCode.":".$f->message.")";
						break;
					}
				}
			}

			return $ret;
		}

		/*************************************************************************/
	    /*フォルダ内のファイルを削除する							             */
	    /*************************************************************************/
		function deleteData ( $dir ) {
		    if ( $dirHandle = opendir ( $dir )) {
		        while ( false !== ( $fileName = readdir ( $dirHandle ) ) ) {
		            if ( $fileName != "." && $fileName != ".." ) {
		                unlink ( $dir.$fileName );
		            }
		        }
		        closedir ( $dirHandle );
		    }
		}
	}

?>
