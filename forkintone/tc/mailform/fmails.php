<?php

# 1)変数 $formhtml にフォームのHTMLファイル名を定義します。
#   このファイルと別の場所にある場合はこのファイルからの相対パスで指定します。

$formhtml='https://www.timeconcier.jp/mitsumori.html';

# 2)変数 $mailto に送信先アドレスを定義します。
#   [書式] 名前<support@timeconcier.jp> または support@timeconcier.jp
#   送信先はコンマ区切りでいくつでも設定できます。
#   投稿者に自動返信する場合はリストの先頭にフォームのキーワードの変数で指定します。

  $mailto='$name<$email>,タイムコンシェル<support@timeconcier.jp>';

# 3)変数 $from に送信元に設定するアドレスを定義します。
#   このプログラムを置くサーバードメインのアドレスでないと送信拒否される場合があります。   

$from='タイムコンシェル<support@timeconcier.jp>';

# 4)変数 $replyto に返信先アドレスを定義します。（任意）
#   このアドレスは投稿者への自動返信メールだけに使われます。これ以外のメールには投稿者の
#   アドレスが返信先としてセットされます。

$replyto='サポート<support@timeconcier.jp>';

# 5)変数 $envelopefrom に envelope-fromアドレスを定義します。（任意）
#   メールが配送できなかった場合、このアドレスにエラーレポートが届きます。
#   このプログラムを置くサーバードメインのアドレス以外を設定すると送信拒否される場合が
#   あります。

$envelopefrom='support@timeconcier.jp';

# 6)変数 $denyblank に入力必須項目を定義します。
#   入力必須にする項目IDと項目名をカンマ区切りでいくつでも指定できます。

$denyblank='list[],依頼ソリューション,company,会社名,name,お名前,email,メールアドレス,attach[],出力データファイル,attach[],出力定義ファイル';

# 7)変数 $upfile に添付を許可するファイルの拡張子を定義します。
#   セキュリティ確保のために必ず定義してください。
#   拡張子はカンマ区切りでいくつでも指定できます。

$upfile='xls,xlsx,gif,jpg,jpeg,png';

# 8)変数 $maxsize に添付を許可するファイル最大サイズ(KB単位)を定義します。
#   ただし、サイズの制限値は php.ini の設定値が優先します。

$maxsize='900KB';

# 9)変数 $htmlmail に HTMLメールを使うか否かを定義します。（1=使う、0=使わない）
#   使うとした場合は、本文にはHTMLメール非対応のメールクライアントに対するプレインテキス
#   トメッセージを設定してください。

$htmlmail=1;

#10)変数 $mailbody に、$mailto で指定したアドレスに送信するメール本文を記述します。
#   フォームの項目キーワードの頭に $ を付けた変数で記述すると展開されます。
#   環境変数 $date(現在時刻 YYYY/MM/DD hh:mm:ss)、$ip(REMOTE_ADDR)、
#   $host(REMOTE_HOST) も同様です。

$mailbody='
$name 様
受信日時： $date

送信ありがとうございました。

タイムコンシェルホームページ見積依頼フォームからのメールを受け取りました。
このメールはシステムからの自動返信メールです。
のちほど担当者から改めてご連絡を差しあげますのでご了承願います。

[依頼ソリューション] $list

[会社名] $company 様

[お名前] $name 様

[電話番号] $tell

[メールアドレス] $email

[お問い合わせ内容]
$message



';//（メール本文ここまで）

#11)変数 $finpage に送信完了後に表示するページのURLまたはシステムパスを定義します。
#   パスで定義した場合は、ファイル中の「$英数字」形式の文字列はスカラー変数とみなして展開
#   します。

//$finpage='https://example.jp/index.html';
//$finpage='/home/yurdomain/htdocs/fmail/thanks.html';

#12)変数 $fincomment に送信完了後に表示するコメントを記述します。
#   ただし、11)を定義するとそちらが優先します。

$fincomment='

<!--コンテンツ-->
<div id="middle01">

<div id="bg_zindex02"></div>


<img src="https://www.timeconcier.jp/wordpress/wp-content/themes/78creative/prebuilt/images/mainimg/main_report.jpg" /><br />
<div id="middle02_sub">

<div class="column_main">
	<div id="pk">
		<a href="http://www.timeconcier.jp">Home</a> » 
				帳票依頼フォーム	</div>

	<p id="post-70" class="post-70 page type-page status-publish hentry post">
		<h2 class="title_h2">帳票依頼フォーム<span>REPORT REQUEST</span></h2>
			<h1>送信完了</h1>
			$name様<br>
			<br>
			送信ありがとうございました。<br>

<div class="wp_social_bookmarking_light"><div><g:plusone size="medium"  href="https://www.timeconcier.jp/about.html"></g:plusone></div><div><iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html?url=http%3A%2F%2Fwww.timeconcier.jp%2Fabout.html&amp;text=%E3%82%A2%E3%83%A9%E3%83%BC%E3%83%88%E3%83%9E%E3%83%8D%E3%82%B8%E3%83%A1%E3%83%B3%E3%83%88%E3%81%A8%E3%81%AF&amp;lang=ja&amp;count=horizontal" style="width:90px; height:20px;"></iframe></div><div><a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.timeconcier.jp%2Fabout.html&title=%E3%82%A2%E3%83%A9%E3%83%BC%E3%83%88%E3%83%9E%E3%83%8D%E3%82%B8%E3%83%A1%E3%83%B3%E3%83%88%E3%81%A8%E3%81%AF" title="Share on LinkedIn" rel=nofollow class="wp_social_bookmarking_light_a" target=_blank><img src="https://www.timeconcier.jp/wordpress/wp-content/plugins/wp-social-bookmarking-light/images/linkedin.png" alt="Share on LinkedIn" title="Share on LinkedIn" width="16" height="16" class="wp_social_bookmarking_light_img" /></a></div></div><br class="wp_social_bookmarking_light_clear" />		</p>
		<!---->
</div>



<!-- サイドバーの共有宣言 アラートマネジメント用 -->
<div class="column_side">
		
</div>

<br class="clear" />

<div class="text_r">
<a href="#pagetop" class="pagetop">ページの上へもどる</a>
</div>

</div>

<img src="https://www.timeconcier.jp/wordpress/wp-content/themes/78creative/prebuilt/images/common/bottom_bg.gif" /><br />
</div>

<!--ボトムナビ-->

<!--ボトムナビ-->
<div id="footer_navi01">
	<div id="footer_navi01_l">
	</div>
	<div id="footer_navi01_r">
		<strong class="text_14px">タイムコンシェル株式会社</strong><br />
		TIME CONCIER Co.,Ltd<br />
		〒781-8007<br />
		高知県高知市仲田町2-11<br />
		TEL：088-833-5660<br />
		FAX：088-833-5666<br />
		受付時間:10:00～12:00<br />
		13:00～18:00<br />
		　※土日祝日を除く
	</div>
	<br class="clear" />
</div>

<br class="clear" />

<!--コピーライト-->
<div id="footer_copy01">
	<div id="footer_copy02"><p>Copiright(C)TIME CONCIER Co.,Ltd. All rights reserved.</p></div>
</div>


<script type="text/javascript" src="https://www.timeconcier.jp/wordpress/wp-content/plugins/contact-form-7/jquery.form.js?ver=2.52"></script>
<script type="text/javascript" src="https://www.timeconcier.jp/wordpress/wp-content/plugins/contact-form-7/scripts.js?ver=3.0.2.1"></script>
<!-- BEGIN: WP Social Bookmarking Light -->
<!-- END: WP Social Bookmarking Light -->
';
?>