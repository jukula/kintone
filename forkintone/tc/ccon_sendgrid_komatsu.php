<?php
/*****************************************************************************/
/*   メール送信PHP                                            (Version 1.00) */
/*   ファイル名 : ccon_sendgrid.php                                          */
/*   更新履歴   2014/11/17  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcSendGrid();

	// データ読み込み。
	// １．案件管理画面から受け取ったデータをセットする
	$clsSrs->paraRecordNo = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcSendGrid
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraRecordNo		= null; 	// 送信先アドレス（パラメタ）
	    var $err;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcSendGrid() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {

//			$url = 'http://sendgrid.com/';
			$url = 'https://api.sendgrid.com/';

			// ユーザーID・パスワード・送信元を設定(本稼働時はdefkintoneconf.incに定義するべき)
			$user = 'sg450sf8@kke.com';
			$pass = 'hiromi';

			// アプリからデータ取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKDHK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraRecordNo; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			if( $k->strHttpCode == 200 ) {
				echo "データの取得に成功しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
			} else {
				echo "データの取得に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
				break;
			}

			if( $k->intDataCount > 0 ) {
				// パラメータのセット
				$mailfrom  = $json->records[0]->送信元メールアドレス->value;
				$mailto    = $json->records[0]->送信先メールアドレス->value;
				$mailcc    = $json->records[0]->CC->value;
				$mailbcc   = $json->records[0]->BCC->value;
				$mailTitle = $json->records[0]->件名->value;
				$mailtext  = $json->records[0]->備考->value;

				// 添付があるなら設定する
				if( $json->records[0]->メール添付->value ) {

					$fileName = $json->records[0]->メール添付->value[0]->name;
					$filePath = dirname(__FILE__);

					$retDUnfk = $this->getDwnUpNewFileKey( $json->records[0]->メール添付 , $json->records[0]->メール添付->value[0]->fileKey );
					if( $retDUnfk  == "" ) {
						// 正常
					} else {
						// エラー
						echo $retDUnfk."<br>\n 終了。";
						exit;
					}

					$params = array(
					 'api_user' => $user,
					 'api_key'  => $pass,
					 'to' 		=> $mailto,
					 'cc' 		=> $mailcc,
					 'bcc' 		=> $mailbcc,
					 'subject'  => $mailTitle,
					 'text' 	=> $mailtext,
					 'from' 	=> $mailfrom,
					 'files['.$fileName.']' => '@'.$filePath.'/bkfile/'.$fileName
					);

				}else{
					$params = array(
					 'api_user' => $user,
					 'api_key'  => $pass,
					 'to' 		=> $mailto,
					 'cc' 		=> $mailcc,
					 'bcc' 		=> $mailbcc,
					 'subject'  => $mailTitle,
					 'text' 	=> $mailtext,
					 'from' 	=> $mailfrom
					);
				}

				$request = $url.'api/mail.send.json';
				// Generate curl request
				$session = curl_init($request);
				// Tell curl to use HTTP POST
				curl_setopt ($session, CURLOPT_POST, true);
				// Tell curl that this is the body of the POST
				curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
				// Tell curl not to return headers, but do return the response
				curl_setopt($session, CURLOPT_HEADER, false);
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				// obtain response
				$response = curl_exec($session);
				curl_close($session);
				// print everything out
				print_r($response);

				// パラメータのセット
				$mailfrom  = $json->records[0]->送信元メールアドレス->value;
				$mailto    = $json->records[0]->送信元メールアドレス->value;
				$mailTitle = $json->records[0]->件名->value;
				$mailtext  = $json->records[0]->備考->value;

				// 添付があるなら設定する
				if( $fileName ) {

					$params = array(
					 'api_user' => $user,
					 'api_key'  => $pass,
					 'to' 		=> $mailto,
					 'subject'  => $mailTitle,
					 'text' 	=> $mailtext,
					 'from' 	=> $mailfrom,
					 'files['.$fileName.']' => '@'.$filePath.'/bkfile/'.$fileName
					);

				}else{
					$params = array(
					 'api_user' => $user,
					 'api_key'  => $pass,
					 'to' 		=> $mailto,
					 'subject'  => $mailTitle,
					 'text' 	=> $mailtext,
					 'from' 	=> $mailfrom
					);
				}

				$request = $url.'api/mail.send.json';
				// Generate curl request
				$session = curl_init($request);
				// Tell curl to use HTTP POST
				curl_setopt ($session, CURLOPT_POST, true);
				// Tell curl that this is the body of the POST
				curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
				// Tell curl not to return headers, but do return the response
				curl_setopt($session, CURLOPT_HEADER, false);
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				// obtain response
				$response = curl_exec($session);
				curl_close($session);
				// print everything out
				print_r($response);

			    echo "処理が完了しました。  ";
			}
			echo "<input type='button' value='閉じる' onClick='window.close()'>\n";
		}

		/*************************************************************************/
	    /* 添付ファイルをダウン->アップし、新filekeyを取得・設定する             */
	    /*  引数	項目情報(参照) 、 レコード情報(参照)                         */
	    /*  関数値  正常："" 、 異常：エラーメッセージ                           */
	    /*************************************************************************/
		function getDwnUpNewFileKey( &$pKmk , &$pKmkFileKey ) {
			$ret = "";

			if( $pKmk->type == "FILE" ) {
			} else {
				return $ret;
			}

			// ダウンロード
			$f = new TcKintone();
			$f->parInit();
			$f->intAppID = $clsSrs->paraAppID;
			$wkFileInfo = array();
			if( is_array($pKmk->value) ) {
				// 複数ファイル有
				$wkFileInfo = $pKmk->value;
			} else {
				// 単一ファイル
				$wkFileInfo[] = $pKmk->value;
			}
			foreach( $wkFileInfo as $key_f => $val_f ) {
				if( $val_f ) {
					$dwndat = $f->runDOWNLOAD( $val_f );
					if( $f->strHttpCode == 200 ) {

						// アップロード
						$filePath = "/home/cross-tier/timeconcier.jp/public_html/forkintone/tc/bkfile/";
						$u = new TcKintone();
						$u->parInit();
						$u->intAppID = $clsSrs->paraAppID;
						$retFileKey = $u->runUPLOAD( $val_f , $dwndat , $filePath);
						if( $u->strHttpCode == 200 ) {
							// 新filekey
							$pKmkFileKey = $retFileKey->fileKey;
						} else {
							$ret = "ファイルをアップロードできませんでした。 ".$val_f-name." (".$u->strHttpCode.":".$u->message.")";
							break;
						}
					} else {
						$ret = "ファイルをダウンロードできませんでした。 ".$val_f-name." (".$f->strHttpCode.":".$f->message.")";
						break;
					}
				}
			}

			return $ret;
		}

	}
?>