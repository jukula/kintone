<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tc_web.php                 				                 */
/*   更新履歴   2013/07/16  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcMail();

//	$clsSrs->paraAppID = $_REQUEST['appno'] - 0;
	$clsSrs->paraAppID = 2772 - 0;

	// 実行
	$clsSrs->main();

	class TcMail
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;
		var $paraAppID 	= null;
		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcMail() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$ret = false;
			$keyset = false;

			$array	= (unserialize(file_get_contents($this->paraAppID.'.txt')));

			for($a = 0; $a < count($array[0]->records); $a++) {
				// --------------------
				// 更新する。
				// --------------------
				$k = new TcKintone();
				$k->parInit();							// API連携用のパラメタを初期化する
				$k->intAppID 		= $this->paraAppID;	// アプリID
				$k->strContentType	= "Content-Type: application/json";

				if($keyset == false){
					while ($ary_name = current($array[0]->records[$a])) {
					    $ary_keys[] = key($array[0]->records[$a]);
					    next($array[0]->records[$a]);
					}
					$keyset = true;
				}

				$recObj = new stdClass;
				$recObj->id = $array[0]->records[$a]->レコード番号->value;
//print_r($array[0]);
				for($j = 0; $j < count($ary_keys); $j++){
					$pos = strpos($ary_keys[$j], "$");
					if($pos === false ){
						if( $this->getInsUpNgKmk( $array[0]->records[$a]->$ary_keys[$j]->type ) == false ) {
//echo($array[0]->records[$a]->$ary_keys[$j]->type ."←タイプ　中身→". $array[0]->records[$a]->$ary_keys[$j]->value."<br>");

	 						$recObj->record->$ary_keys[$j] = $array[0]->records[$a]->$ary_keys[$j];
/*
							switch( $array[0]->records[$a]->$ary_keys[$j]->type ) {
								case "SUBTABLE":
								case "CHECK_BOX":
								case "MULTI_SELECT":
								case "USER_SELECT":
	 								$recObj->record->$ary_keys[$j] = $array[0]->records[$a]->$ary_keys[$j];
									break;
								default:
									$recObj->record->$ary_keys[$j] = $this->valEnc( $array[0]->records[$a]->$ary_keys[$j]->value );
									break;
							}
*/
						}
					}else{
					}
				}
// print_r($recObj);


				$updData 			= new stdClass;
				$updData->app 		= $this->paraAppID;
				$updData->records[] = $recObj;
				$k->aryJson = $updData;

				$json = $k->runCURLEXEC( TC_MODE_UPD );
print_r($k);
			}
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){

			$base  = new DateTime();
			$today = $base->format('Ymd');

			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');

			$age = (int) (($today - $birthday) / 10000);

			return $age;
		}

		/*************************************************************************/
	    /* 添付ファイルをダウン->アップし、新filekeyを取得・設定する             */
	    /*  引数	項目情報(参照) 、 レコード情報(参照)                         */
	    /*  関数値  正常："" 、 異常：エラーメッセージ                           */
	    /*************************************************************************/
		function getDwnUpNewFileKey( &$pKmk , &$pKmkFileKey ) {
			$ret = "";

			if( $pKmk->type == "FILE" ) {
			} else {
				return $ret;
			}

			// ダウンロード
			$f = new TcKintone();
			$f->parInit();
			$f->intAppID = 2772;
			$wkFileInfo = array();
			if( is_array($pKmk->value) ) {
				// 複数ファイル有
				$wkFileInfo = $pKmk->value;
			} else {
				// 単一ファイル
				$wkFileInfo[] = $pKmk->value;
			}
			foreach( $wkFileInfo as $key_f => $val_f ) {
				if( $val_f ) {
					$dwndat = $f->runDOWNLOAD( $val_f );
					if( $f->strHttpCode == 200 ) {

						// アップロード
						$filePath = "/home/cross-tier/timeconcier.jp/public_html/forkintone/apicom/tctmp/";
						$u = new TcKintone();
						$u->parInit();
						$u->intAppID = 2772;
						$retFileKey = $u->runUPLOAD( $val_f , $dwndat , $filePath);
						if( $u->strHttpCode == 200 ) {
							// 新filekey
							$pKmkFileKey = $retFileKey->fileKey;
						} else {
							$ret = "ファイルをアップロードできませんでした。 ".$val_f-name." (".$u->strHttpCode.":".$u->message.")";
							break;
						}
					} else {
						$ret = "ファイルをダウンロードできませんでした。 ".$val_f-name." (".$f->strHttpCode.":".$f->message.")";
						break;
					}
				}
			}

			return $ret;
		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		function getInsUpNgKmk( $pType ) {
			$ret = false;

			switch( strtoupper( $pType ) ) {
				// 更新しない項目
				case "CREATOR":
					//作成者（取得のみ）
				case "MODIFIER":
					//更新者（取得のみ）
				case "STATUS_ASSIGNEE":
					//作業者（取得のみ）
				case "CREATED_TIME":
					//作成日時（取得のみ）
				case "UPDATED_TIME":
					//更新日時（取得のみ）
				case "RECORD_NUMBER":
					//レコード番号（取得のみ）
				case "STATUS":
					//ステータス（取得のみ）
				case "LOOKUP":
					//ルックアップ(※ルックアップは文字列か数値型が返ってくる仕様)
				case "FILE":

					$ret = true;
					break;
/*
				// 更新する項目
				case "FILE":
				case "SUBTABLE":
					break;
*/
				default:
					break;
			}

			return( $ret );
		}


	}

?>
