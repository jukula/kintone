<?php
/*****************************************************************************/
/* 	 見積管理PHP                                              (Version 1.00) */
/*   ファイル名 : mitumori.php                                       		 */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");

    include_once("defkintoneconf.inc");
    include_once("../tccom/tcutility.inc");
    include_once("tcerror.php");
    // kintoneクラスの定義を呼び出す。
    // 以下、整理(2015.2.9 Kataoka)
    include_once("../tccom/tckintone.php");
	include_once("../tccom/tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_TB1_COUNT" , 15 );
	define( "TC_TB2_COUNT" , 7 );

	// パターンＢ用
	define( "TC_TB3_COUNT" , 14 );
	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// 差分読み込み。
	// １．案件管理画面から呼ばれた場合、その案件のみ処理対象とする。
	// ２．作成済み労務原価の最新作成日付以降に
	// 　　作成・更新した作業日報を処理対象とする。
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;
	$clsSrs->paraLayout  = $_REQUEST['ptrn'];

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// レコード番号（パラメタ）
	    var $paraLayout			= null; 	// レコード番号（パラメタ）
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err 	  = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function main() {
			$msg = "";

			// 見積管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCMTKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = 266"; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );
			// エクセルの契約書テンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setIncludeCharts(TRUE); // ここがポイント
			$objPHPExcel = $objReader->load("templates/grf_test.xlsx");

			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , 0 , $json->records[0] );
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "見積書.xlsx";
			// ダウンロード用エクセルを保存
			$objWriter->setIncludeCharts(TRUE); // ここがポイント
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "https://www.timeconcier.jp/forkintone/tc/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pSheetNo , $pDat ) {
			$ret = "";

			$pPHPExcel	->setActiveSheetIndex( $pSheetNo )
			            ->setCellValue('N23'	,	"20"	)
			            ->setCellValue('O23'	,	"20"	)
			            ->setCellValue('P23'	,	"20"	)
			            ->setCellValue('Q23'	,	"20"	)
			            ->setCellValue('R23'	,	"20"	)
			            ->setCellValue('N19'	,	"90"	);
			return ($ret);

		}

	}

?>
