<?php
/*****************************************************************************/
/* 請求書出力PHP                                              (Version 1.00) */
/*   ファイル名 : gcseikyu.php                                               */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	define( "TC_SHN_GYO" , 16 );
	define( "TC_SHN_SHEET" , 5 );
	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	//パラメタ受取り
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

// 実行
	$clsSrs->main();


	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg 	 = "";
			$balance = 0;

			// 請求入金管理アプリ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= 2772;						// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = "."1450"; // クエリパラメータ
			$json = $k->runCURLEXEC( TC_MODE_SEL );

			// セルへ設定
			if( $k->intDataCount > 0 ) {
echo("取得成功<br>");
			}



			if($json->records[0]->プロセス->value == "請求書作成中"){
				// --------------------
				// 請求入金管理を更新する。
				// --------------------
				$k = new TcKintone();
				$k->parInit();									// API連携用のパラメタを初期化する
				$k->intAppID 		= TC_APPID_GC_SNKK;			// アプリID
				$k->strContentType	= "Content-Type: application/json";

				// プロセスが請求書作成中の場合、請求書発行済にする
				$recObj = new stdClass;
				$recObj->id = $this->paraAnkenID;
				$recObj->record->プロセス = $this->common->valEnc("請求書発行済");

				$updData 			= new stdClass;
				$updData->app 		= TC_APPID_GC_SNKK;
				$updData->records[] = $recObj;
				$k->aryJson = $updData;
				$jsonSNKK = $k->runCURLEXEC( TC_MODE_UPD );
			}
		}

		/*************************************************************************/
	    /*  日付型から月か日を返す                                               */
	    /*  引数	$pDat　 日付項目                                             */
	    /*  引数	$pPara　変換内容   0 = 月 / 1 = 日                           */
	    /*  関数値  int		処理対象の作業日報件数                               */
	    /*************************************************************************/
		function setDayGet( &$pDat , $pPara ) {
			$ret = "";

			$Date = explode("-", $pDat);

			if($pPara == 0 ){
				$ret = $Date[1];
			}else{
				$ret = $Date[2];
			}

			return ($ret);
		}

	}

?>
