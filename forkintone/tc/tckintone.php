<?php
/*****************************************************************************/
/*  kintone連携クラス                                         (Version 1.00) */
/*   クラス名       TcKintone                                                */
/*   メンバ変数     strDomain     	ドメイン                                 */
/*                  strUser       	ユーザ名                                 */
/*                  strPasswd     	パスワード                               */
/*                  strAuthUser   	認証ユーザ                               */
/*                  strAuthPasswd 	認証パスワード                           */
/*                  strDL         	ダウンロード先                           */
/*                  strURL        	URL                                      */
/*                  intAppID      	アプリID                                 */
/*                  strQuery   		クエリパラメータ                         */
/*                  arySelFields  	フィールドパラメータ                     */
/*                  aryJson			レコード追加、更新用データ(json形式)     */
/*                  aryDelRecNo   	削除対象のレコード番号                   */
/*                  strCustmReq		カスタムリクエスト                       */
/*                  strContentType	Content-Type (CURLOPT_HTTPHEADER へ追加) */
/*                  cHandler      	cURLハンドル                             */
/*                  aryOptionHeader	CURLOPT_HTTPHEADER                       */
/*                  intDataCount 	取得したデータのカウント                 */
/*                  strHttpCode    	Kintoneからの戻り値                      */
/*                  err           	エラー内容                               */
/*   メンバ関数     getURL          プロパティからURLを生成する              */
/*   メソッド       parInit         プロパティを初期化する                   */
/*                  runCURLEXEC     http通信を実行する                       */
/*   必要ファイル                                                            */
/*      defkintone.inc / tcutility.inc / tcerror.php                         */
/*   作成日         2013/04/15                                               */
/*   更新履歴       2013/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
include_once("../tccom/defkintone.inc");
include_once("defkintoneconf.inc");
include_once("../tccom/tcutility.inc");
include_once("tcerror.php");

// kintoneクラスの定義を呼び出す。
include_once("../tccom/tckintone.php");

?>
