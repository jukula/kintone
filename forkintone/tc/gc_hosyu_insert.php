<?php
/*****************************************************************************/
/* TC保守登録PHP                                              (Version 1.00) */
/*   ファイル名 : gc_hosyu_insert.php                                        */
/*   更新履歴   2013/04/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	define( "TC_URL1" , "https://".TC_CY_DOMAIN."/k/guest/12/".TC_APPID_ANKJ."/show#record=" ); // 案件管理URL
	define( "TC_URL2" , "https://".TC_CY_DOMAIN."/k/".TC_APPID_TCHSKR."/show#record=" ); // TC保守管理URL

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;


	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）

		var $err;
		var $HSKKID             = null;     // TC保守管理作成ID
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 請求先の名前保持用

			// 案件管理アプリのデータを取得
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客管理アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$jsonANKK->records[0]->TC顧客レコード番号->value; // クエリパラメータ
			$jsonKKKK = $k->runCURLEXEC( TC_MODE_SEL );

			// TC保守管理アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "(顧客レコード番号 = \"".$jsonKKKK->records[0]->レコード番号->value."\")"; // クエリパラメータ
			$jsonHSKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客データがあるかチェック
			if(count($jsonKKKK->records) > 0 ){
				// 保守データがあるかチェック
				if(count($jsonHSKK->records) == 0 ){

					// なければインサート
					$this->insHSKK( $jsonANKK , $jsonKKKK);
					$this->updANKK( $jsonANKK , $jsonKKKK);
					$rmgkKei = $this->HSKKID;

				}else{
					// すでに保守データができている場合、FALSE2を返す
					$rmgkKei = "FALSE2";
				}
			}else{
				// 顧客データがない場合、FALSE1を返す
				$rmgkKei = "FALSE1";
			}
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  TC保守管理へデータを追加する                                         */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  引数	$sknp_json  TC顧客管理のデータを参照する                     */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insHSKK( &$sgnp_json , &$sknp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			//顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			//保守サポート費用テーブルのデータ数を取得
			$tbcount = count($sgnp_json->records[0]->保守サポート費用テーブル->value);
			$hairetu1 = ""; //表示順並び替え用の項目　表示順　+　インサートNOを作成する
			$hairetu2 = ""; //表示順並び替え用の項目　$hairetu1を分解し、格納する

			for($a = 0; $a <= ($tbcount - 1); $a++) {
				$hairetu1[]	  = $sgnp_json->records[0]->保守サポート費用テーブル->value[$a]->value->NO_保守_->value.",".$a;
			}

			sort($hairetu1);
			for($i = 0; $i <= ($tbcount - 1); $i++) {
				$hairetu2[] = explode(",", $hairetu1[$i]);
			}

			$a=0; // 初期化
			$i=0; // 初期化

			// 書込み準備
			$recObj = new stdClass;

			// 通常項目
			$recObj->案件_地盤_リンク 				  = $this->valEnc( TC_URL1.$this->paraAnkenID );
			$recObj->案件レコード番号_地盤_ 		  = $this->valEnc( $this->paraAnkenID );
			$recObj->契約名 						  = $this->valEnc( "保守サポート（グランドコンシェル）" );
			$recObj->支払条件 						  = $this->tgfEnc( $tcRec , $i , "支払条件_保守サポート_" );
			$recObj->その他条件 					  = $this->tgfEnc( $tcRec , $i , "その他条件" );
			$recObj->契約日 						  = $this->tgfEnc( $tcRec , $i , "内示日" );
			$recObj->契約開始日 					  = $this->tgfEnc( $tcRec , $i , "サービス開始時期" );
			$recObj->保守区分 						  = $this->valEnc( "ｸﾞﾗﾝﾄﾞｺﾝｼｪﾙ" );

			$tbcount = count($sgnp_json->records[0]->保守サポート費用テーブル->value);
			$tbl = $tcRec->getFieldValue( $i , "保守サポート費用テーブル" );
			// テーブル項目

		    for($a = 0; $a <= ($tbcount - 1); $a++) {
				$recObj->保守契約明細テーブル->value[$a]->value->表示順 			= 	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "NO_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->ステータス１ 		= 	 $this->valEnc( "契約中" );
				$recObj->保守契約明細テーブル->value[$a]->value->商品・サービス名 	=	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "商品・サービス名_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->商品詳細 			= 	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "商品詳細_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->数量 			 	=	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "数量_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->単位 			 	= 	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "単位_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->売単価 			= 	 $this->tgfEnc( $tbl , $hairetu2[$a][1] , "単価_保守_" );
				$recObj->保守契約明細テーブル->value[$a]->value->摘要 			 	=    $this->tgfEnc( $tbl , $hairetu2[$a][1] , "契約摘要_保守_");
				$recObj->保守契約明細テーブル->value[$a]->value->原単価 			=    $this->tgfEnc( $tbl , $hairetu2[$a][1] , "原単価_保守_");
				$recObj->保守契約明細テーブル->value[$a]->value->開始日 			=    $this->valEnc( $sgnp_json->records[0]->サービス開始時期->value );
			}

			// --------------------
			// TC保守管理へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$insData 			= new stdClass;
			$insData->app 		= TC_APPID_TCHSKR;
			$insData->records[] = $recObj;

			$k->aryJson = $insData;						// 追加対象のレコード番号
			$json = $k->runCURLEXEC( TC_MODE_INS );
			$this->HSKKID = $k->strInsRecNo[0];
		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json , &$sknp_json) {

			// 案件管理(地盤)
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			// 顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			$i=0;
			$tbcount = 0;

			// -----------------------------
			// 案件管理(地盤調査)を更新する。
			// -----------------------------
			$k = new TcKintone(TC_CY_DOMAIN,TC_CY_USER,TC_CY_PASSWORD,TC_CY_AUTH_USER,TC_CY_AUTH_PASS,12);	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_ANKJ;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;

			$recObj->record->会社名 	  			= $this->tgfEnc( $tcReck , $i , "顧客名" );
			$recObj->record->会社名フリガナ		  	= $this->tgfEnc( $tcReck , $i , "フリガナ" );
			$recObj->record->郵便番号			  	= $this->tgfEnc( $tcReck , $i , "郵便番号" );
			$recObj->record->都道府県			  	= $this->tgfEnc( $tcReck , $i , "都道府県名" );
			$recObj->record->所在地 			  	= $this->tgfEnc( $tcReck , $i , "住所１" );
			$recObj->record->電話番号 		  		= $this->tgfEnc( $tcReck , $i , "電話番号" );
			$recObj->record->ＦＡＸ		 		  	= $this->tgfEnc( $tcReck , $i , "FAX" );
			$recObj->record->代表者氏名 			= $this->tgfEnc( $tcReck , $i , "代表者名" );
			$recObj->record->代表者フリガナ 		= $this->tgfEnc( $tcReck , $i , "代表者名フリガナ" );
			$recObj->record->担当者 			  	= $this->tgfEnc( $tcReck , $i , "担当者" );
			$recObj->record->担当者携帯 			= $this->tgfEnc( $tcReck , $i , "担当者携帯" );
			$recObj->record->締め日 			  	= $this->tgfEnc( $tcReck , $i , "締め日" );
			$recObj->record->回収サイト 			= $this->tgfEnc( $tcReck , $i , "回収サイト" );
			$recObj->record->回収日 			  	= $this->tgfEnc( $tcReck , $i , "回収日" );
			$recObj->record->回収条件 			  	= $this->tgfEnc( $tcReck , $i , "回収条件" );
			$recObj->record->回収方法 			  	= $this->tgfEnc( $tcReck , $i , "回収方法" );
			$recObj->record->保守管理リンク 		= $this->valEnc( TC_URL2.$this->HSKKID );
			$recObj->record->TC保守レコード番号 	= $this->valEnc( $this->HSKKID );

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_ANKJ;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );

		}
	}

?>
