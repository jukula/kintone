<?php
/*****************************************************************************/
/* TC保守登録PHP                                              (Version 1.00) */
/*   ファイル名 : tc_hosyu_insert.php                                        */
/*   更新履歴   2014/10/15  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	// レコード番号、テーブルの行番号
	$clsSrs->paraAnkenID = $_REQUEST['ptno'] - 0;

	// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）

		var $err;
		var $HSKKID             = null;     // TC保守管理作成ID
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg = "";
			$sAnkenname = ""; // 請求先の名前保持用

			// 案件管理アプリのデータを取得
			$k = new TcKintone();	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonANKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客管理アプリのデータを取得
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCKKKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$jsonANKK->records[0]->顧客レコード番号->value; // クエリパラメータ
			$jsonKKKK = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客データがあるかチェック
			if(count($jsonKKKK->records) > 0 ){
				// なければインサート
				$this->insHSKK( $jsonANKK , $jsonKKKK);
				$this->updANKK( $jsonANKK , $jsonKKKK);
				$rmgkKei = $this->HSKKID;
			}else{
				// 顧客データがない場合、FALSE1を返す
				$rmgkKei = "FALSE1";
			}
			echo ("phpRet = '".$rmgkKei."';\n");
			echo $msg;

		}

	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function tgfEnc( &$obj , $idx , $fldNm ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
			return ( $wk );
		}

		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

		/*************************************************************************/
	    /*  TC保守管理へデータを追加する                                         */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  引数	$sknp_json  TC顧客管理のデータを参照する                     */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function insHSKK( &$sgnp_json , &$sknp_json ) {

			// ------------------------------------------------
			// 一括登録件数の制限ごとに登録にデータを加工する。
			// ------------------------------------------------

			//案件項目
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			//顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			//テーブル_お支払条件のデータ数を取得
			$tbcount = count($sgnp_json->records[0]->テーブル_お支払条件->value);

			$a=0; // 初期化
			$i=0; // 初期化
			$viewcnt = 1;
			// 書込み準備
			$recObj = new stdClass;

			// 通常項目
			$recObj->顧客レコード番号 		  		  = $this->tgfEnc( $tcReck , $i , "レコード番号" );
			$recObj->契約名 						  = $this->tgfEnc( $tcRec , $i , "案件名" );
			$recObj->支払条件 						  = $this->tgfEnc( $tcRec , $i , "支払条件_ランニング_" );
			$recObj->その他条件 					  = $this->tgfEnc( $tcRec , $i , "その他条件" );
			$recObj->契約日 						  = $this->tgfEnc( $tcRec , $i , "内示日" );
			$recObj->契約開始日 					  = $this->tgfEnc( $tcRec , $i , "契約開始日" );
			$recObj->案件レコード番号 				  = $this->tgfEnc( $tcRec , $i , "レコード番号" );
			$recObj->事業区分 						  = $this->tgfEnc( $tcRec , $i , "事業区分" );
			$recObj->エリア区分 					  = $this->tgfEnc( $tcRec , $i , "エリア区分" );
			$recObj->直間区分 				  		  = $this->tgfEnc( $tcRec , $i , "直間区分" );
			$recObj->営業施策区分 				  	  = $this->tgfEnc( $tcRec , $i , "営業施策区分" );
//			$recObj->保守区分 						  = $this->valEnc( "kintone" );

			$tbcount = count($sgnp_json->records[0]->テーブル_お支払条件->value);
			$tbl = $tcRec->getFieldValue( $i , "テーブル_お支払条件" );
			// テーブル項目

		    for($a = 0; $a <= ($tbcount - 1); $a++) {
				if($sgnp_json->records[0]->テーブル_お支払条件->value[$a]->value->商品区分->value == "ランニング費"){
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->表示順 			= 	 $this->valEnc( ( $viewcnt ) );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->ステータス１ 		= 	 $this->valEnc( "契約中" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->商品・サービス名 	=	 $this->tgfEnc( $tbl , $a , "商品名" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->商品詳細 			= 	 $this->tgfEnc( $tbl , $a , "商品名備考" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->数量 			 	=	 $this->tgfEnc( $tbl , $a , "数量" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->単位 			 	= 	 $this->tgfEnc( $tbl , $a , "単位" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->売単価 			= 	 $this->tgfEnc( $tbl , $a , "単価" );
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->摘要 			 	=    $this->tgfEnc( $tbl , $a , "特記事項");
//					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->原単価 			=    $this->tgfEnc( $tbl , $a , "原単価_保守_");
					$recObj->保守契約明細テーブル->value[$viewcnt-1]->value->開始日 			=    $this->valEnc( $sgnp_json->records[0]->契約開始日->value );
					$viewcnt++;
				}
			}

			// --------------------
			// TC保守管理へ追加する。
			// --------------------
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCHSKR;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$insData 			= new stdClass;
			$insData->app 		= TC_APPID_TCHSKR;
			$insData->records[] = $recObj;

			$k->aryJson = $insData;						// 追加対象のレコード番号
			$json = $k->runCURLEXEC( TC_MODE_INS );
			$this->HSKKID = $k->strInsRecNo[0];
		}

		/*************************************************************************/
	    /*  案件管理のデータを更新する  	                                     */
	    /*  引数	$sgnp_json  案件管理のデータを参照する                       */
	    /*  関数値  string		正常終了:true、異常終了:false                    */
	    /*************************************************************************/
		function updANKK( &$sgnp_json , &$sknp_json) {

			// TC案件管理
			$tcRec = new TcKintoneRecord();
			$tcRec->setRecordList( $sgnp_json->records );

			// TC顧客項目
			$tcReck = new TcKintoneRecord();
			$tcReck->setRecordList( $sknp_json->records );

			$i=0;

			// -----------------------------
			// TC案件管理を更新する。
			// -----------------------------
			$k = new TcKintone();	// API連携クラス(ゲストスペースのため、パラメータを渡す)
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_TCANKK;			// アプリID
			$k->strContentType	= "Content-Type: application/json";

			$recObj = new stdClass;
			$recObj->id = $this->paraAnkenID;

			$recObj->record->保守レコード番号 	= $this->valEnc( $this->HSKKID );

			$updData 			= new stdClass;
			$updData->app 		= TC_APPID_TCANKK;
			$updData->records[] = $recObj;

			$k->aryJson = $updData;
			$json = $k->runCURLEXEC( TC_MODE_UPD );
		}
	}

?>
