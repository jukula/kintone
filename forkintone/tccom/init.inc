<?php
/*****************************************************************************/
/*  システム用 環境設定                                                      */
/*                                                                           */
/*                                    Copyright(C)2003-2009 CrossTier Corp.  */
/*****************************************************************************/

define("INSTALL_DIR"  , "kaerukun/"  );

$strHost = GetEnv("SERVER_NAME"  );
$strRoot = GetEnv("DOCUMENT_ROOT");

// 本番サーバ
define("ROOT_DIR"  , $strRoot . "/" . INSTALL_DIR               );
define("ROOT_URL"  , "http://"  . $strHost . "/" . INSTALL_DIR  );
define("SECURE_URL", "https://" . $strHost . "/" . INSTALL_DIR  );
define("COOKIE_URL", $strHost                                   );

//サイトタイトル
define("SIGHT_TITLE", "かえるくんシステム");

// 何日以内の書き込みをNewとするか？
define("NEW_DAYS"   , 7                   );

?>
