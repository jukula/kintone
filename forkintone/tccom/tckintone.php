<?php
/*****************************************************************************/
/*  kintone連携クラス                                         (Version 1.03) */
/*   クラス名       TcKintone                                                */
/*   メンバ変数     strDomain       ドメイン                                 */
/*                  strUser         ユーザ名                                 */
/*                  strPasswd       パスワード                               */
/*                  strAuthUser     認証ユーザ                               */
/*                  strAuthPasswd   認証パスワード                           */
/*                  strDL           ダウンロード先                           */
/*                  strURL          URL                                      */
/*                  strKGV1         共通のURL・カスタムヘッダの一部          */
/*                  intAppID        アプリID                                 */
/*                  intGuestID      ゲストID                                 */
/*                  strQuery        クエリパラメータ                         */
/*                  arySelFields    フィールドパラメータ                     */
/*                  aryJson         レコード追加、更新用データ(json形式)     */
/*                  aryDelRecNo     削除対象のレコード番号                   */
/*                  strCustmReq     カスタムリクエスト                       */
/*                  strContentType  Content-Type (CURLOPT_HTTPHEADER へ追加) */
/*                  intDataCount    取得したデータのカウント                 */
/*                  strHttpCode     Kintoneからの戻り値(Httpレスポンス)      */
/*                  strKntErrId     Kintoneからの戻り値(ID)：エラー時        */
/*                  strKntErrCode   Kintoneからの戻り値(CODE)：エラー時      */
/*                  strKntErrMsg    Kintoneからの戻り値(MESSAGE)：エラー時   */
/*                  strInsRecNo     新規登録後のレコード番号                 */
/*                  cHandler        cURLハンドル                             */
/*                  aryOptionHeader CURLOPT_HTTPHEADER                       */
/*                  err                                                      */
/*   メンバ関数     getURL          プロパティからURLを生成する              */
/*   メソッド       parInit         プロパティを初期化する                   */
/*                  runCURLEXEC     http通信を実行する                       */
/*                      ↓クライアントのWEBサーバで実行する                  */
/*                  runDOWNLOAD     ファイルダウンロード                     */
/*                  runUPLOAD       ファイルアップロード                     */
/*   必要ファイル                                                            */
/*      tckintone.php でインクルードする                                     */
/*   作成日         2013/09/02                                               */
/*   更新履歴       2014/11/09      Version 1.01(J.Kataoka)                  */
/*                                  SSL関連のオプション設定を停止            */
/*                  2014/11/10      Version 1.02(C.Komatsu)                  */
/*                                  CURLOPT_SSL_VERIFYPEERと                 */
/*                                  CURLOPT_SSL_VERIFYHOSTを変更             */
/*                  2015/02/09      Version 1.03(J.Kataoka)                  */
/*                                  cURL関連見直し、include追加              */
/*                                                                           */
/*                               Copyright(C)2013-2015 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
include_once("defkintone.inc");

class TcKintone
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $strDomain     	= "";		// ドメイン
    var $strUser       	= "";		// ユーザ名
    var $strPasswd     	= "";		// パスワード
    var $strAuthUser   	= "";		// 認証ユーザ
    var $strAuthPasswd 	= "";		// 認証パスワード
    var $strDL         	= "";		// ダウンロード先
    var $strURL        	= ""; 		// URL

	var $strKGV1		= ""; 		// 共通のURL・カスタムヘッダの一部
    var $intAppID      	= 0; 		// アプリID
	var $intGuestID		= "";		// ゲストID

	var $strToken       = "";       // APIトークン

	// SEL
    var $strQuery   	= ""; 		// クエリパラメータ
    var $arySelFields  	= array(); 	// フィールドパラメータ
    // INS,UPD
	var $aryJson		= array(); 	// レコード追加、更新用データ(json形式)
	// DEL
    var $aryDelRecNo   	= array();	// 削除対象のレコード番号
	var $strCustmReq	= "";		// カスタムリクエスト
	// 添付ファイル
	var $strContentType	= "";		// Content-Type (CURLOPT_HTTPHEADER へ追加)
	// 戻り値
	var $intDataCount 	= 0;		// 取得したデータのカウント
	var $strHttpCode;				// Kintoneからの戻り値(Httpレスポンス)
	var $strKntErrId;				// Kintoneからの戻り値(ID)：エラー時のみ
	var $strKntErrCode;				// Kintoneからの戻り値(CODE)：エラー時のみ
	var $strKntErrMsg;				// Kintoneからの戻り値(MESSAGE)：エラー時のみ
	var $strInsRecNo;				// 新規登録後のレコード番号

    var $cHandler      	= null;		// cURLハンドル
    var $aryOptionHeader;			// CURLOPT_HTTPHEADER
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function TcKintone( $pDomain = "" , $pAccount = "", $pPass = "", $pAuthUser = "", $pAuthPasswd = "", $pGuestID = "" , $pTokenID = "" ) {

		if( trim($pDomain.$pAccount.$pPass.$pAuthUser.$pAuthPasswd.$pGuestID) == "" ) {
			$this->strDomain     	= TC_CY_DOMAIN;		// ドメイン
			$this->strUser       	= TC_CY_USER;		// ユーザ名
			$this->strPasswd     	= TC_CY_PASSWORD;	// パスワード
			$this->strAuthUser   	= TC_CY_AUTH_USER;	// 認証ユーザ
			$this->strAuthPasswd 	= TC_CY_AUTH_PASS;	// 認証パスワード
			$this->intGuestID		= "";				// ゲストID
			$this->strToken			= $pTokenID;		// APIトークン
		} else {
			$this->strDomain     	= $pDomain;			// ドメイン
			$this->strUser       	= $pAccount;		// ユーザ名
			$this->strPasswd     	= $pPass;			// パスワード
			$this->strAuthUser   	= $pAuthUser;		// 認証ユーザ
			$this->strAuthPasswd 	= $pAuthPasswd;		// 認証パスワード
			$this->intGuestID		= $pGuestID;		// ゲストID
			$this->strToken			= $pTokenID;		// APIトークン
		}

		if( $this->intGuestID == "" ) {
			$wk = "";
		} else {
			$wk = "/guest/".$this->intGuestID;
		}
		$this->strKGV1 = "/k".$wk."/v1/";

		$this->err = new TcError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/

    /*************************************************************************/
    /* URLを取得する                                                         */
    /*  引数    "records" , "file"                                           */
    /*  関数値  string      正常:URL文字列 / NG:""                           */
    /*************************************************************************/
    function getURL( $pFileKBN ) {
        // DOMAINが設定されてなければ、""
        if( isNull($this->strDomain) ) {
			return("");
		} else {
        	return( "https://" . $this->strDomain . $this->strKGV1 . $pFileKBN .".json" );
		}
    }

    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* API連携用のパラメタを初期化する                                       */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function parInit() {
	    $this->intAppID      	= 0             ; // アプリID
	    $this->strQuery      	= ""            ; // クエリパラメータ
	    $this->arySelFields     = array()       ; // フィールドパラメータ
		$this->aryJson			= array()       ; // レコード追加、更新用データ(json形式)
	    $this->aryDelRecNo   	= array()       ; // 削除対象のレコード番号
		$this->strCustmReq		= ""			; // カスタムリクエスト
	    $this->strContentType 	= ""            ; // ヘッダパラメータ(CURLOPT_HTTPHEADER へ追加)
    }

    /*************************************************************************/
    /* 実行前にパラメタを初期化する                                          */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function parInitCOM() {
        $ret = false;

		// ログインIDおよびパスワード
        if(isNull($this->strUser)) {
            $this->err->setError( ERR_V200 );
            return($ret);
        }
        if(isNull($this->strPasswd)) {
            $this->err->setError( ERR_V200 );
            return($ret);
        }

		$this->intDataCount 	= 0;		// 取得件数を初期化
		$this->strInsRecNo		= array();	// 新規登録時のレコード番号

		$this->strKntErrId 		= "";		// kintoneエラーID
		$this->strKntErrCode	= "";		// kintoneエラーCODE
		$this->strKntErrMsg		= "";		// kintoneエラーMESSAGE

        // ユーザ認証用
		$strIdPass = $this->strUser . ":" . $this->strPasswd;
        $authToken = base64_encode($strIdPass);
        // BASIC認証用
        $strIdPassBasic = $this->strAuthUser . ":" . $this->strAuthPasswd;
        $authTokenBasic = base64_encode($strIdPassBasic);

		// トークンAPIが設定されていない場合とでヘッダを変える
		if( $this->strToken == "" ){
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"Host: ".$this->strDomain.":443" , 
				"X-Cybozu-Authorization: $authToken", 
				"Authorization: basic $authTokenBasic"
			);
		}else{
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"Host: ".$this->strDomain.":443" , 
				"X-Cybozu-API-Token: $this->strToken", 
				"X-Cybozu-Authorization: ", 
				"Authorization: basic $authTokenBasic"
			);
		}

		// Content-Type設定
		if( isNull($this->strContentType) ) {
			$this->aryOptionHeader[] = "Content-Type: ".TC_CONTENTTYPE_JSON;
		} else {
			if( strpos( strtoupper($this->strContentType ) , "CONTENT-TYPE:" ) === false ) {
				$this->aryOptionHeader[] = "Content-Type: ".$this->strContentType;
			} else {
				$this->aryOptionHeader[] = $this->strContentType;
			}
		}

		$ret = true;

		return( $ret );
    }

    /*************************************************************************/
    /* cURLハンドラに対し実行する                                            */
    /*  引数    $pMode  API連携モード                                        */
    /*          $pCURL  Trueの場合、旧関数呼び出し                           */
    /*  関数値  配列    OK:Json形式 / NG:null                                */
    /*************************************************************************/
    function runCURLEXEC( $pMode, $pCURL = true ) {
        if($pCURL) {
            return ($this->runCURLEXEC_OLD($pMode));
        }

        // URL
		$this->strURL = $this->getURL( "records" );

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;
		// 初期化とユーザ、パスワードチェック
		if( $this->parInitCOM() ) {
			//
		} else {
			return( $ret );
		}

		// ----------------
		// http通信の初期化
		// ----------------
/*
		$ch = curl_init( $this->strURL );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }
*/

		$wkReq = "";
		switch( $pMode ) {
			case TC_MODE_SEL:
				$wkReq = "GET";
				$wkaj 			= new stdClass;
				$wkaj->app		= $this->intAppID;
				$wkaj->query	= $this->strQuery;
				$wkaj->fields 	= $this->arySelFields;
				$this->aryJson 	= $wkaj;
				break;

			case TC_MODE_INS:
				$wkReq = "POST";
				break;

			case TC_MODE_UPD:
				$wkReq = "PUT";
				$wkaj 			= new stdClass;
				$wkaj->app		= $this->intAppID;
				break;

			case TC_MODE_DEL:
				if(  count($this->aryDelRecNo) > 0 ) {
					$wkReq = "DELETE";
					$wkaj 		= new stdClass;
					$wkaj->app	= $this->intAppID;
					$wkaj->ids 	= $this->aryDelRecNo;
					$this->aryJson = $wkaj;
				}
				break;

			default: 
				return( $ret );
				break;
		}
		$this->strCustmReq = $wkReq." ".$this->strKGV1."records.json HTTP/1.1";

/*
		curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $this->strCustmReq );
		curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, json_encode($this->aryJson) );
*/
// ***** 2014.11.9 サイボウズバージョンアップにともない、以下３パラメータの設定を停止
// ***** 2014.11.10 CURLOPT_SSL_VERIFYPEERとCURLOPT_SSL_VERIFYHOSTはtrueで使用
/*
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, true);
//		curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);   // 2や3を設定すると、SSLv2およびSSLv3の既知の脆弱性の影響を受けるため、デフォルトを使用するためコメント化
        curl_setopt($this->cHandler, CURLOPT_HEADER, false);
        curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);
*/

	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// KintoneAPIを実行する（HTTP通信）
/*
        $json = curl_exec($this->cHandler);
*/

        // Header
//        $header = array($wkReq => $this->strKGV1 . "records.json HTTP/1.1");
        $header = $this->aryOptionHeader;

        // HTTPコンテキスト生成
        $context = array( "http" => array( "method" => $wkReq,
                                           "header" => implode("\r\n", $header),
                                           "content" => json_encode($this->aryJson)
                                         )
                        );

        $res = file_get_contents( $this->strURL,
                                  false, // use_include_pathは必要ないのでfalse
                                  stream_context_create($context) // コンテキストの生成
        );

        if(!$res) {
            // エラー
        } else {
            $this->strHttpCode = $http_response_header[0];
            $json = $res;
        }

/*
		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);
		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);
*/

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		$ret = json_decode($json);
//		if ( $this->strHttpCode == 200 ) {
		if ( strpos($this->strHttpCode, "200") !== false ) {
			switch( $pMode ) {
				case TC_MODE_SEL:
					$this->intDataCount = count( $ret->records );
					break;

				case TC_MODE_INS:
					if( array_key_exists( 'id' , $ret ) ) {
						// 1件の場合 (1件でも複数件形式で更新しているので、ここは通らない。)
						$this->intDataCount	= count( $ret->id );
						$this->strInsRecNo	= $ret->id;
					} elseif( array_key_exists( 'ids' , $ret ) ) {
						// 複数件の場合
						$this->intDataCount	= count( $ret->ids );
						$this->strInsRecNo	= $ret->ids;
					}
					break;

				case TC_MODE_UPD:
				case TC_MODE_DEL:
				default: 
					break;
			}

		} else {
			// エラー時
			$this->strKntErrId 		= $ret->id;
			$this->strKntErrCode	= $ret->code;
			$this->strKntErrMsg		= $ret->message;
		}

		return( $ret);
    }

    /*************************************************************************/
    /* cURLハンドラに対し実行する                                            */
    /*  引数    API連携モード                                                */
    /*  関数値  配列     OK:Json形式 / NG:null                               */
    /*************************************************************************/
    function runCURLEXEC_OLD( $pMode ) {
        // URL
		$this->strURL = $this->getURL( "records" );

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;
		// 初期化とユーザ、パスワードチェック
		if( $this->parInitCOM() ) {
			//
		} else {
			return( $ret );
		}

		// ----------------
		// http通信の初期化
		// ----------------
		$ch = curl_init( $this->strURL );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }

	    /*-----------------------------------------------------------------------*/
	    /* cURLハンドラに対し共通のオプションを設定する                          */
	    /*-----------------------------------------------------------------------*/
		// ------------------------
		// モード別のオプション設定
		// ------------------------
/*
		switch( $pMode ) {
			case TC_MODE_SEL:
				// URLパラメタを作成する
				$strPara	= "";
				$wk	 		= array();
				$wk[] = "app=".$this->intAppID;		// アプリID
				if( isNull($this->strQuery) ) {		// クエリパラメタ
					//
				} else {
					$wk[] = "query=".urlencodeUtf8( $this->strQuery );
				}
				// 読込時のフィールドパラメタ
				for($idx = 0 ; $idx < count( $this->arySelFields ) ; $idx++ ) {
					$wk[] = urlencode( "fields[" . $idx . "]") . "=" . urlencodeUtf8( $this->arySelFields[$idx] );
				}
				// パラメタを結合する
				if( count($wk) == 0 ) {
					$strPara = "";
				} else {
					$strPara = implode( $wk , "&" );
				}
				$this->strCustmReq = "GET ".$this->strKGV1."records.json?". $strPara." HTTP/1.1";
				break;

			case TC_MODE_INS:	// TC_MODE_UPD にも必要。
				curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, json_encode($this->aryJson) );
			case TC_MODE_UPD:
				$this->strCustmReq = "PUT ".$this->strKGV1."records.json?app=".$this->intAppID." HTTP/1.1";
				break;

			case TC_MODE_DEL:
				if(  count($this->aryDelRecNo) > 0 ) {
					for($i = 0; $i < count($this->aryDelRecNo); $i++) {
						$this->aryDelRecNo[$i] = "&ids[$i]=".$this->aryDelRecNo[$i];
					}
					$this->strCustmReq = "DELETE ".$this->strKGV1."records.json?app=".$this->intAppID.implode($this->aryDelRecNo,"")." HTTP/1.1";
				}
				break;

			default: 
				return( $ret );
				break;
		}
*/
		$wkReq = "";
		switch( $pMode ) {
			case TC_MODE_SEL:
				$wkReq = "GET";
				$wkaj 			= new stdClass;
				$wkaj->app		= $this->intAppID;
				$wkaj->query	= $this->strQuery;
				$wkaj->fields 	= $this->arySelFields;
				$this->aryJson 	= $wkaj;
				break;

			case TC_MODE_INS:
				$wkReq = "POST";
				break;

			case TC_MODE_UPD:
				$wkReq = "PUT";
				$wkaj 			= new stdClass;
				$wkaj->app		= $this->intAppID;
				break;

			case TC_MODE_DEL:
				if(  count($this->aryDelRecNo) > 0 ) {
					$wkReq = "DELETE";
					$wkaj 		= new stdClass;
					$wkaj->app	= $this->intAppID;
					$wkaj->ids 	= $this->aryDelRecNo;
					$this->aryJson = $wkaj;
				}
				break;

			default: 
				return( $ret );
				break;
		}
		$this->strCustmReq = $wkReq." ".$this->strKGV1."records.json HTTP/1.1";

        // 2015.2.9 サイボウズバージョンアップに伴い、変更(2015.2.9 Kataoka)
		// curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $this->strCustmReq );
		curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $wkReq );
		curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, json_encode($this->aryJson) );

// ***** 2014.11.9 サイボウズバージョンアップにともない、以下３パラメータの設定を停止
// ***** 2014.11.10 CURLOPT_SSL_VERIFYPEERとCURLOPT_SSL_VERIFYHOSTはtrueで使用
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, true);
//		curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);   // 2や3を設定すると、SSLv2およびSSLv3の既知の脆弱性の影響を受けるため、デフォルトを使用するためコメント化
        curl_setopt($this->cHandler, CURLOPT_HEADER, false);
        curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);


	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// KintoneAPIを実行する（HTTP通信）
        $json = curl_exec($this->cHandler);

		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);

		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		$ret = json_decode($json);
		if ( $this->strHttpCode == 200 ) {
			switch( $pMode ) {
				case TC_MODE_SEL:
					$this->intDataCount = count( $ret->records );
					break;

				case TC_MODE_INS:
					if( array_key_exists( 'id' , $ret ) ) {
						// 1件の場合 (1件でも複数件形式で更新しているので、ここは通らない。)
						$this->intDataCount	= count( $ret->id );
						$this->strInsRecNo	= $ret->id;
					} elseif( array_key_exists( 'ids' , $ret ) ) {
						// 複数件の場合
						$this->intDataCount	= count( $ret->ids );
						$this->strInsRecNo	= $ret->ids;
					}
					break;

				case TC_MODE_UPD:
				case TC_MODE_DEL:
				default: 
					break;
			}

		} else {
			// エラー時
			$this->strKntErrId 		= $ret->id;
			$this->strKntErrCode	= $ret->code;
			$this->strKntErrMsg		= $ret->message;
		}

		return( $ret);
    }

    /*************************************************************************/
    /* ファイルダウンロード                                                  */
    /*  引数    file情報	->contentType ->fileKey ->name ->name            */
    /*  関数値  OK:filekey	NG:null                                          */
    /*************************************************************************/
    function runDOWNLOAD( $pFinfo ) {
        // URL
        $this->strURL = $this->getURL( "file" );

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;
		// 初期化とユーザ、パスワードチェック
		if( $this->parInitCOM() ) {
			//
		} else {
			return( $ret );
		}

		// ファイル情報チェック
		if( $pFinfo ) {
			//
		} else {
			return( $ret );
		}

	    /*-----------------------*/
	    /* URLパラメタを作成する */
	    /*-----------------------*/
		// ----------------
		// http通信の初期化
		// ----------------
		$ch = curl_init( $this->strURL );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }

	    /*-----------------------------------------------------------------------*/
	    /* cURLハンドラに対し共通のオプションを設定する                          */
	    /*-----------------------------------------------------------------------*/
		$wkReq = "GET";
		$wkaj 			= new stdClass;
		$wkaj->app		= $this->intAppID;
		$wkaj->fileKey 	= $pFinfo->fileKey;
		$this->aryJson 	= $wkaj;

	    /*------------------------------*/
	    /* ファイルオプションを設定する */
	    /*------------------------------*/
		$this->strCustmReq = $wkReq." ".$this->strKGV1."file.json HTTP/1.1";

        // 2015.2.9 サイボウズバージョンアップに伴い、変更(2015.2.9 Kataoka)
		// curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $this->strCustmReq );
		curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $wkReq );
		curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, json_encode($this->aryJson) );

		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, true);
//		curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);   // 2や3を設定すると、SSLv2およびSSLv3の既知の脆弱性の影響を受けるため、デフォルトを使用するためコメント化
        curl_setopt($this->cHandler, CURLOPT_HEADER, false);
        curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// KintoneAPIを実行する（HTTP通信）
        $json = curl_exec($this->cHandler);

		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);

		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		
		if ( $this->strHttpCode == 200 ) {
			// 正常
			$ret = &$json;
		} else {
			// エラー時
			$ret = json_decode($json);
			$this->strKntErrId 		= $ret->id;
			$this->strKntErrCode	= $ret->code;
			$this->strKntErrMsg		= $ret->message;
		}

		return( $ret );
    }


    /*************************************************************************/
    /* ファイルアップロード                                                  */
    /*  引数    file情報	->contentType ->fileKey ->name ->name            */
    /*  関数値  OK:filekey	NG:null                                          */
    /*************************************************************************/
    function runUPLOAD( $pFinfo , &$updata ,$pfilePath) {

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;
		$filePath = TC_UPLOAD_FOLDER;

		if($pfilePath != ""){
			$filePath = $pfilePath;
		}

		// 初期化とユーザ、パスワードチェック
		if( $this->parInitCOM() ) {
			//
		} else {
			return( $ret );
		}

		// チェック
		if( $pFinfo->fileKey == "" ) {
			return( $ret );
		}
        // URL
        $this->strURL = $this->getURL( "file" );

		// ----------------
		// http通信の初期化
		// ----------------
		$ch = curl_init( $this->strURL );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }

		// --------------------------------------
		// ｱｯﾌﾟロドをするためにファイルを書き込む
		// --------------------------------------
		file_put_contents( $filePath.$pFinfo->name , $updata );

	    /*-----------------------------------------------------------------------*/
	    /* cURLハンドラに対し共通のオプションを設定する                          */
	    /*-----------------------------------------------------------------------*/
        // ユーザ認証用
		$strIdPass = $this->strUser . ":" . $this->strPasswd;
        $authToken = base64_encode($strIdPass);
        // BASIC認証用
        $strIdPassBasic = $this->strAuthUser . ":" . $this->strAuthPasswd;
        $authTokenBasic = base64_encode($strIdPassBasic);

		// トークンAPIが設定されていない場合とでヘッダを変える
		if( $this->strToken == "" ){
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"X-Cybozu-Authorization: $authToken", 
				"Authorization: basic $authTokenBasic"
			);
		}else{
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"X-Cybozu-API-Token: $this->strToken", 
				"X-Cybozu-Authorization: ", 
				"Authorization: basic $authTokenBasic"
			);
		}

	    /*------------------------------*/
	    /* オプションを設定する */
	    /*------------------------------*/
	    // 20180611(m.t)
	    // php5.3->5.6にバージョンアップに伴い以下指定では動かなくなったため、ファイルのアップロード時はCURLFileを利用する
		//$file_array = array ( "file" => "@".$filePath.$pFinfo->name.";filename=".$pFinfo->name.";type=".$pFinfo->contentType );
		$cfile = new CURLFile( $filePath.$pFinfo->name, $pFinfo->contentType, $pFinfo->name);
		$file_array = array('file' => $cfile);

		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, true);
//		curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);   // 2や3を設定すると、SSLv2およびSSLv3の既知の脆弱性の影響を受けるため、デフォルトを使用するためコメント化
		curl_setopt($this->cHandler, CURLOPT_HEADER, false);
		curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->cHandler, CURLOPT_FAILONERROR, false);
		//
		curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, $file_array);
		curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// KintoneAPIを実行する（HTTP通信）
        $json = curl_exec($this->cHandler);

		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);

		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		$ret = json_decode($json);
		if ( $this->strHttpCode == 200 ) {
			//
		} else {
			// エラー時
			$this->strKntErrId 		= $ret->id;
			$this->strKntErrCode	= $ret->code;
			$this->strKntErrMsg		= $ret->message;
		}

		return( $ret);
    }

    /*************************************************************************/
    /* ファイルアップロード                                                  */
    /*  引数    file情報	->contentType ->fileKey ->name ->name            */
    /*  関数値  OK:filekey	NG:null                                          */
    /*************************************************************************/
    function runUPLOAD_NEWINS( $pName ,$pfilePath) {

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;

		// 初期化とユーザ、パスワードチェック
		if( $this->parInitCOM() ) {
			//
		} else {
			return( $ret );
		}

        // URL
        $this->strURL = $this->getURL( "file" );

		// ----------------
		// http通信の初期化
		// ----------------
		$ch = curl_init( $this->strURL );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }

	    /*-----------------------------------------------------------------------*/
	    /* cURLハンドラに対し共通のオプションを設定する                          */
	    /*-----------------------------------------------------------------------*/
        // ユーザ認証用
		$strIdPass = $this->strUser . ":" . $this->strPasswd;
        $authToken = base64_encode($strIdPass);
        // BASIC認証用
        $strIdPassBasic = $this->strAuthUser . ":" . $this->strAuthPasswd;
        $authTokenBasic = base64_encode($strIdPassBasic);

		// トークンAPIが設定されていない場合とでヘッダを変える
		if( $this->strToken == "" ){
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"X-Cybozu-Authorization: $authToken", 
				"Authorization: basic $authTokenBasic"
			);
		}else{
	        // ヘッダパラメタ追加作成
			$this->aryOptionHeader = array(
				"X-Cybozu-API-Token: $this->strToken", 
				"X-Cybozu-Authorization: ", 
				"Authorization: basic $authTokenBasic"
			);
		}

	    /*------------------------------*/
	    /* オプションを設定する */
	    /*------------------------------*/
	    // 20180611(m.t)
	    // php5.3->5.6にバージョンアップに伴い以下指定では動かなくなったため、ファイルのアップロード時はCURLFileを利用する
		//$file_array = array ( "file" => "@".$pfilePath.$pName.";filename=".$pName.";type=image/jpeg" );
		$cfile = new CURLFile( $pfilePath.$pName, "image/jpeg", $pName);
		$file_array = array('file' => $cfile);

		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, true);
//		curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);   // 2や3を設定すると、SSLv2およびSSLv3の既知の脆弱性の影響を受けるため、デフォルトを使用するためコメント化
		curl_setopt($this->cHandler, CURLOPT_HEADER, false);
		curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->cHandler, CURLOPT_FAILONERROR, false);
		//
		curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, $file_array);
		curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// KintoneAPIを実行する（HTTP通信）
        $json = curl_exec($this->cHandler);

		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);

		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		$ret = json_decode($json);
		if ( $this->strHttpCode == 200 ) {
			//
		} else {
			// エラー時
			$this->strKntErrId 		= $ret->id;
			$this->strKntErrCode	= $ret->code;
			$this->strKntErrMsg		= $ret->message;
		}

		return( $ret);
    }

}
?>
