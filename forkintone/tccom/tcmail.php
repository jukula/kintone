<?php
/*****************************************************************************/
/* メール送信クラス                                          (Version 1.07a) */
/*   クラス名   : tcMail                                                     */
/*   メンバ変数 : $sFrom          // From                                    */
/*                $sTo            // To                                      */
/*                $sTitle         // Title                                   */
/*                $sBody          // メール本文                              */
/*                $m_sHeader      // ヘッダ情報                              */
/*   メソッド   : sendMail        // メール送信                              */
/*   更新履歴   2005/09/03      Version 1.01(Jun.K)                          */
/*                              MAIL_FROM_JP 追加                            */
/*                              setBody にてコンバート処理追加               */
/*                              デバグ時の出力文字列整形                     */
/*              2005/09/23      Version 1.02(Jun.K)                          */
/*                              文字コード判定処理追加                       */
/*              2005/10/19      Version 1.03(Jun.K)                          */
/*                              setTitle の処理を変更(下位互換なし)          */
/*              2005/10/27      Version 1.04(Jun.K)                          */
/*                              setTitle の処理を変更(1.03互換に戻した)      */
/*              2006/09/06      Version 1.05(Jun.K)                          */
/*                              ログ出力を行うようにした                     */
/*              2009/07/02      Version 1.06(Jun.K)                          */
/*                              clearTo 処理追加                             */
/*              2015/06/21      Version 1.07(Jun.K)                          */
/*                              機種依存文字変換、ログへの送信先出力追加     */
/*              2016/11/17      Version 1.07a(Jun.K)                         */
/*                              addToメソッドについて改修対応                */
/*   [必要ファイル]                                                          */
/*      TcError.php    エラークラス                                          */
/*                                                                           */
/*                               Copyright(C)2003-2015 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
require_once("defmail.inc");
require_once("tcutility.inc");
require_once("tcerror.php");
include_once("tclog.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////
define("MAIL_TOOL" , "TC-MailSystem v1.07");

//define("MAIL_DEBUG", true);

///////////////////////////////////////////////////////////////////////////////
// クラス定義
///////////////////////////////////////////////////////////////////////////////
class tcMail
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $sFrom     = ""  ;
    var $sTo       = ""  ;
    var $sReply    = ""  ;
    var $sTitle    = ""  ;
    var $sBody     = ""  ;
    var $sFooter   = ""  ;
    var $m_sHeader = ""  ;
    var $jFrom     = ""  ;
    var $aryCc     = null;  // 未使用
    var $aryBcc    = null;  // 未使用
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function tcMail() {
        global $gMailFooter;

        mb_language("ja");
//        if (mb_internal_encoding() != "EUC-JP") {
////            mb_language("ja");
//            mb_internal_encoding("EUC-JP");
//        }
        mb_internal_encoding("UTF-8");

//        $this->sFrom     = MAIL_FROM;
        if (MAIL_FROM_JP == "") $this->sFrom     = MAIL_FROM;
        else $this->setFrom(MAIL_FROM, MAIL_FROM_JP);
        $this->sTo       = MAIL_TO;
        $this->sTitle    = mb_encode_mimeheader( MAIL_TITLE );
        $this->sFooter   = $gMailFooter;
        $this->m_sHeader = "X-Mailer:" . MAIL_TOOL . "\n";
//                         . "Return-Path: " . MAIL_FROM . "\n"
//                         . "Content-Transfer-Encoding:7bit\n"
//                         . "Content-Type:text/plain\n";
        $this->err = new TcError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/
    function setFrom( $arg, $pJapan = "" ) {
        $this->sFrom = $arg;
        if($pJapan != "") {
            $this->jFrom = mb_encode_mimeheader($pJapan);
//            $this->sFrom = mb_encode_mimeheader($pJapan);
//            $this->jFrom = " <" . $arg . ">";
        }
    }
    function getFrom() {
        if ($this->jFrom != "") {
            return( $this->jFrom . " <" . $this->sFrom . ">" );
        } else {
            return( $this->sFrom );
        }
//        return( $this->sFrom );
    }

    function setTo( $arg ) {
        $this->sTo = $arg;
    }
    function getTo() {
        return( $this->sTo );
    }
    function addTo( $arg ) {
        if( is_array($this->sTo) ) {
            $this->sTo[] = $arg;
        } else {
            if(isNull($this->sTo)) {
                $this->sTo = array($arg);
            } else {
                $this->sTo = array($this->sTo, $arg);
            }
        }
    }
    function clearTo() {
        $this->sTo = "";
    }

    function setReply( $arg ) {
        $this->sReply = $arg;
    }
    function getReply() {
        return( $this->sReply );
    }

    function addCc( $arg ) {
        if( is_array($this->aryCc) ) {
            $this->aryCc[] = $arg;
        } else {
            $this->aryCc = array($arg);
        }
    }

    function addBcc( $arg ) {
        if( is_array($this->aryBcc) ) {
            $this->aryBcc[] = $arg;
        } else {
            $this->aryBcc = array($arg);
        }
    }

    function setTitle( $arg, $opt = 0 ) {
        // 通常エンコード処理(ただし文字数によっては処理しない
        if (mb_strwidth($arg) > 16)
            $this->sTitle = $arg;
        else
            $this->sTitle = mb_encode_mimeheader($arg);

        // $opt指定時はエンコードしない
        if( $opt == 1 )
            $this->sTitle = $arg;
//            $this->sTitle = mb_encode_mimeheader( $arg, "ISO-2022-JP", "B", "\r\n" );
    }
    function getTitle() {
        return( $this->sTitle );
    }

    function setBody($arg, $opt = false) {
        $this->sBody = $arg;
        if ($opt) {
            $this->sBody = mb_convert_kana($arg, "KV");
        }
    }
    function getBody() {
        return( $this->sBody );
    }

    /*************************************************************************/
    /* メールフッタ文字列                                                    */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function setFooter( $arg ) {
        $this->sFooter = $arg;
    }
    function getFooter() {
        return( $this->sFooter );
    }

    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* メール送信                                                            */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function sendMail( $pFooter = true, $pMD = true ) {
        $ret = true;
        // メールヘッダ設定
        $sHeader = "From: " . $this->getFrom() . "\n"
                 . $this->m_sHeader;
//                 . $this->m_sHeader . "\n";
//        $sHeader = "From: " . $this->sFrom . $this->jFrom . "\n"
//                  . $this->m_sHeader;
//        $addInfo = "-f" . MAIL_FROM;
        if( $this->sReply <> "") {
            $addInfo = "-f" . $this->sReply;
        } else {
            $addInfo = "-f" . MAIL_FROM;
        }
        // 本文設定
        $strMess = $this->sBody . (( $pFooter ) ? $this->sFooter : "");
        // 機種依存文字変換
        if ($pMD) {
            $strMess = replaceMDText($strMess, 1);
        }
        // CCヘッダ追加
        $sCc = "";
        if( is_array($this->aryCc) ) {
            foreach( $this->aryCc as $val ) {
                $sCc .= ($sCc == "" ? "" : ",") . $val;
            }
        }
        // BCCヘッダ追加
        $addBcc = "";
        if( is_array($this->aryBcc) ) {
            foreach( $this->aryBcc as $val ) {
                $addBcc .= (($addBcc == "") ? "Bcc: " : ",") . $val;
            }
        }
        // メール送信
        if( is_array($this->sTo) ) {
            $sTo  = "";
            $sBcc = "";
            $numBcc = 0;
            foreach ( $this->sTo as $key=>$val ) {
                if( MAIL_DEBUG ) {
                    print "send mail! => $val<br>";
                } else {
                    // To未設定の場合はToに設定
                    if($sTo == "") {
                        $sTo = $val;
                    // To設定済みの場合は、Bcc設定
                    } else {
                        $sTo .= ",$val";
//                        $sBcc .= "Bcc:" . $val . "\n";
                        $numBcc ++;
                    }
                    // １回目だけ、CC設定
                    if($sCc <> "") {
                        $addHeader = "Cc: " . $sCc . "\n";
                        $sCc = "";
//                    } else {
//                        $addHeader = "";
                    }
                    // １回目だけ、BCC設定
                    if($addBcc <> "") {
                        $sHeader .= $addBcc . "\n";
                        $addBcc = "";
                    }
                    // 一度の送信するMAXを超えた場合、いったん送信
                    if($numBcc > BCC_MAX) {
                        $ret = mb_send_mail($sTo, $this->sTitle, $strMess,
                                            $sHeader . $sBcc . $addHeader, $addInfo);
                        $sTo  = "";
                        $sBcc = "";
                        $numBcc = 0;
                        if($addHeader != "") {
                            $sCc = "";
                            $addHeader = "";
                        }
                    }
                }
                if( !$ret ) {
                    // 送信エラー
                    $this->err->setError( ERR_M000, $val );
                }
            }
            // 未送信分を送出
            if($sTo <> "") {
                $ret = mb_send_mail($sTo, $this->sTitle, $strMess,
                                    $sHeader . $sBcc . $addHeader, $addInfo);
                if( !$ret ) {
                    // 送信エラー
                    $this->err->setError( ERR_M000, $val );
                }
            }
        } else {
//            if($sCc <> "") $sCc = "Cc: " . $sCc . "\n";
            if($sCc <> "")    $sHeader .= "Cc: " . $sCc . "\n";
            if($addBcc <> "") $sHeader .= $addBcc . "\n";
            if( MAIL_DEBUG ) {
                print "send mail! => $this->sTo<br>";
            } else {
                $ret = mb_send_mail($this->sTo, $this->sTitle, $strMess,
                                    $sHeader, $addInfo);
//                                    $sHeader . $sCc, $addInfo);
            }
            if( !$ret ) {
                // 送信エラー
                $this->err->setError( ERR_M000, $this->sTo );
            }
        }
        // DEBUG情報
        if( MAIL_DEBUG ) {
            print "【メールヘッダ】<br>\n" . nl2br($sHeader) . "<br>\n";
            print "【メール件名】<br>\n$this->sTitle<br>\n";
            print "【メール本文】<br>\n" . nl2br($strMess) . "<br>\n";
        }

        // ログ出力
        if (MAIL_LOG) {
            $strLog = "----------------------------------------------\n"
                    . "【メールヘッダ】\n" . $sHeader . "\n"
                    . "【メール送信先】\n" . $this->sTo . "\n"
                    . "【メール件名】\n" . $this->sTitle . "\n"
                    . "【メール本文】\n" . $strMess . "\n";
            $log = new TcLog("tcmail.log");
            $log->writeLog(str_replace("\n", "\r\n", $strLog));
        }

        return( $ret );
    }

}
?>
