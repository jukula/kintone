<?php
/*****************************************************************************/
/*  kintone連携クラス                                         (Version 1.00) */
/*   クラス名       TcKintone                                                */
/*   メンバ変数     strDomain     	ドメイン                                 */
/*                  strUser       	ユーザ名                                 */
/*                  strPasswd     	パスワード                               */
/*                  strAuthUser   	認証ユーザ                               */
/*                  strAuthPasswd 	認証パスワード                           */
/*                  strDL         	ダウンロード先                           */
/*                  strURL        	URL                                      */
/*                  intAppID      	アプリID                                 */
/*                  strQuery   		クエリパラメータ                         */
/*                  arySelFields  	フィールドパラメータ                     */
/*                  aryJson			レコード追加、更新用データ(json形式)     */
/*                  aryDelRecNo   	削除対象のレコード番号                   */
/*                  strCustmReq		カスタムリクエスト                       */
/*                  strContentType	Content-Type (CURLOPT_HTTPHEADER へ追加) */
/*                  cHandler      	cURLハンドル                             */
/*                  aryOptionHeader	CURLOPT_HTTPHEADER                       */
/*                  intDataCount 	取得したデータのカウント                 */
/*                  strHttpCode    	Kintoneからの戻り値                      */
/*                  err           	エラー内容                               */
/*   メンバ関数     getURL          プロパティからURLを生成する              */
/*   メソッド       parInit         プロパティを初期化する                   */
/*                  runCURLEXEC     http通信を実行する                       */
/*   必要ファイル                                                            */
/*      defkintone.inc / tcutility.inc / tcerror.php                         */
/*   作成日         2013/04/15                                               */
/*   更新履歴       2013/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
include_once("defkintone.inc");
include_once("tcutility.inc");
include_once("tcerror.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////

class TcKintone
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $strDomain     	= TC_CY_DOMAIN   ; // ドメイン
    var $strUser       	= TC_CY_USER     ; // ユーザ名
    var $strPasswd     	= TC_CY_PASSWORD ; // パスワード
    var $strAuthUser   	= TC_CY_AUTH_USER; // 認証ユーザ
    var $strAuthPasswd 	= TC_CY_AUTH_PASS; // 認証パスワード
    var $strDL         	= ""             ; // ダウンロード先
    var $strURL        	= ""             ; // URL

    var $intAppID      	= 0             ; // アプリID
	// SEL
    var $strQuery   	= ""            ; // クエリパラメータ
    var $arySelFields  	= array()       ; // フィールドパラメータ
    // INS,UPD
	var $aryJson		= array()       ; // レコード追加、更新用データ(json形式)
	// DEL
    var $aryDelRecNo   	= array()       ; // 削除対象のレコード番号
	var $strCustmReq	= ""			; // カスタムリクエスト
	// 添付ファイル
	var $strContentType	= ""            ; // Content-Type (CURLOPT_HTTPHEADER へ追加)
	
	
    var $cHandler      	= null          ; // cURLハンドル
    var $aryOptionHeader				; // CURLOPT_HTTPHEADER
	var $intDataCount 	= 0				; // 取得したデータのカウント
	var $strHttpCode                    ; // Kintoneからの戻り値(Httpレスポンス)
	var $strKintoneId                 	; // Kintoneからの戻り値(ID)：エラー時のみ
	var $strKintoneCode                 ; // Kintoneからの戻り値(CODE)：エラー時のみ
	var $strKintoneMsg                 	; // Kintoneからの戻り値(MESSAGE)：エラー時のみ
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function TcKintone() {
        // URL
        $this->strURL = $this->getURL();
        $this->err = new TcError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/

    /*************************************************************************/
    /* URLを取得する                                                         */
    /*  引数    なし                                                         */
    /*  関数値  string      正常:URL文字列 / NG:""                           */
    /*************************************************************************/
    function getURL() {
        // DOMAINが設定されてなければ、""
        if( isNull($this->strDomain) ) {
			return("");
		} else {
        	return( "https://" . $this->strDomain . "/k/v1/records.json" );
		}
    }


    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* API連携用のパラメタを初期化する                                       */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function parInit() {
	    $this->intAppID      	= 0             ; // アプリID
	    $this->strQuery      	= ""            ; // クエリパラメータ
	    $this->arySelFields     = array()       ; // フィールドパラメータ
		$this->aryJson			= array()       ; // レコード追加、更新用データ(json形式)
	    $this->aryDelRecNo   	= array()       ; // 削除対象のレコード番号
		$this->strCustmReq		= ""			; // カスタムリクエスト
	    $this->strContentType 	= ""            ; // ヘッダパラメータ(CURLOPT_HTTPHEADER へ追加)
    }

    /*************************************************************************/
    /* cURLハンドラに対し実行する                                            */
    /*  引数    API連携モード                                                */
    /*  関数値  配列     OK:Json形式 / NG:null                               */
    /*************************************************************************/
    function runCURLEXEC( $pMode ) {

	    /*----------------*/
	    /* 戻り値の初期化 */
	    /*----------------*/
        $ret = null;
		$this->strKintoneId 	= "";
		$this->strKintoneCode   = "";
		$this->strKintoneMsg    = "";

	    /*-----------------------*/
	    /* URLパラメタを作成する */
	    /*-----------------------*/
		$strPara	= "";
		$wk	 		= array();
		// アプリID
		$wk[] = "app=".$this->intAppID;
		// クエリパラメタ
		if( isNull($this->strQuery) ) {
			//
		} else {
			$wk[] = "query=".urlencodeUtf8( $this->strQuery );
		}
		// 読込時のフィールドパラメタ
		for($idx = 0 ; $idx < count( $this->arySelFields ) ; $idx++ ) {
			$wk[] = urlencode( "fields[" . $idx . "]") . "=" . urlencodeUtf8( $this->arySelFields[$idx] );
		}
		// パラメタを結合する
		if( count($wk) == 0 ) {
			$strPara = "";
		} else {
			$strPara = implode( $wk , "&" );
		}

		// ----------------
		// http通信の初期化
		// ----------------
		$ch = curl_init( $this->strURL ."?". $strPara );
        if($ch) {
            $this->cHandler = $ch;
            $ret = true;
        } else {
            $this->err->setError( ERR_T000 );
        }

	    /*-----------------------------------------------------------------------*/
	    /* cURLハンドラに対し共通のオプションを設定する                          */
	    /*-----------------------------------------------------------------------*/

        // ログインIDおよびパスワード
        if(isNull($this->strUser)) {
            $this->err->setError( ERR_V200 );
            return($ret);
        }
        if(isNull($this->strPasswd)) {
            $this->err->setError( ERR_V200 );
            return($ret);
        }
        $strIdPass = $this->strUser . ":" . $this->strPasswd;
        $authToken = base64_encode($strIdPass);

        // BASIC認証用
        $strIdPassBasic = $this->strAuthUser . ":" . $this->strAuthPasswd;
        $authTokenBasic = base64_encode($strIdPassBasic);

        // ヘッダパラメタ追加作成
		$this->aryOptionHeader = array(
			"Host: ".$this->strDomain.":443" , 
			"X-Cybozu-Authorization: $authToken", 
			"Authorization: basic $authTokenBasic"
		);
		
		if( isNull($this->strContentType) ) {
			//
		} else {
			$this->aryOptionHeader[] = $this->strContentType;
		}

		// ------------------------
		// モード別のオプション設定
		// ------------------------
		switch( $pMode ) {
			case TC_MODE_SEL:
				break;

			case TC_MODE_UPD:
				$this->strCustmReq = "PUT /k/v1/records.json?app=".$this->intAppID." HTTP/1.1";
				curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $this->strCustmReq );

			case TC_MODE_INS:	// TC_MODE_UPD にも必要。
				curl_setopt($this->cHandler, CURLOPT_POSTFIELDS, json_encode($this->aryJson) );

				break;

			case TC_MODE_DEL:
				if(  count($this->aryDelRecNo) > 0 ) {
					for($i = 0; $i < count($this->aryDelRecNo); $i++) {
						$this->aryDelRecNo[$i] = "&ids[$i]=".$this->aryDelRecNo[$i];
					}
					$this->strCustmReq = "DELETE /k/v1/records.json?app=".$this->intAppID.implode($this->aryDelRecNo,"")." HTTP/1.1";
					curl_setopt($this->cHandler, CURLOPT_CUSTOMREQUEST , $this->strCustmReq );
				}
				break;

			default: 
				break;
		}

        curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->cHandler, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->cHandler, CURLOPT_SSLVERSION, 3);
        curl_setopt($this->cHandler, CURLOPT_HEADER, false);
        curl_setopt($this->cHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->cHandler, CURLOPT_FAILONERROR, true);
        curl_setopt($this->cHandler, CURLOPT_HTTPHEADER, $this->aryOptionHeader);

	    /*-----------------------------------------------------------------------*/
	    /* http通信を実行する                                                    */
	    /*-----------------------------------------------------------------------*/
		// 取得件数を初期化
		$this->intDataCount = 0;

		// KintoneAPIを実行する（HTTP通信）
        $json = curl_exec($this->cHandler);

		// HTTPレスポンスのステータスコードを標準出力する
		$this->strHttpCode = curl_getinfo($this->cHandler , CURLINFO_HTTP_CODE);

		// セッションを閉じ、全てのリソースを開放します。 cURL ハンドル ch も削除されます。
		curl_close($this->cHandler);

		// ステータスコードが「200」なら、レスポンスボディ（JSONデータ）を標準出力する
		if ( $this->strHttpCode == 200 ) {
			// jsonからPHP配列に変換
			$ret = json_decode($json);
			$this->intDataCount = count( $ret->records );
		} else {
			// エラー時
			$msg = json_decode($json);
			$this->strKintoneId 	= $msg['id'];
			$this->strKintoneCode   = $msg['code'];
			$this->strKintoneMsg    = $msg['message'];
		}

		return( $ret);
    }

}
?>
