<?php
/*****************************************************************************/
/* いとにんげんドットコム  メール送信クラス                   (Version 1.06) */
/*   クラス名   : tcAlert                                                    */
/*   メンバ変数 : $sFrom          // From                                    */
/*                $sTo            // To                                      */
/*                $sTitle         // Title                                   */
/*                $sBody          // メール本文                              */
/*                $m_sHeader      // ヘッダ情報                              */
/*   メソッド   : sendMail        // メール送信                              */
/*   更新履歴   2005/09/03      Version 1.01(Jun.K)                          */
/*                              MAIL_FROM_JP 追加                            */
/*                              setBody にてコンバート処理追加               */
/*                              デバグ時の出力文字列整形                     */
/*              2005/09/23      Version 1.02(Jun.K)                          */
/*                              文字コード判定処理追加                       */
/*              2005/10/19      Version 1.03(Jun.K)                          */
/*                              setTitle の処理を変更(下位互換なし)          */
/*              2005/10/27      Version 1.04(Jun.K)                          */
/*                              setTitle の処理を変更(1.03互換に戻した)      */
/*              2006/09/06      Version 1.05(Jun.K)                          */
/*                              ログ出力を行うようにした                     */
/*              2009/07/02      Version 1.06(Jun.K)                          */
/*                              clearTo 処理追加                             */
/*   [必要ファイル]                                                          */
/*      TcError.php    エラークラス                                         */
/*                                                                           */
/*                                   Copyright(C)2003-2009 konpeito-net.com  */
/*****************************************************************************/
require_once("defmail.inc");
require_once("tcutility.inc");
require_once("tcerror.php");
//include_once("tclog.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////
define("MAIL_TOOL" , "Konpeito-net-MailSystem v1.06");


///////////////////////////////////////////////////////////////////////////////
// クラス定義
///////////////////////////////////////////////////////////////////////////////
class tcAlert
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $sFrom     = ""  ;
    var $sTo       = ""  ;
    var $sTitle    = ""  ;
    var $sBody     = ""  ;
    var $sFooter   = ""  ;
    var $m_sHeader = ""  ;
    var $jFrom     = ""  ;
    var $aryCc     = null;  // 未使用
    var $aryBcc    = null;  // 未使用
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function tcAlert() {
        global $gMailFooter;

        mb_language("ja");
//        if (mb_internal_encoding() != "EUC-JP") {
////            mb_language("ja");
//            mb_internal_encoding("EUC-JP");
//        }
        mb_internal_encoding("UTF-8");

//        $this->sFrom     = MAIL_FROM;
        if (MAIL_FROM_JP == "") $this->sFrom     = MAIL_FROM;
        else $this->setFrom(MAIL_FROM, MAIL_FROM_JP);
        $this->sTo       = MAIL_TO;
        $this->sTitle    = mb_encode_mimeheader( MAIL_TITLE );
        $this->sFooter   = $gMailFooter;
        $this->m_sHeader = "X-Mailer:" . MAIL_TOOL . "\n";
//                         . "Return-Path: " . MAIL_FROM . "\n"
//                         . "Content-Transfer-Encoding:7bit\n"
//                         . "Content-Type:text/plain\n";
        $this->err = new TcError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/
    function setFrom( $arg, $pJapan = "" ) {
        $this->sFrom = $arg;
        if($pJapan != "") {
            $this->sFrom = mb_encode_mimeheader($pJapan);
            $this->jFrom = " <" . $arg . ">";
        }

    }
    function getFrom() {
        return( $this->sFrom );
    }

    function setTo( $arg ) {
        $this->sTo = $arg;
    }
    function getTo() {
        return( $this->sTo );
    }
    function addTo( $arg ) {
        if( is_array($this->sTo) ) {
            $this->sTo[] = $arg;
        } else {
            if(isNull($this->sTo)) {
                $this->sTo = array($arg);
            } else {
                $this->sTo = array($this->sTo, $arg);
            }
        }
    }
    function clearTo() {
        $this->sTo = "";
    }

    function addCc( $arg ) {
        if( is_array($this->aryCc) ) {
            $this->aryCc[] = $arg;
        } else {
            $this->aryCc = array($arg);
        }
    }

    function addBcc( $arg ) {
        if( is_array($this->aryBcc) ) {
            $this->aryBcc[] = $arg;
        } else {
            $this->aryBcc = array($arg);
        }
    }

    function setTitle( $arg, $opt = 0 ) {
        // 通常エンコード処理(ただし文字数によっては処理しない
        if (mb_strwidth($arg) > 16)
            $this->sTitle = $arg;
        else
//            $this->sTitle = mb_encode_mimeheader($arg);
            $this->sTitle = ($arg);
        // $opt指定時はエンコードしない
        if( $opt == 1 )
            $this->sTitle = $arg;
//            $this->sTitle = mb_encode_mimeheader( $arg, "ISO-2022-JP", "B", "\r\n" );
    }
    function getTitle() {
        return( $this->sTitle );
    }

    function setBody($arg, $opt = false) {
        $this->sBody = $arg;
        if ($opt) {
            $this->sBody = mb_convert_kana($arg, "KV");
        }
    }
    function getBody() {
        return( $this->sBody );
    }

    /*************************************************************************/
    /* メールフッタ文字列                                                    */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function setFooter( $arg ) {
        $this->sFooter = $arg;
    }
    function getFooter() {
        return( $this->sFooter );
    }

    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* メール送信                                                            */
    /*  引数    なし                                                         */
    /*  関数値  なし                                                         */
    /*************************************************************************/
    function sendMail( $pFooter = true, $pMD = true ,$pBcc) {
        $ret = true;
        // メールヘッダ設定
        $sHeader = "From: " . $this->sFrom . $this->jFrom . "\n"
                  . $this->m_sHeader;

		if($pBcc != ""){
        	$addInfo = "-f" . $pBcc;
		}else{
        	$addInfo = "-f" . MAIL_FROM;
		}

        // 本文設定
        $strMess = $this->sBody . (( $pFooter ) ? $this->sFooter : "");
        // 機種依存文字変換
        if ($pMD) {
            $strMess = replaceMDText($strMess, 1);
        }

		if($pBcc != "" ){
			$pBcc = "Bcc:" . $pBcc . "\n";
		}

        // メール送信
        if( is_array($this->sTo) ) {
            $sTo  = "";
            $sBcc = "";
            $numBcc = 0;
            foreach ( $this->sTo as $key=>$val ) {
                if( MAIL_DEBUG ) {
                    print "send mail! => $val<br>";
                } else {
                    // To未設定の場合はToに設定
                    if($sTo == "") {
                        $sTo = $val;
                    // To設定済みの場合は、Bcc設定
                    } else {
                        $sBcc .= "Bcc:" . $val . "\n";
                        $numBcc ++;
                    }
                    // 一度の送信するMAXを超えた場合、いったん送信
                    if($numBcc > BCC_MAX) {
						if($pBcc != "" ){
                      		$ret = mb_send_mail($sTo, $this->sTitle, $strMess,
                        	 	                $sHeader . $pBcc, $addInfo);
						}else{
	                    	$ret = mb_send_mail($sTo, $this->sTitle, $strMess,
                                          		$sHeader . $sBcc, $addInfo);
						}

                        $sTo  = "";
                        $sBcc = "";
                        $numBcc = 0;
                    }
                }
                if( !$ret ) {
                    // 送信エラー
                    $this->err->setError( ERR_M000, $val );
                }
            }
            // 見送信分を送出
            if($sTo <> "") {

				if($pBcc != "" ){
	                $ret = mb_send_mail($sTo, $this->sTitle, $strMess,
                                 	    $sHeader . $pBcc, $addInfo);
				}else{
	                $ret = mb_send_mail($sTo, $this->sTitle, $strMess,
     	                                $sHeader . $sBcc, $addInfo);
				}

                if( !$ret ) {
                    // 送信エラー
                    $this->err->setError( ERR_M000, $val );
                }
            }
        } else {
            if( MAIL_DEBUG ) {
                print "send mail! => $this->sTo<br>";
            } else {
				if($pBcc != "" ){
 	                $ret = mb_send_mail($this->sTo, $this->sTitle, $strMess,
          	                            $sHeader . $pBcc, $addInfo);
				}else{
 	                $ret = mb_send_mail($this->sTo, $this->sTitle, $strMess,
          	                            $sHeader, $addInfo);
				}
            }
            if( !$ret ) {
                // 送信エラー
                $this->err->setError( ERR_M000, $this->sTo );
            }
        }
        // DEBUG情報
        if( MAIL_DEBUG ) {
            print "【メールヘッダ】<br>\n" . nl2br($sHeader) . "<br>\n";
            print "【メール件名】<br>\n$this->sTitle<br>\n";
            print "【メール本文】<br>\n" . nl2br($strMess) . "<br>\n";
        }
/*
        // ログ出力
        if (MAIL_LOG) {
            $strLog = "----------------------------------------------\n"
                    . "【メールヘッダ】\n" . $sHeader . "\n"
                    . "【メール送信先】\n" . $this->sTo . "\n"
                    . "【メール件名】\n" . $this->sTitle . "\n"
                    . "【メール本文】\n" . $strMess . "\n";
            $log = new TcLog("tcAlert.log");
            $log->writeLog(str_replace("\n", "\r\n", $strLog));
        }
*/
        return( $ret );
    }

}
?>
