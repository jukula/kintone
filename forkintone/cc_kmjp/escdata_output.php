<?php
/*****************************************************************************/
/* 帳票出力PHP                                                (Version 1.00) */
/*   ファイル名 : escdata_output.php                                         */
/*   更新履歴   2015/08/11  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Access-Control-Allow-Origin: *");
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("tcdef.inc");
	include_once("defkintoneconf.inc");
	include_once("../tccom/tcutility.inc");
	include_once("../tccom/tcerror.php");
	include_once("../tccom/tckintone.php");
	include_once("../tccom/tckintonerecord.php");
	include_once("../tccom/tckintonecommon.php");

	require_once '../Classes/PHPExcel/IOFactory.php';

	/*****************************************************************************/
	/* 開始                                                                      */
	/*****************************************************************************/
	$clsSrs = new TcKykSyoAnken();
	
	$clsSrs->paraAnkenID = $_REQUEST['rcno'] - 0;


// 実行
	$clsSrs->main();

	/*****************************************************************************/
	/* クラス定義：メイン                                                        */
	/*****************************************************************************/
	class TcKykSyoAnken
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $paraAnkenID		= null; 	// 案件レコード番号（パラメタ）
		var $err;
		var $common;
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcKykSyoAnken() {
	        $this->err = new TcError();
	        $this->common = new TcKintoneCommon();
	    }

		/*************************************************************************/
	    /* メインの処理を実行する                                                */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function main() {
			$msg     = "";
		    $rowdata = array();

			// 架電入力
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_KDNR;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "レコード番号 = ".$this->paraAnkenID; // クエリパラメータ
			$jsonKDNR = $k->runCURLEXEC( TC_MODE_SEL );

			// 顧客マスタ
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_KKKM;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

		    $k->strQuery = "会社統一コード = ".$jsonKDNR->records[0]->会社統一コード->value; // クエリパラメータ
			$jsonKKKM = $k->runCURLEXEC( TC_MODE_SEL );

			// 設置機器
			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= TC_APPID_CC_STKK;			// アプリID
		    $k->arySelFields	= array(); 					// 読込用フィールドパラメータ

			$k->strQuery = "品目CD_機番 = \"".$jsonKDNR->records[0]->品目CD_機番->value."\""; // クエリパラメータ
			$jsonSTKK = $k->runCURLEXEC( TC_MODE_SEL );

			// エクセルのテンプレートの準備
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load("templates/esc.xlsx");

			// セルへ設定
			if( $k->intDataCount > 0 ) {
				$msg = $this->setCell( $objPHPExcel , $jsonKDNR->records[0] , $jsonKKKM->records[0] , $jsonSTKK->records[0]);
			}

			// ダウンロード用エクセルを準備
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			// ファイル名生成
			list($msec, $sec) = explode(" ", microtime());
			$saveName = "エスカレーションCSTシート(".date('YmdHi').").xlsx";
			// ダウンロード用エクセルを保存
			$objWriter->save("tctmp/".$saveName);
			$saveurl = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName;

			echo '<li><a href="' .$saveurl. '" target="_blank">帳票データのダウンロードはこちら</a>（エクセル形式）</li><br>';
			echo $msg;
		}

		/*************************************************************************/
	    /* データをセルへ設定する。                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setCell( &$pPHPExcel , $pDatKDNR , $pDatKKKM , $pDatSTKK) {
			$ret = "";

			// 1シート目ヘッダー処理
			$pPHPExcel	->setActiveSheetIndex( 0 )
			            ->setCellValue('A1'		,	"社外秘　".$pDatKDNR->エクセルタイトル->value	)
			            ->setCellValue('A3'		,	$pDatKDNR->営業担当者->value	)
			            ->setCellValue('H7'		,	$pDatKDNR->旧簡易No->value	)
			            ->setCellValue('H8'		,	$pDatKDNR->旧エスカレーションNo->value	)
			            ->setCellValue('H9'		,	$pDatKDNR->引渡し日->value	)
			            ->setCellValue('H10'	,	$pDatKDNR->架電日->value	)
			            ->setCellValue('C11'	,	$pDatKDNR->その他->value	)
			            ->setCellValue('D13'	,	$pDatSTKK->会社名１->value	)
			            ->setCellValue('D14'	,	"〒".$pDatKDNR->設置先郵便番号->value	)
			            ->setCellValue('F14'	,	$pDatKDNR->設置先住所1->value	)
			            ->setCellValue('D15'	,	$pDatKDNR->設置先住所2->value	)
			            ->setCellValue('H15'	,	$pDatKDNR->設置先住所3->value	)
			            ->setCellValue('D16'	,	$pDatKDNR->電話番号->value	)
			            ->setCellValue('H16'	,	$pDatKDNR->電話番号->value	)
			            ->setCellValue('D17'	,	$pDatKDNR->製品名->value	)
			            ->setCellValue('H17'	,	$pDatSTKK->機番->value	)
			            ->setCellValue('D18'	,	$pDatKDNR->設置日->value	)
			            ->setCellValue('H18'	,	$pDatSTKK->初回設置日->value	)
			            ->setCellValue('D19'	,	$pDatKDNR->契約形態->value	)
			            ->setCellValue('H19'	,	$pDatSTKK->導入形態->value	)
			            ->setCellValue('D20'	,	$pDatKDNR->設置先CD->value	)
			            ->setCellValue('H20'	,	$pDatSTKK->本体契約先CD->value	)
			            ->setCellValue('D21'	,	$pDatKKKM->会社URL->value	)
			            ->setCellValue('D22'	,	$pDatKDNR->営業担当部門名->value	)
			            ->setCellValue('D23'	,	$pDatKDNR->営業担当者->value	)
			            ->setCellValue('D24'	,	$pDatKDNR->販売部門名->value	)
			            ->setCellValue('D25'	,	$pDatKDNR->販売担当者名->value	)
			            ->setCellValue('D26'	,	$pDatSTKK->本体契約先名1->value	)
			            ->setCellValue('D27'	,	$pDatSTKK->本体契約先名2->value	)
			            ->setCellValue('D28'	,	$pDatSTKK->リースレンタル先名->value	)
			            ->setCellValue('D29'	,	$pDatSTKK->リースレンタル_月->value	)
			            ->setCellValue('H29'	,	$pDatKDNR->経過年月->value	)
			            ->setCellValue('C30'	,	$pDatKDNR->担当者の方へ->value	)
			            ->setCellValue('C32'	,	$pDatKDNR->A16対話者氏名1->value	)
			            ->setCellValue('C34'	,	$pDatSTKK->会社名１->value	)
			            ->setCellValue('C35'	,	$pDatKDNR->A22対話者権限1->value	)
			            ->setCellValue('C37'	,	$pDatKDNR->エスカ内容->value	)
			            ->setCellValue('C39'	,	$pDatKDNR->A55顧客側困りごと->value	)
			            ->setCellValue('C41'	,	$pDatKDNR->その他特記事項->value	)
			            ->setCellValue('C45'	,	$pDatKDNR->訪問日->value	)
			            ->setCellValue('G45'	,	$pDatKDNR->訪問者名->value	)
			            ->setCellValue('C47'	,	$pDatKDNR->返信日->value	)
			            ->setCellValue('B50'	,	$pDatKDNR->訪問商談内容->value	)
			            ->setCellValue('F50'	,	$pDatKDNR->返信_その他特記事項->value	);

			$sheet=	$pPHPExcel	->setActiveSheetIndex( 0 );
			$sheet->setCellValueExplicit('D16',$pDatKDNR->電話番号->value,PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->setCellValueExplicit('H17',$pDatSTKK->機番->value,PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->setCellValueExplicit('H16',$pDatKDNR->電話番号->value,PHPExcel_Cell_DataType::TYPE_STRING);
			$sheet->setCellValueExplicit('H20',$pDatSTKK->本体契約先CD->value,PHPExcel_Cell_DataType::TYPE_STRING);

			if($pDatKDNR->担当者の方へ->value == "必ず返信して下さい"){
				$pPHPExcel	->setActiveSheetIndex( 0 )
				            ->setCellValue('C30'	,	"■必ず返信して下さい"	);
			}else if($pDatKKKK->担当者の方へ->value == "今回返信不要です"){
				$pPHPExcel	->setActiveSheetIndex( 0 )
			    	        ->setCellValue('F30'	,	"■今回返信不要です"	);
			}

			if($pDatKKKK->A19対話者男女->value == "男"){
				$pPHPExcel	->setActiveSheetIndex( 0 )
				            ->setCellValue('C33'	,	"■　男"	);
			}else if($pDatKKKK->A19対話者男女->value == "女"){
				$pPHPExcel	->setActiveSheetIndex( 0 )
			    	        ->setCellValue('E33'	,	"■　女"	);
			}

			for($i = 0; $i <= count($pDatKKKK->見積希望->value); $i++) {
				switch ( $pDatKKKK->見積希望->value[$i] ){
					case "MFP":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('B7'	,	"■MFP"	);
						break;
					case "他商品":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('C7'	,	"■他商品"	);
						break;
					case "見積送付希望":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('D7'	,	"■見積送付希望"	);
						break;
					case "その他訪問依頼":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('A11'	,	"■その他訪問依頼"	);
						break;
					default:
				}
			}

			for($i = 0; $i <= count($pDatKKKK->資料希望->value); $i++) {
				switch ( $pDatKKKK->資料希望->value[$i] ){
					case "MFP":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('B9'	,	"■MFP"	);
						break;
					case "他商品":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('C9'	,	"■他商品"	);
						break;
					case "資料送付":
						$pPHPExcel	->setActiveSheetIndex( 0 )
						            ->setCellValue('D9'	,	"■資料送付"	);
						break;
					default:
				}
			}

			return ($ret);
		}

		/*************************************************************************/
	    /* ユーザー情報の変換                                                    */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setKanriNendo( $pCode ) {

			$setName = "";

			switch ( $pCode ){
			case "平成27年度　（2015年）":
				$setName = "H27";
				break;
			case "平成28年度　（2016年）":
				$setName = "H28";
			  	break;
			case "平成29年度　（2017年）":
				$setName = "H29";
			  	break;
			case "平成30年度　（2018年）":
				$setName = "H30";
			  	break;
			case "平成31年度　（2019年）":
				$setName = "H31";
			  	break;
			default:
			}

			return ( $setName );
		}
	}

?>
