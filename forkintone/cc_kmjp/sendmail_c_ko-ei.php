<?php
/*****************************************************************************/
/* メール送信用PHP                                            (Version 1.00) */
/*   ファイル名 : sendmail_konica.php                                        */
/*   更新履歴   2016/04/21  Version 1.00                                     */
/*   [備考]                                                                  */
/*                                                                           */
/*****************************************************************************/
include_once("defkintoneconf.inc");
include_once("../tccom/tcutility.inc");
include_once("../tccom/tclog.php");
include_once("../tccom/tcerror.php");
include_once("../tccom/tckintone.php");
include_once("../tccom/tcmail.php");
include_once("../tccom/tcuser.php");


/*****************************************************************************/
/* Function定義                                                              */
/*****************************************************************************/

/*****************************************************************************/
/* kintoneからデータ取得                                                     */
/*  引数    $pRecno     取得レコード番号                                     */
/*          $pAppId     アプリID                                             */
/*  関数値  String      エラーメッセージ(正常の場合、空)                     */
/*****************************************************************************/
function updKintone($pRecno, $pAppId, $rec) {
			$recObj = new stdClass;
			$recObj->id	= $pRecno;

      $eDate = YMDtoStr(date("Ymd"), YMDTYPE_03, true) .  "T" . date("H:i:s") . "+09:00";
      $recObj->record = new stdClass();   // Inoue Add(2018.06.13)
			$recObj->record->エスカ送信日時KM	= valEnc($eDate);

      $kDate = YMDtoStr(date("Ymd"), YMDTYPE_03, true);
			$recObj->record->架電チェック日付 = valEnc($kDate);

      $hassin = $rec->支援G発信者->value;

			$recObj->record->架電チェック者 = valEnc($hassin);


			$recObj->record->架電チェック状況 = valEnc('チェック済み');


			$updData 			= new stdClass;
			$updData->app 		= $pAppId;
			$updData->records[] = $recObj;

			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= $pAppId;			// アプリID(顧客管理)
			$k->strContentType	= "Content-Type: application/json";

			$k->aryJson = $updData;
//var_dump($k);
			$json = $k->runCURLEXEC( TC_MODE_UPD );
			if( $k->strHttpCode == 200 ) {
			} else {
				echo "データの更新に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
			}
}


	function valEnc( $val ) {
		$wk = new stdClass;
		$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
		return ( $wk );
	}

/*****************************************************************************/
/* kintoneからデータ取得                                                     */
/*  引数    $pKintone   kintoneオブジェクト                                  */
/*          $pRecno     取得するレコード番号                                 */
/*          $jsonData   取得したJSONデータ格納域                             */
/*  関数値  String      エラーメッセージ(正常の場合、空)                     */
/*  備考    呼び出し元へjsonDataパラメータにて取得値を返却します             */
/*****************************************************************************/
function getKintone(&$pKintone, $pRecno, &$jsonData) {
    $sRet = "";     // 返却値(エラーメッセージ)

    // 条件設定
    $pKintone->strQuery = "(レコード番号 = $pRecno )";
    
    // データ取得
    $jsonData = $pKintone->runCURLEXEC( TC_MODE_SEL );
    
    if( $pKintone->strHttpCode == 200 ) {
        // 正常
    } else {
        $sRet = "抽出エラー -> CODE：".$pKintone->strHttpCode;
    }
    // 件数をチェックする。
    if( $pKintone->intDataCount == 0 ) {
        $sRet = "データが見つかりませんでした：$pRecno";
    }

    return($sRet);
}

/*****************************************************************************/
/* kintoneからユーザーデータ取得                                             */
/*  引数    $pCode      取得するコード                                       */
/*  関数値  String      エラーメッセージ(正常の場合、空)                     */
/*  備考    呼び出し元へjsonDataパラメータにて取得値を返却します             */
/*****************************************************************************/
//function getKintoneUser(&$pKintone, $pCode, &$userData) {
function getKintoneUser($pCode, &$userData) {
    // Kintoneオブジェクト初期処理
    $pKintone = new TcUser();
    $pKintone->parInit();                       // API連携用のパラメタを初期化する
    $pKintone->arySelFields = array();          // 読込用フィールドパラメータ

    $sRet = "";     // 返却値(エラーメッセージ)

    // 条件設定
    $pKintone->strQuery = "codes[0]=$pCode";

    // データ取得
    $userData = $pKintone-> runCURLEXEC_MAIL( TC_MODE_SEL);
    
    if( $pKintone->strHttpCode == 200 ) {
        // 正常
    } else {
        $sRet = "抽出エラー -> CODE：".$pKintone->strHttpCode;
    }
    // 件数をチェックする。
    if( $pKintone->intDataCount == 0 ) {
        $sRet = "データが見つかりませんでした：$pRecno";
    }

    return($sRet);
}



/*****************************************************************************/
/* メール送信                                                                */
/*  引数    $recData    取得レコード                                         */
/*          $pAppId     アプリID                                             */
/*  関数値  Boolean     正常:True / エラー:False                             */
/*****************************************************************************/
function sendMail( $recData, $pAppId ) {
    $mail = new tcMail();

    // ヘッダ設定
    $mail->m_sHeader = "MIME-Version: 1.0\n"
                     . "Content-Transfer-Encoding:7bit\n"
                     . "Content-Type:text/plain; charset=ISO-2022-JP\n"
                     . "Return-Path: kmj_cs_kintone@timeconcier.jp\n"
                     . "X-Mailer: PHP/" . phpversion() . "\n";

    // お客様名取得
    //$cstName = $recData->設置先名1->value . $recData->設置先名2->value;
    $cstName = $recData->会社名->value;
    // 件名
    //$mail->setTitle(mb_convert_encoding("【エスカ連絡】" . $cstName . " 様分", 'ISO-2022-JP-MS', 'UTF-8'), 1);

    $recSyosai = $recData->エスカ詳細->value;
    $cstSyosai = "";
    foreach ($recSyosai as $value) {
      if ( $cstSyosai != "" ) {
        $cstSyosai .= ",";
      }
	    $cstSyosai .= $value;
    }

    $Syosai = $recData->問い合わせ詳細->value;
    $arrSyosai = explode( "##", $Syosai );
    //$mail->setTitle(mb_convert_encoding("☆【" . $recData->エリア->value . "エスカ連絡】" . $recData->会社名->value . " 様分（" . $cstSyosai . "）"  . $recData->問い合わせ詳細->value, 'ISO-2022-JP-MS', 'UTF-8'), 1);
    $mail->setTitle(mb_convert_encoding("☆【" . $recData->エリア->value . "エスカ連絡】" . $recData->会社名->value . " 様分（" . $cstSyosai . "）"  . $arrSyosai[0], 'ISO-2022-JP-MS', 'UTF-8'), 1);


    // 本文生成
    $strMess = <<<_MAIL_BODY_

##EIGYO_TANTO##様

お疲れ様です。営業支援推進部ISGの##HASSIN##です。

##MAILSOUNYU##
お手数ですが、下記の案件についてご確認をお願い致します。
尚、この案件に関する問合せは
 isg-gl@pub.konicaminolta.jpへご連絡お願いいたします。



------------------ エスカレーション報告 ------------------


■##KADEN_THEME##（##AREA##「##PR_SYOZAI##」）
エスカ緊急性：##ESCA_KINKYU##　エスカ依頼先：##ESCA_IRAISAKI##
##ESCA_SYOSAI##
##TOIAWASE_SYOSAI##

架電日時：##KADEN_DATE## ##KADEN_TIME##
架電者：##KADEN_SYA##

●営業担当の方へ依頼内容
・依頼内容-①
##ESCA##

##ANKENMEI##

●対話内容
##TAIWA##

＊対話者：##TAIWASYA##様 ##TAIWASYA_SEI##
##RENRAKU_TEL##
##RENRAKU_NAME##

##ESCADATE##

■顧客情報
●設置先名1：##SECCHI1##
●設置先名2：##SECCHI2##
●住所　：〒##SECCHI_ZIP## ##SECCHI_ADDRESS##
●ＴＥＬ：##SECCHI_TEL##

●設置先CD：##SECCHI_NO## ●マシンID: ##MAS##
●商品名：##HINMOKU##　　　●機番　：##KIBAN##
●設置日：##SECCHI_DATE##　　　●初回設置日：##SECCHI_DATEF##
●契約形態：##KEIYAKU_TYPE##　●販売形態：##HANBAI_TYPE##
●リースレンタル_先名：##LEASE##
●本体契約先名：##KEIYAKUSAKI##


　以上、宜しくお願い致します。



[送信日時]：##now##

_MAIL_BODY_;


    // メールフッター
    $gMailFooter = <<<_MAIL_FOOTER_

-----------------------------------------------------------------
※このメールアドレスは配信専用です。
 ご返信いただいてもお返事できませんので、あらかじめご了承ください。
-----------------------------------------------------------------

_MAIL_FOOTER_;


    // 変換後設定
//    $mailbody = changeString($strMess, $recData);
    $mail->setBody( mb_convert_encoding(changeString($strMess, $recData, $pAppId), 'ISO-2022-JP-MS', 'UTF-8') );

    // 宛先
    $mail->setTo( $recData->エスカ担当営業メールアドレス->value );

    if ( !isNull($recData->メールアドレスCC部長->value) ) {
		  $mail->addCc( $recData->メールアドレスCC部長->value );
		}
    if ( !isNull($recData->メールアドレスCC課長->value) ) {
      $mail->addCc( $recData->メールアドレスCC課長->value );
    }
    if ( !isNull($recData->メールアドレスCCリーダー->value) ) {
		  $mail->addCc( $recData->メールアドレスCCリーダー->value );
    }

    $tuika1 = $recData->エスカ送信先TO->value;



    foreach ($tuika1 as $value) {
//var_dump($value->code);
    //メールアドレス取得
    $pCode = $value->code;
    $sRet = getKintoneUser( $pCode, $userData );
    if ( $sRet ) {
//var_dump($userData->users[0]->email);
	    $mail->addCc( $userData->users[0]->email );
    }

    }
// kintoneからデータ取得------------------------------------------------------------------------------------------

//    $userData = $jsonDataU->records[0];
//    var_dump(userData);

    $tuika2 = $recData->メールCC追加->value;
    foreach ($tuika2 as $value) {
	    $mail->addCc( $value );
    }
    // 差出人
    $mail->setFrom( "kmj_cs_kintone@timeconcier.jp","エスカ連絡" );
    $mail->setReply( "kmj_cs_kintone@timeconcier.jp" );
    //$mail->setFooter($gMailFooter);
    $mail->setFooter( mb_convert_encoding($gMailFooter, 'ISO-2022-JP-MS', 'UTF-8') );

    $res = $mail->sendMail();

    // 金丸様より依頼（メール遅延調査のため）
//    $mail->setTo( "h.inoue@timeconcier.jp" );
// 		$mail->aryCc = null;
//    $mail->sendMail();

//    $mail->setTo( "hideki.kagotani@konicaminolta.com" );
// 		$mail->aryCc = null;
//    $mail->sendMail();




    return($res);
}

/*****************************************************************************/
/* 文字列置換処理                                                            */
/*  引数    $pString    変換元文字列                                         */
/*  関数値  String      変換後の文字列                                       */
/*****************************************************************************/
function changeString($pString, $rec, $pAppId) {
    $eDate = "";     
    if ( !isNull($rec->回答期日->value) ) {
//		  $eDate =  "＊エスカ回答期日：" . YMDtoStr(date("Ymd",strtotime($rec->回答期日->value)), YMDTYPE_01, true) . "\n（期日内に「kintone営業コメント欄」への記載をお願い致します。）\n\n";
		  $eDate =  "○回答期日\n" . YMDtoStr(date("Ymd",strtotime($rec->回答期日->value)), YMDTYPE_01, true) . "\n\n※「回答期日」までにkintoneへ回答をお願いします。\n　今後の架電及び営業活動に使用させて頂きます。\n";
    }

    $recSyosai = $rec->エスカ詳細->value;
    $cstSyosai = "";
    $ankenview = "";
    foreach ($recSyosai as $value) {
      $chkanken = explode(".", $value);
      if( $chkanken[0] == "1" || $chkanken[0] == "2" || $chkanken[0] == "5" || $chkanken[0] == "9" ){
        $ankenview = "on";
      }
      if( $ankenview == "" ) {
        if( $chkanken[0] == "10" || $chkanken[0] == "11" ){
          $ankenview = "off";
        }
      }
      if ( $cstSyosai != "" ) {
        $cstSyosai .= ",";
      }
	    $cstSyosai .= $value;
    }

    $irainaiyo2 = "";
    if ( $ankenview == "on" ) {
      if ( $rec->案件名->value != "" ) {
        $irainaiyo2 = "・依頼内容-②\n";
        $irainaiyo2 .= "雷神への案件登録をお願い致します。案件登録名は下記『　』内の案件名称で登録お願い致します。\n\n";
        $irainaiyo2 .= "案件名　『" . $rec->案件名->value . "』\n\n";
        $irainaiyo2 .= "※注意点\n";
        $irainaiyo2 .= "・『』内の案件名称は変更しないようお願い致します。\n";
        $irainaiyo2 .= "・結果にかかわらず案件を削除しないようお願い致します。\n";
        $irainaiyo2 .= "・上記データは営業効率向上に向けてビックデータで集計・活用致します。\n";
        $irainaiyo2 .= "・MFP以外の案件の場合はユーザー名以降は各案件に合わせた登録名称でお願いします。\n";
        $irainaiyo2 .= "\n";
      }
    } elseif ( $ankenview == "off" ) {
       $irainaiyo2 = "※※本メール内容に相違がない場合は、インサイドセールスグループへ\n";
       $irainaiyo2 .= "変更依頼の旨を転送にてご連絡下さい。こちらで担当アシスタントへ依頼いたします。\n";
       $irainaiyo2 .= "\n";
    }

    $Syosai = $rec->問い合わせ詳細->value;
    $arrSyosai = explode( "##", $Syosai );

    // 変換元配列
    $pattern = array( "/" . preg_quote("##now##")               . "/",   // 01
                      "/" . preg_quote("##EIGYO_TANTO##")       . "/",   // 02
                      "/" . preg_quote("##HASSIN##")            . "/",   // 03
                      "/" . preg_quote("##MAILSOUNYU##")        . "/",   // 04
                      "/" . preg_quote("##KADEN_THEME##")       . "/",   // 05
                      "/" . preg_quote("##AREA##")              . "/",   // 06
                      "/" . preg_quote("##PR_SYOZAI##")         . "/",   // 07
                      "/" . preg_quote("##ESCA_KINKYU##")       . "/",   // 08
                      "/" . preg_quote("##ESCA_IRAISAKI##")     . "/",   // 09
                      "/" . preg_quote("##ESCA_SYOSAI##")       . "/",   // 10
                      "/" . preg_quote("##KADEN_DATE##")        . "/",   // 11
                      "/" . preg_quote("##KADEN_TIME##")        . "/",   // 12
                      "/" . preg_quote("##KADEN_SYA##")         . "/",   // 13
                      "/" . preg_quote("##ESCA##")              . "/",   // 14
                      "/" . preg_quote("##TAIWA##")             . "/",   // 15
                      "/" . preg_quote("##TAIWASYA##")          . "/",   // 16
                      "/" . preg_quote("##TAIWASYA_SEI##")      . "/",   // 17
                      "/" . preg_quote("##RENRAKU_TEL##")      . "/",    // 18
                      "/" . preg_quote("##RENRAKU_NAME##")      . "/",   // 19
                      "/" . preg_quote("##ESCADATE##")          . "/",   // 20

                      "/" . preg_quote("##SECCHI1##")           . "/",   // 21
                      "/" . preg_quote("##SECCHI2##")           . "/",   // 22
                      "/" . preg_quote("##SECCHI_ZIP##")        . "/",   // 23
                      "/" . preg_quote("##SECCHI_ADDRESS##")    . "/",   // 24
                      "/" . preg_quote("##SECCHI_TEL##")        . "/",   // 25
                      "/" . preg_quote("##SECCHI_NO##")         . "/",   // 26
                      "/" . preg_quote("##HINMOKU##")           . "/",   // 27
                      "/" . preg_quote("##KIBAN##")             . "/",   // 28
                      "/" . preg_quote("##SECCHI_DATE##")       . "/",   // 29
                      "/" . preg_quote("##SECCHI_DATEF##")      . "/",   // 30
                      "/" . preg_quote("##KEIYAKU_TYPE##")      . "/",   // 31
                      "/" . preg_quote("##HANBAI_TYPE##")       . "/",   // 32
                      "/" . preg_quote("##LEASE##")             . "/",   // 33
                      "/" . preg_quote("##KEIYAKUSAKI##")       . "/",   // 34
                      "/" . preg_quote("##ANKENMEI##")          . "/",   // 35
                      "/" . preg_quote("##TOIAWASE_SYOSAI##")   . "/",   // 36
//                      "/" . preg_quote("##MAILTUIKA##")         . "/",   // 37
                      "/" . preg_quote("##MAS##")               . "/",   // 38

                      "/" . preg_quote("##RECORD_ID##")         . "/",   // 39
                      "/" . preg_quote("##APP_ID##")            . "/"    // 40



                    );
    $replace = array(YMDtoStr(date("Ymd"), YMDTYPE_01, true) . date("H:i:s"),      // 01
                     $rec->エスカ担当営業->value                     ,             // 02
                     $rec->支援G発信者->value                        ,             // 03
                     $rec->メール挿入文->value                       ,             // 04
                     $rec->架電テーマ->value                         ,             // 05
                     $rec->エリア->value                             ,             // 06
                     $rec->PR商材->value                             ,             // 07
                     $rec->エスカ緊急性->value                       ,             // 08
                     $rec->エスカ依頼先->value                       ,             // 09
                     $cstSyosai                                      ,             // 10
                     $rec->架電日->value                             ,             // 11
                     $rec->架電時刻->value                           ,             // 12
                     $rec->架電者->value                            ,              // 13
                     $rec->営業部門への依頼内容->value               ,             // 14
                     $rec->架電内容->value                           ,             // 15
                     $rec->対話者->value                             ,             // 16
                     $rec->対話者性別->value                         ,             // 17
                     $rec->連絡先TEL->value                          ,             // 18
                     $rec->連絡先名称->value                         ,             // 19
                     $eDate                                          ,             // 20

                     $rec->設置先名1->value                          ,             // 21
                     $rec->設置先名2->value                          ,             // 22
                     $rec->設置先郵便番号->value                     ,             // 23
                     $rec->設置先住所->value                         ,             // 24
                     $rec->設置先TEL->value                          ,             // 25
                     $rec->設置先CD->value                           ,             // 26
                     $rec->品目名->value                             ,             // 27
                     $rec->機番->value                               ,             // 28
                     $rec->設置日->value                             ,             // 29
                     $rec->初回設置日->value                         ,             // 30
                     $rec->契約形態->value                           ,             // 31
                     $rec->販売形態->value                           ,             // 32
                     $rec->リースレンタル先名->value                 ,             // 33
                     $rec->本体契約先名1->value                      ,             // 34
                     $irainaiyo2                                     ,             // 35
                     $arrSyosai[0]                                   ,             // 36
//                     $arrSyosai[1]                                   ,             // 37
                     $rec->マシンID->value                           ,             // 38

                     $rec->レコード番号->value                       ,             // 39
                     $pAppId                                                       // 40
                    );

    $strRet = preg_replace($pattern, $replace, $pString);

    //「\n」を改行に変換
    $strRet = str_replace('\n', "\n", $strRet);

//    // ログ出力
//    $log = new tcLog();
//    $log->writeLog("【SENDMAIL_TEST】$strRet");

    return($strRet);
}


/*****************************************************************************/
/* 内部変数初期化                                                            */
/*****************************************************************************/
$bErr = false;  // エラー

/*****************************************************************************/
/* GETパラメータ取得                                                         */
/*****************************************************************************/
$pRecno = $_GET["rno"];
//if ($pRecno == 0) {
if (isset($_GET["rno"]) == false ) {

    // 終了
    exit("Exit!");
}

// 接続先アプリIDがわたってくる
$conApp = TC_APPID_CC_DBEK;
$pAppid = $_GET["appid"];
if ($pAppid != "") {
    $conApp = $pAppid;
}

// Kintoneオブジェクト初期処理
$oKintone = new TcKintone();
$oKintone->parInit();                       // API連携用のパラメタを初期化する
//$oKintone->intAppID = TC_APPID_CC_DBEK;     // アプリID
$oKintone->intAppID = $conApp;     // アプリID
$oKintone->arySelFields = array();          // 読込用フィールドパラメータ

// kintoneからデータ取得
$sRet = getKintone( $oKintone, $pRecno, $jsonData );

if($sRet <> "") {
    // エラー発生
    $bErr = true;
}

if(! $bErr) {
    // レコード情報取得
    $recData = $jsonData->records[0];

    // メール送信
    sendMail($recData, $conApp);
}

if($bErr) {
    // エラー発生
    print "メール送信が失敗しました。";
} else {
    // 送信成功
    //print "OK!!";

    updKintone($pRecno, $conApp, $recData);

    print $recData->エスカ担当営業->value . "様宛にメール送信しました。";
    print YMDtoStr(date("Ymd"), YMDTYPE_01, true) . date("H:i:s");
    print "<p><a href='#' onClick='window.close(); return false;'>閉じる</a></p>";
}




?>
