<?php
/*****************************************************************************/
/* メール送信用PHP                                            (Version 1.00) */
/*   ファイル名 : sendmail.php                                               */
/*   更新履歴   2016/04/21  Version 1.00                                     */
/*   [備考]                                                                  */
/*                                                                           */
/*****************************************************************************/
include_once("defkintoneconf.inc");
include_once("../tccom/tcutility.inc");
include_once("../tccom/tclog.php");
include_once("../tccom/tcerror.php");
include_once("../tccom/tckintone.php");
include_once("../tccom/tcmail.php");


/*****************************************************************************/
/* Function定義                                                              */
/*****************************************************************************/

/*****************************************************************************/
/* kintoneからデータ取得                                                     */
/*  引数    $pRecno     取得レコード番号                                     */
/*          $pAppId     アプリID                                             */
/*  関数値  String      エラーメッセージ(正常の場合、空)                     */
/*****************************************************************************/
function updKintone($pRecno, $pAppId) {
			$recObj = new stdClass;
			$recObj->id	= $pRecno;

//YMDtoStr(date("Ymd"), YMDTYPE_03, true) . date("H:i:s")

			//$recObj->record->エスカ送信日時	= valEnc("2016-06-28T11:30:00Z");
      $eDate = YMDtoStr(date("Ymd"), YMDTYPE_03, true) .  "T" . date("H:i:s") . "+09:00";
//var_dump($eDate);
			$recObj->record->エスカ送信日時	= valEnc($eDate);

			$updData 			= new stdClass;
			$updData->app 		= $pAppId;
			$updData->records[] = $recObj;

			$k = new TcKintone();
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 		= $pAppId;			// アプリID(顧客管理)
			$k->strContentType	= "Content-Type: application/json";

			$k->aryJson = $updData;
//var_dump($k);
			$json = $k->runCURLEXEC( TC_MODE_UPD );
//print "<br><br><br>";
//var_dump($k);
			if( $k->strHttpCode == 200 ) {
			} else {
				echo "データの更新に失敗しました。( ".$k->strHttpCode." : ".$k->strKntErrMsg." )<br><br>\n";
			}
}


	function valEnc( $val ) {
		$wk = new stdClass;
		$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
		return ( $wk );
	}

/*****************************************************************************/
/* kintoneからデータ取得                                                     */
/*  引数    $pKintone   kintoneオブジェクト                                  */
/*          $pRecno     取得するレコード番号                                 */
/*          $jsonData   取得したJSONデータ格納域                             */
/*  関数値  String      エラーメッセージ(正常の場合、空)                     */
/*  備考    呼び出し元へjsonDataパラメータにて取得値を返却します             */
/*****************************************************************************/
function getKintone(&$pKintone, $pRecno, &$jsonData) {
    $sRet = "";     // 返却値(エラーメッセージ)

    // 条件設定
    $pKintone->strQuery = "(レコード番号 = $pRecno )";
    
    // データ取得
    $jsonData = $pKintone->runCURLEXEC( TC_MODE_SEL );
    
    if( $pKintone->strHttpCode == 200 ) {
        // 正常
    } else {
        $sRet = "抽出エラー -> CODE：".$pKintone->strHttpCode;
    }
    // 件数をチェックする。
    if( $pKintone->intDataCount == 0 ) {
        $sRet = "データが見つかりませんでした：$pRecno";
    }

    return($sRet);
}

/*****************************************************************************/
/* メール送信                                                                */
/*  引数    $recData    取得レコード                                         */
/*          $pAppId     アプリID                                             */
/*  関数値  Boolean     正常:True / エラー:False                             */
/*****************************************************************************/
function sendMail( $recData, $pAppId ) {
    $mail = new tcMail();

    // ヘッダ設定
    $mail->m_sHeader = "MIME-Version: 1.0\n"
                     . "Content-Transfer-Encoding:7bit\n"
                     . "Content-Type:text/plain; charset=ISO-2022-JP\n"
                     . "Return-Path: kmj_cs_kintone@timeconcier.jp\n"
                     . "X-Mailer: PHP/" . phpversion() . "\n";

    // お客様名取得
    $cstName = $recData->設置先名1->value . $recData->設置先名2->value;
    // 件名
//    $mail->setTitle("【エスカ連絡】" . $cstName . " 様分", 1);
//    $mail->setTitle(mb_encode_mimeheader("【エスカ連絡】" . $cstName . " 様分", "ISO-2022-JP"), 1);
    $mail->setTitle(mb_convert_encoding("【エスカ連絡】" . $cstName . " 様分", 'ISO-2022-JP-MS', 'UTF-8'), 1);
    //$mail->setTitle(mb_convert_encoding("【エスカ連絡】" . $cstName . " 様分", 'ISO-2022-JP-MS', 'UTF-8'), 0);

    // 本文生成
    $strMess = <<<_MAIL_BODY_

##EIGYO_TANTO##様

お疲れ様です。CS推進Gの##HASSIN##です。
##MAILSOUNYU##
お手数ですが、下記の案件についてご確認をお願い致します。


-------- エスカレーション報告 ------------------

【##KADEN_TAISYO##】
種別１：##KADEN_SYUBETU1##　種別２：##KADEN_SYUBETU2##
架電日時：##KADEN_DATE## ##KADEN_TIME##
架電者：##KADEN_SYA##

○営業担当の方へ依頼内容
##ESCA##

○対話内容
##TAIWA##

＊対話者：##TAIWASYA##様 ##TAIWASYA_SEI##

##ESCADATE##

■顧客情報
○会社名：##SECCHI1##
○部署名：##SECCHI2##
○住所　：〒##SECCHI_ZIP## ##SECCHI_ADDRESS##
○ご担当：##TANTO## 様 ##TANTO_SEI##
○ＴＥＬ：##SECCHI_TEL##

○設置先CD：##KANRI_NO##
○商品名：##HINMOKU##　　　○機番　：##KIBAN##
○設置日：##SECCHI_DATE##　　　○初回設置日：##SECCHI_DATEF##
○契約形態：##KEIYAKU_TYPE##　○販売形態：##HANBAI_TYPE##
○リースレンタル_先名：##LEASE##
○本体契約先名1：##KEIYAKUSAKI##

■下記URLにて架電情報をご確認頂けます
　　https://kmjp.cybozu.com/k/##APP_ID##/show#record=##RECORD_ID##

　以上、宜しくお願い致します。



[送信日時]：##now##

_MAIL_BODY_;


    // メールフッター
    $gMailFooter = <<<_MAIL_FOOTER_

-----------------------------------------------------------------
※このメールアドレスは配信専用です。
 ご返信いただいてもお返事できませんので、あらかじめご了承ください。
-----------------------------------------------------------------

_MAIL_FOOTER_;


    // 変換後設定
//    $mailbody = changeString($strMess, $recData);
    $mail->setBody( mb_convert_encoding(changeString($strMess, $recData, $pAppId), 'ISO-2022-JP-MS', 'UTF-8') );

    // 宛先
    $mail->setTo( $recData->メールアドレス->value );
    if ( !isNull($recData->メールアドレスCC部長->value) ) {
		  $mail->addCc( $recData->メールアドレスCC部長->value );
		}
    if ( !isNull($recData->メールアドレスCC課長->value) ) {
      $mail->addCc( $recData->メールアドレスCC課長->value );
    }
    if ( !isNull($recData->メールアドレスCCリーダー->value) ) {
		  $mail->addCc( $recData->メールアドレスCCリーダー->value );
    }
    if ( !isNull($recData->メーリングリスト->value) ) {
		  $mail->addCc( $recData->メーリングリスト->value );
    }
    $tuika = $recData->追加メールアドレス->value;
    foreach ($tuika as $value) {
	    $mail->addCc( $value );
    }
    // 差出人
    $mail->setFrom( "kmj_cs_kintone@timeconcier.jp","エスカ連絡" );
    $mail->setReply( "kmj_cs_kintone@timeconcier.jp" );
    //$mail->setFooter($gMailFooter);
    $mail->setFooter( mb_convert_encoding($gMailFooter, 'ISO-2022-JP-MS', 'UTF-8') );

    $res = $mail->sendMail();

    // 金丸様より依頼（メール遅延調査のため）
    $mail->setTo( "tadao.kanamaru@konicaminolta.com" );
 		$mail->aryCc = null;
    
    $mail->sendMail();



    return($res);
}

/*****************************************************************************/
/* 文字列置換処理                                                            */
/*  引数    $pString    変換元文字列                                         */
/*  関数値  String      変換後の文字列                                       */
/*****************************************************************************/
function changeString($pString, $rec, $pAppId) {
    $eDate = "";     
    if ( !isNull($rec->エスカ回答期日->value) ) {
//		  $eDate =  "○回答期日\n" . YMDtoStr(date("Ymd",strtotime($rec->エスカ回答期日->value)), YMDTYPE_01, true) . "\n";
		  $eDate =  "○回答期日\n" . YMDtoStr(date("Ymd",strtotime($rec->エスカ回答期日->value)), YMDTYPE_01, true) . "\n\n※「回答期日」までにkintoneへ回答をお願いします。\n　今後の架電及び営業活動に使用させて頂きます。\n";
    }
    // 変換元配列
    $pattern = array( "/" . preg_quote("##now##")               . "/",   // 01
                      "/" . preg_quote("##EIGYO_TANTO##")        . "/",   // 02
                      "/" . preg_quote("##HASSIN##")            . "/",   // 03
                      "/" . preg_quote("##KADEN_TAISYO##")      . "/",   // 04
                      "/" . preg_quote("##KADEN_SYUBETU1##")    . "/",   // 05
                      "/" . preg_quote("##KADEN_SYUBETU2##")    . "/",   // 06
                      "/" . preg_quote("##KADEN_DATE##")        . "/",   // 07
                      "/" . preg_quote("##KADEN_TIME##")        . "/",   // 08
                      "/" . preg_quote("##TAIWA##")             . "/",   // 09
                      "/" . preg_quote("##TAIWASYA##")          . "/",   // 10
                      "/" . preg_quote("##TAIWASYA_SEI##")      . "/",   // 11
                      "/" . preg_quote("##ESCA##")              . "/",   // 12
                      "/" . preg_quote("##SECCHI1##")           . "/",   // 13
                      "/" . preg_quote("##SECCHI2##")           . "/",   // 14
                      "/" . preg_quote("##SECCHI_ZIP##")        . "/",   // 15
                      "/" . preg_quote("##SECCHI_ADDRESS##")    . "/",   // 16
                      "/" . preg_quote("##TANTO##")             . "/",   // 17
                      "/" . preg_quote("##TANTO_SEI##")         . "/",   // 18
                      "/" . preg_quote("##SECCHI_TEL##")        . "/",   // 19
                      "/" . preg_quote("##KANRI_NO##")          . "/",   // 20
                      "/" . preg_quote("##HINMOKU##")           . "/",   // 21
                      "/" . preg_quote("##KIBAN##")             . "/",   // 22
                      "/" . preg_quote("##SECCHI_DATE##")       . "/",   // 23
                      "/" . preg_quote("##SECCHI_DATEF##")      . "/",   // 24
                      "/" . preg_quote("##KEIYAKU_TYPE##")      . "/",   // 25
                      "/" . preg_quote("##HANBAI_TYPE##")       . "/",   // 26
                      "/" . preg_quote("##LEASE##")             . "/",   // 27
                      "/" . preg_quote("##KEIYAKUSAKI##")       . "/",   // 28
                      "/" . preg_quote("##MAILSOUNYU##")        . "/",   // 29
                      "/" . preg_quote("##ESCADATE##")          . "/",   // 30
                      "/" . preg_quote("##RECORD_ID##")         . "/",   // 31
                      "/" . preg_quote("##KADEN_SYA##")         . "/",   // 32
                      "/" . preg_quote("##APP_ID##")            . "/"    // 33
                    );
    $replace = array(YMDtoStr(date("Ymd"), YMDTYPE_01, true) . date("H:i:s"),      // 01
                     $rec->エスカ担当者営業->value                   ,   // 02
                     $rec->エスカ発信者->value                       ,   // 03
                     $rec->架電対象->value                           ,   // 04
                     $rec->種別１->value                             ,   // 05
                     $rec->種別２->value                             ,   // 06
                     $rec->架電日->value                             ,   // 07
                     $rec->架電時間->value                           ,   // 08
                     $rec->対話内容->value                           ,   // 09
                     $rec->対話者->value                             ,   // 10
                     $rec->性別対話者->value                         ,   // 11
                     $rec->営業担当の方へ依頼内容->value             ,   // 12
                     $rec->設置先名1->value                          ,   // 13
                     $rec->設置先名2->value                          ,   // 14
                     $rec->設置先郵便番号->value                     ,   // 15
                     $rec->設置先住所->value                         ,   // 16
                     $rec->担当者->value                             ,   // 17
                     $rec->性別_担当者->value                        ,   // 18
                     $rec->設置先TEL->value                          ,   // 19
                     $rec->管理No->value                             ,   // 20
                     $rec->品目名->value                             ,   // 21
                     $rec->機番->value                               ,   // 22
                     $rec->設置日->value                             ,   // 23
                     $rec->初回設置日->value                         ,   // 24
                     $rec->契約形態->value                           ,   // 25
                     $rec->販売形態->value                           ,   // 26
                     $rec->リースレンタル先名->value                 ,   // 27
                     $rec->本体契約先名1->value                      ,   // 28
                     $rec->メール挿入文->value                       ,   // 29
                     $eDate                                          ,   // 30
                     $rec->レコード番号->value                       ,   // 31
                     $rec->架電者->value                             ,   // 32
                     $pAppId                                             // 33
                    );

    $strRet = preg_replace($pattern, $replace, $pString);

    //「\n」を改行に変換
    $strRet = str_replace('\n', "\n", $strRet);

//    // ログ出力
//    $log = new tcLog();
//    $log->writeLog("【SENDMAIL_TEST】$strRet");

    return($strRet);
}


/*****************************************************************************/
/* 内部変数初期化                                                            */
/*****************************************************************************/
$bErr = false;  // エラー

/*****************************************************************************/
/* GETパラメータ取得                                                         */
/*****************************************************************************/
$pRecno = $_GET["rno"];
//if ($pRecno == 0) {
if (isset($_GET["rno"]) == false ) {

    // 終了
    exit("Exit!");
}

// テストの場合は、接続先アプリIDがわたってくる
$conApp = TC_APPID_CC_DBEK;
$pAppid = $_GET["appid"];
if ($pAppid != "") {
    $conApp = $pAppid;
}

// Kintoneオブジェクト初期処理
$oKintone = new TcKintone();
$oKintone->parInit();                       // API連携用のパラメタを初期化する
//$oKintone->intAppID = TC_APPID_CC_DBEK;     // アプリID
$oKintone->intAppID = $conApp;     // アプリID
$oKintone->arySelFields = array();          // 読込用フィールドパラメータ

// kintoneからデータ取得
$sRet = getKintone( $oKintone, $pRecno, $jsonData );

if($sRet <> "") {
    // エラー発生
    $bErr = true;
}

if(! $bErr) {
    // レコード情報取得
    $recData = $jsonData->records[0];

    // メール送信
    sendMail($recData, $conApp);
}

if($bErr) {
    // エラー発生
    print "メール送信が失敗しました。";
} else {
    // 送信成功
    //print "OK!!";

    updKintone($pRecno, $conApp);

    print $recData->エスカ担当者営業->value . "様宛にメール送信しました。";
    print YMDtoStr(date("Ymd"), YMDTYPE_01, true) . date("H:i:s");
    print "<p><a href='#' onClick='window.close(); return false;'>閉じる</a></p>";
}

?>
