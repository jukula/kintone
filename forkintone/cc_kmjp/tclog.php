<?php
/*****************************************************************************/
/*  ログ出力クラス                                            (Version 1.02) */
/*   クラス名       TcLog                                                    */
/*   メンバ変数     bolOutput       出力フラグ                               */
/*                  strFilePath     ログファイルパス                         */
/*                  intSizeMax      ログファイル最大サイズ                   */
/*   メンバ関数     openLog         ログファイルオープン                     */
/*                  writeLog        ログ出力                                 */
/*   メソッド       checkSize       ファイルサイズチェック                   */
/*                  logBackup       ファイルバックアップ                     */
/*                  openLog         ログファイルオープン                     */
/*                  closeLog        ログファイルクローズ                     */
/*                  writeLog        ログ出力                                 */
/*   作成日         2013/04/08                                               */
/*   更新履歴       20xx/xx/xx      Version 1.0x(xxxxxx.x)                   */
/*                                  XXXXXXXXXXXXXXX                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
require_once("deflog.inc");
include_once("tcerror.php");

///////////////////////////////////////////////////////////////////////////////
// 定数定義
///////////////////////////////////////////////////////////////////////////////

class TcLog
{
    /*************************************************************************/
    /* メンバ変数                                                            */
    /*************************************************************************/
    var $bolOutput      = TC_LOGOUT ;
    var $strFilePath    = TC_LOGFILE;
    var $intSizeMax     = TC_LOGSIZE;
    var $bolDns         = TC_LOGDNS ;
    var $intFilePointer = false;
    var $bolCheckSize   = true;         // サイズチェックをするか?
    var $bolEveryMonth  = false;        // 月毎に出力するか?
    var $err;

    /*************************************************************************/
    /* コンストラクタ                                                        */
    /*************************************************************************/
    function ctLog( $pLogFile = "" ) {
        if( $pLogFile != "" ) $this->strFilePath = $pLogFile;
        $this->err = new ctError();
    }

    /*************************************************************************/
    /* メンバ関数                                                            */
    /*************************************************************************/
    function setFilePath( $arg ) {
        $this->strFilePath = $arg;
    }
    function getFilePath() {
        return( $this->strFilePath );
    }

    function setSizeMax( $arg ) {
        $this->intSizeMax = $arg;
    }
    function getSizeMax() {
        return( $this->intSizeMax );
    }

    /*************************************************************************/
    /* メソッド                                                              */
    /*************************************************************************/

    /*************************************************************************/
    /* ファイルサイズチェック                                                */
    /*  引数    なし                                                         */
    /*  関数値  boolean     OK:true / NG:false                               */
    /*************************************************************************/
    function checkSize() {
        // ファイルフルパス
        $strPath = ROOT_DIR . TC_LOGDIR . $this->strFilePath;
        // ファイルサイズ確認
        clearstatcache();
        $res = @filesize( $strPath );
        $ret = ( $res < $this->intSizeMax ) ? true : false;
        return( $ret );
    }

    /*************************************************************************/
    /* ファイルバックアップ                                                  */
    /*  引数    なし                                                         */
    /*  関数値  boolean     正常:True / エラー:False                         */
    /*************************************************************************/
    function logBackup() {
        // ファイルフルパス
        $strPath = ROOT_DIR . TC_LOGDIR . $this->strFilePath;
        // ファイルパス切り出し
        $pathInfo = pathinfo( $strPath );

        // 圧縮ファイル名設定
        $strGzPath = $pathInfo["dirname"] . "/"
                   . substr( $pathInfo["basename"], 0,
                           strlen($pathInfo["basename"]) - strlen($pathInfo["extension"]) - 1 )
                   . date("YmdHi") . ".gz";
        // ファイル圧縮
        $aryData = @file( $strPath );
        if( !$aryData ) {
            // ファイルオープンエラー
            $this->err->setError( ERR_F002, $strPath );
            return(false);
        }
        $gz = @gzopen( $strGzPath, "w" );
        if( !$gz ) {
            // ファイル圧縮エラー
            $this->err->setError( ERR_F008, $strGzPath );
            return(false);
        }
        
        $data = implode("", $aryData);
        gzwrite($gz, $data, strlen($data));
        gzclose($gz);

        return(true);
    }

    /*************************************************************************/
    /* ログファイルオープン                                                  */
    /*  引数    $mode       オープンモード                                   */
    /*  関数値  boolean     正常:True / エラー:False                         */
    /*************************************************************************/
    function openLog( $mode = "a+" ) {
        // 出力指定確認
        if( !$this->bolOutput ) return(true);

        // ファイルフルパス
        $strPath = ROOT_DIR . TC_LOGDIR . $this->strFilePath;
        // 月毎出力の場合
        if( $this->bolEveryMonth ) {
            // ファイルパス切り出し
            $pathInfo = pathinfo( $strPath );
            // 月毎ファイル名設定
            $strMonthPath = $pathInfo["dirname"] . "/"
                     . substr($pathInfo["basename"], 0,
                              strlen($pathInfo["basename"]) - strlen($pathInfo["extension"]) - 1 )
                     . date("m") . ".log";
            $strPath = $strMonthPath;
        } else {
            // ファイルサイズ確認
            if( $this->bolCheckSize ) {
                $res = $this->checkSize();
                if( !$res ) {
                    // バックアップ
                    $res = $this->logBackup();
                    if( $res ) {
                        // オープンモード変更
                        $mode = "w+";
                    } else {
                        // バックアップ失敗
                        return(false);
                    }
                }
            }
        }

        // ファイルオープン
        $res = fopen( $strPath, $mode );
        if( !$res ) {
            // オープンエラー
            $this->err->setError( ERR_F002, $this->strFilePath );
        } else {
            $this->intFilePointer = $res;
        }
        return( $res );
    }

    /*************************************************************************/
    /* ログファイルクローズ                                                  */
    /*  引数    なし                                                         */
    /*  関数値  boolean     正常:True / エラー:False                         */
    /*************************************************************************/
    function closeLog() {
        // ファイルクローズ
        $res = @fclose( $this->intFilePointer );
        if( !$res ) {
            // クローズエラー
            $this->err->setError( ERR_F003, $this->strFilePath );
        }
        return( $res );
    }

    /*************************************************************************/
    /* ログ出力                                                              */
    /*  引数    $pMess      出力文字列                                       */
    /*          $pOpen      ファイルオープン する:true / しない:false        */
    /*  関数値  boolean     正常:True / エラー:False                         */
    /*************************************************************************/
    function writeLog( $pMess, $pOpen = true ) {
        // 出力指定確認
        if( !$this->bolOutput ) return(true);

        $ret = true;
        // ファイルオープン
        if( $pOpen ) {
            $res = $this->openLog();
            if( !$res ) return(false);
        }
        
        // 出力メッセージ編集
        $strTime = date("[Y/m/d H:i:s]");
        $strIP   = getenv("REMOTE_ADDR");
        $strHost = "";
        if($this->bolDns) $strHost = "[" . @gethostbyaddr( $strIP ) . "]";
        $addMess = str_replace( "\n", " ", $pMess );
        $strMess = $strTime . " $strHost($strIP) $addMess\n";
        $res = @fputs( $this->intFilePointer, $strMess );
        if( !$res ) {
            $this->err->setError( ERR_F004, $strMess );
            $ret = false;
        }
        // ファイルクローズ
        if( $pOpen ) $this->closeLog();

        return( $ret );
    }
}
?>
