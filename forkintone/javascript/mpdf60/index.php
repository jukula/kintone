<?php
//=============================================================================
// $_REQUESTパラメータで渡されたHTMLタグをPDFに出力する
//
//=============================================================================
header("Access-Control-Allow-Origin: *");
header("Content-Type:text/html;charset=utf-8");

mb_language("Japanese");

if(0){
// kintoneアクセス用に必要なTCモジュールを読み込み
include_once("../../tccom/tcutility.inc");
include_once("../tcmodule/tcdef.inc");
include_once("../tcmodule/tcerror.php");
include_once("../tcmodule/tckintone.php");
include_once("../tcmodule/tckintonerecord.php");
}//endif(0)

//=============================================================================
//   main
//=============================================================================

/*
** メイン処理
**  - kintoneから取得したフィールドをHTML化し、PDFに出力する
*/
function print_main() {

    // 出力タグの編集
    //  - kintone側のJavascriptからパラメータで渡されたDOM要素(HTML文字列)を加工
    $pHtml = str_replace('\"', '"', $_REQUEST['pElement']);					// 自動でエスケープされた「"」を元に戻す
    $pHtml = strip_tags($pHtml, '<table><thead><tbody><tr><th><td><span><div><br>');	// 不要なタグ除去(<a><img>が邪魔なのでのける)
    
    // 出力先の切り替え
    if ( $_REQUEST['pHtmlView'] ) {
        // メンテナンス画面(HTML出力)
        $flg = 1;
    } else {
        // PHP出力
        $flg = 0;
    }

if ( $flg ) {


echo "★★★★メンテナンス中です★★★★<br><br>";
echo $_REQUEST['pElement'];
echo "<br>：<br>：<br>：<br>";

$stylesheet = file_get_contents("templates/style1.css");

echo '<head><meta charset="utf-8" /><title>印刷ビュー</title><style type="text/css">' . $stylesheet . '</style></head>';
echo $pHtml;


} else {

    // mPDFによるPDF出力
    include("mpdf.php");
    $mpdf=new mPDF('ja+aCJK',		// モード ('sjis'では日本語非対応(文字化け)なので、'ja+aCJK'パラメータ指定で日本語対応)
                   'A4-L',			// 用紙サイズ
                   0,				// フォントサイズ
                   '',				// フォントファミリー
                   12,				// 左マージン
                   12,				// 右マージン
                   8,				// トップマージン
                   8,				// ボトムマージン
                   0,				// ヘッダーマージン
                   0,				// ボトムマージン
                   '');				// L-landscape,P-Pportrait
    $stylesheet = file_get_contents("templates/style1.css");
/*
    $mpdf->SetHTMLHeader('<p>ヘッダテスト</p>');
    $mpdf->SetHTMLFooter('{PAGENO}/{nbpg}');
*/
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($pHtml,2);
    $mpdf->Output();
    exit;
}//endif(0)

}

print_main();
