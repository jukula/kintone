<?php
//=============================================================================
// $_REQUESTパラメータで渡されたHTMLタグをPDFに出力する
//
//=============================================================================
header("Access-Control-Allow-Origin: *");
header("Content-Type:text/html;charset=utf-8");

mb_language("Japanese");

if(0){
// kintoneアクセス用に必要なTCモジュールを読み込み
include_once("../../tccom/tcutility.inc");
include_once("../tcmodule/tcdef.inc");
include_once("../tcmodule/tcerror.php");
include_once("../tcmodule/tckintone.php");
include_once("../tcmodule/tckintonerecord.php");
}//endif(0)

//=============================================================================
//   main
//=============================================================================

/*
** メイン処理
**  - kintoneから取得したフィールドをHTML化し、PDFに出力する
*/
function print_main() {

if(0){
    // ----------------------------------------------
    // ★API連携先kintoneログイン情報を設定
    //  - 通常"defkintoneconf.inc"でベタ打ちだが
    //    汎用的にするためパラメータから動的に設定する
    // ----------------------------------------------
    define("TC_CY_DOMAIN"   , $_REQUEST['pDomain'] );		// ドメイン
    define("TC_CY_USER"     , $_REQUEST['pUser']   );		// ユーザ名
    define("TC_CY_PASSWORD" , $_REQUEST['pPass']   );		// パスワード

    // ----------------------------------------------
    // 案件管理からデータを取得する
    // ----------------------------------------------
    $k = new TcKintone();				// API連携クラス
    $k->parInit();						// API連携用のパラメタを初期化する
    $k->intAppID = 2626;				// アプリID（案件管理）
    $k->intRecID = $_REQUEST['pId'];	// レコードID（案件管理）

    // 検索条件を作成する。
    $k->strQuery = "レコード番号 = ".$k->intRecID;

    // http通信を実行する。
    $ret_json = $k->runCURLEXEC( TC_MODE_SEL );

    // データが存在しない。
    if( $k->intDataCount == 0 ) {
        echo '<li>データが存在しません。</li>';
        return;
    }

    // 取得したJSON情報
    $dat = $ret_json->records[ $k->intDataCount - 1 ];

    // HTMLタグ
    $tagTblHead = '';
    $tagTblData = '';

    // 処理対象フィールド設定
    $field = $dat->$_REQUEST['pCode'];

    if ( $field->type == "SUBTABLE" ) {

        // 行単位に処理
        for ( $cnt = 0; $cnt < count($field->value); $cnt++ ) {

            $row = $field->value[$cnt]->value;	// 処理対象行を設定

            // 列単位に処理
            $tagTblData .= '<tr>';				// データ行の<tr>を開始する
            foreach ( $row as $key => $value ) {
                // 0行目の時のみ、ヘッダ行のタグを生成する
                if ( $cnt < 1 ) { $tagTblHead .= '<th>'.$key.'</th>'; }
                // 列の値を取得
                $tagTblData .= '<td>'. $row->$key->value. '</td>';
            }
            $tagTblData .= '</tr>';				// データ行の<tr>を閉じる

        }

        // 出力するHTMLタグを生成
        $html = '<table class="printview"><tbody><tr>' . $tagTblHead . '</tr>' . $tagTblData . '</tbody></table>';

    } else {
        // 未対応
        echo "現在サブテーブル以外のフィールドは出力非対応です。";
        return;
    }
}//endif(0)


    // 出力タグの編集
    //  - kintone側のJavascriptからパラメータで渡されたDOM要素(HTML文字列)を加工
    $html = str_replace('\"', '"', $_REQUEST['pElement']);					// 自動でエスケープされた「"」を元に戻す
    $html = strip_tags($html, '<table><thead><tr><th><td><span><div><br>');	// 不要なタグ除去(<a><img>が邪魔なのでのける)
/*
    $title = str_replace('\"', '"', $_REQUEST['pTitle']);					// 自動でエスケープされた「"」を元に戻す
    $title = strip_tags($title, '<span><div>');								// 不要なタグ除去(<a><img>が邪魔なのでのける)
    $maker = str_replace('\"', '"', $_REQUEST['pMaker']);					// 自動でエスケープされた「"」を元に戻す
    $maker = strip_tags($maker, '<span><div>');								// 不要なタグ除去(<a><img>が邪魔なのでのける)
    $today = str_replace('\"', '"', $_REQUEST['pToday']);					// 自動でエスケープされた「"」を元に戻す
    $today = strip_tags($today, '<span><div>');								// 不要なタグ除去(<a><img>が邪魔なのでのける)
    $payday = str_replace('\"', '"', $_REQUEST['pPayday']);					// 自動でエスケープされた「"」を元に戻す
    $payday = strip_tags($payday, '<span><div>');							// 不要なタグ除去(<a><img>が邪魔なのでのける)
    $sumval = str_replace('\"', '"', $_REQUEST['pSumVal']);					// 自動でエスケープされた「"」を元に戻す
    $sumval = strip_tags($sumval, '<span><div>');							// 不要なタグ除去(<a><img>が邪魔なのでのける)
*/

if ( 1 ) {
echo "★★★★メンテナンス中です★★★★<br><br>";
echo $_REQUEST['pElement'];
echo "<br>：<br>：<br>：<br>";

$stylesheet = file_get_contents("templates/style1.css");

echo '<head><meta charset="utf-8" /><title>印刷ビュー</title><style type="text/css">' . $stylesheet . '</style></head>';
echo $html;

} else {

    // mPDFによるPDF出力
    include("mpdf.php");
    $mpdf=new mPDF('ja+aCJK', 'A4-L');	// 'sjis'では日本語非対応(文字化け)なので、'ja+aCJK'パラメータ指定で日本語対応
    $stylesheet = file_get_contents("templates/style1.css");
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    $mpdf->Output();
    exit;
}//endif(0)

}

print_main();
