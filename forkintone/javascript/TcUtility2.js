/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TC基本Utility
//   どのアプリからでも呼び出されるモジュール集
//
// ▼メモ
//
// ▼更新履歴
// 20150826	新規作成 (m.takeuchi)
/*--------------------------------------------------------------------------------------------------------------------------------------------*/


// CSSロード
function loadCSS(href) { document.write('<link rel="stylesheet" type="text/css" href="' + href + '" />'); }
// JSロード
function loadJS(source) { document.write('<script src="' + source + '"></script>'); }

/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TCUtility(JS)
//   - TC社の基本的なユーティリティ関数で、どのアプリからでも利用される想定のもの
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
    /******************************************************/
    // 外部PHPの呼び出し実行
    //   FileName	実行ファイル名
    //   Query		PHPパラメタ
    //   isReload	リロードするか(bool)(省略可)
    //   Domain 	使用するドメイン指定
    //   関数値		なし
    /******************************************************/
    function TcPostPHP ( Domain, FileName, Query, isReload ) {

        // Ajax通信を開始する
        $.ajax({ url: "https://www.timeconcier.jp/forkintone/" + Domain + "/" + FileName, type: 'post', dataType:'html', data: Query })
        // ・ステータスコードは正常で、dataTypeで定義したようにパース出来たとき
        .done(function (response) { alert( "成功") })
        // ・サーバからステータスコード400以上が返ってきたとき
        // ・ステータスコードは正常だが、dataTypeで定義したようにパース出来なかったとき
        // ・通信に失敗したとき
        .fail(function () { alert(errorHandler(arguments)); });

    }
    function errorHandler(args) {
        var error;
        // errorThrownはHTTP通信に成功したときだけ空文字列以外の値が定義される
        if (args[2]) {
            try {
                // JSONとしてパースが成功し、且つ {"error":"..."} という構造であったとき
                // (undefinedが代入されるのを防ぐためにtoStringメソッドを使用)
                error = $.parseJSON(args[0].responseText).error.toString();
            } catch (e) {
                // パースに失敗した、もしくは期待する構造でなかったとき
                // (PHP側にエラーがあったときにもデバッグしやすいようにレスポンスをテキストとして返す)
                error = 'parsererror(' + args[2] + '): ' + args[0].responseText;
            }
        } else {
            // 通信に失敗したとき
            error = args[1] + '(HTTP request failed)';
        }
        return error;
    }



/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★kintone標準Utility
//   - kintone標準(サイボウズ提供)のユーティリティ関数
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// URLの?や#より前の文字列を取得
function getUrlPath_() {
        var url = document.URL;
        if (url.indexOf('?') > 0) {
                return url.substr(0, url.indexOf('?'));
        }
        if (url.indexOf('#') > 0) {
                return url.substr(0, url.indexOf('#'));
        }
        return url;
}


// CSSロード
function loadCSS(href) { document.write('<link rel="stylesheet" type="text/css" href="' + href + '" />'); }

// レコード追加画面ならtrue
function isAddPage() { return getUrlPath_().match(/edit$/); }

// レコード詳細画面ならtrue
function isShowPage() { return getUrlPath_().match(/show$/) && !location.hash.match(/mode=edit/); }

// レコード編集画面ならtrue
function isEditPage() { return getUrlPath_().match(/show$/) && location.hash.match(/mode=edit/); }

// レコード一覧画面ならtrue
function isViewPage() { return getUrlPath_().match(/\/[0-9]+\/$/); }

// ポータル画面ならtrue
function isPortalPage() { var url = document.URL; return url.match(/portal$/); }

// グラフ画面ならtrue
function isGraphPage(graphId) {

    if ( getUrlPath_().match(/report$/) ) {
        // グラフの表示ページの場合、グラフのIDを返す
        var url = document.URL;				// 現在のURL
        var aryRet = url.split("=");		// "="で分割
        
        if ( graphId == aryRet[1] ) {
            return true;
        }

    }

    // グラフの表示ページではない または 指定グラフIDが一致しない
    return false;
}


//----------------------------------------

// 要素のテキストを取得
function getTextContent_(el) {
    return el.childNodes[0].textContent !== undefined ? el.childNodes[0].textContent : el.childNodes[0].innerText;
}
 
// フィールド名の要素を取得
function getFieldLabelEl_(fieldId) {
    return document.getElementsByClassName('label-' + fieldId)[0];
}
 
// フィールド値の要素を取得
function getFieldValueEl_(fieldId) {
    return document.getElementsByClassName('value-' + fieldId)[0];
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueLength(fieldId) {
    return document.getElementsByClassName('value-' + fieldId).length;
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueEls_(fieldId, i) {
    return document.getElementsByClassName('value-' + fieldId)[i];
}
 
// フィールド名を取得（すべての画面で利用可）
function getFieldName(fieldId) {
    return getTextContent_(getFieldLabelEl_(fieldId));
}
 
// フィールド値を取得（一覧画面と詳細画面で利用可、一部のフィールドタイプには未対応）
function getFieldValue(fieldId) {
    return getTextContent_(getFieldValueEl_(fieldId));
}

// フィールド値を取得（テーブル内のフィールド）
function getFieldValueFromTable(fieldId, i) {
    return getTextContent_(getFieldValueEls_(fieldId, i));
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（テーブル内のフィールド）
function getFieldValueFromTableInput(fieldId,i) {
    return getFieldValueEls_(fieldId,i).querySelector('input');
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（テーブル内のフィールド）
function getFieldValueFromTableInputValue(fieldId,i) {
    return getFieldValueFromTableInput(fieldId,i).value;
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（追加画面と編集画面で利用可）
function getSingleLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('input');
}
 
// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（追加画面と編集画面で利用可）
function getSingleLineInputValue(fieldId) {
    return getSingleLineInput(fieldId).value;
}
 
// 文字列(複数行)フィールドのTEXTAREA要素を取得（追加画面と編集画面で利用可）
function getMultiLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('textarea');
}
 
// 文字列(複数行)フィールドのTEXTAREA要素の入力値を取得（追加画面と編集画面で利用可）
function getMultiLineInputValue(fieldId) {
    return getMultiLineInput(fieldId).value;
}

//----------------------------------------

// イベントリスナーを登録
function addEventListener_(el, type, listener) {
    if (el.addEventListener !== undefined) {
        el.addEventListener(type, listener);
    } else if (el.attachEvent !== undefined) {
        el.attachEvent('on' + type, listener);
    }
}

// イベントリスナーを削除
function delEventListener_(el, type, listener) {
    if (el.removeEventListener !== undefined) {
        el.removeEventListener(type, listener);
    } else if (el.detachEvent !== undefined) {
        el.detachEvent('on' + type, listener);
    }
}


/******************************************************/
// 外部ページへのデータ渡し
//   url      渡し先URL
//   query    送信するデータ(key:xxx val:xxx)
//   name     送信ターゲット名
//   meth     POST or GET
//   関数値   なし
/******************************************************/
function TcOpenHtml( url, query, name, meth ) {
        var win = window.open( "", name );
        
// formを生成
        var fm = document.createElement("form");
        fm.target = name;
        fm.action = url;
        fm.method = meth;
        for( var i = 0; i < query.length; i++ ) {
            var kv = query[i];
            var key = kv["key"];
            var val = kv["val"];
            if ( key ) {
                var input = document.createElement("input");
                val = ( val != null ? val : "" );
                input.type = "hidden";
                input.name = key;
                input.value = val;
                fm.appendChild(input);
                }
        }
        // 一時的にbodyへformを追加。サブミット後、formを削除する
        var body = document.getElementsByTagName("body")[0];
        body.appendChild(fm);
        fm.submit();
        body.removeChild(fm);
}



/******************************************************/
// kintoneと通信を行うクラス
/******************************************************/
var KintoneRecordManager = (function() {
    KintoneRecordManager.prototype.records = [];	// 取得したレコード
    KintoneRecordManager.prototype.appId = null;	// アプリID
    KintoneRecordManager.prototype.query = '';		// 検索クエリ
    KintoneRecordManager.prototype.limit = 10;		// 一回あたりの最大取得件数
    KintoneRecordManager.prototype.offset = 0;		// オフセット

    function KintoneRecordManager() {
        this.appId = kintone.app.getId();
    }

    // すべてのレコード取得する
    KintoneRecordManager.prototype.getRecords = function(callback) {
        kintone.api(
            kintone.api.url('/k/v1/records', true),
            'GET',
            {
                app: this.appId,
                query: kintone.app.getQueryCondition() + (' limit ' + this.limit + ' offset ' + this.offset)
            },
            (function(_this) {
                return function(res) {
                    var len;
                    Array.prototype.push.apply(_this.records, res.records);
                    len = res.records.length;
                    _this.offset += len;
                    if (len < _this.limit) { // まだレコードがあるか？
                        _this.ready = true;
                        if (callback !== null) {
                            callback(_this.records); // レコード取得後のcallback
                        }
                    } else {
                        _this.getRecords(callback); // 自分自身をコール
                    }
                };
            })(this)
        );
    };
    return KintoneRecordManager;
})();

