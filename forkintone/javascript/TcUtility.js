/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TC基本Utility
//   どのアプリからでも呼び出されるモジュール集
//
//
// ▼どこでも利用
//  - TcPutDmyField_Sp().........要素IDで指定したスペース(空白パーツ)をダミーフィールドに置き換え
//  - TcGetDmyField_Val()........フィールドIDで指定したダミーフィールドのValue値を取得
//  - TcPutFieldMsg()............フィールドIDで指定した通常フィールドに対してメッセージを表示
//  (具体的な機能)
//    - TcChangeColorTbl().......特定のチェックがされたテーブル行の背景色を変更
//    - TcCalcTerm().............開始日～終了日から期間(日数)を算出
//    - TcAppDataPost()..........アプリIDで指定した同サブドメイン内の他アプリを新規編集で開いてデータ転記
//    - TcComputeDate()..........n日後の日付を取得
//    - TcCreateElement()........要素の生成
//
// ▼イベントハンドラで利用
//  - TcPutButton_Cd()...........フィールドコードで指定した通常フィールドに対してボタンを配置(詳細画面のみ対応)
//  - TcPutButton_Sp()...........要素IDで指定したスペース(空白パーツ)に対してボタンを配置(詳細・追加・編集画面対応)
//  - TcPutButton_Index()........一覧画面のメニューの右or下側にボタンを配置(一覧画面のみ対応)
//  - TcTextSizeChange().........詳細画面表示時に、指定したフォントサイズに変更
//  - TcFldConvLink()............フィールドのリンク化
//  - TcConvMapLink()............住所のgoogleマップリンク化
//  - TcSetHistoryBackButton()...前の画面に戻るボタン配置
//  - TcSetFieldShown()..........指定された項目の表示・非表示切替え
//  - TcMoveTableToGroup().......フォーム上のサブテーブル要素をグループ内に移動させる
//  - TcCreateTableValue().......新規入力時にサブテーブルに初期値をセットする
//  - TcPutButtonLabel().........指定アプリのアクションメニューボタンに対してスタイルをあてる
//  - TcAddTotalRow()............一覧ページで先頭行に数値の合計行を表示する
//  - TcAdjustTableColum().......サブテーブル・関連レコード一覧の列幅を自動調整
//  - TcCalcAge()................生年月日から今の年齢を求める
//
// ▼イベントハンドラ以外で利用
//  - TcPutButton_Id()...........フィールドIDで指定した通常・テーブルフィールドに対してボタン配置
//  - TcDisplayField()...........フィールドIDで指定した通常・テーブルフィールドの表示非表示切替
//  - TcDisplayFieldAll()........指定されたフィールドの表示非表示を切り替える
//  - TcGetRadioChecked()........フィールドIDで指定したラジオボタンの選択値を取得
//  - TcGetChkBoxChecked().......フィールドIDで指定したチェックボックスの選択値を取得
//  - TcGetDropDown()............フィールドIDで指定したドロップダウンの選択値を取得
//  - TcGetTimeText()............フィールドIDで指定した時刻フィールドの値を取得
//  - TcGetUserText()............フィールドIDで指定したユーザフィールドの選択値を取得
//  (具体的な機能)
//    - TcClassifySumTbl().......テーブルの指定数値をドロップダウンの選択値(種目)別に合計値算出
//    - TcTblSeqOperate()........テーブルの行追加削除ボタンを、チェックボックスの状態によって制御
//    - TcPostTblChkData().......テーブルのチェックされた行の指定値(金額など)を、決定値として別フィールドに転記
//    - TcCalcTblSubTtl()........テーブル行毎の小計、テーブル全体の小計を計算してフィールド更新
//    - TcPostFieldData()........指定したフィールドの値を別のフィールドに差し込む
//    - TcPostDDtoLU()...........ルックアップ取得内容をドロップダウンの内容で絞り込む
//    - TcConvLinkTarget().......画面上のリンクを同一ウインドウで開くように変更
//    - TcPutCustomGrid_H()......縦棒グラフに対してTCカスタムグリッド(水平)を配置
//
// ▽モジュール
//  - TcSwCopy().................別アプリに転記するためのモジュール(TcAppDataPost()等で利用)
//  - TcRetObjVal()..............JSONオブジェクトのValue値を返す
//  - TcCallPHP()................外部PHPの呼び出し実行
//  - TcMakeButton().............ボタン要素を作成・返す
//  - TcFilLookUpFld()...........ドロップダウン選択値をルックアップ入力欄に転記
//  - TcSendReq()................引数のURLへリクエストを送信
//  - TcCreateHttpRequest()......XMLHttpRequestオブジェクト生成関数
//
// ▼メモ
// ※詳細画面表示時のテーブルに対してボタン配置やフィールド制御はできない
//   画面表示時にテーブルが表示されきれていない場合があり、テーブルサイズが取得できない為である
//
// ▼更新履歴
// 20160302	TcSendReq(), TcCreateHttpRequest()追加 (m.takeuchi)
// 20150910	TcPutButton_Sp()改修 (m.takeuchi)
// 20141010	TcPutCustomGrid_H()追加 (m.takeuchi)
// 20140919	TcPutButtonLabel()追加 (m.takeuchi)
// 20140912	TcCreateTableValue()追加 (m.takeuchi)
// 20140911	TcSetFieldShown()追加 (m.takeuchi)
// 20140904	TcSetHistoryBackButton(), TcConvLinkTarget()追加 (m.takeuchi)
// 20140903	TcCreateElement()追加 (m.takeuchi)
// 20140718	TcFldConvLink(), TcConvMapLink()追加 (m.takeuchi)
// 20140714	TcDisplayFieldAll()追加 (j.kataoka)
// 20140520	TcGetChkBoxChecked内のinnerText→textContentに変更 (a.oono)
// 20140117	新規作成 (m.takeuchi)
/*--------------------------------------------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★TCUtility(JS)
//   - TC社の基本的なユーティリティ関数で、どのアプリからでも利用される想定のもの
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
/*
    jQuery(function($) {

        // 「郵便番号-住所検索API」を利用
        // 参照元URL:http://zipaddress.net/
//                var zip = $(this).val();
        var zip = "2410826";
        var url = 'https://api.zipaddress.net?callback=?';
        var query = {'zipcode': zip};
        $.getJSON(url, query, function(json){
 
            * json.pref        都道府県の取得
             * json.address     市区町村の取得
             * json.fullAddress 都道府県+市区町村の取得 

//            $('#pref').val(json.pref);
//            $('#addr').val(json.address);

            var record = kintone.app.record.get();
            record['record'][ config["prm_addr1"] ]['value'] = json.pref;
            kintone.app.record.set(record);
            return event;

console.log(json); 

        });

    });
*/
    function TcSetAddress(pConf){

/*
//        var record = kintone.app.record.get();
//        var zip = record['record'][ pConf["prm_zip"] ]['value'];
        var zip = "2410826";
        var url = 'https://api.zipaddress.net?callback=?';
        var query = {'zipcode': zip};
        jQuery.getJSON(url, query, function(json){
*/ 
            /*
            ** json.pref        都道府県の取得
            ** json.address     市区町村の取得
            ** json.fullAddress 都道府県+市区町村の取得
            */

//            $('#pref').val(json.pref);
//            $('#addr').val(json.address);
/*
            var record = kintone.app.record.get();
            record['record'][ pConf["prm_addr1"] ]['value'] = json.pref;
            record['record'][ pConf["prm_addr2"] ]['value'] = json.address;
            kintone.app.record.set(record);
            return event;
        });
*/





    }

// --------------------------------
// ▼どこでも利用
// --------------------------------
    /******************************************************/
    // ダミーフィールド配置
    //  - 要素IDで指定したスペース(空白パーツ)をダミーフィールドに置き換え
    //   FldCd		置き換え先スペース要素ID
    //   FldLbl		表示するラベル
    //   FldVal		表示する値
    //   Wsize		フィールド横幅(省略可)
    //   Fsize		Valueのフォントサイズ(省略可)
    /******************************************************/
    function TcPutDmyField_Sp (FldCd, FldLbl, FldVal, Wsize, Fsize) {

        if ( Wsize == undefined || Wsize == "auto" ) { Wsize = "auto"; } else { Wsize += "px";}		// Wsizeが省略された場合、サイズは"auto"
        if ( Fsize == undefined ) { Fsize = 10; }			// Fsizeが省略された場合、規定フォントサイズは10pt

        // ダミーフィールド(div要素)作成
        var div1 = document.createElement('div');
        div1.setAttribute("style", "box-sizing: border-box; width: " + Wsize + "; height: auto;");
        div1.setAttribute("class", "control-gaia control-show-gaia");
        div1.innerHTML = "<div class='control-label-gaia'><span>" + FldLbl + "</span></div>"
                        + "<div class='control-value-gaia'><span class='dmyVal' style='white-space: nowrap; font-size:" + Fsize + "pt'>" + FldVal + "</span></div>"
                        + "<div class='control-design-gaia'></div>";
        div1.id = "DmyFld_" + FldCd;

        // FldCdのスペースをダミーフィールドに置き換え
        var tmpKmk = kintone.app.record.getSpaceElement(FldCd);
        // ボタン配置
        if ( isShowPage() ) {
            tmpKmk.parentNode.appendChild(div1);	// 詳細画面の場合
        } else {
            tmpKmk.appendChild(div1);				// 追加・編集画面の場合
        }
    }


    /******************************************************/
    // ダミーフィールドのValue値を取得
    //   FldId		ダミーフィールドのスペース要素ID
    //   関数値		フィールド値
    /******************************************************/
    function TcGetDmyField_Val(FldId) {

        // 指定ダミーフィールドをIDで取得
        var elDmy = document.getElementById("DmyFld_" + FldId);

        // 指定ダミーフィールドのValue値をreturn
        return elDmy.getElementsByClassName("dmyVal")[0].textContent;
    }


    /******************************************************/
    // フィールドメッセージ表示
    //  - 任意のメッセージを出力したい場合に使用
    //   FldId		表示対象フィールドID
    //   MsgKind	メッセージ種別 ("OK"/"ERR")
    //   MsgStr		表示メッセージ内容
    /******************************************************/
    function TcPutFieldMsg(FldId, MsgKind, MsgStr){

        if ( MsgKind == "OK" ) {
            var strClass = 'validator-valid-cybozu';	// ルックアップ取得メッセージ
        } else if ( MsgKind == "ERR" ) {
            var strClass = 'input-error-cybozu';		// ルックアップ入力漏れメッセージ
        }

        var check = getFieldValueEl_(FldId).getElementsByClassName( strClass );
        if(check.length == 0){
            // 必須メッセージがない場合のみ、エレメント追加
            var el = document.createElement('div');
            el.className = strClass;
            el.innerHTML = MsgStr;
            getFieldValueEl_(FldId).appendChild(el);
        }
    }


    /******************************************************/
    // チェックされたテーブル行の背景色を変更する
    //  - チェックボックス・ラジオボタンに対応
    //   FldId		チェック対象フィールドID
    //   SelStr		チェックされたら背景色を変える選択文字
    //   COLOR		変更する色
    /******************************************************/
    function TcChangeColorTbl (FldId, SelStr, COLOR) {

        /*
        ** フィールドIDを配列に変更
        */
        if ( Array.isArray(FldId) == true ) {
            var aryPutFld = FldId;		// フィールドIDを配列で指定した場合
        } else {
            var aryPutFld = [FldId];	// フィールドIDを配列以外で指定した場合
        }

        for ( var cnt = 0; cnt < aryPutFld.length; cnt++ ) {

            var FldId = aryPutFld[cnt];

            // すべての行に対して背景色変更を適用する
            for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

                if ( isShowPage() ) {
                    // [仮入力]チェック有の場合、その行の背景色を変更する
                    var State = getFieldValueFromTable(FldId, row);		// [仮入力]の値を取得
                    if ( State.indexOf(SelStr) != -1 ) {
                        getFieldValueEls_(FldId, row).parentNode.parentNode.parentNode.setAttribute("style", COLOR);
                    }

                } else if ( isAddPage() || isEditPage() ) {
                    /*
                    ** クラス名からフィールド種別を判定しチェック状態を判定
                    */
                    var clsName = getFieldValueEls_(FldId, row).childNodes[0].className;
                    if ( clsName.indexOf("input-checkbox-cybozu") != -1 ) {
                        var State = TcGetChkBoxChecked(FldId, row);	// チェックされたボックス名
                    } else if ( clsName.indexOf("input-radio-cybozu") != -1 ) {
                        var State = TcGetRadioChecked(FldId, row);		// チェックされたボタン名
                    }

                    /*
                    ** テーブル行の色を変更する
                    */
                    var elTrgRow = getFieldValueEls_(FldId,row).parentNode.parentNode.parentNode;
                    if ( State.indexOf(SelStr) != -1 ) {
                        elTrgRow.setAttribute("style", COLOR);	// 指定文字のチェック有り：対象行の背景色を変更
                    } else {
                        elTrgRow.setAttribute("style", "");		// 指定文字のチェック無し：対象行の背景色を元に戻す
                    }

                } else { return 0; }
            }
        }
    }


    /******************************************************/
    // 開始日～終了日までの期間(日数)を求める
    //  - kintoneの計算フィールドでも計算できるが、値の書き換えをしたい際に使用する
    //    ★経過年等を求めるように拡張も可能(現段階2014/01/08では必要ない為書かない)
    //   strSrt		開始日
    //   strEnd		終了日
    //   関数値		日数
    /******************************************************/
    function TcCalcTerm(strSrt, strEnd) {

        // 取得文字列を日付型(システム経過秒)に変換
        var datSrtDate = Date.parse(strSrt);	// 開始日
        var datEndDate = Date.parse(strEnd);	// 終了日
        var ret = "";							// 戻り値

        // [着工予定日]と[完工予定日]が空白でない場合、工期を求める
        if ( strSrt != undefined && strEnd != undefined ) {
            ret = Math.ceil((datEndDate - datSrtDate) / (60*60*24*1000) + 1);	// 開始日から終了日までの日数
        }

        return ret;

    }


    /******************************************************/
    // 他アプリへデータをPOSTする汎用関数
    //   AppId      転記先アプリID
    //   Query      転記するデータ(形式はTcSwCopy()参照)
    //   RecId      詳細モードで開くレコード番号(省略可)
    //   関数値     なし
    /******************************************************/
    function TcAppDataPost(AppId, Query, RecId) {

        // 環境変数取得
        var prm = TcGetEnvPrm();

        // ウインドウ名の生成("childwin時分秒")
        var DD = new Date();
        var Hours = DD.getHours();
        var Minutes = DD.getMinutes();
        var Seconds = DD.getSeconds();
        var win_name = "childwin" + Hours + Minutes + Seconds;

        // 転記先アプリ
        if ( RecId == undefined || RecId < 1 ) {
            // レコード番号指定無しの場合：編集モードで開く
            var appURL = "https://" + prm['サブドメイン'] + ".cybozu.com/k/" + AppId + "/edit";
console.log("EditMode");
        } else {
            // レコード番号指定有りの場合：詳細モードで開く
            var appURL = "https://" + prm['サブドメイン'] + ".cybozu.com/k/" + AppId + "/show#record=" + RecId;
            Query = {};
console.log("ViewMode");
        }
        win = window.open(appURL, win_name);

        /*
        ** 別窓のアプリにTcSwCopy()でデータ転記
        ** アプリの表示、JS読み込みのタイミングをずらす(setInterval)
        */
        var tc = 0;
        timerID = setInterval( function() {
                                if(TcSwCopy(win, Query)) {
                                    clearInterval(timerID);
                                    timerID = null;
                                } else {
                                    tc++;
                                    if(tc>3) {
                                        clearInterval(timerID);
                                        timerID = null;
                                    }
                                }
                            }, 2500);
    }


    /******************************************************/
    // n日後の日付を取得
    // strDate		日付("yyyy-mm-dd"形式)
    // addDays		加算日(マイナス指定でn日前も設定可能)
    /******************************************************/
    function TcComputeDate(strDate, addDays) {

        // 日付がない(null)の場合はreturn
        if ( strDate == null ) { return (null); }

        // yyyy-mm-dd形式を年月日に分割
        var year  = strDate.split("-")[0];	// 年
        var month = strDate.split("-")[1];	// 月
        var day   = strDate.split("-")[2];	// 日

        // addDays日後の日付を求める
        var dt = new Date(year, month - 1, day);
        var baseSec = dt.getTime();
        var addSec = addDays * 86400000;		//日数 * 1日のミリ秒数
        var targetSec = baseSec + addSec;
        dt.setTime(targetSec);

        // yyyy-mm-dd形式にする
        var yy = dt.getFullYear();
        var mm = ("0" + (dt.getMonth() + 1)).slice(-2);
        var dd = ("0" + (dt.getDate())).slice(-2);
        var retDate = yy + "-" + mm + "-" + dd;

        return retDate;

    }


    /******************************************************/
    // 要素の生成
    // strName		タグ名("div"や"a"など)
    // pPrm		プロパティのオブジェクト (pPrm{"className":"Tcxxxx",[..]}
    // pTxt		要素内のテキスト指定(textContent)
    /******************************************************/
    function TcCreateElement(strName, pPrm, pTxt, pCls) {

        // 新しい要素を生成
        var newElm = document.createElement(strName);
        

        // 対応するkeyの属性値を設定する
        for ( key in pPrm ) { newElm.setAttribute( key, pPrm[key]); }

        // 指定されたtextContentを設定する
        if ( pTxt ) { newElm.textContent = pTxt; }

        // クラス名設定
        if ( pCls ) { newElm.className = pCls; }

        return newElm;

    }


    /******************************************************/
    // 文字列・数値型フィールド間の値転記を行う
    //   trgId	差し込み先フィールドID
    //   srcId	差し込み元フィールドID
    /******************************************************/
    function TcPostingSingleInputValue(trgId, srcId) {

        var srcVal = getSingleLineInputValue(srcId).split(",").join("");	// 数値の場合の桁区切りを除去
        // 差し込み元が有効な値の場合に転記する
        if ( srcVal != "#N/A!" ) {
            getSingleLineInput(trgId).value = srcVal;
        }

    }

// --------------------------------
// ▼イベントハンドラで利用
// --------------------------------
    /******************************************************/
    // コードで指定されたフィールドに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //    ※詳細画面でのみ利用可能
    //   FldCd		ボタン配置先フィールドコード
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Cd(FldCd, BtnLbl, FncName, BtnTag, SetStyle) {

        // 詳細画面でない場合は処理しない
        if ( !isShowPage() ) {
            console.log("TcPutButton_Cdは、レコード詳細画面でのみ利用可能です。");	// 追加・編集画面ではgetFieldElement()は対応していない
            return;
        }

        // ★フィールドコードで指定した要素を取得
        var tmpKmk = kintone.app.record.getFieldElement(FldCd);

        // ボタンパーツを取得
        var emButton = TcMakeButton(FldCd, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        tmpKmk.parentNode.appendChild(emButton);	// 詳細画面の場合

    }


    /******************************************************/
    // 要素IDで指定されたスペースに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   ElmId		ボタン配置先スペース要素ID
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Sp(ElmId, BtnLbl, FncName, BtnTag, SetStyle) {

        // ★画面のスペース(空白)要素を取得
        var tmpKmk = kintone.app.record.getSpaceElement(ElmId);

        // スペースのサイズに合わせたボタンスタイル
        if ( SetStyle == "fit" ) { SetStyle = tmpKmk.parentNode.getAttribute("style"); }	// Add 20150910 (m.takeuchi)


        // ボタンパーツを取得
        var emButton = TcMakeButton(ElmId, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        if ( isShowPage() ) {
            tmpKmk.parentNode.appendChild(emButton);	// 詳細画面の場合
        } else {
            tmpKmk.appendChild(emButton);				// 追加・編集画面の場合
        }

    }


    /******************************************************/
    // 一覧画面のメニューの右or下側にボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   PosCd		ボタン配置先指定(right/bottom)
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Index(PosCd, BtnLbl, FncName, BtnTag, SetStyle) {

       // ★指定したポジション(右・下)の要素を取得
        if ( PosCd == "right" ) {
            if ( isShowPage() ) {
                // 詳細画面の場合
                var tmpKmk = kintone.app.record.getHeaderMenuSpaceElement();		// メニュー右側の空白
            } else {
                // 一覧画面の場合
                var tmpKmk = kintone.app.getHeaderMenuSpaceElement();		// メニュー右側の空白
            }
        } else if ( PosCd == "bottom" ) {
            var tmpKmk = kintone.app.getHeaderSpaceElement();			// メニュー下側の空白
        } else {
            console.log("PosCdの指定が不正です。");
        }

        // ボタンパーツを取得
        var emButton = TcMakeButton(PosCd, BtnLbl, FncName, BtnTag, SetStyle);

        // ボタン配置
        tmpKmk.appendChild(emButton);

    }


    /******************************************************/
    // 全体の文字サイズを変更
    //   sfontsize	フォントサイズ(pt)
    /******************************************************/
    function TcTextSizeChange(sfontsize) {
/*
        // 旧ロジック(20140713 kintoneアップデート以降動作が不正になったため書き換え)
        var odate = document.getElementsByClassName ('row-gaia clearFix-cybozu');
        if ( odate.length > 0 ) {
            for ( var odate_cnt = 0; odate_cnt < odate.length; odate_cnt++ ) {
                for ( var odate_Child_cnt = 0; odate_Child_cnt < odate[odate_cnt].childNodes.length; odate_Child_cnt++ ) {
                    if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes.length > 2 ) {
                        if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[1].childNodes.length == 1 ) {
                            if ( odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[0].innerHTML.indexOf("<span>") == 0 ) {
                                 odate[odate_cnt].childNodes[odate_Child_cnt].childNodes[1].childNodes[0].setAttribute("style", "font-size:" + sfontsize + "pt;");
                            }
                        }
                    }
                }
            }
        }
*/

        // フィールド値の要素をすべて取得し、フォントサイズ指定する
        var elVal = document.getElementsByClassName('control-value-gaia');
        for ( var cnt = 0; cnt < elVal.length; cnt++ ) {

            // ★取得した要素にsubtableを含むか判定
            var eltmp = elVal[cnt].getElementsByClassName('subtable-gaia');
            if ( !eltmp.length ) {
                // subtableを含まない(一般要素)場合、フォントサイズを変更する
                elVal[cnt].setAttribute("style", "font-size:" + sfontsize + "pt;");
            } else {
                // subtableを含む(関連レコード一覧要素)場合、列幅をauto指定
                elVal[cnt].parentNode.style.width = "auto"
            }

        }

    }

    /******************************************************/
    // フィールドのリンク化
    //  FldId	リンク化したいフィールドのID
    //  prmUrl	リンク先URL
    //  prmTrg	ウインドウの開き方 ※省略可(true渡せば_blankで開く)
    /******************************************************/
    function TcFldConvLink(FldId, prmUrl, prmTrg) {

        var valAddr = getFieldValue(FldId);						// フィールド入力値

        // <a>タグ生成
        var elHref = document.createElement('a');				// <a>タグ生成
        elHref.href = prmUrl;									// リンク先
        if ( prmTrg ) { elHref.target = "_blank"; }				// 新規ウインドウで開く
        elHref.textContent = valAddr;							// 表示文字

        // 要素を<span>から<a>タグに書き換える
        var elVal = getFieldValueEl_(FldId);					// 要素取得
        elVal.removeChild(elVal.childNodes[0]);					// <span>タグ(表示値)を削除
        elVal.appendChild(elHref);								// <a>タグを追加

    }

    /******************************************************/
    // 住所フィールドをMAPリンク化する
    //  - フィールド名にキーワードを含むフィールド値を<a>タグ化
    //   strKeyWord		キーワード
    //   関数値			なし
    /******************************************************/
    function TcConvMapLink(strKeyWord) {

        /*
        ** ★フィールド名に指定の文字を含む場合、フィールド値をMAPリンク化する
        */
        // すべてのフィールド名要素を取得して文字判定
        var elLbl = document.getElementsByClassName("control-label-gaia");
        for ( var cnt = 0; cnt < elLbl.length; cnt++ ) {

            // フィールド名(ラベル)に指定文字を含む場合then
            if ( elLbl[cnt].childNodes[0].textContent.indexOf(strKeyWord) != -1 ) {

                var strClsName = elLbl[cnt].parentNode.childNodes[1].className;		// クラス名文字列
                var aryClsName = strClsName.split(" ");								// 空白で分割したクラス名配列

                // 分割したクラス名からフィールドIDを取得する
                for ( var lcnt = 0; lcnt < aryClsName.length; lcnt++ ) {

                    // クラス名を"-"で分割した作業用配列
                    var aryWrk = aryClsName[lcnt].split("-");

                    // aryWrkが ["value", 数値] の場合、数値をフィールドIDとして取得する
                    if ( aryWrk[0] == "value" && !aryWrk[1].match(/[^0-9]+/) ) {
                        var FldId = aryWrk[1];		// フィールドID確定
                    }

                }

                // フィールドIDが決まった場合、そのフィールド値をMAPリンク化する
                if ( FldId ) {
                    var urlMap = "https://www.google.co.jp/maps/place/";	// googleマップURL
                    var valAddr = getFieldValue(FldId);						// [住所]入力値
                    TcFldConvLink(FldId, urlMap + valAddr, true);			// [住所]フィールドの<a>マップリンク化
                }

            }
        }

    }


    /******************************************************/
    // 前の画面に戻るボタンを配置
    //  - 一覧画面・詳細画面のイベントハンドラでのみ動作
    /******************************************************/
    function TcSetHistoryBackButton() {

        // 戻るボタンの追加
        if ( isViewPage() ) {
            // 一覧ページの場合
            var elMenuSpace = kintone.app.getHeaderMenuSpaceElement();
        } else if ( isShowPage() ) {
            // 詳細ページの場合
            var elMenuSpace = kintone.app.record.getHeaderMenuSpaceElement().parentNode.firstChild;
        } else {
            // 上記以外のページの場合、ボタン配置しない
            return;
        }
        
        if ( !document.getElementById("btn_TcBack") ) {
            elMenuSpace.appendChild( TcMakeButton("TcBack", "戻る", function(){ history.back(); }) );
        }

    }


    /***********************************************/
    // 指定された項目の表示・非表示切換え
    // aryField     フィールドID(配列)
    // pShown       true:表示 / false:非表示
    // 関数値       なし
    /***********************************************/
    function TcSetFieldShown( aryField, pShown ) {
        var i;
        for( i=0; i<aryField.length; i++ ) {
            kintone.app.record.setFieldShown(aryField[i], pShown); // 表示・非表示
        }
    }


    /******************************************************/
    // フォーム上のサブテーブル要素をグループ内に移動させる
    //  - フィールドIDを指定してテーブルをグループ内の指定位置に要素移動
    //   srcId		移動元テーブルID
    //   trgId		移動先グループID
    //   stdId		移動先の差し込み基準フィールドID(省略可)
    //   posCode	差し込み基準フィールドの前後どちらの行に追加するか(省略可)
    /******************************************************/
    function TcMoveTableToGroup(srcId, trgId, stdId, posCode) {

        // 要素取得
        var elTable = document.getElementsByClassName('subtable-' + srcId)[0];	// テーブル要素
        var elGroup = document.getElementsByClassName('field-' + trgId)[0];		// グループ要素

        // グループ内用<div>行要素生成
        var elDivRow = TcCreateElement("div", {"class":"row-gaia clearFix-cybozu", "style":"position:relative;"});
        elDivRow.appendChild(elTable);

        // 差し込み基準フィールド指定の有無でテーブル差し込み位置を変更
        if ( stdId ) {
            // 指定有：基準フィールドの(前/次)の行に差し込み
            var elStdFld = document.getElementsByClassName('field-' + stdId)[0];
            var elStdRow = elStdFld.parentNode;
            if ( posCode == "before" ) {
                elPosition = elStdRow;					// 前の行に追加
            } else {
                elPosition = elStdRow.nextSibling;		// 次の行に追加
            }
            elGroup.insertBefore(elDivRow, elPosition);	// insertBefore()で要素追加
        } else {
            // 省略：グループ末尾に差し込み
            elGroup.appendChild(elDivRow);							// appendChild()でelGroupの末尾に要素追加
        }

    }


    /******************************************************/
    // 新規入力時にサブテーブルに初期値をセットする
    //   record		レコード情報(event.record)
    //   tblCode	初期値設定対象のテーブル名
    //   objPrm		初期値を指定したオブジェクト配列
    //              ( objPrm = [ {"FldCode":"FldValue", ...}, ...} )
    /******************************************************/
    function TcCreateTableValue(record, tblCode, objPrm) {

        // 追加画面ではない場合return
        if ( !isAddPage() ) { return; }

        // 指定パラメータの型を調べる為に実際のテーブル情報を取得
        for ( var key in record ) {
            if ( key == tblCode && record[key]['type'] == "SUBTABLE" ) {
                // 指定したサブテーブルが見つかった場合
                var tblField = record[key]['value'][0]['value'];	// 指定サブテーブルのオブジェクト取得
                continue;
            }
        }
        if ( !tblField ) { return; }		// 指定サブテーブルが見つからなかった場合は以降処理しない

        var objRet = new Array();			// サブテーブル初期値設定用オブジェクト配列

        // 指定パラメータの配列数分処理
        for ( var cnt = 0; cnt < objPrm.length; cnt++ ) {

            objRet[cnt] = {};
            objRet[cnt]['value'] = {};

            // ★指定サブテーブル内のすべてのフィールド(key要素)分オブジェクトを初期化
            for ( var key in tblField ) {
                objRet[cnt]['value'][key] = {};
                objRet[cnt]['value'][key]['type'] = tblField[key]['type'];		// 型指定
                var tblVal = "";
                if ( objPrm[cnt][key] ) { tblVal = objPrm[cnt][key]; }			// 値が指定されている場合のみ
                objRet[cnt]['value'][key]['value'] = tblVal;					// 値指定
            }

        }
        // サブテーブルに対して初期値を設定
        record[tblCode]['value'] = objRet;

    }


    /******************************************************/
    // 指定アプリのアクションメニューボタンに対してスタイルをあてる
    //   appId		ボタンスタイルを変更するアプリID
    /******************************************************/
    function TcPutButtonLabel(aryAppId) {

        // ★指定アプリ以外は処理しない
        //   ※アプリIDが0の場合はすべてを対象とする
        for ( var cnt = 0; cnt < aryAppId.length; cnt++ ) {

            if ( kintone.app.getId() == aryAppId[cnt] || aryAppId[cnt] == 0) {


                if ( kintone.getUiVersion() != 2 ) {
                    // 旧デザインの場合
                    var elMenuUl = document.getElementsByClassName("gaia-ui-actionmenu-left")[0];
                    var strCls = " tc-menubtn-opelate";
                } else {
                    // 新デザインの場合
                    var elMenuUl = document.getElementsByClassName("gaia-argoui-app-toolbar-menu")[0];
                    var strCls = " tc-toolbar-app-menu";
                }


                for ( var cnt in elMenuUl.childNodes ) {
                    if ( !isNaN(cnt) ) {
                        // クラス名付与
                        var elIcon = elMenuUl.childNodes[cnt];
                        if ( elIcon.className == "" ) { continue; }		// 縦棒要素は処理しない
                        elIcon.className += strCls;

                        // titleプロパティを表示する
                        if ( kintone.getUiVersion() != 2 ) {
                            // 旧デザインの場合
                            var strTitle = elIcon.childNodes[0].title;
                            var elMenuLbl = TcCreateElement("div", {}, strTitle);
                            elIcon.childNodes[0].appendChild( elMenuLbl );
                        } else {
                            // 新デザインの場合(追加・編集・再利用・印刷ボタンのラベルを出す)
                            if ( cnt >= 4 ) { continue; }
                            var strTitle = elIcon.childNodes[0].textContent;
                            var elMenuLbl = TcCreateElement("span", {}, strTitle, "tc-toolbar-menu-content");
                            elIcon.appendChild( elMenuLbl );
                        }

                    }
                }

                return;		// 終了

            }
        }

    }


    /******************************************************/
    // 一覧ページで先頭行に数値の合計行を表示する
    //   pEvent			イベント情報
    //   pCode			対象となるフィールドのコード
    /******************************************************/
    function TcAddTotalRow (pEvent, pCode) {

        var records = pEvent.records;

        // データがない・カレンダービューの場合は処理しない
        if ( !records.length ) { return; }

        // ★一番上の行のクローンを生成
        var elTbody = document.getElementsByTagName("tbody")[0];
        var elCloneRow = elTbody.childNodes[0].cloneNode(true);
        elCloneRow.ClassName = "tc_TotalRow";

        // ★生成したクローン行の初期化(表示値をクリア)
        for ( var cnt1 in elCloneRow.childNodes ) {
            if ( isNaN(cnt1) ) { continue; }							// 数値でない場合は弾く
            var elCell = elCloneRow.childNodes[cnt1].childNodes[0];
            elCell.removeChild( elCell.firstChild );					// 表示内容の要素を削除
        }
        elCell.removeChild( elCell.firstChild );						// 右端のごみ箱ボタンを消す(専用対策)

        // ★全レコード情報から求めた合計値をクローン行に表示する
        var manager = new KintoneRecordManager;
        manager.getRecords( function(records) {

            // 指定フィールドコード単位に処理する
            for ( var cnt1 in pCode ) {
                // フィールドコードから取得したクラス名と一致する要素
                var elFld = kintone.app.getFieldElements(pCode[cnt1]);
                if ( !elFld ) { continue; }
                var elCell = elCloneRow.getElementsByClassName( elFld[0].className )[0];
                // 表示内容を構成してelCellに追加する
                var elSpan = document.createElement("span");
                var sumVal = 0;
                for ( var cnt2 in records ) { sumVal += Number(records[cnt2][pCode[cnt1]]['value']); }	// 合計値取得
                elSpan.textContent = String(sumVal).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );		// 正規表現でカンマ区切り
                elCell.childNodes[0].appendChild(elSpan);
            }

        });

        // ★先頭行に追加(※すでにある場合は処理しない)
        if ( !document.getElementsByClassName("tc_TotalRow")[0] ) {
            elTbody.insertBefore(elCloneRow, elTbody.firstChild);
        }

    }

    /******************************************************/
    // サブテーブル・関連レコード一覧の列幅を自動調整
    //   pRecord		レコードの情報
    //   pCode			対象となるフィールドのコード
    //   - ※対象外フィールドコード指定時は処理しない
    /******************************************************/
    function TcAdjustTableColum(pRecord, pCode) {

        /*
        ** 指定したフィールドコードの型によって列幅調整内容を切り替える
        **  - サブテーブルの場合    ：すべての列を調整対象とする
        **  - 関連レコード一覧の場合：一番左のアクションボタンを列幅調整対象外とする
        **  - 上記以外の場合        ：対象外
        */
        if ( pRecord[pCode] ) {
            if ( pRecord[pCode]['type'] == "SUBTABLE" ) {
                var loopStart = 0;	// サブテーブルの場合
            } else {
                return;				// サブテーブル以外の場合
            }
        } else {
            var loopStart = 1;		// 関連レコード一覧の場合
        }


        /*
        ** 関連レコード一覧の列幅調整
        **  - 列の項目種別によって設定されている"min-width"のサイズを0にして、文字長さ合わせる
        */ 
        var elTarg = kintone.app.record.getFieldElement(pCode);								// 指定フィールドの要素
        var elTr = elTarg.getElementsByTagName("thead")[0].getElementsByTagName("tr")[0];	// <tr>要素取得

        /*
        ** 横幅の調整
        */
        // 各列の幅最小幅を変更
        for ( var cnt = loopStart; cnt < elTr.childNodes.length; cnt++ ) {

            var elTarget = elTr.childNodes[cnt];

            // styleのmin-widthが設定されている場合のみ
            if ( elTarget.getAttribute("style","min-width") ) {
                elTarget.setAttribute("style","min-width:0px");
            }

        }
        // 関連レコード一覧自体の幅をauto
        elTarg.parentNode.style.width = "auto";

    }


    /******************************************************/
    // 生年月日から今の年齢を求める
    //   pDate		生年月日
    /******************************************************/
    function TcCalcAge (pDate) {
    
        if ( !pDate ) { return; }

        // 本日の日付(yyyymmdd)
        var today = new Date();
        today = today.getFullYear() * 10000 + today.getMonth() * 100 + 100 + today.getDate();

        // 生年月日(yyyymmdd)
        var birth = parseInt( pDate.replace(/-/g, '') );	// 区切り文字"-"を除去

        // (今日の日付 - 生年月日) / 10000 の小数点以下切捨て = 頭2桁が年齢
        return ( Math.floor( ( today - birth ) / 10000 ) );

    }


// --------------------------------
// ▼イベントハンドラ以外で利用
// --------------------------------
    /******************************************************/
    // IDで指定されたフィールドに対してボタンを配置する
    //  - クリックイベントを省略した場合は押せないボタンを配置
    //   FldId		ボタン配置先フィールドID
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    /******************************************************/
    function TcPutButton_Id(FldId, BtnLbl, FncName, BtnTag, SetStyle) {

        /*
        ** 引数FldIdをフィールドID配列に代入
        */
        if ( Array.isArray(FldId) == true ) {
            var aryPutFld = FldId;		// フィールドIDを配列で指定した場合
        } else {
            var aryPutFld = [FldId];	// フィールドIDを配列以外で指定した場合
        }

        if ( BtnTag == undefined ) { BtnTag = "btn"; }

        // 指定フィールドID個数分処理
        for ( var cnt = 0; cnt < aryPutFld.length; cnt++ ) {

            // ボタン配置対象を取得
            var FldId = aryPutFld[cnt];

            if ( typeof(FldId) != "number" ) { console.log("TcPutButton_Id()のFldIdは数値で指定してください。"); return 0; }	// FldIdが数値でない場合、リターン
            // 要素(Label/Value)を取得(ラベルのクラス名でテーブルか判定する為)
            var elLbl = getFieldLabelEl_(FldId);

            /*
            ** ボタンの配置方法によって処理を分ける
            **  - テーブルである場合：各行指定フィールドに対してボタンを配置。テーブル行の動的変更に対応する為、CALLの度にボタンIDを更新する。
            **  - テーブルでない場合：指定フィールドに対してボタンを配置。
            */
            if ( elLbl.className.indexOf("subtable") != -1 ) {

                /*
                ** テーブルの場合
                **  - テーブルのすべての行に対してボタン配置処理
                **    - 未配置の行には新規でボタンを配置
                **    - 配置済の行には既存ボタンのid情報を更新(id自体が行情報を保持するため)
                */
                for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

                    // 指定フィールドの要素数が2以下の場合のみボタンを置く(2個は必ずある)
                    if ( getFieldValueEls_(FldId, row).parentNode.childNodes.length < 3 ) {

                        // ボタンパーツを取得
                        var emButton = TcMakeButton(FldId, BtnLbl, FncName, BtnTag, SetStyle, row);

                        // 行毎にボタン配置
                        getFieldValueEls_(FldId, row).parentNode.appendChild(emButton);

                    } else {
                        /*
                        ** すでにボタン配置済みの場合、行番号でボタンidを更新する
                        */
                        var emButton = getFieldValueEls_(FldId, row).parentNode.lastChild;	// ボタン要素取得
                        emButton.id = 'btn' + FldId + '_' + row;							// ボタンID(行番号)更新
                    }

                }

            } else {

                /*
                ** テーブルではない場合
                **  - 指定された位置に新規でボタンを配置
                */
                // ボタンパーツを取得
                var emButton = TcMakeButton(FldId, BtnLbl, FncName, BtnTag, SetStyle);

                // ボタン配置
                if ( isShowPage() ) {
                    getFieldValueEl_(FldId).parentNode.appendChild(emButton);	// 詳細画面の場合
                } else {
                    getFieldValueEl_(FldId).appendChild(emButton);				// 追加・編集画面の場合
                }
            }

        }
    }


    /******************************************************/
    // 指定されたフィールドの表示非表示を切り替える
    //   FldId	対象フィールドID
    //   isDsp	表示:1 非表示:0
    /******************************************************/
    function TcDisplayField(FldId, isDsp) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(FldId);
        var elVal = document.getElementsByClassName("value-" + FldId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        // style.displayを書き換える値を設定(isDspが1の場合：空文字(表示)/ 0の場合："none"(非表示))
        if ( isDsp == 1 ) { DspState = ""; } else { DspState = "none"; }

        // DOM内部構造(style.display)を直接変更する
        // ※kintoneバージョンアップ後に動作しなくなる可能性あり
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルの場合
            elLbl.style.display = DspState;									// テーブルヘッダ<th>のlabel
            for ( var row = 0; row < elVal.length; row++ ) {
                elVal[row].parentNode.parentNode.style.display = DspState;	// テーブルデータ<td>のValue(行数分)
            }
        } else {
            // テーブルではない場合
            elVal[0].parentNode.style.display = DspState;					// LabelとValueごとdisplayを書き換える
        }

    }

    /******************************************************/
    // 指定されたフィールドの表示非表示を切り替える
    //   Fld	対象フィールドIDの配列
    //   isDsp	表示:1 非表示:0
    /******************************************************/
    function TcDisplayFieldAll(Fld, isDsp) {
        // 指定されたフィールド数分処理する
        for ( var cnt = 0; cnt < Fld.length; cnt++ ) {
            // display切り替えを行う
            TcDisplayField(Fld[cnt], isDsp);
        }
    }

    /******************************************************/
    // ラジオボタンの選択されたボタン名を取得
    //   FldId		アプリの項目のフィールドID
    //   row		テーブルの場合指定(行番号)
    //   関数値		選択されたボタン名
    /******************************************************/
    function TcGetRadioChecked(FldId, row) {

        // ラジオのボタングループ要素取得
        if ( row == undefined ) { row = 0; }
        var elRadio = getFieldValueEls_(FldId, row).childNodes[0];

        /*
        ** ラジオのすべてのボタンのうち、チェックされたボタン名を返す
        **  - ラジオボタンでは必ず1つなので見つかった時点でreturn
        */
        for ( var cnt = 0; cnt < elRadio.childNodes.length; cnt++ ) {
            if (elRadio.childNodes[cnt].childNodes[0].checked == true) {
                // ヒットした子ノードの値を返却
                return (elRadio.childNodes[cnt].childNodes[0].value);
            }
        }
    }


    /******************************************************/
    // チェックボックスの選択されたチェック名を取得
    //   FldId		アプリの項目のフィールドID
    //   row		テーブルの場合指定(行番号)
    //   関数値		選択されたチェック名(配列)
    /******************************************************/
    function TcGetChkBoxChecked(FldId, row) {

        // チェックボックス要素取得
        if ( row == undefined ) { row = 0; }
        var elCheck = getFieldValueEls_(FldId, row);

        var box = new Array();

        /*
        ** チェックされたボックス名を配列に格納
        **  - すべてのボックス要素を判定し、チェックされたボックス名をすべて格納してreturn
        */
        for ( var cnt = 0; cnt < elCheck.childNodes[0].childNodes.length; cnt++ ) {
            if (elCheck.childNodes[0].childNodes[cnt].childNodes[0].checked == true) {
                // チェックされたボックス名を配列に格納
                box.push( elCheck.getElementsByTagName("label")[cnt].textContent );
            }
        }
        return box;    // 配列を返す
    }


    /******************************************************/
    // ドロップダウンの選択された内容を取得
    //   fieldId	取得したいドロップダウンのフィールドID
    //   row		行番号
    //   関数値		取得した内容
    /******************************************************/
    function TcGetDropDown(fieldId, row) {
        if ( row == undefined ) { row = 0; }
        // 選択された文字を返却
        var strDD = getTextContent_(getFieldValueEls_(fieldId,row).getElementsByTagName("div")[0]);
        if ( strDD.indexOf("-----") != -1 ) { strDD = ""; }

        return strDD;
    }


    /******************************************************/
    // 時刻フィールド値取得
    //   FldId		アプリの項目のフィールドID
    //   関数値		取得した時刻文字列
    //            ※ テキストの書式はユーザ設定による
    /******************************************************/
    function TcGetTimeText(FldId, row) {

        var sRet = "";
        if ( row == undefined ) { row = 0; }

        /*
        ** 時刻値フィールドを取得して、valueを返却
        */
        var el = getFieldValueEls_(FldId,row).getElementsByClassName("input-time-cybozu")[0];
        if ( el != undefined ) {
            sRet = el.childNodes[0].value;
        }
        return sRet;

    }


    /******************************************************/
    // ユーザーフィールド値取得
    //   ※選択肢が指定されたフィールドの場合は非対応
    //   FldId		アプリの項目のフィールドID
    //   関数値		取得したユーザー名(複数ユーザーの場合、カンマ(,)区切り)
    /******************************************************/
    function TcGetUserText(FldId) {

        // TODO:配列で返却したいが、既存箇所でテキスト取得しているところを考慮して現状はsRetを返す(2014/01/17)
        var sRet = "";
//        var aryRet = new Array();

        /*
        ** 選択済みユーザリストを取得し、選択されたユーザの表示名を配列で返却
        */
        var el = getFieldValueEl_(FldId).getElementsByClassName("entity-list-inner-cybozu")[0];
        if ( el != undefined ) {
            // 選択ユーザ数分ループ
            for ( var cnt = 0; cnt < el.childNodes.length; cnt++ ) {
                // 選択されたユーザの表示名を取得し、配列に格納
                var userName = el.childNodes[cnt].getElementsByClassName("entity-user-cybozu")[0].innerHTML;
                if ( sRet == "" ) { sRet = userName; } else { sRet = sRet + "," + userName; }
//                aryRet.push(userName);
            }
        }

        return sRet;
    }


    function TcPostFldVal() {}

    /******************************************************/
    // テーブルの合計値を分類別に保持したオブジェクトを取得
    //   objSum		分類別合計値保持オブジェクト
    //   ClsId		分類ドロップダウンフィールドID
    //   TrgId		合計対象フィールドID
    //   関数値		objSum[ {分類名:合計値}, {...} ]
    /******************************************************/
    function TcClassifySumTbl(objSum, ClsId, TrgId) {

        /*
        ** objSum[ {分類名:合計値}, {...} ] の形で分類別の合計値を保持する
        */
        for ( var row = 0; row < getFieldValueLength(ClsId); row++ ) {

            // ドロップダウンの選択値を取得
            var strDD = TcGetDropDown(ClsId, row).replace(/\s+/g, "");

            // 初出の分類の場合、初期化(0クリア)
            if ( objSum[strDD] == undefined ) { objSum[strDD] = 0; }

            // [金額]のフィールド値を数値として取得
            var numVal = parseInt(getFieldValueFromTableInputValue(TrgId, row).split(",").join(""));

            // [金額]フィールドがNaNではない(数値)の場合のみ処理
            if( isNaN(numVal) == false ) {
                // 合計対象フィールド(TrgId)を分類毎(ClsId)に分けて足しこむ
                objSum[strDD] += numVal;	// 区切り記号カンマを外し数値にしたうえで足しこみ
            }



        }

        return objSum;

    }


    /******************************************************/
    // チェックされたテーブル行の操作ボタンを無効/有効化
    //   FldId      対象チェックボックスフィールドID
    //   関数値     なし
    /******************************************************/
    function TcTblSeqOperate(FldId) {

        for ( var row = 0; row < getFieldValueLength(FldId); row++ ) {

            // その行のチェックボックス要素取得
            var elVal = getFieldValueEls_(FldId,row);

            //「×」ボタン要素取得
            var elBtn = elVal.parentNode.parentNode.parentNode.getElementsByClassName("remove-row-image-gaia")[0];

            // 1個目のチェックボックス選択状態取得
            var chkState = elVal.childNodes[0].childNodes[0].childNodes[0].checked;
            if ( chkState == true ) {
                elBtn.setAttribute("style", "display:none;");		// チェック有の場合：ボタン非表示
            } else if ( getFieldValueLength(FldId) != 1 )  {
                elBtn.removeAttribute("style");						// チェック無かつテーブル2行以上の場合：ボタン表示
            }
        }

    }


    /******************************************************/
    // チェックされた行の指定フィールドを別フィールドに転記
    //   TrgId    差し込み先フィールドID
    //   SrcId    差し込み元フィールドID
    //   ChkId    判定チェックボックスID
    //   iniVal   初期値
    //   関数値   なし
    /******************************************************/
    function TcPostTblChkData(TrgId, SrcId, ChkId, iniVal) {

        var chkCnt = 0;								// チェック数カウンタ
        var elTrg = getSingleLineInput(TrgId);		// 差し込み先フィールド

        for ( var row = 0; row < getFieldValueLength(SrcId); row++ ) {

            // テーブル行要素
            var elSrc = getFieldValueFromTableInput(SrcId, row);						// 差し込み元フィールド
            var elChk = getFieldValueEls_(ChkId, row);									// 判定チェックボックス
            var ChkState = elChk.childNodes[0].childNodes[0].childNodes[0].checked;		// チェック状態

            /*
            ** ★チェックされた行データのフィールド値を差し込み先フィールドに転記
            **  - データ転記＋チェックされた行カウンタをインクリメント
            **  - チェックされた行以外のチェックボックスを無効化
            */
            if ( ChkState == true ) {
                elTrg.value = elSrc.value;		// 差し込み先.value ← 差し込み元.value
                chkCnt++;						// チェック数カウント

                /*
                ** ★lcnt(チェックされた行)以外のチェックボックスを無効にする
                */
                for ( var cnt = 0; cnt < getFieldValueLength(SrcId); cnt++ ) {
                    if ( cnt != row ) {
                        // チェックされた行ではない場合
                        var elChk2 = getFieldValueEls_(ChkId, cnt);								// 判定チェックボックス
                        elChk2.childNodes[0].childNodes[0].childNodes[0].disabled = true;		// チェックを無効化
                    }
                }
            }
        }

        /*
        ** ★テーブル単位のチェック数別の処理
        */
        if ( chkCnt > 1 ) {
            // チェック数が2個以上の場合、警告メッセージ表示
            alert("採用区分は1テーブルにつき1チェックにしてください");
        }
        if ( chkCnt != 1 ) {
            // チェック数が1ではない(0個)の場合、すべてのチェックボックスを有効化
            for ( var cnt=0; cnt < getFieldValueLength(SrcId); cnt++ ) {
                elTrg.value = iniVal;												// 差し込み先フィールドを初期化
                var elChk2 = getFieldValueEls_(ChkId, cnt);							// 判定チェックボックス
                elChk2.childNodes[0].childNodes[0].childNodes[0].disabled = false;	// チェックを有効化
                elChk2.childNodes[0].childNodes[0].childNodes[0].checked = false;	// チェックをはずす
            }
        }

    }


    /******************************************************/
    // テーブル行毎の小計、テーブル全体の小計を計算してフィールド更新
    //   SelId    [分類]ドロップダウンフィールドID
    //   QtyId    [数量]数値フィールドID
    //   PrcId    [単価]数値フィールドID
    //   MnyId    [金額]数値フィールドID
    //   TtlId    [小計]数値フィールドID
    //   関数値   なし
    /******************************************************/
    function TcCalcTblSubTtl(SelId, QtyId, PrcId, MnyId, TtlId) {

        // 計算用変数
        var subTtl_row = 0;			// 行小計([金額]用)
        var subTtl_all = 0;			// 全小計([小計]用)

        // 要素取得
        var elTtl = getSingleLineInput(TtlId);		// [小計]要素

        for ( var row = 0; row < getFieldValueLength(SelId); row++ ) {

            // テーブル行単位要素取得&金額計算
            var elQty = getFieldValueFromTableInput(QtyId, row);	// [数量]要素
            var elPrc = getFieldValueFromTableInput(PrcId, row);	// [単価]要素
            var elMny = getFieldValueFromTableInput(MnyId, row);	// [金額]要素
            var calcResult = elQty.value * elPrc.value;				// 金額算出

            // [分類]ドロップダウンの選択値を取得
            var selStr = TcGetDropDown(SelId, row);

            /*
            ** ★[分類]によって設定する行情報を切り替える
            **  - "小計"である場合：[数量][単価]に空文字、[金額]に小計を設定
            **  - "小計"でない場合：[数量][単価]が空文字の場合0クリア、[金額]に金額値を設定し、小計を足し込む
            */
            if (  selStr.indexOf("小計") != -1 ) {
                // "小計"である場合
                elQty.value = "";								// [数量]に空文字代入
                elPrc.value = "";								// [単価]に空文字代入
                elMny.value = subTtl_row;						// [金額]に行小計を差し込む
                subTtl_row = 0;									// 行小計0クリア
            } else {
                // "小計"でない場合
                if ( elQty.value == "" ) { elQty.value = 0; }	// [数量]が空文字の場合0クリア
                if ( elPrc.value == "" ) { elPrc.value = 0; }	// [単価]が空文字の場合0クリア
                elMny.value = calcResult;						// [金額]に金額差し込む
                subTtl_row += calcResult;						// 行小計を足し込む
                subTtl_all += calcResult;						// 全小計を足し込む
            }

            /*
            ** ★フィールド無効化
            */
            // [金額]フィールド
            elMny.parentNode.className = "input-text-outer-cybozu disabled-cybozu";
            elMny.disabled = true;

        }

        // [小計]フィールド更新
        elTtl.value = subTtl_all;

    }


    /******************************************************/
    // 指定したフィールドの値を別のフィールドに差し込む
    //  - 通常/テーブルのフィールド両方に対応
    //   TrgId    差し込み先フィールドID
    //   SrcId    差し込み元フィールドID
    //   関数値   なし
    /******************************************************/
    function TcPostFieldData(TrgId, SrcId) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(TrgId);
        var elVal = document.getElementsByClassName("value-" + TrgId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        /*
        ** ★差し込み先フィールド値が空文字の場合、差し込み元フィールド値を差し込む
        */
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルの場合
            for ( var row = 0; row < elVal.length; row++ ) {
                var TrgVal = getFieldValueFromTableInputValue(TrgId, row);	// 差し込み先フィールド値取得
                if ( TrgVal == "" ) {
                    // 空文字の場合、SrcIdのフィールド値をTrgIdのフィールドに差し込む
                    getFieldValueFromTableInput(TrgId,row).value = getFieldValueFromTableInputValue(SrcId, row).replace(/\s+$/, "");
                }
            }
        } else {
            // テーブルではない場合
            var TrgVal = getSingleLineInputValue(TrgId);
            if ( TrgVal == "" ) {
                // 空文字の場合、SrcIdのフィールド値をTrgIdのフィールドに差し込む
                getSingleLineInput(TrgId).value = getSingleLineInputValue(SrcId).replace(/\s+$/, "");
            }
        }

    }


    /******************************************************/
    // ルックアップ取得内容をドロップダウンの内容で絞り込む
    //   DDId	ドロップダウンフィールドID(各分類)
    //   LUId	ルックアップフィールドID(材料費・労務費・機械費・その他)
    //   row	行番号
    /******************************************************/
    function TcPostDDtoLU(DDId, LUId, row) {

        // ドロップダウンとルックアップの値を取得
        var str_dd = TcGetDropDown( DDId, row ).replace(/\s+$/, "");

        // 取得したドロップダウンの選択値をルックアップフィールドに転記
        getFieldValueFromTableInput( LUId, row ).value = str_dd.replace(/\-/g, "");

        // ルックアップフィールドの無効化(disabled)
        getFieldValueEls_( LUId, row ).childNodes[0].childNodes[0].className = "input-text-outer-cybozu disabled-cybozu";
        getFieldValueEls_( LUId, row ).childNodes[0].childNodes[0].childNodes[0].disabled= true;
    }


    /******************************************************/
    // 画面上のリンクを同一ウインドウで開くように変更
    //  - テーブル内のルックアップに対応するため常に呼び出す
    //  targVal	ウインドウを開く対象("_self"/"_blank")※省略時は"_self"
    /******************************************************/
    function TcConvLinkTarget(targVal) {

        // 省略時の対応
        if ( !targVal ) { targVal = "_self"; }

        var elLookup = document.getElementsByTagName("a");			// <a>要素取得
        for ( var cnt = 0; cnt < elLookup.length; cnt++ ) {
            // 別ウインドウで開くリンクのみ処理
            var strClsName = elLookup[cnt].parentNode.className;	// 親ノードのクラス名取得
            if ( elLookup[cnt].target != targVal ) {
                elLookup[cnt].target = targVal;						// 同一ウインドウで開くように変更
            }
        }

    }


    /******************************************************/
    // 縦棒グラフに対して、TCカスタムグリッドを配置する
    //  - 表示された縦棒グラフに指定値の高さに目標線を引く
    //    ※グラフ種別は判定していない為、意識して利用する
    //  prmVal	TCカスタムグリッドの指定目盛値
    /******************************************************/
    function TcPutCustomGrid_H(prmVal) {

        // 引数未指定時は処理しない
        if ( !prmVal ) { return; }

        // 既存の目盛やグリッドを取得
        var elGraphScale = document.getElementsByClassName("highcharts-axis-labels highcharts-yaxis-labels")[0];	// グラフ目盛取得
        var elGraphGrid = document.getElementsByClassName("highcharts-grid")[1];									// グラフグリッド(横線)取得

        // 対象レコードがない場合(グラフが表示しない)は処理しない
        if ( !elGraphScale ) { return; }

        var scaleLen = elGraphScale.childNodes.length-1;									// 目盛の数
        var scaleVal_Max = elGraphScale.childNodes[scaleLen].textContent;					// 表示目盛値(最大値)取得

        // 表示目盛値から最大値を求める
        var aryScaleVal = new Array();
        for ( var cnt = 0; cnt < scaleLen; cnt++ ) {
            aryScaleVal[cnt] = Number(elGraphScale.childNodes[cnt].textContent);
        }
        var scaleVal_Max = Math.max.apply(Math, aryScaleVal);

        // 指定目盛値と表示目盛の最大値を比較
        //  - 指定目盛値以下の目盛しか表示されていない場合は、TCカスタムグリッド配置対象外
        if ( prmVal < scaleVal_Max ) {

               // 表示目盛値毎に処理
                for ( var cnt = 0; cnt < scaleLen; cnt++ ) {

                    var scaleVal_Now = Number(elGraphScale.childNodes[cnt].textContent);	// 表示目盛値取得

                    // 指定目盛値と取得表示目盛値を比較
                    //  - 指定目盛値以上の表示目盛値が判定された場合のみ処理
                    if ( prmVal <= scaleVal_Now ) {

                        // 指定目盛値に一番近く上側のグリッドを取得
                        var scaleVal_Prev = Number(elGraphScale.childNodes[cnt-1].textContent);				// グリッドラインの目盛値(※これが欲しい)
                        var posVal_upper = Number(elGraphScale.childNodes[cnt-1].getAttribute("y"));		// グリッドラインの高さ(※これが欲しい)
                        // 指定目盛値に一番近く下側のグリッドを取得
                        var elCloneGrid = elGraphGrid.childNodes[0].cloneNode(true);						// 指定目盛値のラインのグリッドを取得・複製
                        var posVal_lower = Number(elGraphScale.childNodes[cnt].getAttribute("y"));			// グリッドラインの高さ(※これが欲しい)

                        // TCカスタムグリッドの座標(高さ)を決定
                        var posMargin = (posVal_lower - posVal_upper);										// 上下グリッドの座標(高さ)差
                        var posRate = (scaleVal_Now - prmVal) / (scaleVal_Now - scaleVal_Prev);				// 表示目盛値の差から指定目盛値の座標(高さ)位置を算出
                        var customLinePos = posVal_upper + (posMargin * posRate);							// TCカスタムグリッドの座標(高さ)を決定

                        // 表示目盛にすでに指定目盛値が表示されている場合、既存グリッドと高さを合わせる
                        // 既存グリッドからTCカスタムグリッド座標に近い座標値算出
                        if ( aryScaleVal.indexOf(prmVal) != -1 ) {
                            var absVal = 999;
                            var tmpVal = 999;
                            for ( var wrk = 0; wrk < aryScaleVal.length; wrk++ ) {
                                var gridHeight = Number(elGraphGrid.childNodes[wrk].getAttribute("d").split(" ")[2]);
                                // 既存グリッドの座標(高さ)のうち、一番近いものを算出
                                if ( aryScaleVal[wrk] != customLinePos ) {
                                    var wrkVal = Math.abs(gridHeight - customLinePos);
                                    if ( wrkVal < tmpVal ) {
                                        absVal = gridHeight;	// 一番近い値更新
                                        tmpVal = wrkVal;		// ひとつ前の高さ差の絶対値記憶(ループ計算用)
                                    }
                                }
                            }
                            customLinePos = absVal;
                        }

                        // TCカスタムグリッド一旦削除
                        if ( document.getElementById("tc_custom_grid") ) {
                            var elOldNode = document.getElementById("tc_custom_scale");
                            if ( elOldNode ) { elOldNode.parentNode.removeChild(elOldNode); }
                            var elOldNode = document.getElementById("tc_custom_grid");
                            if ( elOldNode ) { elOldNode.parentNode.removeChild(elOldNode); }
                        }

                        // ★TCカスタムグリッド(目盛値)を追加
                        var elCloneScale = elGraphScale.childNodes[cnt].cloneNode(true);
                        elCloneScale.id = "tc_custom_scale";
                        elCloneScale.textContent = prmVal;						// 表示文字
                        elCloneScale.setAttribute("y", (customLinePos + 4));	// 文字座標(高さ)
                        elGraphScale.appendChild(elCloneScale);

                        // ★TCカスタムグリッド(横線)を追加
                        elCloneGrid.id = "tc_custom_grid";
                        elCloneGrid.setAttribute("stroke","red");				// 赤線にする
                        elCloneGrid.setAttribute("stroke-width","2");			// 線を太く
                        var pathAtr = elCloneGrid.getAttribute("d").split(" ");
                        pathAtr[2] = customLinePos;								// 線の始点(高さ)を再指定
                        pathAtr[5] = customLinePos;								// 線の終点(高さ)を再指定
                        elCloneGrid.setAttribute("d", pathAtr.join(" "));		// 線座標情報
                        elGraphGrid.appendChild(elCloneGrid);

                        return;
                    }

                }	// end for

            } else {
                // 指定目盛より小さい表示目盛の場合はTCカスタムグリッド削除
                var elOldNode = document.getElementById("tc_custom_scale");
                if ( elOldNode ) { elOldNode.parentNode.removeChild(elOldNode); }
                var elOldNode = document.getElementById("tc_custom_grid");
                if ( elOldNode ) { elOldNode.parentNode.removeChild(elOldNode); }
            }


    }


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★モジュール
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
    /**********************************************************/
    // 別アプリへのデータ転記
    //   oSubwin    サブwindowオブジェクト
    //   oQuery     転記する文字列を下記形式で指定
    //                oQuery = [ { src:value1, target:fieldid1},
    //                           { src:value2, target:fieldid2},
    //                         ];
    //              value : 転記元となる値
    //              target: 転記先のフィールドID
    //   関数値     True:成功 / False:失敗
    /**********************************************************/
    function TcSwCopy(oSubwin, oQuery) {
        var bRet = true;
        for ( var i = 0; i < oQuery.length; i++ ) {
            var item = oQuery[i];
            // 転記先がラジオボタンの場合
            if ( item["type"] == "radio" ) {
                var elTarget = oSubwin.getFieldValueEl_(item["target"]);
                if ( elTarget ) {
                    var ipTags = elTarget.getElementsByTagName("input");
                    for ( var rCnt = 0; rCnt < ipTags.length; rCnt++ ) {
                        if ( ipTags[rCnt].value == item["src"] ) {
                            ipTags[rCnt].checked = true;
                        } else {
                            ipTags[rCnt].checked = false;
                        }
                    }
                } else {
                    bRet = false;
                }
            // 転記先がチェックボックスの場合
            } else if ( item["type"] == "check" ) {
                var elTarget = oSubwin.getFieldValueEl_(item["target"]);
                if ( elTarget ) {
                    var ipTags = elTarget.getElementsByTagName("input");
                    for ( var rCnt = 0; rCnt < ipTags.length; rCnt++ ) {
                        if ( getTextContent_(ipTags[rCnt].parentNode.childNodes[1]) == item["src"] ) {
                            ipTags[rCnt].checked = true;
                        } else {
                            ipTags[rCnt].checked = false;
                        }
                    }
                } else {
                    bRet = false;
                }
            // 転記先がマルチテキストの場合
            } else if ( item["type"] == "multi" ) {
                var elTarget = oSubwin.getMultiLineInput(item["target"]);
                if ( elTarget ) elTarget.value = item["src"];
                else bRet = false;
            } else {
                var elTarget = oSubwin.getSingleLineInput(item["target"]);
                if ( elTarget ) elTarget.value = item["src"];
                else bRet = false;
            }
        }
        return(bRet);
    }


    /******************************************************/
    // JSONオブジェクトのValue値を返す
    //  - オブジェクトの値がある場合はその値を、オブジェクト自体がないor値がない場合は空白を返す
    //   関数値   オブジェクトの値 or 空白
    /******************************************************/
    function TcRetObjVal(objFld) {
        /*
        ** ★引数オブジェクト値をreturn
        */
        if ( objFld != undefined && objFld['value'] != null ) {
            // 引数オブジェクトに値が入っている
            return objFld['value'];
        } else {
            // 引数オブジェクト自体が存在しない または 値が入っていない
            return "";
        }
    }


    /******************************************************/
    // 外部PHPの呼び出し実行
    //   FileName	実行ファイル名
    //   Query		PHPパラメタ
    //   isReload	リロードするか(bool)(省略可)
    //   Domain 	使用するドメイン指定(省略可)
    //   reloadTime	リロード時間(省略可)
    //   関数値		なし
    /******************************************************/
    function TcCallPHP(FileName, Query, isReload ,Domain, reloadTime) {

        // 送信先URLを設定
		if(Domain == undefined ){
	        // 環境変数取得
	        var prm = TcGetEnvPrm();
        	var url = "https://www.timeconcier.jp/forkintone/" + prm['PHPドメイン'] + "/" + FileName;
		}else{
        	var url = "https://www.timeconcier.jp/forkintone/" + Domain + "/" + FileName;
		}
        TcOpenHtml(url, Query, "_newWin", "POST");

        // isReloadがfalseでない場合はリロードする
        if ( isReload == undefined || isReload == true ) {
            if ( reloadTime == undefined ) { reloadTime = 1500; }	// デフォルトリロード時間1.5秒
            setTimeout( function() { location.reload(); }, reloadTime);
        }

    }


    /******************************************************/
    // ボタン要素を作成して返す
    //   BtnId		ボタン識別子
    //   BtnLbl		表示するボタン名
    //   FncName	設定するクリックイベント(省略可)
    //   BtnTag		設定するボタン名(省略可)
    //   SetStyle	設定するボタンスタイル(省略可)
    //   row		配置先がテーブルの場合、行番号(省略可)
    /******************************************************/
    function TcMakeButton(BtnId, BtnLbl, FncName, BtnTag, SetStyle, row) {

        // ボタンパーツ作成
        var emButton = document.createElement('input');
        emButton.type = 'button';
        emButton.value = BtnLbl;
        emButton.id  = 'btn_' + BtnId;						// クリックイベントからはbtn.idで呼び出し元を判定する
        if ( row != undefined ) {
            emButton.id += '_' + row;						// 行番号指定時
        }
        emButton.tag = BtnTag;								// ボタン識別のためのタグ名

        // イベントが指定されている場合は、クリックイベントを追加する
        if ( FncName != undefined ) {
            addEventListener_(emButton, 'click', FncName);	// クリックイベント追加
        } else {
//            emButton.value += "(EV未設定)";
            emButton.disabled = true;						// ボタン無効化
        }

        // スタイルが指定されている場合は、ボタンスタイルを設定する
        if ( SetStyle != undefined ) { emButton.setAttribute("style", SetStyle); }

        // ボタン要素を返す
        return emButton;
    }


    /******************************************************/
    // ドロップダウン選択値をルックアップ入力欄に転記
    //   TrgId		ルックアップフィールドID
    //   SrcId		ドロップダウンフィールドID
    //   row		行番号
    /******************************************************/
    function TcFilLookUpFld(TrgId, SrcId, row) {

        // 要素(Label/Value)を取得
        var elLbl = getFieldLabelEl_(TrgId);
        var elVal = document.getElementsByClassName("value-" + TrgId);	// 指定フィールドすべて取得するためにgetFieldValeEl_()は使わない

        /*
        ** ★ドロップダウンの選択値をルックアップフィールドに差し込む
        */
        if ( elLbl.className.indexOf("subtable") != -1 ) {
            // テーブルである場合
            for ( var row = 0; row < elVal.length; row++ ) {

                // 転記
                var str_dd = TcGetDropDown(SrcId, row).replace(/\s+$/, "");					// ドロップダウンの選択値取得
                getFieldValueFromTableInput(TrgId, row).value = str_dd.replace(/\-/g, "");	// 取得した選択値をルックアップに転記

                // ルックアップフィールドの無効化(disabled)
                var elTrg = getFieldValueEls_(TrgId, row).childNodes[0].childNodes[0];	// ルックアップ要素取得
                elTrg.className = "input-text-outer-cybozu disabled-cybozu";
                elTrg.childNodes[0].disabled= true;
            }
        } else {
            // テーブルでない場合
            console.log("通常フィールドは未対応");
        }
    }


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// ★kintone標準Utility
//   - kintone標準(サイボウズ提供)のユーティリティ関数
/*--------------------------------------------------------------------------------------------------------------------------------------------*/
// URLの?や#より前の文字列を取得
function getUrlPath_() {
        var url = document.URL;
        if (url.indexOf('?') > 0) {
                return url.substr(0, url.indexOf('?'));
        }
        if (url.indexOf('#') > 0) {
                return url.substr(0, url.indexOf('#'));
        }
        return url;
}


// CSSロード
function loadCSS(href) {
    document.write('<link rel="stylesheet" type="text/css" href="' + href + '" />');
}

// レコード追加画面ならtrue
function isAddPage() {
    return getUrlPath_().match(/edit$/);
}

// レコード詳細画面ならtrue
function isShowPage() {
    return getUrlPath_().match(/show$/) && !location.hash.match(/mode=edit/);
}

// レコード編集画面ならtrue
function isEditPage() {
    return getUrlPath_().match(/show$/) && location.hash.match(/mode=edit/);
}

// レコード一覧画面ならtrue
function isViewPage() {
    return getUrlPath_().match(/\/[0-9]+\/$/);
}

// ポータル画面ならtrue
function isPortalPage() {
    var url = document.URL;
    return url.match(/portal$/);
}

// グラフ画面ならtrue
function isGraphPage(graphId) {

    if ( getUrlPath_().match(/report$/) ) {
        // グラフの表示ページの場合、グラフのIDを返す
        var url = document.URL;				// 現在のURL
        var aryRet = url.split("=");		// "="で分割
        
        if ( graphId == aryRet[1] ) {
            return true;
        }

    }

    // グラフの表示ページではない または 指定グラフIDが一致しない
    return false;
}


//----------------------------------------

// 要素のテキストを取得
function getTextContent_(el) {
    return el.childNodes[0].textContent !== undefined ? el.childNodes[0].textContent : el.childNodes[0].innerText;
}
 
// フィールド名の要素を取得
function getFieldLabelEl_(fieldId) {
    return document.getElementsByClassName('label-' + fieldId)[0];
}
 
// フィールド値の要素を取得
function getFieldValueEl_(fieldId) {
    return document.getElementsByClassName('value-' + fieldId)[0];
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueLength(fieldId) {
    return document.getElementsByClassName('value-' + fieldId).length;
}

// フィールド値の要素を取得（テーブル用）
function getFieldValueEls_(fieldId, i) {
    return document.getElementsByClassName('value-' + fieldId)[i];
}
 
// フィールド名を取得（すべての画面で利用可）
function getFieldName(fieldId) {
    return getTextContent_(getFieldLabelEl_(fieldId));
}
 
// フィールド値を取得（一覧画面と詳細画面で利用可、一部のフィールドタイプには未対応）
function getFieldValue(fieldId) {
    return getTextContent_(getFieldValueEl_(fieldId));
}

// フィールド値を取得（テーブル内のフィールド）
function getFieldValueFromTable(fieldId, i) {
    return getTextContent_(getFieldValueEls_(fieldId, i));
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（テーブル内のフィールド）
function getFieldValueFromTableInput(fieldId,i) {
    return getFieldValueEls_(fieldId,i).querySelector('input');
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（テーブル内のフィールド）
function getFieldValueFromTableInputValue(fieldId,i) {
    return getFieldValueFromTableInput(fieldId,i).value;
}

// 文字列(1行)、数値、リンクフィールドのINPUT要素を取得（追加画面と編集画面で利用可）
function getSingleLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('input');
}
 
// 文字列(1行)、数値、リンクフィールドのINPUT要素の入力値を取得（追加画面と編集画面で利用可）
function getSingleLineInputValue(fieldId) {
    return getSingleLineInput(fieldId).value;
}
 
// 文字列(複数行)フィールドのTEXTAREA要素を取得（追加画面と編集画面で利用可）
function getMultiLineInput(fieldId) {
    return getFieldValueEl_(fieldId).querySelector('textarea');
}
 
// 文字列(複数行)フィールドのTEXTAREA要素の入力値を取得（追加画面と編集画面で利用可）
function getMultiLineInputValue(fieldId) {
    return getMultiLineInput(fieldId).value;
}

//----------------------------------------

// イベントリスナーを登録
function addEventListener_(el, type, listener) {
    if (el.addEventListener !== undefined) {
        el.addEventListener(type, listener);
    } else if (el.attachEvent !== undefined) {
        el.attachEvent('on' + type, listener);
    }
}

// イベントリスナーを削除
function delEventListener_(el, type, listener) {
    if (el.removeEventListener !== undefined) {
        el.removeEventListener(type, listener);
    } else if (el.detachEvent !== undefined) {
        el.detachEvent('on' + type, listener);
    }
}


/******************************************************/
// 外部ページへのデータ渡し
//   url      渡し先URL
//   query    送信するデータ(key:xxx val:xxx)
//   name     送信ターゲット名
//   meth     POST or GET
//   関数値   なし
/******************************************************/
function TcOpenHtml( url, query, name, meth ) {
        var win = window.open( "", name );
        
// formを生成
        var fm = document.createElement("form");
        fm.target = name;
        fm.action = url;
        fm.method = meth;
        for( var i = 0; i < query.length; i++ ) {
            var kv = query[i];
            var key = kv["key"];
            var val = kv["val"];
            if ( key ) {
                var input = document.createElement("input");
                val = ( val != null ? val : "" );
                input.type = "hidden";
                input.name = key;
                input.value = val;
                fm.appendChild(input);
                }
        }
        // 一時的にbodyへformを追加。サブミット後、formを削除する
        var body = document.getElementsByTagName("body")[0];
        body.appendChild(fm);
        fm.submit();
        body.removeChild(fm);
}


/******************************************************/
// 引数のURLへリクエストを送信
//   URL    	呼び出し先URL
//   関数値     URLのPHPからJavaScriptを返す。
/******************************************************/
function TcSendReq( url ){

    var res = null;
    // XMLHttpRequestオブジェクト生成
    var oj = TcCreateHttpRequest();
    if ( oj != null ) {
        // 着信コールバックメソッド
        oj.onreadystatechange =function () {
            if(oj.readyState==4 && oj.status == 200){
                // レスポンスを取得
                res = oj.responseText;
            }
        }
        //open,send メソッド
        oj.open( 'POST' , url , false );
        oj.setRequestHeader("Content-Type" , "application/x-www-form-urlencoded");
        oj.send( null );
    }
    return ( res );
}

/******************************************************/
// XMLHttpRequestオブジェクト生成関数
//   関数値     XMLHttpRequestオブジェクト
/******************************************************/
function TcCreateHttpRequest() {

    if(window.XMLHttpRequest){
        // Win Mac Linux m1,f1,o8 Mac s1 Linux k3 & Win e7用
        return new XMLHttpRequest() ;
    } else if(window.ActiveXObject) {
        // Win e4,e5,e6用
        try {
            return new ActiveXObject("Msxml2.XMLHTTP") ;
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP") ;
            } catch (e2) {
                return null;
            }
        }
    }
}


/******************************************************/
// kintoneと通信を行うクラス
/******************************************************/
var KintoneRecordManager = (function() {
    KintoneRecordManager.prototype.records = [];	// 取得したレコード
    KintoneRecordManager.prototype.appId = null;	// アプリID
    KintoneRecordManager.prototype.query = '';		// 検索クエリ
    KintoneRecordManager.prototype.limit = 10;		// 一回あたりの最大取得件数
    KintoneRecordManager.prototype.offset = 0;		// オフセット

    function KintoneRecordManager() {
        this.appId = kintone.app.getId();
    }

    // すべてのレコード取得する
    KintoneRecordManager.prototype.getRecords = function(callback) {
        kintone.api(
            kintone.api.url('/k/v1/records', true),
            'GET',
            {
                app: this.appId,
                query: kintone.app.getQueryCondition() + (' limit ' + this.limit + ' offset ' + this.offset)
            },
            (function(_this) {
                return function(res) {
                    var len;
                    Array.prototype.push.apply(_this.records, res.records);
                    len = res.records.length;
                    _this.offset += len;
                    if (len < _this.limit) { // まだレコードがあるか？
                        _this.ready = true;
                        if (callback !== null) {
                            callback(_this.records); // レコード取得後のcallback
                        }
                    } else {
                        _this.getRecords(callback); // 自分自身をコール
                    }
                };
            })(this)
        );
    };
    return KintoneRecordManager;
})();

