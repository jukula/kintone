<?php 
	////////////////////////////////////
	//  タイムコンシェル用
	////////////////////////////////////

	include_once("tcalertsend_inokouei.php");

	define( "TC_KIGEN" 	 , 1 ); // 期限通知
	define( "TC_STEP"  	 , 2 ); // ステップ通知
	define( "TC_KINEN" 	 , 3 ); // 記念日通知
	define( "TC_TEIKI" 	 , 4 ); // 定期通知
	define( "TC_STEP_S"  , 5 ); // ステップ通知（初回）

	function execMailSet_inokouei() {

		///////////////////////////////////////////////////////////
		// 基本接続情報設定
	    $domain = "ino-kouei.cybozu.com"; 	// ドメイン
	    $user   = "Administrator";			// ID
	    $passwd = "Tc2010";					// パスワード
		///////////////////////////////////////////////////////////

		//***********************************************************************************
		// 期限通知
		//***********************************************************************************
		// クエリ用の値（○○日の○○前）
		$today1 = date("Y-m-d",strtotime("-1 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "1";                       // 使用する繰り返し本文
		$addlist	 = "ino.kouei@gmail.com";  // 送信先リスト

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_KIGEN,$body,$addlist);

		//***********************************************************************************
		// ステップ通知
		//***********************************************************************************
		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知初回(○○日当日)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "1";                       // 使用する繰り返し本文
		$addlist	 = "ino.kouei@gmail.com";  // 送信先リスト


		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP_S,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知1回(○○日後)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("+3 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "2";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知2回(○○日後)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("+7 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "3";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知3回(○○日後)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("+21 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)
		$body        = "4";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-3 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品３日後のフォローをしましょう！";  		  // メールタイトル
		$header   	 = "納品３日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "4";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-7 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品７日後のフォローをしましょう！";  		  // メールタイトル
		$header   	 = "納品７日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "4";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-21 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品２１日後のフォローをしましょう！";  // メールタイトル
		$header   	 = "納品２１日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$body        = "4";                       // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// 0時にのみ動く設定
		//-----------------------------------------------------------------------------------
		if(date("H") == "00"){
			//-----------------------------------------------------------------------------------
			// ●【契約後フォロー通知】契約日の3日後に施工待ち状態のフォローしましょう！
			//-----------------------------------------------------------------------------------
			$today1 = date("Y-m-d",strtotime("-3 day")); // 対象日付(○日・○月前)
			$joken1 = "3.契約(施工待ち)";

			$app  		 = "11"; 					  // データを取得するアプリID
			$query  	 = "( 契約日 = \"".$today1."\") and ( 進捗状況 in (\"".$joken1."\") )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【契約後フォロー通知】契約日の3日後に施工待ち状態のフォローしましょう！";  		  // メールタイトル
			$header   	 = "施工待ちのお客様のうち、\n契約日から3日後のお客様情報をお知らせします。";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  		  // メールアドレス項目名(テーブル不可)
			$body        = "7";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// ●【契約後フォロー通知】契約日の7日後に施工待ち状態のフォローしましょう！
			//-----------------------------------------------------------------------------------
			$today1 = date("Y-m-d",strtotime("-7 day")); // 対象日付(○日・○月前)
			$joken1 = "3.契約(施工待ち)";

			$app  		 = "11"; 					  // データを取得するアプリID
			$query  	 = "( 契約日 = \"".$today1."\") and ( 進捗状況 in (\"".$joken1."\") )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【契約後フォロー通知】契約日の7日後に施工待ち状態のフォローしましょう！";  		  // メールタイトル
			$header   	 = "施工待ちのお客様のうち、\n契約日から7日後のお客様情報をお知らせします。";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  // メールアドレス項目名(テーブル不可)
			$body        = "7";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// ●【契約後フォロー通知】契約日の21日後に施工待ち状態のフォローしましょう！
			//-----------------------------------------------------------------------------------
			$today1 = date("Y-m-d",strtotime("-21 day")); // 対象日付(○日・○月前)
			$joken1 = "3.契約(施工待ち)";

			$app  		 = "11"; 					  // データを取得するアプリID
			$query  	 = "( 契約日 = \"".$today1."\") and ( 進捗状況 in (\"".$joken1."\") )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【契約後フォロー通知】契約日の21日後に施工待ち状態のフォローしましょう！";  		  // メールタイトル
			$header   	 = "施工待ちのお客様のうち、\n契約日から21日後のお客様情報をお知らせします。";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  // メールアドレス項目名(テーブル不可)
			$body        = "7";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// クエリ用の値（●【定期点検フォロー】完工日から半年後にフォローを行う！）
			//-----------------------------------------------------------------------------------
			$today1	= date("Y-m-d",strtotime("-6 month")); // 対象日付(○日・○月前)

			$app  		 = "11"; 					  			  	  // データを取得するアプリID
			$query  	 = "( 完工日 = \"".$today1."\")"; 	  // 絞り込みをするクエリ文字列
			$subject   	 = "【定期点検フォロー】完工日から半年後のお客様のお知らせ";  	  // メールタイトル
			$header   	 = "完工日から半年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！\n\n";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  					  // メールアドレス項目名(テーブル不可)
			$body        = "2";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// クエリ用の値（●【定期点検フォロー】完工日から1年後にフォローを行う！）
			//-----------------------------------------------------------------------------------
			$today1	= date("Y-m-d",strtotime("-1 year")); // 対象日付(○日・○月前)

			$app  		 = "11"; 					  			  	  // データを取得するアプリID
			$query  	 = "( 完工日 = \"".$today1."\")"; 	  // 絞り込みをするクエリ文字列
			$subject   	 = "【定期点検フォロー】完工日から1年後のお客様のお知らせ";  	  // メールタイトル
			$header   	 = "完工日から1年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  					  // メールアドレス項目名(テーブル不可)
			$body        = "2";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// クエリ用の値（●【定期点検フォロー】完工日から3年後にフォローを行う！）
			//-----------------------------------------------------------------------------------
			$today1	= date("Y-m-d",strtotime("-3 year")); // 対象日付(○日・○月前)

			$app  		 = "11"; 					  			  	  // データを取得するアプリID
			$query  	 = "( 完工日 = \"".$today1."\")"; 	  // 絞り込みをするクエリ文字列
			$subject   	 = "【定期点検フォロー】完工日から3年後のお客様のお知らせ";  	  // メールタイトル
			$header   	 = "完工日から3年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  					  // メールアドレス項目名(テーブル不可)
			$body        = "2";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// クエリ用の値（●【定期点検フォロー】完工日から5年後にフォローを行う！）
			//-----------------------------------------------------------------------------------
			$today1	= date("Y-m-d",strtotime("-5 year")); // 対象日付(○日・○月前)

			$app  		 = "11"; 					  			  	  // データを取得するアプリID
			$query  	 = "( 完工日 = \"".$today1."\")"; 	  // 絞り込みをするクエリ文字列
			$subject   	 = "【定期点検フォロー】完工日から5年後のお客様のお知らせ";  	  // メールタイトル
			$header   	 = "完工日から5年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  					  // メールアドレス項目名(テーブル不可)
			$body        = "2";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

			//-----------------------------------------------------------------------------------
			// クエリ用の値（●【定期点検フォロー】完工日から7年後にフォローを行う！）
			//-----------------------------------------------------------------------------------
			$today1	= date("Y-m-d",strtotime("-7 year")); // 対象日付(○日・○月前)

			$app  		 = "11"; 					  			  	  // データを取得するアプリID
			$query  	 = "( 完工日 = \"".$today1."\")"; 	  // 絞り込みをするクエリ文字列
			$subject   	 = "【定期点検フォロー】完工日から7年後のお客様のお知らせ";  	  // メールタイトル
			$header   	 = "完工日から7年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！";  // 本文のヘッダー
			$mailapp     = "10"; 	 	                  // メールアドレスを取得するアプリID
			$address 	 = "メールアドレス_PC";	  					  // メールアドレス項目名(テーブル不可)
			$body        = "2";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);
		}
		//-----------------------------------------------------------------------------------
		// クエリ用の値（最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-30 day")); // 対象日付(○日・○月・○年前)

		$app  		 = "945"; 					  		  						   // データを取得するアプリID
		$query  	 = "( アクション実施日 = \"".$today1."\")"; 				   // 絞り込みをするクエリ文字列
		$subject   	 = "最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！";  		 		 	   // メールタイトル
		$header   	 = "最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$body        = "3";                       		  // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（納品１年後を記念日としてお客様をフォローしましょう！（引き渡し記念日））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-1 year")); // 対象日付(○日・○月・○年前)

		$app  		 = "944"; 					  		  						   // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; 							   // 絞り込みをするクエリ文字列
		$subject   	 = "納品１年後のフォローをしましょう！";  		 		 	   // メールタイトル
		$header   	 = "納品１年後のフォローをしましょう！\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$body        = "4";                       		  // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（見積提出７日後のフォローをしましょう！）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-7 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  		  										   // データを取得するアプリID
		$query  	 = "( 見積提出日 = \"".$today1."\") and (進捗状況 in (\""."3.提案見積"."\"))"; // 絞り込みをするクエリ文字列
		$subject   	 = "見積提出７日後のフォローをしましょう！";  		 		 				   // メールタイトル
		$header   	 = "見積提出７日後のフォローをしましょう！\n\n";  		  					   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$body        = "5";                       		  // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);

		//-----------------------------------------------------------------------------------
		// クエリ用の値（契約後のフォローしましょう！（バイヤーズリモース））
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-3 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  		  										   // データを取得するアプリID
		$query  	 = "( 受注日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "契約後のフォローしましょう！（バイヤーズリモース）";  		 		 	   // メールタイトル
		$header   	 = "契約後のフォローしましょう！（バイヤーズリモース）\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$body        = "6";                       		  // 使用する繰り返し本文
		$addlist     = "ino.kouei@gmail.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
//		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);
/*
		//***********************************************************************************
		// 記念日通知(記念日をお祝いしましょう！)
		//***********************************************************************************
		// 通知日を設定
		$setday = 13;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  														// データを取得するアプリID
			$query  	 = " ( 記念日_月_ in (\"".$today1."\")  ) and ( 記念日_日_ >= \"1\"  ) and ( 記念日_日_ <= \"31\"  )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、記念日を迎えらえる方です（社員関連）";  		  			// メールタイトル
			$header   	 = "来月、記念日を迎えらえる方です（社員関連）\n\nささやかなお祝いをしましょう！\n\n";  // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$body        = "1";                       													// 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
//			$mail->sendmailset($domain,$user,$passwd,TC_KINEN,$body,$addlist);
		}

		//***********************************************************************************
		// 記念日通知(お誕生日をお祝いしましょう！(社員家族))
		//***********************************************************************************
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 誕生日_月_ in (\"".$today1."\")  ) and ( 誕生日_日_ >= \"1\"  ) and ( 誕生日_日_ <= \"31\"  )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（社員家族）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です（社員家族）\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$body        = "2";                       													// 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
//			$mail->sendmailset($domain,$user,$passwd,TC_KINEN,$body,$addlist);
		}

		//***********************************************************************************
		// 記念日通知(お誕生日をお祝いしましょう！(社員本人))
		//***********************************************************************************
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 月 in (\"".$today1."\")  ) and ( 月 >= \"1\"  ) and ( 日 <= \"31\"  )";		 				   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（社員本人）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です（社員本人）\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$body        = "3";                       													// 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
//			$mail->sendmailset($domain,$user,$passwd,TC_KINEN,$body,$addlist);
		}

		//***********************************************************************************
		// 記念日通知(お誕生日をお祝いしましょう！(名刺))
		//***********************************************************************************
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "941"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 月 in (\"".$today1."\")  ) and ( 月 >= \"1\"  ) and ( 日 <= \"31\"  )";		 				   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（顧客）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です。\n\nささやかなお祝いをしましょう！\n\n";  	// 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$body        = "4";                       													// 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
//			$mail->sendmailset($domain,$user,$passwd,TC_KINEN,$body,$addlist);
		}

		//***********************************************************************************
		// 記念日通知(お誕生日をお祝いしましょう！(顧客))
		//***********************************************************************************
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月
//			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "942"; 					  																	   // データを取得するアプリID
			$query  	 = " ( 月_創業日_ in (\"".$today1."\")  ) and ( 日_創業日_ >= \"1\"  ) and ( 日_創業日_ <= \"31\"  )";		 			   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、創立日を迎えらえる会社様です（顧客）";  		  							   // メールタイトル
			$header   	 = "来月、創立日を迎えらえる会社様です。\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$body        = "5";                       													// 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
//			$mail->sendmailset($domain,$user,$passwd,TC_KINEN,$body,$addlist);
		}
*/
		//***********************************************************************************
		// 定期通知
		//***********************************************************************************
		// 通知日を設定
		$setday = 31;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date('m'); // 絞り込み条件

			$app  		 = "2226"; 					  // データを取得するアプリID
			$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
			$subject   	 = "メールタイトル";  		  // メールタイトル
			$header   	 = "TCのテストメールです。";  // 本文のヘッダー
			$mailapp     = "";	                      // メールアドレスを取得するアプリID
			$address 	 = "";						  // メールアドレス項目名(テーブル不可)
			$body        = "1";                       // 使用する繰り返し本文
			$addlist     = "ino.kouei@gmail.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
	//		$mail->sendmailset($domain,$user,$passwd,TC_TEIKI,$body,$addlist);
		}

		//***********************************************************************************
		// ●【着工予定一覧通知】毎週月曜日に来月の着工予定一覧をお知らせします。
		//***********************************************************************************
		if(date("H") == "00"){
			// 通知日を設定
			$setday = "月";

			// 本日の曜日を取得
			$w_no = date("w");
			$w_array = array("日", "月", "火", "水", "木", "金", "土");

			// 設定日が当日ならメール作成処理
			if( $setday == $w_array[$w_no] ){

				// 条件設定
				$today1 = date("Y-m-01",strtotime("+1 month")); 		// 来月
				$today2 = date('Y-m-d', mktime(0, 0, 0, date('m') + 2, 0, date('Y')));

				$app  		 = "11"; 					  // データを取得するアプリID
				$query  	 = "( 着工予定日 >= \"".$today1."\") and ( 着工予定日 <= \"".$today2."\")"; // 絞り込みをするクエリ文字列
				$subject   	 = "【着工予定一覧通知】毎週月曜日に来月の着工予定一覧";  		 			// メールタイトル
				$header   	 = "来月、着工予定の案件一覧をお知らせします。";  // 本文のヘッダー
				$mailapp     = "10";	                      			  // メールアドレスを取得するアプリID
				$address 	 = "メールアドレス_PC";						  // メールアドレス項目名(テーブル不可)
				$body        = "2";                       // 使用する繰り返し本文
				$addlist     = "ino.kouei@gmail.com";

				// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
				$mail = new TcAlertSet();
				$mail->setAppID(  $app );
				$mail->setQuery(  $query );
				$mail->setsubject( $subject );
				$mail->setHeader( $header );
				$mail->setMailapp( $mailapp );
				$mail->setAddress( $address );

				// メール作成、送信処理
				$mail->sendmailset($domain,$user,$passwd,TC_TEIKI,$body,$addlist);
			}
		}

		//***********************************************************************************
		// 期限通知google差し込み
		//***********************************************************************************
		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知初回google(○○日当日)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-1 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "";          	          // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "digitalhisyo"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "timeconcier";  // 設定元として使用するgoogleパスワード

		//差し込み日付を作成
		$step1 = array();
		$step1[] = $today1;

		//ToDoを作成
		$step2 = array();
		$step2[] = "フォロー";

		//手段を作成
		$step3 = array();
		$step3[] = "電話";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// google通知処理
//		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3);

		//***********************************************************************************
		// ステップ通知google差し込み
		//***********************************************************************************
		//-----------------------------------------------------------------------------------
		// クエリ用の値（ステップ通知初回google(○○日当日)）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "";                        // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "digitalhisyo"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "timeconcier";  // 設定元として使用するgoogleパスワード

		//ステップの回数分差し込み日付を作成
		$step1 = array();
		$step1[] = date("Y-m-d",strtotime("+3 day"));
		$step1[] = date("Y-m-d",strtotime("+7 day"));
		$step1[] = date("Y-m-d",strtotime("+21 day"));

		//ステップの回数分ToDoを作成
		$step2 = array();
		$step2[] = "フォロー";
		$step2[] = "お礼";
		$step2[] = "プレゼント";

		//ステップの回数分手段を作成
		$step3 = array();
		$step3[] = "電話";
		$step3[] = "訪問";
		$step3[] = "郵送";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// google通知処理
//		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3);

		//-----------------------------------------------------------------------------------
		// ●【契約後フォロー通知】契約日の(3・7・21)日後に施工待ち状態のフォローしましょう！
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d"); // 対象日付(当日)
		$joken1 = "3.契約(施工待ち)";

		$app  		 = "11"; 					  // データを取得するアプリID
		$query  	 = "( 契約日 != \""."\") and ( 進捗状況 in (\"".$joken1."\") ) and ( 契約後フォロー in (\""."\") )"; // 絞り込みをするクエリ文字列
		$subject   	 = "【契約後フォロー通知】契約日の(3・7・21)日後に施工待ち状態のフォローしましょう！";  		  // メールタイトル
		$header   	 = "施工待ちのお客様のうち、\n契約日から(3・7・21)日後のお客様情報をお知らせします。";  // 本文のヘッダー
		$mailapp     = "";                        // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "ino.kouei@gmail.com"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "inokouzaburou";  	  // 設定元として使用するgoogleパスワード

		//ステップの回数分差し込み日付を作成
//		$step1 = array();
//		$step1[] = date("Y-m-d",strtotime("+3 day"));
//		$step1[] = date("Y-m-d",strtotime("+7 day"));
//		$step1[] = date("Y-m-d",strtotime("+21 day"));

		//ステップの対象項目を設定
		$stepkom = "契約日";
		$stepchk = "契約後フォロー";
		$stepno  = "1";

		//ステップの回数分ToDoを作成
		$step2 = array();
		$step2[] = " お礼状";
		$step2[] = " フォロー";
		$step2[] = " フォロー";

		//ステップの回数分手段を作成
		$step3 = array();
		$step3[] = "郵送 3日目";
		$step3[] = "訪問 7日目";
		$step3[] = "訪問 21日目";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// google通知処理
		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$stepkom,$step2,$step3,$stepchk,$stepno);

		//-----------------------------------------------------------------------------------
		// ●【定期点検フォロー】完工日から半年・1・3・5・7年後にフォローを行う！(google通知)
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "11"; 					  				  // データを取得するアプリID
		$query  	 = "( 完工日 != \""."\") and ( 定期点検フォロー in (\""."\") )"; 	  // 絞り込みをするクエリ文字列
		$subject   	 = "【定期点検フォロー】完工日から(半年・1・3・5・7)年後のお客様のお知らせ";  		  // メールタイトル
		$header   	 = "完工日から(半年・1・3・5・7)年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！\n\n";;  // 本文のヘッダー
		$mailapp     = "";                        // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "ino.kouei@gmail.com"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "inokouzaburou";  	  // 設定元として使用するgoogleパスワード

		//ステップの回数分差し込み日付を作成
//		$step1 = array();
//		$step1[] = date("Y-m-d",strtotime("+6 month"));
//		$step1[] = date("Y-m-d",strtotime("+1 year"));
//		$step1[] = date("Y-m-d",strtotime("+3 year"));
//		$step1[] = date("Y-m-d",strtotime("+5 year"));
//		$step1[] = date("Y-m-d",strtotime("+7 year"));

		//ステップの対象項目を設定
		$stepkom = "完工日";
		$stepchk = "定期点検フォロー";
		$stepno  = "2";

		//ステップの回数分ToDoを作成
		$step2 = array();
		$step2[] = " 半年後フォロー";
		$step2[] = " 1年後フォロー";
		$step2[] = " 3年後フォロー";
		$step2[] = " 5年後フォロー";
		$step2[] = " 7年後フォロー";

		//ステップの回数分手段を作成
		$step3 = array();
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// google通知処理
		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$stepkom,$step2,$step3,$stepchk,$stepno);
	}

?>