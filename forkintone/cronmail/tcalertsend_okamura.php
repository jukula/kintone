<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset.php               				                 */
/*   更新履歴   2014/02/05  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("tcgoogleset.php");

	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcAlertSet_okamura
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数

		var $DomainName = "";			// ドメイン名

		var $AppID 	  	= "";             // アプリID 
		var $Query 	  	= array();        // SQL文
		var $subject  	= "";     	    // メールタイトル
		var $Header   	= "";     	    // ヘッダー
		var $Mailapp  	= "";     	    // メール送信先アプリID
		var $Address  	= "";     	    // メール送信先項目
		var $getAddress = "";     	    // メール送信先項目
		var $Condition1 = "";     	    // クエリ条件
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAlertSet() {
	        $this->err = new TcError();
	    }
	    /*************************************************************************/
    	/* メンバ関数                                                            */
    	/*************************************************************************/
		// データ差し込み用のアプリIDを取得
	    function setAppID( $arg ) {
	        $this->AppID = $arg;
	    }
	    function getAppID() {
	        return( $this->AppID );
	    }

		// データ取得用のクエリを取得
	    function setQuery( $arg ) {
	        $this->Query = $arg;
	    }
	    function getQuery() {
	        return( $this->Query );
	    }

		// メールタイトル
	    function setsubject( $arg ) {
	        $this->subject = $arg;
	    }
	    function getsubject() {
	        return( $this->subject );
	    }

		// ヘッダーを取得
	    function setHeader( $arg ) {
	        $this->Header = $arg;
	    }
	    function getHeader() {
	        return( $this->Header );
	    }

		// メールアドレスを取得するアプリID
	    function setMailapp( $arg ) {
	        $this->Mailapp = $arg;
	    }
	    function getMailapp() {
	        return( $this->Mailapp );
	    }

		// メールアドレス項目名(テーブル不可)
	    function setAddress( $arg ) {
	        $this->Address = $arg;
	    }
	    function getAddress() {
	        return( $this->Address );
	    }

		// クエリ条件（テーブル用）
	    function setCondition1( $arg ) {
	        $this->Condition1 = $arg;
	    }
	    function getCondition1() {
	        return( $this->Condition1 );
	    }
		/*************************************************************************/
	    /* 文面作成の処理を実行する                                              */
	    /*  引数	$domain  (ドメイン)                                          */
	    /*  		$user    (ログインID)                                        */
	    /*  		$passwd  (ログインパスワード)                                */
	    /*  		$tuchi   (通知区分)                                          */
	    /*  		$bodyptr (使用する繰り返し本文NO)                            */
	    /*  		$addlist (送信先リスト)                                      */
	    /*************************************************************************/
		function sendmailset($domain,$user,$passwd,$tuchi,$bodyptr,$addlist) {
			$ret 	 = false;
			$setbody = "";

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();											// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;							// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			$this->DomainName = $domain;

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";

/*
				if( $this->Address != "" ){
			    	$k->strQuery = implode( $aryQ , " and ")." order by ".$this->Address." asc";
				}else{
			    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";
				}
*/
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{

					switch ($tuchi) {
					    case "1":
							$setbody .= $this->setbody1( $mail_json ,$bodyptr);
					        break;
					    case "2":
							$setbody .= $this->setbody2( $mail_json ,$bodyptr ,$domain ,$user ,$passwd ,$addlist);
					        break;
					    case "3":
							$setbody .= $this->setbody3( $mail_json ,$bodyptr);
					        break;
					    case "4":
							$setbody .= $this->setbody4( $mail_json ,$bodyptr);
					        break;
					    case "5":
							$setbody .= $this->setbody5( $mail_json ,$bodyptr);
					        break;
					    default:
					}
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

// 今は使わない
/*
			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->Mailapp;			// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
				if( $this->Address != "" ){
			    	$k->strQuery = implode( $aryQ , " and ")." order by ".$this->Address." asc";
				}else{
			    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";
				}
				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";
			$getAddress = $this->Address;

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->$getAddress->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->$getAddress->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->$getAddress->value;
						}
					}
				}
			}
*/

	if( $this->Address == "" ){
			// -------------
			// メールを送信する
			// -------------
			$subject    = $this->subject;    // タイトルの設定
			// 本文の設定
			$mailbody 	= $this->Header;
			$mailbody  .= $setbody; //繰り返し項目

			$sendmail = new tcAlert();
			// お客様向け
			$email = $addlist;
			$sendmail->setTo( $email );
			$sendmail->setTitle( $subject );
			$sendmail->setBody( $mailbody );
			$res = $sendmail->sendMail();
	}
			return $ret;

		}

		/*************************************************************************/
	    /*  googleカレンダーの処理を実行する(googleカレンダー)                   */
	    /*  引数	$domain    (ドメイン)                                        */
	    /*  	    $user       (ログインID)                                   	 */
	    /*  	    $passwd    (ログインパスワード)                              */
	    /*  	    $mainid     (googleアカウント)                               */
	    /*  	    $mainpass   (googleパスワード)                               */
	    /*  	    $step1      (差し込み日付)                                 	 */
	    /*  	    $step2      (ToDo)                                         	 */
	    /*  	    $step3      (電話)                                         	 */
	    /*************************************************************************/
		function sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3) {
			$ret 	 = false;
			$setbody = "";

			$this->DomainName = $domain;

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{
					$this->setbodygoogle( $mail_json, $mainid, $mainpass, $step1, $step2, $step3);
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			return $ret;


		}

	    /*************************************************************************/
	    /* 差込用データ作成(google通知)	                                         */
	    /*************************************************************************/
		function setbodygoogle($mail_json,$mainid,$mainpass,$step1,$step2,$step3) {

			$a = 0;
			$setlist  = "";
			$age      = "";
			$ageDay   = "";

			// googleカレンダー挿入
			$google = new TcgoogleSet();

			for($a = 0; $a < count($mail_json->records); $a++) {
				$Date="";
				$notifydate = $mail_json->records[$a]->生年月日->value;

				$search = array('株式会社','（株）','(株)','㈱','有限会社','（有）','(有)','㈲');

				$str = str_replace($search,'',$mail_json->records[$a]->顧客名->value);
				$str = 	mb_substr($str,0,6,'UTF-8');

				for($a = 0; $a < count($step1); $a++) {
					$Date		= explode("-", $notifydate);
					$notifydate = $step1[$a];            	  // 差し込み日付
					$title 		= $str.$step2[$a].$step3[$a]; // 差し込みタイトル
					$desc		= "https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value;

					$res 	= $google->setgoogle($mainid, $mainpass, $notifydate, $title, $desc);
				}
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成（期限通知）                                          */
	    /*************************************************************************/
		function setbody1($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."さん ".$ageDay." ".$age."\n";
					}
					break;
				case "2":
				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知）	                                 */
	    /*************************************************************************/
		function setbody2($mail_json , $bodyptr ,$domain ,$user ,$passwd ,$addlist) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$komadd   = $this->Address; // メールアドレス項目
			$komday   = 7;				//データ入力日-対象日付項目
			$chkday   = 0;
			$mailflg  = "";  							   // 送信正否の値(OK/NG)             
			$fmail    = "okamura-bungu@okamura-bungu.com"; // メールfrom
			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$tbcount = count($mail_json->records[$a]->名刺交換情報->value);

					    for($i = 0; $i <= ($tbcount); $i++) {
							if (($mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換日->value != "") && ($mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換日入力日->value != "") ){

								// 日付の差を計算
								$chkday = (strtotime($mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換日入力日->value) - strtotime($mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換日->value)) / ( 60 * 60 * 24);

								// 7日以上古ければ送らない
								if( ( $komday >=  $chkday ) && ($chkday >= 0) && ( $mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換日入力日->value == date("Y-m-d",strtotime("-1 day")) )){
									// 初期化
									$subject  = "";
									$mailbody = "";
									$email    = "";
									$setlist  = "";

									if($mail_json->records[$a]->名刺交換情報->value[$i]->value->メールテンプレートレコード番号->value != "" ){

										// ------------------------------------------------------
										// 同顧客で通知日以上の日付がないか検索（あれば通知不要）
										// ------------------------------------------------------
										$db = new TcKintone($domain,$user,$passwd);	// API連携クラス
										$db->parInit();								// API連携用のパラメタを初期化する
										$db->intAppID 	= "33";				// アプリID

									    $db->strQuery = "( レコード番号 = ".$mail_json->records[$a]->名刺交換情報->value[$i]->value->メールテンプレートレコード番号->value.")" ; // クエリパラメータ

										$nipo_json = $db->runCURLEXEC( TC_MODE_SEL );

										if( $db->intDataCount > 0 ) {
											$subject    = $nipo_json->records[0]->件名->value;    // タイトルの設定
											// 対象データ文章の組み立て
											$setlist.=	$mail_json->records[$a]->顧客名->value."\n".$mail_json->records[$a]->社員名->value."　様\n\n";
											$setlist.=	$nipo_json->records[0]->本文->value."\n";
										}
									}else{

/*
										$subject    = $this->subject;    // タイトルの設定
										// 対象データ文章の組み立て
										$setlist.=	$mail_json->records[$a]->顧客名->value."\n".$mail_json->records[$a]->社員名->value."　様\n\n";
										$setlist.=	"メールにて失礼いたします。\n";
										$setlist.=	"先日の経営セミナーにて名刺を頂戴しました、\n";
										$setlist.=	"株式会社○○・".$mail_json->records[$a]->名刺交換情報->value[$i]->value->名刺交換担当者->value[0]->name."です。\n";
										$setlist.=	"この度はご縁をいただき、ありがとうございます。\n\n";
										$setlist.=	"また、お目にかかれることを楽しみにいたしております。\n";
										$setlist.=	"今後とも、どうぞよろしくお願いいたします。\n";
*/
									}

									$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

									if($email != "" && $mail_json->records[$a]->名刺交換情報->value[$i]->value->メールテンプレートレコード番号->value != "" ){
										// ヘッダーと繋ぐ
										$mailbody 	= $this->Header;	 // 本文の設定
										$mailbody  .= $setlist; 		 //繰り返し項目

										// メール送信処理
										$sendmail = new tcAlert();
										$sendmail->setFrom( $fmail , "岡村文具" );
										$sendmail->setTo( $email );
										$sendmail->setTitle( $subject );
										$sendmail->setBody( $mailbody );

										$res = $sendmail->sendMail( "", "", $addlist );

										$mailset = ""; // 初期化

										if ( $res ) {
											// echo "メールの送信に成功しました。";
											$mailset = "OK";
										} else {
											// echo "メールの送信に失敗しました。";
											$mailset = "NG";
										}
									}else{
										// メールアドレス無し
										$mailset = "NG";
									}

									// --------------------------------
									// メール送信ログへインサートする。
									// --------------------------------
									// 書込み準備
									$recObj = new stdClass;

									$recObj->結果			  		= $this->valEnc($mailset);
									$recObj->送信日付		  		= $this->valEnc(date("Y-m-d",strtotime("0 day")));
									$recObj->送信時刻	 	  		= $this->valEnc(date("H:i", time() ));
									$recObj->送信元メールアドレス   = $this->valEnc($fmail);
									$recObj->送信先メールアドレス   = $this->valEnc($email);
									$recObj->メールテンプレート		= $this->valEnc($mail_json->records[$a]->名刺交換情報->value[$i]->value->メールテンプレートレコード番号->value);

									$k = new TcKintone($domain,$user,$passwd);
									$k->parInit();						// API連携用のパラメタを初期化する
									$k->intAppID 		= "41";			// アプリID(メール送信ログ)
									$k->strContentType	= "Content-Type: application/json";

									$insData 			= new stdClass;
									$insData->app 		= "41";
									$insData->records[] 	= $recObj;

									$k->aryJson = $insData;						// 追加対象のレコード番号
									$json = $k->runCURLEXEC( TC_MODE_INS );

								}
							}
						}
					}
					break;
				case "2":
				    break;
			    case "3":


					for($a = 0; $a < count($mail_json->records); $a++) {

						// ------------------------------------------------------
						// 同顧客で通知日以上の日付がないか検索（あれば通知不要）
						// ------------------------------------------------------
						$db = new TcKintone($domain,$user,$passwd);	// API連携クラス
						$db->parInit();								// API連携用のパラメタを初期化する
						$db->intAppID 	= $this->AppID;				// アプリID

					    $db->strQuery = "( 案件レコード番号 = ".$mail_json->records[$a]->案件レコード番号->value.") and ( アクション実施日 > \"".$mail_json->records[$a]->アクション実施日->value."\"))" ; // クエリパラメータ
						$nipo_json = $db->runCURLEXEC( TC_MODE_SEL );

						if( $db->intDataCount == 0 ) {
							// 対象データ文章の組み立て
							$setlist.=	"顧客名　　　　　　：".$mail_json->records[$a]->顧客名->value."\n";
							$setlist.=	"最終アクション日　：".$mail_json->records[$a]->アクション実施日->value."\n";
							$setlist.=	"レコード　　　　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
							$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス
						}
					}




			        break;
			    case "4":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"納品日　　　：".$mail_json->records[$a]->納品日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}
			        break;
			    case "5":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"見積提出日　：".$mail_json->records[$a]->見積提出日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}
			        break;
			    case "6":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"受注日　　　：".$mail_json->records[$a]->受注日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}

			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（記念日通知）	                                     */
	    /*************************************************************************/
		function setbody3($mail_json , $bodyptr) {

			$i = 0;
			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$age      = ""; // 年齢
			$ageDay   = ""; // 誕生日
			$tbcount  = 0;  // テーブルレコード数
			$nowyear  = date('Y');
			$setyear  = "";

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$tbcount = count($mail_json->records[$a]->記念日テーブル->value);

					    for($i = 0; $i <= ($tbcount); $i++) {
							if( ( $mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_月_->value == $this->Condition1 ) && ($mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_日_->value != "" )){
								$setlist.=	$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日名->value." ".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_月_->value."月".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_日_->value."日\n";
							}
						}
					}
					break;
				case "2":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$tbcount = count($mail_json->records[$a]->家族テーブル->value);

					    for($i = 0; $i <= ($tbcount); $i++) {
							if( ( $mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_月_->value == $this->Condition1 ) && ($mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_日_->value != "" )){

								if( $mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_年_->value != ""){
									$setyear = (intval($nowyear) - intval($mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_年_->value));
								}else{
									$setyear = "";
								}

								$setlist.=	$mail_json->records[$a]->社員名->value."さんの ".$mail_json->records[$a]->家族テーブル->value[$i]->value->続柄->value." ". $mail_json->records[$a]->家族テーブル->value[$i]->value->家族名->value ." ".$mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_月_->value."月".$mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_日_->value."日(".$setyear.")\n";
							}
						}
					}
				    break;
			    case "3":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->事業所->value." ".$mail_json->records[$a]->所属->value." ".$mail_json->records[$a]->役職->value." ".$mail_json->records[$a]->職種->value." ".$mail_json->records[$a]->月->value."月".$mail_json->records[$a]->日->value."日(".$setyear.")\n";

					}
			        break;
			    case "4":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->月->value."月".$mail_json->records[$a]->日->value."日(".$setyear.")\n";

					}
			        break;
			    case "5":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年_創業日_->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年_創業日_->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->代表者名->value."社長 ".$mail_json->records[$a]->年_創業日_->value."年".$mail_json->records[$a]->月_創業日_->value."月".$mail_json->records[$a]->日_創業日_->value."日(".$setyear.")\n";

					}
			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（定期通知）		                                 */
	    /*************************************************************************/
		function setbody4($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."\n";
					}
					break;
				case "2":
				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知(初回)）	                             */
	    /*************************************************************************/
		function setbody5($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."\n";
					}
					break;
				case "2":
				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
