<?php
/*****************************************************************************/
/* 	 googleカレンダー送信PHP                                  (Version 1.01) */
/*   ファイル名 : tcgoogleset.php               				             */
/*   更新履歴   2014/04/12  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/

	$clsSrs = new TcgoogleSet();

	const CLIENT_ID 		   = "796436853406-v47m2icm5hqtuu2ovaaguf3a70au4b1c.apps.googleusercontent.com";
	const SERVICE_ACCOUNT_NAME = "796436853406-v47m2icm5hqtuu2ovaaguf3a70au4b1c@developer.gserviceaccount.com";
	const KEY_FILE 			   = "/home/cross-tier/timeconcier.jp/public_html/service/googlecalendar/1eedcc5fe52f25f7b1815d77a36ce9c9f4cbe65f-privatekey.p12";

	// 実行
	$clsSrs->setgoogle();
	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcgoogleSet
	{

		/*************************************************************************/
	    /* googleカレンダーへ差し込み                                            */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function setgoogle() {
			//*****************************
	      	// Account check
			//*****************************
echo("動作しました");

			$calendarId = "ino.kouei@gmail.com";

		    $title 	  = "テスト中です";				//タイトル
			$where 	  = ""; 						//場所
			$desc  	  = "本文";     				//本文
		    $start 	  = "2014-12-30";				//開始日
		    $end   	  = "2014-12-30";				//終了日
			$startT   = "";							//開始時刻
			$endT  	  = "";							//終了時刻
			$Calendar = "";							//差し込み先カレンダー
			$rmgkKei  = "OK";

			//*****************************
	      	// 登録処理
			//*****************************
			// googleAPIの読み込み
			require_once "/home/cross-tier/timeconcier.jp/public_html/service/googlecalendar/src/Google_Client.php";
			require_once "/home/cross-tier/timeconcier.jp/public_html/service/googlecalendar/src/contrib/Google_CalendarService.php";

			$client = new Google_Client();
			$client->setApplicationName("Google Prediction Sample");
			$client->setAssertionCredentials(new Google_AssertionCredentials(
	    		SERVICE_ACCOUNT_NAME,
	    		array('https://www.googleapis.com/auth/calendar'),
	    		file_get_contents(KEY_FILE)
			));

			/**
			 * 予定(イベント)の作成
			 */
			// 日付の設定
			$startDate = $start;
			$startTime = $startT;
			$endDate = $end;
			$endTime = $endT;
			$tzOffset = "+09";

			$service = new Google_CalendarService($client);
			$event = new Google_Event();
			if($startT == "" || $endT == "" ){
				$endDate = date( "Y-m-d", strtotime( $startDate." +1 day" ) );
				$start_time = new Google_EventDateTime();
				$start_time->setDateTime("{$startDate}T00:00:00+09:00");
				$event->setStart($start_time); //開始日
				$end_time = new Google_EventDateTime();
				$end_time->setDateTime("{$endDate}T00:00:00+09:00");
				$event->setEnd($end_time); //終了日
			}else{
				$start_time = new Google_EventDateTime();
				$start_time->setDateTime("{$startDate}T{$startTime}:00.000{$tzOffset}:00");
				$event->setStart($start_time); //開始日
				$end_time = new Google_EventDateTime();
				$end_time->setDateTime("{$endDate}T{$endTime}:00.000{$tzOffset}:00");
				$event->setEnd($end_time); //終了日
			}
			$event->setLocation($where); // 場所
			$event->setSummary($title); // タイトル
			$event->setDescription($desc); // 説明
			$service->events->insert($calendarId, $event);

		}
	}
?>

   
