<?php 
/*****************************************************************************/
/* 	 メール設定PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset.php               						         */
/*   更新履歴   2014/09/19  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*   [必要ファイル]                                                          */
/*      																     */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/

	include_once("tcalertsend_ogasa.php");
	include_once("tckintone.php");


	// シナリオ設定
	define( "TC_KIGEN_SCENARIO_01" 	 , false ); // 期限通知(クエリ用の値（○○日の○○前）)

	define( "TC_STEP_SCENARIO_01" 	 , false ); // ステップ通知(クエリ用の値（ステップ通知初回(○○日当日)）)
	define( "TC_STEP_SCENARIO_02" 	 , false ); // ステップ通知(クエリ用の値（ステップ通知1回(○○日後)）)
	define( "TC_STEP_SCENARIO_03" 	 , false ); // ステップ通知(クエリ用の値（ステップ通知2回(○○日後)）)
	define( "TC_STEP_SCENARIO_04" 	 , false ); // ステップ通知(クエリ用の値（ステップ通知3回(○○日後)）)
	define( "TC_STEP_SCENARIO_05" 	 , false ); // ステップ通知(クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム３日後））)
	define( "TC_STEP_SCENARIO_06" 	 , false ); // ステップ通知(クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム７日後））)
	define( "TC_STEP_SCENARIO_07" 	 , false ); // ステップ通知(クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム２１日後））)
	define( "TC_STEP_SCENARIO_08" 	 , false ); // ステップ通知(クエリ用の値（最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！））)
	define( "TC_STEP_SCENARIO_09" 	 , false ); // ステップ通知(クエリ用の値（納品１年後を記念日としてお客様をフォローしましょう！（引き渡し記念日））)
	define( "TC_STEP_SCENARIO_10" 	 , false ); // ステップ通知(クエリ用の値（見積提出７日後のフォローをしましょう！）)
	define( "TC_STEP_SCENARIO_11" 	 , false ); // ステップ通知(クエリ用の値（契約後のフォローしましょう！（バイヤーズリモース））)

	define( "TC_KINEN_SCENARIO_01" 	 , false ); // 記念日通知(記念日通知(記念日をお祝いしましょう！))
	define( "TC_KINEN_SCENARIO_02" 	 , false ); // 記念日通知(記念日通知(お誕生日をお祝いしましょう！(社員家族)))
	define( "TC_KINEN_SCENARIO_03" 	 , false ); // 記念日通知(記念日通知(お誕生日をお祝いしましょう！(社員本人)))
	define( "TC_KINEN_SCENARIO_04" 	 , false ); // 記念日通知(記念日通知(お誕生日をお祝いしましょう！(名刺)))
	define( "TC_KINEN_SCENARIO_05" 	 , false ); // 記念日通知(記念日通知(お誕生日をお祝いしましょう！(顧客)))

	define( "TC_TEIKI_SCENARIO_01" 	 , false ); // 定期通知()
	define( "TC_GOOGLE_SCENARIO_01"  , false ); // 期限通知google差し込み()
	define( "TC_GOOGLES_SCENARIO_01" , false ); // ステップ通知google差し込み()

	// オガサ独自設定
	define( "TC_GOOGLE_SCENARIO_02" 	 , true ); // ●【工事完了後フォロー】完工日から半年・1・3・5・7・10年後にフォローを行う！(google通知)

	///////////////////////////////////////////////////////////
	// 基本接続情報設定
	$domain = "ogasaseikou.cybozu.com"; // ドメイン
	$user   = "Administrator";			// ID
	$passwd = "Tc2010";			// パスワード
	///////////////////////////////////////////////////////////

	//***********************************************************************************
	// 期限通知
	//***********************************************************************************
	//-----------------------------------------------------------------------------------
	// 定期通知(名刺管理から来月誕生日の方を月末に通知)
	//-----------------------------------------------------------------------------------
	if( TC_KIGEN_SCENARIO_01 ){
		// クエリ用の値（○○日の○○前）
		$today1 = date("Y-m-d",strtotime("-1 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist	 = "komatsu@cross-tier.com";  // 送信先リスト

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_KIGEN,TCTYPE_PTN1,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// 当日の誕生日のお知らせ
	//-----------------------------------------------------------------------------------
	if( TC_KIGEN_SCENARIO_02 ){

		$today1 = date("n"); 	// 今月の月
		$today2 = date('j');    // 今日の日付

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\") and ( 日 = \"".$today2."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "【記念日通知】本日、誕生日です";  		  // メールタイトル
		$header   	 = $today1."月".$today2."日、誕生日を迎えらえる方です。\n電話やメールで連絡すると喜んでいただけますよ。\n\n--------------------------------\n\n";  // 本文のヘッダー
		$mailapp     = "";                    // メールアドレスを取得するアプリID
		$address 	 = "";	  // メールアドレス項目名(テーブル不可)
		$addlist	 = "m.fukushima@timeconcier.jp,j.kataoka@timeconcier.jp";  // 送信先リスト

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_KIGEN,TCTYPE_PTN2,$addlist);
	}

	//***********************************************************************************
	// ステップ通知
	//***********************************************************************************
	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知初回(○○日当日)）
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_01 ){
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist	 = "komatsu@cross-tier.com";  // 送信先リスト

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP_1ST,TCTYPE_PTN1,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知1回(○○日後)）
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_02 ){
		$today1 = date("Y-m-d",strtotime("+3 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN2,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知2回(○○日後)）
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_03 ){
		$today1 = date("Y-m-d",strtotime("+7 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "2204";                    // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN3,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知3回(○○日後)）
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_04 ){
		$today1 = date("Y-m-d",strtotime("+21 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN4,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_05 ){
		$today1 = date("Y-m-d",strtotime("-3 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品３日後のフォローをしましょう！";  		  // メールタイトル
		$header   	 = "納品３日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN4,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_06 ){
		$today1 = date("Y-m-d",strtotime("-7 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品７日後のフォローをしましょう！";  		  // メールタイトル
		$header   	 = "納品７日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN4,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（［案件］納品後のフォローをしてお客様にファンになっていただきましょう！（２１日間感動プログラム））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_07 ){
		$today1 = date("Y-m-d",strtotime("-21 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "納品２１日後のフォローをしましょう！";  // メールタイトル
		$header   	 = "納品２１日後のフォローをしましょう！\n\n";  // 本文のヘッダー
		$mailapp     = ""; 	 	                  // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN4,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_08 ){
		$today1 = date("Y-m-d",strtotime("-30 day")); // 対象日付(○日・○月・○年前)

		$app  		 = "945"; 					  		  						   // データを取得するアプリID
		$query  	 = "( アクション実施日 = \"".$today1."\")"; 				   // 絞り込みをするクエリ文字列
		$subject   	 = "最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！";  		 		 	   // メールタイトル
		$header   	 = "最終アクション（営業・取引・来店）日から３０日経過した顧客をフォローしましょう！\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN3,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（納品１年後を記念日としてお客様をフォローしましょう！（引き渡し記念日））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_09 ){
		$today1 = date("Y-m-d",strtotime("-1 year")); // 対象日付(○日・○月・○年前)

		$app  		 = "944"; 					  		  						   // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; 							   // 絞り込みをするクエリ文字列
		$subject   	 = "納品１年後のフォローをしましょう！";  		 		 	   // メールタイトル
		$header   	 = "納品１年後のフォローをしましょう！\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN4,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（見積提出７日後のフォローをしましょう！）
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_10 ){
		$today1 = date("Y-m-d",strtotime("-7 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  		  										   // データを取得するアプリID
		$query  	 = "( 見積提出日 = \"".$today1."\") and (進捗状況 in (\""."3.提案見積"."\"))"; // 絞り込みをするクエリ文字列
		$subject   	 = "見積提出７日後のフォローをしましょう！";  		 		 				   // メールタイトル
		$header   	 = "見積提出７日後のフォローをしましょう！\n\n";  		  					   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN5,$addlist);
	}

	//-----------------------------------------------------------------------------------
	// クエリ用の値（契約後のフォローしましょう！（バイヤーズリモース））
	//-----------------------------------------------------------------------------------
	if( TC_STEP_SCENARIO_11 ){
		$today1 = date("Y-m-d",strtotime("-3 day")); // 対象日付(○日・○月前)

		$app  		 = "944"; 					  		  										   // データを取得するアプリID
		$query  	 = "( 受注日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "契約後のフォローしましょう！（バイヤーズリモース）";  		 		 	   // メールタイトル
		$header   	 = "契約後のフォローしましょう！（バイヤーズリモース）\n\n";  		  		   // 本文のヘッダー
		$mailapp     = "";                    		      // メールアドレスを取得するアプリID
		$address 	 = "メールアドレス_PC_";		 	  // メールアドレス項目名(テーブル不可)
		$addlist     = "komatsu@cross-tier.com";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TCTYPE_STEP,TCTYPE_PTN6,$addlist);
	}

	//***********************************************************************************
	// 記念日通知(記念日をお祝いしましょう！)
	//***********************************************************************************
	if( TC_KINEN_SCENARIO_01 ){
		// 通知日を設定
		$setday = 13;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); // 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  														// データを取得するアプリID
			$query  	 = " ( 記念日_月_ in (\"".$today1."\")  ) and ( 記念日_日_ >= \"1\"  ) and ( 記念日_日_ <= \"31\"  )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、記念日を迎えらえる方です（社員関連）";  		  			// メールタイトル
			$header   	 = "来月、記念日を迎えらえる方です（社員関連）\n\nささやかなお祝いをしましょう！\n\n";  // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_KINENBI,TCTYPE_PTN1,$addlist);
		}
	}

	//***********************************************************************************
	// 記念日通知(お誕生日をお祝いしましょう！(社員家族))
	//***********************************************************************************
	if( TC_KINEN_SCENARIO_02 ){
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 誕生日_月_ in (\"".$today1."\")  ) and ( 誕生日_日_ >= \"1\"  ) and ( 誕生日_日_ <= \"31\"  )"; // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（社員家族）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です（社員家族）\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_KINENBI,TCTYPE_PTN2,$addlist);
		}
	}

	//***********************************************************************************
	// 記念日通知(お誕生日をお祝いしましょう！(社員本人))
	//***********************************************************************************
	if( TC_KINEN_SCENARIO_03 ){
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "943"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 月 in (\"".$today1."\")  ) and ( 月 >= \"1\"  ) and ( 日 <= \"31\"  )";		 				   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（社員本人）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です（社員本人）\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_KINENBI,TCTYPE_PTN3,$addlist);
		}
	}

	//***********************************************************************************
	// 記念日通知(お誕生日をお祝いしましょう！(名刺))
	//***********************************************************************************
	if( TC_KINEN_SCENARIO_04 ){
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "941"; 					  																		   // データを取得するアプリID
			$query  	 = " ( 月 in (\"".$today1."\")  ) and ( 月 >= \"1\"  ) and ( 日 <= \"31\"  )";		 				   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、誕生日を迎えらえる方です（顧客）";  		  							   // メールタイトル
			$header   	 = "来月、誕生日を迎えらえる方です。\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_KINENBI,TCTYPE_PTN4,$addlist);
		}
	}

	//***********************************************************************************
	// 記念日通知(お誕生日をお祝いしましょう！(顧客))
	//***********************************************************************************
	if( TC_KINEN_SCENARIO_05 ){
		// 通知日を設定
		$setday = 9;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date("m",strtotime("+1 month")); 		// 来月1日
			$today2 = date('Y-m-t', strtotime($today1));    // 来月末

			$app  		 = "942"; 					  																	   // データを取得するアプリID
			$query  	 = " ( 月_創業日_ in (\"".$today1."\")  ) and ( 日_創業日_ >= \"1\"  ) and ( 日_創業日_ <= \"31\"  )";		 			   // 絞り込みをするクエリ文字列
			$subject   	 = "【記念日通知】来月、創立日を迎えらえる会社様です（顧客）";  		  							   // メールタイトル
			$header   	 = "来月、創立日を迎えらえる会社様です。\n\nささやかなお祝いをしましょう！\n\n";  			   // 本文のヘッダー
			$mailapp     = "";                    														// メールアドレスを取得するアプリID
			$address 	 = "";		  																	// メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";


			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );
			$mail->setCondition1( $today1 );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_KINENBI,TCTYPE_PTN5,$addlist);
		}
	}

	//***********************************************************************************
	// 定期通知
	//***********************************************************************************
	if( TC_TEIKI_SCENARIO_01 ){
		// 通知日を設定
		$setday = 31;

		// 31の場合、月末を設定
		if($setday == 31){
			$setday = date('t'); // 31の場合、月末を設定
		}

		// 設定日が当日ならメール作成処理
		if(date("d") == $setday ){

			// 条件設定
			$today1 = date('m'); // 絞り込み条件

			$app  		 = "2226"; 					  // データを取得するアプリID
			$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
			$subject   	 = "メールタイトル";  		  // メールタイトル
			$header   	 = "TCのテストメールです。";  // 本文のヘッダー
			$mailapp     = "";	                      // メールアドレスを取得するアプリID
			$address 	 = "";						  // メールアドレス項目名(テーブル不可)
			$addlist     = "komatsu@cross-tier.com";

			// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
			$mail = new TcAlertSet();
			$mail->setAppID(  $app );
			$mail->setQuery(  $query );
			$mail->setsubject( $subject );
			$mail->setHeader( $header );
			$mail->setMailapp( $mailapp );
			$mail->setAddress( $address );

			// メール作成、送信処理
			$mail->sendmailset($domain,$user,$passwd,TCTYPE_TEIKI,TCTYPE_PTN1,$addlist);
		}
	}

	//***********************************************************************************
	// 期限通知google差し込み
	//***********************************************************************************
	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知初回google(○○日当日)）
	//-----------------------------------------------------------------------------------
	if( TC_GOOGLE_SCENARIO_01 ){
		$today1 = date("Y-m-d",strtotime("-1 day")); // 対象日付(○日・○月前)

		$app  		 = "2226"; 					  // データを取得するアプリID
		$query  	 = "( 月 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "";          	          // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "digitalhisyo"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "timeconcier";  // 設定元として使用するgoogleパスワード

		//差し込み日付を作成
		$step1 = array();
		$step1[] = $today1;

		//ToDoを作成
		$step2 = array();
		$step2[] = "フォロー";

		//手段を作成
		$step3 = array();
		$step3[] = "電話";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3);
	}

	//***********************************************************************************
	// ステップ通知google差し込み
	//***********************************************************************************
	//-----------------------------------------------------------------------------------
	// クエリ用の値（ステップ通知初回google(○○日当日)）
	//-----------------------------------------------------------------------------------
	if( TC_GOOGLES_SCENARIO_01 ){
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "944"; 					  // データを取得するアプリID
		$query  	 = "( 納品日 = \"".$today1."\")"; // 絞り込みをするクエリ文字列
		$subject   	 = "メールタイトル";  		  // メールタイトル
		$header   	 = "TCのテストメールです。";  // 本文のヘッダー
		$mailapp     = "";                        // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "digitalhisyo"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "timeconcier";  // 設定元として使用するgoogleパスワード

		//ステップの回数分差し込み日付を作成
		$step1 = array();
		$step1[] = date("Y-m-d",strtotime("+3 day"));
		$step1[] = date("Y-m-d",strtotime("+7 day"));
		$step1[] = date("Y-m-d",strtotime("+14 day"));

		//ステップの回数分ToDoを作成
		$step2 = array();
		$step2[] = "フォロー";
		$step2[] = "お礼";
		$step2[] = "プレゼント";

		//ステップの回数分手段を作成
		$step3 = array();
		$step3[] = "電話";
		$step3[] = "訪問";
		$step3[] = "郵送";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3);
	}

	//*******************************************************************************************************************
	// 期限通知google差し込み(●【工事完了後フォロー】完工日から1・3・5・7・10年後にフォローを行う！(google通知))
	//*******************************************************************************************************************
	if( TC_GOOGLE_SCENARIO_02 ){
		$today1 = date("Y-m-d"); // 対象日付(当日)

		$app  		 = "42"; 					  				  								// データを取得するアプリID
		$query  	 = "( 工事完了日 != \""."\") and ( 工事完了後フォロー in (\""."\") )"; 	    	// 絞り込みをするクエリ文字列
		$subject   	 = "【定期点検フォロー】完工日から(1・3・5・7・10)年後のお客様のお知らせ";  // メールタイトル
		$header   	 = "完工日から(1・3・5・7・10)年後のお客様情報をお知らせします。\n顧客フォローを行いましょう！\n\n";;  // 本文のヘッダー
		$mailapp     = "";                        // メールアドレスを取得するアプリID
		$address 	 = "";						  // メールアドレス項目名(テーブル不可)

	 	$mainid   	 = "cc.ogasaseikou@gmail.com"; // 設定元として使用するgoogleアカウント
	 	$mainpass 	 = "ogasaseikou";  	  		   // 設定元として使用するgoogleパスワード

		//ステップの対象項目を設定
		$stepkom = "工事完了日";
		$stepchk = "工事完了後フォロー";

		//ステップの回数分ToDoを作成
		$step2 = array();
		$step2[] = " 1年後フォロー";
		$step2[] = " 3年後フォロー";
		$step2[] = " 5年後フォロー";
		$step2[] = " 7年後フォロー";
		$step2[] = " 10年後フォロー";

		//ステップの回数分手段を作成
		$step3 = array();
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";
		$step3[] = "訪問";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// google通知処理
		$mail->sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$stepkom,$step2,$step3,$stepchk,TCTYPE_PTN2);
	}

?>