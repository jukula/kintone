<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset.php               				                 */
/*   更新履歴   2014/02/05  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("tcgoogleset.php");

	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcAlertSet
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数

		var $AppID 	  = "";             // アプリID 
		var $Query 	  = array();        // SQL文
		var $subject  = "";     	    // メールアドレス
		var $Header   = "";     	    // ヘッダー
		var $Mailapp  = "";     	    // メール送信先アプリID
		var $Address  = "";     	    // メール送信先項目
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAlertSet() {
	        $this->err = new TcError();
	    }
	    /*************************************************************************/
    	/* メンバ関数                                                            */
    	/*************************************************************************/
		// データ差し込み用のアプリIDを取得
	    function setAppID( $arg ) {
	        $this->AppID = $arg;
	    }
	    function getAppID() {
	        return( $this->AppID );
	    }

		// データ取得用のクエリを取得
	    function setQuery( $arg ) {
	        $this->Query = $arg;
	    }
	    function getQuery() {
	        return( $this->Query );
	    }

		// メールタイトル
	    function setsubject( $arg ) {
	        $this->subject = $arg;
	    }
	    function getsubject() {
	        return( $this->subject );
	    }

		// ヘッダーを取得
	    function setHeader( $arg ) {
	        $this->Header = $arg;
	    }
	    function getHeader() {
	        return( $this->Header );
	    }

		// メールアドレスを取得するアプリID
	    function setMailapp( $arg ) {
	        $this->Mailapp = $arg;
	    }
	    function getMailapp() {
	        return( $this->Mailapp );
	    }

		// メールアドレス項目名(テーブル不可)
	    function setAddress( $arg ) {
	        $this->Address = $arg;
	    }
	    function getAddress() {
	        return( $this->Address );
	    }

		/*************************************************************************/
	    /* 誕生日用の処理を実行する                                              */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function sendbirthday($domain,$user,$passwd) {
			$ret 	 = false;
			$setbody = "";

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{
					$setbody .= $this->setbodybirthday( $mail_json );
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );


			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->Mailapp;			// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";
			$getAddress = $this->Address;

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->$getAddress->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->$getAddress->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->$getAddress->value;
						}
					}
				}
			}

			// -------------
			// メールを送信する
			// -------------
			$subject= "今月が誕生日の方";    // タイトルの設定
			// 本文の設定
			$mailbody 	= $this->Header."\n";
			$mailbody  .= "今月誕生日のお客さまは以下のとおりです。\n";
			$mailbody  .= $setbody; //繰り返し項目

			$sendmail = new tcAlert();
			// お客様向け
			$email = "c.komatsu@timeconcier.jp";
			$sendmail->setTo( $email );
			$sendmail->setTitle( $subject );
			$sendmail->setBody( $mailbody );
			$res = $sendmail->sendMail();


			return $ret;


		}

		/*************************************************************************/
	    /* ○○○用の処理を実行する(googleカレンダー)                            */
	    /*  引数	なし                                                         */
	    /*************************************************************************/
		function sendgoogleset($domain,$user,$passwd,$mainid,$mainpass) {
			$ret 	 = false;
			$setbody = "";


			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{
					$setbody .= $this->setbody1( $mail_json, $mainid,$mainpass );
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );


			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->Mailapp;			// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";
			$getAddress = $this->Address;

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->$getAddress->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->$getAddress->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->$getAddress->value;
						}
					}
				}
			}

			return $ret;


		}

	    /*************************************************************************/
	    /* 差込用データ作成		                                                 */
	    /*************************************************************************/
		function setbody1($mail_json,$mainid,$mainpass) {

			$a = 0;
			$setlist  = "";
			$age      = "";
			$ageDay   = "";

			// googleカレンダー挿入
			$google = new TcgoogleSet();

			for($a = 0; $a < count($mail_json->records); $a++) {
				$Date="";
				$notifydate = $mail_json->records[$a]->生年月日->value;

				$search = array('株式会社','（株）','(株)','㈱','有限会社','（有）','(有)','㈲');

				$str = str_replace($search,'',$mail_json->records[$a]->顧客名->value);
				$str = 	mb_substr($str,0,6,'UTF-8');

if($notifydate != ""){
echo($str);
$Date = explode("-", $notifydate);
$notifydate = date("Y")."-".$Date[1]."-".$Date[2];
echo($notifydate);
				$title 	= $str." フォロー 電話";
				$res 	= $google->setgoogle($mainid, $mainpass, $notifydate, $title);
}
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成		                                                 */
	    /*************************************************************************/
		function setbodybirthday($mail_json) {

			$a = 0;
			$setlist  = "";
			$age      = "";
			$ageDay   = "";

			for($a = 0; $a < count($mail_json->records); $a++) {
				if( ($mail_json->records[$a]->誕生日_年_->value != "") && ($mail_json->records[$a]->月->value != "") && ($mail_json->records[$a]->日->value != "")){
					$age = $this->birthToAge($mail_json->records[$a]->誕生日_年_->value."-".$mail_json->records[$a]->月->value."-".$mail_json->records[$a]->日->value ) . "歳";
				}else{
					$age = "";
				}

				if( $mail_json->records[$a]->日->value != "" ){
					$ageDay = $mail_json->records[$a]->月->value."月".$mail_json->records[$a]->日->value . "日";
				}else{
					$ageDay = "";
				}

				$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."さん ".$ageDay." ".$age."\n";
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
