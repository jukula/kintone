<?php 
	////////////////////////////////////
	//  タイムコンシェル用
	////////////////////////////////////

	include_once("tcalertsend_okamura.php");

	define( "TC_KIGEN" 	 , 1 ); // 期限通知
	define( "TC_STEP"  	 , 2 ); // ステップ通知
	define( "TC_KINEN" 	 , 3 ); // 記念日通知
	define( "TC_TEIKI" 	 , 4 ); // 定期通知
	define( "TC_STEP_S"  , 5 ); // ステップ通知（初回）

	function execMailSet_okamura() {

		///////////////////////////////////////////////////////////
		// 基本接続情報設定
	    $domain = "okamura-bungu.cybozu.com"; 	// ドメイン
	    $user   = "Administrator";				// ID
	    $passwd = "okamura";					// パスワード
		///////////////////////////////////////////////////////////

		//-----------------------------------------------------------------------------------
		// クエリ用の値（見積提出７日後のフォローをしましょう！）
		//-----------------------------------------------------------------------------------
		$today1 = date("Y-m-d",strtotime("-1 day")); // 対象日付(１日後)

		$app  		 = "34"; 					  		  					   // データを取得するアプリID
		$query  	 = "( 名刺交換日入力日 in (\"".$today1."\") )"; 		   // 絞り込みをするクエリ文字列
		$subject   	 = "名刺をいただき、ありがとうございました。";  		   // メールタイトル
		$header   	 = "";											  		   // 本文のヘッダー
		$mailapp     = "";                    		      					   // メールアドレスを取得するアプリID
		$address 	 = "パソコンメールアドレス";	 	 					   // メールアドレス項目名(テーブル不可)
		$body        = "1";                       		 					   // 使用する繰り返し本文
		$addlist     = "m.takeuchi@timeconcier.jp";

		// 取得先アプリID、クエリ式、メールタイトル、メールヘッター、メールアドレスを渡す
		$mail = new TcAlertSet_okamura();
		$mail->setAppID(  $app );
		$mail->setQuery(  $query );
		$mail->setsubject( $subject );
		$mail->setHeader( $header );
		$mail->setMailapp( $mailapp );
		$mail->setAddress( $address );

		// メール作成、送信処理
		$mail->sendmailset($domain,$user,$passwd,TC_STEP,$body,$addlist);
	}

?>