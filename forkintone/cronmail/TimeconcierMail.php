<?php 
/*****************************************************************************/
/*   タイムコンシェル機能(メール送信)                         (Version 1.00) */
/*   ファイル名 : timeconciermail.php                                        */
/*   更新履歴   2016/03/29  Version 1.00(M.T)                                */
/*   [備考]                                                                  */
/*     既存のタイムコンシェル機能を構成していた                              */
/*     tcalertset_ccon.phpとtcalertsend_ccon.phpを統合                       */
/*   [必要ファイル]                                                          */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/

    mb_language("Japanese");

    include_once("../tccom/tcalert.php");
    include_once("../tccom/tcutility.inc");
    include_once("tcdef.inc");
    include_once("tcerror.php");
    include_once("tckintone.php");
    include_once("tckintonerecord.php");

    /*****************************************************************************/
    /* クラス定義                                                                */
    /*****************************************************************************/
    class TimeconcierMail
    {

        /*************************************************************************/
        /* メンバ変数                                                            */
        /*************************************************************************/
        var $err;

        var $domain = "";            // ドメイン名
        var $user = "";
        var $pass = "";

        /*************************************************************************/
        /* コンストラクタ                                                        */
        /*************************************************************************/
        function TimeconcierMail( $pDomain, $pUser, $pPass) {

            // kintone環境の接続情報を設定
            $this->domain = $pDomain;
            $this->user   = $pUser;
            $this->pass   = $pPass;

            $this->err = new TcError();
        }

        /*************************************************************************/
        /* メンバ関数                                                            */
        /*************************************************************************/


        /*************************************************************************/
        /* 文面作成の処理を実行する                                              */
        /*  引数    $addlist  (送信先リスト)                                     */
        /*          $pSubject (メール件名)                                       */
        /*          $pBody    (メール件名)                                       */
        /*************************************************************************/
        function sendMailSet( $pMail ) {

            $addlist  = $pMail[to];
            $pSubject = $pMail[subject];
            $pBody    = $pMail[body];

            // 管理者通知用BCC
            $bcc = "";
            if ( $pMail[bcc] ) { $bcc = $pMail[bcc]; }

            // 送信元
            $fromAdd = "support@timeconcier.jp";
            $fromName = "タイムコンシェル";
            if ( $pMail[fromAdd] && $pMail[fromName] ) {
                $fromAdd = $pMail[fromAdd];
                $fromName = $pMail[fromName];
            }


            $ret      = false;

            if( $pBody != "" ){

                // 送信先アドレスが設定されていない場合、送らない

                if( $addlist != "" ){
                    // ----------------------------------------------------
                    // メールを送信する
                    // ----------------------------------------------------
                    $sendmail = new tcAlert();
                    // お客様向け
                    $sendmail->setFrom ( $fromAdd , $fromName );
                    $sendmail->setTo   ( $addlist );
                    $sendmail->setTitle( $pSubject );
                    $sendmail->setBody ( $pBody );
                    $res = $sendmail->sendMail("","",$bcc);
                }
            }

            return $ret;

        }


        //***********************************************************************************
        // メールオブジェクトの一部をkintoneからの取得値で置き換え
        //  - 引数($pMailは必須、$pGetInfo省略時は""指定)
        //   $pMail   :  Array( [to]      => "宛先アドレス,[...]",
        //                      [subject] => "メール件名",
        //                      [body]    => "メール本文" )
        //   $pGetInfo:  Array( [appId]   => アプリID,
        //                      [query]   => "クエリ")
        //
        //  - $pMail[body]の置き換え文字
        //     "＄＄cnt＄＄"              : クエリ実行結果の取得件数
        //     "＄＄フィールドコード＄＄" : 取得指定フィールドの
        //***********************************************************************************
        function replaceMailString( $pMail, $pGetInfo ) {
// 要改修
            $body = $pMail[body];


            if ( $pGetInfo != "" ) {

                $result = $this->getRecords ( $pGetInfo[appId], $pGetInfo[query] );

                // ----------------------------------------------------------------
                // 取得件数が1件以上あった場合、本文の置き換え文字を取得値で置換
                // ----------------------------------------------------------------
                if ( count($result) > 0 ) {

                    $wrk = explode("＄＄", $pMail[body]);
                    $body = "";
                    foreach ( $wrk as $key => $value ) {
                        if ( ($key % 2) == 1 ) {
                            // 置き換え文字の場合
                            if ( $value == "cnt" ) {
                                // "＄＄cnt＄＄"が指定された場合、取得件数表示
                                $body .= count($result);
                            } else {
                                // "＄＄項目＄＄"のように指定された場合、指定されたフィールドコードの値を表示
                                $body .= $result[0]->$value->value;
                            }
                        } else {
                            // 置き換え文字でない場合
                            $body .= $wrk[$key];
                        }
                    }
                }    // end if ( count($result) > 0 )

            }

            return $body;

        }


        /*************************************************************************/
        // kintoneアプリからレコード情報を取得
        /*************************************************************************/
        function getRecords ( $pAppId, $pQuery ) {

            // ----------------------------------------------
            // 対象データを取得
            // ----------------------------------------------
            $k = new TcKintone( $this->domain, $this->user, $this->pass );                // API連携クラス
            $k->parInit();                  // API連携用のパラメタを初期化する
            $k->intAppID     = $pAppId;     // アプリID

            $recno = 0;                     // kintoneデータ取得件数制限の対応
            $i     = 0;                     // 取得データ格納用カウント数
            $ret   = array();

            do {
                // 検索条件を作成する。
                $aryQ = array();
                $aryQ[] = $pQuery;
                $aryQ[] = "( レコード番号 > $recno )";

                $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

                // http通信を実行する。
                $getDat = $k->runCURLEXEC( TC_MODE_SEL );


                if( $k->intDataCount == 0 ) {
                    break;
                }

                foreach( $getDat->records as $key => $value ) {
                    array_push($ret, $getDat->records[$key]);
                }

                // 次に読み込む条件の設定
                $recno = $getDat->records[ $k->intDataCount - 1 ]->レコード番号->value;

                $i++; // カウントアップ

            } while( $k->intDataCount > 0 );

            return $ret;

        }


        /*************************************************************************/
        // レコード情報を更新
        /*************************************************************************/
        function updateRecords ( $pAppId, $pRecords ) {

            $k = new TcKintone( $this->domain, $this->user, $this->pass );                // API連携クラス
            $k->parInit();                                                                // API連携用のパラメタを初期化する
            $k->intAppID = $pAppId;                                                  // アプリID
            $k->strContentType    = "Content-Type: application/json";

            foreach( $pRecords as $key => $val ) {
                $updData              = new stdClass;
                $updData->app         = $pAppId;
                $updData->records     = $val;
                $k->aryJson = $updData;
                $json = $k->runCURLEXEC( TC_MODE_UPD );
            }

        }



        /*************************************************************************/
        /* メンバ関数                                                            */
        /*************************************************************************/
        function tgfEnc( &$obj , $idx , $fldNm ) {
            $wk = new stdClass;
            $wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
            return ( $wk );
        }

        function valEnc( $val ) {
            $wk = new stdClass;
            $wk->value = mb_convert_encoding($val , "UTF-8", "auto");
            return ( $wk );
        }




    }    // class TimeconcierMail
?>