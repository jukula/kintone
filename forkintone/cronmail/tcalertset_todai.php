<?php 
/*****************************************************************************/
/* 	 連番リセット設定PHP                                      (Version 1.01) */
/*   ファイル名 : tcalertset_todai.php               					     */
/*   更新履歴   2015/04/14  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*   他のTC機能とは違い、このPHPで完結                                       */
/*   tcalertsend_○○○.phpは無し                                            */
/*   [必要ファイル]                                                          */
/*      																     */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/

	include_once("tckintone.php");
	include_once("../tccom/tckintonecommon.php");

	// シナリオ設定
	define( "TC_GOOGLES_SCENARIO_01" , false ); // ステップ通知google差し込み()

	// 東大独自設定
	define( "TC_TODAI_SCENARIO_01" 	 , true ); // 毎月1日に連番のリセット
	///////////////////////////////////////////////////////////
	// 基本接続情報設定
	$domain = "ducrhatsumei.cybozu.com"; // ドメイン
	$user   = "Administrator";		 	 // ID
	$passwd = "Nto7En8x";				 // パスワード
	///////////////////////////////////////////////////////////

	//***********************************************************************************
	// 連番リセット
	//***********************************************************************************
	if( TC_TODAI_SCENARIO_01 ){

		// 条件設定
		$para = "1"; // 絞り込み条件
		$appID = 15;
		// --------------------
		// 連番管理
		// --------------------
		$upd = new TcKintone($domain, $user, $passwd);
		$upd->parInit();								// API連携用のパラメタを初期化する
		$upd->intAppID 		    = $appID;					// アプリID(連番管理)
		$upd->strContentType	= "Content-Type: application/json";

	    $common = new TcKintoneCommon();
		$recObj = new stdClass;
		$recObj->id = $para;  // 更新レコード番号を指定（必須）

		$recObj->record = new stdClass;
		$recObj->record->大学院法学政治学研究科 = $common->valEnc("1");
		$recObj->record->大学院医学系研究科 = $common->valEnc("1");
		$recObj->record->医学部附属病院 = $common->valEnc("1");
		$recObj->record->大学院工学系研究科 = $common->valEnc("1");
		$recObj->record->大学院人文社会系研究科 = $common->valEnc("1");
		$recObj->record->大学院理学系研究科 = $common->valEnc("1");
		$recObj->record->大学院農学生命科学研究科 = $common->valEnc("1");
		$recObj->record->大学院経済学研究科 = $common->valEnc("1");
		$recObj->record->大学院総合文化研究科 = $common->valEnc("1");
		$recObj->record->大学院教育学研究科 = $common->valEnc("1");
		$recObj->record->大学院薬学系研究科 = $common->valEnc("1");
		$recObj->record->大学院数理科学研究科 = $common->valEnc("1");
		$recObj->record->大学院新領域創成科学研究科 = $common->valEnc("1");
		$recObj->record->大学院情報理工学系研究科 = $common->valEnc("1");
		$recObj->record->大学院情報学環 = $common->valEnc("1");
		$recObj->record->医科学研究所 = $common->valEnc("1");
		$recObj->record->地震研究所 = $common->valEnc("1");
		$recObj->record->東洋文化研究所 = $common->valEnc("1");
		$recObj->record->社会科学研究所 = $common->valEnc("1");
		$recObj->record->生産技術研究所 = $common->valEnc("1");
		$recObj->record->史料編纂所 = $common->valEnc("1");
		$recObj->record->定量生命科学研究所 = $common->valEnc("1");
		$recObj->record->宇宙線研究所 = $common->valEnc("1");
		$recObj->record->物性研究所 = $common->valEnc("1");
		$recObj->record->大気海洋研究所 = $common->valEnc("1");
		$recObj->record->総合研究博物館 = $common->valEnc("1");
		$recObj->record->低温センター = $common->valEnc("1");
		$recObj->record->アイソトープ総合センター = $common->valEnc("1");
		$recObj->record->環境安全研究センター = $common->valEnc("1");
		$recObj->record->国際センター_日本語教育センター = $common->valEnc("1");
		$recObj->record->先端科学技術研究センター = $common->valEnc("1");
		$recObj->record->人工物工学研究センター = $common->valEnc("1");
		$recObj->record->生物生産工学研究センター = $common->valEnc("1");
		$recObj->record->アジア生物資源環境研究センター = $common->valEnc("1");
		$recObj->record->大学総合教育研究センター = $common->valEnc("1");
		$recObj->record->空間情報科学研究センター = $common->valEnc("1");
		$recObj->record->保健_健康推進本部 = $common->valEnc("1");
		$recObj->record->情報基盤センター = $common->valEnc("1");
		$recObj->record->素粒子物理国際研究センター = $common->valEnc("1");
		$recObj->record->大規模集積システム設計教育研究センター = $common->valEnc("1");
		$recObj->record->環境安全本部 = $common->valEnc("1");
		$recObj->record->IRT研究機構 = $common->valEnc("1");
		$recObj->record->ナノ量子情報エレクトロニクス研究機構 = $common->valEnc("1");
		$recObj->record->フューチャーセンター推進機構 = $common->valEnc("1");
		$recObj->record->知の構造化センター = $common->valEnc("1");
		$recObj->record->大学院公共政策学連携研究部 = $common->valEnc("1");
		$recObj->record->本部_総括プロジェクト機構_その他 = $common->valEnc("1");

		$updData 			= new stdClass;
		$updData->app 		= $appID;
		$updData->records[] = $recObj;
		$upd->aryJson = $updData;
		$jsonDATA = $upd->runCURLEXEC( TC_MODE_UPD );
	}
?>