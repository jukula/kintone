<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset_shizen.php               		                 */
/*   更新履歴   2014/02/05  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("tcgoogleset.php");

	define( "TC_MAILID_01" , "kumiko.isono" ); 	 	// Kumiko Isono
	define( "TC_MAILID_02" , "makoto.hara" ); 	 	// makoto.hara
	define( "TC_MAILID_03" , "yoshitaka.shimizu" ); // Yoshitaka Shimizu
	define( "TC_MAILID_04" , "masaya.hasegawa" ); 	// Masaya Hasegawa
	define( "TC_MAILID_05" , "masayoshi.tanaka" );  // Masayoshi Tanaka
	define( "TC_MAILID_06" , "seigou.harada" ); 	// seigou harada

	define( "TC_MAIL_01" , "kumiko.isono@shizenenergy.net" ); 	 // Kumiko Isono
	define( "TC_MAIL_02" , "makoto.hara@shizenenergy.net" ); 	 // makoto.hara
	define( "TC_MAIL_03" , "shimizu@nijikumo.net" ); 			 // Yoshitaka Shimizu
	define( "TC_MAIL_04" , "masaya.hasegawa@shizenenergy.net" ); // Masaya Hasegawa
	define( "TC_MAIL_05" , "" ); // Masayoshi Tanaka
	define( "TC_MAIL_06" , "" ); // seigou harada
    ///////////////////////////////////////////////////////////////////////////////
    // 定数定義
    ///////////////////////////////////////////////////////////////////////////////

	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcAlertSet
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数

		var $DomainName = "";			// ドメイン名

		var $AppID 	  	= "";           // アプリID 
		var $Query 	  	= array();      // SQL文
		var $subject  	= "";     	    // メールタイトル
		var $Header   	= "";     	    // ヘッダー
		var $Mailapp  	= "";     	    // メール送信先アプリID
		var $Address  	= "";     	    // メール送信先項目
		var $getAddress = "";     	    // メール送信先項目
		var $Condition1 = "";     	    // クエリ条件
		var $fromAddress= "support@timeconcier.jp";  // メールfrom
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAlertSet() {
	        $this->err = new TcError();
	    }
	    /*************************************************************************/
    	/* メンバ関数                                                            */
    	/*************************************************************************/
		// データ差し込み用のアプリIDを取得
	    function setAppID( $arg ) {
	        $this->AppID = $arg;
	    }
	    function getAppID() {
	        return( $this->AppID );
	    }

		// データ取得用のクエリを取得
	    function setQuery( $arg ) {
	        $this->Query = $arg;
	    }
	    function getQuery() {
	        return( $this->Query );
	    }

		// メールタイトル
	    function setsubject( $arg ) {
	        $this->subject = $arg;
	    }
	    function getsubject() {
	        return( $this->subject );
	    }

		// ヘッダーを取得
	    function setHeader( $arg ) {
	        $this->Header = $arg;
	    }
	    function getHeader() {
	        return( $this->Header );
	    }

		// メールアドレスを取得するアプリID
	    function setMailapp( $arg ) {
	        $this->Mailapp = $arg;
	    }
	    function getMailapp() {
	        return( $this->Mailapp );
	    }

		// メールアドレス項目名(テーブル不可)
	    function setAddress( $arg ) {
	        $this->Address = $arg;
	    }
	    function getAddress() {
	        return( $this->Address );
	    }

		// クエリ条件（テーブル用）
	    function setCondition1( $arg ) {
	        $this->Condition1 = $arg;
	    }
	    function getCondition1() {
	        return( $this->Condition1 );
	    }
		/*************************************************************************/
	    /* 文面作成の処理を実行する                                              */
	    /*  引数	$domain  (ドメイン)                                          */
	    /*  		$user    (ログインID)                                        */
	    /*  		$passwd  (ログインパスワード)                                */
	    /*  		$tuchi   (通知区分)                                          */
	    /*  		$bodyptr (使用する繰り返し本文NO)                            */
	    /*  		$addlist (送信先リスト)                                      */
	    /*************************************************************************/
		function sendmailset($domain,$user,$passwd,$tuchi,$bodyptr,$addlist) {
			$ret 	 = false;
			$setbody = "";

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);		// API連携クラス
			$k->parInit();									// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;					// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			$this->DomainName = $domain;

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > ".$recno." )";

		    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json[$i]->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			if( $this->kensu_mail != 0 ){
				// ----------------------------------------------
				// 差込データ作成
				// 通知時に差し込みの設定を行う
				// ※ステップの場合は送信も行う
				// ----------------------------------------------
				switch ($tuchi) {
				    case TCTYPE_KIGEN:      // 期限通知
						$setbody .= $this->setbody1( $mail_json ,$bodyptr ,$domain ,$user ,$passwd );
				        break;
				    case TCTYPE_STEP:       // ステップ通知
						$setbody .= $this->setbody2( $mail_json ,$bodyptr ,$domain ,$user ,$passwd );
				        break;
				    case TCTYPE_KINENBI:    // 記念日通知
						$setbody .= $this->setbody3( $mail_json ,$bodyptr);
				        break;
				    case TCTYPE_TEIKI:      // 定期通知
						$setbody .= $this->setbody4( $mail_json ,$bodyptr);
				        break;
				    case TCTYPE_STEP_1ST:   // ステップ通知（初回）
						$setbody .= $this->setbody5( $mail_json ,$bodyptr);
				        break;
				    default:
				}
			}else{
				// 定期通知のみ対象が無くても通知
				if( $tuchi == TCTYPE_TEIKI ){
					$setbody .= "通知対象がありません";
				}
			}

// 今は使わない
/*
			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->Mailapp;			// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
				if( $this->Address != "" ){
			    	$k->strQuery = implode( $aryQ , " and ")." order by ".$this->Address." asc";
				}else{
			    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";
				}
				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				// 更新対象の分析管理データの取得件数をチェックする。
				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";
			$getAddress = $this->Address;

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->$getAddress->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->$getAddress->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->$getAddress->value;
						}
					}
				}
			}
*/
/*
			if( $setbody != "" ){
				// 送信先アドレスが設定されていない場合、送らない

				if( $this->Address == "" ){
					$bcc = "c.komatsu@timeconcier.jp"; // 管理者通知用BCC
					// -------------
					// メールを送信する
					// -------------
					$subject    = $this->subject;    // タイトルの設定
					// 本文の設定
					$mailbody 	= $this->Header;
					$mailbody  .= $setbody; //繰り返し項目

					$sendmail = new tcAlert();
					// お客様向け
					$email = $addlist;
					$sendmail->setFrom( $this->fromAddress , "タイムコンシェル" );
					$sendmail->setTo( $email );
					$sendmail->setTitle( $subject );
					$sendmail->setBody( $mailbody );
					$res = $sendmail->sendMail("","",$bcc);
				}
			}
*/
			return $ret;

		}

		/*************************************************************************/
	    /*  googleカレンダーの処理を実行する(googleカレンダー)                   */
	    /*  引数	$domain    (ドメイン)                                        */
	    /*  	    $user       (ログインID)                                   	 */
	    /*  	    $passwd    (ログインパスワード)                              */
	    /*  	    $mainid     (googleアカウント)                               */
	    /*  	    $mainpass   (googleパスワード)                               */
	    /*  	    $step1      (差し込み日付)                                 	 */
	    /*  	    $step2      (ToDo)                                         	 */
	    /*  	    $step3      (電話)                                         	 */
	    /*************************************************************************/
		function sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$step1,$step2,$step3) {
			$ret 	 = false;
			$setbody = "";

			$this->DomainName = $domain;

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{
					$this->setbodygoogle( $mail_json, $mainid, $mainpass, $step1, $step2, $step3);
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			return $ret;


		}

	    /*************************************************************************/
	    /* 差込用データ作成(google通知)	                                         */
	    /*************************************************************************/
		function setbodygoogle($mail_json,$mainid,$mainpass,$step1,$step2,$step3) {

			$a = 0;
			$setlist  = "";
			$age      = "";
			$ageDay   = "";

			// googleカレンダー挿入
			$google = new TcgoogleSet();

			for($i = 0; $i < count($mail_json->records); $i++) {
				$Date="";
				$notifydate = $mail_json->records[$i]->生年月日->value;

				$search = array('株式会社','（株）','(株)','㈱','有限会社','（有）','(有)','㈲');

				$str = str_replace($search,'',$mail_json->records[$i]->顧客名->value);
				$str = 	mb_substr($str,0,6,'UTF-8');

				for($a = 0; $a < count($step1); $a++) {
					$Date		= explode("-", $notifydate);
					$notifydate = $step1[$a];            	  // 差し込み日付
					$title 		= $str.$step2[$a].$step3[$a]; // 差し込みタイトル
					$desc		= "https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$i]->レコード番号->value;

					$res 	= $google->setgoogle($mainid, $mainpass, $notifydate, $title, $desc);
				}
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成（期限通知）                                          */
	    /*************************************************************************/
		function setbody1($mail_json , $bodyptr ,$domain ,$user ,$passwd) {
			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case TCTYPE_PTN1:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist   = "";
							$setlist   = "https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";
							$setstrday = "";
							$setendday = "";
							$daydiff   = "";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							switch ($mail_json[$i]->records[$a]->ドロップダウン_3->value){
								case "2週間";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 week"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 week"))); // 終了日(予定)
									}
									break;
								case "1ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 month"))); // 終了日(予定)
									}
									break;
								case "6ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 month"))); // 終了日(予定)
									}
									break;
								case "1年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
									}
									break;
								case "2年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 year"))); // 終了日(予定)
									}
									break;
								case "3年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+3 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+3 year"))); // 終了日(予定)
									}
									break;
								case "4年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+4 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+4 year"))); // 終了日(予定)
									}
									break;
								case "5年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+5 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+5 year"))); // 終了日(予定)
									}
									break;
								case "6年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 year"))); // 終了日(予定)
									}
									break;
								case "7年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+7 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+7 year"))); // 終了日(予定)
									}
									break;
								case "8年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+8 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+8 year"))); // 終了日(予定)
									}
									break;
								case "9年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+9 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+9 year"))); // 終了日(予定)
									}
									break;
								case "10年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+10 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+10 year"))); // 終了日(予定)
									}
									break;
								case "13年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+13 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+13 year"))); // 終了日(予定)
									}
									break;
								case "15年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+15 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+15 year"))); // 終了日(予定)
									}
									break;
								case "20年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+20 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+20 year"))); // 終了日(予定)
									}
									break;
								default:
							}

							$recObj->record->日付_1   			= $this->valEnc(""); // 開始日（実績）
							$recObj->record->日付_0   			= $this->valEnc(""); // 終了日（実績）

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);
						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}

					break;
				case TCTYPE_PTN2:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							if( $mail_json[$i]->records[$a]->日付->value != "" ){
								$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
								$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
								$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

								$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
								$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
							}

							if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
								$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN3:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							if( $mail_json[$i]->records[$a]->日付->value != "" ){
								$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
								$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
								$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

								$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
								$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
							}

							if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
								$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN4:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							switch ($mail_json[$i]->records[$a]->ドロップダウン_3->value){
								case "2週間";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 week"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 week"))); // 終了日(予定)
									}
									break;
								case "1ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 month"))); // 終了日(予定)
									}
									break;
								case "6ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 month"))); // 終了日(予定)
									}
									break;
								case "1年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
									}
									break;
								case "2年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 year"))); // 終了日(予定)
									}
									break;
								case "3年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+3 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+3 year"))); // 終了日(予定)
									}
									break;
								case "4年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+4 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+4 year"))); // 終了日(予定)
									}
									break;
								case "5年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+5 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+5 year"))); // 終了日(予定)
									}
									break;
								case "6年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 year"))); // 終了日(予定)
									}
									break;
								case "7年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+7 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+7 year"))); // 終了日(予定)
									}
									break;
								case "8年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+8 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+8 year"))); // 終了日(予定)
									}
									break;
								case "9年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+9 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+9 year"))); // 終了日(予定)
									}
									break;
								case "10年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+10 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+10 year"))); // 終了日(予定)
									}
									break;
								case "13年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+13 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+13 year"))); // 終了日(予定)
									}
									break;
								case "15年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+15 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+15 year"))); // 終了日(予定)
									}
									break;
								case "20年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+20 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+20 year"))); // 終了日(予定)
									}
									break;
								default:
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN5:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							switch ($mail_json[$i]->records[$a]->ドロップダウン_3->value){
								case "2週間";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 week"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 week"))); // 終了日(予定)
									}
									break;
								case "1ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 month"))); // 終了日(予定)
									}
									break;
								case "6ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 month"))); // 終了日(予定)
									}
									break;
								case "1年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
									}
									break;
								case "2年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 year"))); // 終了日(予定)
									}
									break;
								case "3年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+3 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+3 year"))); // 終了日(予定)
									}
									break;
								case "4年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+4 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+4 year"))); // 終了日(予定)
									}
									break;
								case "5年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+5 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+5 year"))); // 終了日(予定)
									}
									break;
								case "6年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 year"))); // 終了日(予定)
									}
									break;
								case "7年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+7 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+7 year"))); // 終了日(予定)
									}
									break;
								case "8年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+8 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+8 year"))); // 終了日(予定)
									}
									break;
								case "9年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+9 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+9 year"))); // 終了日(予定)
									}
									break;
								case "10年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+10 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+10 year"))); // 終了日(予定)
									}
									break;
								case "13年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+13 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+13 year"))); // 終了日(予定)
									}
									break;
								case "15年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+15 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+15 year"))); // 終了日(予定)
									}
									break;
								case "20年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+20 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+20 year"))); // 終了日(予定)
									}
									break;
								default:
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN6:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							switch ($mail_json[$i]->records[$a]->ドロップダウン_3->value){
								case "2週間";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 week"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 week"))); // 終了日(予定)
									}
									break;
								case "1ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 month"))); // 終了日(予定)
									}
									break;
								case "6ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 month"))); // 終了日(予定)
									}
									break;
								case "1年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
									}
									break;
								case "2年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 year"))); // 終了日(予定)
									}
									break;
								case "3年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+3 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+3 year"))); // 終了日(予定)
									}
									break;
								case "4年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+4 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+4 year"))); // 終了日(予定)
									}
									break;
								case "5年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+5 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+5 year"))); // 終了日(予定)
									}
									break;
								case "6年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 year"))); // 終了日(予定)
									}
									break;
								case "7年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+7 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+7 year"))); // 終了日(予定)
									}
									break;
								case "8年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+8 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+8 year"))); // 終了日(予定)
									}
									break;
								case "9年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+9 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+9 year"))); // 終了日(予定)
									}
									break;
								case "10年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+10 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+10 year"))); // 終了日(予定)
									}
									break;
								case "13年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+13 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+13 year"))); // 終了日(予定)
									}
									break;
								case "15年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+15 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+15 year"))); // 終了日(予定)
									}
									break;
								case "20年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+20 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-3 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+20 year"))); // 終了日(予定)
									}
									break;
								default:
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN7:
					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");

							switch ($mail_json[$i]->records[$a]->ドロップダウン_3->value){
								case "2週間";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 week"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 week"))); // 終了日(予定)
									}
									break;
								case "1ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 month"))); // 終了日(予定)
									}
									break;
								case "6ヶ月";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 month"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 month"))); // 終了日(予定)
									}
									break;
								case "1年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+1 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+1 year"))); // 終了日(予定)
									}
									break;
								case "2年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+2 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+2 year"))); // 終了日(予定)
									}
									break;
								case "3年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+3 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+3 year"))); // 終了日(予定)
									}
									break;
								case "4年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+4 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+4 year"))); // 終了日(予定)
									}
									break;
								case "5年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+5 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+5 year"))); // 終了日(予定)
									}
									break;
								case "6年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+6 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+6 year"))); // 終了日(予定)
									}
									break;
								case "7年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+7 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+7 year"))); // 終了日(予定)
									}
									break;
								case "8年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+8 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+8 year"))); // 終了日(予定)
									}
									break;
								case "9年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+9 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+9 year"))); // 終了日(予定)
									}
									break;
								case "10年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+10 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+10 year"))); // 終了日(予定)
									}
									break;
								case "13年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+13 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+13 year"))); // 終了日(予定)
									}
									break;
								case "15年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+15 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+15 year"))); // 終了日(予定)
									}
									break;
								case "20年";
									if( $mail_json[$i]->records[$a]->日付->value != "" ){
										$setkaisi  = date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付->value."+20 year"));
										$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
										$setnihizi = date("c", strtotime($setkaisi.$setzi."-1 month")); // 通知日時の計算

										$recObj->record->日付		  		= $this->valEnc($setkaisi); // 開始日(予定)
										$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
									}

									if( $mail_json[$i]->records[$a]->日付_2->value != "" ){
										$recObj->record->日付_2	 	  		= $this->valEnc(date("Y-m-d", strtotime($mail_json[$i]->records[$a]->日付_2->value."+20 year"))); // 終了日(予定)
									}
									break;
								default:
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN8:
					$year  = date("Y");
					$month = date("m");
					$strday == "";

					// 次回の月を取得
					if($month == "5" || $month == "6" || $month == "7"){
						$month = "8";
						$maxday = date("t", strtotime($year."-08-01"));
					}else if($month == "8" || $month == "9" || $month == "10" || $month == "11" || $month == "12" || $month == "1" || $month == "2" || $month == "3"|| $month == "4"){
						$month = "5";
						$year  = $year + 1;
						$maxday = date("t", strtotime($year."-05-01"));
					}

					if( $month == "5" ){
						$frkeflg = false;
						for($i = 0; $i < $maxday; $i++) {
							$day   = $i + 1;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');
							// 土日以外の日付で第一営業日を探す
							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								switch ($day){
									case "3"; // 憲法記念日
										break;
									case "4"; // 国民の休日、みどりの日
										break;
									case "5"; // こどもの日
										break;
									default:
										if( $frkeflg ){
											// 振替休日がある場合、第一営業日をずらす
											$frkeflg = false;
										}else{
											if( $strday == "" ){
												// 第一営業日が入っていない場合、入れる
												$strday = $year."-".$month."-".$day;
											}
										}
								}
							}else if(($week[$w] == "日")){
								switch ($day){
									case "3"; // 憲法記念日
										$frkeflg = true;
										break;
									case "4"; // 国民の休日、みどりの日
										break;
										$frkeflg = true;
									case "5"; // こどもの日
										$frkeflg = true;
										break;
									default:
								}
							}
						}
					}else if( $month == "8" ){
						for($i = 0; $i < $maxday; $i++) {
							$day   = $i + 1;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');
							// 土日以外の日付で第一営業日を探す
							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								if( $strday == "" ){
									// 第一営業日が入っていない場合、入れる
									$strday = $year."-".$month."-".$day;
								}
							}
						}
					}

					$endday = "";

					for($i = 0; $i < $maxday; $i++) {
						$day   = $maxday - $i;
						$datetime = new DateTime();
						$datetime->setDate($year, $month, $day);
						$week = array("日", "月", "火", "水", "木", "金", "土");
						$w = (int)$datetime->format('w');
						// 土日以外の日付で第一営業日を探す
						if( ($week[$w] != "日") && ($week[$w] != "土") ){
							if( $endday == "" ){
								// 第一営業日が入っていない場合、入れる
								$endday = $year."-".$month."-".$day;
							}
						}
					}

					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");
							$recObj->record->日付		  		= $this->valEnc($strday); // 開始日（予定）
							$recObj->record->日付_2	 	  		= $this->valEnc($endday); // 終了日（予定）

							if($mail_json[$i]->records[$a]->日時_3->value != ""){
								$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
								$setnihizi = date("c", strtotime($strday.$setzi."-1 month")); // 通知日時の計算
								$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
				case TCTYPE_PTN9:
					$year  = date("Y");
					$month = date("m");
					$strday == "";

					// 次回の月を取得
					if($month == "4" || $month == "5" || $month == "6" || $month == "7" || $month == "8" || $month == "9"){
						$month = "10";
						$maxday = date("t", strtotime($year."-10-01"));
					}else if($month == "10" || $month == "11" || $month == "12" || $month == "1" || $month == "2" || $month == "3"){
						$month = "4";
						$year  = $year + 1;
						$maxday = date("t", strtotime($year."-04-01"));
					}

					for($i = 0; $i < $maxday; $i++) {
						$day   = $i + 1;
						$datetime = new DateTime();
						$datetime->setDate($year, $month, $day);
						$week = array("日", "月", "火", "水", "木", "金", "土");
						$w = (int)$datetime->format('w');
						// 土日以外の日付で第一営業日を探す
						if( ($week[$w] != "日") && ($week[$w] != "土") ){
							if( $strday == "" ){
								// 第一営業日が入っていない場合、入れる
								$strday = $year."-".$month."-".$day;
							}
						}
					}

					$endday = "";
					if( $month == "4" ){
						for($i = 0; $i < $maxday; $i++) {
							$day   = $maxday - $i;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');

							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								switch ($day){
									case "29"; // 昭和の日
										break;
									default:
										if( $endday == "" ){
											$endday = $year."-".$month."".$day;
										}
								}
							}else if(($week[$w] == "日")){
								switch ($day){
									case "29"; // 昭和の日
										$endday = "";
										break;
									default:
								}
							}
						}
					}else{
						for($i = 0; $i < $maxday; $i++) {
							$day   = $maxday - $i;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');
							// 土日以外の日付で第一営業日を探す
							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								if( $endday == "" ){
									// 第一営業日が入っていない場合、入れる
									$endday = $year."-".$month."-".$day;
								}
							}
						}
					}

					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");
							$recObj->record->日付		  		= $this->valEnc($strday); // 開始日（予定）
							$recObj->record->日付_2	 	  		= $this->valEnc($endday); // 終了日（予定）

							if($mail_json[$i]->records[$a]->日時_3->value != ""){
								$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
								$setnihizi = date("c", strtotime($strday.$setzi."-1 month")); // 通知日時の計算
								$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
			  	    break;
				case TCTYPE_PTN10:
					$year  = date("Y");
					$month = date("m");
					$strday == "";

					// 次回の月を取得
					if($month == "5" || $month == "6" || $month == "7" || $month == "8"){
						$month = "9";
						$maxday = date("t", strtotime($year."-09-01"));
					}else if($month == "9" || $month == "10" || $month == "11" || $month == "12" || $month == "1" || $month == "2" || $month == "3"|| $month == "4"){
						$month = "5";
						$year  = $year + 1;
						$maxday = date("t", strtotime($year."-05-01"));
					}

					if( $month == "5" ){
						$frkeflg = false;
						for($i = 0; $i < $maxday; $i++) {
							$day   = $i + 1;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');
							// 土日以外の日付で第一営業日を探す
							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								switch ($day){
									case "3"; // 憲法記念日
										break;
									case "4"; // 国民の休日、みどりの日
										break;
									case "5"; // こどもの日
										break;
									default:
										if( $frkeflg ){
											// 振替休日がある場合、第一営業日をずらす
											$frkeflg = false;
										}else{
											if( $strday == "" ){
												// 第一営業日が入っていない場合、入れる
												$strday = $year."-".$month."-".$day;
											}
										}
								}
							}else if(($week[$w] == "日")){
								switch ($day){
									case "3"; // 憲法記念日
										$frkeflg = true;
										break;
									case "4"; // 国民の休日、みどりの日
										break;
										$frkeflg = true;
									case "5"; // こどもの日
										$frkeflg = true;
										break;
									default:
								}
							}
						}
					}else if( $month == "9" ){
						for($i = 0; $i < $maxday; $i++) {
							$day   = $i + 1;
							$datetime = new DateTime();
							$datetime->setDate($year, $month, $day);
							$week = array("日", "月", "火", "水", "木", "金", "土");
							$w = (int)$datetime->format('w');
							// 土日以外の日付で第一営業日を探す
							if( ($week[$w] != "日") && ($week[$w] != "土") ){
								if( $strday == "" ){
									// 第一営業日が入っていない場合、入れる
									$strday = $year."-".$month."-".$day;
								}
							}
						}
					}

					$endday = "";

					for($i = 0; $i < $maxday; $i++) {
						$day   = $maxday - $i;
						$datetime = new DateTime();
						$datetime->setDate($year, $month, $day);
						$week = array("日", "月", "火", "水", "木", "金", "土");
						$w = (int)$datetime->format('w');
						// 土日以外の日付で第一営業日を探す
						if( ($week[$w] != "日") && ($week[$w] != "土") ){
							if( $endday == "" ){
								// 第一営業日が入っていない場合、入れる
								$endday = $year."-".$month."-".$day;
							}
						}
					}

					$idxM = 0;
					$idxR = 0;
					$aryUpdAnk = array();
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	"https://shizenenergy.cybozu.com/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n";

							// --------------------------------
							// アップデート情報をインサートする。
							// --------------------------------
							// 書込み準備
							$recObj = new stdClass;
							$recObj->id = $mail_json[$i]->records[$a]->レコード番号->value;

							$recObj->record->ドロップダウン_0	= $this->valEnc("未着手");
							$recObj->record->日付		  		= $this->valEnc($strday); // 開始日（予定）
							$recObj->record->日付_2	 	  		= $this->valEnc($endday); // 終了日（予定）

							if($mail_json[$i]->records[$a]->日時_3->value != ""){
								$setzi     = mb_substr($mail_json[$i]->records[$a]->日時_3->value, 10);
								$setnihizi = date("c", strtotime($strday.$setzi."-1 month")); // 通知日時の計算
								$recObj->record->日時_3		  		= $this->valEnc($setnihizi); // 開始日(予定)
							}

							$aryUpdAnk[ $idxM ][ $idxR ] = $recObj;
							$idxR += 1;
							if( $idxR == CY_UPD_MAX ) {
								$idxM += 1;
								$idxR = 0;
							}
							$recObj = null;

							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";

							$subject    = $this->subject;         // タイトルの設定
							$email 	    = ""; 					  // メールアドレス
							$bcc 		= ""; 					  // 管理者通知用BCC

							for($j = 0; $j <= count($mail_json[$i]->records[$a]->ユーザー選択->value); $j++) {
								switch ($mail_json[$i]->records[$a]->ユーザー選択->value[$j]->code){
									case TC_MAILID_01;
										if( $email == "" ){
											$email = TC_MAIL_01;
										}else{
											$email = $email .",". TC_MAIL_01;
										}
										break;
									case TC_MAILID_02;
										if( $email == "" ){
											$email = TC_MAIL_02;
										}else{
											$email = $email .",". TC_MAIL_02;
										}
										break;
									case TC_MAILID_03;
										if( $email == "" ){
											$email = TC_MAIL_03;
										}else{
											$email = $email .",". TC_MAIL_03;
										}
										break;
									case TC_MAILID_04;
										if( $email == "" ){
											$email = TC_MAIL_04;
										}else{
											$email = $email .",". TC_MAIL_04;
										}
										break;
									case TC_MAILID_05;

										break;
									case TC_MAILID_06;

										break;
									default:
								}

							}

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);

						}
					}

					// --------------------
					// 案件管理を更新する。
					// --------------------
					$k = new TcKintone($domain,$user,$passwd);
					$k->parInit();								// API連携用のパラメタを初期化する
					$k->intAppID 		= $this->AppID;			// アプリID
					$k->strContentType	= "Content-Type: application/json";
					foreach( $aryUpdAnk as $key => $val ) {
						$updData 			= new stdClass;
						$updData->app 		= $this->AppID;
						$updData->records 	= $val;
						$k->aryJson = $updData;						// 更新対象のレコード番号
						$json = $k->runCURLEXEC( TC_MODE_UPD );

						if( $k->strHttpCode == 200 ) {
						} else {
						}
					}
				    break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知）	                                 */
	    /*************************************************************************/
		function setbody2($mail_json , $bodyptr ,$domain ,$user ,$passwd) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$komadd   = $this->Address; // メールアドレス項目
			switch ($bodyptr) {
				case TCTYPE_PTN1:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."\n";
						}
					}
					break;
				case TCTYPE_PTN2:
				    break;
			    case TCTYPE_PTN3:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {

							// ------------------------------------------------------
							// 同顧客で通知日以上の日付がないか検索（あれば通知不要）
							// ------------------------------------------------------
							$db = new TcKintone($domain,$user,$passwd);	// API連携クラス
							$db->parInit();								// API連携用のパラメタを初期化する
							$db->intAppID 	= $this->AppID;				// アプリID

						    $db->strQuery = "(( 案件レコード番号 = ".$mail_json[$i]->records[$a]->案件レコード番号->value.") and ( アクション実施日 > \"".$mail_json[$i]->records[$a]->アクション実施日->value."\"))" ; // クエリパラメータ
							$nipo_json = $db->runCURLEXEC( TC_MODE_SEL );

							if( $db->intDataCount == 0 ) {
								// 対象データ文章の組み立て
								$setlist.=	"顧客名　　　　　　：".$mail_json[$i]->records[$a]->顧客名->value."\n";
								$setlist.=	"最終アクション日　：".$mail_json[$i]->records[$a]->アクション実施日->value."\n";
								$setlist.=	"レコード　　　　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n\n";
								$email 	 =  $mail_json[$i]->records[$a]->$komadd->value; // メールアドレス

								// ヘッダーと繋ぐ
								$mailbody 	= $this->Header;	 // 本文の設定
								$mailbody  .= $setlist; 		 //繰り返し項目

								// メール送信処理
								$bcc = ""; // 管理者通知用BCC
								$sendmail = new tcAlert();
								$sendmail->setFrom( $this->fromAddress , "タイムコンシェル" );
								$sendmail->setTo( $email );
								$sendmail->setTitle( $this->subject );
								$sendmail->setBody( $mailbody );
								$res = $sendmail->sendMail("","",$bcc);
							}
						}
					}
			        break;
			    case TCTYPE_PTN4:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";
							$setlist  = "";

							$subject    = $this->subject;    // タイトルの設定
							// 対象データ文章の組み立て
							$setlist.=	"お客様名　　：".$mail_json[$i]->records[$a]->顧客名->value."\n";
							$setlist.=	"案件名　　　：".$mail_json[$i]->records[$a]->案件名->value."\n";
							$setlist.=	"納品日　　　：".$mail_json[$i]->records[$a]->納品日->value."\n";
							$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n\n";
							$email 	 =  $mail_json[$i]->records[$a]->$komadd->value; // メールアドレス

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$bcc = ""; // 管理者通知用BCC
							$sendmail = new tcAlert();
							$sendmail->setFrom( $this->fromAddress , "タイムコンシェル" );
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);
						}
					}
			        break;
			    case TCTYPE_PTN5:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";
							$setlist  = "";

							$subject    = $this->subject;    // タイトルの設定
							// 対象データ文章の組み立て
							$setlist.=	"お客様名　　：".$mail_json[$i]->records[$a]->顧客名->value."\n";
							$setlist.=	"案件名　　　：".$mail_json[$i]->records[$a]->案件名->value."\n";
							$setlist.=	"見積提出日　：".$mail_json[$i]->records[$a]->見積提出日->value."\n";
							$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n\n";
							$email 	 =  $mail_json[$i]->records[$a]->$komadd->value; // メールアドレス

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$bcc = ""; // 管理者通知用BCC
							$sendmail = new tcAlert();
							$sendmail->setFrom( $this->fromAddress , "タイムコンシェル" );
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);
						}
					}
			        break;
			    case TCTYPE_PTN6:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							// 初期化
							$subject  = "";
							$mailbody = "";
							$email    = "";
							$setlist  = "";

							$subject    = $this->subject;    // タイトルの設定
							// 対象データ文章の組み立て
							$setlist.=	"お客様名　　：".$mail_json[$i]->records[$a]->顧客名->value."\n";
							$setlist.=	"案件名　　　：".$mail_json[$i]->records[$a]->案件名->value."\n";
							$setlist.=	"受注日　　　：".$mail_json[$i]->records[$a]->受注日->value."\n";
							$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json[$i]->records[$a]->レコード番号->value."\n\n";
							$email 	 =  $mail_json[$i]->records[$a]->$komadd->value; // メールアドレス

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$bcc = ""; // 管理者通知用BCC
							$sendmail = new tcAlert();
							$sendmail->setFrom( $this->fromAddress , "タイムコンシェル" );
							$sendmail->setTo( $email );
							$sendmail->setTitle( $subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail("","",$bcc);
						}
					}
			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（記念日通知）	                                     */
	    /*************************************************************************/
		function setbody3($mail_json , $bodyptr) {

			$i = 0;
			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$age      = ""; // 年齢
			$ageDay   = ""; // 誕生日
			$tbcount  = 0;  // テーブルレコード数
			$nowyear  = date('Y');
			$setyear  = "";

			switch ($bodyptr) {
				case TCTYPE_PTN1:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$tbcount = count($mail_json[$i]->records[$a]->記念日テーブル->value);
						    for($j = 0; $j <= ($tbcount); $j++) {
								if( ( $mail_json[$i]->records[$a]->記念日テーブル->value[$j]->value->記念日_月_->value == $this->Condition1 ) && ($mail_json[$i]->records[$a]->記念日テーブル->value[$j]->value->記念日_日_->value != "" )){
									$setlist.=	$mail_json[$i]->records[$a]->社員名->value."さん ".$mail_json[$i]->records[$a]->記念日テーブル->value[$j]->value->記念日名->value." ".$mail_json[$i]->records[$a]->記念日テーブル->value[$j]->value->記念日_月_->value."月".$mail_json[$i]->records[$a]->記念日テーブル->value[$j]->value->記念日_日_->value."日\n";
								}
							}
						}
					}
					break;
				case TCTYPE_PTN2:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$tbcount = count($mail_json[$i]->records[$a]->家族テーブル->value);
						    for($j = 0; $j <= ($tbcount); $j++) {
								if( ( $mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_月_->value == $this->Condition1 ) && ($mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_日_->value != "" )){

									if( $mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_年_->value != ""){
										$setyear = (intval($nowyear) - intval($mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_年_->value));
									}else{
										$setyear = "";
									}
									$setlist.=	$mail_json[$i]->records[$a]->社員名->value."さんの ".$mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->続柄->value." ".$mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->家族名->value." ".$mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_月_->value."月".$mail_json[$i]->records[$a]->家族テーブル->value[$j]->value->誕生日_日_->value."日(".$setyear.")\n";
								}
							}
						}
					}
				    break;
			    case TCTYPE_PTN3:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							if( $mail_json[$i]->records[$a]->年->value != ""){
								$setyear = (intval($nowyear) - intval($mail_json[$i]->records[$a]->年->value));
							}else{
								$setyear = "";
							}
							$setlist.=	$mail_json[$i]->records[$a]->社員名->value."さん ".$mail_json[$i]->records[$a]->事業所->value." ".$mail_json[$i]->records[$a]->所属->value." ".$mail_json[$i]->records[$a]->役職->value." ".$mail_json[$i]->records[$a]->職種->value." ".$mail_json[$i]->records[$a]->月->value."月".$mail_json[$i]->records[$a]->日->value."日(".$setyear.")\n";
						}
					}
			        break;
			    case TCTYPE_PTN4:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							if( $mail_json[$i]->records[$a]->年->value != ""){
								$setyear = (intval($nowyear) - intval($mail_json[$i]->records[$a]->年->value));
							}else{
								$setyear = "";
							}
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."さん ".$mail_json[$i]->records[$a]->月->value."月".$mail_json[$i]->records[$a]->日->value."日(".$setyear.")\n";
						}
					}
			        break;
			    case TCTYPE_PTN5:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							if( $mail_json[$i]->records[$a]->年_創業日_->value != ""){
								$setyear = (intval($nowyear) - intval($mail_json[$i]->records[$a]->年_創業日_->value));
							}else{
								$setyear = "";
							}
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->代表者名->value."社長 ".$mail_json[$i]->records[$a]->年_創業日_->value."年".$mail_json[$i]->records[$a]->月_創業日_->value."月".$mail_json[$i]->records[$a]->日_創業日_->value."日(".$setyear.")\n";
						}
					}
			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（定期通知）		                                 */
	    /*************************************************************************/
		function setbody4($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case TCTYPE_PTN1:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."\n";
						}
					}
					break;
				case TCTYPE_PTN2:

					$setbody = "";
					$age     = "";
					$ageDay  = "";
					$arrsort = "";
					$n = 0;
					$seturl  = "https://timeconcier.cybozu.com/k/2226/show#record=";

					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							if( ($mail_json[$i]->records[$a]->誕生日_年_->value != "") && ($mail_json[$i]->records[$a]->月->value != "") && ($mail_json[$i]->records[$a]->日->value != "")){
								$age = ($this->birthToAge($mail_json[$i]->records[$a]->誕生日_年_->value."-".$mail_json[$i]->records[$a]->月->value."-".$mail_json[$i]->records[$a]->日->value ));
								$age = $age . "歳";
							}else{
								$age = "";
							}

							if( $mail_json[$i]->records[$a]->日->value != "" ){
								if(mb_strlen($mail_json[$i]->records[$a]->日->value) == 1 ){
									$ageDay = $mail_json[$i]->records[$a]->月->value."月 ".$mail_json[$i]->records[$a]->日->value . "日　";
								}else{
									$ageDay = $mail_json[$i]->records[$a]->月->value."月".$mail_json[$i]->records[$a]->日->value . "日　";
								}
							}else{
								$ageDay = "";
							}

							$arrsort[$n][id] = $mail_json[$i]->records[$a]->日->value;
							$arrsort[$n][val] = $ageDay.$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."さん "." ".$age."\n　　　　　".$seturl.$mail_json[$i]->records[$a]->レコード番号->value."\n";
							$n++;
						}
					}

					foreach($arrsort as $key=>$value){
					    $id[$key] = $value['id'];
					}

					array_multisort($id ,SORT_ASC,$arrsort);

					for($a = 0; $a < count($arrsort); $a++) {
						$setlist.= $arrsort[$a][val];
					}

				    break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知(初回)）	                             */
	    /*************************************************************************/
		function setbody5($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case TCTYPE_PTN1:
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."\n";
						}
					}
					break;
				case TCTYPE_PTN2:
				    break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
