<?php
/*****************************************************************************/
/* 	 メール送信PHP                                            (Version 1.01) */
/*   ファイル名 : tcalertset.php               				                 */
/*   更新履歴   2014/02/05  Version 1.00(T.M)                                */
/*   [備考]                                                                  */
/*      tcutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      tcdef.inc / tcutility.inc / tckintone.php / tckintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
//	header("Content-Type:text/html;charset=utf-8");

	mb_language("Japanese");

	include_once("../tccom/tcalert.php");
	include_once("../tccom/tcutility.inc");
	include_once("tcdef.inc");
	include_once("tcerror.php");
	include_once("tckintone.php");
	include_once("tckintonerecord.php");
	include_once("tcgoogleset.php");

	/*****************************************************************************/
	/* クラス定義                                                                */
	/*****************************************************************************/
	class TcAlertSet
	{

	    /*************************************************************************/
	    /* メンバ変数                                                            */
	    /*************************************************************************/
	    var $err;

		var $kensu_mail	= 0;		// 処理対象件数
		var $kensu_add	= 0;		// 処理対象件数

		var $DomainName = "";			// ドメイン名

		var $AppID 	  	= "";             // アプリID 
		var $Query 	  	= array();        // SQL文
		var $subject  	= "";     	    // メールタイトル
		var $Header   	= "";     	    // ヘッダー
		var $Mailapp  	= "";     	    // メール送信先アプリID
		var $Address  	= "";     	    // メール送信先項目
		var $getAddress = "";     	    // メール送信先項目
		var $Condition1 = "";     	    // クエリ条件
	    /*************************************************************************/
	    /* コンストラクタ                                                        */
	    /*************************************************************************/
	    function TcAlertSet() {
	        $this->err = new TcError();
	    }
	    /*************************************************************************/
    	/* メンバ関数                                                            */
    	/*************************************************************************/
		// データ差し込み用のアプリIDを取得
	    function setAppID( $arg ) {
	        $this->AppID = $arg;
	    }
	    function getAppID() {
	        return( $this->AppID );
	    }

		// データ取得用のクエリを取得
	    function setQuery( $arg ) {
	        $this->Query = $arg;
	    }
	    function getQuery() {
	        return( $this->Query );
	    }

		// メールタイトル
	    function setsubject( $arg ) {
	        $this->subject = $arg;
	    }
	    function getsubject() {
	        return( $this->subject );
	    }

		// ヘッダーを取得
	    function setHeader( $arg ) {
	        $this->Header = $arg;
	    }
	    function getHeader() {
	        return( $this->Header );
	    }

		// メールアドレスを取得するアプリID
	    function setMailapp( $arg ) {
	        $this->Mailapp = $arg;
	    }
	    function getMailapp() {
	        return( $this->Mailapp );
	    }

		// メールアドレス項目名(テーブル不可)
	    function setAddress( $arg ) {
	        $this->Address = $arg;
	    }
	    function getAddress() {
	        return( $this->Address );
	    }

		// クエリ条件（テーブル用）
	    function setCondition1( $arg ) {
	        $this->Condition1 = $arg;
	    }
	    function getCondition1() {
	        return( $this->Condition1 );
	    }
		/*************************************************************************/
	    /* 文面作成の処理を実行する                                              */
	    /*  引数	$domain  (ドメイン)                                          */
	    /*  		$user    (ログインID)                                        */
	    /*  		$passwd  (ログインパスワード)                                */
	    /*  		$tuchi   (通知区分)                                          */
	    /*  		$bodyptr (使用する繰り返し本文NO)                            */
	    /*  		$addlist (送信先リスト)                                      */
	    /*************************************************************************/
		function sendmailset($domain,$user,$passwd,$tuchi,$bodyptr,$addlist) {
			$ret 	 = false;
			$setbody = "";

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; 	// kintoneデータ取得件数制限の対応
			$i     = 0; 	// 取得データ格納用カウント数

			$this->DomainName = $domain;

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";

		    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			if( $this->kensu_mail != 0 ){
				// ----------------------------------------------
				// 差込データ作成
				// 通知事に差し込みの設定を行う
				// ※ステップの場合は送信も行う
				// ----------------------------------------------
				switch ($tuchi) {
					case "1": // 期限通知
						$setbody .= $this->setbody1( $mail_json ,$bodyptr);
						break;
					case "2": // ステップ通知
						$setbody .= $this->setbody2( $mail_json ,$bodyptr ,$domain ,$user ,$passwd );
				        break;
				    case "3": // 記念日通知
						$setbody .= $this->setbody3( $mail_json ,$bodyptr);
				        break;
				    case "4": // 定期通知
						$setbody .= $this->setbody4( $mail_json ,$bodyptr);
				        break;
				    case "5": // ステップ通知（初回）
						$setbody .= $this->setbody5( $mail_json ,$bodyptr);
				        break;
				    default:
				}
			}else{
				// 定期通知のみ対象が無くても通知
				if( $tuchi == "4" ){
					$setbody .= "通知対象がありません";
				}
			}

			// ----------------------------------------------
			// 送信先メールアドレスを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);	// API連携クラス
			$k->parInit();								// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->Mailapp;			// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = "( レコード番号 > $recno )";
				if( $this->Address != "" ){
			    	$k->strQuery = implode( $aryQ , " and ")." order by ".$this->Address." asc";
				}else{
			    	$k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";
				}
				// http通信を実行する。
				$add_json[$i] = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}
				// 件数カウント、次に読み込む条件の設定
				$this->kensu_add	+= $k->intDataCount;
				$recno 			 	 = $add_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			// ----------------------------------------------
			// メールアドレスの取得
			// ----------------------------------------------
			$i = 0;
			$a = 0;
			$email = "";
			$getAddress = $this->Address;

			for($i = 0; $i <= count($add_json); $i++) {
				for($a = 0; $a < count($add_json[$i]->records); $a++) {
					if($add_json[$i]->records[$a]->$getAddress->value != "" ){
						if($email == ""){
							$email = $add_json[$i]->records[$a]->$getAddress->value;
						}else{
							$email .= ",".$add_json[$i]->records[$a]->$getAddress->value;
						}
					}
				}
			}

			if( $setbody != "" ){
				// 送信先アドレスが設定されていない場合、送らない
				if( $addlist != "" ){
					$bcc = "m.fukushima@timeconcier.jp,m.takeuchi@timeconcier.jp,c.komatsu@timeconcier.jp";
					// -------------
					// メールを送信する
					// -------------
					$fmail    	= "support@timeconcier.jp"; // メールfrom
					$subject    = $this->subject;    // タイトルの設定
					// 本文の設定
					$mailbody 	= $this->Header."\n\n";
					$mailbody  .= $setbody; //繰り返し項目

					$sendmail = new tcAlert();
					// お客様向け

					// すでに入っている場合は繋ぐ
					if( $email == "" ){
						$email = $addlist;
					}else{
						$email .= ",".$addlist;
					}

					$sendmail->setFrom( $fmail , "タイムコンシェル" );
					$sendmail->setTo( $email );
					$sendmail->setTitle( $subject );
					$sendmail->setBody( $mailbody );
					$res = $sendmail->sendMail("","",$bcc);
				}
			}

			return $ret;

		}

		/*************************************************************************/
	    /*  googleカレンダーの処理を実行する(googleカレンダー)                   */
	    /*  引数	$domain    (ドメイン)                                        */
	    /*  	    $user       (ログインID)                                   	 */
	    /*  	    $passwd    (ログインパスワード)                              */
	    /*  	    $mainid     (googleアカウント)                               */
	    /*  	    $mainpass   (googleパスワード)                               */
	    /*  	    $step1      (差し込み日付)                                 	 */
	    /*  	    $step2      (ToDo)                                         	 */
	    /*  	    $step3      (電話)                                         	 */
	    /*************************************************************************/
		function sendgoogleset($domain,$user,$passwd,$mainid,$mainpass,$stepkom,$step2,$step3,$stepchk,$stepno) {
			$ret 	 = false;
			$setbody = "";

			$this->DomainName = $domain;

			// ----------------------------------------------
			// 対象データを取得
			// ----------------------------------------------
			$k = new TcKintone($domain,$user,$passwd);				// API連携クラス
			$k->parInit();						// API連携用のパラメタを初期化する
			$k->intAppID 	= $this->AppID;		// アプリID

			$recno = 0; // kintoneデータ取得件数制限の対応
			$i     = 0; //取得データ格納用カウント数

			do {
				// 検索条件を作成する。
				$aryQ = array();
				$aryQ[] = $this->Query;
				$aryQ[] = "( レコード番号 > $recno )";
			    $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc";

				// http通信を実行する。
				$mail_json = $k->runCURLEXEC( TC_MODE_SEL );

				if( $k->intDataCount == 0 ) {
					break;
				}else{
					$this->setbodygoogle( $mail_json, $mainid, $mainpass, $stepkom, $step2, $step3 ,$stepchk ,$domain ,$user ,$passwd,$stepno);
				}

				// 件数カウント、次に読み込む条件の設定
				$this->kensu_mail	+= $k->intDataCount;
				$recno 			 	 = $mail_json->records[ $k->intDataCount - 1 ]->レコード番号->value;

				$i++; // カウントアップ

			} while( $k->intDataCount > 0 );

			return $ret;


		}

	    /*************************************************************************/
	    /* 差込用データ作成(google通知)	                                         */
	    /*************************************************************************/
		function setbodygoogle($mail_json,$mainid,$mainpass,$stepkom,$step2,$step3,$stepchk,$domain,$user,$passwd,$stepno) {

			$a = 0;
			$setlist  = "";
			$age      = "";
			$ageDay   = "";

			// googleカレンダー挿入
			$google = new TcgoogleSet();

			for($i = 0; $i < count($mail_json->records); $i++) {
				$Date="";
				$notifydate = $mail_json->records[$i]->生年月日->value;

				//ステップの回数分差し込み日付を作成


				switch ($stepno) {
					case "1": // 契約後フォロー
						$step1 = array();
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +3 day"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +7 day"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +21 day"));
						break;
					case "2": // 定期点検フォロー
						$step1 = array();
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +6 month"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +1 year"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +3 year"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +5 year"));
						$step1[] = date("Y-m-d",strtotime($mail_json->records[$i]->$stepkom->value." +7 year"));
				        break;
				    default:
				}


				$search = array('株式会社','（株）','(株)','㈱','有限会社','（有）','(有)','㈲');

				$str = str_replace($search,'',$mail_json->records[$i]->顧客名->value);
				$str = 	mb_substr($str,0,6,'UTF-8');

				for($a = 0; $a < count($step1); $a++) {
					$Date		= explode("-", $notifydate);
					$notifydate = $step1[$a];            	  // 差し込み日付
					$title 		= $str.$step2[$a].$step3[$a]; // 差し込みタイトル
					$desc		= "https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$i]->レコード番号->value;

					$res 	= $google->setgoogle($mainid, $mainpass, $notifydate, $title, $desc);
				}

				// --------------------
				// 請求入金管理を更新する。
				// --------------------
				$k = new TcKintone($domain,$user,$passwd);
				$k->parInit();								// API連携用のパラメタを初期化する
				$k->intAppID 		= $this->AppID;			// アプリID
				$k->strContentType	= "Content-Type: application/json";

				// プロセスが請求書作成中の場合、請求書発行済にする
				$recObj = new stdClass;
				$recObj->id = $mail_json->records[$i]->レコード番号->value;
				$recObj->record->$stepchk = $this->valEnc("済み");

				$updData 			= new stdClass;
				$updData->app 		= $this->AppID;
				$updData->records[] = $recObj;
				$k->aryJson = $updData;
				$jsonSNKK = $k->runCURLEXEC( TC_MODE_UPD );

//				echo("<br>");
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成（期限通知）                                          */
	    /*************************************************************************/
		function setbody1($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."さん ".$ageDay." ".$age."\n";
					}
					break;
				case "2":
				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知）	                                 */
	    /*************************************************************************/
		function setbody2($mail_json , $bodyptr ,$domain ,$user ,$passwd) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$komadd   = $this->Address; // メールアドレス項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."\n";
					}
					break;
				case "2":
					$strurl = "https://".$this->DomainName."/k/".$this->AppID."/show#record="; // 戻り先アドレスの設定

					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$strslist = ""; // 施工種別を繋いで表示する文字列
							for($n = 0; $n < count($mail_json[$i]->records[$a]->施工種別->value); $n++) {
								if($strslist == ""){
									$strslist = $mail_json[$i]->records[$a]->施工種別->value[$n];
								}else{
									$strslist .= "・". $mail_json[$i]->records[$a]->施工種別->value[$n];
								}
							}

							$setlist .= $mail_json[$i]->records[$a]->完工日->value." ".$mail_json[$i]->records[$a]->顧客名->value." / ".$mail_json[$i]->records[$a]->住所->value."(".$strslist.")\n           ".$strurl.$mail_json[$i]->records[$a]->レコード番号->value."\n";
						}
					}

				    break;
			    case "3":

					for($a = 0; $a < count($mail_json->records); $a++) {

						// ------------------------------------------------------
						// 同顧客で通知日以上の日付がないか検索（あれば通知不要）
						// ------------------------------------------------------
						$db = new TcKintone($domain,$user,$passwd);	// API連携クラス
						$db->parInit();								// API連携用のパラメタを初期化する
						$db->intAppID 	= $this->AppID;				// アプリID

					    $db->strQuery = "(( 案件レコード番号 = ".$mail_json->records[$a]->案件レコード番号->value.") and ( アクション実施日 > \"".$mail_json->records[$a]->アクション実施日->value."\"))" ; // クエリパラメータ
						$nipo_json = $db->runCURLEXEC( TC_MODE_SEL );

						if( $db->intDataCount == 0 ) {
							// 対象データ文章の組み立て
							$setlist.=	"顧客名　　　　　　：".$mail_json->records[$a]->顧客名->value."\n";
							$setlist.=	"最終アクション日　：".$mail_json->records[$a]->アクション実施日->value."\n";
							$setlist.=	"レコード　　　　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
							$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

							// ヘッダーと繋ぐ
							$mailbody 	= $this->Header;	 // 本文の設定
							$mailbody  .= $setlist; 		 //繰り返し項目

							// メール送信処理
							$sendmail = new tcAlert();
							$sendmail->setTo( $email );
							$sendmail->setTitle( $this->subject );
							$sendmail->setBody( $mailbody );
							$res = $sendmail->sendMail();
						}
					}

			        break;
			    case "4":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"納品日　　　：".$mail_json->records[$a]->納品日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}
			        break;
			    case "5":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"見積提出日　：".$mail_json->records[$a]->見積提出日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}
			        break;
			    case "6":
					for($a = 0; $a < count($mail_json->records); $a++) {
						// 初期化
						$subject  = "";
						$mailbody = "";
						$email    = "";
						$setlist  = "";

						$subject    = $this->subject;    // タイトルの設定
						// 対象データ文章の組み立て
						$setlist.=	"お客様名　　：".$mail_json->records[$a]->顧客名->value."\n";
						$setlist.=	"案件名　　　：".$mail_json->records[$a]->案件名->value."\n";
						$setlist.=	"受注日　　　：".$mail_json->records[$a]->受注日->value."\n";
						$setlist.=	"レコード　　："."https://".$this->DomainName."/k/".$this->AppID."/show#record=".$mail_json->records[$a]->レコード番号->value."\n\n";
						$email 	 =  $mail_json->records[$a]->$komadd->value; // メールアドレス

						// ヘッダーと繋ぐ
						$mailbody 	= $this->Header;	 // 本文の設定
						$mailbody  .= $setlist; 		 //繰り返し項目

						// メール送信処理
						$sendmail = new tcAlert();
						$sendmail->setTo( $email );
						$sendmail->setTitle( $subject );
						$sendmail->setBody( $mailbody );
						$res = $sendmail->sendMail();
					}

			        break;
				case "7":
					$strurl = "https://".$this->DomainName."/k/".$this->AppID."/show#record="; // 戻り先アドレスの設定

					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$strslist = ""; // 施工種別を繋いで表示する文字列
							for($n = 0; $n < count($mail_json[$i]->records[$a]->施工種別->value); $n++) {
								if($strslist == ""){
									$strslist = $mail_json[$i]->records[$a]->施工種別->value[$n];
								}else{
									$strslist .= "・". $mail_json[$i]->records[$a]->施工種別->value[$n];
								}
							}

							$setlist .= $mail_json[$i]->records[$a]->契約日->value." ".$mail_json[$i]->records[$a]->顧客名->value." / ".$mail_json[$i]->records[$a]->住所->value."(".$strslist.")\n           ".$strurl.$mail_json[$i]->records[$a]->レコード番号->value."\n";
						}
					}
			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（記念日通知）	                                     */
	    /*************************************************************************/
		function setbody3($mail_json , $bodyptr) {

			$i = 0;
			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$age      = ""; // 年齢
			$ageDay   = ""; // 誕生日
			$tbcount  = 0;  // テーブルレコード数
			$nowyear  = date('Y');
			$setyear  = "";

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$tbcount = count($mail_json->records[$a]->記念日テーブル->value);

					    for($i = 0; $i <= ($tbcount); $i++) {
							if( ( $mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_月_->value == $this->Condition1 ) && ($mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_日_->value != "" )){
								$setlist.=	$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日名->value." ".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_月_->value."月".$mail_json->records[$a]->記念日テーブル->value[$i]->value->記念日_日_->value."日\n";
							}
						}
					}
					break;
				case "2":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$tbcount = count($mail_json->records[$a]->家族テーブル->value);

					    for($i = 0; $i <= ($tbcount); $i++) {
							if( ( $mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_月_->value == $this->Condition1 ) && ($mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_日_->value != "" )){

								if( $mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_年_->value != ""){
									$setyear = (intval($nowyear) - intval($mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_年_->value));
								}else{
									$setyear = "";
								}

								$setlist.=	$mail_json->records[$a]->社員名->value."さんの ".$mail_json->records[$a]->家族テーブル->value[$i]->value->続柄->value." ". $mail_json->records[$a]->家族テーブル->value[$i]->value->家族名->value ." ".$mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_月_->value."月".$mail_json->records[$a]->家族テーブル->value[$i]->value->誕生日_日_->value."日(".$setyear.")\n";
							}
						}
					}
				    break;
			    case "3":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->事業所->value." ".$mail_json->records[$a]->所属->value." ".$mail_json->records[$a]->役職->value." ".$mail_json->records[$a]->職種->value." ".$mail_json->records[$a]->月->value."月".$mail_json->records[$a]->日->value."日(".$setyear.")\n";

					}
			        break;
			    case "4":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."さん ".$mail_json->records[$a]->月->value."月".$mail_json->records[$a]->日->value."日(".$setyear.")\n";

					}
			        break;
			    case "5":
					for($a = 0; $a < count($mail_json->records); $a++) {

						if( $mail_json->records[$a]->年_創業日_->value != ""){
							$setyear = (intval($nowyear) - intval($mail_json->records[$a]->年_創業日_->value));
						}else{
							$setyear = "";
						}

						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->代表者名->value."社長 ".$mail_json->records[$a]->年_創業日_->value."年".$mail_json->records[$a]->月_創業日_->value."月".$mail_json->records[$a]->日_創業日_->value."日(".$setyear.")\n";

					}
			        break;
			    default:
			}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（定期通知）		                                 */
	    /*************************************************************************/
		function setbody4($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目
			$arrsort = "";
			$k = 0;

			switch ($bodyptr) {
				case "1":
					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$setlist.=	$mail_json[$i]->records[$a]->顧客名->value." ".$mail_json[$i]->records[$a]->社員名->value."\n";
						}
					}
					break;
				case "2":
					$strurl = "https://".$this->DomainName."/k/".$this->AppID."/show#record="; // 戻り先アドレスの設定

					for($i = 0; $i <= count($mail_json); $i++) {
						for($a = 0; $a < count($mail_json[$i]->records); $a++) {
							$strslist = ""; // 施工種別を繋いで表示する文字列
							for($n = 0; $n < count($mail_json[$i]->records[$a]->施工種別->value); $n++) {
								if($strslist == ""){
									$strslist = $mail_json[$i]->records[$a]->施工種別->value[$n];
								}else{
									$strslist .= "・". $mail_json[$i]->records[$a]->施工種別->value[$n];
								}
							}

							// ソート用の日付と、文字列を配列に格納
							$arrsort[$k][id]   = $mail_json[$i]->records[$a]->着工予定日->value;
							$arrsort[$k][val]  = $mail_json[$i]->records[$a]->着工予定日->value." ".$mail_json[$i]->records[$a]->顧客名->value." / ".$mail_json[$i]->records[$a]->住所->value."(".$strslist.")\n           ".$strurl.$mail_json[$i]->records[$a]->レコード番号->value."\n";

							$k++; // 配列のカウントアップ
						}
					}

					foreach($arrsort as $key=>$value){
					    $id[$key] = $value['id'];
					}

					array_multisort($id ,SORT_ASC,$arrsort);

					for($a = 0; $a < count($arrsort); $a++) {
						$setlist.= $arrsort[$a][val];
					}

				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 差込用データ作成	（ステップ通知(初回)）	                             */
	    /*************************************************************************/
		function setbody5($mail_json , $bodyptr) {

			$a = 0;
			$setlist  = ""; // 繰り返し項目

			switch ($bodyptr) {
				case "1":
					for($a = 0; $a < count($mail_json->records); $a++) {
						$setlist.=	$mail_json->records[$a]->顧客名->value." ".$mail_json->records[$a]->社員名->value."\n";
					}
					break;
				case "2":
				    break;
			    case "3":
			        break;
			    case "4":
			        break;
			    case "5":
			        break;
			    default:
					}

			return $setlist;
		}

	    /*************************************************************************/
	    /* 生年月日から年齢変換                                                  */
	    /*************************************************************************/
		function birthToAge($ymd){
		 
			$base  = new DateTime();
			$today = $base->format('Ymd');
		 
			$birth    = new DateTime($ymd);
			$birthday = $birth->format('Ymd');
		 
			$age = (int) (($today - $birthday) / 10000);
		 
			return $age;
		}


	    /*************************************************************************/
	    /* メンバ関数                                                            */
	    /*************************************************************************/
		function valEnc( $val ) {
			$wk = new stdClass;
			$wk->value = mb_convert_encoding($val , "UTF-8", "auto");
			return ( $wk );
		}

	}

?>
