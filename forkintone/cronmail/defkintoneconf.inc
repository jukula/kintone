<?php
/*****************************************************************************/
/*  kintoneｱﾌﾟﾘ初期定義ファイル                               (Version 1.00) */
/*                                                                           */
/*   本ファイル内で定義されたものは各サイトごとに異なるものです              */
/*                                                                           */
/*                                    Copyright(C)2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/
if (!defined("DEFKINTONECONF_INC")) {
    // ２重インクルード防止
    define("DEFKINTONECONF_INC", true);

    ///////////////////////////////////////////////////////////////////////////////
    // 初期設定
    ///////////////////////////////////////////////////////////////////////////////
    define("TC_CY_DOMAIN"   , "tc-demo.cybozu.com"	  ); // ドメイン

    define("TC_CY_USER"     , "Administrator"         ); // ユーザ名
    define("TC_CY_PASSWORD" , "Tc2010"         	  ); // パスワード

    define("TC_CY_AUTH_USER", ""                      ); // 認証ユーザ
    define("TC_CY_AUTH_PASS", ""                      ); // 認証パスワード

    define("TC_CY_PHP_DOMAIN", "cronmail"             ); // PHPのドメイン

	///////////////////////////////////////////////////////////////////////////////
	// kintoneｱﾌﾟﾘ設定
	///////////////////////////////////////////////////////////////////////////////


	// 集計用 行区分（保守管理集計用）
	define("TC_SKIKBN_MEISAI"   , 0);		// 明細行
	define("TC_SKIKBN_KEI_TUKI" , 1);		// 集計行：月計（未使用：明細が月計のため）
	define("TC_SKIKBN_KEI_NEN"  , 2);		// 集計行：年計
	define("TC_SKIKBN_KEI_RKEI" , 3);		// 集計行：累計

	define("TC_SEK_TNI_TUKI" 	, "月");	// 積算単位
	define("TC_SEK_TNI_NEN" 	, "年");	// 積算単位

	// メール通知区分
    define("TCTYPE_KIGEN"   , 1 );  // 期限通知
    define("TCTYPE_STEP"    , 2 );  // ステップ通知
    define("TCTYPE_KINENBI" , 3 );  // 記念日通知
    define("TCTYPE_TEIKI"   , 4 );  // 定期通知
    define("TCTYPE_STEP_1ST", 5 );  // ステップ通知(初回)

	// メール本文パターン
    define("TCTYPE_PTN1"   , 1 );  // 本文パターン1
    define("TCTYPE_PTN2"   , 2 );  // 本文パターン2
    define("TCTYPE_PTN3"   , 3 );  // 本文パターン3
    define("TCTYPE_PTN4"   , 4 );  // 本文パターン4
    define("TCTYPE_PTN5"   , 5 );  // 本文パターン5
    define("TCTYPE_PTN6"   , 6 );  // 本文パターン6
    define("TCTYPE_PTN7"   , 7 );  // 本文パターン7
    define("TCTYPE_PTN8"   , 8 );  // 本文パターン8
    define("TCTYPE_PTN9"   , 9 );  // 本文パターン9
    define("TCTYPE_PTN10"  , 10 ); // 本文パターン10
	///////////////////////////////////////////////////////////////////////////////
	// アップロード、ダウンロード用一時フォルダ
	///////////////////////////////////////////////////////////////////////////////
	define( "TC_UPLOAD_FOLDER" 	, "c:/uploadtmp/" );

    ///////////////////////////////////////////////////////////////////////////////
    // グローバル変数
    ///////////////////////////////////////////////////////////////////////////////

}
?>