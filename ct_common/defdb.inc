<?php
/*****************************************************************************/
/*  汎用DB管理用定数定義ファイル                              (Version 1.03) */
/*                                                                           */
/*   本ファイル内で定義されたものは各サイトごとに異なるものです              */
/*                                                                           */
/*   更新履歴       2005/07/21      Version 1.01(Jun.K)                      */
/*                                  コードテーブル修正に伴う変更             */
/*                  2006/10/12      Version 1.02(Jun.K)                      */
/*                                  MySQL 対応                               */
/*                  2008/07/06      Version 1.03(Jun.K)                      */
/*                                  tinyint、datetime型対応                  */
/*                                                                           */
/*                                    Copyright(C)2004-2008 CrossTier Corp.  */
/*****************************************************************************/
if (!defined("DEFDB_INC")) {
    // ２重インクルード防止
    define("DEFDB_INC", true);

    ///////////////////////////////////////////////////////////////////////////////
    // データベース接続情報
    ///////////////////////////////////////////////////////////////////////////////
    define("DB_TYPE"       , 1                );    // DB(0:PostgreSQL / 1:MySQL)
    define("CONNECT_HOST"  , "mysql3b.xserver.jp"      );    // DBサーバ
    define("CONNECT_DB"    , "crosstier_aurora"   ); // データベース名
    define("CONNECT_USER"  , "crosstier_aurora");    // 接続ユーザ名
    define("CONNECT_PASS"  , "Aurora2019"       );    // 接続パスワード

    ///////////////////////////////////////////////////////////////////////////////
    // フィールド型
    ///////////////////////////////////////////////////////////////////////////////
    define("FTYPE_INT"      , 1 );
    define("FTYPE_REAL"     , 2 );
    define("FTYPE_DOUBLE"   , 3 );
    define("FTYPE_TINYINT"  , 4 );
    define("FTYPE_CHAR"     , 5 );
    define("FTYPE_VARCHAR"  , 6 );
    define("FTYPE_TEXT"     , 7 );
    define("FTYPE_TIMESTAMP", 10);
    define("FTYPE_DATE"     , 11);
    define("FTYPE_TIME"     , 12);
    define("FTYPE_DATETIME" , 13);

}
?>
