<?php
/*****************************************************************************/
/*  汎用定数定義ファイル                                      (Version 1.20) */
/*                                                                           */
/*   作成日         2002/01/10                                               */
/*   更新履歴       2005/07/19      Version 1.01(Yoshikazu.N)                */
/*                                  ACT_PASSCH 追加                          */
/*                  2006/08/30      Version 1.02(Yoshikazu.N)                */
/*                                  ACT_CSVUP、ACT_CSVDL 追加                */
/*                  2015/06/20      Version 1.03(J.Kataoka)                  */
/*                                  ACT_DETAIL、ACT_LOG 追加                 */
/*                  2019/06/27      Version 1.20(Jun.K)                      */
/*                                  項目整理                                 */
/*                                                                           */
/*                                    Copyright(C)2002-2019 CrossTier Corp.  */
/*****************************************************************************/
//include_once("init.inc");

/////////////////////
// フラグ値
/////////////////////
define( "FLG_ON"      , "1"  );   // ON
define( "FLG_OFF"     , "0"  );   // OFF

/////////////////////
// 作業フォルダ
/////////////////////
define("TEMP_DIR"      , "tmp/"   );  // 一時保存フォルダ
define("INC_DIR"       , "inc/"   );  // インクルードファイル作成フォルダ

///////////////////////////
// 画面制御用
///////////////////////////
define("ACT_EDIT"      , 1   ); // 編集
define("ACT_UPDATE"    , 2   ); // 編集(未使用)
define("ACT_DEL"       , 3   ); // 削除
define("ACT_ADD"       , 4   ); // 登録
define("ACT_INPUT"     , 5   ); // 登録(未使用)
define("ACT_INSERT"    , 6   ); // 登録(未使用)
define("ACT_RE"        , 7   ); // 復帰
define("ACT_COPY"      , 8   ); // 複写
define("ACT_PRI"       , 9   ); // 表示変更
define("ACT_ACC"       , 10  ); // 承認
define("ACT_PASSCH"    , 11  ); // パスワード変更
define("ACT_KILL"      , 12  ); // 完全削除
define("ACT_CSVUP"     , 13  ); // CSVアップロード
define("ACT_CSVDL"     , 14  ); // CSVダウンロード
define("ACT_MAIL"      , 15  ); // メール送信
define("ACT_DETAIL"    , 16  ); // 詳細表示
define("ACT_LOG"       , 17  ); // ログ表示

define("ACT_UPDATEM"   , 21  ); // メール配信フラグ更新

///////////////////////////
// ログインエラー最大回数
///////////////////////////
define( "LOGINERR_MAX", 15 );   // ログインエラー最大回数

/////////////////////
// 会員レベル
/////////////////////
define("LEVEL_ZERO"   ,  0 );   // 承認待ち(未承認) = 削除状態
define("LEVEL_GUEST"  ,  1 );   // ゲスト会員(不要？)
define("LEVEL_NORMAL" ,  3 );   // 一般会員（社員）
define("LEVEL_MANAGER", 10 );   // 店長
define("LEVEL_ADMIN"  , 90 );   // 管理者
define("LEVEL_SUPER"  , 99 );   // スーパーユーザー


//date_default_timezone_set("Asia/Tokyo");

//////////////////////////
// 以下、サイトごと定義
//////////////////////////
// 管理者初期値
define("ADMIN_ID"      , "admin");  // 管理者ID
define("ADMIN_PASS"    , "pass" );  // 管理者パスワード

// 写真枚数
define("PHOTO_MAX"     , 6  );
// 写真サイズ
define("FILESIZE_MAX"  , 1024 * 500);   // 500kb

// CSVファイル名
define("CSV_LIST"      , "list.csv"); // 一覧

// 並び順の並び替え指示
define("ORDER_UP"    ,    1 ); // デクリメント  (画面上、上へ)
define("ORDER_DOWN"  ,    2 ); // インクリメント(画面上、下へ)
define("ORDER_TOP"   ,    3 ); // 先頭へ  (画面上、一番上へ)
define("ORDER_BOTTOM",    -1); // 最後尾へ(画面上、一番下へ)

/////////////////////////////
// グローバル変数
/////////////////////////////


?>
