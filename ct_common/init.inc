<?php
/*****************************************************************************/
/*  システム用 環境設定                                                      */
/*                                                                           */
/*                               Copyright(C)2003-2013 TIME CONCIER Co.,Ltd. */
/*****************************************************************************/

$strHost = GetEnv("SERVER_NAME");
if(strToUpper($strHost) == "AURORA.CROSS-TIER.COM") {
    // テストサーバ
    define("ROOT_DIR"  , "/home/cross-tier/cross-tier.com/public_html/aurora/");
    define("ROOT_URL"  , "http://aurora.cross-tier.com/" );
    define("SECURE_URL", "http://aurora.cross-tier.com/" );
    define("COOKIE_URL", $strHost);

    // 画像URL共通化のため
    define("ROOT_IMG_URL"  , "http://aurora.cross-tier.com/" );

    define('SHOP',          'Aurora[TEST]');
    define('TITLE',         'Aurora[TEST]');

}else{
    // 本番サーバ
    define("ROOT_DIR"  , "/");
    define("ROOT_URL"  , "http://" . $strHost."/" );
    define("SECURE_URL", "https://" . $strHost."/" );
    define("COOKIE_URL", $strHost);

    // 画像URL共通化のため
    define("ROOT_IMG_URL"  , "https://www.xxxxxx/" );

    define('SHOP',          'Aurora');
    define('TITLE',         'Aurora');

}

//サイトタイトル
define("SIGHT_TITLE", "販売管理システム");

?>
