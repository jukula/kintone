<?php
/*****************************************************************************/
/*  システム用 環境設定                                                      */
/*                                                                           */
/*                                    Copyright(C)2003-2019 CrossTier Corp.  */
/*****************************************************************************/

$strHost = GetEnv("SERVER_NAME");
if(strToUpper($strHost) == "KINTONE.CROSS-TIER.COM") {
    // テストサーバ
    define("ROOT_DIR"  , "/home/cross-tier/cross-tier.com/public_html/kintone/");
    define("ROOT_URL"  , "http://kintone.cross-tier.com/" );
    define("SECURE_URL", "http://kintone.cross-tier.com/" );
    define("COOKIE_URL", $strHost);

    // 画像URL共通化のため
    define("ROOT_IMG_URL"  , "http://kintone.cross-tier.com/" );

    define('SHOP',          'kintone[TEST]');
    define('TITLE',         'kintone[TEST]');

}else{
    // 本番サーバ
    define("ROOT_DIR"  , "/");
    define("ROOT_URL"  , "http://" . $strHost."/" );
    define("SECURE_URL", "https://" . $strHost."/" );
    define("COOKIE_URL", $strHost);

    // 画像URL共通化のため
    define("ROOT_IMG_URL"  , "http://kintone.cross-tier.com/" );

    define('SHOP',          'kintone');
    define('TITLE',         'kintone');

}

/******************/
/*    共通設定    */
/******************/
// 共通フォルダ
define("CT_COM_DIR",  "ct_common/");

//サイトタイトル
define("SIGHT_TITLE", "販売管理システム");

?>
