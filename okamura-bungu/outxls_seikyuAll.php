<?php
/*****************************************************************************/
/* 月報出力PHP                                                (Version 1.00) */
/*   ファイル名 : outxls_seikyuAll.php                                       */
/*   更新履歴   2019/07/04  Version 1.00(Jun.K)                              */
/*                                                                           */
/*   [備考]                                                                  */
/*      ctutility.incを必ずインクルードすること                              */
/*   [必要ファイル]                                                          */
/*      ctdef.inc / ctutility.inc / ctkintone.php / ctkintonerecord.php      */
/*                                                                           */
/*                                                                           */
/*                                       Copyright(C)2019 CrossTier Co.,Ltd. */
/*****************************************************************************/
    header("Access-Control-Allow-Origin: *");
    header("Content-Type:text/html;charset=utf-8");

    mb_language("Japanese");

    require_once("init.inc");
//    print(ROOT_DIR . CT_COM_DIR . "ctdef.inc");
    require_once(ROOT_DIR . CT_COM_DIR . "ctdef.inc");
    require_once(ROOT_DIR . CT_COM_DIR . "ctutility.inc");
    require_once(ROOT_DIR . CT_COM_DIR . "cterror.php");

    require_once("initkintone.conf");
    require_once(ROOT_DIR . CT_CY_COMDIR . "ctkintone.php");
    require_once(ROOT_DIR . CT_CY_COMDIR . "ctkintonerecord.php");
    require_once(ROOT_DIR . CT_CY_COMDIR . "ctkintonecommon.php");
//    include_once("../tccom/tckintoneguest.php");

    require_once(ROOT_DIR . CT_CY_COMDIR . 'Classes/PHPExcel/IOFactory.php');

    define( "CT_TB1_COUNT" , 10 );
    define( "CT_TB2_COUNT" , 34 );
    /*****************************************************************************/
    /* 開始                                                                      */
    /*****************************************************************************/
    $clsSrs = new outExcelMain();

    $clsSrs->appId      = $_REQUEST['pAppId'] - 0;
    $clsSrs->insName    = $_REQUEST['pName'];
    $clsSrs->pQuery     = $_REQUEST['pQuery'];

    // 実行
    $clsSrs->main();

    /*****************************************************************************/
    /* クラス定義：メイン                                                        */
    /*****************************************************************************/
    class outExcelMain
    {

        /*************************************************************************/
        /* メンバ変数                                                            */
        /*************************************************************************/
        var $appId        = null;     // アプリ番号（パラメタ）
        var $guestId      = null;     // ゲスト番号（パラメタ）
        var $insName      = null;
        var $pQuery       = null;
        var $xlsFile1     = "templates/outxls_seikyu1.xlsx";
        var $xlsFile2     = "templates/outxls_seikyu2.xlsx";
        var $xlsFile3     = "templates/outxls_seikyu3.xlsx";
        var $xlsFile4     = "templates/outxls_seikyu4.xlsx";
        var $err;
        var $common;
        /*************************************************************************/
        /* コンストラクタ                                                        */
        /*************************************************************************/
        function __construct() {
            $this->err = new ctError();
            $this->common = new ctKintoneCommon();
        }

        /*************************************************************************/
        /* メインの処理を実行する                                                */
        /*  引数    なし                                                         */
        /*************************************************************************/
        function main() {
            $msg     = "";
/*
echo "APPID：".$this->appId."<br>";
echo "--------------------------------------------------<br>";
*/
            // エクセルのテンプレートの準備
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objPHPExcel1 = $objReader->load($this->xlsFile1);
            $objPHPExcel2 = $objReader->load($this->xlsFile2);
            $objPHPExcel3 = $objReader->load($this->xlsFile3);
            $objPHPExcel4 = $objReader->load($this->xlsFile4);

            // ------------------------------------------------
            // ctKintoneクラスのインスタンス生成
            // ------------------------------------------------
            $k = new ctKintone();
            $k->parInit();                                        // API連携用のパラメタを初期化する
            $k->intAppID     = $this->appId;                      // アプリID
            $k->arySelFields = array(                             // 読込用フィールドパラメータ
            );
            // クエリパラメータ
            $k->strQuery     = $this->pQuery;
            // クエリ実行(REST API:GET)
            $jsonData = $k->runCURLEXEC( CT_MODE_SEL );

            // ------------------------------------------------
            // 原価管理データ取得
            // ------------------------------------------------
            $appId    = $this->appId;
            $strQuery = '請求書発行日 = THIS_MONTH()';
            $recInfo  = $this->getRecords( $appId, $strQuery, array() );    // 全項目取得

            // ------------------------------------------------
            // 取得できた場合、セルへ設定
            // ------------------------------------------------
            if( $k->intDataCount > 0 ) {
                $msg = $this->setCell( $objPHPExcel1, $objPHPExcel2, $objPHPExcel3, $objPHPExcel4, $appId, $jsonData->records, $recInfo );
            }


            // 保存用ディレクトリクリア
//            $dir = "/home/cross-tier/timeconcier.jp/public_html/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/";
            $dir = ROOT_DIR . CT_CY_PHPDIR . CT_CY_TMPDIR;
            $this->deleteData ( $dir );

            // ------------------------------------------------
            // ダウンロード用エクセルを保存
            // ------------------------------------------------
            for ( $i = 0; $i < count($msg); $i++ ) {

                if ( $msg[$i] > 0 ) {
                    $j = $i + 1;

                    // ダウンロード用エクセルを準備
                    $saveName = "";
                    $link = "";
                    $insTitle = "";
                    if ( $this->insName ) { $insTitle = "(". $this->insName. ")"; }
                    switch ( $i ) {
                        case 0:
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel1, 'Excel2007');
                            $saveName = "請求書".$insTitle."_".date('Y年m月').".xlsx";
                            $link = "請求書のダウンロードはこちら";
                            break;
                        case 1:
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel2, 'Excel2007');
                            $saveName = "口座振替のお知らせ".$insTitle."_".date('Y年m月').".xlsx";
                            $link = "口座振替のお知らせのダウンロードはこちら";
                            break;
                        case 2:
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel3, 'Excel2007');
                            $saveName = "請求書(送付先込み)".$insTitle."_".date('Y年m月').".xlsx";
                            $link = "請求書(送付先込み)のダウンロードはこちら";
                            break;
                        case 3:
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel4, 'Excel2007');
                            $saveName = "口座振替のお知らせ(送付先込み)".$insTitle."_".date('Y年m月').".xlsx";
                            $link = "口座振替のお知らせ(送付先込み)のダウンロードはこちら";
                            break;
                        default:
                            break;
                    }

                    // ファイル出力
print "OK1<br>";
//                    $objWriter->save("tctmp/".$saveName);
//                    $saveurl = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName;
//$objWriter->save($dir . $saveName);
                    $objWriter->save("tmp/" . $saveName);
print "OK2<br>";
                    $saveurl = ROOT_URL . CT_CY_PHPDIR . CT_CY_TMPDIR . $saveName;
                    echo '<li><a href="' . $saveurl . '" target="_blank">' . $link . '</a>（エクセル形式）</li><br>';

                }

            }
/*
            // ダウンロード用エクセルを準備
            $objWriter1 = PHPExcel_IOFactory::createWriter($objPHPExcel1, 'Excel2007');
            $objWriter2 = PHPExcel_IOFactory::createWriter($objPHPExcel2, 'Excel2007');
            $objWriter3 = PHPExcel_IOFactory::createWriter($objPHPExcel3, 'Excel2007');
            $objWriter4 = PHPExcel_IOFactory::createWriter($objPHPExcel4, 'Excel2007');
            // ファイル名生成
            list($msec, $sec) = explode(" ", microtime());
            $saveName1 = "請求書_".date('Y年m月').".xlsx";
            $saveName2 = "口座振替のお知らせ_".date('Y年m月').".xlsx";
            $saveName3 = "請求書(送付先込み)_".date('Y年m月').".xlsx";
            $saveName4 = "口座振替のお知らせ(送付先込み)_".date('Y年m月').".xlsx";
            // ファイル出力
            $objWriter1->save("tctmp/".$saveName1);
            $objWriter2->save("tctmp/".$saveName2);
            $objWriter3->save("tctmp/".$saveName3);
            $objWriter4->save("tctmp/".$saveName4);
            $saveurl1 = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName1;
            $saveurl2 = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName2;
            $saveurl3 = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName3;
            $saveurl4 = "http://www.timeconcier.jp/forkintone/".TC_CY_PHP_DOMAIN."/tctmp/".$saveName4;

            echo '<li><a href="' .$saveurl1. '" target="_blank">請求書のダウンロードはこちら</a>（エクセル形式）</li><br>';
            echo '<li><a href="' .$saveurl2. '" target="_blank">口座振替のお知らせのダウンロードはこちら</a>（エクセル形式）</li><br>';
            echo '<li><a href="' .$saveurl3. '" target="_blank">請求書(送付先込み)のダウンロードはこちら</a>（エクセル形式）</li><br>';
            echo '<li><a href="' .$saveurl4. '" target="_blank">口座振替のお知らせ(送付先込み)のダウンロードはこちら</a>（エクセル形式）</li><br>';
            echo($msg);
*/

//header("Location: {$saveurl}");

            echo "<input type='button' value='閉じる' onclick='window.close();'></>\n";
//            return;

        }

        /*************************************************************************/
        /* データをセルへ設定する。                                              */
        /*  引数    なし                                                         */
        /*************************************************************************/
        function setCell( &$pPHPExcel1 , &$pPHPExcel2 ,&$pPHPExcel3 ,&$pPHPExcel4 , $pAppId , $pRecords , $recInfo ) {
            $ret = array(0,0,0,0);

            // 複数シート作成
            $sheetNo1 = 1;  // 出力シート位置の指定(請求書)
            $sheetNo2 = 1;  // 出力シート位置の指定(口座振替)
            $sheetNo3 = 1;  // 出力シート位置の指定(請求書(送付先込み))
            $sheetNo4 = 1;  // 出力シート位置の指定(口座振替(送付先込み))
            for ( $i = 0; $i < count($pRecords); $i++ ) {
                if ( $pRecords[$i]->種別->value == "振込" ) {
                    if ( $pRecords[$i]->送付先情報->value == "無" ) {
                        if ( $sheetNo1 > 1 ) {
                            $SheetTitle = "Sheet". ( $sheetNo1 );
                            $baseSheet = $pPHPExcel1 -> getSheet( 0 );
                            $copySheet = $baseSheet -> copy();
                            $copySheet -> setTitle( $SheetTitle );
                            $pPHPExcel1 -> addSheet( $copySheet );
                        }
                        $sheetNo1++;
                    } else if ( $pRecords[$i]->送付先情報->value == "有" ) {
                        if ( $sheetNo3 > 1 ) {
                            $SheetTitle = "Sheet". ( $sheetNo3 );
                            $baseSheet = $pPHPExcel3 -> getSheet( 0 );
                            $copySheet = $baseSheet -> copy();
                            $copySheet -> setTitle( $SheetTitle );
                            $pPHPExcel3 -> addSheet( $copySheet );
                        }
                        $sheetNo3++;
                    }
                } else {
                    if ( $pRecords[$i]->送付先情報->value == "無" ) {
                        if ( $sheetNo2 > 1 ) {
                            $SheetTitle = "Sheet". ( $sheetNo2 );
                            $baseSheet = $pPHPExcel2 -> getSheet( 0 );
                            $copySheet = $baseSheet -> copy();
                            $copySheet -> setTitle( $SheetTitle );
                            $pPHPExcel2 -> addSheet( $copySheet );
                        }
                        $sheetNo2++;
                    } else if ( $pRecords[$i]->送付先情報->value == "有" ) {
                        if ( $sheetNo4 > 1 ) {
                            $SheetTitle = "Sheet". ( $sheetNo4 );
                            $baseSheet = $pPHPExcel4 -> getSheet( 0 );
                            $copySheet = $baseSheet -> copy();
                            $copySheet -> setTitle( $SheetTitle );
                            $pPHPExcel4 -> addSheet( $copySheet );
                        }
                        $sheetNo4++;
                    }
                }
            }

            $sheetNo1 = 0;  // 出力シート位置の指定(請求書)
            $sheetNo2 = 0;  // 出力シート位置の指定(口座振替)
            $sheetNo3 = 0;  // 出力シート位置の指定(請求書(請求先込み))
            $sheetNo4 = 0;  // 出力シート位置の指定(口座振替(請求先込み))
            $today    = date('Y-m-d');
            $ymd      = explode( '-' , $today );
            $next     = explode( '-' , date('Y-m', strtotime( date('Y-m-1') . '+1 month' ) ) );
            $name1    = "";
            $name2    = "";
            $name3    = "";
            $name4    = "";
            $keisyo   = "";
            $keisyo2  = "";
            $seikyuNo = "";
            $goods    = "まいぷれ高知（ショップ）". (int)$ymd[1]. "月掲載料";
            $set      = 0;
            $bkseiNo  = array();

            // 請求書発行NO末番取得
            $cnt = 1;
            for ( $j = 0; $j < count($recInfo); $j++ ) {
                if ( $recInfo[$j]->請求書発行No->value ) {
                    $cnt++;
                }
            }

            for ( $i = 0; $i < count($pRecords); $i++ ) {

                $postal   = "〒". $pRecords[$i]->請求先郵便番号->value;

                // 請求先敬称判断
                if ( $pRecords[$i]->請求先宛名->value ) {
                    $name1  = $pRecords[$i]->請求先名->value;
                    $name2  = $pRecords[$i]->請求先宛名->value;
                    $keisyo = "様";
                } else {
                    $name1  = "";
                    $name2  = $pRecords[$i]->請求先名->value;
                    $keisyo = "御中";
                }

                if ( $pRecords[$i]->発行状態->value == "済" ) {
                    $seikyuNo = $pRecords[$i]->請求書発行No->value;
                } else {
                    $yNo = substr($ymd[0], 2, 2);
                    $seikyuNo = "No：MP". $ymd[1]. "-". $yNo. str_pad($cnt, 3, 0, STR_PAD_LEFT);
                    $bkseiNo[$set] = $seikyuNo;
                    $set++;
                    $cnt++;
                }

                $flg3 = false;
                $setno3 = "";
                $gname3 = "";
                $suryo3 = "";
                $tanni3 = "";
                $tanka3 = "";
                if ( $pRecords[$i]->商品名3->value ) {
                    $flg3   = true;
                    $gname3 = $pRecords[$i]->商品名3->value;
                }
                if ( $pRecords[$i]->数量3->value ) {
                    $flg3   = true;
                    $suryo3 = $pRecords[$i]->数量3->value;
                    $tanni3 = $pRecords[$i]->単位3->value;
                    $tanka3 = $pRecords[$i]->単価3->value;
                }

                $flg4 = false;
                $setno4 = "";
                $gname4 = "";
                $suryo4 = "";
                $tanni4 = "";
                $tanka4 = "";
                if ( $pRecords[$i]->商品名4->value ) {
                    $flg4   = true;
                    $gname4 = $pRecords[$i]->商品名4->value;
                }
                if ( $pRecords[$i]->数量4->value ) {
                    $flg4   = true;
                    $suryo4 = $pRecords[$i]->数量4->value;
                    $tanni4 = $pRecords[$i]->単位4->value;
                    $tanka4 = $pRecords[$i]->単価4->value;
                }

                if ( $pRecords[$i]->種別->value == "振込" ) {

                    if ( $pRecords[$i]->送付先情報->value == "無" ) {

                        $hurikomi = (int)$next[0]. "年". (int)$next[1]. "月". "末日までに下記口座に御請求額をお振込み下さい";

                        $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 )
                                    -> setCellValue( 'AO3', $seikyuNo )
                                    -> setCellValue( 'AF5', $ymd[0] )
                                    -> setCellValue( 'AJ5', $ymd[1] )
                                    -> setCellValue( 'AM5', $ymd[2] )
                                    -> setCellValue( 'D1' , $postal )
                                    -> setCellValue( 'D2' , $pRecords[$i]->請求書送付先住所->value )
                                    -> setCellValue( 'D3' , $name1 )
                                    -> setCellValue( 'D4' , $name2 )
                                    -> setCellValue( 'T4' , $keisyo )
                                    -> setCellValue( 'B18', $pRecords[$i]->請求金額->value )
                                    -> setCellValue( 'C32', $hurikomi );

                        if ( $pRecords[$i]->数量->value ) {
                            if ( $flg3 ) { $setno3 = 3; }
                            if ( $flg4 ) { $setno4 = 4; }
                            $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 )
                                        -> setCellValue( 'B21', "1" )
                                        -> setCellValue( 'D21', "初期登録費用" )
                                        -> setCellValue( 'T21', "1" )
                                        -> setCellValue( 'W21', "ヶ月" )
                                        -> setCellValue( 'Z21', $pRecords[$i]->単価1->value )
                                        -> setCellValue( 'B22', "2" )
                                        -> setCellValue( 'D22', $goods )
                                        -> setCellValue( 'T22', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W22', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z22', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B23', $setno3 )
                                        -> setCellValue( 'D23', $gname3 )
                                        -> setCellValue( 'T23', $suryo3 )
                                        -> setCellValue( 'W23', $tanni3 )
                                        -> setCellValue( 'Z23', $tanka3 )
                                        -> setCellValue( 'B24', $setno4 )
                                        -> setCellValue( 'D24', $gname4 )
                                        -> setCellValue( 'T24', $suryo4 )
                                        -> setCellValue( 'W24', $tanni4 )
                                        -> setCellValue( 'Z24', $tanka4 );
                        } else {
                            if ( $flg3 ) { $setno3 = 2; }
                            if ( $flg4 ) { $setno4 = 3; }
                            $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 )
                                        -> setCellValue( 'B21', "1" )
                                        -> setCellValue( 'D21', $goods )
                                        -> setCellValue( 'T21', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W21', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z21', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B22', $setno3 )
                                        -> setCellValue( 'D22', $gname3 )
                                        -> setCellValue( 'T22', $suryo3 )
                                        -> setCellValue( 'W22', $tanni3 )
                                        -> setCellValue( 'Z22', $tanka3 )
                                        -> setCellValue( 'B23', $setno4 )
                                        -> setCellValue( 'D23', $gname4 )
                                        -> setCellValue( 'T23', $suryo4 )
                                        -> setCellValue( 'W23', $tanni4 )
                                        -> setCellValue( 'Z23', $tanka4 );
                        }

                        $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 ) -> getStyle( 'H18' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 ) -> getStyle( 'AC18' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel1 -> setActiveSheetIndex( $sheetNo1 ) -> setTitle( $pRecords[$i]->請求先名->value );

                        $sheetNo1++;

                    } else if ( $pRecords[$i]->送付先情報->value == "有" ) {

                        // 送付先敬称判断
                        if ( $pRecords[$i]->送付先宛名->value ) {
                            $name3   = $pRecords[$i]->送付先名->value;
                            $name4   = $pRecords[$i]->送付先宛名->value;
                            $keisyo2 = "様";
                        } else {
                            $name3   = "";
                            $name4   = $pRecords[$i]->送付先名->value;
                            $keisyo2 = "御中";
                        }

                        $hurikomi = (int)$next[0]. "年". (int)$next[1]. "月". "末日までに下記口座に御請求額をお振込み下さい";

                        $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 )
                                    -> setCellValue( 'D1' , $postal )
                                    -> setCellValue( 'D2' , $pRecords[$i]->請求書送付先住所->value )
                                    -> setCellValue( 'D3' , $name3 )
                                    -> setCellValue( 'D4' , $name4 )
                                    -> setCellValue( 'T4' , $keisyo2 )
                                    -> setCellValue( 'AO6', $seikyuNo )
                                    -> setCellValue( 'AF7', $ymd[0] )
                                    -> setCellValue( 'AJ7', $ymd[1] )
                                    -> setCellValue( 'AM7', $ymd[2] )
                                    -> setCellValue( 'D12', $name1 )
                                    -> setCellValue( 'D13', $name2 )
                                    -> setCellValue( 'T13', $keisyo )
                                    -> setCellValue( 'B22', $pRecords[$i]->請求金額->value )
                                    -> setCellValue( 'C32', $hurikomi );

                        if ( $pRecords[$i]->数量->value ) {
                            if ( $flg3 ) { $setno3 = 3; }
                            if ( $flg4 ) { $setno4 = 4; }
                            $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 )
                                        -> setCellValue( 'B25', "1" )
                                        -> setCellValue( 'D25', "初期登録費用" )
                                        -> setCellValue( 'T25', "1" )
                                        -> setCellValue( 'W25', "ヶ月" )
                                        -> setCellValue( 'Z25', $pRecords[$i]->単価1->value )
                                        -> setCellValue( 'B26', "2" )
                                        -> setCellValue( 'D26', $goods )
                                        -> setCellValue( 'T26', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W26', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z26', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B27', $setno3 )
                                        -> setCellValue( 'D27', $gname3 )
                                        -> setCellValue( 'T27', $suryo3 )
                                        -> setCellValue( 'W27', $tanni3 )
                                        -> setCellValue( 'Z27', $tanka3 )
                                        -> setCellValue( 'B28', $setno4 )
                                        -> setCellValue( 'D28', $gname4 )
                                        -> setCellValue( 'T28', $suryo4 )
                                        -> setCellValue( 'W28', $tanni4 )
                                        -> setCellValue( 'Z28', $tanka4 );
                        } else {
                            if ( $flg3 ) { $setno3 = 2; }
                            if ( $flg4 ) { $setno4 = 3; }
                            $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 )
                                        -> setCellValue( 'B25', "1" )
                                        -> setCellValue( 'D25', $goods )
                                        -> setCellValue( 'T25', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W25', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z25', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B26', $setno3 )
                                        -> setCellValue( 'D26', $gname3 )
                                        -> setCellValue( 'T26', $suryo3 )
                                        -> setCellValue( 'W26', $tanni3 )
                                        -> setCellValue( 'Z26', $tanka3 )
                                        -> setCellValue( 'B27', $setno4 )
                                        -> setCellValue( 'D27', $gname4 )
                                        -> setCellValue( 'T27', $suryo4 )
                                        -> setCellValue( 'W27', $tanni4 )
                                        -> setCellValue( 'Z27', $tanka4 );
                        }

                        $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 ) -> getStyle( 'H22' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 ) -> getStyle( 'AC22' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel3 -> setActiveSheetIndex( $sheetNo3 ) -> setTitle( $pRecords[$i]->請求先名->value );

                        $sheetNo3++;

                    }

                } else {

                    if ( $pRecords[$i]->送付先情報->value == "無" ) {

//                        $hurikomi = "下記の金額を平成". ((int)$next[0]-1988). "年". (int)$next[1]. "月". "5日頃ご指定の口座よりお振替させていただきます";
                        $hurikomi = "下記の金額を令和". ((int)$next[0]-2018). "年". (int)$next[1]. "月". "5日(土日祝日の場合は翌営業日)ご指定の口座よりお振替させていただきます";

                        $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 )
                                    -> setCellValue( 'AO3', $seikyuNo )
                                    -> setCellValue( 'AF5', $ymd[0] )
                                    -> setCellValue( 'AJ5', $ymd[1] )
                                    -> setCellValue( 'AM5', $ymd[2] )
                                    -> setCellValue( 'D1' , $postal )
                                    -> setCellValue( 'D2' , $pRecords[$i]->請求書送付先住所->value )
                                    -> setCellValue( 'D3' , $name1 )
                                    -> setCellValue( 'D4' , $name2 )
                                    -> setCellValue( 'R4' , $keisyo )
                                    -> setCellValue( 'B15', $hurikomi )
                                    -> setCellValue( 'B18', $pRecords[$i]->請求金額->value );

                        if ( $pRecords[$i]->数量->value ) {
                            if ( $flg3 ) { $setno3 = 3; }
                            if ( $flg4 ) { $setno4 = 4; }
                            $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 )
                                        -> setCellValue( 'B21', "1" )
                                        -> setCellValue( 'D21', "初期登録費用" )
                                        -> setCellValue( 'T21', "1" )
                                        -> setCellValue( 'W21', "ヶ月" )
                                        -> setCellValue( 'Z21', $pRecords[$i]->単価1->value )
                                        -> setCellValue( 'B22', "2" )
                                        -> setCellValue( 'D22', $goods )
                                        -> setCellValue( 'T22', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W22', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z22', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B23', $setno3 )
                                        -> setCellValue( 'D23', $gname3 )
                                        -> setCellValue( 'T23', $suryo3 )
                                        -> setCellValue( 'W23', $tanni3 )
                                        -> setCellValue( 'Z23', $tanka3 )
                                        -> setCellValue( 'B24', $setno4 )
                                        -> setCellValue( 'D24', $gname4 )
                                        -> setCellValue( 'T24', $suryo4 )
                                        -> setCellValue( 'W24', $tanni4 )
                                        -> setCellValue( 'Z24', $tanka4 );
                        } else {
                            if ( $flg3 ) { $setno3 = 2; }
                            if ( $flg4 ) { $setno4 = 3; }
                            $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 )
                                        -> setCellValue( 'B21', "1" )
                                        -> setCellValue( 'D21', $goods )
                                        -> setCellValue( 'T21', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W21', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z21', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B22', $setno3 )
                                        -> setCellValue( 'D22', $gname3 )
                                        -> setCellValue( 'T22', $suryo3 )
                                        -> setCellValue( 'W22', $tanni3 )
                                        -> setCellValue( 'Z22', $tanka3 )
                                        -> setCellValue( 'B23', $setno4 )
                                        -> setCellValue( 'D23', $gname4 )
                                        -> setCellValue( 'T23', $suryo4 )
                                        -> setCellValue( 'W23', $tanni4 )
                                        -> setCellValue( 'Z23', $tanka4 );
                        }

                        $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 ) -> getStyle( 'H18' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 ) -> getStyle( 'AC18' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel2 -> setActiveSheetIndex( $sheetNo2 ) -> setTitle( $pRecords[$i]->請求先名->value );

                        $sheetNo2++;

                    } else if ( $pRecords[$i]->送付先情報->value == "有" ) {

                        // 送付先敬称判断
                        if ( $pRecords[$i]->送付先宛名->value ) {
                            $name3   = $pRecords[$i]->送付先名->value;
                            $name4   = $pRecords[$i]->送付先宛名->value;
                            $keisyo2 = "様";
                        } else {
                            $name3   = "";
                            $name4   = $pRecords[$i]->送付先名->value;
                            $keisyo2 = "御中";
                        }

//                        $hurikomi = "下記の金額を平成". ((int)$next[0]-1988). "年". (int)$next[1]. "月". "5日頃ご指定の口座よりお振替させていただきます";
                        $hurikomi = "下記の金額を令和". ((int)$next[0]-2018). "年". (int)$next[1]. "月". "5日(土日祝日の場合は翌営業日)ご指定の口座よりお振替させていただきます";

                        $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 )
                                    -> setCellValue( 'D1' , $postal )
                                    -> setCellValue( 'D2' , $pRecords[$i]->請求書送付先住所->value )
                                    -> setCellValue( 'D3' , $name3 )
                                    -> setCellValue( 'D4' , $name4 )
                                    -> setCellValue( 'T4' , $keisyo2 )
                                    -> setCellValue( 'AO6', $seikyuNo )
                                    -> setCellValue( 'AF7', $ymd[0] )
                                    -> setCellValue( 'AJ7', $ymd[1] )
                                    -> setCellValue( 'AM7', $ymd[2] )
                                    -> setCellValue( 'D12', $name1 )
                                    -> setCellValue( 'D13', $name2 )
                                    -> setCellValue( 'T13', $keisyo )
                                    -> setCellValue( 'B19', $hurikomi )
                                    -> setCellValue( 'B22', $pRecords[$i]->請求金額->value );

                        if ( $pRecords[$i]->数量->value ) {
                            if ( $flg3 ) { $setno3 = 3; }
                            if ( $flg4 ) { $setno4 = 4; }
                            $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 )
                                        -> setCellValue( 'B25', "1" )
                                        -> setCellValue( 'D25', "初期登録費用" )
                                        -> setCellValue( 'T25', "1" )
                                        -> setCellValue( 'W25', "ヶ月" )
                                        -> setCellValue( 'Z25', $pRecords[$i]->単価1->value )
                                        -> setCellValue( 'B26', "2" )
                                        -> setCellValue( 'D26', $goods )
                                        -> setCellValue( 'T26', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W26', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z26', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B27', $setno3 )
                                        -> setCellValue( 'D27', $gname3 )
                                        -> setCellValue( 'T27', $suryo3 )
                                        -> setCellValue( 'W27', $tanni3 )
                                        -> setCellValue( 'Z27', $tanka3 )
                                        -> setCellValue( 'B28', $setno4 )
                                        -> setCellValue( 'D28', $gname4 )
                                        -> setCellValue( 'T28', $suryo4 )
                                        -> setCellValue( 'W28', $tanni4 )
                                        -> setCellValue( 'Z28', $tanka4 );
                        } else {
                            if ( $flg3 ) { $setno3 = 2; }
                            if ( $flg4 ) { $setno4 = 3; }
                            $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 )
                                        -> setCellValue( 'B25', "1" )
                                        -> setCellValue( 'D25', $goods )
                                        -> setCellValue( 'T25', $pRecords[$i]->数量2->value )
                                        -> setCellValue( 'W25', $pRecords[$i]->単位2->value )
                                        -> setCellValue( 'Z25', $pRecords[$i]->単価2->value )
                                        -> setCellValue( 'B26', $setno3 )
                                        -> setCellValue( 'D26', $gname3 )
                                        -> setCellValue( 'T26', $suryo3 )
                                        -> setCellValue( 'W26', $tanni3 )
                                        -> setCellValue( 'Z26', $tanka3 )
                                        -> setCellValue( 'B27', $setno4 )
                                        -> setCellValue( 'D27', $gname4 )
                                        -> setCellValue( 'T27', $suryo4 )
                                        -> setCellValue( 'W27', $tanni4 )
                                        -> setCellValue( 'Z27', $tanka4 );
                        }

                        $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 ) -> getStyle( 'H22' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 ) -> getStyle( 'AC22' ) -> getNumberFormat() -> setFormatCode( '"\"#,##0' );
                        $pPHPExcel4 -> setActiveSheetIndex( $sheetNo4 ) -> setTitle( $pRecords[$i]->請求先名->value );

                        $sheetNo4++;

                    }

                }

            }

            if ( $sheetNo1 > 0 ) { $ret[0] = 1; }
            if ( $sheetNo2 > 0 ) { $ret[1] = 1; }
            if ( $sheetNo3 > 0 ) { $ret[2] = 1; }
            if ( $sheetNo4 > 0 ) { $ret[3] = 1; }

            $set  = 0;
            $idxM = 0;
            $idxR = 0;
            $aryUpdPrm = array();
            foreach( $pRecords as $i => $rec ) {

                if ( $rec->発行状態->value != "済" ){

                    // ----------------------------------------------------------------
                    // 更新用パラメータ設定
                    // ----------------------------------------------------------------
                    $recObj = new stdClass;
                    $recObj->record = new stdClass;
                    $recObj->id = $rec->レコード番号->value;
                    $recObj->record->発行状態     = $this->valEnc( "済" );
                    $recObj->record->請求書発行日 = $this->valEnc( $today );
                    $recObj->record->請求書発行No = $this->valEnc( $bkseiNo[$set] );
                    $set++;

                    // 更新用パラメータ配列整備
                    //  - APIパラメータ数上限を超えないよう、2次元配列でパラメータを管理する
                    $aryUpdPrm[ $idxM ][ $idxR ] = $recObj;
                    $idxR += 1;
                    if( $idxR == CY_UPD_MAX ) {
                        $idxM += 1;
                        $idxR = 0;
                    }
                    $recObj = null;

                }

            }

            // ----------------------------------------------------------------
            // アプリ更新
            // ----------------------------------------------------------------
            if ( $aryUpdPrm ) {

                $k = new ctKintone();
                $k->parInit();
                $k->intAppID          = $pAppId;
                $k->strContentType    = "Content-Type: application/json";
                foreach( $aryUpdPrm as $key => $val ) {
                    $updData              = new stdClass;
                    $updData->app         = $pAppId;
                    $updData->records     = $val;
                    $k->aryJson = $updData;                        // 更新対象のレコード番号
                    $json = $k->runCURLEXEC( CT_MODE_UPD );
                }
                echo "★更新しました！<br>";
                echo "<br>";

            }

            return ($ret);
        }


        /*************************************************************************/
        /*フォルダ内のファイルを削除する                                         */
        /*************************************************************************/
        function deleteData ( $dir ) {
            if ( $dirHandle = opendir ( $dir )) {
                while ( false !== ( $fileName = readdir ( $dirHandle ) ) ) {
                    if ( $fileName != "." && $fileName != ".." ) {
                        unlink ( $dir.$fileName );
                    }
                }
                closedir ( $dirHandle );
            }
        }


        /*************************************************************************/
        /* メンバ関数                                                            */
        /*************************************************************************/
        function tgfEnc( &$obj , $idx , $fldNm ) {
            $wk = new stdClass;
            $wk->value = mb_convert_encoding( $obj->getFieldValue( $idx , $fldNm ) , "UTF-8", "auto");
            return ( $wk );
        }

        function valEnc( $val ) {
            $wk = new stdClass;
            $wk->value = mb_convert_encoding($val , "UTF-8", "auto");
            return ( $wk );
        }


        /*************************************************************************/
        // kintoneアプリからレコード情報を取得
        /*************************************************************************/
        function getRecords ( $pAppId, $pQuery, $pField) {

            // ----------------------------------------------
            // 対象データを取得
            // ----------------------------------------------
            $k = new ctKintone();
            $k->parInit();                  // API連携用のパラメタを初期化する
            $k->intAppID     = $pAppId;     // アプリID
            $k->arySelFields = $pField;     // 取得するフィールド

            $recno = 0;                     // kintoneデータ取得件数制限の対応
            $i     = 0;                     // 取得データ格納用カウント数
            $ret   = array();

            do {
                // 検索条件を作成する。
                $aryQ = array();
                $aryQ[] = "(". $pQuery .")";
                $aryQ[] = "( レコード番号 > $recno )";
                $k->strQuery = implode( $aryQ , " and ")." order by レコード番号 asc limit 500";

                // http通信を実行する。
                $getDat = $k->runCURLEXEC( CT_MODE_SEL );

                if( $k->intDataCount == 0 ) {
                    break;
                }

                foreach( $getDat->records as $key => $value ) {
                    array_push($ret, $getDat->records[$key]);
                }

                // 次に読み込む条件の設定
                $recno = $getDat->records[ $k->intDataCount - 1 ]->レコード番号->value;

                $i++; // カウントアップ

            } while( $k->intDataCount > 0 );

            return $ret;

        }

    }


?>
